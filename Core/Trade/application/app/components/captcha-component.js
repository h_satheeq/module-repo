import Ember from 'ember';
import sharedService from '../models/shared/shared-service';

export default Ember.Component.extend({
    layoutName: 'components/captcha-component',

    captchaTextStatus: {
        Success: 1,
        Fail: 0
    },

    didInsertElement: function () {
        this._generateCaptchaImage();
    },

    validateCaptcha: function () {
        this._validateCaptchaText();
    }.observes('isCaptchaValReceived'),

    _generateCaptchaImage: function () {
        var that = this;
        var currentTime = new Date().getTime();
        var uniqueCaptchaId = 'captcha-' + currentTime;

        this.set('uniqueCaptchaId', uniqueCaptchaId);

        sharedService.getService('trade').sendCaptchaImageRequest({
            unqReqId: uniqueCaptchaId
        }, function (img) {
            that._onCaptchaImageResponse(img);
        });
    },

    _validateCaptchaText: function () {
        var that = this;
        var captchaText = this.get('captchaText');

        if (captchaText) {
            sharedService.getService('trade').sendCaptchaValidationRequest({
                unqReqId: this.get('uniqueCaptchaId'),
                captcha: captchaText
            }, function (sts) {
                that._onCaptchaTextResponse(sts);
            });
        } else {
            this.sendAction('verifyAction', [this.get('app').lang.messages.notRobot]);
        }
    },

    _onCaptchaImageResponse: function (img) {
        this.set('captchaImage', 'data:image/png;base64,' + img);
    },

    _onCaptchaTextResponse: function (sts) {
        var errorCollection = [];

        if (sts === this.captchaTextStatus.Success) {
            this.set('isCaptchaVerified', true);
        } else {
            errorCollection = [this.get('app').lang.messages.incorrectText];
        }

        this.sendAction('verifyAction', errorCollection);
    },

    actions: {
        changeCaptcha: function () {
            this._generateCaptchaImage();
        }
    }
});

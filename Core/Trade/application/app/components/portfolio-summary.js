import Ember from'ember';
import BaseComponent from './base-component';
import sharedService from '../models/shared/shared-service';
import languageDataStore from '../models/shared/language/language-data-store';
import fieldMetaConfig from '../config/field-meta-config';
import userSettings from '../config/user-settings';
import appConfig from '../config/app-config';

export default BaseComponent.extend({
    layoutName: 'components/portfolio-summary',
    app: languageDataStore.getLanguageObj(),
    currentPortfolio: {},
    portfolioCollection: Ember.A(),
    portfolios: [],
    changeCss: 'appttl-dark-back-color',
    tradeService: sharedService.getService('trade'),
    portfolioDes: sharedService.getService('trade').fieldConfigs.portfolioDes,

    isTablet: function () {
        return appConfig.customisation.isTablet;
    }.property(),

    onReady: function () {
        this.loadPortfolioOptions();

        var titleBar = sharedService.getService('sharedUI').getService('titleBar');

        if (titleBar && titleBar.setDefaultComponent) {
            titleBar.setDefaultComponent(this.layoutName, this);
        }
    }.on('init'),

    loadPortfolioOptions: function () {
        this.set('portfolioCollection', this.tradeService.accountDS.getTradingAccCollection());
    },

    bindPortfolios: function () {
        Ember.run.once(this, this.setPortfolio);
    }.observes('portfolioCollection.@each'),

    setPortfolio: function () {
        var portfolioCollection = this.get('portfolioCollection');
        var currentPortfolio = this.get('currentPortfolio');
        var portfolios = [];

        if (portfolioCollection.length > 0 && !currentPortfolio.tradingAccId) {
            this.set('currentPortfolio', portfolioCollection[0]);
            this.tradeService.accountDS.subscribeTradingAccountInfo(this.get('currentPortfolio'));
        }

        portfolios.pushObjects(portfolioCollection);
        this.set('portfolios', portfolios);
    },

    updateChangeCSS: function () {
        var currentPort = this.get('currentPortfolio');

        if (currentPort && currentPort.gainLoss) {
            var gainLoss = currentPort.get('gainLoss');

            if (gainLoss > 0) {
                this.set('changeCss', 'appttl-up-back-color');
            } else if (gainLoss < 0) {
                this.set('changeCss', 'appttl-down-back-color');
            } else {
                this.set('changeCss', 'appttl-dark-back-color');
            }
        }
    }.observes('currentPortfolio.gainLoss'),

    setDecimals: function () {
        var multiFactors = fieldMetaConfig.multiFactors;
        var exgArray = this.get('currentPortfolio') ? this.get('currentPortfolio').exgArray : '';
        var exchange = exgArray && exgArray.length ? exgArray[0] : '';
        var decimalPlaces = userSettings.displayFormat.decimalPlaces;

        if (exchange && multiFactors) {
            var exchangeFieldMeta = multiFactors[exchange];

            if (exchangeFieldMeta && exchangeFieldMeta.decimalPlaces) {
                decimalPlaces = exchangeFieldMeta.decimalPlaces;
            }
        }

        this.set('decimalPlaces', decimalPlaces);
    }.observes('currentPortfolio'),

    actions: {
        onSelectPortfolio: function (item) {
            var currentPortfolio = this.get('currentPortfolio');
            this.tradeService.accountDS.unSubscribeTradingAccountInfo(currentPortfolio);

            this.set('currentPortfolio', item);
            this.tradeService.accountDS.subscribeTradingAccountInfo(item);
        }
    }
});

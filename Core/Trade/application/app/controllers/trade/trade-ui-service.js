import Ember from 'ember';
import sharedService from '../../models/shared/shared-service';
import utils from '../../utils/utils';
import appConfig from '../../config/app-config';
import authenticationConstants from '../../controllers/authentication/authentication-constants';
import tradeConstants from '../../models/trade/trade-constants';
import ControllerFactory from '../controller-factory';
import appEvents from '../../app-events';

export default Ember.Object.extend({
    subscriptionKey: 'tradeUI',
    serviceMap: {},
    container: undefined,
    isDeviceAwake: false,

    initialize: function (appLanguage) {
        this.set('app', appLanguage);
        this.set('appLayout', sharedService.getService('sharedUI').getService('appLayoutConfig'));
        this.set('tradeService', sharedService.getService('trade'));

        this.tradeService.subscribeAuthSuccess(this, this.subscriptionKey);
    },

    registerService: function (type, service) {
        var serviceMap = this.get('serviceMap');
        serviceMap[type] = service;
    },

    getService: function (type) {
        return this.get('serviceMap')[type];
    },

    _getTitleBar: function () {
        return appConfig.customisation.isMobile ? sharedService.getService('sharedUI').getService('tickerPanel') : sharedService.getService('sharedUI').getService('titleBar');
    },

    onLayoutReady: function (appLayout) {
        this.initializeComponents(appLayout);
    },

    onVisibilityChanged: function (isHidden) {
        var that = this;

        if (!isHidden) {
            that.set('isDeviceAwake', true);

            Ember.run.later(function () {
                that.set('isDeviceAwake', false);
            }, tradeConstants.TimeIntervals.DeviceWakeTimeout);
        }
    },

    initializeComponents: function (appLayout) {
        this.container = appLayout.container;

        var defaultComp = appLayout.container.lookupFactory('component:portfolio-summary').create();
        var titleBar = sharedService.getService('sharedUI').getService('titleBar');

        if (titleBar && titleBar.setDefaultComponent) {
            titleBar.setDefaultComponent(defaultComp.layoutName, defaultComp);
        }

        var orderNotificationComp = appLayout.container.lookupFactory('controller:trade/widgets/order-ticket/components/order-notification-viewer').create();
        this.registerService('order-notification-viewer', orderNotificationComp);

        appEvents.subscribeVisibilityChanged(this, this.get('subscriptionKey'));
    },

    onAuthSuccess: function () {
        var that = this;
        var modal = sharedService.getService('sharedUI').getService('modalPopupId');

        if (modal) {
            modal.send('closeModalPopup');
        }

        Ember.run.next(function () {
            that._invokeChangePassword();

            if (appConfig.customisation.tilaAgreementEnabled && that.tradeService.userDS.authSts === tradeConstants.AuthStatus.FirstTimeLogin) {
                that._invokeTILAAgreement();
            }
        });
    },

    showPopupWidget: function (config, args) {
        var controllerString, routeString;
        controllerString = config.controllerString;
        routeString = config.routeString;

        var widgetController = config.container.lookupFactory(controllerString).create();
        widgetController.set('hideTitle', true);
        widgetController.set('routeString', routeString);

        var widgetPopupView = config.container.lookupFactory(config.viewName).create({dimensions: args.dimensions});
        args.widgetPopupView = widgetPopupView;

        var params = {widgetArgs: args}; // Send full arg object

        // var params = {
        //    widgetArgs: {
        //        sym: args.sym,
        //        exg: args.exg,
        //        inst: args.inst,
        //        side: args.tabId,
        //        order: args.order,
        //        qty: args.qty,
        //        widgetPopupView: widgetPopupView
        //    }
        // };

        widgetPopupView.show(widgetController, function () {
            widgetController.initializeWidget({wn: controllerString.split('/').pop()}, params);
        });

        // Close menu
        var modal = sharedService.getService('sharedUI').getService('modalPopupId');
        modal.send('closeModalPopup');
    },

    loadOtpLogin: function () {
        var modal = sharedService.getService('sharedUI').getService('modalPopupId');

        if (modal && !modal.get('isEnabled')) {
            modal.send('enableOverlay');
            modal.send('showModalPopup');
            modal.set('modalPopupStyle', 'full-height full-width');
        }

        var otpComponent = this.container.lookupFactory('component:index-otp-login').create();
        var route = this.container.lookup('route:application');

        route.render('components/index-otp-login', {
            into: 'application',
            outlet: 'modalPopupContent',
            controller: otpComponent
        });
    },

    notifyTradeConnectionStatus: function () {
        var that = this;

        if (that.get('isDeviceAwake')) {
            Ember.run.later(function () {
                that._showHideConnectionStatus();
            }, tradeConstants.TimeIntervals.ShowNotificationTimeout);
        } else {
            that._showHideConnectionStatus();
        }
    }.observes('tradeService.connectionStatus'),

    showOrderStatusMessage: function (order) {
        var isStatusNotified = false;
        var preNotifiedOrderId = this.get('preNotifiedOrderId');
        var preNotifiedOrderSts = this.get('preNotifiedOrderSts');
        var notificationComp = this.getService('order-notification-viewer');

        if (preNotifiedOrderId) {
            isStatusNotified = (preNotifiedOrderId === order.clOrdId) && (preNotifiedOrderSts === order.ordSts);
        }

        if (!isStatusNotified && notificationComp && Ember.$.isFunction(notificationComp.showOrderStatusMessage)) {
            notificationComp.showOrderStatusMessage(order);

            this.set('preNotifiedOrderId', order.clOrdId);
            this.set('preNotifiedOrderSts', order.ordSts);
        }
    },

    _showHideConnectionStatus: function () {
        var that = this;
        var layoutName = 'components/single-message-viewer';
        var messageComponent = sharedService.getService('sharedUI').getService('single-message-viewer');
        var titleBar = that._getTitleBar();

        if (messageComponent) {
            if (!that.get('tradeService').connectionStatus) {
                messageComponent.set('message', that.get('app') ? that.get('app').lang.messages.tradeDisconnected : '');
                messageComponent.set('showMessage', true);
                messageComponent.set('type', utils.Constants.MessageTypes.Error);
                messageComponent.set('messageCss', '');

                if (titleBar && titleBar.renderNotificationTemplate) {
                    titleBar.renderNotificationTemplate(layoutName, messageComponent);
                }
            } else {
                messageComponent.set('showMessage', false);

                if (titleBar && titleBar.hideNotificationTemplate) {
                    titleBar.hideNotificationTemplate(layoutName, messageComponent);
                }
            }
        }
    },

    _invokeChangePassword: function () {
        if (appConfig.customisation.authenticationMode === authenticationConstants.AuthModes.TradeRetailPriceSso &&
            this.tradeService.userDS.authSts === tradeConstants.AuthStatus.FirstTimeLogin) {
            sharedService.getService('sharedUI').invokeChangePassword(this.container);
        }
    },

    _invokeTILAAgreement: function () {
        var modal = sharedService.getService('sharedUI').getService('modalPopupId');
        modal.set('isEnabled', true);
        modal.set('isOverlayEnabled', true);

        var route = this.container.lookup('route:application');
        var widgetController = ControllerFactory.createController(this.container, 'controller:trade/widgets/tila-agreement');
        var filePath = '/plugin.privateUserAgreement_EN.htm';

        if (sharedService.userSettings.currentLanguage === 'AR') {
            filePath = '/plugin.privateUserAgreement_AR.htm';
        }

        widgetController.set('filePath', filePath);
        var viewName = 'trade/widgets/tila-agreement';
        route.render(viewName, {
            into: 'application',
            outlet: 'modalPopupContent',
            controller: widgetController
        });
    }
});
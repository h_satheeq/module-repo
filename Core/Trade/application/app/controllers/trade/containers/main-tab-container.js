import BaseController from '../../base-controller';
import LanguageDataStore from '../../../models/shared/language/language-data-store';
import sharedService from '../../../models/shared/shared-service';

export default BaseController.extend({
    app: LanguageDataStore.getLanguageObj(),
    exchange: sharedService.getService('price').exchangeDS.getExchange(sharedService.getService('price').userDS.currentExchange),
    currentController: 'controller:trade/widgets/order-list',
    controllerTemplate: 'trade.widgets.order-list',
    layout: 'trade.containers.main-tab-container',
    outlet: 'w1',

    onLoadWidget: function () {
        this.loadCurrentWidget();
    },

    onUnloadWidget: function () {
        this.set('columnDeclarations', []);
    },

    loadCurrentWidget: function () {
        var controllerString = this.get('currentController');
        var controllerTemplate = this.get('controllerTemplate');
        var layout = this.get('layout');
        var outlet = this.get('outlet');
        var route = this.container.lookup('route:application');
        var keys = controllerTemplate.split('.');
        var widgetController = this.container.lookupFactory(controllerString).create();
        widgetController.set('exg', this.get('exchange'));
        widgetController.set('wkey', keys[keys.length - 1]);
        widgetController.set('isClassicView', true);
        widgetController.initializeWidget({wn: controllerString.split('/').pop()});

        route.render(controllerTemplate, {
            into: layout,
            outlet: outlet,
            controller: widgetController
        });
    },

    actions: {
        tabAction: function (id) {
            var controllerStr, controllerTemplate;

            switch (id) {
                case 1 :
                    controllerStr = 'controller:trade/widgets/order-list';
                    controllerTemplate = 'trade.widgets.order-list';
                    break;
                case 2 :
                    controllerStr = 'controller:trade/widgets/portfolio';
                    controllerTemplate = 'trade.widgets.portfolio';
                    break;
                case 3 :
                    controllerStr = 'controller:trade/widgets/account-summary';
                    controllerTemplate = 'trade.widgets.account-summary';
                    break;
                case 4 :
                    controllerStr = 'controller:trade/widgets/deposit';
                    controllerTemplate = 'trade.widgets.deposit';
                    break;
                case 5 :
                    controllerStr = 'controller:trade/widgets/withdrawal';
                    controllerTemplate = 'trade.widgets.withdrawal';
                    break;
                case 6 :
                    controllerStr = 'controller:trade/widgets/transaction-history';
                    controllerTemplate = 'trade.widgets.transaction-history';
                    break;
                default :
                    controllerStr = 'controller:shared/empty';
                    controllerTemplate = 'shared.empty';
                    break;
            }

            this.set('currentController', controllerStr);
            this.set('controllerTemplate', controllerTemplate);
            this.loadCurrentWidget();
        }
    }
});

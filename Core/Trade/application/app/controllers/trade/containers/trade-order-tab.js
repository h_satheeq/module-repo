/* *
 * Created by anushkag on 8/11/2016.
 */
import BaseWidgetContainer from '../../base-widget-container';
import responsiveHandler from '../../../helpers/responsive-handler';
import sharedService from '../../../models/shared/shared-service';

export default BaseWidgetContainer.extend({
    responsiveLevel: undefined,

    init: function () {
        this._super();
        this.initializeResponsive();
    },

    initializeResponsive: function () {
        this.set('responsive', responsiveHandler.create({controller: this, widgetId: 'tradeOrderTab', callback: this.onResponsive, isContainer: true}));
        this.responsive.bindResize(); // For the container responsive behavior no need to call initialize function
    },

    onResponsive: function (responsiveArgs) {
        var controller = responsiveArgs.controller;
        var responsiveLimit = -5; // Level which is map to 1920 x 1440 resolution

        // Skip first responsiveProcess call
        if (controller.responsiveLevel !== undefined) {
            controller._setResponsive(controller, responsiveArgs.responsiveLevel);
        }

        controller.set('responsiveLevel', responsiveArgs.responsiveLevel);

        if (responsiveArgs.responsiveLevel <= responsiveLimit) {
            controller.set('responsive-height-one', 'trade-row-3-with-margin');
            controller.set('responsive-height-two', 'trade-row-bottom');
            controller.set('responsive-height-three', 'trade-row-middle');
        } else {
            controller.set('responsive-height-one', 'trade-row-1');
            controller.set('responsive-height-two', 'trade-row-3-with-margin');
            controller.set('responsive-height-three', 'trade-row-3-with-margin');
        }
    },

    _setResponsive: function (controller, responsiveLevel) {
        controller.set('responsiveLevel', responsiveLevel);
        sharedService.getService('sharedUI').getService('mainPanel').responsiveCallback('trade-order-tab');
    }
});


import TransactionBase from './transaction-base';
import sharedService from '../../../models/shared/shared-service';

export default TransactionBase.extend({
    submit: function () {
        var errors = this.validate();
        var valueDate = this.get('valueDate');
        var depDate = this.utils.validators.isAvailable(valueDate) ? this.utils.formatters.convertToDisplayTimeFormat(valueDate, 'YYYYMMDD') : '';

        if (errors && errors.length > 0) {
            this.set('errorMessages', errors);
            this.showMessagePopup();
        } else {
            sharedService.getService('trade').sendDepositRequest({
                payMtd: this.get('currentPayMethod.Id'),
                cashAccId: this.get('currentAccount.cashAccountId'),
                curr: this.get('currencySelection.DisplayName'),
                amt: this.get('amount'),
                toAcc: this.get('currentBnkAcnt.accId'),
                valDte: depDate
            });
        }
    }
});
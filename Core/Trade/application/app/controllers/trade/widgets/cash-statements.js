import Ember from 'ember';
import TradeTableController from '../controllers/trade-table-controller';
import tradeWidgetConfig from '../../../config/trade-widget-config';
import sharedService from '../../../models/shared/shared-service';
import tradeConstants from '../../../models/trade/trade-constants';
import userSettings from '../../../config/user-settings';

// Cell Views
import ClassicHeaderCell from '../../../views/table/classic-header-cell';
import HeaderCell from '../../../views/table/dual-cells/header-cell';
import ClassicCell from '../../../views/table/classic-cell';
import DualChangeCell from '../../../views/table/dual-cells/dual-change-cell';
import TableRow from '../../../views/table/table-row';

export default TradeTableController.extend({
    DefaultCashStatementPeriod: 30,
    sortMode: 0,
    DateTimeFormat: 'YYYYMMDD',
    defaultGroupColumn: 'trnsType',
    portfolioDes: sharedService.getService('trade').fieldConfigs.portfolioDes,
    defaultColumnIds: tradeWidgetConfig.cashStatement.defaultColumnIds,

    portfolios: Ember.A(),
    cashStatements: Ember.A(),
    sortByOptions: Ember.A(),
    defaultOptionForSorting: {},
    filteredContent: Ember.A(),
    types: Ember.A(),
    defaultOptionForGrouping: {},
    currentPortfolio: {},
    groupedContent: {},
    currentGroup: {},
    fromDate: '',
    toDate: '',
    toEndDate: '',
    datePickerFormat: userSettings.displayFormat.dateFormat.toLowerCase(),

    tradeService: sharedService.getService('trade'),

    onLoadWidget: function () {
        this.tradeService.subscribeTradeMetaReady(this, this.get('wkey'));
        this.tradeService.subscribeTradeDisconnect(this, this.get('wkey'));

        this.set('defaultColumnMapping', tradeWidgetConfig.cashStatement.defaultColumnMapping);
        this.setLangLayoutSettings(sharedService.userSettings.currentLanguage);
        this.setCellViewsScopeToGlobal();
        this.setDropDownOptions();
        this.set('widgetContainerKey', 'cashStatementContainer-' + this.get('wkey'));

        this.setDefaultColumns();
        this.setDefaultColumnDeclarations();

        var today = new Date();
        this.set('toDate', today);
        this.set('toEndDate', today);

        var fromDate = new Date();
        fromDate.setDate(fromDate.getDate() - this.DefaultCashStatementPeriod);
        this.set('fromDate', fromDate);
    },

    setDropDownOptions: function () {
        var app = this.get('app');
        this.set('types', [{label: app.lang.labels.none, code: 0},
            {label: app.lang.labels.date, code: 'date'},
            {label: app.lang.labels.transactionType, code: 'trnsType'},
            {label: app.lang.labels.settlementDate, code: 'settleDate'}]);
        this.set('sortByOptions', [{label: app.lang.labels.date, sortMode: 0}, {label: app.lang.labels.settlementDate, sortMode: 1}]);
        this.set('defaultOptionForGrouping', this.get('types')[0]);
        this.set('defaultOptionForSorting', this.get('sortByOptions')[0]);
        this.set('currentGroup', this.get('defaultOptionForGrouping'));
    },

    onPrepareData: function () {
        this.loadContent();
        this.setErrorMessage();
    },

    onLanguageChanged: function () {
        this.setDropDownOptions();
        this.setErrorMessage();
        this.toggleProperty('isRefreshed');

        Ember.run.next(this, this.setLangLayoutSettings, sharedService.userSettings.currentLanguage);
    },

    onClearData: function () {
        this.set('portfolios', Ember.A());
        this.set('types', Ember.A());
        this.set('content', Ember.A());
        this.set('sortByOptions', Ember.A());
        this.set('masterContent', Ember.A());
        this.set('filteredContent', Ember.A());
        this.set('cashStatements', Ember.A());
        this.set('currentPortfolio', {});
        this.set('columnDeclarations', []);
        this.set('defaultOptionForGrouping', {});
        this.set('defaultOptionForSorting', {});
        this.set('groupedContent', {});
        this.set('currentGroup', {});
    },

    loadContent: function () {
        var portfolios = [];
        var portfolioCollection = this.tradeService.accountDS.getTradingAccCollection();
        var currentPortfolio = this.get('currentPortfolio');
        portfolios.pushObjects(portfolioCollection);

        if (!currentPortfolio || !currentPortfolio.tradingAccId) {
            this.set('currentPortfolio', portfolios.length > 0 ? portfolios[0] : {});
        }

        this.set('portfolios', portfolios);
        this.sendRequest();
    },

    setCellViewsScopeToGlobal: function () {
        Ember.HeaderCell = HeaderCell;
        Ember.ClassicHeaderCell = ClassicHeaderCell;
        Ember.ClassicCell = ClassicCell;
        Ember.DualChangeCell = DualChangeCell;
        Ember.TableRow = TableRow;
    },

    cellViewsForColumns: {
        classicCell: 'Ember.ClassicCell',
        dualChange: 'Ember.DualChangeCell'
    },

    onCheckDataAvailability: function () {
        return this.get('content').length !== 0;
    },

    groupContent: function () {
        var groupArray = Ember.A();
        var group = this.get('currentGroup');

        if (group && group.code) {
            var that = this;
            var currentGroup = group.code;
            var currentContent = this.get('filteredContent');
            var groupedContentMap = {};

            Ember.$.each(currentContent, function (key, row) {
                if (groupedContentMap[row[currentGroup]]) {
                    groupedContentMap[row[currentGroup]].pushObject(row);
                } else {
                    groupedContentMap[row[currentGroup]] = Ember.A([that.getRootRow(row, currentGroup), row]);
                }
            });

            Ember.$.each(groupedContentMap, function (key, rowArray) {
                groupArray.pushObjects(rowArray);
            });

            this.set('groupedContent', groupedContentMap);
        } else {
            // Selecting none in 'group by' while filter is added
            groupArray = this.get('masterContent');
        }

        this.set('content', groupArray);
    },

    getRootRow: function (row, currentGroup) {
        var rootRow = Ember.Object.create(row);
        var defaultGroupColumn = this.get('defaultGroupColumn');

        Ember.$.each(rootRow, function (key) {
            if (key !== currentGroup) {
                if (rootRow.hasOwnProperty(key) && key !== defaultGroupColumn) {
                    rootRow[key] = '';
                }
            } else {
                var groupKey = rootRow[key];
                rootRow.isRoot = true;
                rootRow.groupKey = rootRow[key];

                if (key !== defaultGroupColumn) {
                    rootRow[key] = '';
                }

                rootRow[defaultGroupColumn] = '[ - ]  ' + groupKey;
            }
        });

        return rootRow;
    },

    rearrangeGroupedRows: function (groupKey, isShrunk) {
        if (groupKey) {
            var defaultGroupColumn = this.get('defaultGroupColumn');
            var groupedContent = Ember.$.extend({}, this.get('groupedContent'));
            var groupedContentArray = groupedContent[groupKey];
            var contentArray = Ember.A();

            Ember.$.each(groupedContentArray, function (key, row) {
                if (row && row.isRoot) {
                    Ember.set(row, defaultGroupColumn, isShrunk ? '[ + ]  ' + groupKey : '[ - ]  ' + groupKey);
                    Ember.set(row, 'isShrunk', isShrunk);
                }
            });

            Ember.$.each(groupedContent, function (key, rowArray) {
                groupedContent[key] = rowArray[0].isShrunk ? Ember.A([rowArray[0]]) : rowArray;
            });

            Ember.$.each(groupedContent, function (key, rowArray) {
                contentArray.pushObjects(rowArray);
            });

            this.set('content', contentArray);
        }
    },

    groupDefault: function () {
        var groupCode = this.get('currentGroup').code;

        if (groupCode !== 0) {
            Ember.run.debounce(this, this.groupContent, tradeConstants.TimeIntervals.CashStatementDebounceInterval);
        }
    }.observes('cashStatements.length'),

    sendRequest: function () {
        this.tradeService.cashStatementDS.clearCollection(this.get('wkey'));

        var sortMode = this.get('sortMode');
        var fromDate = this.get('fromDate');
        var toDate = this.get('toDate');
        var cashStatements = this.tradeService.cashStatementDS.getCashStatementColl();

        this.set('filteredContent', cashStatements);
        this.set('content', cashStatements);
        this.set('masterContent', cashStatements);
        this.set('cashStatements', cashStatements);
        this.setRequestTimeout(tradeConstants.TimeIntervals.LoadingInterval, 'content.length');

        if (!this.utils.validators.isAvailable(toDate)) {
            toDate = new Date();
        }

        if (!this.utils.validators.isAvailable(fromDate)) {
            var defaultFromDate = new Date(toDate);
            defaultFromDate.setDate(defaultFromDate.getDate() - this.DefaultCashStatementPeriod);
            fromDate = defaultFromDate;
        }

        var reqObj = {
            secAccNum: this.get('currentPortfolio.tradingAccId'),
            frmDate: this.utils.formatters.convertToDisplayTimeFormat(fromDate, this.DateTimeFormat),
            toDate: this.utils.formatters.convertToDisplayTimeFormat(toDate, this.DateTimeFormat),
            sortMode: sortMode,
            startSeq: 0,
            totalNoRec: 20
        };

        this.tradeService.sendCashStatementRequest(reqObj);
    },

    actions: {
        onSubmit: function () {
            this.sendRequest();
        },

        setSortOption: function (item) {
            this.set('sortMode', item.sortMode);
        },

        clickRow: function (selectedRow, event) {
            var rowData = selectedRow.getProperties('isRoot');

            if (rowData && rowData.isRoot && event.button !== 2) {
                rowData = selectedRow.getProperties('groupKey', 'isShrunk');

                this.rearrangeGroupedRows(rowData.groupKey, !rowData.isShrunk);
            }
        },

        setGroup: function (option) {
            this.set('currentGroup', option);
            this.groupContent();
        },

        setPortfolio: function (option) {
            this.set('currentPortfolio', option);
        },

        fullScreenToggle: function () {
            this.toggleProperty('disableInnerWidgets');
            this.toggleFullScreen('cashStatementContainer-' + this.get('wkey'), this.get('wkey'));
            this.toggleProperty('isRefreshed');
        }
    }
});
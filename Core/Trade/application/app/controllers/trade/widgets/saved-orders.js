import Ember from 'ember';
import OrderList from './order-list';
import OrderParams from '../../../models/trade/business-entities/order-params';
import CheckBoxCell from '../../../views/table/check-box-cell';
import invokerFactory from './order-ticket/order-invoker-factory';
import tradeWidgetConfig from '../../../config/trade-widget-config';
import sharedService from '../../../models/shared/shared-service';

export default OrderList.extend({
    priceService: sharedService.getService('price'),
    tradeService: sharedService.getService('trade'),

    isOrdersNotSelected: true,
    defaultColumnIds: tradeWidgetConfig.savedOrderList.defaultColumnIds,

    groupOptions: [{code: 0, des: '', langKey: 'none'},
        {code: 'symbol', des: '', langKey: 'symbol'},
        {code: 'date', des: '', langKey: 'date', labelKey: 'date'}],

    setAvailabilityOfOrders: function () {
        var savedOrderArray = this.get('content');
        var that = this;

        this.set('isOrdersNotSelected', true);

        Ember.$.each(savedOrderArray, function (key, value) {
            if (value.isChecked) {
                that.set('isOrdersNotSelected', false);
                return false;
            }
        });
    }.observes('content.@each.isChecked'),

    onLoadWidget: function () {
        this.tradeService.subscribeTradeMetaReady(this, this.get('wkey'));
        this.tradeService.subscribeTradeDisconnect(this, this.get('wkey'));

        this.setCellViewsScopeToGlobal();
        this.set('defaultColumnMapping', tradeWidgetConfig.savedOrderList.defaultColumnMapping);
        this.setLangLayoutSettings(sharedService.userSettings.currentLanguage);

        this.setDefaultColumns();
        this.setDefaultColumnDeclarations();
    },

    onUnloadWidget: function () {
        this.set('columnDeclarations', []);
    },

    loadContent: function () {
        this.loadDropDowns();

        var savedOrderList = this.tradeService.savedOrdersDS.getSavedOrderCollection();

        this.set('content', savedOrderList);
        this.set('filteredContent', savedOrderList);
        this.set('masterContent', savedOrderList);
    },

    setCellViewsScopeToGlobal: function () {
        this._super();
        Ember.CheckBoxCell = CheckBoxCell;
        this.cellViewsForColumns.checkBoxCell = 'Ember.CheckBoxCell';
    },

    getRootRow: function (row, currentGroup) {
        var rootRow = Ember.Object.create(row);
        var defaultGroupColumn = this.get('defaultGroupColumn');

        Ember.$.each(rootRow, function (key) {
            if (key !== currentGroup) {
                if (rootRow.hasOwnProperty(key) && key !== defaultGroupColumn) {
                    rootRow[key] = '';
                }
            } else {
                var groupKey = rootRow[key];
                rootRow.isRoot = true;
                rootRow.groupKey = groupKey;
                rootRow.key = key;

                if (key !== defaultGroupColumn) {
                    rootRow[defaultGroupColumn] = '';
                }

                rootRow[key] = '[ - ]  ' + groupKey;
            }
        });

        return rootRow;
    },

    rearrangeGroupedRows: function (groupKey, isShrunk) {
        if (groupKey) {
            var groupedContent = Ember.$.extend({}, this.get('groupedContent'));
            var groupedContentArray = groupedContent[groupKey];
            var contentArray = Ember.A();

            Ember.$.each(groupedContentArray, function (key, row) {
                if (row && row.isRoot) {
                    Ember.set(row, row.key, isShrunk ? '[ + ]  ' + groupKey : '[ - ]  ' + groupKey);
                    Ember.set(row, 'isShrunk', isShrunk);
                }
            });

            Ember.$.each(groupedContent, function (key, rowArray) {
                groupedContent[key] = rowArray[0].isShrunk ? Ember.A([rowArray[0]]) : rowArray;
            });

            Ember.$.each(groupedContent, function (key, rowArray) {
                contentArray.pushObjects(rowArray);
            });

            this.set('content', contentArray);
        }
    },

    loadDropDowns: function () {
        this.loadPortfolioOptions();
        this.loadSideDropdown();
        this.loadGroupOptions();
    },

    _setInvokerProperties: function (invoker, order) {
        if (!invoker) {
            return;
        }

        var oldSym = invoker.get('symbolInfo');
        var newOrder = OrderParams.create();
        var exchange = order.exg;
        var symbol = order.symbol;
        var insType = order.instruTyp;

        if (oldSym && oldSym.sym && oldSym.exg) {
            this.priceService.removeSymbolRequest(oldSym.get('exg'), oldSym.get('sym'), oldSym.get('inst'));
        }

        if (symbol && exchange) {
            this.priceService.addSymbolRequest(exchange, symbol);
            var stock = this.priceService.stockDS.getStock(exchange, symbol, insType);
            invoker.set('symbolInfo', stock);
        } else {
            invoker.set('symbolInfo', {});
        }

        newOrder.setData(order);

        invoker.set('orderParams', newOrder);
        invoker.updateStrategy();
    },

    actions: {
        onSave: function () {
            var that = this;
            var savedOrderArray = this.get('content');

            Ember.$.each(savedOrderArray, function (key, value) {
                if (value.isChecked) {
                    var orderInvoker = invokerFactory.getOrderInvoker();
                    that._setInvokerProperties(orderInvoker, value);

                    var success = orderInvoker.confirmOrder();

                    if (success) {
                        that.set('message', that.get('app').lang.messages.selectedOrdersExe);
                        that.tradeService.savedOrdersDS.removeOrder(value);
                    }
                }
            });
        },

        onDelete: function () {
            var that = this;
            var savedOrderArray = this.get('content');

            Ember.$.each(savedOrderArray, function (key, value) {
                if (value.isChecked) {
                    that.tradeService.savedOrdersDS.removeOrder(value);
                }
            });
        }
    }
});
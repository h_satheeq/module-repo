import Ember from 'ember';
import TradeTableController from '../controllers/trade-table-controller';
import tradeWidgetConfig from '../../../config/trade-widget-config';
import userSettings from '../../../config/user-settings';
import appConfig from '../../../config/app-config';

// Cell Views
import ClassicHeaderCell from '../../../views/table/classic-header-cell';
import HeaderCell from '../../../views/table/dual-cells/header-cell';
import Cell from '../../../views/table/cell';
import ButtonCell from '../../../views/table/button-cell';
import ButtonMenuCell from '../../../views/table/button-menu-cell';
import ClassicCell from '../../../views/table/classic-cell';
import PayMethodCell from '../../../views/table/trade/pay-method-cell';
import TransTypCell from '../../../views/table/trade/transaction-type-cell';
import TransStatusCell from '../../../views/table/trade/transaction-status-cell';
import ClassicProgressCell from '../../../views/table/classic-progress-cell';
import ClassicMappingCell from '../../../views/table/trade/classic-mapping-cell';
import ClassicStatusCell from '../../../views/table/trade/classic-status-cell';
import ChangeCell from '../../../views/table/change-cell';
import UpDownCell from '../../../views/table/up-down-cell';
import DotCell from '../../../views/table/dual-cells/dot-cell';
import DualArrowCell from '../../../views/table/dual-cells/dual-arrow-cell';
import DualChangeCell from '../../../views/table/dual-cells/dual-change-cell';
import DualTextCell from '../../../views/table/dual-cells/dual-text-cell';
import DualCell from '../../../views/table/dual-cells/dual-cell';
import ProgressCell from '../../../views/table/dual-cells/progress-cell';
import TableRow from '../../../views/table/table-row';
import sharedService from '../../../models/shared/shared-service';

export default TradeTableController.extend({
    tradeService: sharedService.getService('trade'),
    exchange: sharedService.getService('price').exchangeDS.getExchange(sharedService.getService('price').userDS.currentExchange),
    fixedColumns: tradeWidgetConfig.transactionHistory.tableParams.numOfFixedColumns,
    defaultColumnIds: tradeWidgetConfig.transactionHistory.defaultColumnIds,

    sortAscending: false,
    isShowTitle: true,

    filteredContent: [],
    types: [],
    status: [],

    defaultGroupColumn: 'symbol',
    sortProperties: ['chTranId'],
    dateFormat: 'YYYYMMDD',
    datePickerFormat: userSettings.displayFormat.dateFormat.toLowerCase(),

    defaultHistoryPeriod: 30,

    // Pop up Widget Parameters
    clickedRowOrderNo: null,
    clickedRowSymbol: null,
    clickedRowExchange: null,

    // Current filter values
    currentAccount: {},
    currentType: {},
    currentStatus: {},

    groupedContent: {},
    group: {},

    fromDate: '',
    toDate: '',
    lan: '',

    // TODO [Arosha] Check getting dropdown values dynamically
    accountColl: Ember.A(),

    isTablet: appConfig.customisation.isTablet,

    updateTransactionList: function () {
        Ember.run.once(this, this._loadTransactionsList);
    }.observes('tradeService.transDS.transactionsCollection.@each'),

    onLoadWidget: function () {
        this.tradeService.subscribeTradeMetaReady(this, this.get('wkey'));
        this.tradeService.subscribeTradeDisconnect(this, this.get('wkey'));

        this.setCellViewsScopeToGlobal();
        this.set('defaultColumnMapping', tradeWidgetConfig.transactionHistory.defaultColumnMapping);
        this.setLangLayoutSettings(sharedService.userSettings.currentLanguage);

        this.setDefaultColumns();
        this.setDefaultColumnDeclarations();

        var fromDate = new Date();
        fromDate.setDate(fromDate.getDate() - this.defaultHistoryPeriod);

        this.set('toDate', new Date());
        this.set('toEndDate', new Date());
        this.set('fromDate', fromDate);
    },

    onPrepareData: function () {
        this.setErrorMessage();
        this.loadContent();
        this._setDropDownDefaults();
    },

    onCheckDataAvailability: function () {
        return this.get('content').length !== 0;
    },

    onClearData: function () {
        this.set('content', Ember.A());
        this.set('masterContent', Ember.A());
        this.set('filteredContent', Ember.A());
    },

    onUnloadWidget: function () {
        this.set('columnDeclarations', []);

        this.tradeService.unSubscribeTradeMetaReady(this, this.get('wkey'));
        this.tradeService.unSubscribeTradeDisconnect(this, this.get('wkey'));
    },

    onLanguageChanged: function () {
        this.set('columnDeclarations', []);
        this.set('status', []);
        this.set('types', []);
        this.set('accountColl', []);

        this.setErrorMessage();
        this.onLoadWidget();
        this.loadContent();
        this._setDropDownDefaults();
    },

    onTradeMetaReady: function () {
        this.loadContent();
    },

    loadContent: function () {
        var accColl = this.tradeService.accountDS.getTradingAccCollection();
        var accountColl = [];
        var app = this.get('app');
        var allLabel = app.lang.labels.all;

        accountColl.pushObject(Ember.Object.create({cashAccount: {invAccNo: allLabel}, isShowAll: true, dispProp: allLabel}));
        accountColl.pushObjects(accColl);

        this.set('accountColl', accountColl);
        this.set('types', [{code: -1, des: app.lang.labels.all, isShowAll: true}, {code: '0', des: app.lang.labels.transTyp_0, langKey: 'transTyp_0'}, {code: '1', des: app.lang.labels.transTyp_1, langKey: 'transTyp_1'}, {code: '2', des: app.lang.labels.transTyp_2, langKey: 'transTyp_2'}]);
        this.set('status', [{code: 0, des: app.lang.labels.all, isShowAll: true}, {code: '1', des: app.lang.labels.transStatus_1, langKey: 'transStatus_1'}, {code: '2', des: app.lang.labels.transStatus_2, langKey: 'transStatus_2'}, /* {code: '3', des: app.lang.labels.transStatus_3, langKey: 'transStatus_3'}, {code: '4', des: app.lang.labels.transStatus_4, langKey: 'transStatus_4'}, */ {code: '5', des: app.lang.labels.transStatus_5, langKey: 'transStatus_5'}, {code: '6', des: app.lang.labels.transStatus_6, langKey: 'transStatus_6'}, {code: '7', des: app.lang.labels.transStatus_7, langKey: 'transStatus_7'}]);

        this.setRequestTimeout(4, 'content.length');
        this._sendTransactionRequest();
        this._loadTransactionsList();
    },

    checkFilterMatch: function (transaction, portfolioFilter, typeFilter, statusFilter) {
        // If an argument is false, that means that filter is not applied
        var isMatchedPortfolioFilter = !portfolioFilter || portfolioFilter.isShowAll;
        var isMatchedTypeFilter = !typeFilter || typeFilter.isShowAll;
        var isMatchedStatusFilter = !statusFilter || statusFilter.isShowAll;

        if (!isMatchedPortfolioFilter) {
            isMatchedPortfolioFilter = transaction.secAccNum === portfolioFilter.secAccNum || transaction.secAccNum === portfolioFilter.cshAccNum;
        }

        if (!isMatchedTypeFilter) {
            isMatchedTypeFilter = transaction.trnTyp === parseInt(typeFilter.code, 10);
        }

        if (!isMatchedStatusFilter) {
            isMatchedStatusFilter = transaction.sts === parseInt(statusFilter.code, 10);
        }

        return isMatchedPortfolioFilter && isMatchedTypeFilter && isMatchedStatusFilter;
    },

    filterTransaction: function () {
        var portfolioFilter = this.get('currentAccount');
        var typFilter = this.get('currentType');
        var statusFilter = this.get('currentStatus');

        var filteredTrans = this.get('masterContent').filter((function (that) {    //eslint-disable-line
            return function (transaction) {
                if (portfolioFilter || typFilter || statusFilter) {
                    return that.checkFilterMatch(transaction, portfolioFilter, typFilter, statusFilter);
                } else {
                    return true;
                }
            };
        })(this));

        this.set('filteredContent', filteredTrans);
        this.set('content', filteredTrans);
    }.observes('currentAccount', 'currentType', 'currentStatus'),

    setCellViewsScopeToGlobal: function () {
        Ember.HeaderCell = HeaderCell;
        Ember.ClassicHeaderCell = ClassicHeaderCell;
        Ember.Cell = Cell;
        Ember.ClassicCell = ClassicCell;
        Ember.ClassicProgressCell = ClassicProgressCell;
        Ember.ClassicMappingCell = ClassicMappingCell;
        Ember.TransTypCell = TransTypCell;
        Ember.PayMethodCell = PayMethodCell;
        Ember.TransStatusCell = TransStatusCell;
        Ember.ClassicStatusCell = ClassicStatusCell;
        Ember.ChangeCell = ChangeCell;
        Ember.UpDownCell = UpDownCell;
        Ember.ButtonCell = ButtonCell;
        Ember.ButtonMenuCell = ButtonMenuCell;
        Ember.DotCell = DotCell;
        Ember.DualArrowCell = DualArrowCell;
        Ember.DualChangeCell = DualChangeCell;
        Ember.DualTextCell = DualTextCell;
        Ember.DualCell = DualCell;
        Ember.ProgressCell = ProgressCell;
        Ember.TableRow = TableRow;      //  TODO [AROSHA] MOVE THIS TO GLOBAL
    },

    cellViewsForColumns: {
        button: 'Ember.ButtonCell',
        buttonMenu: 'Ember.ButtonMenuCell',
        classicProgressCell: 'Ember.ClassicProgressCell',
        classicMappingCell: 'Ember.ClassicMappingCell',
        transTypCell: 'Ember.TransTypCell',
        payMethodCell: 'Ember.PayMethodCell',
        transStatusCell: 'Ember.TransStatusCell',
        classicStatusCell: 'Ember.ClassicStatusCell',
        classicCell: 'Ember.ClassicCell',
        changeCell: 'Ember.ChangeCell',
        upDown: 'Ember.UpDownCell',
        dual: 'Ember.DualCell',
        dualText: 'Ember.DualTextCell',
        dualChange: 'Ember.DualChangeCell',
        progress: 'Ember.ProgressCell',
        dot: 'Ember.DotCell',
        dualArrow: 'Ember.DualArrowCell'
    },

    rearrangeGroupedRows: function (groupKey, isShrunk) {
        if (groupKey) {
            var groupedContent = Ember.$.extend({}, this.get('groupedContent'));
            var groupedContentArray = groupedContent[groupKey];
            var contentArray = Ember.A();
            var defaultGroupColumn = this.get('defaultGroupColumn');

            Ember.$.each(groupedContentArray, function (key, row) {
                if (row && row.isRoot) {
                    Ember.set(row, defaultGroupColumn, isShrunk ? '[ + ]  ' + groupKey : '[ - ]  ' + groupKey);
                    Ember.set(row, 'isShrunk', isShrunk);
                }
            });

            Ember.$.each(groupedContent, function (key, rowArray) {
                groupedContent[key] = rowArray[0].isShrunk ? Ember.A([rowArray[0]]) : rowArray;
            });

            Ember.$.each(groupedContent, function (key, rowArray) {
                contentArray.pushObjects(rowArray);
            });

            this.set('content', contentArray);
        }
    },

    _setupPopup: function (controllerString, routeString, title, dimensions) {
        var widgetController = this.container.lookupFactory(controllerString).create();
        widgetController.set('hideTitle', true);
        widgetController.set('routeString', routeString);
        widgetController.set('title', title);

        var widgetPopupView = this.container.lookupFactory('view:widget-popup-view').create({dimensions: dimensions});

        widgetPopupView.show(widgetController, function () {
            widgetController.initializeWidget({wn: controllerString.split('/').pop()});
        });
    },

    _setDropDownDefaults: function () {
        var app = this.get('app');
        var accountColl = this.get('accountColl');

        if (!this.utils.validators.isAvailable(this.get('currentAccount.cshAccNum'))) {
            this.set('currentAccount', accountColl[0]);
        }

        if (this.utils.validators.isAvailable(this.get('currentType.langKey'))) {
            this.set('currentType.des', app.lang.labels[this.get('currentType.langKey')]);
        } else {
            this.set('currentType', this.get('types')[0]);
        }

        if (this.utils.validators.isAvailable(this.get('currentStatus.langKey'))) {
            this.set('currentStatus.des', app.lang.labels[this.get('currentStatus.langKey')]);
        } else {
            this.set('currentStatus', this.get('status')[0]);
        }
    },

    _sendTransactionRequest: function () {
        var fromDate = this.get('fromDate');
        var toDate = this.get('toDate');

        if (!this.utils.validators.isAvailable(toDate)) {
            toDate = new Date();
        }

        if (!this.utils.validators.isAvailable(fromDate)) {
            var defaultFromDate = new Date(toDate);
            defaultFromDate.setDate(defaultFromDate.getDate() - this.defaultHistoryPeriod);
            fromDate = defaultFromDate;
        }

        if (this.tradeService.userDS.get('mubNo')) {
            this.tradeService.sendTransactionListRequest({
                sMubNo: this.tradeService.userDS.get('mubNo'),
                strDte: this.utils.formatters.convertToDisplayTimeFormat(fromDate, this.dateFormat),
                endDte: this.utils.formatters.convertToDisplayTimeFormat(toDate, this.dateFormat)
            });
        }
    },

    _loadTransactionsList: function () {
        var transactions = this.tradeService.transDS.getTransactionCollection();

        this.set('masterContent', transactions);
        this.filterTransaction();
    },

    actions: {
        sort: function (column) {
            if (!column.get('isSortSupported')) {
                return;
            }

            if (this.get('sortColumn') !== column) {
                this.get('columns').setEach('isSorted', false);
                column.set('isSorted', true);
                this.set('sortColumn', column);
                this.set('sortProperties', [column.get('sortKey')]);
                this.set('isSortApplied', true);
            } else if (this.get('sortColumn') === column) {
                // Handle disabling sorts
                if (this.get('sortAscending') === true) {
                    this.set('sortColumn', undefined);
                    this.set('sortAscending', false);
                    column.set('isSorted', false);
                    this.set('isSortApplied', false);
                    this.set('sortProperties', []);
                } else {
                    this.set('sortProperties', [column.get('sortKey')]);
                    this.toggleProperty('sortAscending');
                }
            }
        },

        clickRow: function (selectedRow, event) {
            var rowData = selectedRow.getProperties('isRoot', 'symbol', 'clOrdId', 'exg');
            var modal = this.get('rightClickOrderList');

            if (!rowData.isRoot) {
                this._super(event);
            }

            if (rowData && rowData.isRoot && event.button !== 2) {
                rowData = selectedRow.getProperties('groupKey', 'isShrunk');
                this.rearrangeGroupedRows(rowData.groupKey, !rowData.isShrunk);
            }

            if (rowData && rowData.symbol) {
                this.set('clickedRowOrderNo', rowData.clOrdId);
                this.set('clickedRowSymbol', rowData.symbol);
                this.set('clickedRowExchange', rowData.exg);
            }

            if (event && event.button === 2) { // Add a constant to this
                modal.send('showModalPopup');
                this.setClickMenuPosition('rightClickOrderList');
            } else {
                modal.send('closeModalPopup');
            }
        },

        setPortfolio: function (option) {
            this.set('currentAccount', option);
        },

        setType: function (option) {
            this.set('currentType', option);
        },

        setStatus: function (option) {
            this.set('currentStatus', option);
        },

        onSubmit: function () {
            this.tradeService.transDS.clearCollection();
            this._sendTransactionRequest();
            this.setRequestTimeout(4, 'content.length');
        },

        onDeposit: function () {
            var controllerString = 'controller:trade/widgets/deposit';
            var routeString = 'trade/widgets/deposit';
            var dimensions = {w: 650, h: 330};
            this._setupPopup(controllerString, routeString, 'Deposit', dimensions);
        },

        onWithdrawal: function () {
            var controllerString = 'controller:trade/widgets/withdrawal';
            var routeString = 'trade/widgets/withdrawal';
            var dimensions = {w: 650, h: 280};
            this._setupPopup(controllerString, routeString, 'Withdrawal', dimensions);
        }
    }
});
import Ember from 'ember';
import TradeBaseController from '../controllers/trade-base-controller';
import utils from '../../../utils/utils';
import sharedService from '../../../models/shared/shared-service';
import InputPassword from '../../../components/input-password';
import userSettings from '../../../config/user-settings';

export default TradeBaseController.extend({
    isShowTitle: true,
    isSubmitDisabled: true,

    messagePopupId: '',
    messagePopup: {},
    errorMessages: [],

    accountColl: [],
    currentAccount: {},
    portfolioDes: sharedService.getService('trade').fieldConfigs.portfolioDes,

    currencySelection: undefined,
    currencyDropdownOptions: [],

    payMethods: [],
    currentPayMethod: {},

    bankAccounts: [],
    currentBnkAcnt: {},
    valueDate: '',
    datePickerFormat: userSettings.displayFormat.dateFormat.toLowerCase(),

    amount: '',

    tradeService: sharedService.getService('trade'),

    onLoadWidget: function () {
        var wkey = this.get('wkey');

        this.set('messagePopupId', ['messagePopup', wkey].join('-'));

        this.tradeService.subscribeTradeMetaReady(this, wkey);
        this.tradeService.subscribeTradeDisconnect(this, wkey);

        this.set('payMethods', [{Id: 1, DisplayName: this.app.lang.labels.cash}]);
        this.set('currentPayMethod', this.get('payMethods')[0]);
    },

    onPrepareData: function () {
        this.loadAccounts();
    },

    onTradeMetaReady: function () {
        this.loadAccounts();
    },

    onAfterRender: function () {
        this.set('messagePopup', this.get(this.get('messagePopupId')));
    },

    onClearData: function () {
        this.set('accountColl', []);
        this.set('currencyDropdownOptions', []);
        this.set('bankAccounts', []);
        this.set('valueDate', '');
        this.set('currentBnkAcnt', {});
        this.set('payMethods', []);
        this.set('currentAccount', {});
        this.set('errorMessages', []);
    },

    onUnloadWidget: function () {
        this.tradeService.unSubscribeTradeMetaReady(this, this.get('wkey'));
        this.tradeService.unSubscribeTradeDisconnect(this, this.get('wkey'));

        this.set('messagePopupId', '');
        this.set('messagePopup', {});
    },

    loadAccounts: function () {
        var that = this;
        var accountColl = this.tradeService.accountDS.getTradingAccCollection();

        if (accountColl && accountColl.length > 0) {
            Ember.$.each(accountColl, function (key, acc) {
                that.tradeService.sendBuyingPowerRequest(acc);
            });

            if (!this.get('currentAccount.cashAccountId')) {
                this.set('currentAccount', accountColl[0]);
            }
        }

        this.set('accountColl', accountColl);
    },

    submit: function () {
        // To be implemented in sub classes
    },

    _onAccountChanged: function () {
        this.set('currencyDropdownOptions', this.tradeService.accountDS.getCurrencyCollection(this.get('currentAccount.tradingAccId')));
        this.set('currencySelection', this.get('currencyDropdownOptions')[0]);

        var bankAccounts = this.tradeService.bankAccountDS.getBankAccColl(this.get('currentAccount.cashAccountId'));
        this.set('bankAccounts', bankAccounts);
        this.set('currentBnkAcnt', bankAccounts[0]);
    }.observes('currentAccount.cashAccountId'),

    validate: function () {
        var isValidationFailed = false;
        var errors = [];

        if (!utils.validators.isAvailable(this.get('currentAccount.cashAccountId'))) {
            isValidationFailed = true;
        }

        if (!utils.validators.isAvailable(this.get('currentBnkAcnt.bnkAccNum'))) {
            isValidationFailed = true;
        }

        var amount = this.get('amount');

        if (!amount || isNaN(amount) || amount <= 0) {
            isValidationFailed = true;
        }

        if (!utils.validators.isAvailable(this.get('valueDate'))) {
            isValidationFailed = true;
        }

        if (isValidationFailed) {
            errors.pushObject(this.app.lang.messages.mandatoryFields);
        }

        this.set('isSubmitDisabled', isValidationFailed);

        return errors;
    },

    reset: function () {
        this.set('valueDate', '');
        this.set('amount', '');
    },

    showMessagePopup: function () {
        var modal = this.get('messagePopup');
        var messagePopup = Ember.$('#' + this.get('messagePopupId'));

        modal.send('showModalPopup');
        messagePopup.draggable();
    },

    actions: {
        onAccountChanged: function (item) {
            this.set('currentAccount', item);
        },

        onPayMtdChanged: function (item) {
            this.set('currentPayMethod', item);
        },

        onCurrencyChanged: function (item) {
            this.set('currencySelection', item);
        },

        onBankAccountChanged: function (item) {
            this.set('currentBnkAcnt', item);
        },

        onSubmit: function () {
            this.submit();
            this.reset();
        },

        onReset: function () {
            this.reset();
        }
    }
});

Ember.Handlebars.helper('input-password', InputPassword);
import Ember from 'ember';

export default Ember.Object.extend({
    disclosedMargin: {default: 0},
    minMargin: {default: 0},
    amendAllowedStsByExg: {default: []},
    cancelAllowedStsByExg: {default: []},
    tifRule: {
        // '*': {10: 0.01, 25: 0.02, 50: 0.05, 100: 0.1, '*': 0.2}      // TODO [Dasun] Activate new rules after 14-06-2017
        '*': {10: 0.05, 25: 0.05, 50: 0.1, 100: 0.25, '*': 0.25}
    },

    /* *
     * Add default values for all exchange specific variable maps
     */
    init: function () {
        var amendAllowedStsByExg = this.get('amendAllowedStsByExg');

        amendAllowedStsByExg.default = ['0', 'A', '1', '5'];
        amendAllowedStsByExg.TDWL = ['0', 'A', '1', '5'];

        var cancelAllowedStsByExg = this.get('cancelAllowedStsByExg');

        cancelAllowedStsByExg.default = ['T', 'O', '0', 'A', '1', '5'];
        cancelAllowedStsByExg.TDWL = ['T', 'O', '0', 'A', '1', '5'];
    }
}).create();

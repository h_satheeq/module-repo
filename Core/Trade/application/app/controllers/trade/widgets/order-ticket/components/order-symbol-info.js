import Ember from 'ember';

export default Ember.Component.extend({
    layoutName: 'trade/widgets/order-ticket/components/order-symbol-info',

    actions: {
        setPriceValue: function (priceVal) {
            this.set('displayPrice', priceVal);
        }
    }
});
import BuyStrategy from './buy-strategy';

export default BuyStrategy.extend({
    isSideDisabled: true,
    isPortfolioDisabled: true,
    isSymbolDisabled: true,
    isSaveDisabled: true,

    executeButtonLabel: 'amend',
    titleKey: 'amendOrder',

    setOrderParams: function () {
        this.setOrderPrice();
    },

    setOrderPrice: function () {
        var orderParams = this.get('orderParams');
        var symbolInfo = this.get('symbolInfo');

        if (this.get('isMarketOrder') && orderParams && symbolInfo.bap) {
            orderParams.set('displayPrice', '');
            orderParams.set('price', symbolInfo.get('bap'));
        }
    },

    resetOrder: function () {
        this.resetAmendOrder();
    }
});
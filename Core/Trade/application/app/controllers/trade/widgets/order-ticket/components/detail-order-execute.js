import OrderExecute from './order-execute';
import appConfig from '../../../../../config/app-config';
import sharedService from '../../../../../models/shared/shared-service';
import tradeConstants from '../../../../../models/trade/trade-constants';

export default OrderExecute.extend({
    layoutName: 'trade/widgets/order-ticket/components/detail-order-execute',
    appConfig: appConfig,
    tradeService: sharedService.getService('trade'),

    isVATEnabled: function () {
        return this.tradeService.userDS.isVatApplicble === tradeConstants.VATStatus.Enabled;
    }.property('tradeService.userDS.isVatApplicble')
});
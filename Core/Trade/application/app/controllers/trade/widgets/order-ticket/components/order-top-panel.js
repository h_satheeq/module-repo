import Ember from 'ember';
import OrderMainDetails from './order-main-details';
import responsiveHandler from '../../../../../helpers/responsive-handler';
import fieldMetaConfig from '../../../../../config/field-meta-config';
import userSettings from '../../../../../config/user-settings';

export default OrderMainDetails.extend({
    layoutName: 'trade/widgets/order-ticket/components/order-top-panel',
    holdingCss: 'holding-trade',

    initializeResponsive: function () {
        var that = this;

        Ember.run.next(function () {
            that.set('responsive', responsiveHandler.create({
                controller: that,
                widgetId: 'order-top-panel',
                callback: that.onResponsive
            }));

            that.responsive.addList('buyingPower-free', [
                {id: 'buyingPower', width: 5}
            ]);

            that.responsive.initialize();
        });
    }.on('didInsertElement'),

    onResize: function () {
        var that = this;

        if (that.responsive) {
            var resHandler = that.responsive;

            // Call onResize when values are changed
            Ember.run.later(function () {
                Ember.run.debounce(resHandler, 'onResize', 500);
            }, 1000);
        }
    }.observes('invoker.orderParams.smblmgnBuyPwr', 'invoker.currentPortfolio.cashAccount.buyPwr', 'invoker.currentHolding.avaiQty', 'invoker.currentHolding.gainLoss'),

    onResponsive: function () {
        // Callback
    },

    setDecimals: function () {
        var multiFactors = fieldMetaConfig.multiFactors;
        var exgArray = this.get('invoker.currentPortfolio') ? this.get('invoker.currentPortfolio').exgArray : '';
        var exchange = exgArray && exgArray.length ? exgArray[0] : '';
        var decimalPlaces = userSettings.displayFormat.decimalPlaces;

        if (exchange && multiFactors) {
            var exchangeFieldMeta = multiFactors[exchange];

            if (exchangeFieldMeta && exchangeFieldMeta.decimalPlaces) {
                decimalPlaces = exchangeFieldMeta.decimalPlaces;
            }
        }

        this.set('decimalPlaces', decimalPlaces);
    }.observes('invoker.currentPortfolio', 'invoker.currentPortfolio.cashAccount'),

    gainLosCss: function () {
        var invoker = this.get('invoker');
        var holding = invoker && invoker.currentHolding ? invoker.get('currentHolding') : null;

        if (holding && holding.gainLoss) {
            var gainLoss = holding.get('gainLoss');
            return gainLoss < 0 ? 'loss' : gainLoss > 0 ? 'gain' : 'colour-normal';
        }

        return 'colour-normal';
    }.property('invoker.currentHolding.gainLoss')
});

import OrderStrategy from './order-strategy';
import sharedService from '../../../../../models/shared/shared-service';

export default OrderStrategy.extend({
    panelClass: 'fade-up-back-color',
    btnClass: 'btn-buy',
    isBuy: true,
    executeButtonLabel: 'buy',

    setOrderPrice: function () {
        var orderParams = this.get('orderParams');
        var symbolInfo = this.get('symbolInfo');

        if (orderParams && symbolInfo.bap) {
            if (this.get('isMarketOrder')) {
                orderParams.set('displayPrice', '');
                orderParams.set('price', symbolInfo.get('bap'));
            } else {
                orderParams.set('displayPrice', symbolInfo.get('bap'));
            }
        }
    },

    verifyOrder: function () {
        var errorsAndWarnings = this._super();

        // Verify buying power
        var invoker = this.get('invoker');
        var order = this.get('orderParams');

        if (sharedService.getService('trade').settings.orderConfig.isValidateHoldings && order && invoker && invoker.currentPortfolio) {
            var buyPwr = order.get('isMar') ? order.get('smblmgnBuyPwr') : invoker.get('currentPortfolio.cashAccount.buyPwr');

            if (order.get('calcNetOrdVal') > buyPwr) {
                errorsAndWarnings.errors.push(this.get('app').lang.messages.ordValLTBuyPwr);
            }
        }

        return errorsAndWarnings;
    }
});
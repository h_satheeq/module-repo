import OrderExecute from '../order-execute';

export default OrderExecute.extend({
    layoutName: 'trade/widgets/order-ticket/components/rapid/rapid-order-execute',

    actions: {
        onBuy: function () {
            // TODO: [satheeqh] Make sure this button action not triggered more than once for a same order
            var that = this;
            var orderParams = this.get('invoker').get('orderParams');
            orderParams.set('ordSide', '1');

            this.get('invoker').executeOrder(function () {
                that.showMessagePopup();
            });
        },

        onSell: function () {
            // TODO: [satheeqh] Make sure this button action not triggered more than once for a same order
            var that = this;
            var orderParams = this.get('invoker').get('orderParams');
            orderParams.set('ordSide', '2');

            this.get('invoker').executeOrder(function () {
                that.showMessagePopup();
            });
        },

        onReset: function () {
            // TODO: [satheeq] Handle on Cancel
            this.get('invoker').resetOrder();
        }
    }
});
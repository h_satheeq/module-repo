/* global Mousetrap */

import Ember from 'ember';
import ModalPopup from '../../../../../components/modal-popup';
import sharedService from '../../../../../models/shared/shared-service';
import appConfig from '../../../../../config/app-config';
import fieldMetaConfig from '../../../../../config/field-meta-config';
import userSettings from '../../../../../config/user-settings';
import tradeConstants from '../../../../../models/trade/trade-constants';

export default ModalPopup.extend({
    layoutName: 'trade/widgets/order-ticket/components/order-confirmation',
    invoker: {},
    exchange: {},
    confirmationId: '',
    cancelId: '',
    tradeService: sharedService.getService('trade'),

    isVATEnabled: function () {
        return this.tradeService.userDS.isVatApplicble === tradeConstants.VATStatus.Enabled;
    }.property('tradeService.userDS.isVatApplicble'),

    initialize: function () {
        this._setValueDecimals();
    }.on('init'),

    expose: function () {
        this._super();
        this.onExchangeChange();
    }.on('didInsertElement'),

    onExchangeChange: function () {
        var invoker = this.get('invoker');
        var orderParams = invoker && invoker.orderParams ? invoker.get('orderParams') : undefined;

        if (orderParams && orderParams.exg) {
            var exg = orderParams.get('exg');
            this.set('exchange', sharedService.getService('price').exchangeDS.getExchange(exg));

            this._setValueDecimals();
        }
    }.observes('invoker.orderParams.exg'),

    isOfflineOrder: function () {
        var exchange = this.get('exchange');
        var invoker = this.get('invoker');

        if (exchange && exchange.stat) {
            var stat = exchange.get('stat');

            return sharedService.getService('price').MarketStatus.isOfflineStatus(stat) && invoker &&
                invoker.orderStrategy && !invoker.orderStrategy.isOfflineOrdWarningDisabled;
        }

        return false;
    }.property('exchange.stat'),

    _setValueDecimals: function () {
        var multiFactors = fieldMetaConfig.multiFactors;
        var exchange = this.get('invoker.orderParams.exg');

        if (exchange && multiFactors) {
            var exchangeFieldMeta = multiFactors[exchange];
            var orderValueDecimals = userSettings.displayFormat.decimalPlaces;

            if (exchangeFieldMeta && exchangeFieldMeta.decimalPlaces) {
                orderValueDecimals = exchangeFieldMeta.decimalPlaces;
            }

            this.set('orderValueDecimals', orderValueDecimals);
        }
    },

    setupKeyEvents: function () {
        this._super();

        var that = this;
        var id = this.get('id');
        var confirmationId = ['confBtn-', id].join('');
        var cancelId = ['cancelBtn-', id].join('');

        this.set('confirmationId', confirmationId);
        this.set('cancelId', cancelId);

        Ember.run.later(function () {
            Ember.$('#' + confirmationId).focus().select();
        }, 100);

        Mousetrap.bind('enter', function () {
            Mousetrap.unbind('enter', that.get('id'));

            // To make sure double enter key press not executed
            Ember.run.throttle(that, that._onEnterKey, 1000);
        }, id);

        Mousetrap.bind('tab', function () {
            that._onTabKey();

            return false; // Prevent other actions
        }, id);

        Mousetrap.bind('shift+tab', function () {
            that._onTabKey();

            return false;
        }, id);
    },

    _onTabKey: function () {
        var focusedElem = Ember.$(document.activeElement);
        var nextElemId = this.get('confirmationId');

        if (focusedElem && focusedElem[0].tagName === 'BUTTON' && focusedElem[0].id === nextElemId) {
            nextElemId = this.get('cancelId');
        }

        Ember.$('#' + nextElemId).focus().select();
    },

    _onEnterKey: function () {
        var focusedElem = Ember.$(document.activeElement);

        if (focusedElem && focusedElem[0].tagName === 'BUTTON' && focusedElem[0].textContent === this.app.lang.labels.cancel) {
            this.send('closeModalPopup');
        } else {
            this.send('confirmOrder');
        }
    },

    actions: {
        confirmOrder: function () {
            this.send('closeModalPopup');

            var success = this.get('invoker').confirmOrder();

            if (success) {
                this.sendAction(this.get('onReset'));
            }
        },

        closeModalPopup: function () {
            this._super();

            Ember.appGlobal.events.isAllowTabKey = true;

            if (!appConfig.customisation.isMobile) {
                var id = this.get('id');

                Mousetrap.unbind('tab', id);
                Mousetrap.unbind('shift+tab', id);
            }
        }
    }
});
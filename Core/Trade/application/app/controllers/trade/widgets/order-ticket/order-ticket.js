/* global Mousetrap */

import Ember from 'ember';
import BaseController from '../../../base-controller';
import OrderParams from '../../../../models/trade/business-entities/order-params';
import invokerFactory from './order-invoker-factory';

import MainDetailComp from './components/order-main-details';
import SymbolInfoComp from './components/order-symbol-info';
import ActionsComp from './components/order-action-container';
import SummaryComp from './components/order-summary';
import ExecuteComp from './components/order-execute';
import sharedService from '../../../../models/shared/shared-service';
import utils from '../../../../utils/utils';
import tradeConstant from '../../../../models/trade/trade-constants';
import OrderConfirmationComp from './components/order-confirmation';
import appEvents from '../../../../app-events';
import userSettings from '../../../../config/user-settings';
import appConfig from '../../../../config/app-config';

export default BaseController.extend({
    componentList: null,
    invoker: {},
    isShowTitle: false,
    priceService: sharedService.getService('price'),
    title: '',
    titleKey: 'orderTicket',
    messagePopupId: '',
    messagePopup: {},
    confirmationPopupId: '',
    confirmationPopup: {},

    qtyFieldId: '',
    orderPriceId: '',
    orderDisQtyId: '',
    orderMinQtyId: '',

    executeComponentId: '',
    actionComponentId: '',
    searchFieldId: '',
    mktSubscriptionMap: {},

    tradeService: sharedService.getService('trade'),

    isTablet: function () {
        return appConfig.customisation.isTablet;
    }.property(),

    onLoadWidget: function () {
        var componentList = [];
        var wkey = this.get('wkey');

        this.tradeService.subscribeTradeMetaReady(this, wkey);
        this.tradeService.subscribeTradeDisconnect(this, wkey);

        this.set('executeComponentId', ['executeComp', wkey].join('-'));
        this.set('actionComponentId', ['actionComp', wkey].join('-'));
        componentList[componentList.length] = this.get('actionComponentId');
        componentList[componentList.length] = this.get('executeComponentId');

        this.set('componentList', componentList);
        this.set('messagePopupId', ['messagePopup', wkey].join('-'));
        this.set('confirmationPopupId', ['confirmationPopup', wkey].join('-'));
        this.set('qtyFieldId', ['qtyField', wkey].join('-'));
        this.set('orderPriceId', ['orderPriceId', wkey].join('-'));
        this.set('orderDisQtyId', ['orderDisQtyId', wkey].join('-'));
        this.set('orderMinQtyId', ['orderMinQtyId', wkey].join('-'));
        this.set('searchFieldId', ['searchField', wkey].join('-'));
        this.set('customerSearchFieldId', ['customerSearchFieldId', wkey].join('-'));
        this.set('isShowTitle', !this.get('hideTitle'));
        this.set('invoker', invokerFactory.getOrderInvoker());
        this._setInvokerProperties();

        appEvents.subscribeSymbolChanged(this.get('wkey'), this, this.get('selectedLink'));
    },

    onAfterRender: function () {
        var that = this;
        var wkey = this.get('wkey');

        Ember.appGlobal.events.isAllowTabKey = true;

        this.set('messagePopup', this.get(this.get('messagePopupId')));
        this.set('confirmationPopup', this.get(this.get('confirmationPopupId')));

        if (!appConfig.customisation.isMobile) {
            Mousetrap.bind('enter', function () {
                that._onEnterKey();
            }, wkey);

            Mousetrap.bind('tab', function () {
                return Ember.appGlobal.events.isAllowTabKey; // Prevent other actions
            }, wkey);

            Mousetrap.bind('shift+tab', function () {
                return Ember.appGlobal.events.isAllowTabKey;
            }, wkey);

            Mousetrap.bind('space', function () {
                return false;
            }, wkey);
        }
    },

    /* *
     * Overwriting mixin function to avoid loading indices
     */
    onWidgetKeysChange: function (args) {
        if (!this.utils.AssetTypes.isIndices(args.inst)) {
            this._super(args);
        }
    },

    onLanguageChanged: function () {
        if (!this.get('invoker.symbolInfo.sym')) {
            this.setTitle();
        }

        this.notifyComponents('onLanguageChanged');

        var msgModal = this.get('messagePopup');
        var confirmModal = this.get('confirmationPopup');

        if (msgModal) {
            msgModal.send('closeModalPopup');
        }

        if (confirmModal) {
            confirmModal.send('closeModalPopup');
        }
    },

    onClearData: function () {
        var invoker = this.get('invoker');

        if (invoker && invoker.symbolInfo) {
            var symbol = invoker.get('symbolInfo');

            if (symbol && symbol.sym) {
                this.priceService.removeSymbolRequest(symbol.get('exg'), symbol.get('sym'), symbol.get('inst'));
            }
        }

        if (invoker && invoker.currentPortfolio) {
            this.tradeService.accountDS.unSubscribeTradingAccountInfo(invoker.get('currentPortfolio'));
        }
    },

    onTradeMetaReady: function () {
        this.notifyComponents('onTradeMetaReady');
    },

    onTradeDisconnect: function () {
        this.onClearData();
    },

    onUnloadWidget: function () {
        var wkey = this.get('wkey');

        this.tradeService.unSubscribeTradeMetaReady(this, wkey);
        this.tradeService.unSubscribeTradeDisconnect(this, wkey);

        this.set('invoker', {});
        this.set('executeComponentId', '');
        this.set('actionComponentId', '');
        this.set('componentList', []);
        this.set('messagePopupId', '');
        this.set('confirmationPopupId', '');
        this.set('qtyFieldId', '');
        this.set('searchFieldId', '');
        this.set('customerSearchFieldId', '');

        Ember.appGlobal.events.isAllowTabKey = true;

        appEvents.unSubscribeSymbolChanged(wkey, this.get('selectedLink'));

        if (!appConfig.customisation.isMobile) {
            Mousetrap.unbind('enter', wkey);
            Mousetrap.unbind('tab', wkey);
            Mousetrap.unbind('shift+tab', wkey);
            Mousetrap.unbind('space', wkey);
        }
    },

    setTitle: function () {
        var invoker = this.get('invoker');
        var title = this.get('app').lang.labels[this.get('titleKey')];

        if (invoker.symbolInfo && invoker.symbolInfo.sym && utils.validators.isAvailable(invoker.symbolInfo.get('dispProp1'))) {
            title = [title, invoker.symbolInfo.get('dispProp1')].join(' - ');
        }

        this.set('title', title);
    }.observes('invoker.symbolInfo.sDes'),

    /* *
     * Main method to prepare order ticket
     * @param symbol
     * @param exchange
     * @param insType
     * @param order - for amend & cancel, order object passed
     */
    prepare: function (symbol, exchange, insType, order, isFocusQty) {
        var invoker = this.get('invoker');
        var oldSym = invoker ? invoker.get('symbolInfo') : undefined;
        var notifyComponent = false;

        if (oldSym && oldSym.sym && oldSym.exg) {
            var oldExg = oldSym.exg;
            this.priceService.removeSymbolRequest(oldSym.get('exg'), oldSym.get('sym'), oldSym.get('inst'));

            if (oldExg !== exchange) {
                if (this.tradeService.tradeMetaDS.isTifDependOnStatus(oldExg)) {
                    this.priceService.removeExchangeRequest(oldExg);
                }

                notifyComponent = true;
            }
        }

        if (symbol && exchange) {
            this.priceService.addSymbolRequest(exchange, symbol, insType);

            var stock = this.priceService.stockDS.getStock(exchange, symbol, insType);
            invoker.set('symbolInfo', stock);

            if (notifyComponent) {
                this.notifyComponents('onReset');
            }
        } else {
            invoker.set('symbolInfo', {});
        }

        this._setInvokerProperties(order);
        this._sendMarketSubscription(exchange);
        this.setTitle();

        if (isFocusQty) {
            this.focusField(this.get('qtyFieldId'));
        }

        this.saveWidget({sym: symbol, exg: exchange, inst: insType});
    },

    focusField: function (fieldId) {
        if (!this.get('isTablet')) {
            Ember.run.later(function () {
                Ember.$('#' + fieldId).focus().select();
            }, 10);
        }
    },

    _setInvokerProperties: function (order) {
        var invoker = this.get('invoker');
        var orderParams = invoker.get('orderParams');

        if (!order) {
            var newOrder = OrderParams.create();

            if (orderParams && orderParams.ordSide && orderParams.ordTyp) {
                newOrder.set('ordSide', orderParams.get('ordSide'));
                newOrder.set('ordTyp', orderParams.get('ordTyp'));
                newOrder.set('tif', orderParams.get('tif'));
                newOrder.set('ordQty', orderParams.get('ordQty'));
                newOrder.set('secAccNum', orderParams.get('secAccNum'));
                newOrder.set('symbolInfo', invoker.get('symbolInfo'));
            }

            orderParams = newOrder;
        } else if (orderParams && orderParams.setData && order.secAccNum) {
            orderParams.setData(order);
        }

        invoker.set('orderParams', orderParams);
    },

    notifyComponents: function (method) {
        var componentList = this.get('componentList');

        if (componentList) {
            Ember.$.each(componentList, function (key, componentId) {
                var component = Ember.View.views[componentId];

                if (component) {
                    component.send(method);
                }
            });
        }
    },

    onBidOfferChanged: function (args) {
        var invoker = this.get('invoker');

        if (invoker && invoker.orderParams && args) {
            var orderParams = invoker.get('orderParams');
            var isBid = args.isBid;

            orderParams.set('ordSide', isBid ? tradeConstant.OrderSide.Sell : tradeConstant.OrderSide.Buy);
        }
    },

    showMessagePopup: function () {
        var modal = this.get('messagePopup');
        var messagePopup = Ember.$('#' + this.get('messagePopupId'));

        modal.send('showModalPopup');
        messagePopup.draggable();
    },

    showOrderConfirmation: function () {
        Ember.appGlobal.events.isAllowTabKey = false;

        var modal = this.get('confirmationPopup');
        modal.send('showModalPopup');
    },

    executeOrder: function () {
        var that = this;

        this.get('invoker').executeOrder(
            function () {
                that.showMessagePopup();
            },

            function () {
                that.showOrderConfirmation();
            });
    },

    _onEnterKey: function () {
        var focusedElem = Ember.$(document.activeElement);

        if (focusedElem) { // To activate mousetrap key binding from text inputs
            focusedElem.blur();

            if (focusedElem[0].type === 'button') {
                var btnLbl = focusedElem[0].textContent;

                if (btnLbl === this.app.lang.labels.save) {
                    this.saveOrder();
                } else if (btnLbl === this.app.lang.labels.reset) {
                    this.resetOrderTicket();
                } else {
                    this.executeOrder();
                }
            } else {
                this.executeOrder();
            }
        } else {
            this.executeOrder();
        }
    },

    saveOrder: function () {
        var that = this;
        var invoker = this.get('invoker');

        if (invoker) {
            this.get('invoker').executeOrder(
                function () {
                    that.showMessagePopup();
                },

                function () {
                    var order = invoker.orderParams;
                    var portfolio = invoker.currentPortfolio;
                    var unqId = Math.floor(Date.now()).toString();
                    var bkId = portfolio && portfolio.exgAccLst && portfolio.exgAccLst[order.get('exg')] ? portfolio.exgAccLst[order.get('exg')].defBkId : '';

                    order.set('unqId', unqId);
                    order.set('bkId', bkId);
                    order.set('isChecked', false);
                    order.set('date', utils.formatters.convertToDisplayTimeFormat(new Date(), userSettings.displayFormat.dateTimeFormat));

                    that.tradeService.savedOrdersDS.saveOrder(order);
                    that.resetOrderTicket();
                });
        }
    },

    resetOrderTicket: function () {
        var invoker = this.get('invoker');

        if (invoker && invoker.orderStrategy) {
            invoker.get('orderStrategy').resetOrder();
        }

        // Notify all components to reset
        this.notifyComponents('onReset');
    },

    _sendMarketSubscription: function (exchange) {
        // This function sends market subscription to receive market status
        // Used to check to show a message to the user at order placement
        // Whenever symbol changes, a corresponding market status is requested from the server with a 10 sec time interval
        // Within that time interval, if user changes the symbols of the same market, a new request will not be sent

        var that = this;

        if (!this.mktSubscriptionMap[exchange]) {
            this.priceService.addExchangeRequest(exchange);
            this.mktSubscriptionMap[exchange] = true;

            Ember.run.later(function () {
                that.priceService.removeExchangeRequest(exchange);
                that.mktSubscriptionMap[exchange] = false;
            }, 10000);
        }
    },

    actions: {
        onSymbolChanged: function (symbol) {
            var exg, sym, inst;

            if (symbol && symbol.sym) {
                exg = symbol.get('exg');
                sym = symbol.get('sym');
                inst = symbol.get('inst');
            }

            this.prepare(sym, exg, inst);
        },

        onExecute: function () {
            this._onEnterKey();
        },

        onReset: function () {
            this.resetOrderTicket();
        },

        onSave: function () {
            this.saveOrder();
        }
    }
});

Ember.Handlebars.helper('order-main-details', MainDetailComp);
Ember.Handlebars.helper('order-symbol-info', SymbolInfoComp);
Ember.Handlebars.helper('order-action-container', ActionsComp);
Ember.Handlebars.helper('order-execute', ExecuteComp);
Ember.Handlebars.helper('order-summary', SummaryComp);
Ember.Handlebars.helper('order-confirmation', OrderConfirmationComp);
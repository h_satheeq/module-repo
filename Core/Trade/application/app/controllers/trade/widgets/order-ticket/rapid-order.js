import Ember from 'ember';
import OrderTicket from './order-ticket';
import RapidOrderAction from './components/rapid/rapid-order-action';
import RapidOrderMainDetails from './components/rapid/rapid-order-main-details';
import RapidOrderExecute from './components/rapid/rapid-order-execute';

export default OrderTicket.extend({
    title: 'Rapid Order'
});

Ember.Handlebars.helper('rapid-order-action', RapidOrderAction);
Ember.Handlebars.helper('rapid-order-main-details', RapidOrderMainDetails);
Ember.Handlebars.helper('rapid-order-execute', RapidOrderExecute);
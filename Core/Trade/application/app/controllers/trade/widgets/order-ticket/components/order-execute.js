import Ember from 'ember';
import BaseComponent from '../../../../../components/base-component';
import appConfig from '../../../../../config/app-config';
import tradeConstants from '../../../../../models/trade/trade-constants';
import fieldMetaConfig from '../../../../../config/field-meta-config';
import userSettings from '../../../../../config/user-settings';
import utils from '../../../../../utils/utils';
import sharedService from '../../../../../models/shared/shared-service';

export default BaseComponent.extend({
    layoutName: 'trade/widgets/order-ticket/components/order-execute',
    appConfig: appConfig,

    actionLabel: '',

    isTablet: function () {
        return appConfig.customisation.isTablet;
    }.property(),

    onLoadComponent: function () {
        this.setOrderValueDecimals();
    }.on('init'),

    onInsertElement: function () {
        this.setActionLabel();
    }.on('didInsertElement'),

    checkLabelUpdates: function () {
        Ember.run.once(this, this.setActionLabel);
    }.observes('invoker.orderParams.price', 'invoker.orderParams.ordTyp', 'invoker.orderStrategy.executeButtonLabel'),

    setActionLabel: function () {
        var invoker = this.get('invoker');

        if (!invoker.orderParams || !invoker.orderStrategy) {
            this.set('actionLabel', this.get('app').lang.labels.buy);

            return;
        }

        var label = this.get('app').lang.labels[invoker.get('orderStrategy').executeButtonLabel];
        var mktPrcLabel = this.get('app').lang.labels.mkt;
        var orderParams = invoker.get('orderParams');
        var symbolInfo = invoker.get('symbolInfo');

        // TODO: [satheeqh] Default decimal value should be taken from user settings when available via share service.
        var deci = 2;

        if (symbolInfo && symbolInfo.deci) {
            deci = symbolInfo.get('deci');
        }

        if (orderParams && orderParams.action !== tradeConstants.OrderAction.Cancel) {
            var price = orderParams.price;

            if (orderParams.ordTyp === tradeConstants.OrderType.Market) {
                label = [label, utils.Constants.StringConst.At, mktPrcLabel].join(utils.Constants.StringConst.Space);
            } else if (price > 0) {
                label = [label, utils.Constants.StringConst.At, utils.formatters.formatNumber(price, deci)].join(utils.Constants.StringConst.Space);
            }
        }

        this.set('actionLabel', label);
    },

    setOrderValueDecimals: function () {
        var multiFactors = fieldMetaConfig.multiFactors;
        var exchange = this.get('invoker.orderParams.exg');

        if (exchange && multiFactors) {
            var exchangeFieldMeta = multiFactors[exchange];
            var orderValueDecimals = userSettings.displayFormat.decimalPlaces;

            if (exchangeFieldMeta && exchangeFieldMeta.decimalPlaces) {
                orderValueDecimals = exchangeFieldMeta.decimalPlaces;
            }

            this.set('orderValueDecimals', orderValueDecimals);
        }
    }.observes('invoker.orderParams.exg'),

    isTradingDisabledExg: function () {
        var tradeService = sharedService.getService('trade');
        return tradeService && !tradeService.userDS.isTradeEnabledExchange(this.get('invoker.orderParams.exg'));
    }.property('invoker.orderParams.exg'),

    actions: {
        onSubmitOrder: function () {
            this.sendAction('onExecute');
        },

        onResetOrder: function () {
            this.sendAction(this.get('onReset'));
        },

        onSaveOrder: function () {
            this.sendAction(this.get('onSave'));
        },

        showRecentOrders: function () {
            this.sendAction(this.get('showRecentOrders'));
        },

        onLanguageChanged: function () {
            this.setActionLabel();
        },

        onTradeMetaReady: function () {
            // TODO: [satheeqh] This should be implemented in base component. Find a better place.
        },

        onReset: function () {
            // TODO: [satheeqh] This should be implemented in base component. Find a better place.
        }
    }
});
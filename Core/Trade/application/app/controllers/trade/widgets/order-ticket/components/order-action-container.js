import Ember from 'ember';
import BaseComponent from '../../../../../components/base-component';
import tradeConstants from '../../../../../models/trade/trade-constants';
import utils from '../../../../../utils/utils';
import appConfig from '../../../../../config/app-config';
import sharedService from '../../../../../models/shared/shared-service';
import userSettings from '../../../../../config/user-settings';

export default BaseComponent.extend({
    layoutName: 'trade/widgets/order-ticket/components/order-action-container',

    exchange: undefined,
    orderTypeOptions: Ember.A(),
    tifTypeOptions: Ember.A(),
    selectedOrderType: {},
    selectedTifType: {},

    isDatePickerEnabled: false,
    tiffDropdownCSS: 'col-lg-12',
    expDateTime: '',
    defaultGTDays: 1,
    startDateGTD: '',
    endDateGTD: '',
    datePickerFormat: userSettings.displayFormat.dateFormat.toLowerCase(),
    lan: '',

    isBuy: true,
    isLimitOrder: true,
    defaultOrderType: '2',
    qtyCaption: '',

    priceService: sharedService.getService('price'),
    tradeService: sharedService.getService('trade'),
    partialControlPanel: 'trade/widgets/order-ticket/components/partial/general-panel',

    isTablet: appConfig.customisation.isTablet,

    mktPlaceholder: function () {
        var invoker = this.get('invoker');
        return invoker && invoker.orderStrategy && invoker.orderStrategy.isMarketOrder ?
            this.get('app').lang.labels.marketPrice : '';
    }.property('invoker.orderStrategy.isMarketOrder', 'lan'),

    onReady: function () {
        this.onPortfolioChanged();
        this.onTiffChanged();
        this.updateQuantityCaption();

        this.set('startDateGTD', new Date());
        this.set('isDiscloseQtyEnabled', appConfig.customisation.isDiscloseQtyEnabled);
        this.set('lan', sharedService.userSettings.currentLanguage.toLowerCase());
    }.on('didInsertElement'),

    onSideChange: function () {
        var invoker = this.get('invoker');
        var side = this.get('isBuy') ? tradeConstants.OrderSide.Buy : tradeConstants.OrderSide.Sell;

        if (invoker && invoker.orderParams) {
            invoker.orderParams.set('ordSide', side);
        }
    }.observes('isBuy'),

    onOrdTypeChange: function () {
        var invoker = this.get('invoker');
        var type = this.get('isLimitOrder') ? tradeConstants.OrderType.Limit : tradeConstants.OrderType.Market;

        if (invoker && invoker.orderParams) {
            invoker.orderParams.set('ordTyp', type);
        }
    }.observes('isLimitOrder'),
    /* *
     * Update dropdown options based on exchange while portfolio selected.
     */
    onPortfolioChanged: function () {
        var invoker = this.get('invoker');

        if (invoker.currentPortfolio && invoker.currentPortfolio.tradingAccId) {
            this.loadTypeDropdown();
        }
    }.observes('invoker.currentPortfolio'),

    updateQuantityCaption: function () {
        var langLabels = this.get('app').lang.labels;
        var symbolInfo = this.get('invoker.symbolInfo');

        // TODO: [Bashitha] Instrument type is now available as both numeric and as string, make it numeric in all places
        var instType = symbolInfo && !isNaN(symbolInfo.inst) ? parseInt(symbolInfo.inst, 10) : -1;
        var qtyCaption = symbolInfo && utils.AssetTypes.isOption(instType) ? langLabels.contracts : langLabels.quantity;

        this.set('qtyCaption', qtyCaption);
    }.observes('invoker.symbolInfo'),

    onLanguageChanged: function () {
        this._updateDescription(this.get('orderTypeOptions'), this.tradeService.tradeMetaDS.metaMapping.orderType);
        this.updateQuantityCaption();
        this.loadTypeDropdown();
        this.loadTifDropdown();

        this.set('lan', sharedService.userSettings.currentLanguage.toLowerCase());
    },

    /* *
     * Enable/disable date picker and update tiff dropdown css.
     */
    onTiffChanged: function () {
        var invoker = this.get('invoker');

        if (invoker && invoker.orderParams && utils.validators.isAvailable(invoker.orderParams.tif)) {
            var isDatePickerEnabled = false;
            var tiffDropdownCSS = 'col-lg-12';
            var orderParams = invoker.get('orderParams');
            var tif = orderParams.get('tif');
            var orderExpTime = orderParams.get('expTime');

            if (orderParams.action && orderParams.get('action') === tradeConstants.OrderAction.New) {
                orderExpTime = new Date();

                if (tif && tif === tradeConstants.OrderTiffs.GTD) {
                    orderExpTime.setDate(orderExpTime.getDate() + this.defaultGTDays);
                }
            } else if (utils.validators.isAvailable(orderExpTime)) {
                orderExpTime = utils.formatters.convertStringToDate(orderExpTime);
            }

            if (tif && tif === tradeConstants.OrderTiffs.GTD) {
                isDatePickerEnabled = true;
                tiffDropdownCSS = 'date-picker-dropdown';
            }

            this.set('isDatePickerEnabled', isDatePickerEnabled);
            this.set('tiffDropdownCSS', tiffDropdownCSS);
            this.set('expDateTime', orderExpTime);
        }
    }.observes('invoker.orderParams.tif'),

    formatExpTime: function () {
        var invoker = this.get('invoker');
        var expDateTime = this.get('expDateTime');

        if (utils.validators.isAvailable(expDateTime)) {
            expDateTime.setHours(23, 59, 59);
            var expTime = utils.formatters.convertToDisplayTimeFormat(expDateTime, 'YYYYMMDDHHmmss');

            if (invoker && invoker.orderParams) {
                invoker.get('orderParams').set('expTime', expTime);
            }

            this.set('displayDate', utils.formatters.convertToDisplayTimeFormat(expDateTime, 'DD/MM'));
        }
    }.observes('expDateTime'),

    loadTypeDropdown: function () {
        var invoker = this.get('invoker');
        var orderParams = invoker ? invoker.get('orderParams') : undefined;
        var exg = invoker && invoker.get('symbolInfo.exg') ? invoker.get('symbolInfo.exg') : this.tradeService.userDS.get('userExchg')[0];

        var orderTypes = this.tradeService.tradeMetaDS.getOrderTypeCollectionByExchange(exg);
        var types = [];
        types.pushObjects(orderTypes);

        var defaultType = orderParams && orderParams.ordTyp ? orderParams.get('ordTyp') : tradeConstants.OrderType.Limit;
        var selOrderType = this.tradeService.tradeMetaDS.getOrderTypeMapByExchange(exg)[defaultType];

        this.set('defaultOrderType', defaultType);

        // In case default type is not available in meta
        if (!selOrderType && orderTypes.length > 0) {
            selOrderType = orderTypes[0];

            if (orderParams) {
                orderParams.set('ordTyp', selOrderType.code);
            }
        }

        this.set('orderTypeOptions', types);
        this.set('selectedOrderType', selOrderType);
    },

    loadTifDropdown: function () {
        var invoker = this.get('invoker');
        var selectedType = this.get('selectedOrderType');
        var tiffs = [];

        if (selectedType && selectedType.tifTypes) {
            tiffs.pushObjects(selectedType.get('tifTypes'));
        } else {
            var exg = invoker && invoker.get('symbolInfo.exg') ? invoker.get('symbolInfo.exg') : this.tradeService.userDS.get('userExchg')[0];
            var orderType = selectedType && selectedType.code ? selectedType.code : this.get('defaultOrderType');

            if (this.tradeService.tradeMetaDS.isTifDependOnStatus(exg)) {
                this.set('exchange', this.priceService.exchangeDS.getExchange(exg));
                this.priceService.addExchangeRequest(exg);
            }

            tiffs.pushObjects(this.tradeService.tradeMetaDS.getTifTypeCollectionByExchange(exg, orderType, this.get('exchange.stat')));
        }

        var orderParams = invoker ? invoker.get('orderParams') : undefined;
        var defaultTif = orderParams && orderParams.tif ? orderParams.get('tif') : tradeConstants.OrderTiffs.DAY;
        var selectedTif = '';

        Ember.$.each(tiffs, function (key, value) {
            if (value.code === defaultTif) {
                selectedTif = value;
            }
        });

        if (!selectedTif && tiffs.length > 0) {
            selectedTif = tiffs[0];

            if (orderParams) {
                orderParams.set('tif', selectedTif.code);
            }
        }

        this._updateTifTitle();
        this._updateDescription(tiffs, this.tradeService.tradeMetaDS.metaMapping.tifType);
        this.set('tifTypeOptions', tiffs);
        this.set('selectedTifType', selectedTif);
    }.observes('selectedOrderType', 'exchange.stat'),

    _updateTifTitle: function () {
        var that = this;
        var tiffs = this.get('tifTypeOptions');

        Ember.$.each(tiffs, function (key, value) {
            Ember.set(value, 'tiffsTitle', that.get('app').lang.labels[value.des.toLowerCase()]);
        });
    },

    _updateDescription: function (options, type) {
        var that = this;

        if (options) {
            Ember.$.each(options, function (index, option) {
                var description = that.app.lang.labels[[type, option.code].join('_')];
                Ember.set(option, 'des', description);

                if (appConfig.customisation.isMobile) {     // This is used for native controls
                    Ember.set(option, 'text', description);
                    Ember.set(option, 'value', option.code || option.code === 0 ? option.code.toString() : '');
                }
            });
        }
    },

    onTifTypeChanged: function (type) {
        var tifType = (type.code || type.code === 0) ? type : '';
        var tifTypeOptions = this.get('tifTypeOptions');

        if (!tifType) {
            Ember.$.each(tifTypeOptions, function (index, option) {
                if (option.code === parseInt(type, 10)) {
                    tifType = option;

                    return false;
                }
            });
        }

        this.set('selectedTifType', tifType);

        var invoker = this.get('invoker');
        invoker.get('orderParams').set('tif', tifType.code);

        var modal = sharedService.getService('sharedUI').getService('modalPopupId');

        if (modal) {
            modal.send('closeModalPopup');
        }
    },

    _onTradeMetaReady: function () {
        this.onPortfolioChanged();
        this.loadTifDropdown();
    },

    selectTifType: function () {
        var viewName = 'components/tif-dropdown-list';
        var instanceName = 'component:tif-dropdown-list';

        // Render component to application.hbs
        var modal = sharedService.getService('sharedUI').getService('modalPopupId');
        var tifDropdown = this.container.lookupFactory(instanceName).create();

        tifDropdown.showPopup(this, viewName, modal, undefined, true);
    },

    _setGTDMaxDate: function () {
        var exg = this.get('invoker.symbolInfo.exg');
        var tif = this.get('invoker.orderParams.tif');

        if (exg && tif && tif === tradeConstants.OrderTiffs.GTD) {
            var getGtdValidPeriod = this.tradeService.tradeMetaDS.getExgGTDExpiry(exg);

            if (getGtdValidPeriod) {
                var endDateGTD = new Date();

                endDateGTD.setDate(endDateGTD.getDate() + getGtdValidPeriod);
                this.set('endDateGTD', endDateGTD);
            } else {
                this.set('endDateGTD', '');
            }
        }
    }.observes('invoker.orderParams.tif', 'invoker.symbolInfo.exg'),

    actions: {
        onTypeChanged: function (item) {
            var invoker = this.get('invoker');
            this.set('selectedOrderType', item);
            invoker.get('orderParams').set('ordTyp', item.code);
        },

        onTifTypeChanged: function (type) {
            this.onTifTypeChanged(type);
        },

        changeOrderType: function (type) {
            var partialControlPanel;

            switch (type) {
                case 1:
                    partialControlPanel = 'trade/widgets/order-ticket/components/partial/advanced-panel';
                    break;

                case 2:
                    partialControlPanel = 'trade/widgets/order-ticket/components/partial/algorithm-panel';
                    break;

                default:
                    partialControlPanel = 'trade/widgets/order-ticket/components/partial/general-panel';
            }

            this.set('partialControlPanel', partialControlPanel);
        },

        onLanguageChanged: function () {
            this.onLanguageChanged();
        },

        onTradeMetaReady: function () {
            this._onTradeMetaReady();
        },

        // This function should be added to base component and inherit from there.
        onReset: function () {
            this.loadTypeDropdown();
            this.loadTifDropdown();
        },

        selectTifType: function () {
            this.selectTifType();
        }
    }
});
import OrderStrategy from './order-strategy';
import sharedService from '../../../../../models/shared/shared-service';

export default OrderStrategy.extend({
    panelClass: 'fade-down-back-color',
    btnClass: 'btn-sell',
    executeButtonLabel: 'sell',

    setOrderPrice: function () {
        var orderParams = this.get('orderParams');
        var symbolInfo = this.get('symbolInfo');

        if (orderParams && symbolInfo.bbp) {
            if (this.get('isMarketOrder')) {
                orderParams.set('displayPrice', '');
                orderParams.set('price', symbolInfo.get('bbp'));
            } else {
                orderParams.set('displayPrice', symbolInfo.get('bbp'));
            }
        }
    },

    verifyOrder: function () {
        var errorsAndWarnings = this._super();
        this.validateAvailQty(errorsAndWarnings.errors);

        return errorsAndWarnings;
    },

    validateAvailQty: function (errors) {
        // Validate available qty
        var invoker = this.get('invoker');
        var order = this.get('orderParams');

        if (sharedService.getService('trade').settings.orderConfig.isValidateHoldings && order && invoker && invoker.currentHolding && order.get('ordQty') > invoker.get('currentHolding.avaiQty')) {
            errors.push(this.get('app').lang.messages.ordQtyLTAvailQty);
        }

    }
});
import Ember from 'ember';
import ruleVariables from './rule-variables';
import ruleConverter from '../../../../models/trade/rule-converter';

export default Ember.Object.extend({
    validateDisclosedStatus: function (orderParams) {
        var disMargin = ruleVariables.get('disclosedMargin');
        var disQtyMargin = ruleVariables.get('disclosedQtyMargin');

        var mktDisQtyMargin = disQtyMargin ? disQtyMargin[orderParams.exg] : 0;
        var margin = disMargin[orderParams.exg] ? disMargin[orderParams.exg] : disMargin.default;
        var disQtyEnableMargin = orderParams.get('ordQty') < mktDisQtyMargin;

        return orderParams.get('calcOrdVal') < margin || disQtyEnableMargin;
    },

    validateMinFillStatus: function (orderParams) {
        var minMargin = ruleVariables.get('minMargin');
        var margin = minMargin[orderParams.exg] ? minMargin[orderParams.exg] : minMargin.default;

        return orderParams.get('calcOrdVal') < margin;
    },

    getTickSize: function (exg, inst, value, isDecrement) {
        var priceValue = (value && !isNaN(value)) ? value : 0;
        var defaultRuleSet = {'*': {10: 0.01, 25: 0.02, 50: 0.05, 100: 0.1, '*': 0.2}};
        var ruleSet = ruleVariables.tickRule ? ruleVariables.tickRule : defaultRuleSet;

        if (ruleSet[exg]) {
            ruleSet = ruleSet[exg];
        }

        var ruleObj = ruleSet[inst] ? ruleSet[inst] : ruleSet['*'];

        return ruleConverter.convertLessThanRule(ruleObj, priceValue, isDecrement);
    }
}).create();

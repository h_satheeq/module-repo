import Ember from 'ember';
import ruleValidator from '../rule-validator';
import utils from '../../../../../utils/utils';
import tradeConstants from '../../../../../models/trade/trade-constants';
import sharedService from '../../../../../models/shared/shared-service';
import languageDataStore from '../../../../../models/shared/language/language-data-store';
import userSettings from '../../../../../config/user-settings';

export default Ember.Object.extend({
    panelClass: '',
    btnClass: '',

    isPriceDisabled: false,
    isQtyDisabled: false,
    isDisclosedDisabled: true,
    isMinQtyDisabled: true,
    isSideDisabled: false,
    isPortfolioDisabled: false,
    isSymbolDisabled: false,
    isOrderTypeDisabled: false,
    isTiffDisabled: false,
    isOfflineOrdWarningDisabled: false,
    isMarketOrder: false,
    isResetDisabled: false,
    isSaveDisabled: false,

    executeButtonLabel: 'buy',
    titleKey: 'orderTicket',

    orderParams: {},
    symbolInfo: {},
    invoker: {},
    errors: [],
    warnings: [],

    app: languageDataStore.getLanguageObj(),
    priceService: sharedService.getService('price'),
    tradeService: sharedService.getService('trade'),

    setup: function () {
        Ember.addObserver(this.get('orderParams'), 'ordTyp', this, this.setTypeRelatedFieldStatus);
        Ember.addObserver(this.get('orderParams'), 'calcOrdVal', this, this.setOrderValRelatedFieldStatus);
        Ember.addObserver(this.get('orderParams'), 'displayPrice', this, this.setTickSize);
        Ember.addObserver(this, 'symbolInfo', this, this.setOrderParams);

        this.setTypeRelatedFieldStatus();
        this.setOrderValRelatedFieldStatus();
        this.setTickSize();
    }.on('init'),

    destroyStrategy: function () {
        Ember.removeObserver(this.get('orderParams'), 'ordTyp', this, this.setTypeRelatedFieldStatus);
        Ember.removeObserver(this.get('orderParams'), 'calcOrdVal', this, this.setOrderValRelatedFieldStatus);
        Ember.removeObserver(this.get('orderParams'), 'displayPrice', this, this.setTickSize);
        Ember.removeObserver(this, 'symbolInfo', 'sym', this, this.setOrderParams);

        this.set('orderParams', {});
        this.set('symbolInfo', {});
        this.set('errors', []);
        this.set('warnings', []);
    },

    setTypeRelatedFieldStatus: function () {
        var orderParams = this.get('orderParams');

        if (orderParams) {
            var isMarketOrder = orderParams.ordTyp === tradeConstants.OrderType.Market;
            this.set('isMarketOrder', isMarketOrder);
            this.set('isPriceDisabled', isMarketOrder);
        }

        this.setOrderPrice();
    },

    setTickSize: function () {
        var orderParams = this.get('orderParams');

        if (orderParams) {
            orderParams.set('tickSize', ruleValidator.getTickSize(orderParams.exg, orderParams.instruTyp, orderParams.displayPrice));
        }
    },

    setOrderValRelatedFieldStatus: function () {
        var orderParams = this.get('orderParams');
        var symbolInfo = this.get('symbolInfo');

        // TODO: [Bashitha] Keep only one reference for symbolInfo without duplicating in strategy and params
        symbolInfo = !Ember.$.isEmptyObject(symbolInfo) ? symbolInfo : this.get('orderParams.symbolInfo');

        if (orderParams && orderParams.get('calcOrdVal') >= 0 && symbolInfo && symbolInfo.inst !== undefined) {
            var isDisQtyDisabled = utils.AssetTypes.isOption(symbolInfo.inst) || ruleValidator.validateDisclosedStatus(orderParams);

            this.set('isDisclosedDisabled', isDisQtyDisabled);
            this.set('isMinQtyDisabled', ruleValidator.validateMinFillStatus(orderParams));
        }

        if (this.get('isDisclosedDisabled') && orderParams && orderParams.disQty) {
            orderParams.set('disQty', '');
        }

        if (this.get('isMinQtyDisabled') && orderParams && orderParams.minQty) {
            orderParams.set('minQty', '');
        }
    },

    setOrderParams: function () {
        var orderParams = this.get('orderParams');
        var symbolInfo = this.get('symbolInfo');

        if (orderParams) {
            if (symbolInfo.sym) {
                orderParams.set('symbol', symbolInfo.get('sym'));
                orderParams.set('exg', symbolInfo.get('exg'));
                orderParams.set('instruTyp', symbolInfo.get('inst'));

                if (!utils.validators.isAvailable(orderParams.get('ordQty'))) {
                    var orderConfig = userSettings.orderConfig;
                    var defaultOrderQty = (orderConfig && orderConfig.defaultOrderQty !== undefined) ? orderConfig.defaultOrderQty : tradeConstants.OrderDefaultQty;

                    orderParams.set('ordQty', defaultOrderQty);
                }

                this.setOrderValRelatedFieldStatus();
            } else {
                orderParams.set('ordQty', '');
                orderParams.set('minQty', '');
                orderParams.set('disQty', '');
            }
        }

        this.setOrderPrice();
    },

    setOrderPrice: function () {
        // Implemented in extended classes
    },

    resetOrder: function () {
        var orderParams = this.get('orderParams');

        if (orderParams) {
            orderParams.set('ordTyp', tradeConstants.OrderType.Limit);
            orderParams.set('tif', tradeConstants.OrderTiffs.DAY);
            orderParams.set('ordQty', '');
            orderParams.set('minQty', '');
            orderParams.set('disQty', '');
            orderParams.set('displayPrice', '');
            orderParams.set('contract', '');
        }
    },

    resetAmendOrder: function () {
        var orderParams = this.get('orderParams');

        if (orderParams) {
            var orderNum = orderParams.get('mubOrdNum');
            var orgOrder = sharedService.getService('trade').orderDS.getOrder(orderNum);

            if (orgOrder) {
                orderParams.set('ordQty', orgOrder.get('ordQty'));
                orderParams.set('minQty', orgOrder.get('minQty'));
                orderParams.set('disQty', orgOrder.get('disQty'));
                orderParams.set('displayPrice', orgOrder.get('price'));
                orderParams.set('contract', orgOrder.get('contract'));
            }

            // Set order type wise price values.
            this.setOrderPrice();
        }
    },

    verifyOrder: function () {
        var orderParams = this.get('orderParams');
        this.set('errors', []);
        this.set('warnings', []);

        var errors = this.get('errors');
        var warnings = this.get('warnings');

        if (!orderParams) {
            errors.push(this.get('app').lang.messages.orderCreationError);

            return {
                errors: errors,
                warnings: warnings
            };
        }

        var sym = orderParams.get('symbol');

        if (!utils.validators.isAvailable(sym)) {
            errors.push(this.get('app').lang.messages.selectValidSymbol);

            return {
                errors: errors,
                warnings: warnings
            };
        }

        this._validatePrice();
        this._validateQty();
        this._validateDisQty();
        this._validateMinFill();
        this._validatePortfolio();

        return {
            errors: this.get('errors'),
            warnings: this.get('warnings')
        };
    },

    _validatePrice: function () {
        var symbolInfo = this.get('symbolInfo');
        var errors = this.get('errors');
        var warnings = this.get('warnings');
        var orderParams = this.get('orderParams');
        var tickSize;

        if (orderParams) {
            tickSize = orderParams.get('tickSize');
        }

        if (!this.get('isMarketOrder')) {    // Limit
            var price = orderParams.price;

            if (!price || isNaN(price) || price <= 0) {
                errors.push(this.get('app').lang.messages.priceGTZero);
            } else if (tickSize > 0 && (parseFloat(price / tickSize).toPrecision(6)) % 1 !== 0) {
                errors.push(this.get('app').lang.messages.priceInvalidTick);
            }

            var orderMinValidation = true;
            var orderMaxValidation = true;
            var isPriceLimitCheckAvailable = this.tradeService.tradeMetaDS.getExgPriceLimitCheck(symbolInfo.get('exg')) === tradeConstants.PriceLimitCheck.Enabled;

            if (isPriceLimitCheckAvailable) {
                orderMinValidation = !utils.AssetTypes.isFixedIncome(symbolInfo.get('ast')) && orderParams.get('ordSide') !== tradeConstants.OrderSide.Buy;
                orderMaxValidation = !utils.AssetTypes.isFixedIncome(symbolInfo.get('ast')) && orderParams.get('ordSide') !== tradeConstants.OrderSide.Sell;
            }

            if (orderMinValidation && price < symbolInfo.get('min') && symbolInfo.get('min') > 0) {
                warnings.push(this.get('app').lang.messages.priceGTMin);
            } else if (orderMaxValidation && price > symbolInfo.get('max') && symbolInfo.get('max') > 0) {
                warnings.push(this.get('app').lang.messages.priceLTMax);
            }
        }
    },

    _validateQty: function () {
        var errors = this.get('errors');
        var ordQty = this.get('orderParams').ordQty;

        if (!ordQty || isNaN(ordQty) || ordQty <= 0) {
            errors.push(this.get('app').lang.messages.quantityGTZero);
        }
    },

    _validateDisQty: function () {
        var errors = this.get('errors');
        var disQty = this.get('orderParams').disQty;
        var ordQty = this.get('orderParams').ordQty;

        if (!this.get('isDisclosedDisabled') && disQty && (isNaN(disQty) || disQty < 0)) {
            errors.push(this.get('app').lang.messages.disQtyGTZero);
        } else if (disQty > ordQty) {
            errors.push(this.get('app').lang.messages.disQtyLTOrderQty);
        } else if (!this.get('isDisclosedDisabled') && disQty && (ordQty * tradeConstants.OrderQtyDisQtyPercentage / 100) > disQty) {
            errors.push(this.get('app').lang.messages.disQtyGTOrdQtyPercentage);
        }
    },

    _validateMinFill: function () {
        var errors = this.get('errors');
        var minQty = this.get('orderParams').minQty;

        if (!this.get('isMinQtyDisabled') && minQty && (isNaN(minQty) || minQty < 0)) {
            errors.push(this.get('app').lang.messages.minFillGTZero);
        } else if (minQty > this.get('orderParams').ordQty) {
            errors.push(this.get('app').lang.messages.minFillLTOrderQty);
        }
    },

    _validatePortfolio: function () {
        var errors = this.get('errors');
        var exgArr = this.get('invoker.currentPortfolio.exgArray');

        if (!exgArr || exgArr.indexOf(this.get('symbolInfo.exg')) < 0) {
            errors.push(this.get('app').lang.messages.invalidPortfolio);
        }
    }
});

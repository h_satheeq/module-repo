import Ember from 'ember';
import OrderTicket from './order-ticket';
import OrderPriceInfo from './components/order-price-info';
import OrderTopPanel from './components/order-top-panel';
import DetailOrderExecute from './components/detail-order-execute';
import ControllerFactory from '../../../controller-factory';
import tradeConstants from '../../../../models/trade/trade-constants';
import sharedService from '../../../../models/shared/shared-service';
import appConfig from '../../../../config/app-config';

export default OrderTicket.extend({
    partialControllers: null,
    topPanelCompId: '',
    orderPriceInfoId: '',

    isTablet: function () {
        return appConfig.customisation.isTablet;
    }.property(),

    onLoadWidget: function () {
        this._super();

        var priceUIService = sharedService.getService('priceUI');

        if (priceUIService) {
            priceUIService.subscribeBidOfferChanged(this, this.get('selectedLink'));
        }

        this.set('partialControllers', []);

        var wkey = this.get('wkey');
        var componentList = this.get('componentList');
        this.set('topPanelCompId', ['topPanelComp', wkey].join('-'));
        this.set('orderPriceInfoId', ['orderPriceInfoComp', wkey].join('-'));
        componentList[componentList.length] = this.get('topPanelCompId');
        componentList[componentList.length] = this.get('orderPriceInfoId');

        this.set('side', this.get('tabId'));

        if (!this.get('isTablet')) {
            this.loadPriceInfoWidget();
        }
    },

    onPrepareData: function () {
        this._super();

        var invoker = this.get('invoker');
        var side = this.get('side'); // If order side set by other widgets.

        if (side && invoker.orderParams) {
            var orderParams = invoker.get('orderParams');
            orderParams.set('ordSide', side);
        }

        this.prepare(this.get('sym'), this.get('exg'), this.get('inst'), true);
    },

    prepare: function (symbol, exchange, insType, isFocusQty) {
        this._super(symbol, exchange, insType, undefined, isFocusQty);

        var partialControllers = this.get('partialControllers');

        if (partialControllers && partialControllers.length > 0) {
            var symbolArgs = {sym: symbol, exg: exchange, inst: insType};

            Ember.$.each(partialControllers, function (key, controller) {
                if (controller) {
                    controller.refreshWidget(symbolArgs);
                }
            });
        }
    },

    depthCallback: function (bidAsk) {
        var invoker = this.get('invoker');
        var side = tradeConstants.OrderSide.Buy;
        var price = 0;

        if (bidAsk && bidAsk.prc) {
            price = bidAsk.prc;

            if (bidAsk.isBid()) {
                side = tradeConstants.OrderSide.Sell;
            }

            if (invoker && invoker.orderParams) {
                var orderParams = invoker.get('orderParams');
                orderParams.set('ordSide', side);

                if (!invoker.orderStrategy.isPriceDisabled) {
                    orderParams.set('displayPrice', price);
                }
            }

            this.focusField(this.get('qtyFieldId'));
        }
    },

    loadPriceInfoWidget: function () {
        var that = this;
        var controllerString = 'controller:price/widgets/market-depth-summary';
        var priceInfoWidgetController = ControllerFactory.createController(this.container, controllerString);
        priceInfoWidgetController.set('sym', this.get('sym'));
        priceInfoWidgetController.set('exg', this.get('exg'));
        priceInfoWidgetController.set('inst', this.get('inst'));
        priceInfoWidgetController.set('mode', 1);
        priceInfoWidgetController.set('clickFn', function (bidAsk) {
            that.depthCallback(bidAsk);
        });
        priceInfoWidgetController.initializeWidget({wn: controllerString.split('/').pop()});

        var route = this.container.lookup('route:application');

        route.render('price/widgets/market-depth-summary', {
            into: 'trade/widgets/order-ticket/order-ticket-landscape',
            outlet: 'priceInfo',
            controller: priceInfoWidgetController
        });

        var partialControllers = this.get('partialControllers');

        if (partialControllers) {
            partialControllers.pushObject(priceInfoWidgetController);
        }
    },

    onUnloadWidget: function () {
        this._super();
        var priceUIService = sharedService.getService('priceUI');

        if (priceUIService) {
            priceUIService.unSubscribeBidOfferChanged(this, this.get('selectedLink'));
        }

        this.set('partialControllers', Ember.A());
    },

    setWidgetLink: function (option, popupContainer) {
        var priceUIService = sharedService.getService('priceUI');

        if (this.get('selectedLink') && priceUIService) {
            priceUIService.unSubscribeBidOfferChanged(this, this.get('selectedLink'));
        }

        this._super(option, popupContainer);

        if (this.get('selectedLink') && priceUIService) {
            priceUIService.subscribeBidOfferChanged(this, this.get('selectedLink'));
        }
    },

    actions: {
        setLink: function (option) {
            this.setWidgetLink(option);
        }
    }
});

Ember.Handlebars.helper('order-price-info', OrderPriceInfo);
Ember.Handlebars.helper('order-top-panel', OrderTopPanel);
Ember.Handlebars.helper('detail-order-execute', DetailOrderExecute);
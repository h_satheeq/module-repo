/* global Mousetrap */

import Ember from 'ember';
import OrderTicket from './order-ticket';
import utils from '../../../../utils/utils';
import tradeConstants from '../../../../models/trade/trade-constants';
import appConfig from '../../../../config/app-config';

export default OrderTicket.extend({
    mainDetailCompId: '',
    prevActiveWidget: undefined,

    onLoadWidget: function () {
        this._super();

        var that = this;
        var wkey = this.get('wkey');
        var componentList = this.get('componentList');

        this.set('mainDetailCompId', ['mainDetailComp', wkey].join('-'));
        componentList[componentList.length] = this.get('mainDetailCompId');
        this.set('side', this.get('tabId'));

        if (!appConfig.customisation.isMobile) {
            Mousetrap.bind('esc', function () {
                var invoker = that.get('invoker');

                if (invoker) {
                    invoker.closePopup();
                }
            }, wkey);

            this.set('prevActiveWidget', Ember.appGlobal.activeWidget);
            Ember.appGlobal.activeWidget = wkey;
        }
    },

    onPrepareData: function () {
        this._super();
        var invoker = this.get('invoker');

        if (invoker && invoker.orderParams) {
            var orderParams = invoker.get('orderParams');
            var side = this.get('side');
            var qty = this.get('qty');
            var secAccNum = this.get('secAccNum');
            var widgetPopupView = this.get('widgetPopupView');
            invoker.set('widgetPopupView', widgetPopupView);

            if (side) {
                orderParams.set('ordSide', side);
            }

            if (qty !== undefined) {
                orderParams.set('ordQty', qty);
            }

            if (secAccNum) {
                orderParams.set('secAccNum', secAccNum);
            }
        }

        this.prepare(this.get('sym'), this.get('exg'), this.get('inst'), this.get('order'), true);
    },

    onUnloadWidget: function () {
        this._super();

        if (!appConfig.customisation.isMobile) {
            Mousetrap.unbind('esc', this.get('wkey'));
            Ember.appGlobal.activeWidget = this.get('prevActiveWidget');
        }
    },

    setTitle: function () {
        var invoker = this.get('invoker');
        var title = '';

        if (invoker && invoker.orderStrategy && invoker.orderStrategy.titleKey) {
            var titleKey = invoker.get('orderStrategy').titleKey;
            title = this.get('app').lang.labels[titleKey];
            var orderParams = invoker.get('orderParams');

            if (orderParams && orderParams.action) {
                var action = orderParams.get('action');

                switch (action) {
                    case tradeConstants.OrderAction.New:
                        if (invoker.symbolInfo && invoker.symbolInfo.sym && utils.validators.isAvailable(invoker.symbolInfo.get('dispProp1'))) {
                            title = [title, invoker.symbolInfo.get('dispProp1')].join(' - ');
                        }
                        break;

                    case tradeConstants.OrderAction.Amend:
                    case tradeConstants.OrderAction.Cancel:
                        if (orderParams.clOrdId && utils.validators.isAvailable(orderParams.get('clOrdId'))) {
                            title = [title, orderParams.get('clOrdId')].join(' - ');
                        }
                        break;
                }
            }
        }

        this.set('title', title);
    }.observes('invoker.orderStrategy.titleKey', 'invoker.symbolInfo.sDes')
});
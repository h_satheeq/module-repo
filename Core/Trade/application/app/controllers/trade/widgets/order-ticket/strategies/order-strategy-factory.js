import Ember from 'ember';
import BuyStrategy from './buy-strategy';
import SellStrategy from './sell-strategy';
import BuyAmendStrategy from './buy-amend-strategy';
import SellAmendStrategy from './sell-amend-strategy';
import BuyCancelStrategy from './buy-cancel-strategy';
import SellCancelStrategy from './sell-cancel-strategy';
import sharedService from '../../../../../models/shared/shared-service';

export default Ember.Object.extend({
    getOrderStrategy: function (orderParams) {
        var strategy;

        var tradeService = sharedService.getService('trade');
        var strategyKey = this._getStrategyKey(orderParams);

        switch (strategyKey) {
            case '1|1|1':   // Gen|Buy|New
                strategy = BuyStrategy.create({orderParams: orderParams});
                break;

            case '1|2|1':   // Gen|Sell|New
                strategy = SellStrategy.create({orderParams: orderParams});
                break;

            case '1|1|2':   // Gen|Buy|Amend
                var buyAmendEnableConfig = tradeService.tradeMetaDS.amendEnableConfig;
                strategy = BuyAmendStrategy.create(Ember.$.extend(buyAmendEnableConfig, {orderParams: orderParams}));

                break;

            case '1|2|2':   // Gen|Sell|Amend
                var sellAmendEnableConfig = tradeService.tradeMetaDS.amendEnableConfig;
                strategy = SellAmendStrategy.create(Ember.$.extend(sellAmendEnableConfig, {orderParams: orderParams}));

                break;

            case '1|1|3':   // Gen|Buy|Cancel
                strategy = BuyCancelStrategy.create({orderParams: orderParams});
                break;

            case '1|2|3':   // Gen|Sell|Cancel
                strategy = SellCancelStrategy.create({orderParams: orderParams});
                break;

            default:   // Advance|Buy|New
                // TODO: [satheeq] Need to implement for other order types and category (2|1|0)
                break;
        }

        return strategy;
    },

    _getStrategyKey: function (orderParams) {
        return [orderParams.orderCat, orderParams.ordSide, orderParams.action].join('|');
    }
}).create();
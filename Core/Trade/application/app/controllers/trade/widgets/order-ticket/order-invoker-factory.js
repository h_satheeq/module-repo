import Ember from 'ember';
import OrderInvoker from './order-invoker';

export default Ember.Object.extend({
    getOrderInvoker: function () {
        return OrderInvoker.create();
    }
}).create();
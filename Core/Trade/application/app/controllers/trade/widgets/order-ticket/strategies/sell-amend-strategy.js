import SellStrategy from './sell-strategy';

export default SellStrategy.extend({
    isSideDisabled: true,
    isPortfolioDisabled: true,
    isSymbolDisabled: true,
    isSaveDisabled: true,

    executeButtonLabel: 'amend',
    titleKey: 'amendOrder',

    setOrderParams: function () {
        this.setOrderPrice();
    },

    setOrderPrice: function () {
        var orderParams = this.get('orderParams');
        var symbolInfo = this.get('symbolInfo');

        if (this.get('isMarketOrder') && orderParams && symbolInfo.bbp) {
            orderParams.set('displayPrice', '');
            orderParams.set('price', symbolInfo.get('bbp'));
        }
    },

    validateAvailQty: function () {
        // Ignore this validation
    },

    resetOrder: function () {
        this.resetAmendOrder();
    }
});
import Ember from 'ember';
import sharedService from '../../../../../models/shared/shared-service';
import tradeConstant from '../../../../../models/trade/trade-constants';
import utils from '../../../../../utils/utils';
import languageDS from '../../../../../models/shared/language/language-data-store';
import appConfig from '../../../../../config/app-config';
import tradeConstants from '../../../../../models/trade/trade-constants';

export default Ember.Component.extend({
    app: languageDS.getLanguageObj(),
    layoutName: 'trade/widgets/order-ticket/components/order-notification-viewer',
    messages: [],

    showMessage: function (orderParams, isMarketOrder, symbolInfo) {
        var messages = [];
        var actionLabel = this.getActionLabel(orderParams);
        var titleBar = appConfig.customisation.isMobile ? sharedService.getService('sharedUI').getService('tickerPanel') : sharedService.getService('sharedUI').getService('titleBar');

        messages.pushObject({txt: actionLabel});
        messages.pushObject({txt: this.app.lang.labels.orderSent});
        messages.pushObject({txt: utils.Constants.StringConst.Colon});

        if (utils.validators.isAvailable(orderParams.clOrdId)) {
            messages.pushObject({txt: this.app.lang.labels.orderId});
            messages.pushObject({txt: utils.Constants.StringConst.Hyphen});
            messages.pushObject({txt: orderParams.get('clOrdId') + utils.Constants.StringConst.Comma});
        }

        messages.pushObject({txt: symbolInfo.get('dispProp1') + utils.Constants.StringConst.Comma, style: 'bold'});

        var sideKey = this.getSideKey(orderParams);
        messages.pushObject({txt: this.app.lang.labels[sideKey], style: sideKey});

        messages.pushObject({txt: orderParams.get('ordQty')});
        messages.pushObject({txt: utils.Constants.StringConst.At});

        if (isMarketOrder) {
            messages.pushObject({txt: this.app.lang.labels.mktPrice});
        } else {
            messages.pushObject({txt: utils.formatters.formatNumber(orderParams.get('price'), symbolInfo.deci)});
        }

        this.set('messages', messages);
        this.set('containerCss', appConfig.customisation.isMobile ? 'appttl-light-bg-fore-color bold highlight-back-color-1 pad-l-l' : 'appttl-height');

        if (titleBar && titleBar.renderNotificationTemplate) {
            titleBar.renderNotificationTemplate(this.layoutName, this, false, tradeConstant.TimeIntervals.OrderNotificationTimeout);
        }
    },

    getSideKey: function (orderParams) {
        if (utils.validators.isAvailable(orderParams.ordSide)) {
            return ['orderSide', orderParams.get('ordSide')].join(utils.Constants.StringConst.Underscore);
        }

        return '';
    },

    getActionLabel: function (orderParams) {
        var label = '';

        if (utils.validators.isAvailable(orderParams.action)) {
            switch (orderParams.action) {
                case tradeConstant.OrderAction.New:
                    label = this.app.lang.labels.new;
                    break;

                case tradeConstant.OrderAction.Amend:
                    label = this.app.lang.labels.amend;
                    break;

                case tradeConstant.OrderAction.Cancel:
                    label = this.app.lang.labels.cancel;
                    break;
            }
        }

        return label;
    },

    showOrderStatusMessage: function (order) {
        var messages = [];
        var titleBar = appConfig.customisation.isMobile ? sharedService.getService('sharedUI').getService('tickerPanel') : sharedService.getService('sharedUI').getService('titleBar');
        var orderStatusText = this._getOrderStatus(order.ordSts);

        var app = this.get('app');
        var separatorText = ' : ';
        var messageSeparator = ' > ';
        var orderStatusCss = this._getOrderStatusCss(order.ordSts);
        var sideKey = this.getSideKey(order);

        messages.pushObject({txt: app.lang.labels[sideKey], style: (order.ordSts === tradeConstants.OrderStatus.Filled || order.ordSts === tradeConstants.OrderStatus.Rejected) ? '' : sideKey});
        messages.pushObject({txt: (order.symbolInfo && order.symbolInfo.get('dispProp1')) ? (order.symbolInfo.get('dispProp1') + utils.Constants.StringConst.Space) : '', style: 'bold'});

        messages.pushObject({txt: messageSeparator, style: 'font-xx-l'});
        messages.pushObject({txt: order.get('ordQty')});
        messages.pushObject({txt: utils.Constants.StringConst.At});

        if (order.ordTyp === tradeConstants.OrderType.Market) {
            messages.pushObject({txt: app.lang.labels.mktPrice});
        } else {
            messages.pushObject({txt: utils.formatters.formatNumber(order.get('price'), order.symbolInfo ? order.symbolInfo.deci : '')});
        }

        messages.pushObject({txt: separatorText});
        messages.pushObject({txt: orderStatusText});

        this.set('messages', messages);
        this.set('containerCss', 'pad-l-lr ' + orderStatusCss);

        if (titleBar && titleBar.renderNotificationTemplate) {
            titleBar.renderNotificationTemplate(this.layoutName, this, false, tradeConstant.TimeIntervals.OrderNotificationTimeout, undefined, true);
        }
    },

    _getOrderStatus: function (value) {
        var labels = this.get('app').lang.labels;

        return value && labels['orderStatus_' + value] ? labels['orderStatus_' + value] : value;
    },

    _getOrderStatusCss: function (value) {
        var orderStsObj = tradeConstants.OrderStatus;
        var orderStatusCss = 'orderStatus_1-progress_bar appttl-light-bg-fore-color bold';

        switch (value) {
            case orderStsObj.Filled:
                orderStatusCss = 'up-back-color';
                break;
            case orderStsObj.Rejected:
                orderStatusCss = 'down-back-color';
                break;
        }

        return orderStatusCss;
    }
});

import BuyAmendStrategy from './buy-amend-strategy';

export default BuyAmendStrategy.extend({
    isPriceDisabled: true,
    isQtyDisabled: true,
    isOfflineOrdWarningDisabled: true,
    isResetDisabled: true,
    isTiffDisabled: true,
    isOrderTypeDisabled: true,

    executeButtonLabel: 'cancel',
    titleKey: 'cancelOrder',

    setTypeRelatedFieldStatus: function () {
        this._super();

        // Always disable price.
        this.set('isPriceDisabled', true);
    },

    /* *
     * Overwrite method to stop updating status on cancel
     */
    setOrderValRelatedFieldStatus: function () {
        // Do nothing
    },

    verifyOrder: function () {
        return {
            errors: [],
            warnings: []
        };
        // Add additional validations
    }
});

import Ember from 'ember';
import sharedService from '../../../../models/shared/shared-service';
import utils from '../../../../utils/utils';
import strategyFactory from './strategies/order-strategy-factory';
import tradeConstants from '../../../../models/trade/trade-constants';
import tradeConstant from '../../../../models/trade/trade-constants';
import LanguageDataStore from '../../../../models/shared/language/language-data-store';

export default Ember.Object.extend({
    orderParams: {},
    symbolInfo: {},
    currentHolding: {},
    currentPortfolio: {},

    orderStrategy: undefined,
    message: '',
    errorMessages: [],
    warningMessages: [],

    widgetPopupView: undefined,
    isKeepPopupOpen: false,

    tradeService: sharedService.getService('trade'),
    app: LanguageDataStore.getLanguageObj(),

    executeOrder: function (errorFn, successFn) {
        var errorsAndWarnings = this.get('orderStrategy').verifyOrder();
        var app = this.get('app');

        if (errorsAndWarnings && errorsAndWarnings.errors && errorsAndWarnings.errors.length > 0) {
            errorFn();
            this.set('message', app.lang.labels.invalidOrder);
            this.set('errorMessages', errorsAndWarnings.errors);
        } else {
            if (errorsAndWarnings && errorsAndWarnings.warnings) {
                this.set('warningMessages', errorsAndWarnings.warnings);
            }

            successFn();
        }
    },

    confirmOrder: function () {
        var response = this.sendOrder();

        if (response) {
            var notificationComp = sharedService.getService('tradeUI').getService('order-notification-viewer');

            if (notificationComp && Ember.$.isFunction(notificationComp.showMessage)) {
                var orderParams = this.get('orderParams');
                var orderStrategy = this.get('orderStrategy');
                var isMarketOrder = orderStrategy.get('isMarketOrder');
                notificationComp.showMessage(orderParams, isMarketOrder, this.get('symbolInfo'));
            }

            this.closePopup();
        }

        return response;
    },

    setStrategy: function () {
        Ember.run.once(this, this.updateStrategy);
    }.observes('orderParams.ordSide', 'orderParams.orderCat', 'orderParams.action'),

    updateStrategy: function () {
        var orderParams = this.get('orderParams');

        if (this.get('orderStrategy')) {
            this.get('orderStrategy').destroyStrategy();
        }

        if (orderParams.ordTyp && orderParams.ordSide && orderParams.action && orderParams.orderCat) {
            var newStrategy = strategyFactory.getOrderStrategy(orderParams);

            if (newStrategy) {
                newStrategy.set('symbolInfo', this.get('symbolInfo'));
                newStrategy.set('invoker', this);
                this.set('orderStrategy', newStrategy);
            } else {
                utils.logger.logError(['[order-invoker] Strategy not found for order params', orderParams.ordTyp, orderParams.ordSide].join(','));
            }
        }
    },

    updateHolding: function () {
        var symbolInfo = this.get('symbolInfo');
        var currentPortfolio = this.get('currentPortfolio');
        var orderParams = this.get('orderParams');

        if (symbolInfo && symbolInfo.sym && currentPortfolio && currentPortfolio.tradingAccId) {
            this.set('currentHolding', this.tradeService.holdingDS.getHolding(currentPortfolio.get('tradingAccId'), symbolInfo.get('sym'), symbolInfo.get('exg'), symbolInfo.get('inst')));
            orderParams.set('secAccNum', currentPortfolio.get('tradingAccId'));
            orderParams.set('curr', currentPortfolio.get('curr'));
            orderParams.set('isMar', false);

            if (currentPortfolio.isMar) {
                this._sendSymbolMarginRequest();
            }
        } else {
            this.set('currentHolding', {});
        }
    }.observes('currentPortfolio.tradingAccId', 'symbolInfo.symbol'),

    onAccountChanged: function () {
        var orderParams = this.get('orderParams');

        if (orderParams && orderParams.secAccNum) {
            this.set('currentPortfolio', this.tradeService.accountDS.getTradingAccount(orderParams.secAccNum));
        }
    }.observes('orderParams.secAccNum'),

    getCommission: function () {
        Ember.run.debounce(this, this.sendCommissionRequest, tradeConstant.TimeIntervals.CommissionRequestDebounceInterval);
    }.observes('orderParams.calcOrdVal'),

    sendCommissionRequest: function () {
        var orderParams = this.get('orderParams');
        var ordVal = orderParams && orderParams.calcOrdVal ? orderParams.get('calcOrdVal') : 0;

        if (ordVal > 0) {
            this.tradeService.sendOrderCommissionRequest(Math.floor(Date.now()).toString(), orderParams);
        } else if (ordVal <= 0 && orderParams) {
            orderParams.set('comsn', 0);
            orderParams.set('vatAmount', 0);
        }
    },

    _sendSymbolMarginRequest: function () {
        var unqReqId = Math.floor(Date.now()).toString();

        this.tradeService.sendSymbolMarginRequest(unqReqId, this.get('orderParams'), this.get('symbolInfo'));
    },

    sendOrder: function () {
        var order = this.get('orderParams');
        var resp = false;

        switch (order.action) {
            case tradeConstants.OrderAction.New:
                var portfolio = this.get('currentPortfolio');
                var bkId = portfolio && portfolio.exgAccLst && portfolio.exgAccLst[order.get('exg')] ? portfolio.exgAccLst[order.get('exg')].defBkId : '';

                Ember.set(order, 'bkId', bkId);
                resp = this.tradeService.sendOrderTicket(order);
                break;

            case tradeConstants.OrderAction.Amend:
                resp = this.tradeService.sendAmendOrder(order);
                break;

            case tradeConstants.OrderAction.Cancel:
                resp = this.tradeService.sendCancelOrder(order);
                break;

            default:
                resp = false;
        }

        return resp;
    },

    closePopup: function () {
        var widgetPopupView = this.get('widgetPopupView');

        if (widgetPopupView && !this.get('isKeepPopupOpen')) {
            widgetPopupView.send('closePopup');
        }
    }
});

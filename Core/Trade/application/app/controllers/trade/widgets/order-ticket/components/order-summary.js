import Ember from 'ember';
import fieldMetaConfig from '../../../../../config/field-meta-config';
import userSettings from '../../../../../config/user-settings';
import sharedService from '../../../../../models/shared/shared-service';
import tradeConstants from '../../../../../models/trade/trade-constants';

export default Ember.Component.extend({
    layoutName: 'trade/widgets/order-ticket/components/order-summary',
    orderValueDecimals: undefined,
    tradeService: sharedService.getService('trade'),

    isVATEnabled: function () {
        return this.tradeService.userDS.isVatApplicble === tradeConstants.VATStatus.Enabled;
    }.property('tradeService.userDS.isVatApplicble'),

    onLoadWidget: function () {
        this.setOrderValueDecimals();
    }.on('init'),

    setOrderValueDecimals: function () {
        var multiFactors = fieldMetaConfig.multiFactors;
        var exchange = this.get('invoker.orderParams.exg');

        if (exchange && multiFactors) {
            var exchangeFieldMeta = multiFactors[exchange];
            var orderValueDecimals = userSettings.displayFormat.decimalPlaces;

            if (exchangeFieldMeta && exchangeFieldMeta.decimalPlaces) {
                orderValueDecimals = exchangeFieldMeta.decimalPlaces;
            }

            this.set('orderValueDecimals', orderValueDecimals);
        }
    }.observes('invoker.orderParams.exg'),

    // TODO: [Atheesan] Remove this function.
    updateVatAmount: function () {
        // Added because vat amount in not updating in Amend / Cancel order popup
    }.observes('invoker.orderParams.vatAmount')
});
import Ember from 'ember';
import TradeTableController from '../controllers/trade-table-controller';
import HeaderCell from '../../../views/table/dual-cells/header-cell';
import Cell from '../../../views/table/cell';
import ClassicCell from '../../../views/table/classic-cell';
import BuySellCell from '../../../views/table/buy-sell-cell';
import UpDownCell from '../../../views/table/up-down-cell';
import ButtonCell from '../../../views/table/button-cell';
import ClassicHeaderCell from '../../../views/table/classic-header-cell';
import tradeWidgetConfig from '../../../config/trade-widget-config';
import TableRow from '../../../views/table/table-row';
import sharedService from '../../../models/shared/shared-service';
import tradeConstants from '../../../models/trade/trade-constants';
import fieldMetaConfig from '../../../config/field-meta-config';
import userSettings from '../../../config/user-settings';
import utils from '../../../utils/utils';
import responsiveHandler from '../../../helpers/responsive-handler';

export default TradeTableController.extend({
    isShowTitle: false,
    isRenderingEnabled: false,
    fixedColumns: tradeWidgetConfig.portfolio.tableParams.numOfFixedColumns,
    rowHeight: 25,
    isShowBuyingPower: false,

    footerArray: Ember.A([]),
    tradeService: sharedService.getService('trade'),
    portfolioDes: sharedService.getService('trade').fieldConfigs.portfolioDes,
    totalValueProp: 'dSym',

    currentPortfolio: {},
    currencySelection: {},
    portfolioDropdownOptions: [],
    currencyDropdownOptions: [],

    clickedRowSymbol: '',
    clickedRowExchange: '',
    clickedRowAvailQty: '',
    clickedRowInst: '',
    clickedRowPortfolio: '',
    portfolioContextItems: [],
    orderTicketConfig: {controllerString: 'controller:trade/widgets/order-ticket/order-ticket-portrait',
        routeString: 'trade/widgets/order-ticket/order-ticket-portrait',
        viewName: 'view:widget-popup-view'},

    defaultColumnIds: tradeWidgetConfig.portfolio.defaultColumnIds,

    onLoadWidget: function () {
        var wkey = this.get('wkey');

        this.tradeService.subscribeTradeMetaReady(this, wkey);
        this.tradeService.subscribeTradeDisconnect(this, wkey);

        this.setCellViewsScopeToGlobal();
        this.set('title', this.get('app').lang.labels.portfolio);
        this.set('widgetContainerKey', 'portfolioContainer-' + wkey);
        this.set('isShowTitle', !this.get('hideTitle'));
        this.set('totalValueProp', tradeWidgetConfig.portfolio.footerParams && tradeWidgetConfig.portfolio.footerParams.totalValueProp ?
            tradeWidgetConfig.portfolio.footerParams.totalValueProp : 'dSym');

        this.set('defaultColumnMapping', tradeWidgetConfig.portfolio.defaultColumnMapping);
        this.set('moreColumnsIds', tradeWidgetConfig.portfolio.moreColumnIds);
        this.set('nonRemovableColumnIds', tradeWidgetConfig.portfolio.nonRemovableColumnIds);
        this.set('enableColumnReorder', !this.isTablet);

        this.setDefaultColumns();
        this.setDefaultColumnDeclarations();

        this.setErrorMessage();
        this.setLangLayoutSettings(sharedService.userSettings.currentLanguage);
        this.loadPortfolioContextItems();
    },

    onPrepareData: function () {
        this.set('sortProperties', ['qty']);

        this.loadPortfolioOptions();
        this.loadCurrencyOptions();
    },

    onAddSubscription: function () {
        this.sendTradeRequests();
    },

    onAfterRender: function () {
        var that = this;
        this.set('isRenderingEnabled', true);

        Ember.run.next(this, function () {
            if (Ember.$.isFunction(that.afterRenderCb)) {
                that.afterRenderCb();
            }
        });
    },

    initializeResponsive: function () {
        this.set('responsive', responsiveHandler.create({controller: this, widgetId: this.get('widgetContainerKey'), callback: this.onResponsive}));

        this.responsive.addList('portfolio-middle', [
            {id: 'portfolioMoreColumns', width: 5}
        ]);

        this.responsive.initialize();
    },

    onClearData: function () {
        this._unSubscribePortfolioInfo();
        this.set('content', Ember.A());
        this.set('portfolioDropdownOptions', Ember.A());
        this.set('currencyDropdownOptions', Ember.A());
        this.set('currentPortfolio', {});
        this.set('currencySelection', {});
        this.set('clickedRowSymbol', {});
        this.set('clickedRowExchange', {});
        this.set('clickedRowInst', {});
        this.set('clickedRowPortfolio', {});
        this.set('clickedRowAvailQty', {});
        this.set('portfolioContextItems', []);
    },

    onUnloadWidget: function () {
        this.tradeService.unSubscribeTradeMetaReady(this.get('wkey'));
        this.tradeService.unSubscribeTradeDisconnect(this, this.get('wkey'));

        this.set('columnDeclarations', []);
    },

    onLanguageChanged: function () {
        this.loadPortfolioOptions();
        this.loadPortfolioContextItems();
        this.setFooterArray();
        this.setErrorMessage();
        this.setDefaultColumns();
        this.setDefaultColumnDeclarations();
        this.toggleProperty('isRefreshed');

        Ember.run.next(this, this.setLangLayoutSettings, sharedService.userSettings.currentLanguage);
    },

    onTradeMetaReady: function () {
        this.onPrepareData();
        this.onAddSubscription();
    },

    sendTradeRequests: function () {
        var portfolios = this.tradeService.accountDS.getTradingAccCollection();
        var that = this;

        if (portfolios && portfolios.length > 0) {
            Ember.$.each(portfolios, function (key, port) {
                that.tradeService.sendHoldingsRequest(port);
            });
        }
    },

    cellViewsForColumns: {
        classic: 'Ember.ClassicCell',
        upDown: 'Ember.UpDownCell',
        buySell: 'Ember.BuySellCell',
        button: 'Ember.ButtonCell'
    },

    setCellViewsScopeToGlobal: function () {
        Ember.HeaderCell = HeaderCell;
        Ember.Cell = Cell;
        Ember.ClassicCell = ClassicCell;
        Ember.UpDownCell = UpDownCell;
        Ember.BuySellCell = BuySellCell;
        Ember.TableRow = TableRow;
        Ember.ButtonCell = ButtonCell;
        Ember.ClassicHeaderCell = ClassicHeaderCell;
    },

    onCheckDataAvailability: function () {
        return true;
    },

    loadCurrencyOptions: function () {
        var currOptions = [{Id: 0, DisplayName: this.get('currentPortfolio').curr}];

        this.set('currencyDropdownOptions', currOptions);
        this.set('currencySelection', this.get('currencyDropdownOptions')[0]);
    }.observes('currentPortfolio'),

    loadPortfolioOptions: function () {
        var portCollection = this.tradeService.accountDS.getTradingAccCollection();
        var currentPortfolio = this.get('currentPortfolio');
        var portfolios = [];
        var allLabel = this.get('app').lang.labels.all;

        if (portCollection.length > 1) {
            portfolios.pushObject(Ember.Object.create({secAccList: portCollection, [this.portfolioDes]: allLabel, tradingAccId: 'S1', isShowAll: true}));
        }

        portfolios.pushObjects(portCollection);

        if (Ember.$.isEmptyObject(currentPortfolio) && portfolios.length > 0) {
            this.set('currentPortfolio', portfolios[0]);
            this.updateContent();
            this._setShowBuyingPower();
        }

        this.set('portfolioDropdownOptions', portfolios);
        this._subscribePortfolioInfo();
    },

    _subscribePortfolioInfo: function () {
        var currentPortfolio = this.get('currentPortfolio');
        var that = this;

        if (currentPortfolio && currentPortfolio.tradingAccId) {
            if (currentPortfolio.isShowAll && currentPortfolio.secAccList) {
                Ember.$.each(currentPortfolio.secAccList, function (index, option) {
                    that.tradeService.accountDS.subscribeTradingAccountInfo(option);
                });
            } else {
                this.tradeService.accountDS.subscribeTradingAccountInfo(currentPortfolio);
            }
        }
    },

    _unSubscribePortfolioInfo: function () {
        var currentPortfolio = this.get('currentPortfolio');
        var that = this;

        if (currentPortfolio && currentPortfolio.tradingAccId) {
            if (currentPortfolio.isShowAll && currentPortfolio.secAccList) {
                Ember.$.each(currentPortfolio.secAccList, function (index, option) {
                    that.tradeService.accountDS.unSubscribeTradingAccountInfo(option);
                });
            } else {
                that.tradeService.accountDS.unSubscribeTradingAccountInfo(currentPortfolio);
            }
        }
    },

    onHoldingGainLossChange: function () {
        // This calculation would consume more cpu and triggered on every holding gainLoss change.
        if (this.get('currentPortfolio.tradingAccId') || this.get('currentPortfolio.isShowAll')) {
            Ember.run.debounce(this, this.setFooterArray, 200);
        }
    }.observes('content.@each.gainLoss'),

    setFooterArray: function () {
        var currentPortfolio = this.get('currentPortfolio');
        var costVal = 0;
        var mktVal = 0;
        var gainLoss = 0;
        var gainLossPer = 0;

        if (currentPortfolio.isShowAll) {
            Ember.$.each(currentPortfolio.secAccList, function (index, account) {
                costVal += account.get('costVal');
                mktVal += account.get('mktVal');
            });

            gainLoss = mktVal - costVal;

            if (costVal && costVal !== 0) {
                gainLossPer = gainLoss * 100 / costVal;
            }
        } else if (currentPortfolio.tradingAccId) {
            costVal = currentPortfolio.get('costVal');
            mktVal = currentPortfolio.get('mktVal');
            gainLoss = currentPortfolio.get('gainLoss');
            gainLossPer = currentPortfolio.get('gainLossPer');
        }

        var footerArray = this.get('footerArray');
        var footerObj = {
            costVal: costVal,
            mktVal: mktVal,
            gainLoss: gainLoss,
            gainLossPer: gainLossPer,
            firstValueStyle: 'hide'
        };

        if (footerArray.length > 0) {
            footerArray[0].setProperties(footerObj);
            Ember.set(footerArray[0], [this.totalValueProp], this.app.lang.labels.totalValue);
        } else {
            footerObj[this.totalValueProp] = this.app.lang.labels.totalValue;
            footerArray.pushObject(Ember.Object.create(footerObj));
        }

        if (mktVal > 0) {
            this.calcPortPcnt(mktVal);
        }
    },

    calcPortPcnt: function (mktVal) {
        var content = this.get('content');

        Ember.$.each(content, function (key, hold) {
            var holdVal = hold.get('mktVal');
            var portPer = holdVal * 100 / mktVal;
            hold.set('portPer', portPer);
        });
    },

    updateContent: function () {
        var holdings = [];
        var currentPortfolio = this.get('currentPortfolio');

        if (currentPortfolio.isShowAll) {
            holdings = this.tradeService.holdingDS.getHoldingCollection();
        } else {
            holdings = this.tradeService.holdingDS.getHoldingByAccount(currentPortfolio.tradingAccId);
        }

        this.set('content', holdings);
    },

    loadPortfolioContextItems: function () {
        var that = this;
        var config = this.orderTicketConfig;

        var contextItems = [
            {
                view: {
                    key: 'buy',
                    name: this.get('app').lang.labels.buy,
                    menuClass: 'up-fore-color',
                    iconClass: 'icon-arrow-circle-up up-fore-color',
                    shortcut: 'Alt + B'
                },
                config: {
                    callbackFunc: sharedService.getService('tradeUI').showPopupWidget,
                    controllerString: config.controllerString,
                    routeString: config.routeString,
                    viewName: config.viewName
                },
                args: {tabId: tradeConstants.OrderSide.Buy}
            },
            {
                view: {
                    key: 'liquidate',
                    name: this.get('app').lang.labels.sell,
                    menuClass: 'down-fore-color',
                    iconClass: 'icon-arrow-circle-down down-fore-color',
                    shortcut: 'Alt + S'
                },
                config: {
                    callbackFunc: function (liquidateConfig, args) {
                        that.liquidate(liquidateConfig, args);
                    },
                    controllerString: config.controllerString,
                    routeString: config.routeString,
                    viewName: config.viewName
                },
                args: {tabId: tradeConstants.OrderSide.Sell}
            }
        ];

        this.set('portfolioContextItems', contextItems);
    },

    liquidate: function (config, args) {
        var orderConfig = userSettings.orderConfig;
        var defaultOrderQty = (orderConfig && orderConfig.defaultOrderQty !== undefined) ? orderConfig.defaultOrderQty : tradeConstants.OrderDefaultQty;

        args.qty = defaultOrderQty;
        args.order = {secAccNum: this.get('clickedRowPortfolio')};
        sharedService.getService('tradeUI').showPopupWidget(config, args);
    },

    buyMore: function () {
        var sym = this.get('clickedRowSymbol');
        var exchg = this.get('clickedRowExchange');
        var inst = this.get('clickedRowInst');

        if (sym && exchg) {
            var args = {sym: sym, exg: exchg, inst: inst, tabId: tradeConstants.OrderSide.Buy, secAccNum: this.get('clickedRowPortfolio')};
            var config = this.orderTicketConfig;

            config.container = this.container;

            sharedService.getService('tradeUI').showPopupWidget(config, args);
        }
    },

    setDecimals: function () {
        var multiFactors = fieldMetaConfig.multiFactors;
        var exgArray = this.get('currentPortfolio') ? this.get('currentPortfolio').exgArray : '';
        var exchange = exgArray && exgArray.length ? exgArray[0] : '';
        var decimalPlaces = userSettings.displayFormat.decimalPlaces;

        if (exchange && multiFactors) {
            var exchangeFieldMeta = multiFactors[exchange];

            if (exchangeFieldMeta && exchangeFieldMeta.decimalPlaces) {
                decimalPlaces = exchangeFieldMeta.decimalPlaces;
            }
        }

        this.set('decimalPlaces', decimalPlaces);
    }.observes('currentPortfolio'),

    _generateFullContextMenu: function () {
        this.menuComponent.fullContextMenu.insertAt(0, this.portfolioContextItems);
    },

    _subscribeRights: function () {
        if (utils.AssetTypes.isRight(this.get('clickedRowInst'))) {
            var controllerString = 'controller:trade/widgets/rights-subscription';
            var routeString = 'trade/widgets/rights-subscription';
            var widgetController = this.container.lookupFactory(controllerString).create();

            widgetController.set('routeString', routeString);
            widgetController.set('currentPortfolio', this.get('currentPortfolio'));
            widgetController.set('rowData', this.get('rowData'));
            widgetController.set('title', this.get('app').lang.labels[widgetController.get('titleKey')]);

            var widgetPopupView = this.container.lookupFactory('view:widget-popup-view').create({
                dimensions: {
                    w: 280,
                    h: 237
                }
            });

            widgetPopupView.show(widgetController, function () {
                widgetController.initializeWidget({wn: controllerString.split('/').pop()});
            });
        }
    },

    _setShowBuyingPower: function () {
        var currentPortfolio = this.get('currentPortfolio');
        this.set('isShowBuyingPower', (currentPortfolio.cashAccount && currentPortfolio.cashAccount.buyPwr !== undefined));
    },

    actions: {
        sort: function (column) {
            if (!column.get('isSortSupported')) {
                return;
            }

            if (this.get('sortColumn') !== column) {
                this.get('columns').setEach('isSorted', false);
                column.set('isSorted', true);
                this.set('sortColumn', column);
                this.set('sortProperties', [column.get('sortKey')]);
                this.set('isSortApplied', true);
            } else if (this.get('sortColumn') === column) {
                // Handle disabling sorts
                if (this.get('sortAscending') === true) {
                    this.set('sortColumn', undefined);
                    this.set('sortAscending', false);
                    column.set('isSorted', false);
                    this.set('isSortApplied', false);
                    this.set('sortProperties', []);
                } else {
                    this.set('sortProperties', [column.get('sortKey')]);
                    this.toggleProperty('sortAscending');
                }
            }
        },

        setLink: function (option) {
            this.setWidgetLink(option);
        },

        onSelectPortfolio: function (item) {
            this._unSubscribePortfolioInfo();
            this.set('currentPortfolio', item);
            this.updateContent();
            this._setShowBuyingPower();
            this._subscribePortfolioInfo();
            this.refreshTableComponent();
        },

        onSelectCurrency: function (item) {
            this.set('currencySelection', item);
        },

        clickRow: function (selectedRow, event) {
            var rowData = selectedRow.getProperties('isRoot', 'exg', 'symbol', 'avaiQty', 'instruTyp', 'secAccNum', 'symbolInfo.sDes', 'subPrice');
            this.set('rowData', rowData);

            if (!rowData.isRoot) {
                this._super(event);
            }

            this.menuComponent = this.container.lookup('component:symbol-click-menu-popup');
            this.menuComponent.associatedController = this;

            if (!this.menuComponent) { // Create a symbol-click-menu-popup component object and call base-context-menu
                this.menuComponent = this.container.lookupFactory('component:symbol-click-menu-popup').create({associatedController: this});
            }

            if (rowData) {
                // Set symbol details on symbol-click-menu-popup
                var symbolObj = {sym: rowData.symbol, exg: rowData.exg, inst: rowData.instruTyp};
                this.menuComponent.set('selectedSymbol', symbolObj);

                this.menuComponent.initialize(this.get('wkey'), symbolObj);
            }

            this._generateFullContextMenu();

            // Render announcement-news-popup.hbs component to application.hbs
            var viewName = 'components/symbol-click-menu-popup';
            var modal = sharedService.getService('sharedUI').getService('modalPopupId');

            if (rowData && rowData.isRoot && event.button !== 2) {
                rowData = selectedRow.getProperties('groupKey', 'isShrunk');
                this.rearrangeGroupedRows(rowData.groupKey, !rowData.isShrunk);
            }

            if (rowData && rowData.symbol) {
                this.set('clickedRowSymbol', rowData.symbol);
                this.set('clickedRowExchange', rowData.exg);
                this.set('clickedRowAvailQty', rowData.avaiQty);
                this.set('clickedRowInst', rowData.instruTyp);
                this.set('clickedRowPortfolio', rowData.secAccNum);
            }

            if (event && event.button === 2) { // Add a constant to this
                this.menuComponent.showPopup(this.menuComponent, viewName, modal);
            } else {
                modal.send('closeModalPopup');
            }
        },

        liquidateHolding: function () {
            var sym = this.get('clickedRowSymbol');
            var exchg = this.get('clickedRowExchange');
            var avaiQty = this.get('clickedRowAvailQty');
            var inst = this.get('clickedRowInst');

            if (sym && exchg) {
                var args = {sym: sym, exg: exchg, inst: inst, qty: avaiQty, tabId: tradeConstants.OrderSide.Sell};
                var config = this.orderTicketConfig;
                config.container = this.container;

                this.liquidate(config, args);
            }
        },

        buyHolding: function () {
            this.buyMore();
        },

        subscribeRights: function () {
            this._subscribeRights();
        },

        fullScreenToggle: function () {
            this.toggleProperty('disableInnerWidgets');
            this.toggleFullScreen('portfolioContainer-' + this.get('wkey'), this.get('wkey'));
            this.toggleProperty('isRefreshed');
        }
    }
});
/* global c3 */
import Ember from 'ember';
import TradeBaseController from '../controllers/trade-base-controller';
import sharedService from '../../../models/shared/shared-service';
import PanelField from '../../../models/price/business-entities/panel-field';
import tradeWidgetConfig from '../../../config/trade-widget-config';
import userSettings from '../../../config/user-settings';
import responsiveHandler from '../../../helpers/responsive-handler';
import fieldMetaConfig from '../../../config/field-meta-config';
import appConfig from '../../../config/app-config';

export default TradeBaseController.extend({
    tradingAccColl: Ember.A(),
    cashAccountColl: Ember.A(),
    currentAccount: {},

    isShowTitle: false,
    isVerticalView: false,
    title: 'Account Summary',

    currencySelection: {},
    currencyDropdownOptions: [],

    portfolioChart: undefined,
    portfolioDes: sharedService.getService('trade').fieldConfigs.portfolioDes,
    tradeService: sharedService.getService('trade'),
    panelFields: undefined,

    accountChartHeight: '260px',
    columnClass: 'col-md-6',
    isShowChart: true,

    isTablet: function () {
        return appConfig.customisation.isTablet;
    }.property(),

    chartHeight: function () {
        if (!this.get('isShowSecondRow')) {
            return this.get('isShowSecondRow') ? 'account-summery-vertical-height' : 'account-summery-height';
        }

        return null;
    }.property(),

    initializeWidgetResponsive: function () {
        this.set('responsive', responsiveHandler.create({controller: this, widgetId: 'tradeOrderTab', callback: this.onResponsive, isContainer: true}));
        this.responsive.bindResize(); // For the container responsive behavior no need to call initialize function
    },

    onResponsive: function (responsiveArgs) {
        var responsiveLowLimit = 1; // Level which is map to 1024 x 800 resolution
        var controller = responsiveArgs.controller;
        var resAccChartHeight = controller.accountChartHeight;

        if (responsiveArgs.responsiveLevel >= responsiveLowLimit) {
            resAccChartHeight = '180px';
        }

        controller.set('accountChartHeight', resAccChartHeight);
        controller._drawAccountsChart();
    },

    onLoadWidget: function () {
        this.tradeService.subscribeTradeMetaReady(this, this.get('wkey'));
        this.tradeService.subscribeTradeDisconnect(this, this.get('wkey'));

        this.loadCurrencyOption();
        this.setDecimalPlaces();

        if (this.get('isVerticalView')) {
            this._setVerticalView();
        } else if (this.get('isTablet')) {
            this._setTabletView();
        } else {
            this.initializeWidgetResponsive();
        }
    },

    onPrepareData: function () {
        this.loadPortfolioOptions();
        this.initializeAccountSummary();

        if (this.get('isTablet')) {
            this._setScrollHeight();
        }
    },

    onAfterRender: function () {
        var that = this;

        if (this.get('isTablet')) {
            this.setContentHeight();
            Ember.run.later(this, this._setScrollHeight, 10);
        }

        setTimeout(function () {
            that._drawAccountsChart();
        }, 50);

        this.generateScrollBar(undefined, 4000);
    },

    setContentHeight: function () {
        var container = Ember.$('div#' + 'accountSummary-' + this.get('wkey'));

        if (container) {
            var containerHeight = container.height();
            this.set('containerHeight', containerHeight);
        }
    },

    setDecimalPlaces: function () {
        var dec = userSettings.displayFormat.decimalPlaces;
        var decExchange = sharedService.getService('price').exchangeDS.getExchange(this.get('exg')).dep;
        var decimalPlaces = decExchange ? decExchange : dec;

        var multiFactors = fieldMetaConfig.multiFactors;
        var exgArray = this.get('currentAccount') ? this.get('currentAccount').exgArray : '';
        var exchange = exgArray && exgArray.length ? exgArray[0] : '';

        if (exchange && multiFactors) {
            var exchangeFieldMeta = multiFactors[exchange];

            if (exchangeFieldMeta && exchangeFieldMeta.decimalPlaces) {
                decimalPlaces = exchangeFieldMeta.decimalPlaces;
            }
        }

        this.set('decimalPlace', decimalPlaces);
    }.observes('currentAccount'),

    initializeAccountSummary: function () {
        if (tradeWidgetConfig.accountSummary.accountSummaryFields) {
            var that = this;
            var fields = [];
            var noOfDecimals = this.get('decimalPlace');

            Ember.$.each(tradeWidgetConfig.accountSummary.accountSummaryFields, function (key, value) {
                var fieldObj = PanelField.create({
                    valueObj: that.get('currentAccount'),
                    fieldObj: value,
                    noOfDecimals: noOfDecimals
                });

                fields.pushObject(fieldObj);
            });

            this.set('panelFields', fields);
        }
    },

    onClearData: function () {
        var panelFields = this.get('panelFields');

        if (panelFields) {
            Ember.$.each(panelFields, function (key, panelField) {
                if (!panelField.isDestroyed) {
                    panelField.destroy();
                }
            });
        }

        this.set('tradingAccColl', []);
        this.set('cashAccountColl', []);
        this.set('currentAccount', {});
        this.set('currencyDropdownOptions', []);
        this.set('currencySelection', {});
        this.set('panelFields', undefined);
    },

    onUnloadWidget: function () {
        this.tradeService.unSubscribeTradeMetaReady(this, this.get('wkey'));
        this.tradeService.unSubscribeTradeDisconnect(this, this.get('wkey'));
    },

    onTradeMetaReady: function () {
        this.onPrepareData();
        this.onAddSubscription();
    },

    loadPortfolioOptions: function () {
        var that = this;
        var accColl = this.tradeService.accountDS.getTradingAccCollection();

        this.set('tradingAccColl', accColl);

        if (accColl && accColl.length > 0) {
            this.set('currentAccount', accColl[0]);

            Ember.$.each(accColl, function (key, acc) {
                that.tradeService.sendBuyingPowerRequest(acc);
                that.cashAccountColl.pushObject(acc.cashAccount);
            });

            Ember.run.next(this, function () {
                that._updateFieldObject();
                that.updatePortfolioChart();
            });
        }
    },

    _setVerticalView: function () {
        this.set('isShowChart', false);
        this.set('isShowSecondRow', true);
        this.set('columnClass', 'col-md-12');
    },

    _setTabletView: function () {
        this.set('isShowChart', false);
        this.set('isShowSecondRow', true);
        this.set('columnClass', 'col-md-12');
    },

    _drawAccountsChart: function () {
        var width = Ember.$('#c3-accounts-chart').width();
        var height = Ember.$('#c3-accounts-chart').height();
        var bottomPadding = this.get('isTablet') ? 0 : 35;

        var portCahrt = c3.generate({
            data: {
                columns: [],

                type: 'pie'

                // Can use onclick, onmouseover, onmouseout functions if required
            },

            pie: {
                title: 'Portfolio Breakdown',
                width: 100,
                label: {
                    show: true
                }
            },

            legend: {
                show: false
            },

            tooltip: {
                show: true
            },

            bindto: '#c3-accounts-chart',

            size: {
                width: width,
                height: height
            },

            padding: {
                bottom: bottomPadding
            }
        });

        this.set('portfolioChart', portCahrt);
        this.updatePortfolioChart();
    },

    updatePortfolioData: function () {
        Ember.run.once(this, this.updatePortfolioChart);
    }.observes('cashAccountColl.@each.buyPwr'),

    updatePortfolioChart: function () {
        if (!this.get('isTablet')) {
            var accColl = this.get('tradingAccColl');
            var chart = this.get('portfolioChart');
            var chartData = [];

            if (!chart || !chart.load || !accColl || accColl.length <= 0) {
                return;
            }

            Ember.$.each(accColl, function (key, acc) {
                chartData.pushObject([acc.tradingAccId, acc.get('cashAccount.buyPwr')]);
            });

            chart.load({
                columns: chartData
            });

        } else {
            this._setBarChartData();
        }
    },

    loadCurrencyOption: function () {
        var currency = this.get('currentAccount.cashAccount.curr') ? this.get('currentAccount.cashAccount.curr') : userSettings.customisation.defaultCurrency;
        var currOptions = [{id: 0, displayName: currency}];

        this.set('currencyDropdownOptions', currOptions);
        this.set('currencySelection', this.get('currencyDropdownOptions')[0]);
    }.observes('currentAccount'),

    _setBarChartData: function () {
        var that = this;
        var portCollection = this.get('tradingAccColl');
        var portfolioBarChartData = [];
        var totalBuyPwr = 0;

        if (portCollection && portCollection.length > 1) {
            this.set('isShowBarChart', true);

            Ember.$.each(portCollection, function (key, acc) {
                totalBuyPwr += acc.get('cashAccount.buyPwr');
            });

            if (totalBuyPwr) {
                Ember.$.each(portCollection, function (index, acc) {
                    var percentage = Math.round((acc.get('cashAccount.buyPwr') / totalBuyPwr) * 1000) / 10;
                    var dataRow = {
                        portNme: acc.tradingAccId,
                        buyPwr: acc.get('cashAccount.buyPwr'),
                        percentage: (percentage === 0) ? 0 : percentage + '%',
                        width: (percentage === 0) ? '0.1%' : percentage + '%',
                        color: that._getBarColor(index)
                    };
                    portfolioBarChartData.pushObject(dataRow);
                });
            }

            this.set('portfolioBarChartData', portfolioBarChartData);
        } else {
            this.set('isShowBarChart', false);
        }
    },

    _getBarColor: function (index) {
        var colorArray = ['yellow-back-color', 'purple-back-color', 'green-back-color', 'blue-back-color-2', 'pink-back-color', 'orange-back-color'];
        return colorArray[index % colorArray.length];
    },

    _setScrollHeight: function () {
        this.setContentHeight();
        var containerHeight = this.get('containerHeight');

        if (containerHeight) {
            var topRowHeight = 110;
            var contentScrollHeight;

            if (this.get('tradingAccColl') && this.get('tradingAccColl').length > 1) {
                var chartHeight = this.get('tradingAccColl').length * 20;
                contentScrollHeight = containerHeight - (topRowHeight + chartHeight);
            } else {
                contentScrollHeight = containerHeight - (topRowHeight);
            }

            this.set('contentScrollHeight', contentScrollHeight + 'px');
            this._updateFieldObject();
        }
    },

    _updateFieldObject: function () {
        var that = this;
        var panelField = this.get('panelFields');

        if (panelField) {
            Ember.$.each(panelField, function (key, field) {
                Ember.set(field, 'valueObj', that.get('currentAccount.cashAccount'));
                Ember.set(field, 'noOfDecimals', that.get('decimalPlace'));
            });
        }
    },

    setPortfolio: function (item) {
        if (item) {
            this.set('currentAccount', item);
            this._updateFieldObject();
        }
    },

    actions: {
        portfolioChanged: function (item) {
            this.setPortfolio(item);
        },

        currencyChanged: function (item) {
            this.set('currencySelection', item);
        },

        fullScreenToggle: function () {
            this.toggleProperty('disableInnerWidgets');
            this.toggleFullScreen('accountSummary-' + this.get('wkey'), this.get('wkey'));
        }
    }
});
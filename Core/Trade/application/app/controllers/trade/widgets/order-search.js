import Ember from 'ember';
import TradeTableController from '../controllers/trade-table-controller';
import tradeWidgetConfig from '../../../config/trade-widget-config';
import tradeConstants from '../../../models/trade/trade-constants';
import sharedService from '../../../models/shared/shared-service';
import userSettings from '../../../config/user-settings';
import appConfig from '../../../config/app-config';

// Cell Views
import ClassicHeaderCell from '../../../views/table/classic-header-cell';
import ClassicCell from '../../../views/table/classic-cell';
import ClassicMappingCell from '../../../views/table/trade/classic-mapping-cell';
import ClassicStatusCell from '../../../views/table/trade/classic-status-cell';
import OrderPriceCell from '../../../views/table/trade/order-price-cell';
import responsiveHandler from '../../../helpers/responsive-handler';

export default TradeTableController.extend({
    ODRowHeight: 24,
    ODHeaderHeight: 35,
    ODRejectReasonHeight: 21,

    DefaultSearchDays: 7,
    DefaultPageSize: 10,
    DefaultStartSeq: 1,
    isShowTitle: false,
    isRenderingEnabled: false,
    sortProperties: ['clOrdId'],
    fixedColumns: tradeWidgetConfig.orderSearch.tableParams.numOfFixedColumns,
    tradeService: sharedService.getService('trade'),
    portfolioDes: sharedService.getService('trade').fieldConfigs.portfolioDes,

    portfolios: Ember.A(),
    sides: Ember.A(),
    status: Ember.A(),

    currentPortfolio: {},
    currentSide: {},
    currentStatus: {},

    toEndDate: '',
    fromDate: '',
    toDate: '',
    orderId: '',
    symbol: '',
    lan: '',
    datePickerFormat: userSettings.displayFormat.dateFormat.toLowerCase(),

    searchResult: {},

    isTablet: appConfig.customisation.isTablet,

    defaultColumnIds: tradeWidgetConfig.orderSearch.defaultColumnIds,

    isPrevDisabled: function () {
        var startSeq = this.get('searchResult').startSeq;
        return !startSeq || startSeq <= this.DefaultStartSeq;
    }.property('searchResult.startSeq'),

    isNextDisabled: function () {
        return this.get('searchResult').isNxtPagAvail !== 1;
    }.property('searchResult.isNxtPagAvail'),

    onLoadWidget: function () {
        var wkey = this.get('wkey');

        this.tradeService.subscribeTradeMetaReady(this, wkey);
        this.tradeService.subscribeTradeDisconnect(this, wkey);

        this.setCellViewsScopeToGlobal();
        this.set('title', this.get('app').lang.labels.orderSearch);
        this.set('isShowTitle', !this.get('hideTitle'));

        this.set('defaultColumnMapping', tradeWidgetConfig.orderSearch.defaultColumnMapping);
        this.set('moreColumnsIds', tradeWidgetConfig.orderSearch.moreColumnIds);
        this.set('nonRemovableColumnIds', tradeWidgetConfig.orderSearch.nonRemovableColumnIds);
        this.set('enableColumnReorder', !this.isTablet);

        this.setDefaultColumns();
        this.setDefaultColumnDeclarations();

        this.setLangLayoutSettings(sharedService.userSettings.currentLanguage);

        var today = new Date();
        this.set('toEndDate', today);
        this.set('toDate', today);

        this.set('symbolFieldId', ['symbolFieldId', wkey].join('-'));
        this.set('orderFieldId', ['orderFieldId', wkey].join('-'));

        // Get from date using default search days
        var fromDate = new Date();
        fromDate.setDate(fromDate.getDate() - this.DefaultSearchDays);
        this.set('fromDate', fromDate);
        this.set('lan', sharedService.userSettings.currentLanguage.toLowerCase());
    },

    onPrepareData: function () {
        this.loadContent();

        var searchResult = this.tradeService.orderSearchDS.getWidgetSearchResult(this.get('wkey'));
        this.set('searchResult', searchResult);
        this._setContent();
    },

    onAddSubscription: function () {
        if (this.tradeService.isTradeMetaReady) {
            this._sendSearchRequest(this.DefaultStartSeq);
        }
    },

    onClearData: function () {
        this.set('content', Ember.A());
        this.set('masterContent', Ember.A());
        this.set('portfolios', Ember.A());
        this.set('currentPortfolio', {});
        this.set('currentSide', {});
        this.set('currentStatus', {});
        this.set('searchResult', {});

        this.tradeService.orderSearchDS.clearWidgetSearchResult(this.get('wkey'));
    },

    onUnloadWidget: function () {
        var wkey = this.get('wkey');
        this.set('columnDeclarations', []);

        this.tradeService.unSubscribeTradeMetaReady(wkey);
        this.tradeService.unSubscribeTradeDisconnect(wkey);
    },

    onLanguageChanged: function () {
        this.loadContent();
        this.setErrorMessage();
        this.setDefaultColumns();
        this.setDefaultColumnDeclarations();
        this.toggleProperty('isRefreshed');

        var lang = sharedService.userSettings.currentLanguage;
        this.set('lan', lang.toLowerCase());

        Ember.run.next(this, this.setLangLayoutSettings, lang);
    },

    onCheckDataAvailability: function () {
        return this.get('content').length !== 0;
    },

    loadContent: function () {
        this.loadPortfolioOptions();
        this.loadSideDropdown();
        this.loadStatusDropdown();
    },

    onTradeMetaReady: function () {
        this.onPrepareData();
        this.onAddSubscription();
    },

    onAfterRender: function () {
        var that = this;

        Ember.run.next(this, function () {
            that.set('isRenderingEnabled', true);
        });
    },

    initializeResponsive: function () {
        this.set('responsive', responsiveHandler.create({controller: this, widgetId: 'orderSearch-' + this.get('wkey'), callback: this.onResponsive}));

        this.responsive.addList('orderSearch-middle', [
            {id: 'orderSearchMoreColumns', width: 5}
        ]);

        this.responsive.initialize();
    },

    loadPortfolioOptions: function () {
        var portCollection = this.tradeService.accountDS.getTradingAccCollection();
        var currentPortfolio = this.get('currentPortfolio');
        var portfolios = [];

        if (portCollection.length > 1) {
            var secAccList = [];

            Ember.$.each(portCollection, function (index, option) {
                secAccList.pushObject(option.tradingAccId);
            });

            var allObj = Ember.create({secAccList: secAccList.join(this.utils.Constants.StringConst.Comma), isShowAll: true});
            var allLabel = [this.get('app').lang.labels.portfolio, this.utils.Constants.StringConst.Colon,
                this.utils.Constants.StringConst.Space, this.get('app').lang.labels.all].join(this.utils.Constants.StringConst.Empty);

            Ember.set(allObj, this.portfolioDes, allLabel);
            portfolios.pushObject(allObj);
        }

        portfolios.pushObjects(portCollection);

        if (!currentPortfolio || !currentPortfolio.tradingAccId) {
            this.set('currentPortfolio', portfolios.length > 0 ? portfolios[0] : {});
        }

        this.set('portfolios', portfolios);
    },

    setCellViewsScopeToGlobal: function () {
        Ember.ClassicHeaderCell = ClassicHeaderCell;
        Ember.ClassicCell = ClassicCell;
        Ember.ClassicMappingCell = ClassicMappingCell;
        Ember.ClassicStatusCell = ClassicStatusCell;
        Ember.OrderPriceCell = OrderPriceCell;
    },

    cellViewsForColumns: {
        classicMappingCell: 'Ember.ClassicMappingCell',
        classicStatusCell: 'Ember.ClassicStatusCell',
        classicCell: 'Ember.ClassicCell',
        orderPriceCell: 'Ember.OrderPriceCell'
    },

    loadSideDropdown: function () {
        var that = this;
        var exg = this.tradeService.userDS.get('userExchg')[0];
        var orderSides = this.tradeService.tradeMetaDS.getOrderSideCollectionByExchange(exg);

        if (orderSides) {
            Ember.$.each(orderSides, function (index, option) {
                Ember.set(option, 'DisplayName', that.app.lang.labels[[that.tradeService.tradeMetaDS.metaMapping.orderSide, option.code].join('_')]);
            });
        }

        var sides = [];
        var currentSide = this.get('currentSide');
        var optionAll = {DisplayName: [this.get('app').lang.labels.side, this.utils.Constants.StringConst.Colon,
            this.utils.Constants.StringConst.Space, this.get('app').lang.labels.all].join(this.utils.Constants.StringConst.Empty), code: ''};
        sides.pushObject(optionAll);
        sides.pushObjects(orderSides);

        // TODO: [satheeqh] Set language wise description
        if (!currentSide || !currentSide.secAccNum) {
            this.set('currentSide', optionAll);
        }

        this.set('sides', sides);
    },

    loadStatusDropdown: function () {
        var that = this;
        var currentStatus = this.get('currentStatus');
        var optionAll = {DisplayName: [this.get('app').lang.labels.status, this.utils.Constants.StringConst.Colon,
            this.utils.Constants.StringConst.Space, this.get('app').lang.labels.all].join(this.utils.Constants.StringConst.Empty), value: ''};

        var selectedStatus = [
            tradeConstants.OrderStatusMapping['0'],
            tradeConstants.OrderStatusMapping['1'],
            tradeConstants.OrderStatusMapping['2'],
            tradeConstants.OrderStatusMapping['5'],
            tradeConstants.OrderStatusMapping['4'],
            tradeConstants.OrderStatusMapping['8'],
            tradeConstants.OrderStatusMapping.t];

        var options = [optionAll];

        if (selectedStatus) {
            Ember.$.each(selectedStatus, function (index, option) {
                Ember.set(option, 'DisplayName', that.app.lang.labels[[tradeConstants.ordStatusLabel, option.value].join('_')]);
                options.pushObject(option);
            });
        }

        if (!currentStatus || !currentStatus.value) {
            this.set('currentStatus', optionAll);
        }

        this.set('status', options);
    },

    popUpOrderDetails: function (clOrdId) {
        var tradeUIService = sharedService.getService('tradeUI');
        var order = this.tradeService.orderSearchDS.getOrder(clOrdId, this.get('wkey'));
        var numberOfRows = Math.ceil(tradeWidgetConfig.orderDetails.orderDetailsPopup.length / 3);
        var popupWindowHeight = (numberOfRows * this.ODRowHeight) + this.ODHeaderHeight;
        var popupHeight = order.ordSts === tradeConstants.OrderStatus.Rejected ? popupWindowHeight + this.ODRejectReasonHeight : popupWindowHeight;

        var controllerString = 'controller:trade/widgets/order-details';
        var routeString = 'trade/widgets/order-details';
        var viewName = 'view:widget-popup-view';

        tradeUIService.showPopupWidget({container: this.container, controllerString: controllerString, routeString: routeString, viewName: viewName}, {orderInfo: order, dimensions: {w: 685, h: popupHeight}});
    },

    _sendSearchRequest: function (startSeq) {
        var fromDate = this.get('fromDate');
        fromDate = this.utils.validators.isAvailable(fromDate) ? this.utils.formatters.convertToDisplayTimeFormat(fromDate, 'YYYYMMDD') : '';

        var toDate = this.get('toDate');
        toDate = this.utils.validators.isAvailable(toDate) ? this.utils.formatters.convertToDisplayTimeFormat(toDate, 'YYYYMMDD') : '';

        var currentPortfolio = this.get('currentPortfolio');

        var reqObj = {
            secAccNum: currentPortfolio.isShowAll ? currentPortfolio.secAccList : currentPortfolio.tradingAccId,
            strDte: toDate,
            endDte: fromDate,
            ordSide: this.get('currentSide').code ? this.get('currentSide').code : '',
            ordSts: this.get('currentStatus').value ? this.get('currentStatus').value : '',
            symbol: this.get('symbol'),
            clOrdId: this.get('orderId'),
            startSeq: startSeq,
            totalNoRec: this.DefaultPageSize,
            unqReqId: this.get('wkey')
        };

        this._setContent();
        this.tradeService.sendOrderSearchRequest(reqObj);
    },

    _setContent: function () {
        var searchResult = this.get('searchResult');
        Ember.set(searchResult, 'ordLst', []);
        Ember.set(searchResult, 'orderMap', {});
        this.set('content', searchResult.ordLst);
        this.set('masterContent', searchResult.ordLst);

        this.setRequestTimeout(tradeConstants.TimeIntervals.LoadingInterval, 'content.length');
        this.setErrorMessage();
    },

    actions: {
        sort: function (column) {
            if (!column.get('isSortSupported')) {
                return;
            }

            if (this.get('sortColumn') !== column) {
                this.get('columns').setEach('isSorted', false);
                column.set('isSorted', true);
                this.set('sortColumn', column);
                this.set('sortProperties', [column.get('sortKey')]);
                this.set('isSortApplied', true);
            } else if (this.get('sortColumn') === column) {
                // Handle disabling sorts
                if (this.get('sortAscending') === true) {
                    this.set('sortColumn', undefined);
                    this.set('sortAscending', false);
                    column.set('isSorted', false);
                    this.set('isSortApplied', false);
                    this.set('sortProperties', []);
                } else {
                    this.set('sortProperties', [column.get('sortKey')]);
                    this.toggleProperty('sortAscending');
                }
            }
        },

        onSearchOrder: function () {
            this._sendSearchRequest(this.DefaultStartSeq);
        },

        doubleClickRow: function (selectedRow) {
            var rowData = selectedRow.getProperties('clOrdId');

            if (this.utils.validators.isAvailable(rowData.clOrdId)) {
                this.popUpOrderDetails(rowData.clOrdId);
            }
        },

        setPortfolio: function (option) {
            this.set('currentPortfolio', option);
        },

        setSide: function (option) {
            this.set('currentSide', option);
        },

        setStatus: function (option) {
            this.set('currentStatus', option);
        },

        onClickPrevious: function () {
            var seq = this.get('searchResult').startSeq - this.DefaultPageSize;
            seq = seq > 0 ? seq : this.DefaultStartSeq;
            this._sendSearchRequest(seq);
        },

        onClickNext: function () {
            var seq = this.get('searchResult').startSeq + this.DefaultPageSize;
            this._sendSearchRequest(seq);
        },

        fullScreenToggle: function () {
            this.toggleProperty('disableInnerWidgets');
            this.toggleFullScreen('orderSearch-' + this.get('wkey'), this.get('wkey'));
            this.toggleProperty('isRefreshed');
        }
    }
});
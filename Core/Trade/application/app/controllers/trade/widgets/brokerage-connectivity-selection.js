import Ember from 'ember';
import sharedService from '../../../models/shared/shared-service';
import languageDataStore from '../../../models/shared/language/language-data-store';

export default (function () {
    var priceService;
    var tradeService;
    var dropDownConfig;
    var selectedBrokerage;
    var brokeragesMap;
    var brokeragesArray;

    var prepareView = function () {
        _bindEvents();

        Ember.$('div#divBrokerage').hide();

        priceService = sharedService.getService('price');
        tradeService = sharedService.getService('trade');

        _sendRequest();
        _initializeBrokeragesMap();
        _populateBrokerageDropDown();
        _setDisplayTexts();
    };

    var _sendRequest = function () {
        tradeService.sendBrokerageConnectivityRequest(function (responseObj) {
            _processResponse(responseObj);
        });
    };

    var _initializeBrokeragesMap = function () {
        var brokerages = sharedService.userSettings.get('localBrokeragesMap');

        if (!brokeragesMap) {
            brokeragesMap = brokerages ? brokerages : {};
        }
    };

    var _processResponse = function (responseObj) {
        brokeragesMap = responseObj;

        _saveCollectionToLocalStorage();
        _populateBrokerageDropDown();
    };

    var _populateBrokerageDropDown = function () {
        var currentLangObj = languageDataStore.getLanguageObj().lang;
        brokeragesArray = [];

        Ember.$.each(brokeragesMap, function (key, broker) {
            brokeragesArray.push({text: broker.name, value: broker.id});
        });

        if (brokeragesArray.length > 0) {
            _setCurrentBrokerage();
            _setDropDownTitle();
        }

        dropDownConfig = {
            title: currentLangObj.labels.brokerageSelection,
            items: brokeragesArray,
            selectedValue: '',
            doneButtonLabel: currentLangObj.labels.done,
            cancelButtonLabel: currentLangObj.labels.cancel
        };
    };

    var _setDisplayTexts = function () {
        var currentLangObj = languageDataStore.getLanguageObj().lang;
        Ember.$('button#btnBrokerage').text(currentLangObj.labels.select);
    };

    var _setDropDownTitle = function () {
        if (selectedBrokerage) {
            Ember.$('div#btnBrokerageSelectionTitle').text(selectedBrokerage.name);
        }
    };

    var _setCurrentBrokerage = function () {
        var currentBrokerage = _getSavedBrokerage();

        if (currentBrokerage) {
            selectedBrokerage = currentBrokerage;
        } else {
            selectedBrokerage = brokeragesMap[brokeragesArray[0].value];
        }
    };

    var _setConnectionSettings = function () {
        var priceConnectionSettings = {
            ip: selectedBrokerage.Price.Push.Ip,
            port: selectedBrokerage.Price.Push.Port,
            secure: selectedBrokerage.Price.Push.Secure
        };

        priceService.webSocketManager.setConnectionSettings(priceConnectionSettings);

        var tradeConnectionSettings = {
            ip: selectedBrokerage.Trade.Push.Ip,
            port: selectedBrokerage.Trade.Push.Port,
            secure: selectedBrokerage.Trade.Push.Secure
        };

        tradeService.webSocketManager.setConnectionSettings(tradeConnectionSettings);
    };

    var _saveCurrentBrokerage = function () {
        var currentBrokerage = _getSavedBrokerage();

        if (!currentBrokerage || currentBrokerage !== selectedBrokerage) {
            _saveSelectedBrokerage(selectedBrokerage);
        }
    };

    var _saveSelectedBrokerage = function (currentBrokerage) {
        sharedService.userSettings.set('currentBrokerage', currentBrokerage.id);
        sharedService.userSettings.save();
    };

    var _bindEvents = function () {
        Ember.$('button#btnBrokerage').bind('click', function () {
            if (!selectedBrokerage || !selectedBrokerage.id) {
                return false;
            }

            _setConnectionSettings();
            _saveCurrentBrokerage();

            Ember.$('div#divBrokerage').hide();
            Ember.$('div#divLogin').show();
        });

        Ember.$('button#btnBrokerageSelection').bind('click', function () {
            if (brokeragesArray.length > 0) {
                if (selectedBrokerage) {
                    dropDownConfig.selectedValue = selectedBrokerage.id;
                }

                window.plugins.listpicker.showPicker(dropDownConfig,
                    function (item) {
                        if (brokeragesMap[item]) {
                            selectedBrokerage = brokeragesMap[item];
                            _setDropDownTitle();
                        }
                    }
                );
            }
        });
    };

    var _saveCollectionToLocalStorage = function () {
        sharedService.userSettings.set('localBrokeragesMap', brokeragesMap);
        sharedService.userSettings.save();
    };

    var _getSavedBrokerage = function () {
        return brokeragesMap[sharedService.userSettings.get('currentBrokerage')];
    };

    return {
        prepareView: prepareView
    };
})();
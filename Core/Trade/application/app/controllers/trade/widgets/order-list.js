/* global Mousetrap */

import Ember from 'ember';
import TradeTableController from '../controllers/trade-table-controller';
import tradeWidgetConfig from '../../../config/trade-widget-config';
import tradeConstants from '../../../models/trade/trade-constants';
import appConfig from '../../../config/app-config';
import sharedService from '../../../models/shared/shared-service';

// Cell Views
import ClassicHeaderCell from '../../../views/table/classic-header-cell';
import ButtonCell from '../../../views/table/button-cell';
import ClassicCell from '../../../views/table/classic-cell';
import ClassicMappingCell from '../../../views/table/trade/classic-mapping-cell';
import ClassicStatusCell from '../../../views/table/trade/classic-status-cell';
import OrderPriceCell from '../../../views/table/trade/order-price-cell';
import responsiveHandler from '../../../helpers/responsive-handler';

export default TradeTableController.extend({
    ODRowHeight: 24,
    ODHeaderHeight: 35,
    ODRejectReasonHeight: 21,

    exchange: sharedService.getService('price').exchangeDS.getExchange(sharedService.getService('price').userDS.currentExchange),
    isShowTitle: false,
    isRenderingEnabled: false,
    defaultGroupColumn: 'symbol',
    groupedContent: {},
    filteredContent: [],
    group: {},
    sortProperties: ['clOrdId'],
    fixedColumns: tradeWidgetConfig.orderList.tableParams.numOfFixedColumns,
    tradeService: sharedService.getService('trade'),
    portfolioDes: sharedService.getService('trade').fieldConfigs.portfolioDes,

    // Pop up Widget Parameters
    clickedRowOrderNo: null,
    clickedRowSymbol: null,
    clickedRowExchange: null,
    clickedRowInst: null,

    // Current filter values
    currentAccount: {},
    currentSide: {},
    currentStatus: {},
    currentGroup: {},

    // TODO [Arosha] Check getting dropdown values dynamically
    accountColl: Ember.A(),
    sides: Ember.A(),
    status: Ember.A(),
    groups: Ember.A(),
    groupOptions: [{code: 0, des: '', langKey: 'none'}, {code: 'symbol', des: '', langKey: 'symbol', dispProp: 'dispProp1'}, {code: 'ordSts', des: '', langKey: 'status', labelKey: 'orderStatus'}],

    orderListContextItems: [],
    orderTicketConfig: {controllerString: 'controller:trade/widgets/order-ticket/order-ticket-portrait',
        routeString: 'trade/widgets/order-ticket/order-ticket-portrait',
        viewName: 'view:widget-popup-view'},

    amendVisibilityStyle: 'disable-style ',
    cancelVisibilityStyle: 'disable-style ',

    isTablet: appConfig.customisation.isTablet,

    defaultColumnIds: tradeWidgetConfig.orderList.defaultColumnIds,

    onLoadWidget: function () {
        var that = this;

        sharedService.getService('trade').subscribeTradeMetaReady(this, this.get('wkey'));
        sharedService.getService('trade').subscribeTradeDisconnect(this, this.get('wkey'));

        this.setCellViewsScopeToGlobal();
        this.set('title', this.get('app').lang.labels.orderList);
        this.set('isShowTitle', !this.get('hideTitle'));

        this.set('defaultColumnMapping', tradeWidgetConfig.orderList.defaultColumnMapping);
        this.set('moreColumnsIds', tradeWidgetConfig.orderList.moreColumnIds);
        this.set('nonRemovableColumnIds', tradeWidgetConfig.orderList.nonRemovableColumnIds);
        this.set('enableColumnReorder', !this.isTablet);

        this.setDefaultColumns();
        this.setDefaultColumnDeclarations();

        this.setLangLayoutSettings(sharedService.userSettings.currentLanguage);
        var config = this.orderTicketConfig;
        config.container = this.container;

        if (!this.isTablet) { // Keyboard functions
            Mousetrap.bind('alt+a', function () {
                var args = {
                    sym: that.get('clickedRowSymbol'),
                    exg: that.get('clickedRowExchange'),
                    inst: that.get('clickedRowInst')
                };

                that.amendOrder(config, args);

                return false;
            });

            Mousetrap.bind('alt+x', function () {
                var args = {
                    sym: that.get('clickedRowSymbol'),
                    exg: that.get('clickedRowExchange'),
                    inst: that.get('clickedRowInst')
                };

                that.cancelOrder(config, args);

                return false;
            });
        }
    },

    onPrepareData: function () {
        this.setErrorMessage();
        this.loadContent();
    },

    onAddSubscription: function () {
        this.sendTradeRequests();
    },

    onAfterRender: function () {
        var that = this;

        Ember.run.next(this, function () {
            that.set('isRenderingEnabled', true);
        });
    },

    initializeResponsive: function () {
        this.set('responsive', responsiveHandler.create({controller: this, widgetId: 'orderList-' + this.get('wkey'), callback: this.onResponsive}));

        this.responsive.addList('orderList-middle', [
            {id: 'orderListMoreColumns', width: 5}
        ]);

        this.responsive.initialize();
    },

    onRemoveSubscription: function () {
        this.tradeService.sendPushRequest(tradeConstants.AiolosProtocol.MsgType.Push.OrderUnSub);
    },

    onClearData: function () {
        this.set('content', Ember.A());
        this.set('masterContent', Ember.A());
        this.set('filteredContent', Ember.A());
        this.set('accountColl', Ember.A());
        this.set('currentAccount', {});
        this.set('currentSide', {});
        this.set('currentStatus', {});
        this.set('currentGroup', {});
        this.set('clickedRowOrderNo', {});
        this.set('clickedRowSymbol', {});
        this.set('clickedRowExchange', {});
        this.set('clickedRowInst', {});
        this.set('orderListContextItems', []);
    },

    onUnloadWidget: function () {
        var wkey = this.get('wkey');
        this.set('columnDeclarations', []);

        this.tradeService.unSubscribeTradeMetaReady(wkey);
        this.tradeService.unSubscribeTradeDisconnect(wkey);

        Mousetrap.unbind('alt+x', wkey);
        Mousetrap.unbind('alt+a', wkey);
    },

    onLanguageChanged: function () {
        this.set('title', this.get('app').lang.labels.orderList);

        this.loadDropDowns();
        this.setErrorMessage();
        this.setDefaultColumns();
        this.setDefaultColumnDeclarations();
        this.toggleProperty('isRefreshed');

        Ember.run.next(this, this.setLangLayoutSettings, sharedService.userSettings.currentLanguage);
    },

    onTradeMetaReady: function () {
        this.onPrepareData();
        this.onAddSubscription();
    },

    sendTradeRequests: function () {
        var accountColl = this.get('accountColl');
        var that = this;

        if (accountColl && accountColl.length > 0) {
            Ember.$.each(accountColl, function (key, port) {
                if (!port.isShowAll) {
                    that.tradeService.sendOrderListRequest(port.tradingAccId, '1');
                }
            });
        }

        this.tradeService.sendPushRequest(tradeConstants.AiolosProtocol.MsgType.Push.OrderSub);
    },

    loadContent: function () {
        this.loadDropDowns();
        this.setRequestTimeout(tradeConstants.TimeIntervals.LoadingInterval, 'content.length');

        var orders = this.tradeService.orderDS.getOrderCollection();

        this.set('content', orders);
        this.set('masterContent', orders);
        this.set('filteredContent', orders);
    },

    onCheckDataAvailability: function () {
        return this.get('content').length !== 0;
    },

    loadPortfolioOptions: function () {
        var portCollection = this.tradeService.accountDS.getTradingAccCollection();
        var currentAccount = this.get('currentAccount');
        var accountColl = [];
        var allLabel = this.get('app').lang.labels.all;

        if (portCollection.length > 1) {
            accountColl.pushObject(Ember.Object.create({secAccList: portCollection, [this.portfolioDes]: allLabel, tradingAccId: 'S1', isShowAll: true}));
        }

        accountColl.pushObjects(portCollection);

        if (Ember.$.isEmptyObject(currentAccount)) {
            this.set('currentAccount', accountColl.length > 0 ? accountColl[0] : {});
        }

        Ember.set(this, 'accountColl', accountColl);
    },

    checkFilterMatch: function (order, portfolioFilter, sideFilter, statusFilter) {
        // If an argument is false, that means that filter is not applied
        var isMatchedPortfolioFilter = !portfolioFilter || portfolioFilter.isShowAll;
        var isMatchedSideFilter = !sideFilter || sideFilter.isShowAll;
        var isMatchedStatusFilter = !statusFilter || statusFilter.isShowAll;

        if (!isMatchedPortfolioFilter) {
            isMatchedPortfolioFilter = order.secAccNum === portfolioFilter.tradingAccId;
        }

        if (!isMatchedSideFilter) {
            isMatchedSideFilter = order.ordSide === sideFilter.code;
        }

        if (!isMatchedStatusFilter) {
            isMatchedStatusFilter = order.ordSts === statusFilter.value;
        }

        return isMatchedPortfolioFilter && isMatchedSideFilter && isMatchedStatusFilter;
    },

    checkFilterUpdate: function () {
        Ember.run.once(this, this.filterOrders);
    }.observes('currentAccount', 'currentSide', 'currentStatus', 'masterContent.@each.ordSts'),

    filterOrders: function () {
        var portfolioFilter = this.get('currentAccount');
        var sideFilter = this.get('currentSide');
        var statusFilter = this.get('currentStatus');
        var group = this.get('group');
        var currentGroup = group.code;

        var filteredOrders = this.get('masterContent').filter((function (that) {    //eslint-disable-line
            return function (order) {
                if (portfolioFilter || sideFilter || statusFilter) {
                    return that.checkFilterMatch(order, portfolioFilter, sideFilter, statusFilter);
                } else {
                    return true;
                }
            };
        })(this));

        this.set('filteredContent', filteredOrders);
        this.set('content', filteredOrders);

        if (currentGroup !== 0) {
            this.groupContent(group);
        }
    },

    setCellViewsScopeToGlobal: function () {
        Ember.ClassicHeaderCell = ClassicHeaderCell;
        Ember.ClassicCell = ClassicCell;
        Ember.ClassicMappingCell = ClassicMappingCell;
        Ember.ClassicStatusCell = ClassicStatusCell;
        Ember.ButtonCell = ButtonCell;
        Ember.OrderPriceCell = OrderPriceCell;
    },

    cellViewsForColumns: {
        button: 'Ember.ButtonCell',
        classicMappingCell: 'Ember.ClassicMappingCell',
        classicStatusCell: 'Ember.ClassicStatusCell',
        classicCell: 'Ember.ClassicCell',
        orderPriceCell: 'Ember.OrderPriceCell'
    },

    groupContent: function (group) {
        var groupArray = Ember.A();
        this.set('group', group);

        if (group && group.code) {
            var that = this;
            var currentGroup = group.code;
            var currentContent = this.get('filteredContent');
            var groupedContentMap = {};

            Ember.$.each(currentContent, function (key, row) {
                if (groupedContentMap[row[currentGroup]]) {
                    groupedContentMap[row[currentGroup]].pushObject(row);
                } else {
                    groupedContentMap[row[currentGroup]] = Ember.A([that.getRootRow(row, group), row]);
                }
            });

            Ember.$.each(groupedContentMap, function (key, rowArray) {
                groupArray.pushObjects(rowArray);
            });

            this.set('groupedContent', groupedContentMap);
            this.set('sortProperties', []);
        } else {
            // Selecting none in 'group by' while filter is added
            if (this.get('currentAccount') || this.get('currentType') || this.get('currentStatus')) {
                groupArray = this.get('filteredContent');
            }
            else {
                groupArray = this.get('masterContent');
            }
        }

        this.set('content', groupArray);
    },

    getRootRow: function (row, currentGroup) {
        var that = this;
        var rootRow = Ember.Object.create(row);
        var defaultGroupColumn = this.get('defaultGroupColumn');

        Ember.$.each(rootRow, function (key) {
            if (key !== currentGroup.code) {
                if (rootRow.hasOwnProperty(key) && key !== defaultGroupColumn) {
                    rootRow[key] = '';
                }
            } else {
                rootRow.isRoot = true;
                rootRow.groupKey = rootRow[key];
                var groupKey = rootRow[key];

                if (currentGroup.dispProp && row.symbolInfo) {
                    groupKey = row.symbolInfo.get(currentGroup.dispProp);
                }

                if (key !== defaultGroupColumn) {
                    var labelKey = that.get('group').labelKey;
                    groupKey = that.app.lang.labels[[labelKey, groupKey].join(that.utils.Constants.StringConst.Underscore)];
                    rootRow[key] = '[ - ]  ' + groupKey;
                    rootRow.key = key;
                } else {
                    rootRow.clOrdId = '[ - ]  ' + groupKey;
                    rootRow.key = 'dSym';
                }
            }
        });

        rootRow.firstValueStyle = 'hide';
        return rootRow;
    },

    rearrangeGroupedRows: function (groupedBy, isShrunk) {
        var groupKey = groupedBy;

        if (groupKey) {
            var defaultGroupColumn = this.get('defaultGroupColumn');
            var key = this.get('group').code;
            var groupedContent = Ember.$.extend({}, this.get('groupedContent'));
            var groupedContentArray = groupedContent[groupKey];
            var contentArray = Ember.A();

            if (key !== defaultGroupColumn) {
                var labelKey = this.get('group').labelKey;
                groupKey = this.app.lang.labels[[labelKey, groupKey].join(this.utils.Constants.StringConst.Underscore)];
            }

            Ember.$.each(groupedContentArray, function (index, row) {
                if (row && row.isRoot) {
                    Ember.set(row, row.key, isShrunk ? '[ + ]  ' + groupKey : '[ - ]  ' + groupKey);
                    Ember.set(row, 'isShrunk', isShrunk);
                }
            });

            Ember.$.each(groupedContent, function (groupIndex, rowArray) {
                groupedContent[groupIndex] = rowArray[0].isShrunk ? Ember.A([rowArray[0]]) : rowArray;
            });

            Ember.$.each(groupedContent, function (contentKey, rowArray) {
                contentArray.pushObjects(rowArray);
            });

            this.set('content', contentArray);
        }
    },

    loadSideDropdown: function () {
        var that = this;
        var exg = this.tradeService.userDS.get('userExchg')[0];
        var orderSides = this.tradeService.tradeMetaDS.getOrderSideCollectionByExchange(exg);

        if (orderSides) {
            Ember.$.each(orderSides, function (index, option) {
                Ember.set(option, 'DisplayName', that.app.lang.labels[[that.tradeService.tradeMetaDS.metaMapping.orderSide, option.code].join('_')]);
            });
        }

        var sides = [];
        var currentSide = this.get('currentSide');
        var optionAll = {DisplayName: this.app.lang.labels.all, code: '-1', isShowAll: true};

        sides.pushObject(optionAll);
        sides.pushObjects(orderSides);

        // TODO: [satheeqh] Set language wise description
        if (Ember.$.isEmptyObject(currentSide)) {
            this.set('currentSide', optionAll);
        }

        this.set('sides', sides);
    },

    loadStatusDropdown: function () {
        var that = this;
        var currentStatus = this.get('currentStatus');
        var optionAll = {DisplayName: this.get('app').lang.labels.all, isShowAll: true, value: '-1'};
        var selectedStatus = [
            tradeConstants.OrderStatusMapping['0'],
            tradeConstants.OrderStatusMapping['1'],
            tradeConstants.OrderStatusMapping['2'],
            tradeConstants.OrderStatusMapping['5'],
            tradeConstants.OrderStatusMapping['4'],
            tradeConstants.OrderStatusMapping['8'],
            tradeConstants.OrderStatusMapping.O];

        var options = [optionAll];

        if (selectedStatus) {
            Ember.$.each(selectedStatus, function (index, option) {
                Ember.set(option, 'DisplayName', that.app.lang.labels[[tradeConstants.ordStatusLabel, option.value].join('_')]);
                options.pushObject(option);
            });
        }

        if (Ember.$.isEmptyObject(currentStatus)) {
            this.set('currentStatus', optionAll);
        }

        this.set('status', options);
    },

    loadGroupOptions: function () {
        var that = this;
        var groupOptions = this.get('groupOptions');
        var options = [];

        if (groupOptions) {
            Ember.$.each(groupOptions, function (index, option) {
                Ember.set(option, 'des', that.app.lang.labels[option.langKey]);
                options.pushObject(option);
            });
        }

        var currentGroup = this.get('currentGroup');

        if (Ember.$.isEmptyObject(currentGroup)) {
            this.set('currentGroup', options[0]);
        }

        this.set('groups', options);
    },

    popUpOrderDetails: function (mubOrdNum) {
        var tradeUIService = sharedService.getService('tradeUI');
        var order = this.tradeService.orderDS.getOrder(mubOrdNum);

        if (order) {
            var numberOfRows = Math.ceil(tradeWidgetConfig.orderDetails.orderDetailsPopup.length / 3);
            var popupWindowHeight = (numberOfRows * this.ODRowHeight) + this.ODHeaderHeight;
            var popupHeight = order.ordSts === tradeConstants.OrderStatus.Rejected ? popupWindowHeight + this.ODRejectReasonHeight : popupWindowHeight;

            var controllerString = 'controller:trade/widgets/order-details';
            var routeString = 'trade/widgets/order-details';
            var viewName = 'view:widget-popup-view';

            tradeUIService.showPopupWidget({
                container: this.container,
                controllerString: controllerString,
                routeString: routeString,
                viewName: viewName
            }, {orderInfo: order, dimensions: {w: 685, h: popupHeight}});
        }
    },

    /* *
     * Need to reload context items on every row click as amend/cancel option css need to be updated.
     */
    loadOrderListContextItems: function () {
        var that = this;
        var config = this.orderTicketConfig;

        var contextItems = [
            {
                view: {
                    key: 'amend',
                    name: that.get('app').lang.labels.amend,
                    menuClass: 'highlight-fore-color',
                    iconClass: 'icon-amend',
                    shortcut: 'Alt + A',
                    displayStyle: that.amendVisibilityStyle
                },
                config: {
                    callbackFunc: function (amendConfig, args) {
                        that.amendOrder(amendConfig, args);
                    },
                    controllerString: config.controllerString,
                    routeString: config.routeString,
                    viewName: config.viewName
                },
                args: {}
            },
            {
                view: {
                    key: 'cancel',
                    name: that.get('app').lang.labels.cancel,
                    menuClass: 'color-1',
                    iconClass: 'icon-cancel',
                    shortcut: 'Alt + X',
                    displayStyle: that.cancelVisibilityStyle
                },
                config: {
                    callbackFunc: function (cancelConfig, args) {
                        that.cancelOrder(cancelConfig, args);
                    },
                    controllerString: config.controllerString,
                    routeString: config.routeString,
                    viewName: config.viewName
                },
                args: {}
            }
        ];

        this.set('orderListContextItems', contextItems);
    },

    amendOrder: function (config, args) {
        var clickedOrder = this.get('clickedRowOrderNo') ? this.tradeService.orderDS.getOrder(this.get('clickedRowOrderNo')) : undefined;

        if (clickedOrder && args.sym && args.exg && clickedOrder.get('isAmendEnabled')) {
            clickedOrder.set('action', tradeConstants.OrderAction.Amend);
            clickedOrder.set('displayPrice', clickedOrder.price);
            args.order = clickedOrder;

            sharedService.getService('tradeUI').showPopupWidget(config, args);
        }
    },

    cancelOrder: function (config, args) {
        var clickedOrder = this.get('clickedRowOrderNo') ? this.tradeService.orderDS.getOrder(this.get('clickedRowOrderNo')) : undefined;

        if (clickedOrder && args.sym && args.exg && clickedOrder.get('isCancelEnabled')) {
            clickedOrder.set('action', tradeConstants.OrderAction.Cancel);
            clickedOrder.set('displayPrice', clickedOrder.price);
            args.order = clickedOrder;

            sharedService.getService('tradeUI').showPopupWidget(config, args);
        }
    },

    loadDropDowns: function () {
        this.loadPortfolioOptions();
        this.loadSideDropdown();
        this.loadStatusDropdown();
        this.loadGroupOptions();
    },

    _generateFullContextMenu: function () {
        this.loadOrderListContextItems();
        this.menuComponent.fullContextMenu.insertAt(0, this.get('orderListContextItems'));
        this.menuComponent.fullContextMenu.insertAt(1, this.menuComponent.tradeContextMenu);
    },

    actions: {
        sort: function (column) {
            if (!column.get('isSortSupported')) {
                return;
            }

            if (this.get('sortColumn') !== column) {
                this.get('columns').setEach('isSorted', false);
                column.set('isSorted', true);
                this.set('sortColumn', column);
                this.set('sortProperties', [column.get('sortKey')]);
                this.set('isSortApplied', true);
            } else if (this.get('sortColumn') === column) {
                // Handle disabling sorts
                if (this.get('sortAscending') === true) {
                    this.set('sortColumn', undefined);
                    this.set('sortAscending', false);
                    column.set('isSorted', false);
                    this.set('isSortApplied', false);
                    this.set('sortProperties', []);
                } else {
                    this.set('sortProperties', [column.get('sortKey')]);
                    this.toggleProperty('sortAscending');
                }
            }
        },

        setLink: function (option) {
            this.set('selectedLink', option.code);
        },

        clickRow: function (selectedRow, event) {
            var rowData = selectedRow.getProperties('isRoot', 'symbol', 'mubOrdNum', 'exg', 'instruTyp');

            if (rowData && rowData.isRoot && event.button !== 2) {
                rowData = selectedRow.getProperties('groupKey', 'isShrunk');
                this.rearrangeGroupedRows(rowData.groupKey, !rowData.isShrunk);
            }

            if (!rowData.isRoot) {
                this._super(event);
            } else {
                return;
            }

            this.menuComponent = this.container.lookup('component:symbol-click-menu-popup');
            this.menuComponent.associatedController = this;

            if (!this.menuComponent) { // Create a symbol-click-menu-popup component object and call base-context-menu
                this.menuComponent = this.container.lookupFactory('component:symbol-click-menu-popup').create({associatedController: this});
            }

            if (rowData && rowData.symbol) {
                this.set('clickedRowOrderNo', rowData.mubOrdNum);
                this.set('clickedRowSymbol', rowData.symbol);
                this.set('clickedRowExchange', rowData.exg);
                this.set('clickedRowInst', rowData.instruTyp);

                // Set symbol details on symbol-click-menu-popup
                var symbolObj = {sym: rowData.symbol, exg: rowData.exg, inst: rowData.instruTyp};
                this.menuComponent.set('selectedSymbol', symbolObj);

                // Check order amend/cancel enabled for order
                var clickedOrder = this.get('clickedRowOrderNo') ? this.tradeService.orderDS.getOrder(this.get('clickedRowOrderNo')) : undefined;

                if (clickedOrder && clickedOrder.get('isAmendEnabled')) {
                    this.set('amendVisibilityStyle', '');
                } else {
                    this.set('amendVisibilityStyle', 'disable-style ');
                }

                if (clickedOrder && clickedOrder.get('isCancelEnabled')) {
                    this.set('cancelVisibilityStyle', '');
                } else {
                    this.set('cancelVisibilityStyle', 'disable-style ');
                }

                this.menuComponent.initialize(this.get('wkey'), symbolObj);

                if (this.isTablet) {
                    var cellId;
                    var target = event.target ? event.target : event.srcElement;

                    if (target && target.attributes) {
                        cellId = target.attributes.getNamedItem('cell-id') ? target.attributes.getNamedItem('cell-id').value : target.parentNode.attributes.getNamedItem('cell-id') ? target.parentNode.attributes.getNamedItem('cell-id').value : '';
                    }

                    if (cellId && this.utils.validators.isAvailable(rowData.mubOrdNum)) {
                        this.popUpOrderDetails(rowData.mubOrdNum);
                    }
                }
            }

            this._generateFullContextMenu();

            var viewName = 'components/symbol-click-menu-popup';
            var modal = sharedService.getService('sharedUI').getService('modalPopupId');

            if (event && event.button === 2) { // Add a constant to this
                this.menuComponent.showPopup(this.menuComponent, viewName, modal);
            } else {
                modal.send('closeModalPopup');
            }
        },

        doubleClickRow: function (selectedRow) {
            var rowOrderId;
            var rowData = selectedRow.getProperties('mubOrdNum');

            if (this.utils.validators.isAvailable(rowData.mubOrdNum)) {
                rowOrderId = rowData.mubOrdNum;
                this.popUpOrderDetails(rowOrderId);
            }
        },

        setPortfolio: function (option) {
            this.set('currentAccount', option);
        },

        setSide: function (option) {
            this.set('currentSide', option);
        },

        setStatus: function (option) {
            this.set('currentStatus', option);
        },

        setGroup: function (option) {
            this.set('currentGroup', option);
            this.groupContent(option);
        },

        onAmendOrder: function () {
            var args = {
                sym: this.get('clickedRowSymbol'),
                exg: this.get('clickedRowExchange'),
                inst: this.get('clickedRowInst')
            };

            var config = this.orderTicketConfig;
            config.container = this.container;

            this.amendOrder(config, args);
        },

        onCancelOrder: function () {
            var args = {
                sym: this.get('clickedRowSymbol'),
                exg: this.get('clickedRowExchange'),
                inst: this.get('clickedRowInst')
            };

            var config = this.orderTicketConfig;
            config.container = this.container;

            this.cancelOrder(config, args);
        },

        getOrderDetailsPopup: function () {
            var mubOrdNum = this.get('clickedRowOrderNo');

            if (this.utils.validators.isAvailable(mubOrdNum)) {
                this.popUpOrderDetails(mubOrdNum);
            }
        },

        fullScreenToggle: function () {
            this.toggleProperty('disableInnerWidgets');
            this.toggleFullScreen('orderList-' + this.get('wkey'), this.get('wkey'));
            this.toggleProperty('isRefreshed');
        }
    }
});
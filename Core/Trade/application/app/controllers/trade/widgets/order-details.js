import Ember from 'ember';
import TradeBaseController from '../controllers/trade-base-controller';
import tradeConstants from '../../../models/trade/trade-constants';
import sharedService from '../../../models/shared/shared-service';
import tradeWidgetConfig from '../../../config/trade-widget-config';
import PanelField from '../../../models/price/business-entities/panel-field';
import userSettings from '../../../config/user-settings';
import utils from '../../../utils/utils';

export default TradeBaseController.extend({
    isShowTitle: false,
    title: '',
    titleKey: 'orderDetails',

    statusCss: '',
    sideCss: '',
    orderInfo: {},
    isOrderRejected: false,
    fieldList: [],
    dataObj: [],
    fields: [],
    panelFields: Ember.A(),

    onLoadWidget: function () {
        this.setTitle();
        this.initializeOrderDetails();
    },

    onClearData: function () {
        this.set('orderInfo', {});

        var panelFields = this.get('panelFields');

        Ember.$.each(panelFields, function (key, panelField) {
            if(!panelField.isDestroyed) {
                panelField.destroy();
            }
        });
    },

    onLanguageChanged: function () {
        this.setTitle();
    },

    setTitle: function () {
        var orderInfo = this.get('orderInfo');
        var title = this.get('app').lang.labels[this.get('titleKey')];

        if (orderInfo && orderInfo.symbolInfo) {
            title = [title, orderInfo.symbolInfo.get('dispProp1')].join(' - ');
        }

        this.set('title', title);
    },

    decimalPlace: function () {
        var orderInfo = this.get('orderInfo');

        if (orderInfo && orderInfo.symbol && orderInfo.exg) {
            var priceService = sharedService.getService('price');
            var symbolInfo = priceService.stockDS.getStock(orderInfo.get('exg'), orderInfo.get('symbol'));

            if (symbolInfo && symbolInfo.deci) {
                return symbolInfo.get('deci');
            }
        }

        return userSettings.displayFormat.decimalPlaces;
    }.property('orderInfo'),

    orderStatus: function () {
        var order = this.get('orderInfo');
        var des = '';

        if (order && order.ordSts) {
            var currentValue = order.ordSts;
            var status = [tradeConstants.ordStatusLabel, currentValue].join('_');
            this.set('statusCss', status);
            des = this.get('app').lang.labels[status] ? this.get('app').lang.labels[status] : currentValue;

            this.set('isOrderRejected', currentValue === tradeConstants.OrderStatus.Rejected);
        }

        return des;
    }.property('orderInfo.ordSts', 'app.lang'),

    orderSide: function () {
        var order = this.get('orderInfo');
        var des = '';

        if (order && order.ordSide) {
            var currentValue = order.ordSide;
            var side = [sharedService.getService('trade').tradeMetaDS.metaMapping.orderSide, currentValue].join('_');
            this.set('sideCss', side);
            des = this.get('app').lang.labels[side] ? this.get('app').lang.labels[side] : currentValue;
        }

        return des;
    }.property('orderInfo.ordSide', 'app.lang'),

    orderType: function () {
        var order = this.get('orderInfo');
        var des = '';

        if (order && order.ordTyp) {
            var currentValue = order.ordTyp;
            var type = [sharedService.getService('trade').tradeMetaDS.metaMapping.orderType, currentValue].join('_');
            des = this.get('app').lang.labels[type] ? this.get('app').lang.labels[type] : currentValue;
        }

        return des;
    }.property('orderInfo.ordTyp', 'app.lang'),

    tifType: function () {
        var order = this.get('orderInfo');
        var des = '';

        if (order && this.utils.validators.isAvailable(order.tif)) {
            var currentValue = order.tif;
            var type = [sharedService.getService('trade').tradeMetaDS.metaMapping.tifType, currentValue].join('_');
            des = this.get('app').lang.labels[type] ? this.get('app').lang.labels[type] : currentValue;
        }

        return des;
    }.property('orderInfo.tif', 'app.lang'),

    assetType: function () {
        var orderInfo = this.get('orderInfo');

        if (orderInfo && this.utils.validators.isAvailable(orderInfo.instruTyp)) {
            var inst = orderInfo.get('instruTyp');
            var asset = this.get('app').lang.labels[this.utils.AssetTypes.AssetLangKeys[inst]];
            return asset ? asset : inst;
        }

        return '';
    }.property('orderInfo.instruTyp', 'app.lang'),

    isMarketOrder: function () {
        var orderInfo = this.get('orderInfo');

        return orderInfo && orderInfo.ordTyp && orderInfo.ordTyp === tradeConstants.OrderType.Market;
    }.property('orderInfo'),

    initializeOrderDetails: function () {
        if (tradeWidgetConfig.orderDetails.orderDetailsPopup) {
            var dataObj = this.get('orderInfo');
            var fields = [];
            var noOfDecimals = this.get('decimalPlace');
            var ordType = dataObj.get('ordTyp');

            if (dataObj && dataObj.ordSts) {
                this.set('isOrderRejected', dataObj.ordSts === tradeConstants.OrderStatus.Rejected);
            }

            Ember.$.each(tradeWidgetConfig.orderDetails.orderDetailsPopup, function (key, value) {
                if (!utils.validators.isAvailable(value.allowedOrdTypes) || (value.allowedOrdTypes.length > 0 && value.allowedOrdTypes.contains(ordType))) {
                    var fieldObj = PanelField.create({valueObj: dataObj, fieldObj: value, noOfDecimals: noOfDecimals, value: dataObj.get(value.dataField)});
                    var factor = Math.ceil((fields.length / 3) - (fields.length / 3) % 1);

                    if (factor % 2 === 0) {
                        fieldObj.cellStyle = 'panel-table-row-even';
                    }

                    fields.pushObject(fieldObj);
                }
            });

            this.set('panelFields', fields);
        }
    },

    rejectReasonStyle: function () {
        var order = this.get('orderInfo');
        var style = '';

        if (order && utils.validators.isAvailable(order.ordSts)) {
            this.set('isOrderRejected', order.ordSts === tradeConstants.OrderStatus.Rejected);
            style = [tradeConstants.ordStatusLabel, order.ordSts].join('_');
        }

        return style;
    }.property('orderInfo.ordSts', 'app.lang')
});
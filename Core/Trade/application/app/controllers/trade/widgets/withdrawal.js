import TransactionBase from './transaction-base';
import sharedService from '../../../models/shared/shared-service';
import appConfig from '../../../config/app-config';

export default TransactionBase.extend({
    withdrawalReasons: [],
    currentWithdrawalReason: {},
    tradeService: sharedService.getService('trade'),

    isTablet: appConfig.customisation.isTablet,

    onLoadWidget: function () {
        this._super();

        this.set('withdrawalReasons', this.tradeService.tradeMetaDS.getWithdrawalReasons());
        this.set('currentWithdrawalReason', this.get('withdrawalReasons')[0]);
    },

    submit: function () {
        var withDate = this.utils.validators.isAvailable(this.get('valueDate')) ? this.utils.formatters.convertToDisplayTimeFormat(this.get('valueDate'), 'YYYYMMDD') : '';
        var errors = this.validate();

        if (errors && errors.length > 0) {
            this.set('errorMessages', errors);
            this.showMessagePopup();
        } else {
            sharedService.getService('trade').sendWithdrawalRequest({
                payMtd: this.get('currentPayMethod.Id'),
                cashAccId: this.get('currentAccount.cashAccountId'),
                curr: this.get('currencySelection.DisplayName'),
                amt: this.get('amount'),
                toAcc: this.get('currentBnkAcnt.accId'),
                valDte: withDate,
                narr: this.get('currentWithdrawalReason.DisplayName')
            });
        }
    },

    actions: {
        onWithdReasonChanged: function (item) {
            this.set('currentWithdrawalReason', item);
            var transaction = this.get('transaction');

            if (transaction) {
                transaction.set('nts', item.Id);
            }
        }
    }
});
import Ember from 'ember';
import brokerageSelection from '../widgets/brokerage-connectivity-selection';

export default (function () {
    var prepareBrokerageView = function () {
        brokerageSelection.prepareView();
    };

    var showBrokerageView = function () {
        Ember.$('div#divBrokerage').show();
        Ember.$('div#divLogin').hide();
    };

    return {
        prepareBrokerageView: prepareBrokerageView,
        showBrokerageView: showBrokerageView
    };
})();
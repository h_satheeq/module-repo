import Ember from 'ember';

export default Ember.Mixin.create({
    onTradeMetaReady: function (isReconnection) {
        if (isReconnection) {
            this.onPrepareData();
            this.onAddSubscription();
        }
    },

    onTradeDisconnect: function () {
        this.onClearData();
    }
});
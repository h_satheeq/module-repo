import Ember from 'ember';
import utils from '../../utils/utils';
import sharedService from '../../models/shared/shared-service';
import appConfig from '../../config/app-config';
import authenticationConstants from '../../controllers/authentication/authentication-constants';
import tradeConstants from '../../models/trade/trade-constants';

export default Ember.Controller.extend({
    resendSubscriptions: false,

    /* *
     * Authenticate user
     * @param username Username
     * @param password Password
     * @param allowInit Allow application to initialize before login
     * @private
     */
    authenticateUser: function (username, password, allowInit, authSuccess, authFail) {
        var that = this;
        var tradeService = sharedService.getService('trade');

        // Initialize application if and only if the given user is the last successfully logged-in user
        authSuccess(username, password, allowInit);

        tradeService.authenticateWithUsernameAndPassword({
            username: username,
            password: password,
            resendSubscriptions: that.resendSubscriptions,

            authSuccess: function (otpStatus) {
                var priceService = sharedService.getService('price');

                switch (otpStatus) {
                    case tradeConstants.OtpStatus.OtpDisabled:
                        that._prepareHomePage(authSuccess, username, password, !allowInit);
                        that._authenticatePriceUser(priceService);
                        break;

                    case tradeConstants.OtpStatus.OtpPending:
                        that._prepareHomePage(authSuccess, username, password, !allowInit);
                        break;

                    case tradeConstants.OtpStatus.OtpSuccess:
                        that._authenticatePriceUser(priceService);
                        break;

                    default:
                        break;
                }
            },

            authFailed: function (reason) {
                that.resendSubscriptions = true;
                tradeService.webSocketManager.closeConnection(tradeService.constants.SocketConnectionType.OMS);

                authFail(reason, username, password);
            }
        });
    },

    _prepareHomePage: function (authSuccess, username, password, allowInit) {
        // Initialize application if and only if the given user is the last successfully logged-in user
        authSuccess(username, password, allowInit);
    },

    _authenticatePriceUser: function (priceService) {
        if (authenticationConstants.AuthModes.TradeRetailPriceSso === appConfig.customisation.authenticationMode) {
            if (priceService) {
                priceService.authenticateWithSsoToken({
                    ssoToken: sharedService.getService('trade').userDS.prcUsr,
                    ssoType: utils.Constants.SsoTypes.Trade
                });
            }
        }
    }
}).create();

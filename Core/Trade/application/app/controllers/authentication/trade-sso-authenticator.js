import Ember from 'ember';
import sharedService from '../../models/shared/shared-service';
import appConfig from '../../config/app-config';
import authenticationConstants from '../../controllers/authentication/authentication-constants';

export default Ember.Controller.extend({
    /* *
     * Authenticate user
     * @param ssoToken SSO token
     * @private
     */
    authenticateUser: function (ssoToken, authSuccess, authFail) {
        var tradeService = sharedService.getService('trade');

        tradeService.authenticateWithSsoToken({
            ssoToken: ssoToken,

            authSuccess: function () {
                authSuccess();

                if (authenticationConstants.AuthModes.TradeSsoPriceSso === appConfig.customisation.authenticationMode) {
                    var priceService = sharedService.getService('price');

                    if (priceService) {
                        priceService.authenticateWithUsernameAndPassword({
                            // TODO: [Bashitha] Call price sso authentication
                        });
                    }
                }
            },

            authFailed: function (reason) {
                tradeService.webSocketManager.closeConnection(tradeService.constants.SocketConnectionType.OMS);
                authFail(reason);
            }
        });
    }
}).create();

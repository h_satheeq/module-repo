export default {
    portfolio: {
        defaultColumnMapping: {     // Column Object parameters : id, width, headerName, sortKeyword, multiValueIds, cellStyle, sortDisable, firstValueStyle, isBold, dataType, backgroundColour,
            buyMore: {id: 'buyMore', width: 25, headerCellView: 'Ember.ClassicHeaderCell', name: 'buyMore', headerName: '', headerStyle: 'text-center', iconClass: 'icon-arrow-circle-up font-x-l up-fore-color', isColumnSortDisabled: true, type: 'button', title: 'buyMore', buttonFunction: 'buyHolding'},
            liquidate: {id: 'liquidate', width: 25, headerCellView: 'Ember.ClassicHeaderCell', name: 'liquidate', headerName: '', headerStyle: 'text-center', iconClass: 'icon-arrow-circle-down font-x-l down-fore-color', isColumnSortDisabled: true, type: 'button', title: 'sell', buttonFunction: 'liquidateHolding'},
            sym: {id: 'symbolInfo.dSym', width: 105, headerCellView: 'Ember.ClassicHeaderCell', headerName: 'symbol', headerStyle: 'text-left-header', cellStyle: 'text-left-header', sortKeyword: 'symbol', type: 'classic', firstValueStyle: 'symbol-fore-color bold'},
            sDes: {id: 'symbolInfo.sDes', width: 105, headerCellView: 'Ember.ClassicHeaderCell', headerName: 'sDescription', headerStyle: 'text-left-header', cellStyle: 'text-left-header', sortKeyword: 'symbol', type: 'classic', firstValueStyle: 'bold fore-color'},
            secAccNum: {id: 'secAccNum', width: 100, headerCellView: 'Ember.ClassicHeaderCell', headerName: 'portfolioId', headerStyle: 'text-left-header', cellStyle: 'text-left-header', sortKeyword: 'portfolio', type: 'classic', firstValueStyle: 'fore-color'},
            qty: {id: 'qty', width: 80, headerName: 'quantity', sortKeyword: 'qty', dataType: 'int', type: 'classic', firstValueStyle: 'colour-normal bold', headerCellView: 'Ember.ClassicHeaderCell'},
            pendSell: {id: 'pendSell', width: 70, headerName: 'sellPending', sortKeyword: 'pendSell', dataType: 'int', type: 'classic', firstValueStyle: 'colour-normal', headerCellView: 'Ember.ClassicHeaderCell'},
            pendBuy: {id: 'pendBuy', width: 70, headerName: 'buyPending', sortKeyword: 'pendBuy', dataType: 'int', type: 'classic', firstValueStyle: 'colour-normal', headerCellView: 'Ember.ClassicHeaderCell'},
            avaiQty: {id: 'avaiQty', width: 80, headerName: 'availQty', sortKeyword: 'avaiQty', dataType: 'int', type: 'classic', firstValueStyle: 'colour-normal', isBlink: true, blinkUpStyle: 'blink-font-style-change', blinkDownStyle: 'blink-font-style-change', headerCellView: 'Ember.ClassicHeaderCell'},
            mktPrice: {id: 'mktPrice', width: 80, headerName: 'mktPrice', sortKeyword: 'mktPrice', dataType: 'float', type: 'classic', firstValueStyle: 'colour-normal bold', isBlink: true, blinkUpStyle: 'blink-font-style-change', blinkDownStyle: 'blink-font-style-change', headerCellView: 'Ember.ClassicHeaderCell'},
            avgCst: {id: 'avgCst', width: 60, headerName: 'avgCost', sortKeyword: 'avgCst', dataType: 'float', type: 'classic', firstValueStyle: 'colour-normal', headerCellView: 'Ember.ClassicHeaderCell'},
            costVal: {id: 'costVal', width: 100, headerName: 'costValue', sortKeyword: 'costVal', dataType: 'float', type: 'classic', firstValueStyle: 'colour-normal', headerCellView: 'Ember.ClassicHeaderCell'},
            mktVal: {id: 'mktVal', width: 100, headerName: 'marketValue', sortKeyword: 'marketVal', dataType: 'float', type: 'classic', firstValueStyle: 'colour-normal', headerCellView: 'Ember.ClassicHeaderCell'},
            gainLoss: {id: 'gainLoss', width: 85, headerName: 'unrealizedGL', sortKeyword: 'gainLoss', dataType: 'float', positiveNegativeChange: true, type: 'classic', firstValueStyle: 'bold', headerCellView: 'Ember.ClassicHeaderCell'},
            gainLossPer: {id: 'gainLossPer', width: 80, headerName: 'unrealizedGLPer', sortKeyword: 'gainLossPer', dataType: 'float', positiveNegativeChange: true, type: 'classic', firstValueStyle: 'bold', headerCellView: 'Ember.ClassicHeaderCell'},
            portPer: {id: 'portPer', width: 60, headerName: 'portfolioPer', sortKeyword: 'portPer', dataType: 'float', type: 'classic', headerCellView: 'Ember.ClassicHeaderCell'},
            curr: {id: 'curr', width: 60, headerName: 'curr', headerStyle: 'text-left-header', cellStyle: 'text-center-header', sortKeyword: 'curr', type: 'classic', headerCellView: 'Ember.ClassicHeaderCell'},
            exg: {id: 'exg', width: 65, headerCellView: 'Ember.ClassicHeaderCell', headerName: 'exchange', headerStyle: 'text-left-header', cellStyle: 'text-left-header', sortKeyword: 'exg', type: 'classic', firstValueStyle: 'colour-normal'},
            penHolding: {id: 'penHolding', width: 90, headerName: 'pendHolding', sortKeyword: 'penHolding', dataType: 'int', type: 'classic', firstValueStyle: 'colour-normal', isBlink: true, blinkUpStyle: 'blink-font-style-change', blinkDownStyle: 'blink-font-style-change', headerCellView: 'Ember.ClassicHeaderCell'},
            pldQty: {id: 'pldQty', width: 80, headerName: 'pledgeQty', sortKeyword: 'avaiQty', dataType: 'int', type: 'classic', firstValueStyle: 'colour-normal', isBlink: true, blinkUpStyle: 'blink-font-style-change', blinkDownStyle: 'blink-font-style-change', headerCellView: 'Ember.ClassicHeaderCell'},
            payQty: {id: 'payQty', width: 80, headerName: 'payableQty', sortKeyword: 'avaiQty', dataType: 'int', type: 'classic', firstValueStyle: 'colour-normal', isBlink: true, blinkUpStyle: 'blink-font-style-change', blinkDownStyle: 'blink-font-style-change', headerCellView: 'Ember.ClassicHeaderCell'},
            recQty: {id: 'recQty', width: 85, headerName: 'receivableQty', sortKeyword: 'avaiQty', dataType: 'int', type: 'classic', firstValueStyle: 'colour-normal', isBlink: true, blinkUpStyle: 'blink-font-style-change', blinkDownStyle: 'blink-font-style-change', headerCellView: 'Ember.ClassicHeaderCell'},
            pendSubQty: {id: 'pendSubQty', width: 130, headerName: 'pendingSubscriptionQty', sortKeyword: 'avaiQty', dataType: 'int', type: 'classic', firstValueStyle: 'colour-normal', isBlink: true, blinkUpStyle: 'blink-font-style-change', blinkDownStyle: 'blink-font-style-change', headerCellView: 'Ember.ClassicHeaderCell'},
            subQty: {id: 'subQty', width: 90, headerName: 'subQty', sortKeyword: 'avaiQty', dataType: 'int', type: 'classic', firstValueStyle: 'colour-normal', isBlink: true, blinkUpStyle: 'blink-font-style-change', blinkDownStyle: 'blink-font-style-change', headerCellView: 'Ember.ClassicHeaderCell'},
            wAvgPrice: {id: 'wAvgPrice', width: 80, headerName: 'wAvgPrice', sortKeyword: 'mktPrice', dataType: 'float', type: 'classic', firstValueStyle: 'colour-normal bold', isBlink: true, blinkUpStyle: 'blink-font-style-change', blinkDownStyle: 'blink-font-style-change', headerCellView: 'Ember.ClassicHeaderCell'},
            subPrice: {id: 'subPrice', width: 100, headerName: 'subPrice', sortKeyword: 'mktPrice', dataType: 'float', type: 'classic', firstValueStyle: 'colour-normal bold', isBlink: true, blinkUpStyle: 'blink-font-style-change', blinkDownStyle: 'blink-font-style-change', headerCellView: 'Ember.ClassicHeaderCell'},
            isRightSymbol: {id: 'isRightSymbol', width: 25, headerCellView: 'Ember.ClassicHeaderCell', name: 'subscribe', headerName: '', headerStyle: 'text-center', iconClass: 'glyphicon glyphicon-download', isColumnSortDisabled: true, type: 'button', title: 'subscribe', buttonFunction: 'subscribeRights'}
        },

        defaultColumnIds: ['buyMore', 'liquidate', 'sym', 'sDes', 'secAccNum', 'qty', 'pendSell', 'pendBuy', 'avaiQty', 'mktPrice', 'avgCst', 'costVal', 'mktVal', 'gainLoss', 'gainLossPer', 'portPer', 'curr', 'exg'],

        moreColumnIds: ['sDes', 'secAccNum', 'qty', 'pendSell', 'pendBuy', 'avaiQty', 'mktPrice', 'avgCst', 'costVal', 'mktVal', 'gainLoss', 'gainLossPer', 'portPer', 'curr', 'exg'],

        nonRemovableColumnIds: ['buyMore', 'liquidate', 'sym'],

        tableParams: {
            MaxTableWidth: 5700,
            numOfFixedColumns: 3
        },

        footerParams: {
            // Format Accepting:- data type = string [eg: 'sDes'].
            // Not Accepting:- data type = int, float (other than string) [eg: 'qty'] & Properties like [eg: 'symbolInfo.sDes'].
            totalValueProp: 'dSym'
        }
    },

    orderList: {
        defaultColumnMapping: {    // Column Object parameters : id, width, headerName, sortKeyword, multiValueIds, cellStyle, sortDisable, firstValueStyle, isBold, dataType, backgroundColour,
            isAmendEnabled: {id: 'isAmendEnabled', width: 25, headerCellView: 'Ember.ClassicHeaderCell', name: 'amend', headerName: '', headerStyle: 'text-center', iconClass: 'glyphicon glyphicon-edit', isColumnSortDisabled: true, type: 'button', title: 'amend', buttonFunction: 'onAmendOrder'},
            isCancelEnabled: {id: 'isCancelEnabled', width: 25, headerCellView: 'Ember.ClassicHeaderCell', name: 'cancel', headerName: '', headerStyle: 'text-center', iconClass: 'glyphicon glyphicon-remove', isColumnSortDisabled: true, type: 'button', title: 'cancel', buttonFunction: 'onCancelOrder'},
            sym: {id: 'symbolInfo.dSym', width: 105, headerCellView: 'Ember.ClassicHeaderCell', sortKeyword: 'symbol', headerName: 'symbol', type: 'classicCell', firstValueStyle: 'symbol-fore-color bold', headerStyle: 'text-left-header', cellStyle: 'text-left-header'},
            clOrdId: {id: 'clOrdId', width: 80, headerCellView: 'Ember.ClassicHeaderCell', sortKeyword: 'clOrdId', headerName: 'orderId', type: 'classicCell', firstValueStyle: 'colour-normal bold', headerStyle: 'text-left-header', cellStyle: 'text-left-header'},
            ordSts: {id: 'ordSts', width: 80, headerCellView: 'Ember.ClassicHeaderCell', sortKeyword: 'ordSts', headerName: 'status', type: 'classicStatusCell', firstValueStyle: 'colour-normal bold', headerStyle: 'text-left-header', cellStyle: 'text-left-header', title: 'txt'},
            ordSide: {id: 'ordSide', width: 45, headerCellView: 'Ember.ClassicHeaderCell', name: 'orderSide', sortKeyword: 'ordSide', headerName: 'side', type: 'classicMappingCell', headerStyle: 'text-left-header', cellStyle: 'text-left-header'},
            ordQty: {id: 'ordQty', width: 80, headerCellView: 'Ember.ClassicHeaderCell', sortKeyword: 'ordQty', headerName: 'quantity', type: 'classicCell', dataType: 'int', firstValueStyle: 'colour-normal bold'},
            price: {id: 'price', width: 60, headerCellView: 'Ember.ClassicHeaderCell', sortKeyword: 'price', headerName: 'price', type: 'orderPriceCell', secondId: 'ordTyp', dataType: 'float', firstValueStyle: 'colour-normal bold'},
            cumQty: {id: 'cumQty', width: 60, headerCellView: 'Ember.ClassicHeaderCell', sortKeyword: 'fillQty', headerName: 'filledQty', type: 'classicCell', dataType: 'int', firstValueStyle: 'colour-normal bold', isBlink: true, blinkUpStyle: 'blink-font-style-change', blinkDownStyle: 'blink-font-style-change', isDefaultValueCheck: true, defaultValue: -1},
            pendQty: {id: 'pendQty', width: 100, headerCellView: 'Ember.ClassicHeaderCell', sortKeyword: 'pendQty', headerName: 'pendingQty', type: 'classicCell', dataType: 'int', firstValueStyle: 'colour-normal'},
            avgPrice: {id: 'avgPrice', width: 60, headerCellView: 'Ember.ClassicHeaderCell', sortKeyword: 'avgPrice', headerName: 'avgPrice', type: 'classicCell', dataType: 'float', firstValueStyle: 'colour-normal', isBlink: true, blinkUpStyle: 'blink-font-style-change', blinkDownStyle: 'blink-font-style-change', isDefaultValueCheck: true, defaultValue: -1},
            ordVal: {id: 'ordVal', width: 80, headerCellView: 'Ember.ClassicHeaderCell', sortKeyword: 'netOrdVal', headerName: 'orderValue', type: 'classicCell', dataType: 'float', firstValueStyle: 'colour-normal', isDefaultValueCheck: true, defaultValue: -1},
            commission: {id: 'comsn', width: 75, headerCellView: 'Ember.ClassicHeaderCell', sortKeyword: 'comsn', headerName: 'commission', type: 'classicCell', dataType: 'float', firstValueStyle: 'colour-normal', isDefaultValueCheck: true},
            vat: {id: 'vatAmount', width: 65, headerCellView: 'Ember.ClassicHeaderCell', sortKeyword: 'vatAmount', headerName: 'vat', type: 'classicCell', dataType: 'float', firstValueStyle: 'colour-normal', isDefaultValueCheck: true},
            netOrdVal: {id: 'netOrdVal', width: 80, headerCellView: 'Ember.ClassicHeaderCell', sortKeyword: 'netOrdVal', headerName: 'netValue', type: 'classicCell', dataType: 'float', firstValueStyle: 'colour-normal', isDefaultValueCheck: true, defaultValue: -1},
            ordTyp: {id: 'ordTyp', width: 65, headerCellView: 'Ember.ClassicHeaderCell', name: 'orderType', sortKeyword: 'ordSide', headerName: 'orderType', type: 'classicMappingCell', firstValueStyle: 'colour-normal', headerStyle: 'text-left-header', cellStyle: 'text-left-header'},
            tif: {id: 'tif', width: 45, headerCellView: 'Ember.ClassicHeaderCell', name: 'tifType', sortKeyword: 'ordSide', headerName: 'tif', type: 'classicMappingCell', firstValueStyle: 'colour-normal', headerStyle: 'text-left-header', cellStyle: 'text-left-header'},
            adjustedCrdDte: {id: 'adjustedCrdDte', width: 150, headerCellView: 'Ember.ClassicHeaderCell', sortKeyword: 'crdDte', headerName: 'orderDate', type: 'classicCell', firstValueStyle: 'colour-normal', headerStyle: 'text-left-header', cellStyle: 'text-left-header'},
            adjustedExpTime: {id: 'adjustedExpTime', width: 80, headerCellView: 'Ember.ClassicHeaderCell', sortKeyword: 'expTime', headerName: 'expiryDate', type: 'classicCell', firstValueStyle: 'colour-normal', headerStyle: 'text-left-header', cellStyle: 'text-left-header'},
            exg: {id: 'exg', width: 65, headerCellView: 'Ember.ClassicHeaderCell', sortKeyword: 'exg', headerName: 'exchange', type: 'classicCell', firstValueStyle: 'colour-normal', headerStyle: 'text-left-header', cellStyle: 'text-left-header'},
            instDes: {id: 'symbolInfo.instDes', width: 80, headerCellView: 'Ember.ClassicHeaderCell', headerName: 'symbolType', type: 'classicCell', sortKeyword: 'inst', cellStyle: 'text-left-header', firstValueStyle: 'fore-color bold'},
            sDes: {id: 'symbolInfo.sDes', width: 105, headerCellView: 'Ember.ClassicHeaderCell', headerName: 'sDescription', headerStyle: 'text-left-header', sortKeyword: 'sDes', cellStyle: 'text-left-header', type: 'classicCell', firstValueStyle: 'bold fore-color'}
        },

        defaultColumnIds: ['isAmendEnabled', 'isCancelEnabled', 'sym', 'clOrdId', 'ordSts', 'ordSide', 'ordQty', 'price', 'cumQty', 'pendQty', 'avgPrice', 'ordVal', 'netOrdVal', 'ordTyp', 'adjustedCrdDte', 'adjustedExpTime'],

        moreColumnIds: ['clOrdId', 'ordSts', 'ordSide', 'ordQty', 'price', 'cumQty', 'pendQty', 'avgPrice', 'ordVal', 'netOrdVal', 'ordTyp', 'tif', 'adjustedCrdDte', 'adjustedExpTime', 'exg'],

        nonRemovableColumnIds: ['isAmendEnabled', 'isCancelEnabled', 'sym'],

        tableParams: {
            MaxTableWidth: 5700,
            numOfFixedColumns: 3
        }
    },

    transactionHistory: {
        defaultColumnMapping: {     // Column Object parameters : id, width, headerName, sortKeyword, multiValueIds, cellStyle, sortDisable, firstValueStyle, isBold, dataType, backgroundColour,
            typ: {id: 'typ', width: 25, headerCellView: 'Ember.ClassicHeaderCell', name: 'type', headerName: 'type', headerStyle: 'text-center', iconClass: 'glyphicon glyphicon-edit', isColumnSortDisabled: true, type: 'button', title: 'amend'},
            mtd: {id: 'mtd', width: 30, headerCellView: 'Ember.ClassicHeaderCell', headerName: 'method', headerStyle: 'text-center', iconClass: 'glyphicon glyphicon-remove', isColumnSortDisabled: true, type: 'button', title: 'cancel'},
            sym: {id: 'symbol', width: 80, headerCellView: 'Ember.ClassicHeaderCell', sortKeyword: 'symbol', headerName: 'symbol', type: 'classicCell', firstValueStyle: 'colour-normal bold', headerStyle: 'text-left-header', cellStyle: 'text-left-header'},
            amount: {id: 'Amount', width: 60, headerCellView: 'Ember.ClassicHeaderCell', sortKeyword: 'amnt', headerName: 'amount', type: 'classicCell', firstValueStyle: 'colour-normal bold', headerStyle: 'text-left-header', cellStyle: 'text-left-header'},
            secAccNum: {id: 'secAccNum', width: 60, headerCellView: 'Ember.ClassicHeaderCell', sortKeyword: 'secAccNum', headerName: 'portfolioId', type: 'classicCell', firstValueStyle: 'colour-normal bold', headerStyle: 'text-left-header', cellStyle: 'text-left-header'},
            sts: {id: 'sts', width: 50, headerCellView: 'Ember.ClassicHeaderCell', sortKeyword: 'sts', headerName: 'status', type: 'classicStatusCell', firstValueStyle: 'colour-normal bold', headerStyle: 'text-left-header', cellStyle: 'text-left-header'},
            refId: {id: 'refId', width: 35, headerCellView: 'Ember.ClassicHeaderCell', sortKeyword: 'refId', headerName: 'refId', type: 'classicMappingCell', firstValueStyle: 'colour-normal bold', headerStyle: 'text-left-header', cellStyle: 'text-left-header'},
            bankAcnt: {id: 'bankAcnt', width: 60, headerCellView: 'Ember.ClassicHeaderCell', sortKeyword: 'bankAcnt', headerName: 'bankAccount', type: 'classicCell', dataType: 'int', firstValueStyle: 'colour-normal bold'},
            valDte: {id: 'valDte', width: 50, headerCellView: 'Ember.ClassicHeaderCell', sortKeyword: 'valDte', headerName: 'date', type: 'classicCell', dataType: 'float', firstValueStyle: 'colour-normal bold'},
            curr: {id: 'curr', width: 65, headerCellView: 'Ember.ClassicHeaderCell', sortKeyword: 'refId', headerName: 'curr', type: 'classicMappingCell', firstValueStyle: 'colour-normal', headerStyle: 'text-left-header', cellStyle: 'text-left-header'},
            invAccNum: {id: 'invAccNum', width: 140, headerName: 'investmentAcc', type: 'classicCell', sortKeyword: 'invAccNum', firstValueStyle: 'fore-color'},
            chTranId: {id: 'chTranId', width: 120, headerCellView: 'Ember.ClassicHeaderCell', sortKeyword: 'chTranId', isColumnSortDisabled: false, headerName: 'refId', type: 'classicMappingCell', firstValueStyle: 'colour-normal', headerStyle: 'text-left-header', cellStyle: 'text-left-header'},
            payMtd: {id: 'payMtd', width: 100, headerCellView: 'Ember.ClassicHeaderCell', sortKeyword: 'bankAcnt', headerName: 'method', type: 'payMethodCell', firstValueStyle: 'colour-normal', cellStyle: 'text-left-header'},
            bnkAccNum: {id: 'bnkAccNum', width: 115, headerCellView: 'Ember.ClassicHeaderCell', sortKeyword: 'bankAcnt', headerName: 'bankAccount', type: 'classicCell', firstValueStyle: 'colour-normal', headerStyle: 'text-center-header', cellStyle: 'text-left-header'}
        },

        defaultColumnIds: ['typ', 'mtd', 'sym', 'amount', 'secAccNum', 'sts', 'refId', 'bankAcnt', 'valDte'],

        tableParams: {
            MaxTableWidth: 5700,
            numOfFixedColumns: 3
        }
    },

    savedOrderList: {
        defaultColumnMapping: {     // Column Object parameters : id, width, headerName, sortKeyword, multiValueIds, cellStyle, sortDisable, firstValueStyle, isBold, dataType, backgroundColour,
            isChecked: {id: 'isChecked', width: 20, headerCellView: 'Ember.ClassicHeaderCell', name: 'orderSide', sortKeyword: 'isChecked', headerName: '', type: 'checkBoxCell', headerStyle: 'text-left-header', cellStyle: 'text-left-header'},
            ordSide: {id: 'ordSide', width: 40, headerCellView: 'Ember.ClassicHeaderCell', name: 'orderSide', sortKeyword: 'ordSide', headerName: 'side', type: 'classicMappingCell', headerStyle: 'text-left-header', cellStyle: 'text-left-header'},
            ordQty: {id: 'ordQty', width: 70, headerCellView: 'Ember.ClassicHeaderCell', sortKeyword: 'ordQty', headerName: 'quantity', type: 'classicCell', dataType: 'int', firstValueStyle: 'colour-normal bold'},
            symbol: {id: 'symbol', width: 75, headerCellView: 'Ember.ClassicHeaderCell', sortKeyword: 'symbol', headerName: 'symbol', type: 'classicCell', firstValueStyle: 'symbol-fore-color bold', headerStyle: 'text-left-header', cellStyle: 'text-left-header'},
            sDes: {id: 'symbolInfo.sDes', width: 130, headerCellView: 'Ember.ClassicHeaderCell', sortKeyword: 'sDescription', headerName: 'sDescription', type: 'classicCell', firstValueStyle: 'bold', headerStyle: 'text-left-header', cellStyle: 'text-left-header'},
            exg: {id: 'exg', width: 65, headerCellView: 'Ember.ClassicHeaderCell', sortKeyword: 'exg', headerName: 'exchange', type: 'classicCell', firstValueStyle: 'fore-color bold', headerStyle: 'text-left-header', cellStyle: 'text-left-header'},
            instDes: {id: 'symbolInfo.instDes', width: 80, headerCellView: 'Ember.ClassicHeaderCell', headerName: 'symbolType', type: 'classicCell', sortKeyword: 'inst', cellStyle: 'text-left-header', firstValueStyle: 'fore-color bold'},
            ordTyp: {id: 'ordTyp', width: 65, headerCellView: 'Ember.ClassicHeaderCell', name: 'orderType', sortKeyword: 'ordTyp', headerName: 'orderType', type: 'classicMappingCell', firstValueStyle: 'colour-normal', headerStyle: 'text-left-header', cellStyle: 'text-left-header'},
            price: {id: 'price', width: 60, headerCellView: 'Ember.ClassicHeaderCell', sortKeyword: 'price', headerName: 'price', type: 'orderPriceCell', secondId: 'ordTyp', dataType: 'float', firstValueStyle: 'colour-normal bold'},
            tif: {id: 'tif', width: 55, headerCellView: 'Ember.ClassicHeaderCell', name: 'tifType', sortKeyword: 'tif', headerName: 'tif', type: 'classicMappingCell', firstValueStyle: 'colour-normal', headerStyle: 'text-left-header', cellStyle: 'text-left-header'},
            expTime: {id: 'expTime', width: 95, headerCellView: 'Ember.ClassicHeaderCell', sortKeyword: 'expTime', headerName: 'expiryDate', type: 'classicCell', firstValueStyle: 'colour-normal', headerStyle: 'text-left-header', cellStyle: 'text-left-header', dataType: 'date'},
            date: {id: 'date', width: 130, headerCellView: 'Ember.ClassicHeaderCell', sortKeyword: 'date', headerName: 'date', type: 'classicCell', firstValueStyle: 'colour-normal', headerStyle: 'text-left-header', cellStyle: 'text-left-header'},
            secAccNum: {id: 'secAccNum', width: 90, headerCellView: 'Ember.ClassicHeaderCell', name: 'secAccNum', sortKeyword: 'secAccNum', headerName: 'portfolioId', type: 'classicCell', headerStyle: 'text-left-header', cellStyle: 'text-left-header'}
        },

        defaultColumnIds: ['isChecked', 'ordSide', 'ordQty', 'symbol', 'sDes', 'exg', 'instDes', 'ordTyp', 'price', 'tif', 'expTime', 'date', 'secAccNum'],

        tableParams: {
            MaxTableWidth: 5700
        }
    },

    orderSearch: {
        defaultColumnMapping: {     // Column Object parameters : id, width, headerName, sortKeyword, multiValueIds, cellStyle, sortDisable, firstValueStyle, isBold, dataType, backgroundColour,
            sym: {id: 'symbolInfo.dSym', width: 105, headerCellView: 'Ember.ClassicHeaderCell', sortKeyword: 'symbol', headerName: 'symbol', type: 'classicCell', firstValueStyle: 'symbol-fore-color bold', headerStyle: 'text-left-header', cellStyle: 'text-left-header'},
            sDes: {id: 'symbolInfo.sDes', width: 105, headerCellView: 'Ember.ClassicHeaderCell', headerName: 'sDescription', headerStyle: 'text-left-header', cellStyle: 'text-left-header', sortKeyword: 'symbol', type: 'classicCell', firstValueStyle: 'bold fore-color'},
            clOrdId: {id: 'clOrdId', width: 80, headerCellView: 'Ember.ClassicHeaderCell', sortKeyword: 'clOrdId', headerName: 'orderId', type: 'classicCell', firstValueStyle: 'colour-normal bold', headerStyle: 'text-left-header', cellStyle: 'text-left-header'},
            ordSts: {id: 'ordSts', width: 80, headerCellView: 'Ember.ClassicHeaderCell', sortKeyword: 'ordSts', headerName: 'status', type: 'classicStatusCell', firstValueStyle: 'colour-normal bold', headerStyle: 'text-left-header', cellStyle: 'text-left-header', title: 'txt'},
            ordSide: {id: 'ordSide', width: 45, headerCellView: 'Ember.ClassicHeaderCell', name: 'orderSide', sortKeyword: 'ordSide', headerName: 'side', type: 'classicMappingCell', headerStyle: 'text-left-header', cellStyle: 'text-left-header'},
            ordQty: {id: 'ordQty', width: 80, headerCellView: 'Ember.ClassicHeaderCell', sortKeyword: 'ordQty', headerName: 'quantity', type: 'classicCell', dataType: 'int', firstValueStyle: 'colour-normal bold'},
            price: {id: 'price', width: 60, headerCellView: 'Ember.ClassicHeaderCell', sortKeyword: 'price', headerName: 'price', type: 'orderPriceCell', secondId: 'ordTyp', dataType: 'float', firstValueStyle: 'colour-normal bold'},
            cumQty: {id: 'cumQty', width: 60, headerCellView: 'Ember.ClassicHeaderCell', sortKeyword: 'fillQty', headerName: 'filledQty', type: 'classicCell', dataType: 'int', firstValueStyle: 'colour-normal bold', isBlink: true, blinkUpStyle: 'blink-font-style-change', blinkDownStyle: 'blink-font-style-change', isDefaultValueCheck: true, defaultValue: -1},
            ordVal: {id: 'ordVal', width: 80, headerCellView: 'Ember.ClassicHeaderCell', sortKeyword: 'netOrdVal', headerName: 'orderValue', type: 'classicCell', dataType: 'float', firstValueStyle: 'colour-normal', isDefaultValueCheck: true, defaultValue: -1},
            commission: {id: 'comsn', width: 75, headerCellView: 'Ember.ClassicHeaderCell', sortKeyword: 'comsn', headerName: 'commission', type: 'classicCell', dataType: 'float', firstValueStyle: 'colour-normal', isDefaultValueCheck: true},
            vat: {id: 'vatAmount', width: 65, headerCellView: 'Ember.ClassicHeaderCell', sortKeyword: 'vatAmount', headerName: 'vat', type: 'classicCell', dataType: 'float', firstValueStyle: 'colour-normal', isDefaultValueCheck: true},
            ordTyp: {id: 'ordTyp', width: 65, headerCellView: 'Ember.ClassicHeaderCell', name: 'orderType', sortKeyword: 'ordSide', headerName: 'orderType', type: 'classicMappingCell', firstValueStyle: 'colour-normal', headerStyle: 'text-left-header', cellStyle: 'text-left-header'},
            adjustedCrdDte: {id: 'adjustedCrdDte', width: 150, headerCellView: 'Ember.ClassicHeaderCell', sortKeyword: 'crdDte', headerName: 'orderDate', type: 'classicCell', firstValueStyle: 'colour-normal', headerStyle: 'text-left-header', cellStyle: 'text-left-header'},
            pendQty: {id: 'pendQty', width: 100, headerCellView: 'Ember.ClassicHeaderCell', sortKeyword: 'pendQty', headerName: 'pendingQty', type: 'classicCell', dataType: 'int', firstValueStyle: 'colour-normal'},
            avgPrice: {id: 'avgPrice', width: 60, headerCellView: 'Ember.ClassicHeaderCell', sortKeyword: 'avgPrice', headerName: 'avgPrice', type: 'classicCell', dataType: 'float', firstValueStyle: 'colour-normal', isBlink: true, blinkUpStyle: 'blink-font-style-change', blinkDownStyle: 'blink-font-style-change', isDefaultValueCheck: true, defaultValue: -1},
            netOrdVal: {id: 'netOrdVal', width: 80, headerCellView: 'Ember.ClassicHeaderCell', sortKeyword: 'netOrdVal', headerName: 'netValue', type: 'classicCell', dataType: 'float', firstValueStyle: 'colour-normal', isDefaultValueCheck: true, defaultValue: -1},
            adjustedExpTime: {id: 'adjustedExpTime', width: 80, headerCellView: 'Ember.ClassicHeaderCell', sortKeyword: 'expTime', headerName: 'expiryDate', type: 'classicCell', firstValueStyle: 'colour-normal', headerStyle: 'text-left-header', cellStyle: 'text-left-header'},
            exg: {id: 'exg', width: 65, headerCellView: 'Ember.ClassicHeaderCell', sortKeyword: 'exg', headerName: 'exchange', type: 'classicCell', firstValueStyle: 'colour-normal', headerStyle: 'text-left-header', cellStyle: 'text-left-header'}
        },

        defaultColumnIds: ['sym', 'sDes', 'clOrdId', 'ordSts', 'ordSide', 'ordQty', 'price', 'cumQty', 'ordVal', 'ordTyp', 'adjustedCrdDte'],

        moreColumnIds: ['sDes', 'clOrdId', 'ordSts', 'ordSide', 'ordQty', 'price', 'cumQty', 'pendQty', 'avgPrice', 'ordVal', 'netOrdVal', 'ordTyp', 'tif', 'adjustedCrdDte', 'adjustedExpTime', 'exg'],

        nonRemovableColumnIds: ['sym'],

        tableParams: {
            MaxTableWidth: 5700,
            numOfFixedColumns: 2
        }
    },

    mutualFundTransaction: {
        defaultColumnMapping: {
            fndId: {id: 'fndId', width: 65, headerCellView: 'Ember.ClassicHeaderCell', sortKeyword: 'fndId', headerName: 'code', type: 'classicCell', firstValueStyle: 'symbol-fore-color bold', headerStyle: 'text-left-header', cellStyle: 'text-left-header'},
            fundName: {id: 'fundName', width: 120, headerCellView: 'Ember.ClassicHeaderCell', sortKeyword: 'fundName', headerName: 'name', type: 'classicCell', firstValueStyle: 'symbol-fore-color bold', headerStyle: 'text-left-header', cellStyle: 'text-left-header'},
            transType: {id: 'transType', width: 70, headerCellView: 'Ember.ClassicHeaderCell', name: 'mfTransType', sortKeyword: 'transType', headerName: 'type', type: 'classicMappingCell', firstValueStyle: 'symbol-fore-color bold', headerStyle: 'text-left-header', cellStyle: 'text-left-header'},
            transNo: {id: 'transNo', width: 85, headerCellView: 'Ember.ClassicHeaderCell', sortKeyword: 'transNo', headerName: 'transNo', type: 'classicCell', firstValueStyle: 'colour-normal', headerStyle: 'text-left-header', cellStyle: 'text-left-header'},
            curr: {id: 'curr', width: 60, headerCellView: 'Ember.ClassicHeaderCell', sortKeyword: 'curr', headerName: 'curr', type: 'classicCell', firstValueStyle: 'colour-normal', headerStyle: 'text-left-header', cellStyle: 'text-left-header'},
            amt: {id: 'amt', width: 80, headerCellView: 'Ember.ClassicHeaderCell', sortKeyword: 'amt', headerName: 'orderAmount', type: 'classicCell', dataType: 'float', firstValueStyle: 'colour-normal'},
            fee: {id: 'fee', width: 60, headerCellView: 'Ember.ClassicHeaderCell', sortKeyword: 'fee', headerName: 'orderFee', type: 'classicCell', dataType: 'float', firstValueStyle: 'colour-normal'},
            vat: {id: 'vatAmt', width: 60, headerCellView: 'Ember.ClassicHeaderCell', sortKeyword: 'vatAmt', headerName: 'vat', type: 'classicCell', dataType: 'float', firstValueStyle: 'colour-normal'},
            netAmount: {id: 'totAmt', width: 80, headerCellView: 'Ember.ClassicHeaderCell', sortKeyword: 'totAmt', headerName: 'netAmount', type: 'classicCell', dataType: 'float', firstValueStyle: 'colour-normal'},
            units: {id: 'units', width: 100, headerCellView: 'Ember.ClassicHeaderCell', sortKeyword: 'units', headerName: 'orderUnits', type: 'classicCell', dataType: 'float', firstValueStyle: 'colour-normal', noOfDecimalPlaces: 4},
            untPri: {id: 'untPri', width: 80, headerCellView: 'Ember.ClassicHeaderCell', sortKeyword: 'units', headerName: 'unitPrice', type: 'classicCell', dataType: 'float', firstValueStyle: 'colour-normal', noOfDecimalPlaces: 4},
            navDte: {id: 'navDte', width: 130, headerCellView: 'Ember.ClassicHeaderCell', sortKeyword: 'navDte', headerName: 'valuationDate', type: 'classicCell', dataType: 'dateTime', firstValueStyle: 'colour-normal', headerStyle: 'text-left-header', cellStyle: 'text-left-header'},
            fxRate: {id: 'fxRate', width: 60, headerCellView: 'Ember.ClassicHeaderCell', sortKeyword: 'fxRate', headerName: 'fxRate', type: 'classicCell', dataType: 'float', firstValueStyle: 'colour-normal', positiveNegativeChange: true}
        },

        defaultColumnIds: ['fndId', 'fundName', 'transType', 'transNo', 'curr', 'amt', 'fee', 'vat', 'netAmount', 'units', 'untPri', 'navDte', 'fxRate'],

        tableParams: {
            MaxTableWidth: 5700,
            numOfFixedColumns: 3
        }
    },

    mutualFundSubRedem: {
        defaultColumnMapping: {
            fndId: {id: 'fndId', width: 65, headerCellView: 'Ember.ClassicHeaderCell', sortKeyword: 'fndId', headerName: 'code', type: 'classicCell', firstValueStyle: 'symbol-fore-color bold', headerStyle: 'text-left-header', cellStyle: 'text-left-header'},
            fundName: {id: 'fundName', width: 120, headerCellView: 'Ember.ClassicHeaderCell', sortKeyword: 'fundName', headerName: 'name', type: 'classicCell', firstValueStyle: 'symbol-fore-color bold', headerStyle: 'text-left-header', cellStyle: 'text-left-header'},
            transType: {id: 'transType', width: 70, headerCellView: 'Ember.ClassicHeaderCell', name: 'mfTransType', sortKeyword: 'transType', headerName: 'type', type: 'classicMappingCell', firstValueStyle: 'symbol-fore-color bold', headerStyle: 'text-left-header', cellStyle: 'text-left-header'},
            transNo: {id: 'transNo', width: 85, headerCellView: 'Ember.ClassicHeaderCell', sortKeyword: 'transNo', headerName: 'transNo', type: 'classicCell', firstValueStyle: 'colour-normal', headerStyle: 'text-left-header', cellStyle: 'text-left-header'},
            traDte: {id: 'traDte', width: 130, headerCellView: 'Ember.ClassicHeaderCell', sortKeyword: 'traDte', headerName: 'transactionDate', type: 'classicCell', dataType: 'dateTime', firstValueStyle: 'colour-normal', headerStyle: 'text-left-header', cellStyle: 'text-left-header'},
            curr: {id: 'curr', width: 60, headerCellView: 'Ember.ClassicHeaderCell', sortKeyword: 'curr', headerName: 'curr', type: 'classicCell', firstValueStyle: 'colour-normal', headerStyle: 'text-left-header', cellStyle: 'text-left-header'},
            amt: {id: 'amt', width: 80, headerCellView: 'Ember.ClassicHeaderCell', sortKeyword: 'amt', headerName: 'orderAmount', type: 'classicCell', dataType: 'float', firstValueStyle: 'colour-normal'},
            fee: {id: 'fee', width: 60, headerCellView: 'Ember.ClassicHeaderCell', sortKeyword: 'fee', headerName: 'orderFee', type: 'classicCell', dataType: 'float', firstValueStyle: 'colour-normal'},
            vat: {id: 'vatAmt', width: 60, headerCellView: 'Ember.ClassicHeaderCell', sortKeyword: 'vatAmt', headerName: 'vat', type: 'classicCell', dataType: 'float', firstValueStyle: 'colour-normal'},
            netAmount: {id: 'totAmt', width: 80, headerCellView: 'Ember.ClassicHeaderCell', sortKeyword: 'totAmt', headerName: 'netAmount', type: 'classicCell', dataType: 'float', firstValueStyle: 'colour-normal'},
            units: {id: 'units', width: 100, headerCellView: 'Ember.ClassicHeaderCell', sortKeyword: 'units', headerName: 'orderUnits', type: 'classicCell', dataType: 'float', firstValueStyle: 'colour-normal', noOfDecimalPlaces: 4},
            transStatus: {id: 'transStatus', width: 70, headerCellView: 'Ember.ClassicHeaderCell', sortKeyword: 'transStatus', headerName: 'status', type: 'classicCell', firstValueStyle: 'bold fore-color', headerStyle: 'text-left-header', cellStyle: 'text-left-header'}
        },

        defaultColumnIds: ['fndId', 'fundName', 'transType', 'transNo', 'traDte', 'curr', 'amt', 'fee', 'vat', 'netAmount', 'units', 'transStatus'],

        tableParams: {
            MaxTableWidth: 5700,
            numOfFixedColumns: 3
        }
    },

    mutualFundList: {
        defaultColumnMapping: {
            sym: {id: 'symbol', width: 65, headerCellView: 'Ember.ClassicHeaderCell', sortKeyword: 'symbol', headerName: 'code', type: 'classicCell', firstValueStyle: 'symbol-fore-color bold', headerStyle: 'text-left-header', cellStyle: 'text-left-header'},
            fundName: {id: 'fundName', width: 120, headerCellView: 'Ember.ClassicHeaderCell', sortKeyword: 'fundName', headerName: 'name', type: 'classicLinkCell', firstValueStyle: 'symbol-fore-color bold', headerStyle: 'text-left-header', cellStyle: 'text-left-header', linkFunction: 'onAssetManagement'},
            subscription: {id: 'subscription', width: 25, headerCellView: 'Ember.ClassicHeaderCell', name: 'subscription', headerName: '', headerStyle: 'text-center', iconClass: 'glyphicon glyphicon-plus font-m top-zero', isColumnSortDisabled: true, type: 'button', title: 'subscription', buttonFunction: 'onSubscription', buttonStyle: 'btn btn-buy-small btn-animation mf-btn-icon'},
            redemption: {id: 'redemption', width: 25, headerCellView: 'Ember.ClassicHeaderCell', name: 'redemption', headerName: '', headerStyle: 'text-center', iconClass: 'glyphicon glyphicon-minus font-m top-zero', isColumnSortDisabled: true, type: 'button', title: 'redemption', buttonFunction: 'onRedemption', buttonStyle: 'btn btn-sell-small btn-animation mf-btn-icon'},
            curr: {id: 'curr', width: 60, headerCellView: 'Ember.ClassicHeaderCell', sortKeyword: 'curr', headerName: 'curr', type: 'classicCell', firstValueStyle: 'colour-normal', headerStyle: 'text-left-header', cellStyle: 'text-left-header'},
            untPri: {id: 'untPri', width: 100, headerCellView: 'Ember.ClassicHeaderCell', sortKeyword: 'untPri', headerName: 'unitPrice', type: 'classicCell', dataType: 'float', firstValueStyle: 'colour-normal', noOfDecimalPlaces: 4},
            fndNav: {id: 'fndNav', width: 120, headerCellView: 'Ember.ClassicHeaderCell', sortKeyword: 'fndNav', headerName: 'fundNAV', type: 'classicCell', dataType: 'float', firstValueStyle: 'colour-normal', noOfDecimalPlaces: 4},
            nextNavDte: {id: 'nextNavDte', width: 90, headerCellView: 'Ember.ClassicHeaderCell', sortKeyword: 'nextNavDte', headerName: 'nxtNavDate', type: 'classicCell', dataType: 'date', firstValueStyle: 'colour-normal'},
            perYtd: {id: 'perYtd', width: 85, headerCellView: 'Ember.ClassicHeaderCell', sortKeyword: 'perYtd', headerName: 'performYTD', type: 'classicCell', dataType: 'float', firstValueStyle: 'colour-normal', positiveNegativeChange: true}
        },

        defaultColumnIds: ['sym', 'fundName', 'subscription', 'redemption', 'curr', 'untPri', 'fndNav', 'nextNavDte', 'perYtd'],

        tableParams: {
            MaxTableWidth: 5700,
            numOfFixedColumns: 4
        }
    },

    mutualFundHolding: {
        defaultColumnMapping: {
            sym: {id: 'symbol', width: 65, headerCellView: 'Ember.ClassicHeaderCell', sortKeyword: 'symbol', headerName: 'code', type: 'classicCell', firstValueStyle: 'symbol-fore-color bold', headerStyle: 'text-left-header', cellStyle: 'text-left-header'},
            fundName: {id: 'fundName', width: 120, headerCellView: 'Ember.ClassicHeaderCell', sortKeyword: 'fundName', headerName: 'name', type: 'classicLinkCell', firstValueStyle: 'symbol-fore-color bold', headerStyle: 'text-left-header', cellStyle: 'text-left-header', linkFunction: 'onAssetManagement'},
            subscription: {id: 'isSubRedEnabled', width: 25, headerCellView: 'Ember.ClassicHeaderCell', name: 'subscription', headerName: '', headerStyle: 'text-center', iconClass: 'glyphicon glyphicon-plus font-m top-zero', isColumnSortDisabled: true, type: 'button', title: 'subscription', buttonFunction: 'onSubscription', buttonStyle: 'btn btn-buy-small btn-animation mf-btn-icon'},
            redemption: {id: 'isSubRedEnabled', width: 25, headerCellView: 'Ember.ClassicHeaderCell', name: 'redemption', headerName: '', headerStyle: 'text-center', iconClass: 'glyphicon glyphicon-minus font-m top-zero', isColumnSortDisabled: true, type: 'button', title: 'redemption', buttonFunction: 'onRedemption', buttonStyle: 'btn btn-sell-small btn-animation mf-btn-icon'},
            curr: {id: 'curr', width: 60, headerCellView: 'Ember.ClassicHeaderCell', sortKeyword: 'curr', headerName: 'curr', type: 'classicCell', firstValueStyle: 'colour-normal', headerStyle: 'text-left-header', cellStyle: 'text-left-header'},
            avgCst: {id: 'avgCst', width: 85, headerCellView: 'Ember.ClassicHeaderCell', sortKeyword: 'avgCst', headerName: 'avgPrice', type: 'classicCell', dataType: 'float', firstValueStyle: 'colour-normal', noOfDecimalPlaces: 4},
            untPri: {id: 'untPri', width: 100, headerCellView: 'Ember.ClassicHeaderCell', sortKeyword: 'untPri', headerName: 'unitPrice', type: 'classicCell', dataType: 'float', firstValueStyle: 'colour-normal', noOfDecimalPlaces: 4},
            qty: {id: 'qty', width: 100, headerCellView: 'Ember.ClassicHeaderCell', sortKeyword: 'qty', headerName: 'units', type: 'classicCell', dataType: 'float', firstValueStyle: 'colour-normal', noOfDecimalPlaces: 4},
            profLoss: {id: 'profLoss', width: 85, headerCellView: 'Ember.ClassicHeaderCell', sortKeyword: 'profLoss', headerName: 'gainLossRatio', type: 'classicCell', dataType: 'float', firstValueStyle: 'colour-normal', positiveNegativeChange: true},
            mktVal: {id: 'unitVal', width: 105, headerCellView: 'Ember.ClassicHeaderCell', sortKeyword: 'unitVal', headerName: 'value', type: 'classicCell', dataType: 'float', firstValueStyle: 'colour-normal', noOfDecimalPlaces: 4},
            pledged: {id: 'pledged', width: 70, headerCellView: 'Ember.ClassicHeaderCell', sortKeyword: 'pledged', headerName: 'pledgeUnits', type: 'classicCell', dataType: 'float', firstValueStyle: 'colour-normal', noOfDecimalPlaces: 4},
            avaiQty: {id: 'avaiQty', width: 100, headerCellView: 'Ember.ClassicHeaderCell', sortKeyword: 'avaiQty', headerName: 'availUnits', type: 'classicCell', dataType: 'float', firstValueStyle: 'colour-normal', noOfDecimalPlaces: 4},
            fxRate: {id: 'fxRate', width: 60, headerCellView: 'Ember.ClassicHeaderCell', sortKeyword: 'fxRate', headerName: 'fxRate', type: 'classicCell', dataType: 'float', firstValueStyle: 'colour-normal', positiveNegativeChange: true}
        },

        defaultColumnIds: ['sym', 'fundName', 'subscription', 'redemption', 'curr', 'avgCst', 'untPri', 'qty', 'profLoss', 'mktVal', 'pledged', 'avaiQty', 'fxRate'],

        tableParams: {
            MaxTableWidth: 5700,
            numOfFixedColumns: 4
        }
    },

    cashStatement: {
        defaultColumnMapping: {     // Column Object parameters : id, width, headerCellView, headerName, sortKeyword,  cellStyle, sortDisable, dataType, type, headerStyle, cellStyle
            date: {id: 'date', width: 80, headerCellView: 'Ember.ClassicHeaderCell', sortKeyword: 'Date', headerName: 'date', type: 'classicCell', dataType: 'dateTime', headerStyle: 'text-left-header', cellStyle: 'text-left-header'},
            trnsType: {id: 'trnsType', width: 95, headerCellView: 'Ember.ClassicHeaderCell', sortKeyword: 'trnsType', headerName: 'transactionType', type: 'classicCell', headerStyle: 'text-left-header', cellStyle: 'text-left-header'},
            trnsRef: {id: 'trnsRef', width: 110, headerCellView: 'Ember.ClassicHeaderCell', sortKeyword: 'tnsRef', headerName: 'transNo', type: 'classicCell', headerStyle: 'text-left-header', cellStyle: 'text-left-header'},
            particulars: {id: 'particulars', width: 150, headerCellView: 'Ember.ClassicHeaderCell', sortKeyword: 'particulars', headerName: 'description', type: 'classicCell', headerStyle: 'text-left-header', cellStyle: 'text-left-header'},
            amount: {id: 'amount', width: 100, headerCellView: 'Ember.ClassicHeaderCell', sortKeyword: 'amount', headerName: 'amount', type: 'dualChange', dataType: 'float', positiveNegativeChange: true, headerStyle: 'text-center', cellStyle: 'text-left-header'},
            commission: {id: 'comsn', width: 75, headerCellView: 'Ember.ClassicHeaderCell', sortKeyword: 'comsn', headerName: 'commission', type: 'dualChange', dataType: 'float', positiveNegativeChange: true, headerStyle: 'text-center', cellStyle: 'text-left-header'},
            vat: {id: 'vatAmount', width: 65, headerCellView: 'Ember.ClassicHeaderCell', sortKeyword: 'vatAmount', headerName: 'vat', type: 'dualChange', dataType: 'float', positiveNegativeChange: true, headerStyle: 'text-center', cellStyle: 'text-left-header'},
            balance: {id: 'balance', width: 100, headerCellView: 'Ember.ClassicHeaderCell', sortKeyword: 'balance', headerName: 'availBalance', type: 'classicCell', dataType: 'float', headerStyle: 'text-center', cellStyle: 'text-center'},
            settleDate: {id: 'settleDate', width: 80, headerCellView: 'Ember.ClassicHeaderCell', sortKeyword: 'settleDate', headerName: 'settlementDate', type: 'classicCell', dataType: 'dateTime', headerStyle: 'text-center', cellStyle: 'text-center'},
            sDes: {id: 'des', width: 125, headerCellView: 'Ember.ClassicHeaderCell', sortKeyword: '', headerName: 'description', type: 'classicCell', firstValueStyle: 'colour-normal font-l', headerStyle: 'text-left-header', cellStyle: 'text-left-header'}
        },

        defaultColumnIds: ['date', 'trnsType', 'trnsRef', 'particulars', 'amount', 'balance', 'settleDate'],

        tableParams: {
            MaxTableWidth: 5700
        }
    },

    accountSummary: {
        accountSummaryFields: [     // Column Object parameters : id, width, headerCellView, headerName, sortKeyword,  cellStyle, sortDisable, dataType, type, headerStyle, cellStyle
            {lanKey: 'cashBalance', dataField: 'balance', formatter: 'C', style: ''},
            {lanKey: 'availCashBalance', dataField: 'cashAmt', formatter: 'C', style: ''},
            {lanKey: 'blockedAndOutstanding', dataField: 'blkAmt', formatter: 'C', style: ''},
            {lanKey: 'cashAvailableForWidth', dataField: 'cashForWith', formatter: 'C', style: ''},
            {lanKey: 'unsettledSales', dataField: 'penSet', formatter: 'C', style: ''},
            {lanKey: 'payableAmount', dataField: 'payAmt', formatter: 'C', style: ''},
            {lanKey: 'odLimit', dataField: 'odLmt', formatter: 'C', style: ''},
            {lanKey: 'portfolioValue', dataField: 'valuation', formatter: 'C', style: ''},
            {lanKey: 'totalPortfolio', dataField: 'totVal', formatter: 'C', style: ''}
        ]
    },

    stockStatement: {
        defaultColumnMapping: {     // Column Object parameters : id, width, headerCellView, headerName, sortKeyword,  cellStyle, sortDisable, dataType, type, headerStyle, cellStyle
            sym: {id: 'sym', width: 60, headerCellView: 'Ember.ClassicHeaderCell', sortKeyword: '', headerName: 'symbol', type: 'classicCell', headerStyle: 'text-left-header', cellStyle: 'text-left-header', firstValueStyle: 'symbol-fore-color bold'},
            exg: {id: 'exg', width: 65, headerCellView: 'Ember.ClassicHeaderCell', sortKeyword: 'exg', headerName: 'exchange', type: 'classicCell', firstValueStyle: 'fore-color bold', headerStyle: 'text-left-header', cellStyle: 'text-left-header'},
            sDes: {id: 'symbolInfo.sDes', width: 130, headerCellView: 'Ember.ClassicHeaderCell', sortKeyword: 'sDescription', headerName: 'sDescription', type: 'classicCell', firstValueStyle: 'bold', headerStyle: 'text-left-header', cellStyle: 'text-left-header'},
            instDes: {id: 'symbolInfo.instDes', width: 85, headerCellView: 'Ember.ClassicHeaderCell', headerName: 'symbolType', type: 'classicCell', sortKeyword: 'inst', cellStyle: 'text-left-header', firstValueStyle: 'fore-color bold'},
            date: {id: 'date', width: 90, headerCellView: 'Ember.ClassicHeaderCell', sortKeyword: '', headerName: 'transDate', type: 'classicCell', dataType: 'date', headerStyle: 'text-left-header', cellStyle: 'text-left-header'},
            des: {id: 'des', width: 200, headerCellView: 'Ember.ClassicHeaderCell', sortKeyword: '', headerName: 'description', type: 'holdingDesCell', headerStyle: 'text-left-header', cellStyle: 'text-left-header'},
            trnsRef: {id: 'trnsRef', width: 95, headerCellView: 'Ember.ClassicHeaderCell', sortKeyword: 'tranRef', headerName: 'refId', type: 'classicCell', headerStyle: 'text-left-header', cellStyle: 'text-left-header'},
            qty: {id: 'qty', width: 90, headerName: 'quantity', type: 'classicCell', sortKeyword: '', firstValueStyle: 'fore-color bold', dataType: 'int'},
            avgPrice: {id: 'avgPrice', width: 100, headerCellView: 'Ember.ClassicHeaderCell', sortKeyword: 'amount', headerName: 'averagePrice', type: 'classicCell', dataType: 'float', headerStyle: 'text-center'}
        },

        defaultColumnIds: ['sym', 'exg', 'sDes', 'instDes', 'date', 'des', 'trnsRef', 'qty', 'avgPrice'],

        tableParams: {
            MaxTableWidth: 5700
        }
    },

    cashTransferList: {
        defaultColumnMapping: {     // Column Object parameters : id, width, headerName, sortKeyword, multiValueIds, cellStyle, sortDisable, firstValueStyle, isBold, dataType, backgroundColour,
            typ: {id: 'typ', width: 85, headerCellView: 'Ember.ClassicHeaderCell', name: 'type', headerName: 'type', headerStyle: 'text-center', isColumnSortDisabled: true, type: 'button', title: 'amend'},
            mtd: {id: 'mtd', width: 60, headerCellView: 'Ember.ClassicHeaderCell', headerName: 'method', headerStyle: 'text-center', isColumnSortDisabled: true, type: 'button', title: 'cancel'},
            curr: {id: 'curr', width: 60, headerName: 'curr', headerStyle: 'text-left-header', cellStyle: 'text-center-header', sortKeyword: 'curr', type: 'classic', headerCellView: 'Ember.ClassicHeaderCell'},
            Amount: {id: 'Amount', width: 60, headerCellView: 'Ember.ClassicHeaderCell', sortKeyword: 'amnt', headerName: 'amount', type: 'classicCell', firstValueStyle: 'colour-normal bold', headerStyle: 'text-left-header', cellStyle: 'text-left-header'},
            exgAcc: {id: 'exgAcc', width: 110, headerCellView: 'Ember.ClassicHeaderCell', sortKeyword: 'secAccNum', headerName: 'Exchange AC', type: 'classicCell', firstValueStyle: 'colour-normal bold', headerStyle: 'text-left-header', cellStyle: 'text-left-header'},
            sts: {id: 'sts', width: 85, headerCellView: 'Ember.ClassicHeaderCell', sortKeyword: 'sts', headerName: 'status', type: 'classicStatusCell', firstValueStyle: 'colour-normal bold', headerStyle: 'text-left-header', cellStyle: 'text-left-header'},
            refId: {id: 'refId', width: 95, headerCellView: 'Ember.ClassicHeaderCell', sortKeyword: 'refId', headerName: 'refId', type: 'classicMappingCell', firstValueStyle: 'colour-normal bold', headerStyle: 'text-left-header', cellStyle: 'text-left-header'},
            bankAcnt: {id: 'bankAcnt', width: 110, headerCellView: 'Ember.ClassicHeaderCell', sortKeyword: 'bankAcnt', headerName: 'bankAccount', type: 'classicCell', dataType: 'int', firstValueStyle: 'colour-normal bold'},
            valDte: {id: 'valDte', width: 50, headerCellView: 'Ember.ClassicHeaderCell', sortKeyword: 'valDte', headerName: 'date', type: 'classicCell', dataType: 'float', firstValueStyle: 'colour-normal bold'}
        },

        defaultColumnIds: ['typ', 'mtd', 'curr', 'Amount', 'exgAcc', 'sts', 'refId', 'bankAcnt', 'valDte'],

        tableParams: {
            MaxTableWidth: 5700
        }
    },

    cashTrList: {
        defaultColumnMapping: {     // Column Object parameters : id, width, headerCellView, headerName, sortKeyword,  cellStyle, sortDisable, dataType, type, headerStyle, cellStyle
            typ: {id: 'typ', width: 80, headerCellView: 'Ember.ClassicHeaderCell', sortKeyword: '', headerName: 'type', type: 'classicCell', headerStyle: 'text-left-header', cellStyle: 'text-left-header'},
            mtd: {id: 'mtd', width: 80, headerCellView: 'Ember.ClassicHeaderCell', sortKeyword: '', headerName: 'method', type: 'classicCell', headerStyle: 'text-left-header', cellStyle: 'text-left-header'},
            curr: {id: 'curr', width: 100, headerCellView: 'Ember.ClassicHeaderCell', sortKeyword: '', headerName: 'curr', type: 'classicCell', dataType: 'dateTime', headerStyle: 'text-left-header', cellStyle: 'text-left-header'},
            Amount: {id: 'Amount', width: 50, headerCellView: 'Ember.ClassicHeaderCell', sortKeyword: '', headerName: 'amount', type: 'classicCell', headerStyle: 'text-left-header', cellStyle: 'text-left-header'},
            exgAcc: {id: 'exgAcc', width: 105, headerCellView: 'Ember.ClassicHeaderCell', sortKeyword: '', headerName: 'ExgAC', type: 'classicCell', headerStyle: 'text-left-header', cellStyle: 'text-left-header'},
            sts: {id: 'sts', width: 80, headerCellView: 'Ember.ClassicHeaderCell', sortKeyword: '', headerName: 'status', type: 'classicCell', headerStyle: 'text-left-header', cellStyle: 'text-left-header'},
            refId: {id: 'refId', width: 90, headerCellView: 'Ember.ClassicHeaderCell', sortKeyword: '', headerName: 'refId', type: 'classicCell', headerStyle: 'text-left-header', cellStyle: 'text-left-header'},
            bankAcnt: {id: 'bankAcnt', width: 100, headerCellView: 'Ember.ClassicHeaderCell', sortKeyword: '', headerName: 'bankAccount', type: 'classicCell', headerStyle: 'text-left-header', cellStyle: 'text-left-header'},
            valDte: {id: 'valDte', width: 60, headerCellView: 'Ember.ClassicHeaderCell', sortKeyword: '', headerName: 'Date', type: 'classicCell', headerStyle: 'text-left-header', cellStyle: 'text-left-header'}
        },

        defaultColumnIds: ['typ', 'mtd', 'curr', 'Amount', 'exgAcc', 'sts', 'refId', 'bankAcnt', 'valDte'],

        tableParams: {
            MaxTableWidth: 5700
        }
    },

    rightsSubscriptionList: {
        defaultColumnMapping: {     // Column Object parameters : id, width, headerCellView, headerName, sortKeyword,  cellStyle, sortDisable, dataType, type, headerStyle, cellStyle
            sym: {id: 'sym', width: 60, headerCellView: 'Ember.ClassicHeaderCell', sortKeyword: '', headerName: 'symbol', type: 'classicCell', headerStyle: 'text-left-header', cellStyle: 'text-left-header', firstValueStyle: 'symbol-fore-color bold'},
            dSym: {id: 'dSym', width: 80, headerCellView: 'Ember.ClassicHeaderCell', sortKeyword: '', headerName: 'sDescription', type: 'classicCell', firstValueStyle: 'symbol-fore-color bold', headerStyle: 'text-left-header', cellStyle: 'text-left-header'},
            statusLabel: {id: 'statusLabel', width: 110, headerCellView: 'Ember.ClassicHeaderCell', sortKeyword: '', headerName: 'status', headerStyle: 'text-left-header', cellStyle: 'text-left-header', type: 'classicCell', firstValueStyle: 'bold fore-color'},
            qty: {id: 'qty', width: 60, headerCellView: 'Ember.ClassicHeaderCell', sortKeyword: '', headerName: 'quantity', type: 'classicCell', dataType: 'int', firstValueStyle: 'colour-normal bold'},
            avgPrice: {id: 'avgPrice', width: 50, headerCellView: 'Ember.ClassicHeaderCell', sortKeyword: '', headerName: 'avgPrice', type: 'classicCell', dataType: 'float', firstValueStyle: 'colour-normal bold', headerStyle: 'text-left-header', cellStyle: 'text-left-header'},
            netValue: {id: 'netValue', width: 80, headerCellView: 'Ember.ClassicHeaderCell', sortKeyword: '', headerName: 'netValue', type: 'classicCell', dataType: 'float', firstValueStyle: 'colour-normal', headerStyle: 'text-left-header', cellStyle: 'text-left-header'},
            subNo: {id: 'subNo', width: 110, headerCellView: 'Ember.ClassicHeaderCell', sortKeyword: '', headerName: 'refId', type: 'classicCell', firstValueStyle: 'colour-normal', headerStyle: 'text-left-header', cellStyle: 'text-left-header'},
            subDate: {id: 'subDate', width: 80, headerCellView: 'Ember.ClassicHeaderCell', sortKeyword: '', headerName: 'date', type: 'classicCell', dataType: 'date', firstValueStyle: 'colour-normal', headerStyle: 'text-left-header', cellStyle: 'text-left-header'}
        },

        defaultColumnIds: ['sym', 'dSym', 'statusLabel', 'qty', 'avgPrice', 'netValue', 'subNo', 'subDate'],

        tableParams: {
            MaxTableWidth: 5700
        }
    },

    orderDetails: {
        orderDetailsPopup: [
            {lanKey: 'exgOrderNo', dataField: 'ordId', formatter: 'S', style: ''},
            {lanKey: 'orderId', dataField: 'clOrdId', formatter: 'S', style: ''},
            {lanKey: 'side', dataField: 'ordSide', formatter: 'S', style: '', lanKeyAppend: 'orderSide', isCustomStyle: true},
            {lanKey: 'status', dataField: 'ordSts', formatter: 'S', style: '', lanKeyAppend: 'orderStatus', isCustomStyle: true},
            {lanKey: 'quantity', dataField: 'ordQty', formatter: 'I', style: ''},
            {lanKey: 'pendingQty', dataField: 'pendQty', formatter: 'I', style: ''},
            {lanKey: 'filledQuantity', dataField: 'cumQty', formatter: 'I', style: ''},
            {lanKey: 'orderType', dataField: 'ordTyp', formatter: 'S', style: '', lanKeyAppend: 'orderType'},
            {lanKey: 'price', dataField: 'price', formatter: 'C', style: ''},
            {lanKey: 'stopPriceType', dataField: 'stPrcTyp', formatter: 'I', style: ''},
            {lanKey: 'stopPrice', dataField: 'stPrc', formatter: 'I', style: ''},
            {lanKey: 'averagePrice', dataField: 'avgPrice', formatter: 'C', style: ''},
            {lanKey: 'date', dataField: 'adjustedCrdDte', formatter: 'S', style: ''},
            {lanKey: 'expiryDate', dataField: 'adjustedExpTime', formatter: 'S', style: ''},
            {lanKey: 'maximumPrice', dataField: 'maxPrc', formatter: 'C', style: ''},
            {lanKey: 'disclosedQty', dataField: 'disQty', formatter: 'I', style: ''},
            {lanKey: 'symbolType', dataField: 'instruTyp', formatter: 'S', style: '', isAssetType: true},
            {lanKey: 'tif', dataField: 'tif', formatter: 'S', style: '', lanKeyAppend: 'tifType'},
            {lanKey: 'commission', dataField: 'comsn', formatter: 'C', style: ''},
            {lanKey: 'netValue', dataField: 'netOrdVal', formatter: 'I', style: ''},
            {lanKey: 'orderValue', dataField: 'ordVal', formatter: 'C', style: ''}
        ],

        tableParams: {
            MaxTableWidth: 5700,
            numOfFixedColumns: 3
        }
    }
};
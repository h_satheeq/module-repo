export default {
    appConfig: {
        customisation: {
            authenticationMode: 4,
            isTradingEnabled: true,
            profileServiceEnabled: false,
            isPasswordChangeEnable: true,
            hashType: 'MD5'
        }
    },

    tradeSettings: {
        channelId: 22
    }
};

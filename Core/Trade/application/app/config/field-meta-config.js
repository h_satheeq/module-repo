export default {
    multiFactors: {
        KSE: {
            multiFactor: 0.001,
            decimalPlaces: 4
        }
    }
};
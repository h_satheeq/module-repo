export default {
    channelId: -1,

    connectionParameters: {
        primary: {
            ip: '123.231.48.7',
            port: '8800',
            secure: false // Is connection secure - true or false
        },

        secondary: {
            ip: '123.231.48.7',
            port: '8800',
            secure: false // Is connection secure - true or false
        }
    },

    fieldConfigs: {
        portfolioDes: 'tradingAccName'
    },

    urlTypes: {
        brokerage: 'brokerage'
    },

    orderConfig: {
        isValidateHoldings: false
    },

    tradingDisabledInstTypes: [7, 14]
};

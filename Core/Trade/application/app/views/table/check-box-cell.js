import TableCell from 'ember-table/views/table-cell';

export default TableCell.extend({
    templateName: 'table/views/check-box-cell'
});
import ClassicCell from '../classic-cell';
import languageDataStore from '../../../models/shared/language/language-data-store';
import utils from '../../../utils/utils';

export default ClassicCell.extend({
    templateName: 'table/views/classic-cell',
    app: languageDataStore.getLanguageObj(),

    formattedFirstValue: function () {
        var labels = this.get('app').lang.labels;
        var currentValue = this.get('cellContent') ? this.get('cellContent').firstValue : '';
        var labelKey = this.get('column') && this.get('column').name && utils.validators.isAvailable(currentValue) ? [this.get('column').name, currentValue].join('_') : undefined;

        return labelKey && labels[labelKey] ? labels[labelKey] : currentValue;
    }.property('cellContent', 'app.lang'),

    styleFirstValue: function () {
        var currentValue = this.get('cellContent') ? this.get('cellContent').firstValue : '';
        var styleKey = this.get('column') && this.get('column').name && utils.validators.isAvailable(currentValue) ? [this.get('column').name, currentValue].join('_') : '';

        return [this.get('column.firstValueStyle'), styleKey].join(' ');
    }.property('cellContent')
});
import ClassicCell from '../classic-cell';
import languageDataStore from '../../../models/shared/language/language-data-store';

export default ClassicCell.extend({
    templateName: 'table/views/trade/pay-method-cell',
    app: languageDataStore.getLanguageObj(),

    formattedFirstValue: function () {
        var labels = this.get('app').lang.labels;
        var currentValue = this.get('cellContent') ? this.get('cellContent').firstValue : '';

        return labels['payMtd_' + currentValue] ? labels['payMtd_' + currentValue] : currentValue;
    }.property('cellContent', 'app.lang')
});
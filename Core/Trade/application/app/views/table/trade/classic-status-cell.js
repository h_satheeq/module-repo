import ClassicCell from '../classic-cell';
import languageDataStore from '../../../models/shared/language/language-data-store';
import tradeConstants from '../../../models/trade/trade-constants';

export default ClassicCell.extend({
    templateName: 'table/views/trade/classic-status-cell',
    app: languageDataStore.getLanguageObj(),

    formattedFirstValue: function () {
        var labels = this.get('app').lang.labels;
        var currentValue = this.get('cellContent') ? this.get('cellContent').firstValue : '';

        return currentValue && labels['orderStatus_' + currentValue] ? labels['orderStatus_' + currentValue] : currentValue;
    }.property('cellContent', 'app.lang'),

    title: function () {
        var currentValue = this.get('cellContent') ? this.get('cellContent').firstValue : '';
        var content, titleKey;

        if (currentValue && currentValue === tradeConstants.OrderStatus.Rejected) {
            content = this.getProperties('row') && this.getProperties('row').row && this.getProperties('row').row.content ? this.getProperties('row').row.content : '';
            titleKey = this.get('column') && this.get('column').title ? this.get('column').title : '';
        }

        this.set('styleFirstValue', 'bold orderStatus_' + currentValue);
        return content && titleKey && content.get(titleKey) ? content.get(titleKey) : this.get('formattedFirstValue');
    }.property('cellContent.firstValue')
});
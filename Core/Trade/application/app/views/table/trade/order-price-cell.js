import ClassicCell from '../classic-cell';
import languageDataStore from '../../../models/shared/language/language-data-store';
import tradeConstants from '../../../models/trade/trade-constants';

export default ClassicCell.extend({
    templateName: 'table/views/classic-cell',
    app: languageDataStore.getLanguageObj(),

    formattedFirstValue: function () {
        var ordTyp = this.get('cellContent') ? this.get('cellContent').secondValue : '';

        if (ordTyp && ordTyp === tradeConstants.OrderType.Market) {
            return this.get('app').lang.labels.market;
        } else {
            return this.addFormat(this.get('cellContent') ? this.get('cellContent').firstValue : undefined);
        }
    }.property('cellContent', 'app.lang')
});
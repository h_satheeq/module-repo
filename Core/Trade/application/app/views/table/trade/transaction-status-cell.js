import ClassicCell from '../classic-cell';
import languageDataStore from '../../../models/shared/language/language-data-store';
import tradeConstants from '../../../models/trade/trade-constants';
import Ember from 'ember';

export default ClassicCell.extend({
    templateName: 'table/views/trade/transaction-status-cell',
    app: languageDataStore.getLanguageObj(),

    formattedFirstValue: function () {
        var labels = this.get('app').lang.labels;
        var currentValue = this.get('cellContent') ? this.get('cellContent').firstValue : '';

        return currentValue && labels['transStatus_' + currentValue] ? labels['transStatus_' + currentValue] : currentValue;
    }.property('cellContent', 'app.lang'),

    styleFirstValue: Ember.computed(function () {
        var currentValue = this.get('cellContent') ? this.get('cellContent').firstValue : '';
        var styleVal;

        switch (currentValue) {
            case tradeConstants.TransactionStatus.Approved:
                styleVal = 'up-fore-color';
                break;

            case tradeConstants.TransactionStatus.Rejected:
                styleVal = 'down-fore-color';
                break;

            case tradeConstants.TransactionStatus.Cancelled:
                styleVal = 'down-fore-color';
                break;

            case tradeConstants.TransactionStatus.Validated:
                styleVal = 'highlight-fore-color';
                break;

            case tradeConstants.TransactionStatus.L1Approved:
                styleVal = 'highlight-fore-color';
                break;

            case tradeConstants.TransactionStatus.L2Approved:
                styleVal = 'highlight-fore-color';
                break;
        }

        return 'bold ' + styleVal;
    }).property('cellContent')
});
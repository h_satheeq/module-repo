import ClassicCell from '../classic-cell';
import languageDataStore from '../../../models/shared/language/language-data-store';
import tradeConstants from '../../../models/trade/trade-constants';
import Ember from 'ember';

export default ClassicCell.extend({
    templateName: 'table/views/trade/transaction-type-cell',
    app: languageDataStore.getLanguageObj(),

    formattedFirstValue: function () {
        var labels = this.get('app').lang.labels;
        var currentValue = this.get('cellContent') ? this.get('cellContent').firstValue : '';

        return labels['transTyp_' + currentValue] ? labels['transTyp_' + currentValue] : currentValue;
    }.property('cellContent', 'app.lang'),

    styleFirstValue: Ember.computed(function () {
        var currentValue = this.get('cellContent') ? this.get('cellContent').firstValue : '';
        var styleVal;

        switch (currentValue) {
            case tradeConstants.TransactionTypes.Deposit:
                styleVal = 'up-fore-color';
                break;
            case tradeConstants.TransactionTypes.Withdrawal:
                styleVal = 'down-fore-color';
                break;
            case tradeConstants.TransactionTypes.Transfer:
                styleVal = 'highlight-fore-color';
                break;
        }

        return 'bold ' + styleVal;
    }).property('cellContent')
});
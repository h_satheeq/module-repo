import ClassicCell from '../classic-cell';
import utils from '../../../utils/utils';
import sharedService from '../../../models/shared/shared-service';
import languageDataStore from '../../../models/shared/language/language-data-store';

export default ClassicCell.extend({
    templateName: 'table/views/trade/holding-statement-des-cell',
    app: languageDataStore.getLanguageObj(),

    formattedFirstValue: function () {
        var currentValue = this.get('cellContent') ? this.get('cellContent').firstValue : '';
        currentValue = utils.formatters.convertUnicodeToNativeString(currentValue);

        var desArray = currentValue.split('|');
        var currLang = sharedService.userSettings.currentLanguage;

        switch (currLang) {
            case 'EN':
                currentValue = desArray[0];
                break;
            case 'AR':
                currentValue = desArray[1];
                break;
        }

        return currentValue;
    }.property('cellContent', 'app.lang')
});
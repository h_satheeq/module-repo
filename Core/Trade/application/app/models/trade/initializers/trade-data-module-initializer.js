import Ember from 'ember';
import BaseModuleInitializer from '../../../models/shared/initializers/base-module-initializer';
import sharedDataModuleInitializer from '../../../models/shared/initializers/shared-data-module-initializer';
import tradeRetailAuthenticator from '../../../controllers/authentication/trade-retail-authenticator';
import tradeSsoAuthenticator from '../../../controllers/authentication/trade-sso-authenticator';
import languageDataStore from '../../../models/shared/language/language-data-store';
import TradeService from '../../../models/trade/trade-service';
import sharedService from '../../../models/shared/shared-service';
import utils from '../../../utils/utils';
import tradeSettings from '../../../config/trade-settings';
import tradeWidgetConfig from '../../../config/trade-widget-config';
import extendedSettingsL1 from '../../../config/extended-settings-level-1';
import extendedSettingsL2 from '../../../config/extended-settings-level-2';
import tradeConstants from '../../../models/trade/trade-constants';
import appConfig from '../../../config/app-config';
import authenticationConstants from '../../../controllers/authentication/authentication-constants';
import environment from 'universal-app/config/environment';

export default BaseModuleInitializer.extend({
    preInitialize: function () {
        // Intermediate level settings
        utils.configHelper.mergeConfigSettings(tradeSettings, extendedSettingsL1.tradeSettings);
        utils.configHelper.mergeConfigSettings(tradeWidgetConfig, extendedSettingsL1.tradeWidgetConfig);
        // Customization level settings
        utils.configHelper.mergeConfigSettings(tradeSettings, extendedSettingsL2.tradeSettings);
        utils.configHelper.mergeConfigSettings(tradeWidgetConfig, extendedSettingsL2.tradeWidgetConfig);

        appConfig.customisation.isTradingEnabled = true;
        var service;

        if (!Ember.appGlobal.multiScreen.isParentWindow) { // if Child window of DT multiple window scenario
            var parentWindow = window.opener;
            service = parentWindow.Ember.appGlobal.multiScreen.parentSharedService.getService('trade');
        } else {
            service = this.createService();
        }
        this.set('tradeService', service);

        sharedService.registerService(service.subscriptionKey, service);
        service.createDataStores();

        this._loadConfigs();
        this._setAuthParams();
        this._setConnectionSettings();
        this._loadTradeMeta();
    },

    createService: function () {
        return TradeService.create();
    },

    _setAuthParams: function () {
        switch (appConfig.customisation.authenticationMode) {
            case authenticationConstants.AuthModes.TradeRetailPriceSso:
                sharedDataModuleInitializer.authType = sharedDataModuleInitializer.authTypes.retail;
                sharedDataModuleInitializer.authController = tradeRetailAuthenticator;
                break;

            case authenticationConstants.AuthModes.TradeSsoPriceSso:
                sharedDataModuleInitializer.authType = sharedDataModuleInitializer.authTypes.sso;
                sharedDataModuleInitializer.authController = tradeSsoAuthenticator;
                break;

            default:
                break;
        }
    },

    _setConnectionSettings: function () {
        var settings = environment.APP.isTestMode ? environment.APP.tradeConnectionParameters.primary : tradeSettings.connectionParameters.primary;

        var connectionSettings = {
            ip: settings.ip,
            port: settings.port,
            secure: settings.secure,
            reconnectInterval: tradeConstants.Pulse.ReconnectionTimeInterval,
            maxRetryCount: tradeConstants.Pulse.MaxRetryCount,
            enablePulse: true
        };

        this.tradeService.webSocketManager.setConnectionSettings(connectionSettings);
    },

    _loadTradeMeta: function () {
        this.tradeService.tradeMetaDS.initialize(languageDataStore.getLanguageObj());
        this.tradeService.tradeMetaDS.tradeMeta.load();
        this.tradeService.tradeMetaDS.loadRuleVariables();

        this.tradeService.set('fieldConfigs', tradeSettings.fieldConfigs);
    },

    _loadConfigs: function () {
        this.tradeService.set('constants', tradeConstants);
        this.tradeService.set('settings', tradeSettings);
    }
});

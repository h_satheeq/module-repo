import Ember from 'ember';
import utils from '../../../../utils/utils';
import sharedService from '../../../../models/shared/shared-service';
import tradeConstants from '../../trade-constants';
import environmentConfig from '../../../../config/environment';
import tradeSettings from '../../../../config/trade-settings';

export default (function () {
    var reqGroup = tradeConstants.Request.Group;
    var reqType = tradeConstants.Request.Type;

    // Authentication related requests
    var generateRetailAuthRequest = function (authParams, isReconnection) {
        var datElements = [];

        datElements[datElements.length] = '{"lgnNme":"';
        datElements[datElements.length] = authParams.username;
        datElements[datElements.length] = '","pwd":"';
        datElements[datElements.length] = isReconnection ? authParams.password : utils.crypto.generateHashedText(authParams.password);
        datElements[datElements.length] = '"}';

        var req = _addRequestHeader(datElements.join(''), reqGroup.Authentication, reqType.Authentication.AuthenticationNormal);

        utils.logger.logInfo('Retail Auth Request : ' + req);

        return req;
    };

    var generateSsoAuthRequest = function () {
        return '';
    };

    var generateLevel2AuthRequest = function (password) {
        var datElements = [];

        datElements[datElements.length] = '{"l2Pwd":"';
        datElements[datElements.length] = utils.crypto.generateHashedText(password);
        datElements[datElements.length] = '","l2PwdTyp": 1}';

        var req = _addRequestHeader(datElements.join(''), reqGroup.Authentication, reqType.Authentication.AuthenticationLevel2);

        utils.logger.logInfo('Level 2 Auth Request : ' + req);

        return req;
    };

    var generateReconnectionAuthRequest = function () {
        var req;

        if (utils.validators.isAvailable(Ember.appGlobal.session.id)) {
            var datElements = [];
            var tradeUser = sharedService.getService('trade').userDS;

            datElements[datElements.length] = '{"lgnNme":"';
            datElements[datElements.length] = Ember.appGlobal.session.id;
            datElements[datElements.length] = '","oldSsnId":"';
            datElements[datElements.length] = tradeUser.sessionId;
            datElements[datElements.length] = '","lstLgnDteTme":"';
            datElements[datElements.length] = tradeUser.crntLgnDteTme;
            datElements[datElements.length] = '"}';

            req = _addRequestHeader(datElements.join(''), reqGroup.Authentication, reqType.Authentication.AuthenticationNormal);
            utils.logger.logInfo('Retail Auth Reconnection Request : ' + req);
        }

        return req;
    };

    var generateLogoutRequest = function () {
        return _addRequestHeader('{}', reqGroup.Authentication, reqType.Authentication.Logout);
    };

    var generateCustomerDetailsRequest = function () {
        var datElements = [];

        datElements[datElements.length] = '{"cusId":"';
        datElements[datElements.length] = sharedService.getService('trade').userDS.usrId;
        datElements[datElements.length] = '"}';

        return _addRequestHeader(datElements.join(''), reqGroup.Customer, reqType.Customer.CustomerDetails);
    };

    var generateExchangeConfigRequest = function () {
        var datElements = [];

        datElements[datElements.length] = '{"exgs":"';
        datElements[datElements.length] = sharedService.getService('trade').userDS.userExchg.join(utils.Constants.StringConst.Comma);
        datElements[datElements.length] = '"}';

        return _addRequestHeader(datElements.join(''), reqGroup.Exchange, reqType.Exchange.ExchangeConfig);
    };

    var generateRuleRequest = function () {
        return _addRequestHeader('{}', reqGroup.Exchange, reqType.Exchange.ExchangeRule);
    };

    var generateBuyingPowerRequest = function (tradingAcc) {
        var datElements = [];

        datElements[datElements.length] = '{"secAccNum":"';
        datElements[datElements.length] = tradingAcc.tradingAccId;
        datElements[datElements.length] = '"}';

        return _addRequestHeader(datElements.join(''), reqGroup.Exchange, reqType.Exchange.BuyingPower);
    };

    var generateHoldingsRequest = function (tradingAcc) {
        var datElements = [];

        datElements[datElements.length] = '{"secAccNum":"';
        datElements[datElements.length] = tradingAcc.tradingAccId;
        datElements[datElements.length] = '"}';

        return _addRequestHeader(datElements.join(''), reqGroup.Exchange, reqType.Exchange.Holdings);
    };

    var generateOrderListRequest = function (args) {
        var datElements = [];

        datElements[datElements.length] = '{"secAccNum":"';
        datElements[datElements.length] = args.tradingAccId;
        datElements[datElements.length] = '","ordCatgry":';
        datElements[datElements.length] = args.ordCategory;
        datElements[datElements.length] = '}';

        return _addRequestHeader(datElements.join(''), reqGroup.OrderList, reqType.OrderList.OrderList);
    };

    var generatePushRequest = function () {
        // Blocked message to avoid push messages in current protocol
        return undefined;
    };

    var generatePulseMessage = function () {
        return _addRequestHeader('{}', reqGroup.Pulse, reqType.Pulse.Pulse);
    };

    var generateNormalOrderRequest = function (order) {
        var ordObj = {
            secAccNum: order.get('secAccNum'),
            symbol: order.get('symbol'),
            exg: order.get('exg'),
            ordTyp: order.get('ordTyp'),
            ordSide: order.get('ordSide'),
            ordQty: order.get('ordQty'),
            tif: order.get('tif'),
            disQty: order.get('disQty') > 0 ? order.get('disQty') : 0,
            minQty: order.get('minQty') > 0 ? order.get('minQty') : 0,
            price: order.get('price'), // Format to one decimal to support TDWL with Colombo OMS
            dayOrd: 0,
            instruTyp: order.get('instruTyp'),
            expTime: order.get('expTime'),
            bkId: order.get('bkId')
        };

        return _addRequestHeader(utils.jsonHelper.convertToJson(ordObj), reqGroup.Order, reqType.Order.NormalOder);
    };

    var generateAmendOrderRequest = function (order) {
        var ordObj = {
            secAccNum: order.get('secAccNum'),
            symbol: order.get('symbol'),
            exg: order.get('exg'),
            ordTyp: order.get('ordTyp'),
            ordSide: order.get('ordSide'),
            ordQty: order.get('ordQty'),
            tif: order.get('tif'),
            disQty: order.get('disQty') > 0 ? order.get('disQty') : 0,
            minQty: order.get('minQty') > 0 ? order.get('minQty') : 0,
            price: order.get('price'),
            dayOrd: 0,
            expTime: order.get('expTime'),
            bkId: order.get('bkId'),
            clOrdId: order.get('clOrdId'),
            secTyp: order.get('secTyp'),
            orgClOrdId: order.get('clOrdId'),
            crdDte: order.get('crdDte'),
            ordVal: order.get('calcOrdVal'),
            cumComsn: order.get('cumComsn'),
            netOrdVal: order.get('calcNetOrdVal'),
            stlCurrncy: 'LKR',
            mktCode: 'N',
            mubOrdNum: order.get('mubOrdNum'),
            curr: order.get('curr'),
            ordCatgry: 1,
            instruTyp: order.get('instruTyp'),
            isTimeTriggerede: 0,
            brokerClient: -1
        };

        return _addRequestHeader(utils.jsonHelper.convertToJson(ordObj), reqGroup.Order, reqType.Order.AmendOder);
    };

    var generateCancelOrderRequest = function (order) {
        var ordObj = {
            secAccNum: order.get('secAccNum'),
            symbol: order.get('symbol'),
            exg: order.get('exg'),
            ordTyp: order.get('ordTyp'),
            ordSide: order.get('ordSide'),
            tif: order.get('tif'),
            bkId: order.get('bkId'),
            mubOrdNum: order.get('mubOrdNum'),
            orgClOrdId: order.get('clOrdId'),
            clOrdId: order.get('clOrdId')
        };

        return _addRequestHeader(utils.jsonHelper.convertToJson(ordObj), reqGroup.Order, reqType.Order.CancelOder);
    };

    var generateDepositRequest = function (transaction) {
        return _addRequestHeader(utils.jsonHelper.convertToJson(transaction), reqGroup.Transaction, reqType.Transaction.Deposit);
    };

    var generateWithdrawalRequest = function (transaction) {
        var transObj = {
            payMtd: transaction.payMtd,
            secAccNum: transaction.tradingAccId,
            sMubNo: sharedService.getService('trade').userDS.get('mubNo'),
            curr: transaction.curr,
            amt: transaction.amt,
            bId: transaction.bId,
            toAcc: transaction.toAcc,
            valDte: transaction.valDte,
            nts: 0
        };

        return _addRequestHeader(utils.jsonHelper.convertToJson(transObj), reqGroup.Transaction, reqType.Transaction.Withdrawal);
    };

    var generateCashTransferRequest = function (transaction) {
        return _addRequestHeader(utils.jsonHelper.convertToJson(transaction), reqGroup.Transaction, reqType.Transaction.CashTransfer);
    };

    var generateCurrenyRateRequest = function (transaction) {
        return _addRequestHeader(utils.jsonHelper.convertToJson(transaction), reqGroup.Exchange, reqType.Exchange.CurrencyRate);
    };

    var generateBankAccountsRequest = function (bankAcc) {
        return _addRequestHeader(utils.jsonHelper.convertToJson(bankAcc), reqGroup.Exchange, reqType.Exchange.BankAccounts);
    };

    var generateDestBankAccountRequest = function (bankAcc) {
        return _addRequestHeader(utils.jsonHelper.convertToJson(bankAcc), reqGroup.Exchange, reqType.Exchange.DestBankAccounts);
    };

    var generateTransactionHistoryRequest = function (msgData) {
        return _addRequestHeader(utils.jsonHelper.convertToJson(msgData), reqGroup.Transaction, reqType.Transaction.History);
    };

    var generateOrderCommissionRequest = function (unqReqId, orderParams) {
        var ordObj = {
            secAccNum: orderParams.get('secAccNum'),
            symbol: orderParams.get('symbol'),
            exg: orderParams.get('exg'),
            instruTyp: orderParams.get('instruTyp'),
            ordTyp: orderParams.get('ordTyp'),
            price: orderParams.get('price'),
            ordQty: orderParams.get('instruTyp') === utils.AssetTypes.Option ? orderParams.get('ordQty') * tradeConstants.MultiFactor : orderParams.get('ordQty'),
            unqReqId: unqReqId
        };

        return _addRequestHeader(utils.jsonHelper.convertToJson(ordObj), reqGroup.OrderList, reqType.OrderList.OrderCommission);
    };

    var generateSymbolMarginRequest = function (unqReqId, orderParams, symbolInfo) {
        var reqObj = {
            secAccNum: orderParams.get('secAccNum'),
            symbol: symbolInfo.get('sym'),
            exg: symbolInfo.get('exg'),
            unqReqId: unqReqId
        };

        return _addRequestHeader(utils.jsonHelper.convertToJson(reqObj), reqGroup.Exchange, reqType.Exchange.SymbolMarginBP);
    };

    var generateOrderSearchRequest = function (reqObj) {
        return _addRequestHeader(utils.jsonHelper.convertToJson(reqObj), reqGroup.OrderList, reqType.OrderList.OrderSearch);
    };

    var generateKYCDetailsRequest = function (reqObj) {
        return _addRequestHeader(utils.jsonHelper.convertToJson(reqObj), reqGroup.Customer, reqType.Customer.KYCDetails);
    };

    var generateKYCMasterDataRequest = function (reqObj) {
        return _addRequestHeader(utils.jsonHelper.convertToJson(reqObj), reqGroup.Customer, reqType.Customer.KYCMasterData);
    };

    var generateKYCUpdateRequest = function (reqObj) {
        return _addRequestHeader(utils.jsonHelper.convertToJson(reqObj), reqGroup.Customer, reqType.Customer.KYCUpdate);
    };

    var generateNinIqamaValidationRequest = function (reqObj) {
        return _addRequestHeader(utils.jsonHelper.convertToJson(reqObj), reqGroup.Authentication, reqType.Authentication.NinIqama);
    };

    var generateCaptchaImageRequest = function (reqObj) {
        return _addRequestHeader(utils.jsonHelper.convertToJson(reqObj), reqGroup.Authentication, reqType.Authentication.CaptchaImage);
    };

    var generateCaptchaTextRequest = function (reqObj) {
        return _addRequestHeader(utils.jsonHelper.convertToJson(reqObj), reqGroup.Authentication, reqType.Authentication.CaptchaText);
    };

    var generateCashStatementRequest = function (reqObj) {
        return _addRequestHeader(utils.jsonHelper.convertToJson(reqObj), reqGroup.OrderList, reqType.OrderList.CashStatement);
    };

    var generateStockStatementRequest = function (reqObj) {
        return _addRequestHeader(utils.jsonHelper.convertToJson(reqObj), reqGroup.OrderList, reqType.OrderList.StockStatement);
    };

    var generateChangePasswordRequest = function (reqObj) {
        return _addRequestHeader(utils.jsonHelper.convertToJson(reqObj), reqGroup.Authentication, reqType.Authentication.ChangePassword);
    };

    var generateUserTypeRequest = function (reqObj) {
        return _addRequestHeader(utils.jsonHelper.convertToJson(reqObj), reqGroup.Authentication, reqType.Authentication.UserType);
    };

    var generateFundListRequest = function () {
        var datElements = [];

        datElements[datElements.length] = '{';
        datElements[datElements.length] = '}';

        return _addRequestHeader(datElements.join(''), reqGroup.MutualFund, reqType.MutualFund.FundList);
    };

    var generateTermsConditionGetRequest = function (reqObj) {
        var datElements = [];

        datElements[datElements.length] = '{"usrId":"';
        datElements[datElements.length] = sharedService.getService('trade').userDS.get('usrId');
        datElements[datElements.length] = '","fndName":"';
        datElements[datElements.length] = reqObj.fundName;
        datElements[datElements.length] = '"}';

        return _addRequestHeader(datElements.join(''), reqGroup.MutualFund, reqType.MutualFund.TermsAndConditionGet);
    };

    var generateTermsConditionUpdateRequest = function (reqObj) {
        var datElements = [];

        datElements[datElements.length] = '{"usrId":"';
        datElements[datElements.length] = sharedService.getService('trade').userDS.get('usrId');
        datElements[datElements.length] = '","fndName":"';
        datElements[datElements.length] = reqObj.fundName;
        datElements[datElements.length] = '"}';

        return _addRequestHeader(datElements.join(''), reqGroup.MutualFund, reqType.MutualFund.TermsAndConditionUpdate);
    };

    var generateFundTransactionsRequest = function (reqObj) {
        var datElements = [];

        datElements[datElements.length] = '{"mubNo":"';
        datElements[datElements.length] = sharedService.getService('trade').userDS.get('mubNo');
        datElements[datElements.length] = '","frmDate":"';
        datElements[datElements.length] = reqObj.fromDate;
        datElements[datElements.length] = '","toDate":"';
        datElements[datElements.length] = reqObj.toDate;
        datElements[datElements.length] = '"}';

        return _addRequestHeader(datElements.join(''), reqGroup.MutualFund, reqType.MutualFund.FundTrasactions);
    };

    var generateFundHoldingsRequest = function () {
        var datElements = [];

        datElements[datElements.length] = '{"cusRefNum":"';
        datElements[datElements.length] = sharedService.getService('trade').userDS.get('mubNo');
        datElements[datElements.length] = '"}';

        return _addRequestHeader(datElements.join(''), reqGroup.MutualFund, reqType.MutualFund.FundHoldings);
    };

    var generateNavFundDateRequest = function (reqObj) {
        return _addRequestHeader(utils.jsonHelper.convertToJson(reqObj), reqGroup.MutualFund, reqType.MutualFund.NavFundDate);
    };

    var generateMFSubscriptionFeeRequest = function (reqObj) {
        return _addRequestHeader(utils.jsonHelper.convertToJson(reqObj), reqGroup.MutualFund, reqType.MutualFund.SubscriptionFee);
    };

    var generateRedemptionFeeRequest = function (reqObj) {
        return _addRequestHeader(utils.jsonHelper.convertToJson(reqObj), reqGroup.MutualFund, reqType.MutualFund.RedemptionFee);
    };

    var generateMFRedemptionRequest = function (reqObj) {
        return _addRequestHeader(utils.jsonHelper.convertToJson(reqObj), reqGroup.MutualFund, reqType.MutualFund.Redemption);
    };

    var generateMFSubscriptionRequest = function (reqObj) {
        return _addRequestHeader(utils.jsonHelper.convertToJson(reqObj), reqGroup.MutualFund, reqType.MutualFund.Subscription);
    };

    var generateMFVATRequest = function (unqReqId, mutualFund) {
        return _addRequestHeader(utils.jsonHelper.convertToJson(mutualFund), reqGroup.MutualFund, reqType.MutualFund.VatAmount);
    };

    var generateRightsSubscriptionRequest = function (reqObj) {
        return _addRequestHeader(utils.jsonHelper.convertToJson(reqObj), reqGroup.RightsSubscription, reqType.RightsSubscription.Subscription);
    };

    var generateRightsSubscriptionListRequest = function (reqObj) {
        return _addRequestHeader(utils.jsonHelper.convertToJson(reqObj), reqGroup.RightsSubscription, reqType.RightsSubscription.SubscriptionList);
    };

    var generateProductDetailsRequest = function (reqObj) {
        return _addRequestHeader(utils.jsonHelper.convertToJson(reqObj), reqGroup.Subscription, reqType.Subscription.ProductDetails);
    };

    var generateUserRenewUpgradeProductListRequest = function (reqObj) {
        return _addRequestHeader(utils.jsonHelper.convertToJson(reqObj), reqGroup.Subscription, reqType.Subscription.UserRenewUpgradeProductList);
    };

    var generateSubscriptionFeeRequest = function (reqObj) {
        return _addRequestHeader(utils.jsonHelper.convertToJson(reqObj), reqGroup.Subscription, reqType.Subscription.SubscriptionFee);
    };

    var generateBulkOrderCancelRequest = function (reqObj) {
        return _addRequestHeader(utils.jsonHelper.convertToJson(reqObj), reqGroup.Order, reqType.Order.CancelBulkOrder);
    };

    var generateSubscriptionRequest = function (reqObj) {
        return _addRequestHeader(utils.jsonHelper.convertToJson(reqObj), reqGroup.Subscription, reqType.Subscription.Subscription);
    };

    var generateConsolidatedReportRequest = function (reqObj) {
        return _addRequestHeader(utils.jsonHelper.convertToJson(reqObj), reqGroup.OrderList, reqType.OrderList.ConsolidatedReport);
    };

    // Registration / Forgot Password - Step 1
    var generateCusDetailValidationRequest = function (reqObj, isForgotPwd) {
        if (isForgotPwd) {
            return _addRequestHeader(utils.jsonHelper.convertToJson(reqObj), reqGroup.Registration, reqType.Registration.CustomerDetail);
        } else {
            return _addRequestHeader(utils.jsonHelper.convertToJson(reqObj), reqGroup.Registration, reqType.Registration.RegCustomerDetail);
        }
    };

    // Registration / Forgot Password - Step 2
    var generateOtpValidationRequest = function (reqObj) {
        return _addRequestHeader(utils.jsonHelper.convertToJson(reqObj), reqGroup.Registration, reqType.Registration.OtpLogin);
    };

    // Login OTP
    var generateLoginOtpValidationRequest = function (reqObj) {
        return _addRequestHeader(utils.jsonHelper.convertToJson(reqObj), reqGroup.Authentication, reqType.Authentication.OtpEnabled);
    };

    // Registration - Step 3 / Forgot Password - Step 3
    var generatePasswordValidationRequest = function (reqObj, isForgotPwd) {
        if (isForgotPwd) {
            return _addRequestHeader(utils.jsonHelper.convertToJson(reqObj), reqGroup.Registration, reqType.Registration.PasswordConfirmation);
        } else {
            return _addRequestHeader(utils.jsonHelper.convertToJson(reqObj), reqGroup.Registration, reqType.Registration.RegPasswordConfirmation);
        }
    };

    var _addRequestHeader = function (msgData, msgGroup, msgType) {
        var reqElements = [];

        reqElements[reqElements.length] = '{"HED":{"ver":"DFN_JSON_1.0","msgGrp":';
        reqElements[reqElements.length] = msgGroup;
        reqElements[reqElements.length] = ',"msgTyp":';
        reqElements[reqElements.length] = msgType;
        reqElements[reqElements.length] = ',"chnlId":';
        reqElements[reqElements.length] = tradeSettings.channelId;
        reqElements[reqElements.length] = ',"clVer":"';
        reqElements[reqElements.length] = environmentConfig.APP.version;
        reqElements[reqElements.length] = '","usrId":"';
        reqElements[reqElements.length] = sharedService.getService('trade').userDS.usrId;
        reqElements[reqElements.length] = '","sesnId":"';
        reqElements[reqElements.length] = sharedService.getService('trade').userDS.sessionId;
        reqElements[reqElements.length] = '"},"DAT":';
        reqElements[reqElements.length] = msgData;
        reqElements[reqElements.length] = '}';

        return reqElements.join('');
    };

    return {
        generateRetailAuthRequest: generateRetailAuthRequest,
        generateSsoAuthRequest: generateSsoAuthRequest,
        generateLevel2AuthRequest: generateLevel2AuthRequest,
        generateReconnectionAuthRequest: generateReconnectionAuthRequest,
        generateCustomerDetailsRequest: generateCustomerDetailsRequest,
        generateLogoutRequest: generateLogoutRequest,
        generateExchangeConfigRequest: generateExchangeConfigRequest,
        generateBuyingPowerRequest: generateBuyingPowerRequest,
        generateHoldingsRequest: generateHoldingsRequest,
        generateOrderListRequest: generateOrderListRequest,
        generatePulseMessage: generatePulseMessage,
        generateNormalOrderRequest: generateNormalOrderRequest,
        generateAmendOrderRequest: generateAmendOrderRequest,
        generateCancelOrderRequest: generateCancelOrderRequest,
        generateDepositRequest: generateDepositRequest,
        generateWithdrawalRequest: generateWithdrawalRequest,
        generateBankAccountsRequest: generateBankAccountsRequest,
        generateDestBankAccountRequest: generateDestBankAccountRequest,
        generateTransactionHistoryRequest: generateTransactionHistoryRequest,
        generateOrderCommissionRequest: generateOrderCommissionRequest,
        generateSymbolMarginRequest: generateSymbolMarginRequest,
        generateRuleRequest: generateRuleRequest,
        generateOrderSearchRequest: generateOrderSearchRequest,
        generateCashStatementRequest: generateCashStatementRequest,
        generateStockStatementRequest: generateStockStatementRequest,
        generateChangePasswordRequest: generateChangePasswordRequest,
        generateCusDetailValidationRequest: generateCusDetailValidationRequest,
        generateOtpValidationRequest: generateOtpValidationRequest,
        generateLoginOtpValidationRequest: generateLoginOtpValidationRequest,
        generatePasswordValidationRequest: generatePasswordValidationRequest,
        generateUserTypeRequest: generateUserTypeRequest,
        generateKYCDetailsRequest: generateKYCDetailsRequest,
        generateKYCMasterDataRequest: generateKYCMasterDataRequest,
        generateKYCUpdateRequest: generateKYCUpdateRequest,
        generateNinIqamaValidationRequest: generateNinIqamaValidationRequest,
        generateCaptchaImageRequest: generateCaptchaImageRequest,
        generateCaptchaTextRequest: generateCaptchaTextRequest,
        generateProductDetailsRequest: generateProductDetailsRequest,
        generateUserRenewUpgradeProductListRequest: generateUserRenewUpgradeProductListRequest,
        generateSubscriptionFeeRequest: generateSubscriptionFeeRequest,
        generateSubscriptionRequest: generateSubscriptionRequest,
        generateBulkOrderCancelRequest: generateBulkOrderCancelRequest,
        generateConsolidatedReportRequest: generateConsolidatedReportRequest,
        generateCashTransferRequest: generateCashTransferRequest,
        generateFundListRequest: generateFundListRequest,
        generateTermsConditionGetRequest: generateTermsConditionGetRequest,
        generateTermsConditionUpdateRequest: generateTermsConditionUpdateRequest,
        generateFundTransactionsRequest: generateFundTransactionsRequest,
        generateFundHoldingsRequest: generateFundHoldingsRequest,
        generateNavFundDateRequest: generateNavFundDateRequest,
        generateMFSubscriptionFeeRequest: generateMFSubscriptionFeeRequest,
        generateRedemptionFeeRequest: generateRedemptionFeeRequest,
        generateMFRedemptionRequest: generateMFRedemptionRequest,
        generateMFSubscriptionRequest: generateMFSubscriptionRequest,
        generateMFVATRequest: generateMFVATRequest,
        generateRightsSubscriptionRequest: generateRightsSubscriptionRequest,
        generateRightsSubscriptionListRequest: generateRightsSubscriptionListRequest,
        generateCurrenyRateRequest: generateCurrenyRateRequest,
        generatePushRequest: generatePushRequest
    };
})();
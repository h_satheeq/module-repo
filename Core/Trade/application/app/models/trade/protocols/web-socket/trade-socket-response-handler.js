/* global Queue */
import Ember from 'ember';
import SocketResponseHandler from '../../../shared/protocols/web-socket/socket-response-handler';
import tradeConstants from '../../trade-constants';
import sharedService from '../../../shared/shared-service';
import utils from '../../../../utils/utils';
import languageDataStore from '../../../../models/shared/language/language-data-store';

export default SocketResponseHandler.extend({
    exchangeConfig: {
        tif: '1',
        orderSide: '2',
        orderType: '3'
    },

    exchangeRule: {
        tifByType: '0'
    },

    init: function () {
        var that = this;

        this.set('tradeService', sharedService.getService('trade'));
        this.lang = languageDataStore.getLanguageObj().get('lang');

        this._super();
        this.inputQueue = new Queue();

        this.processTimer = setTimeout(function () {
            that.processResponse();
        }, tradeConstants.TimeIntervals.WebSocketInQueueProcessingInterval);
    },

    processResponse: function () {
        while (this.inputQueue.getLength() > 0) {
            var frameBuffer = this.inputQueue.dequeue();

            utils.logger.logDebug('Received frame : ' + frameBuffer);

            try {
                var frameMessage = JSON.parse(frameBuffer);

                try {
                    this._processMessage(frameMessage, this.onSocketReady);
                } catch (e) {
                    utils.logger.logError('Response processing failed : ' + e);
                }
            } catch (e) {
                utils.logger.logError('Json parse failed : ' + e);
            }
        }

        var that = this;

        setTimeout(function () {
            that.processResponse();
        }, tradeConstants.TimeIntervals.WebSocketInQueueProcessingInterval);
    },

    /* *
     * Processes message frames from the server
     */
    _processMessage: function (message, onSocketReady) {
        var msgGroup = message.HED.msgGrp;
        var msgType = message.HED.msgTyp;

        switch (msgGroup) {
            case tradeConstants.Response.Group.Authentication:
                switch (msgType) {
                    case tradeConstants.Response.Type.Authentication.AuthenticationNormal:
                        this.processAuthOtpResponse(message, this.callbacks.auth, onSocketReady);
                        break;

                    case tradeConstants.Response.Type.Authentication.AuthenticationLevel2:
                        this._processLevel2AuthResponse(message);
                        break;

                    case tradeConstants.Response.Type.Authentication.Logout:
                        this._processLogoutResponse(message, this.callbacks.auth, onSocketReady);
                        break;

                    case tradeConstants.Response.Type.Authentication.ChangePassword:
                        this._processChangePassword(message);
                        break;

                    case tradeConstants.Response.Type.Authentication.UserType:
                        this._processUserType(message);
                        break;

                    case tradeConstants.Response.Type.Registration.RegCustomerDetail:
                    case tradeConstants.Response.Type.Registration.CustomerDetail:
                        this._processRegistrationCustomerDetail(message);
                        break;

                    case tradeConstants.Response.Type.Registration.OtpLogin:
                        var authCallback = this.callbacks ? this.callbacks.auth : undefined;

                        this._processRegistrationOtpLoginDetail(message, authCallback, onSocketReady);
                        break;

                    case tradeConstants.Response.Type.Registration.RegPasswordConfirmation:
                    case tradeConstants.Response.Type.Registration.PasswordConfirmation:
                        this._processRegistrationPasswordConfirmation(message);
                        break;

                    case tradeConstants.Response.Type.Authentication.NinIqama:
                        this._processNinIqamaValidation(message);
                        break;

                    case tradeConstants.Response.Type.Authentication.CaptchaImage:
                        this._processCaptchaImage(message);
                        break;

                    case tradeConstants.Response.Type.Authentication.CaptchaText:
                        this._processCaptchaText(message);
                        break;

                    default:
                        utils.logger.logWarning('Unsupported message type : ' + msgType);
                        break;
                }
                break;

            case tradeConstants.Response.Group.Customer:
                switch (msgType) {
                    case tradeConstants.Response.Type.Customer.CustomerDetails:
                        this._processCustomerDetails(message);
                        break;

                    case tradeConstants.Response.Type.Customer.KYCDetails:
                        this._processKYCDetails(message);
                        break;

                    case tradeConstants.Response.Type.Customer.KYCMasterData:
                        this._processKYCMasterData(message);
                        break;

                    case tradeConstants.Response.Type.Customer.KYCUpdate:
                        this._processKYCUpdate(message);
                        break;

                    case tradeConstants.Response.Type.Customer.CurrencyRate:
                        this._processCurrencyRate(message);
                        break;

                    default:
                        utils.logger.logWarning('Unsupported message type : ' + msgType);
                        break;
                }
                break;

            case tradeConstants.Response.Group.Exchange:
                switch (msgType) {
                    case tradeConstants.Response.Type.Exchange.ExchangeConfig:
                        this._processConfigForExchange(message);
                        break;

                    case tradeConstants.Response.Type.Exchange.BuyingPower:
                        this._processBuyingPower(message);
                        break;

                    case tradeConstants.Response.Type.Exchange.Holdings:
                        this._processHoldings(message);
                        break;

                    case tradeConstants.Response.Type.Exchange.SymbolMarginBP:
                        this._processSymbolMarginBP(message);
                        break;

                    case tradeConstants.Response.Type.Exchange.ExchangeRule:
                        this._processExchangeRules(message);
                        break;

                    case tradeConstants.Response.Type.Exchange.BankAccounts:
                        this._processBankAccounts(message);
                        break;

                    case tradeConstants.Response.Type.Exchange.DestBankAccounts:
                        this._processDesBankAccounts(message);
                        break;

                    case tradeConstants.Response.Type.Exchange.FxStatus:
                        this._processCurrConvertRequest(message);
                        break;

                    default:
                        utils.logger.logWarning('Unsupported message type : ' + msgType);
                        break;
                }
                break;

            case tradeConstants.Response.Group.OrderList:
                switch (msgType) {
                    case tradeConstants.Response.Type.OrderList.OrderSearch:
                        this._processOrderSearch(message);
                        break;

                    case tradeConstants.Response.Type.OrderList.OrderList:
                        this._processOrderList(message);
                        break;

                    case tradeConstants.Response.Type.OrderList.OrderCommission:
                        this._processOrderCommission(message);
                        break;

                    case tradeConstants.Response.Type.OrderList.CashStatement:
                        this._processCashStatement(message);
                        break;

                    case tradeConstants.Response.Type.OrderList.StockStatement:
                        this._processStockStatement(message);
                        break;

                    case tradeConstants.Response.Type.OrderList.ConsolidatedReport:
                        this._processConsolidatedReport(message);
                        break;

                    default:
                        utils.logger.logWarning('Unsupported message type : ' + msgType);
                        break;
                }
                break;

            case tradeConstants.Response.Group.Pulse:
                switch (msgType) {
                    case tradeConstants.Response.Type.Pulse.Pulse:
                        break;

                    default:
                        utils.logger.logWarning('Unsupported message type : ' + msgType);
                        break;
                }
                break;

            case tradeConstants.Response.Group.Order:
                switch (msgType) {
                    case tradeConstants.Response.Type.Order.NormalOder:
                        this._processNormalOrderResponse(message);
                        break;

                    case tradeConstants.Response.Type.Order.CancelBulkOrder:
                        this._processBulkOrderCancelResponse(message);
                        break;

                    default:
                        utils.logger.logWarning('Unsupported message type : ' + msgType);
                        break;
                }
                break;

            case tradeConstants.Response.Group.Transaction:
                switch (msgType) {
                    case tradeConstants.Response.Type.Transaction.Deposit:
                    case tradeConstants.Response.Type.Transaction.Withdrawal:
                        this._processTransactionResponse(message);
                        break;

                    case tradeConstants.Response.Type.Transaction.CashTransfer:
                        this._processCashTranfer(message);
                        break;

                    case tradeConstants.Response.Type.Transaction.History:
                        this._processTransactionList(message);
                        break;

                    default:
                        utils.logger.logWarning('Unsupported message type : ' + msgType);
                        break;
                }
                break;

            case tradeConstants.Response.Group.Subscription:
                switch (msgType) {
                    case tradeConstants.Response.Type.Subscription.ProductDetails:
                        this._processProductDetails(message);
                        break;

                    case tradeConstants.Response.Type.Subscription.UserRenewUpgradeProductList:
                        this._processUserRenewUpgradeProductList(message);
                        break;

                    case tradeConstants.Response.Type.Subscription.SubscriptionFee:
                        this._processSubscriptionFee(message);
                        break;

                    case tradeConstants.Response.Type.Subscription.Subscription:
                        this._processSubscription(message);
                        break;

                    default:
                        utils.logger.logWarning('Unsupported message type : ' + msgType);
                        break;
                }
                break;

            case tradeConstants.Response.Group.RightsSubscription:
                switch (msgType) {
                    case tradeConstants.Response.Type.RightsSubscription.Subscription:
                        this._processRightsSubscription(message);
                        break;

                    case tradeConstants.Response.Type.RightsSubscription.SubscriptionList:
                        this._processRightsSubscriptionList(message);
                        break;

                    default:
                        utils.logger.logWarning('Unsupported message type : ' + msgType);
                        break;
                }
                break;

            case tradeConstants.Response.Group.MutualFund:
                switch (msgType) {
                    case tradeConstants.Response.Type.MutualFund.FundList:
                        this._processFundList(message);
                        break;

                    case tradeConstants.Response.Type.MutualFund.FundTrasactions:
                        this._processFundTransactions(message);
                        break;

                    case tradeConstants.Response.Type.MutualFund.FundHoldings:
                        this._processFundHoldings(message);
                        break;

                    case tradeConstants.Response.Type.MutualFund.NavFundDate:
                        this._processMFNavFundDate(message);
                        break;

                    case tradeConstants.Response.Type.MutualFund.SubscriptionFee:
                        this._processMFSubscriptionFee(message);
                        break;

                    case tradeConstants.Response.Type.MutualFund.RedemptionFee:
                        this._processMFRedemptionFee(message);
                        break;

                    case tradeConstants.Response.Type.MutualFund.Subscription:
                        this._processMFSubscription(message);
                        break;

                    case tradeConstants.Response.Type.MutualFund.Redemption:
                        this._processMFRedemption(message);
                        break;

                    case tradeConstants.Response.Type.MutualFund.TermsAndConditionGet:
                        this._processGetTermsCondition(message);
                        break;

                    case tradeConstants.Response.Type.MutualFund.TermsAndConditionUpdate:
                        this._processUpdateTermsCondition(message);
                        break;

                    case tradeConstants.Response.Type.MutualFund.VatAmount:
                        this._processMFVatAmount(message);
                        break;

                    default:
                        utils.logger.logWarning('Unsupported message type : ' + msgType);
                        break;
                }
                break;

            default:
                utils.logger.logWarning('Unsupported message group : ' + msgGroup);
                break;
        }
    },

    _processLogoutResponse: function (message, authCallbacks, onSocketReady) {
        // TODO: [Bashitha] Check whether we can get the error message from logout response

        var errorMsg = this.lang.messages.sessionExpired;
        utils.logger.logInfo('User logged out.' + errorMsg);

        if (Ember.$.isFunction(onSocketReady)) {
            onSocketReady(false);
        }

        this._logoutApplication(errorMsg);
    },

    _processLevel2AuthResponse: function () {
        // TODO: [Bashitha] Implement
    },

    _processAuthResponse: function (message, authCallbacks, onSocketReady) {
        var authSuccess = false;
        var isLoggedIn = this.tradeService.isAuthenticated();
        var isOtpEnabled = message.DAT.authSts === tradeConstants.AuthStatus.OtpEnabled;

        if (message.DAT.authSts === tradeConstants.AuthStatus.Success || isOtpEnabled || message.DAT.authSts === tradeConstants.AuthStatus.FirstTimeLogin) {
            utils.logger.logInfo('User authenticated successfully.');

            authSuccess = true;

            if (utils.validators.isAvailable(message.DAT.cusNme)) {
                message.DAT.cusNme = utils.formatters.convertUnicodeToNativeString(message.DAT.cusNme);
            }

            this.tradeService.userDS.set('isVatApplicble', message.DAT.isVatApplicble ? message.DAT.isVatApplicble : 0);

            this.tradeService.userDS.setData(Ember.$.extend({}, message.DAT, {sessionId: message.HED.sesnId}));
            this.tradeService.userDS.save();

            if (isOtpEnabled) {
                this.tradeService.userDS.set('isOtpEnabled', true);

                if (Ember.$.isFunction(authCallbacks.successFn)) {
                    authCallbacks.successFn(tradeConstants.OtpStatus.OtpPending);
                }
            } else {
                this._processAuthSuccess(authCallbacks, tradeConstants.OtpStatus.OtpDisabled);
            }
        } else {
            var authMsg = message.DAT.rejResn ? message.DAT.rejResn : this.lang.messages.authFailed;
            authMsg = languageDataStore.generateLangMessage(authMsg);

            utils.logger.logInfo('User authentication failed. ' + authMsg);

            if (isLoggedIn) {
                this._logoutApplication(authMsg);
            } else if (Ember.$.isFunction(authCallbacks.errorFn)) {
                // Simply set error message in login page
                authCallbacks.errorFn(authMsg);
            }
        }

        if (!isOtpEnabled && Ember.$.isFunction(onSocketReady)) {
            onSocketReady(authSuccess);
        }
    },

    _processOtpLoginResponse: function (message, authCallbacks, onSocketReady) {
        if (message && message.DAT) {
            var otpStatus = message.DAT.authSts;
            var indexOtpLoginPopup = sharedService.getService('sharedUI').getService('indexOtpLoginPopup');

            if (otpStatus === tradeConstants.AuthStatus.Success || otpStatus === tradeConstants.AuthStatus.FirstTimeLogin) {
                this.tradeService.userDS.setData(Ember.$.extend({}, message.DAT, {sessionId: message.HED.sesnId}));
                this.tradeService.userDS.save();

                if (authCallbacks) {
                    this._processAuthSuccess(authCallbacks, tradeConstants.OtpStatus.OtpSuccess);

                    if (Ember.$.isFunction(onSocketReady)) {
                        onSocketReady(true);
                    }
                }
            } else {
                var rejectedReason = languageDataStore.generateLangMessage(message.DAT.rejResn);
                Ember.set(indexOtpLoginPopup, 'message', rejectedReason);
            }

            Ember.set(indexOtpLoginPopup, 'otpSts', otpStatus);
            indexOtpLoginPopup.onOtpValidationResponse();
        }
    },

    processAuthOtpResponse: function (message, authCallbacks, onSocketReady) {
        if (this.tradeService.userDS.isOtpEnabled) {
            this._processOtpLoginResponse(message, authCallbacks, onSocketReady);
        } else {
            this._processAuthResponse(message, authCallbacks, onSocketReady);
        }
    },

    _processAuthSuccess: function (authCallbacks, otpStatus) {
        if (Ember.$.isFunction(authCallbacks.successFn)) {
            authCallbacks.successFn(otpStatus);
        }

        if (Ember.$.isFunction(authCallbacks.postSuccessFn)) {
            authCallbacks.postSuccessFn();
        }

        this.tradeService.onAuthenticationSuccess(this.callbacks.auth.isReconnection);
    },

    _logoutApplication: function (message) {
        // Logout from the application with error message set in login page
        utils.webStorage.addString(utils.webStorage.getKey(utils.Constants.CacheKeys.LoginErrorMsg), message, utils.Constants.StorageType.Session);
        utils.applicationSessionHandler.logout(message);
    },

    _processChangePassword: function (message) {
        var callbackFn = this.tradeService.changePasswordCallback;
        var rejectedReason = message.DAT.chgPWdMsg;
        rejectedReason = languageDataStore.generateLangMessage(rejectedReason);

        if (callbackFn && Ember.$.isFunction(callbackFn)) {
            callbackFn(message.DAT.chgPwdSts, rejectedReason);
        }
    },

    _processUserType: function (message) {
        var callbackFn = this.tradeService.userTypeCallback;

        if (callbackFn && Ember.$.isFunction(callbackFn)) {
            callbackFn(message.DAT.stats);
        }
    },

    _processCustomerDetails: function (message) {
        var that = this;

        if (message && message.DAT) {
            if (message.DAT.cshAccLst) {
                Ember.$.each(message.DAT.cshAccLst, function (key, acc) {
                    var cashAcc = that.tradeService.accountDS.getCashAccount(acc.cshAccNum);

                    if (cashAcc) {
                        cashAcc.setData(acc);
                    }
                });
            }

            if (message.DAT.secAccLst) {
                var userExchanges = [];

                Ember.$.each(message.DAT.secAccLst, function (key, acc) {
                    that._setCustomerExgLst(acc, userExchanges);

                    var tradingAcc = that.tradeService.accountDS.getTradingAccount(acc.secAccNum, acc.cshAccNum, acc.exgArray);

                    if (tradingAcc) {
                        tradingAcc.setData(acc);
                    }
                });

                this.tradeService.userDS.set('userExchg', userExchanges);
            }

            if (message.DAT.cusDetails) {
                this.tradeService.userDS.set('cusDetails', message.DAT.cusDetails);
            }

            this.tradeService.onCustomerDetailSuccess();
        }
    },

    _processKYCDetails: function (message) {
        if (message && message.DAT) {
            var cusData = message.DAT;

            if (cusData) {
                var customer = this.tradeService.customerDS.getKYCDetails(message.DAT.cusId);

                if (customer) {
                    customer.setData(cusData);
                }
            }
        }
    },

    _processProductDetails: function (message) {
        if (message && message.DAT) {
            var proData = message.DAT.prdcts;
            var that = this;

            Ember.$.each(proData, function (proId, productObj) {
                if (productObj) {
                    var product = that.tradeService.productDS.getProduct(productObj.proID);

                    if (product) {
                        product.setData(productObj);
                    }
                }
            });
        }
    },

    _processFundList: function (message) {
        if (message && message.DAT) {
            var that = this;
            var fundData = message.DAT.fndPdctDetrl;

            Ember.$.each(fundData, function (fundId, fundObj) {
                if (fundObj) {
                    var fund = that.tradeService.mutualFundDS.getFund(fundObj.code, undefined, fundObj.viewStatus);

                    if (fund) {
                        fund.setData(fundObj);
                    }
                }
            });
        }
    },

    _processGetTermsCondition: function (message) {
        if (message && message.DAT) {
            var callbackFn = this.tradeService.tCGetCallback;

            if (callbackFn && Ember.$.isFunction(callbackFn)) {
                callbackFn(message.DAT);
            }
        }
    },

    _processMFVatAmount: function (message) {
        if (message && message.DAT && message.DAT.vatAmt) {
            var mfVatData = message.DAT;
            var vatAmt = mfVatData.vatAmt;
            var unqReqId = mfVatData.unqReqId;

            if (utils.validators.isAvailable(unqReqId)) {
                var mutualFund = this.tradeService.mutualFundVATRequestMap[unqReqId];
                mutualFund.set('vatAmt', vatAmt);

                delete this.tradeService.mutualFundVATRequestMap[unqReqId];
            }
        }
    },

    _processUpdateTermsCondition: function (message) {
        if (message && message.DAT) {
            var callbackFn = this.tradeService.tCUpdateCallback;

            if (callbackFn && Ember.$.isFunction(callbackFn)) {
                callbackFn(message.DAT);
            }
        }
    },

    _processFundTransactions: function (message) {
        if (message && message.DAT) {
            var that = this;
            var fundTransData = message.DAT.statmntRes;

            Ember.$.each(fundTransData, function (fundTransId, fundTransObj) {
                if (fundTransObj) {
                    var fundTrans = that.tradeService.mutualFundDS.getTransaction(fundTransObj.transNo, fundTransObj.sts);

                    if (fundTrans) {
                        fundTrans.setData(fundTransObj);
                    }
                }
            });
        }
    },

    _processFundHoldings: function (message) {
        if (message && message.DAT) {
            var fundHoldingData = message.DAT.holdings;
            var that = this;

            Ember.$.each(fundHoldingData, function (fundTransId, fundHoldingsObj) {
                if (fundHoldingsObj) {
                    var fundHolding = that.tradeService.mutualFundDS.getHolding(fundHoldingsObj.code);

                    if (fundHolding) {
                        fundHolding.setData(fundHoldingsObj);
                    }
                }
            });
        }
    },

    _processMFSubscriptionFee: function (message) {
        if (message && message.DAT) {
            var fundHoldingData = message.DAT.holdings;
            var that = this;

            Ember.$.each(fundHoldingData, function (fundTransId, fundHoldingsObj) {
                if (fundHoldingsObj) {
                    var fundHolding = that.tradeService.mutualFundDS.getHolding(fundHoldingsObj.code);

                    if (fundHolding) {
                        fundHolding.setData(fundHoldingsObj);
                    }
                }
            });
        }
    },

    _processMFRedemptionFee: function (message) {
        if (message && message.DAT) {
            var fundHoldingData = message.DAT.holdings;
            var that = this;

            Ember.$.each(fundHoldingData, function (fundTransId, fundHoldingsObj) {
                if (fundHoldingsObj) {
                    var fundHolding = that.tradeService.mutualFundDS.getHolding(fundHoldingsObj.code);

                    if (fundHolding) {
                        fundHolding.setData(fundHoldingsObj);
                    }
                }
            });
        }
    },

    _processMFSubscription: function (message) {
        if (message && message.DAT) {
            var callbackFn = this.tradeService.subscriptionCallback;
            var rejectedReason = message.DAT.rsn;

            rejectedReason = languageDataStore.generateLangMessage(rejectedReason);

            if (callbackFn && Ember.$.isFunction(callbackFn)) {
                callbackFn(message.DAT, rejectedReason);
            }

            if (message.DAT.sts === tradeConstants.MFSubRedStatus.Success) {
                this._processMFTransaction(message);
            }
        }
    },

    _processMFRedemption: function (message) {
        if (message && message.DAT) {
            var callbackFn = this.tradeService.redemptionCallback;
            var rejectedReason = message.DAT.rsn;

            rejectedReason = languageDataStore.generateLangMessage(rejectedReason);

            if (callbackFn && Ember.$.isFunction(callbackFn)) {
                callbackFn(message.DAT, rejectedReason);
            }

            if (message.DAT.sts === tradeConstants.MFSubRedStatus.Success) {
                this._processMFTransaction(message);
            }
        }
    },

    _processMFTransaction: function (message) {
        if (message && message.DAT) {
            var that = this;
            var fundTransObj = message.DAT.stmntRes;

                if (fundTransObj) {
                    var fundTrans = that.tradeService.mutualFundDS.getTransaction(fundTransObj.transNo, fundTransObj.sts);

                    if (fundTrans) {
                        fundTrans.setData(fundTransObj);
                    }
                }
        }
    },

    _processUserRenewUpgradeProductList: function (message) {
        if (message && message.DAT) {
            var proData = message.DAT.prdcts;
            var that = this;

            Ember.$.each(proData, function (proId, productObj) {
                if (productObj) {
                    var feeDetails = productObj.feeDetails ? productObj.feeDetails.split(',') : [];
                    var feeValues = [];
                    var feeField;

                    if (feeDetails && feeDetails.length > 0) {
                        Ember.$.each(feeDetails, function (index, field) {
                            feeField = field ? field.split('|') : [];

                            if (feeField.length > 1) {
                                feeValues[feeValues.length] = {
                                    des: parseInt(feeField[1], 10),
                                    feeId: parseInt(feeField[0], 10)
                                };
                            }
                        });
                    }

                    productObj.proNmeAR = utils.formatters.convertUnicodeToNativeString(productObj.proNmeAR);
                    productObj.feeDetails = feeValues;

                    var product = that.tradeService.productDS.getProduct(productObj.proID);

                    if (product) {
                        product.setData(productObj);
                    }
                }
            });
        }
    },

    _processSubscription: function (message) {
        if (message && message.DAT) {
            var rejctdRsn = message.DAT.rjetResn;
            var stats = message.DAT.sts;
            var prdcts = message.DAT.prdcts;
            var encryptedToken = message.DAT.encTkn;
            var proId, product;

            if (prdcts && prdcts.length > 0) {
                proId = prdcts[0].proID;
                product = this.tradeService.productDS.getProductById(proId);
            }

            if (product) {
                product.setData({rjetResn: rejctdRsn, encryptedToken: encryptedToken});
                product.setData({sts: parseInt(stats, 10)}); // Status is observed in the controller. Set status after all the other parameters were set.
            }
        }
    },

    _processRightsSubscription: function (message) {
        if (message && message.DAT) {
            var rightsSubData = message.DAT;
            var unqReqId = rightsSubData.unqReqId;

            if (utils.validators.isAvailable(unqReqId)) {
                var rightsOrder = this.tradeService.rightSubscriptionRequestMap[unqReqId];

                rightsOrder.set('rsn', rightsSubData.rsn);
                rightsOrder.set('sts', rightsSubData.sts);
            }
        }
    },

    _processRightsSubscriptionList: function (message) {
        var that = this;

        if (message && message.DAT && message.DAT.stockList) {
            Ember.$.each(message.DAT.stockList, function (index, rightsSubscription) {
                if (rightsSubscription) {
                    var rightsSubscriptionObj = that.tradeService.rightsSubscriptionDS.getRightsSubscription(rightsSubscription.exg, rightsSubscription.sym, rightsSubscription.InstTyp, rightsSubscription.subNo);

                    if (rightsSubscriptionObj) {
                        rightsSubscriptionObj.setData(rightsSubscription);
                    }
                }
            });
        }
    },

    _processSubscriptionFee: function (message) {
        if (message && message.DAT) {
            var proId, product;
            var expExpDate = message.DAT.expeExpDte;
            var expFromDate = message.DAT.expeFrmDte;
            var feeCurrency = message.DAT.feeCurr;

            var vatExchangeFee = message.DAT.vatExchangeFee ? message.DAT.vatExchangeFee : 0;
            var vatServiceFee = message.DAT.vatServiceFee ? message.DAT.vatServiceFee : 0;
            var vatThirdPartyFee = message.DAT.vatThirdPartyFee ? message.DAT.vatThirdPartyFee : 0;
            var vat = vatExchangeFee + vatServiceFee + vatThirdPartyFee;

            var fee = message.DAT.exgFee + message.DAT.srvceFee + message.DAT.thdPrtyFee + message.DAT.othrFee;
            var netFee = fee + vat;
            var products = message.DAT.prdcts;

            if (products && products.length > 0) {
                proId = products[0].proID;
                product = this.tradeService.productDS.getProductById(proId);
            }

            if (product) {
                product.setData({expExpDte: expExpDate, expFrmDte: expFromDate, feeCurr: feeCurrency, fee: fee, vat: vat, netFee: netFee});
            }
        }
    },

    _processConsolidatedReport: function (message) {
        if (message && message.DAT) {
            var conRepData = message.DAT.consoldRep;
            var that = this;

            Ember.$.each(conRepData, function (repId, repObj) {
                var consolidatedReport = that.tradeService.consolidatedReportDS.getConReport(repId);

                if (consolidatedReport) {
                    consolidatedReport.setData(repObj);
                }
            });
        }
    },

    _processKYCMasterData: function (message) {
        if (message && message.DAT) {
            var masterData = message.DAT.mstData;
            var that = this;

            if (masterData) {
                Ember.$.each(masterData, function (index, field) {
                    var dataColl = field.dataCol;

                    Ember.$.each(dataColl, function (indexOption, option) {
                        option.des = utils.formatters.convertUnicodeToNativeString(option.des);
                    });

                    that.tradeService.kycMasterDS.setTypes(field.typId, dataColl, message.DAT.langKey);
                });
            }
        }
    },

    _setCustomerExgLst: function (message, userExchanges) {
        var exgAccLst = {};
        var exgArray = [];

        if (message.exgAccLst) {
            Ember.$.each(message.exgAccLst, function (exgAccIndex, exgAcc) {
                var exg = exgAcc.exg;

                if (exgAcc && userExchanges.indexOf(exg) === -1) {
                    userExchanges[userExchanges.length] = exg;
                }

                // Process book keepers
                if (exgAcc.bkLst) {
                    var defBkId = '';

                    Ember.$.each(exgAcc.bkLst, function (bkIndex, bKeeper) {
                        if (bKeeper && utils.validators.isAvailable(bKeeper.bkId)) {
                            if (bKeeper.isDeflt === '1' || defBkId === utils.Constants.StringConst.Empty) {
                                defBkId = bKeeper.bkId;
                            }
                        }
                    });

                    exgAcc.defBkId = defBkId;
                }

                exgAccLst[exg] = exgAcc;
                exgArray[exgArray.length] = exg;
            });
        }

        message.exgAccLst = exgAccLst;
        message.exgArray = exgArray;
    },

    _processConfigForExchange: function (message) {
        if (message && message.DAT) {
            var that = this;

            this.tradeService.tradeMetaDS.tradeMeta.setData({exgLevelConfig: message});
            this.tradeService.tradeMetaDS.tradeMeta.save();

            if (message.DAT.exgConf) {
                Ember.$.each(message.DAT.exgConf, function (exgConfIndex, exgConfData) {
                    if (exgConfData) {
                        var tradeMetaDS = that.tradeService.tradeMetaDS;
                        var orderSide = exgConfData.side ? exgConfData.side : tradeMetaDS.defaultOrderSide;
                        var orderType = exgConfData.type ? exgConfData.type : tradeMetaDS.defaultOrderType;
                        var gtdMax = exgConfData.gtdMax ? exgConfData.gtdMax : tradeMetaDS.defaultGtdMax; // If gtdMax = 0; allow unlimited days.
                        var prcLmtChk = exgConfData.prcLmtChk ? exgConfData.prcLmtChk : tradeMetaDS.defaultPrcLmtChk;

                        tradeMetaDS.setOrderSides(exgConfData.exg, orderSide.replace(/\s/g, '').split(','));
                        tradeMetaDS.setOrderTypes(exgConfData.exg, orderType.replace(/\s/g, '').split(','));
                        tradeMetaDS.setExgGTDExpiry(exgConfData.exg, gtdMax);
                        tradeMetaDS.setExgPriceLimitCheck(exgConfData.exg, prcLmtChk);
                    }
                });
            }

            if (message.DAT.tifRuleSet) {
                Ember.$.each(message.DAT.tifRuleSet, function (tifRuleIndex, tifRuleData) {
                    if (tifRuleData) {
                        var tifStrValues = tifRuleData.tifTyps.split(',');

                        if (tifStrValues && tifStrValues.length > 0) {
                            var tifIntValues = [];

                            Ember.$.each(tifStrValues, function (index, tif) {
                                tifIntValues[tifIntValues.length] = parseInt(tif, 10);
                            });

                            that.tradeService.tradeMetaDS.setTifTypes(tifRuleData.exg, tifRuleData.ordTyp, tifRuleData.mktSts, tifIntValues);
                        }
                    }
                });

                this._generateTifMktStatusMap(message.DAT.tifRuleSet);
            }
        }

        this.tradeService.onExchangeConfigSuccess(this.callbacks.auth.isReconnection);
    },

    _generateTifMktStatusMap: function (tifData) {
        var tifMktStatusMap = this.tradeService.tradeMetaDS.tifMktStatusDependencyMap;
        var exgTifMap = {};

        Ember.$.each(tifData, function (idx, tifRuleData) {
            if (!exgTifMap[tifRuleData.exg]) {
                exgTifMap[tifRuleData.exg] = [];
            }

            exgTifMap[tifRuleData.exg][exgTifMap[tifRuleData.exg].length] = tifRuleData;
        });

        Ember.$.each(exgTifMap, function (exgIdx, tifDataObj) {
            if (!tifMktStatusMap[exgIdx] && tifMktStatusMap[exgIdx] !== false) {
                tifMktStatusMap[exgIdx] = false;
            }

            Ember.$.each(tifDataObj, function (tIdx, tRuleData) {
                Ember.$.each(tifDataObj, function (idx, ruleData) {
                    if (tRuleData.mktSts !== ruleData.mktSts && tRuleData.tifTyps !== ruleData.tifTyps) {
                        tifMktStatusMap[tRuleData.exg] = true;
                        return false;
                    }
                });

                return false;
            });
        });
    },

    _processExchangeRules: function (message) {
        var that = this;

        if (message && message.DAT && message.DAT.ruleLst) {
            Ember.$.each(message.DAT.ruleLst, function (exgIndex, exgRule) {
                if (exgRule && exgRule.grpId) {
                    switch (exgRule.grpId) {
                        case that.exchangeRule.tifByType:
                            var conditions = exgRule.condition ? exgRule.condition.split('|') : [];

                            Ember.$.each(conditions, function (ruleIndex, ruleSet) {
                                var rule = ruleSet ? ruleSet.split(':') : [];

                                if (rule.length > 1) {
                                    that.tradeService.tradeMetaDS.setTifByType(exgRule.exg, rule[0], rule[1]);
                                }
                            });
                            break;
                    }
                }
            });
        }
    },

    _processBuyingPower: function (message) {
        if (message && message.DAT && message.DAT.buyPwrLst) {
            var that = this;

            Ember.$.each(message.DAT.buyPwrLst, function (buyPowerIndex, buyingPower) {
                if (buyingPower) {
                    var cashAccount = that.tradeService.accountDS.getCashAccount(buyingPower.cshAccNum);

                    if (cashAccount) {
                        cashAccount.setData(buyingPower);
                    }
                }
            });
        }
    },

    _processBankAccounts: function (message) {
        if (message && message.DAT && message.DAT.bankDtails) {
            var bankData = message.DAT.bankDtails;
            var that = this;

            Ember.$.each(bankData, function (bankIndex, bankField) {
                var bankId = bankField.bankCode;
                var accList = bankField.bnkAccs;

                if (accList) {
                    Ember.$.each(accList, function (accIndex, accField) {
                        // Passing Asterisk as cash account id to map bank accounts to all cash accounts
                        var bank = that.tradeService.bankAccountDS.getBankAcc(accField.bnkAccNum, utils.Constants.StringConst.Asterisk);
                        Ember.set(accField, 'bankId', bankId);

                        if (bank) {
                            bank.setData(accField);
                        }

                    });
                }
            });
        }
    },

    _processDesBankAccounts: function (message) {
        if (message && message.DAT && message.DAT.bankDtails) {
            var bankData = message.DAT.bankDtails;
            var that = this;

            Ember.$.each(bankData, function (bankIndex, bankField) {
                var bankId = bankField.bankCode;
                var accList = bankField.bnkAccs;

                if (accList) {
                    Ember.$.each(accList, function (accIndex, accField) {

                        var bank = that.tradeService.bankAccountDS.getDesBankAcc(accField.bnkAccNum);
                        Ember.set(accField, 'bankId', bankId);

                        if (bank) {
                            bank.setData(accField);
                        }

                    });
                }
            });
        }
    },

    _processHoldings: function (message) {
        if (message && message.DAT && message.DAT.portfls) {
            var that = this;

            Ember.$.each(message.DAT.portfls, function (holdingIndex, holdingData) {
                if (holdingData) {
                    var holding = that.tradeService.holdingDS.getHolding(holdingData.secAccNum, holdingData.symbol, holdingData.exg, holdingData.instruTyp, true);

                    if (holding) {
                        holding.setData(holdingData);

                        if (holding.qty === 0 && holding.secAccNum && holding.symbol) {
                            that.tradeService.holdingDS.removeHolding(holding.secAccNum, holding.symbol);
                        }
                    }
                }
            });
        }
    },

    _processOrderList: function (message) {
        if (message && message.DAT && message.DAT.ordLst) {
            var that = this;
            this.tradeService.beginBulkOrderAddition();

            Ember.$.each(message.DAT.ordLst, function (orderIndex, orderData) {
                if (orderData) {
                    that.tradeService.orderDS.getOrder(orderData.mubOrdNum, orderData.exg, orderData.symbol, orderData.instruTyp, orderData.ordSts, orderData.orgClOrdId, orderData);
                }
            });

            Ember.run.later(function () {   // Response message is not coming in a single message, but in ms range intervals
                that.tradeService.endBulkOrderAddition();
            }, 500);
        }
    },

    _processOrderSearch: function (message) {
        if (message && message.DAT) {
            var wkey = message.DAT.unqReqId;
            var widgetSearchResult = this.tradeService.orderSearchDS.getWidgetSearchResult(wkey);
            var that = this;

            this.tradeService.beginBulkOrderAddition();

            Ember.$.each(message.DAT, function (key, value) {
                if (key !== 'ordLst') {
                    Ember.set(widgetSearchResult, key, value);
                } else {
                    Ember.$.each(message.DAT.ordLst, function (orderIndex, orderData) {
                        if (orderData) {
                            var order = that.tradeService.orderSearchDS.getOrder(orderData.clOrdId, wkey, orderData.symbol, orderData.exg, orderData.instruTyp);

                            if (order) {
                                if (utils.validators.isAvailable(orderData.txt)) {
                                    orderData.txt = languageDataStore.generateLangMessage(utils.formatters.convertUnicodeToNativeString(orderData.txt));
                                }

                                order.setData(orderData);
                            }
                        }
                    });
                }
            });

            Ember.run.later(function () {   // Response message is not coming in a single message, but in ms range intervals
                that.tradeService.endBulkOrderAddition();
            }, 500);
        }
    },

    _processNormalOrderResponse: function (message) {
        if (message && message.DAT) {
            var orderData = message.DAT;
            this.tradeService.orderDS.getOrder(orderData.mubOrdNum, orderData.exg, orderData.symbol, orderData.instruTyp, orderData.ordSts, orderData.orgClOrdId, orderData);
        }
    },

    _processBulkOrderCancelResponse: function (message) {
        if (message && message.DAT) {
            var bulkOrderCanceldata = message.DAT;
            var unqReqId = bulkOrderCanceldata.unqReqId;
            var sts = bulkOrderCanceldata.resSts;
            var rsn = bulkOrderCanceldata.resRes;

            if (utils.validators.isAvailable(unqReqId)) {
                var bulkOrder = this.tradeService.bulkOrderCancelRequestMap[unqReqId];
                bulkOrder.set('sts', sts);
                bulkOrder.set('rsn', rsn);
            }
        }
    },

    _processTransactionResponse: function (message) {
        if (message && message.DAT) {
            var transData = message.DAT;
            var trans = this.tradeService.transDS.getTransaction(transData.transId);

            if (trans) {
                trans.setData(trans);
            }

            var callbackFn = this.tradeService.withdrawalCallback;

            if (callbackFn && Ember.$.isFunction(callbackFn)) {
                callbackFn(message.DAT.sts, message.DAT.reason);
            }
        }
    },

    _processTransactionList: function (message) {
        if (message && message.DAT && message.DAT.cshTrans) {
            var that = this;

            Ember.$.each(message.DAT.cshTrans, function (tranIndx, transactionData) {
                if (transactionData) {
                    var transaction = that.tradeService.transDS.getTransaction(transactionData.chTranId);

                    if (transaction) {
                        transaction.setData(transactionData);
                    }
                }
            });
        }
    },

    _processOrderCommission: function (message) {
        if (message && message.DAT && message.DAT.comsn) {
            var orderData = message.DAT;
            var comsn = orderData.comsn && orderData.comsn !== -1 ? orderData.comsn : 0;
            var unqReqId = orderData.unqReqId;

            if (utils.validators.isAvailable(unqReqId)) {
                var order = this.tradeService.orderCommissionRequestMap[unqReqId];

                if (order && !(order.get('isDestroyed') || order.get('isDestroying'))) {
                    order.set('comsn', comsn);
                    order.set('vatAmount', orderData.vatAmount ? orderData.vatAmount : 0);
                    delete this.tradeService.orderCommissionRequestMap[unqReqId];
                }
            }
        }
    },

    _processSymbolMarginBP: function (message) {
        if (message && message.DAT && message.DAT.smblmgnBuyPwr) {
            var dat = message.DAT;
            var smblmgnBuyPwr = dat.smblmgnBuyPwr;
            var unqReqId = dat.unqReqId;

            if (utils.validators.isAvailable(unqReqId)) {
                var order = this.tradeService.symbolMarginRequestMap[unqReqId];

                if (order && !(order.get('isDestroyed') || order.get('isDestroying'))) {
                    order.set('smblmgnBuyPwr', smblmgnBuyPwr);
                    order.set('isMar', true);

                    delete this.tradeService.symbolMarginRequestMap[unqReqId];
                }
            }
        }
    },

    _processCashTranfer: function (message) {
        if (message && message.DAT) {
            var cashTranferData = message.DAT;
            var sts = cashTranferData.sts;
            var rsn = cashTranferData.rsn ? cashTranferData.rsn : '';
            var unqReqId = cashTranferData.unqReqId;

            if (utils.validators.isAvailable(unqReqId)) {
                var cashTransfer = this.tradeService.cashTransferRequestMap[unqReqId];
                cashTransfer.set('sts', sts);
                cashTransfer.set('rsn', rsn);
            }
        }
    },

    _processCurrConvertRequest: function (message) {
        if (message && message.DAT) {
            var currRateData = message.DAT;
            var sts = currRateData.status;
            var unqReqId = currRateData.unqReqId;

            if (utils.validators.isAvailable(unqReqId)) {
                var currRate = this.tradeService.currConvertRequestMap[unqReqId];
                currRate.set('sts', sts);
            }
        }
    },

    _processCurrencyRate: function (message) {
        if (message && message.DAT) {
            var currRateData = message.DAT;
            var fxRate = currRateData.currRate;
            var amnt = currRateData.amnt;
            var transType = currRateData.transType;
            var unqReqId = currRateData.unqReqId;

            if (utils.validators.isAvailable(unqReqId)) {
                var currRate = this.tradeService.currConvertRequestMap[unqReqId];
                currRate.set('fxRate', fxRate);
                currRate.set('amnt', amnt);
                currRate.set('transType', transType);
            }
        }
    },

    _processCashStatement: function (message) {
        try {
            if (message && message.DAT) {
                if (message.DAT.headColumns) {
                    this._processCashStatementMixProtocol(message.DAT);
                } else {
                    this._processCashStatementArray(message.DAT);
                }
            }
        } catch (e) {
            utils.logger.logError('Error in processing cash statement response : ' + e);
        }

    },

    _processCashStatementMixProtocol: function (message) {
        var headerFields = ['date', 'trxType', 'tranRef', 'particulars', 'amount', 'balance', 'settleDate'];
        var cashStatementHedIdxList = message.headColumns.split(utils.Constants.StringConst.Comma).indicesOf(headerFields);
        var cashStatements = message.dataSet ? message.dataSet.split(utils.Constants.StringConst.SemiColon) : [];
        var that = this;

        Ember.$.each(cashStatements, function (key, value) {
            var cashStatementData = value.split(utils.Constants.StringConst.Comma);
            var date = cashStatementData[cashStatementHedIdxList.date];
            var particulars = cashStatementData[cashStatementHedIdxList.particulars];
            var amount = cashStatementData[cashStatementHedIdxList.amount];
            var balance = cashStatementData[cashStatementHedIdxList.balance];
            var transactionType = cashStatementData[cashStatementHedIdxList.trxType];
            var transactionRef = cashStatementData[cashStatementHedIdxList.tranRef];
            var settleDate = cashStatementData[cashStatementHedIdxList.settleDate];

            if (transactionRef) {
                var cashStatement = that.tradeService.cashStatementDS.getCashStatement(transactionRef);

                cashStatement.setData({
                    date: date,
                    trnsType: transactionType,
                    particulars: particulars,
                    amount: amount,
                    balance: balance,
                    settleDate: settleDate
                });
            }
        });
    },

    _processCashStatementArray: function (data) {
        var currency = data.curr;
        var openingBal = data.openBal;
        var totDeposit = data.totDeposit;
        var totWithdrawal = data.totWithdrawal;
        var totBuy = data.totBuy;
        var totSell = data.totSell;
        var totOther = data.totOther;
        var closeBal = data.closeBal;
        var that = this;

        if (data.cashLogs) {
            Ember.$.each(data.cashLogs, function (tranIndx, transactionData) {
                if (transactionData) {
                    var transaction = that.tradeService.cashStatementDS.getCashStatement(transactionData.trnsRef);

                    if (transaction) {
                        transaction.setData(transactionData);
                    }
                }
            });
        }

        that.tradeService.cashStatementDS.setSummationInfo({
            curr: currency,
            openBal: openingBal,
            totDeposit: totDeposit,
            totWithdrawal: totWithdrawal,
            totBuy: totBuy,
            totSell: totSell,
            totOther: totOther,
            closeBal: closeBal
        });
    },

    _processStockStatement: function (message) {
        try {
            if (message && message.DAT) {
                if (message.DAT.headColumns && message.DAT.dataSet) {
                    this._processStockStatementMixProtocol(message.DAT);
                } else {
                    this._processStockStatementArray(message.DAT);
                }
            }
        } catch (e) {
            utils.logger.logError('Error in processing stock statement response : ' + e);
        }
    },

    _processStockStatementMixProtocol: function (data) {
        var headerFields = ['date', 'sym', 'tranRef', 'symName', 'des', 'qty'];
        var stockStatementHedIdxList = data.headColumns.split(utils.Constants.StringConst.Comma).indicesOf(headerFields);
        var stockStatements = data.dataSet.split(utils.Constants.StringConst.SemiColon);
        var that = this;

        Ember.$.each(stockStatements, function (key, value) {
            var stockStatementData = value.split(utils.Constants.StringConst.Comma);
            var date = stockStatementData[stockStatementHedIdxList.date];
            var des = stockStatementData[stockStatementHedIdxList.des];
            var transactionRef = stockStatementData[stockStatementHedIdxList.tranRef];
            var sym = stockStatementData[stockStatementHedIdxList.sym];
            var symName = stockStatementData[stockStatementHedIdxList.symName];
            var qty = stockStatementData[stockStatementHedIdxList.qty];

            if (transactionRef) {
                var stockStatement = that.tradeService.stockStatementDS.getStockStatement(transactionRef);

                stockStatement.setData({
                    date: date,
                    sym: sym,
                    symName: symName,
                    des: des,
                    qty: qty
                });
            }
        });
    },

    _processStockStatementArray: function (data) {
        if (data.holdingLogs) {
            var that = this;

            Ember.$.each(data.holdingLogs, function (tranIndx, transactionData) {
                if (transactionData) {
                    var transaction = that.tradeService.stockStatementDS.getStockStatement(transactionData.tranRef, transactionData.exg, transactionData.sym);

                    if (transaction) {
                        transaction.setData(transactionData);
                    }
                }
            });
        }
    },

    _processKYCUpdate: function (message) {
        if (message && message.DAT) {
            var kycUpdateData = message.DAT;
            var unqReqId = kycUpdateData.unqReqId;

            if (utils.validators.isAvailable(unqReqId)) {
                var kycUpdateReq = this.tradeService.kycUpdateRequestMap[unqReqId]; // Map

                if (kycUpdateData.nextDte) {
                    kycUpdateReq.set('nextDate', kycUpdateData.nextDte);
                }

                if (kycUpdateData.refNo) {
                    kycUpdateReq.set('refNo', kycUpdateData.refNo);
                }

                kycUpdateReq.set('rsn', kycUpdateData.rsn);
                kycUpdateReq.set('sts', parseInt(kycUpdateData.sts, 10));
            }
        }
    },

    // Registration Step 1 - Registration Detail
    _processRegistrationCustomerDetail: function (message) {
        if (message && message.DAT) {
            var registrationObj = this.tradeService.registrationDS.getRegistration();
            var cusDetailStatus = message.DAT.custStus;

            if (registrationObj) {
                if (cusDetailStatus === tradeConstants.RegistrationStatus.Success) {
                    this.tradeService.registrationDS.setData(message.DAT);
                } else {
                    var rejectedReason = languageDataStore.generateLangMessage(message.DAT.rejResn);
                    Ember.set(registrationObj, 'cusDetailStsMsg', rejectedReason);
                }

                Ember.set(registrationObj, 'cusDetailSts', cusDetailStatus);
            }
        }
    },

    // Registration Step 2 - OTP Login
    _processRegistrationOtpLoginDetail: function (message) {
        if (message && message.DAT) {
            var registrationObj = this.tradeService.registrationDS.getRegistration();
            var otpStatus = message.DAT.stats;

            if (registrationObj) {
                if (otpStatus !== tradeConstants.RegistrationStatus.Success) {
                    var rejectedReason = languageDataStore.generateLangMessage(message.DAT.rejctResn);
                    Ember.set(registrationObj, 'otpStsMsg', rejectedReason);
                }

                Ember.set(registrationObj, 'otpSts', otpStatus); // To fire the reg-otp observer
            }
        }
    },

    // Registration Step 4 - Password Verification
    _processRegistrationPasswordConfirmation: function (message) {
        if (message && message.DAT) {
            var registrationObj = this.tradeService.registrationDS.getRegistration();
            var pwdStatus = message.DAT.stats;

            if (registrationObj) {
                if (pwdStatus !== tradeConstants.RegistrationStatus.Success) {
                    var rejectedReason = languageDataStore.generateLangMessage(message.DAT.rejctResn);
                    Ember.set(registrationObj, 'pwdStsMsg', rejectedReason);
                }

                Ember.set(registrationObj, 'pwdSts', pwdStatus);
            }
        }
    },

    _processNinIqamaValidation: function (message) {
        if (message && message.DAT) {
            var callbackFn = this.tradeService.ninIqamaCallback;
            var rejectedReason = languageDataStore.generateLangMessage(message.DAT.rejResn);

            if (callbackFn && Ember.$.isFunction(callbackFn)) {
                callbackFn(message.DAT.custStus, rejectedReason);
            }
        }
    },

    _processCaptchaImage: function (message) {
        if (message && message.DAT) {
            var callbackFn = this.tradeService.captchaImgCallback;

            if (callbackFn && Ember.$.isFunction(callbackFn)) {
                callbackFn(message.DAT.img);
            }
        }
    },

    _processCaptchaText: function (message) {
        if (message && message.DAT) {
            var callbackFn = this.tradeService.captchaTextCallback;

            if (callbackFn && Ember.$.isFunction(callbackFn)) {
                callbackFn(message.DAT.sts);
            }
        }
    }
});
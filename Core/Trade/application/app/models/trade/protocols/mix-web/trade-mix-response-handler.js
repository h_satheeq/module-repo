import Ember from 'ember';
import appConfig from '../../../../config/app-config';
import utils from '../../../../utils/utils';

export default (function () {
    var processBrokerageConnectivityResponse = function (dataObj) {
        try {
            var brokeragesCollection = {};

            if (dataObj && dataObj.TradeConnection && dataObj.TradeConnection.Country) {
                var countries = dataObj.TradeConnection.Country;
                var brokers;

                if (countries.length > 0) {
                    if (countries.length === 1) {
                        brokers = countries[0].Broker; // Broker returns array of brokers of a country
                    } else {
                        Ember.$.each(countries, function (index, country) {
                            if (country.id === appConfig.customisation.brokerageCountryId) {
                                brokers = country.Broker;
                            }
                        });
                    }
                }

                if (brokers) {
                    Ember.$.each(brokers, function (index, broker) {
                        brokeragesCollection[broker.id] = broker;
                    });
                }

                return brokeragesCollection;
            }
        } catch (e) {
            utils.logger.logError('Error in processing the brokerage connectivity data response : ' + e);
        }
    };

    return {
        processBrokerageConnectivityResponse: processBrokerageConnectivityResponse
    };
})();
import utils from '../../../../utils/utils';
import sharedService from '../../../../models/shared/shared-service';

export default (function () {
    var generateBrokerageConnectivityUrl = function () {
        return utils.requestHelper.generateQueryString(sharedService.getService('trade').settings.urlTypes.brokerage);
    };

    return {
        generateBrokerageConnectivityUrl: generateBrokerageConnectivityUrl
    };
})();

import Ember from 'ember';
import WebConnection from '../../../shared/communication-adapters/web-http-connection';
import ResponseHandler from './trade-mix-response-handler';
import RequestGenerator from './trade-mix-request-generator';

export default (function () {
    var loadBrokerageConnectivityData = function (successFn) {
        WebConnection.sendAjaxRequest({
            url: RequestGenerator.generateBrokerageConnectivityUrl(),

            onSuccess: function (dataObj) {
                var brokerageMap = ResponseHandler.processBrokerageConnectivityResponse(dataObj);

                if (Ember.$.isFunction(successFn)) {
                    successFn(brokerageMap);
                }
            }
        });
    };

    return {
        loadBrokerageConnectivityData: loadBrokerageConnectivityData
    };
})();
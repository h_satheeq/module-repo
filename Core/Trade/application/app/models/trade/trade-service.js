/* global Queue */
import Ember from 'ember';
import WebSocketManager from '../shared/communication-adapters/web-socket-manager';
import tradeSocketRequestHandler from './protocols/web-socket/trade-socket-request-handler'; // Singleton request generator object
import TradeSocketResponseHandler from './protocols/web-socket/trade-socket-response-handler'; // Class blue-print
import tradeConstants from './trade-constants';
import tradeSubscriptionManager from '../../models/trade/trade-subscription-manager';
import utils from '../../utils/utils';
import tradeUser from './business-entities/trade-user';
import tradeDataStoreFactory from './data-stores/trade-data-store-factory';
import sharedService from '../../models/shared/shared-service';
import languageDataStore from '../shared/language/language-data-store';
import mixRequestHandler from './protocols/mix-web/trade-mix-request-handler';
import appConfig from '../../config/app-config';

export default Ember.Object.extend({
    // Generate single web socket manager instance specific to trade service
    webSocketManager: undefined,
    connectionStatus: false,
    reqQueue: new Queue(),
    userDS: tradeUser,

    subscriptionKey: 'trade',
    settings: {},

    orderDS: undefined,
    holdingDS: undefined,
    tradeMetaDS: undefined,
    transDS: undefined,
    orderSearchDS: undefined,
    cashStatementDS: undefined,
    bankAccountDS: undefined,
    savedOrdersDS: undefined,
    accountDS: undefined,

    tradeMetaReadySubscriptions: {},
    tradeDisconnectSubscriptions: {},
    orderCommissionRequestMap: {},
    mutualFundVATRequestMap: {},
    symbolMarginRequestMap: {},
    cashTransferRequestMap: {},
    currConvertRequestMap: {},
    rightSubscriptionRequestMap: {},
    kycUpdateRequestMap: {},
    bulkOrderCancelRequestMap: {},
    fieldConfigs: {},
    authSuccessSubscription: {},
    changePasswordCallback: {},
    tCGetCallback: {},
    tCUpdateCallback: {},
    redemptionCallback: {},
    subscriptionCallback: {},
    userTypeCallback: {},
    withdrawalCallback: {},
    kycCallback: {},
    ninIqamaCallback: {},
    captchaImgCallback: {},
    captchaTextCallback: {},
    isBulkOrderAdditionInProgress: false,

    isTradeMetaReady: false,
    isAuthSuccess: false,
    socketReqQueue: new Queue(),
    isAuthResponseReceived: false,

    init: function () {
        this._super();
        this.webSocketManager = new WebSocketManager(tradeSocketRequestHandler, TradeSocketResponseHandler, this, tradeSubscriptionManager);
        this.set('app', languageDataStore.getLanguageObj());
    },

    isTradeMetadataReady: function () {
        return this.get('isTradeMetaReady');
    },

    isAuthenticated: function () {
        return this.get('isAuthSuccess');
    },

    setConnectionStatus: function (stat) {
        this.set('connectionStatus', stat);

        if (!stat) {
            this.onTradeDisconnect();
        }
    },

    isConnected: function () {
        return this.webSocketManager.isConnected(tradeConstants.SocketConnectionType.OMS);
    },

    createDataStores: function () {
        this.set('orderDS', tradeDataStoreFactory.createOrderDataStore(this));
        this.set('holdingDS', tradeDataStoreFactory.createHoldingDataStore(this));
        this.set('accountDS', tradeDataStoreFactory.createAccountDataStore(this));
        this.set('tradeMetaDS', tradeDataStoreFactory.createTradeMetaDataStore(this));
        this.set('transDS', tradeDataStoreFactory.createTransactionDataStore(this));
        this.set('orderSearchDS', tradeDataStoreFactory.createOrderSearchDataStore(this));
        this.set('cashStatementDS', tradeDataStoreFactory.createCashStatementDataStore(this));
        this.set('bankAccountDS', tradeDataStoreFactory.createBankDataStore(this));
        this.set('savedOrdersDS', tradeDataStoreFactory.createSavedOrdersDataStore(this));
    },

    subscribeAuthSuccess: function (subscriber, key) {
        this.get('authSuccessSubscription')[key] = subscriber;
    },

    authenticateWithUsernameAndPassword: function (authParams) {
        this.set('isAuthResponseReceived', false);

        authParams.authFailed = this._modifyAuthCallback(authParams.authFailed);
        authParams.authSuccess = this._modifyAuthCallback(authParams.authSuccess);

        var req = tradeSocketRequestHandler.generateRetailAuthRequest(authParams);
        this.webSocketManager.sendAuth(req, tradeConstants.SocketConnectionType.OMS, authParams);

        this._checkAuthResponse(authParams);
    },

    authenticateWithSsoToken: function (authParams) {
        var req = tradeSocketRequestHandler.generateSsoAuthRequest(authParams);
        this.webSocketManager.sendAuth(req, tradeConstants.SocketConnectionType.OMS, authParams);
    },

    authenticateLevel2: function (authParams) {
        var req = tradeSocketRequestHandler.generateLevel2AuthRequest(authParams);
        this.webSocketManager.sendData([req], tradeConstants.SocketConnectionType.OMS);
        return authParams;
    },

    logout: function () {
        var req = tradeSocketRequestHandler.generateLogoutRequest();
        this.webSocketManager.sendAuth(req, tradeConstants.SocketConnectionType.OMS);
    },

    loadCustomerDetails: function () {
        var req = tradeSocketRequestHandler.generateCustomerDetailsRequest();
        this.webSocketManager.sendData([req], tradeConstants.SocketConnectionType.OMS);
    },

    loadExchangeLevelConfig: function () {
        var req = tradeSocketRequestHandler.generateExchangeConfigRequest();
        this.webSocketManager.sendData([req], tradeConstants.SocketConnectionType.OMS);
    },

    loadExchangeRules: function () {
        var req = tradeSocketRequestHandler.generateRuleRequest();
        this.webSocketManager.sendData([req], tradeConstants.SocketConnectionType.OMS);
    },

    /* *
     * Send order ticket to oms
     * @param order - Order object
     */
    sendOrderTicket: function (order) {
        var req = tradeSocketRequestHandler.generateNormalOrderRequest(order);
        this.webSocketManager.sendData([req], tradeConstants.SocketConnectionType.OMS);

        return true;
    },

    sendAmendOrder: function (order) {
        var req = tradeSocketRequestHandler.generateAmendOrderRequest(order);
        this.webSocketManager.sendData([req], tradeConstants.SocketConnectionType.OMS);

        return true;
    },

    sendCancelOrder: function (order) {
        var req = tradeSocketRequestHandler.generateCancelOrderRequest(order);
        this.webSocketManager.sendData([req], tradeConstants.SocketConnectionType.OMS);

        return true;
    },

    onAuthenticationSuccess: function (isReconnection) {
        var authSuccessSubscription = this.get('authSuccessSubscription');
        this.set('isAuthSuccess', true);

        if (authSuccessSubscription) {
            Ember.$.each(authSuccessSubscription, function (id, subscriber) {
                if (subscriber && Ember.$.isFunction(subscriber.onAuthSuccess)) {
                    subscriber.onAuthSuccess();
                }
            });
        }

        this.loadCustomerDetails();
        this._sendQueuedSocketRequest();

        if (appConfig.customisation.showFailedLoginAttempt && !isReconnection) {
            this._showFailedLoginAttempt();
        }
    },

    _showFailedLoginAttempt: function () {
        var failedLoginAttempt = this.userDS.get('failAtmps');
        var failedLoginAttemptMsg;

        if (failedLoginAttempt && failedLoginAttempt > 0) {
            failedLoginAttemptMsg = this.app.lang.messages.failedLoginAttempt;
            failedLoginAttemptMsg = failedLoginAttemptMsg.replace('[FailedLoginAttemptCount]', failedLoginAttempt);
        }

        if (appConfig.customisation.isMobile) {
            var lstLgnTme = this.userDS.get('lstLgnTme');
            var userDetailMsg = [this.app.lang.labels.lastLogin, utils.formatters.formatToDateTime(lstLgnTme)].join(': ');

            if (failedLoginAttemptMsg) {
                userDetailMsg = [userDetailMsg, failedLoginAttemptMsg].join(', ');
            }

            utils.messageService.showMessage(userDetailMsg, utils.Constants.MessageTypes.Error, false, this.app.lang.labels.information);
        } else if (failedLoginAttemptMsg) {
            utils.messageService.showMessage(failedLoginAttemptMsg, utils.Constants.MessageTypes.Error, false, this.app.lang.labels.warning);
        }
    },

    onCustomerDetailSuccess: function () {
        this.loadExchangeLevelConfig();
    },

    onExchangeConfigSuccess: function (isReconnection) {
        this.onTradeMetaReady(isReconnection);
        // this.loadExchangeRules(); // Disabling exchange rule request since it is not used.
        this._sendQueuedData();
        this.sendBankAccountRequest();
        this.sendDestBankAccountRequest();
    },

    sendBankAccountRequest: function () {
        if (this.isAuthenticated()) {
            this._sendBankAccountRequest();
        } else {
            this.socketReqQueue.enqueue({
                callbackFn: this._sendBankAccountRequest,
                args: [] // Queue arguments as an array
            });
        }
    },

    _sendBankAccountRequest: function () {
        var req = tradeSocketRequestHandler.generateBankAccountsRequest({sMubNo: this.userDS.get('mubNo')});
        this.webSocketManager.sendData([req], tradeConstants.SocketConnectionType.OMS);
    },

    sendDestBankAccountRequest: function () {
        var req = tradeSocketRequestHandler.generateDestBankAccountRequest({instId: this.userDS.get('instId')});
        this.webSocketManager.sendData([req], tradeConstants.SocketConnectionType.OMS);
    },

    sendBuyingPowerRequest: function (tradingAcc) {
        if (this.isConnected()) {
            var req = tradeSocketRequestHandler.generateBuyingPowerRequest(tradingAcc);
            this.webSocketManager.sendData([req], tradeConstants.SocketConnectionType.OMS);
        } else {
            this.reqQueue.enqueue({
                reqGen: tradeSocketRequestHandler.generateBuyingPowerRequest,
                args: tradingAcc
            });
        }
    },

    sendHoldingsRequest: function (tradingAcc) {
        if (this.isConnected()) {
            var req = tradeSocketRequestHandler.generateHoldingsRequest(tradingAcc);
            this.webSocketManager.sendData([req], tradeConstants.SocketConnectionType.OMS);
        } else {
            this.reqQueue.enqueue({
                reqGen: tradeSocketRequestHandler.generateHoldingsRequest,
                args: tradingAcc
            });
        }
    },

    sendOrderListRequest: function (tradingAccId, ordCategory) {
        if (this.isConnected()) {
            var req = tradeSocketRequestHandler.generateOrderListRequest({
                tradingAccId: tradingAccId,
                ordCategory: ordCategory
            });

            if (req) {
                this.webSocketManager.sendData([req], tradeConstants.SocketConnectionType.OMS);
            }
        } else {
            this.reqQueue.enqueue({
                reqGen: tradeSocketRequestHandler.generateOrderListRequest,
                args: {
                    tradingAccId: tradingAccId,
                    ordCategory: ordCategory
                }
            });
        }
    },

    /* *
     * Send deposit request to oms
     * @param transaction - Deposit object
     */
    sendDepositRequest: function (transaction) {
        var req = tradeSocketRequestHandler.generateDepositRequest(transaction);
        this.webSocketManager.sendData([req], tradeConstants.SocketConnectionType.OMS);
    },

    /* *
     * Send withdrawal request to oms
     * @param transaction - Withdrawal object
     */
    sendWithdrawalRequest: function (transaction, callbackFn) {
        var req = tradeSocketRequestHandler.generateWithdrawalRequest(transaction);
        this.webSocketManager.sendData([req], tradeConstants.SocketConnectionType.OMS);

        this.set('withdrawalCallback', callbackFn);
    },

    sendCashTransferRequest: function (unqReqId, transaction, transactionObj) {
        var cashTransferRequestMap = this.get('cashTransferRequestMap');
        cashTransferRequestMap[unqReqId] = transaction;

        var req = tradeSocketRequestHandler.generateCashTransferRequest(transactionObj);
        this.webSocketManager.sendData([req], tradeConstants.SocketConnectionType.OMS);
    },

    sendCurrenyRateRequest: function (unqReqId, transaction, transactionObj) {
        var currConvertRequestMap = this.get('currConvertRequestMap');
        currConvertRequestMap[unqReqId] = transaction;

        var req = tradeSocketRequestHandler.generateCurrenyRateRequest(transactionObj);
        this.webSocketManager.sendData([req], tradeConstants.SocketConnectionType.OMS);
    },

    sendTransactionListRequest: function (historyReq) {
        if (this.isConnected()) {
            var req = tradeSocketRequestHandler.generateTransactionHistoryRequest(historyReq);
            this.webSocketManager.sendData([req], tradeConstants.SocketConnectionType.OMS);
        } else {
            this.reqQueue.enqueue({
                reqGen: tradeSocketRequestHandler.generateTransactionHistoryRequest,
                args: historyReq
            });
        }
    },

    subscribeTradeMetaReady: function (subscriber, key) {
        if (utils.validators.isAvailable(key)) {
            var tradeMetaReadySubscriptions = this.get('tradeMetaReadySubscriptions');
            tradeMetaReadySubscriptions[key] = subscriber;
        }
    },

    unSubscribeTradeMetaReady: function (key) {
        if (utils.validators.isAvailable(key)) {
            var tradeMetaReadySubscriptions = this.get('tradeMetaReadySubscriptions');
            tradeMetaReadySubscriptions[key] = undefined;
        }
    },

    onTradeMetaReady: function (isReconnection) {
        this.set('isTradeMetaReady', true);

        var tradeMetaReadySubscriptions = this.get('tradeMetaReadySubscriptions');

        if (tradeMetaReadySubscriptions) {
            Ember.$.each(tradeMetaReadySubscriptions, function (key, subscriber) {
                if (subscriber && Ember.$.isFunction(subscriber.onTradeMetaReady)) {
                    subscriber.onTradeMetaReady(isReconnection);
                }
            });
        }

        // Clear order commission request map
        this.set('orderCommissionRequestMap', {});
    },

    subscribeTradeDisconnect: function (subscriber, key) {
        if (utils.validators.isAvailable(key)) {
            var tradeDisconnectSubscriptions = this.get('tradeDisconnectSubscriptions');
            tradeDisconnectSubscriptions[key] = subscriber;
        }
    },

    unSubscribeTradeDisconnect: function (key) {
        if (utils.validators.isAvailable(key)) {
            var tradeDisconnectSubscriptions = this.get('tradeDisconnectSubscriptions');
            tradeDisconnectSubscriptions[key] = undefined;
        }
    },

    onTradeDisconnect: function () {
        var tradeDisconnectSubscriptions = this.get('tradeDisconnectSubscriptions');

        if (tradeDisconnectSubscriptions) {
            Ember.$.each(tradeDisconnectSubscriptions, function (key, subscriber) {
                if (subscriber && Ember.$.isFunction(subscriber.onTradeDisconnect)) {
                    subscriber.onTradeDisconnect();
                }
            });
        }
    },

    /* Send order commission request to oms
     */
    sendOrderCommissionRequest: function (unqReqId, order) {
        var orderCommissionRequestMap = this.get('orderCommissionRequestMap');
        orderCommissionRequestMap[unqReqId] = order;

        var req = tradeSocketRequestHandler.generateOrderCommissionRequest(unqReqId, order);

        if (req) {
            this.webSocketManager.sendData([req], tradeConstants.SocketConnectionType.OMS);
        }
    },

    sendMutualFundVATRequest: function (unqReqId, mfObj, mutualFund) {
        var mutualFundVATRequestMap = this.get('mutualFundVATRequestMap');
        mutualFundVATRequestMap[unqReqId] = mfObj;

        var req = tradeSocketRequestHandler.generateMFVATRequest(unqReqId, mutualFund);

        if (req) {
            this.webSocketManager.sendData([req], tradeConstants.SocketConnectionType.OMS);
        }
    },

    sendSymbolMarginRequest: function (unqReqId, order, symbolInfo) {
        this.get('symbolMarginRequestMap')[unqReqId] = order;

        var req = tradeSocketRequestHandler.generateSymbolMarginRequest(unqReqId, order, symbolInfo);
        this.webSocketManager.sendData([req], tradeConstants.SocketConnectionType.OMS);
    },

    sendOrderSearchRequest: function (reqObj) {
        var req = tradeSocketRequestHandler.generateOrderSearchRequest(reqObj);
        this.webSocketManager.sendData([req], tradeConstants.SocketConnectionType.OMS);
    },

    sendKYCUpdateRequest: function (unqReqId, kycUpdate, reqObj) {
        var kycUpdateRequestMap = this.get('kycUpdateRequestMap');

        kycUpdateRequestMap[unqReqId] = kycUpdate;
        reqObj.lan = sharedService.userSettings.currentLanguage;

        var req = tradeSocketRequestHandler.generateKYCUpdateRequest(reqObj);
        this.webSocketManager.sendPreAuthData([req], tradeConstants.SocketConnectionType.OMS);
    },

    sendKYCDetailsRequest: function (reqObj) {
        if (this.isAuthenticated()) {
            this._sendKYCDetailsRequest(reqObj);
        } else {
            this.socketReqQueue.enqueue({
                callbackFn: this._sendKYCDetailsRequest,
                args: [reqObj] // Queue arguments as an array
            });
        }
    },

    _sendKYCDetailsRequest: function () {
        var mubNo = this.userDS.get('mubNo');
        var req = tradeSocketRequestHandler.generateKYCDetailsRequest({sMubNo: mubNo});
        this.webSocketManager.sendData([req], tradeConstants.SocketConnectionType.OMS);
    },

    sendKYCMasterDataRequest: function (reqObj, isKYC) {
        var req = tradeSocketRequestHandler.generateKYCMasterDataRequest(reqObj);

        if (isKYC) {
            if (this.isAuthenticated()) {
                this._sendKYCMasterDataRequest(reqObj);
            } else {
                this.socketReqQueue.enqueue({
                    callbackFn: this._sendKYCMasterDataRequest,
                    args: [reqObj] // Queue arguments as an array
                });
            }
        } else {
            this.webSocketManager.sendPreAuthData([req], tradeConstants.SocketConnectionType.OMS);
        }
    },

    _sendKYCMasterDataRequest: function (reqObj) {
        var req = tradeSocketRequestHandler.generateKYCMasterDataRequest(reqObj);
        this.webSocketManager.sendData([req], tradeConstants.SocketConnectionType.OMS);
    },

    validateNinIqama: function (reqObj, callbackFn) {
        var req = tradeSocketRequestHandler.generateNinIqamaValidationRequest(reqObj);
        this.webSocketManager.sendPreAuthData([req], tradeConstants.SocketConnectionType.OMS);

        this.set('ninIqamaCallback', callbackFn);
    },

    sendCaptchaImageRequest: function (reqObj, callbackFn) {
        var req = tradeSocketRequestHandler.generateCaptchaImageRequest(reqObj);
        this.webSocketManager.sendPreAuthData([req], tradeConstants.SocketConnectionType.OMS);

        this.set('captchaImgCallback', callbackFn);
    },

    sendCaptchaValidationRequest: function (reqObj, callbackFn) {
        var req = tradeSocketRequestHandler.generateCaptchaTextRequest(reqObj);
        this.webSocketManager.sendPreAuthData([req], tradeConstants.SocketConnectionType.OMS);

        this.set('captchaTextCallback', callbackFn);
    },

    sendCashStatementRequest: function (reqObj) {
        var req = tradeSocketRequestHandler.generateCashStatementRequest(reqObj);
        this.webSocketManager.sendData([req], tradeConstants.SocketConnectionType.OMS);
    },

    sendStockStatementRequest: function (reqObj) {
        var req = tradeSocketRequestHandler.generateStockStatementRequest(reqObj);
        this.webSocketManager.sendData([req], tradeConstants.SocketConnectionType.OMS);
    },

    changePassword: function (reqObj, callbackFn) {
        var req = tradeSocketRequestHandler.generateChangePasswordRequest(reqObj);
        this.webSocketManager.sendData([req], tradeConstants.SocketConnectionType.OMS);

        this.set('changePasswordCallback', callbackFn);
    },

    sendUserTypeRequest: function (reqObj, callbackFn) {
        var req = tradeSocketRequestHandler.generateUserTypeRequest(reqObj);
        this.webSocketManager.sendData([req], tradeConstants.SocketConnectionType.OMS);

        this.set('userTypeCallback', callbackFn);
    },

    sendFundListRequest: function () {
        if (this.isAuthenticated()) {
            var req = tradeSocketRequestHandler.generateFundListRequest();
            this.webSocketManager.sendData([req], tradeConstants.SocketConnectionType.OMS);
        } else {
            this.reqQueue.enqueue({
                reqGen: tradeSocketRequestHandler.generateFundListRequest
            });
        }
    },

    sendFundTermsConditionGetRequest: function (reqObj, callbackFn) {
        var req = tradeSocketRequestHandler.generateTermsConditionGetRequest(reqObj);
        this.webSocketManager.sendData([req], tradeConstants.SocketConnectionType.OMS);

        this.set('tCGetCallback', callbackFn);
    },

    sendFundTermsConditionUpdateRequest: function (reqObj, callbackFn) {
        var req = tradeSocketRequestHandler.generateTermsConditionUpdateRequest(reqObj);
        this.webSocketManager.sendData([req], tradeConstants.SocketConnectionType.OMS);

        this.set('tCUpdateCallback', callbackFn);
    },

    sendFundTransactionsRequest: function (reqObj) {
        if (this.isAuthenticated()) {
            var req = tradeSocketRequestHandler.generateFundTransactionsRequest(reqObj);
            this.webSocketManager.sendData([req], tradeConstants.SocketConnectionType.OMS);
        } else {
            this.reqQueue.enqueue({
                reqGen: tradeSocketRequestHandler.generateFundTransactionsRequest,
                args: reqObj
            });
        }
    },

    sendFundHoldingsRequest: function () {
        if (this.isAuthenticated()) {
            var req = tradeSocketRequestHandler.generateFundHoldingsRequest();
            this.webSocketManager.sendData([req], tradeConstants.SocketConnectionType.OMS);
        } else {
            this.reqQueue.enqueue({
                reqGen: tradeSocketRequestHandler.generateFundHoldingsRequest
            });
        }
    },

    sendNavFundDateRequest: function (reqObj) {
        var req = tradeSocketRequestHandler.generateNavFundDateRequest(reqObj);
        this.webSocketManager.sendData([req], tradeConstants.SocketConnectionType.OMS);
    },

    sendMFSubscriptionFeeRequest: function (reqObj) {
        var req = tradeSocketRequestHandler.generateMFSubscriptionFeeRequest(reqObj);
        this.webSocketManager.sendData([req], tradeConstants.SocketConnectionType.OMS);
    },

    sendRedemptionFeeRequest: function (reqObj) {
        var req = tradeSocketRequestHandler.generateRedemptionFeeRequest(reqObj);
        this.webSocketManager.sendData([req], tradeConstants.SocketConnectionType.OMS);
    },

    sendMFRedemptionRequest: function (reqObj, callbackFn) {
        var req = tradeSocketRequestHandler.generateMFRedemptionRequest(reqObj);
        this.webSocketManager.sendData([req], tradeConstants.SocketConnectionType.OMS);

        this.set('redemptionCallback', callbackFn);
    },

    sendMFSubscriptionRequest: function (reqObj, callbackFn) {
        var req = tradeSocketRequestHandler.generateMFSubscriptionRequest(reqObj);
        this.webSocketManager.sendData([req], tradeConstants.SocketConnectionType.OMS);

        this.set('subscriptionCallback', callbackFn);
    },

    sendRightsSubscriptionRequest: function (unqReqId, subscription, reqObj) {
        var rightsSubRequestMap = this.get('rightSubscriptionRequestMap');
        rightsSubRequestMap[unqReqId] = subscription;

        var req = tradeSocketRequestHandler.generateRightsSubscriptionRequest(reqObj);
        this.webSocketManager.sendData([req], tradeConstants.SocketConnectionType.OMS);
    },

    sendRightsSubscriptionListRequest: function (reqObj) {
        var req = tradeSocketRequestHandler.generateRightsSubscriptionListRequest(reqObj);
        this.webSocketManager.sendData([req], tradeConstants.SocketConnectionType.OMS);
    },

    sendProductDetailsRequest: function (reqObj) {
        if (this.isAuthenticated()) {
            this._sendProductDetailsRequest(reqObj);
        } else {
            this.socketReqQueue.enqueue({
                callbackFn: this._sendProductDetailsRequest,
                args: [reqObj] // Queue arguments as an array
            });
        }
    },

    _sendProductDetailsRequest: function (reqObj) {
        var req = tradeSocketRequestHandler.generateProductDetailsRequest(reqObj);
        this.webSocketManager.sendData([req], tradeConstants.SocketConnectionType.OMS);
    },

    sendUserRenewUpgradeProductListRequest: function (reqObj) {
        if (this.isAuthenticated()) {
            this._sendUserRenewUpgradeProductListRequest(reqObj);
        } else {
            this.socketReqQueue.enqueue({
                callbackFn: this._sendUserRenewUpgradeProductListRequest,
                args: [reqObj] // Queue arguments as an array
            });
        }
    },

    _sendUserRenewUpgradeProductListRequest: function (reqObj) {
        var req = tradeSocketRequestHandler.generateUserRenewUpgradeProductListRequest(reqObj);
        this.webSocketManager.sendData([req], tradeConstants.SocketConnectionType.OMS);
    },

    sendSubscriptionFeeRequest: function (reqObj) {
        if (this.isAuthenticated()) {
            this._sendSubscriptionFeeRequest(reqObj);
        } else {
            this.socketReqQueue.enqueue({
                callbackFn: this._sendSubscriptionFeeRequest,
                args: [reqObj] // Queue arguments as an array
            });
        }
    },

    _sendSubscriptionFeeRequest: function (reqObj) {
        var req = tradeSocketRequestHandler.generateSubscriptionFeeRequest(reqObj);
        this.webSocketManager.sendData([req], tradeConstants.SocketConnectionType.OMS);
    },

    _sendBulkOrderCancelRequest: function (unqReqId, bulkOrder, reqObj) {
        var bulkOrderRequestMap = this.get('bulkOrderCancelRequestMap');
        bulkOrderRequestMap[unqReqId] = bulkOrder;

        var req = tradeSocketRequestHandler.generateBulkOrderCancelRequest(reqObj);
        this.webSocketManager.sendData([req], tradeConstants.SocketConnectionType.OMS);
    },

    sendSubscriptionRequest: function (reqObj) {
        if (this.isAuthenticated()) {
            this._sendSubscriptionRequest(reqObj);
        } else {
            this.socketReqQueue.enqueue({
                callbackFn: this._sendSubscriptionRequest,
                args: [reqObj] // Queue arguments as an array
            });
        }
    },

    sendBulkOrderCancelRequest: function (unqReqId, bulkOrder, reqObj) {
        if (this.isAuthenticated()) {
            this._sendBulkOrderCancelRequest(unqReqId, bulkOrder, reqObj);
        } else {
            this.socketReqQueue.enqueue({
                callbackFn: this._sendBulkOrderCancelRequest,
                args: [unqReqId, bulkOrder, reqObj] // Queue arguments as an array
            });
        }
    },

    sendConsolidatedReportRequest: function (reqObj) {
        if (this.isAuthenticated()) {
            this._sendConsolidatedReportRequest(reqObj);
        } else {
            this.socketReqQueue.enqueue({
                callbackFn: this._sendConsolidatedReportRequest,
                args: [reqObj] // Queue arguments as an array
            });
        }
    },

    /* *
     * Common request service with empty body for all push subscription and un-subscription
     * @param serviceType
     */
    sendPushRequest: function (serviceType) {
        if (this.isConnected()) {
            var req = tradeSocketRequestHandler.generatePushRequest(serviceType);

            if (req) {
                this.webSocketManager.sendData([req], tradeConstants.SocketConnectionType.OMS);
            }
        } else {
            this.reqQueue.enqueue({
                reqGen: tradeSocketRequestHandler.generatePushRequest,
                args: serviceType

            });
        }
    },

    _sendSubscriptionRequest: function (reqObj) {
        var req = tradeSocketRequestHandler.generateSubscriptionRequest(reqObj);
        this.webSocketManager.sendData([req], tradeConstants.SocketConnectionType.OMS);
    },

    _sendConsolidatedReportRequest: function (reqObj) {
        var req = tradeSocketRequestHandler.generateConsolidatedReportRequest(reqObj);
        this.webSocketManager.sendData([req], tradeConstants.SocketConnectionType.OMS);
    },

    // Registration / Forgot Password Step 1 - Register Details
    validateCustomerDetail: function (reqObj, isForgotPwd) {
        var req = tradeSocketRequestHandler.generateCusDetailValidationRequest(reqObj, isForgotPwd);
        this.webSocketManager.sendPreAuthData([req], tradeConstants.SocketConnectionType.OMS);
    },

    // Login OTP - Validate OTP
    validateLoginOtp: function (reqObj) {
        var req = tradeSocketRequestHandler.generateLoginOtpValidationRequest(reqObj);
        this.webSocketManager.sendPreAuthData([req], tradeConstants.SocketConnectionType.OMS);
    },

    // Registration / Forgot Password Step 2 - OTP Login - Validate OTP
    validateOtpPinCode: function (reqObj) {
        var req = tradeSocketRequestHandler.generateOtpValidationRequest(reqObj);
        this.webSocketManager.sendPreAuthData([req], tradeConstants.SocketConnectionType.OMS);
    },

    // Registration / Forgot Password  Step 2 - OTP Login - Resend OTP
    resendOtpPin: function () {
        // TODO: [Atheesan] Enable after implementing
        // Implementation to be finalized
    },

    // Registration Step 4 / Forgot Password Step 3 - Change Password
    validatePasswordDetail: function (reqObj, isForgotPwd) {
        var req = tradeSocketRequestHandler.generatePasswordValidationRequest(reqObj, isForgotPwd);
        this.webSocketManager.sendPreAuthData([req], tradeConstants.SocketConnectionType.OMS);
    },

    _sendQueuedSocketRequest: function () {
        while (this.socketReqQueue.getLength() > 0) {
            // Get queued request
            var socketReqObj = this.socketReqQueue.dequeue();

            // Call queued function
            // Pass-in queued arguments as an array, but use as properties inside callback function
            socketReqObj.callbackFn.apply(this, socketReqObj.args);
        }
    },

    _sendQueuedData: function () {
        if (this.isConnected()) {
            while (this.reqQueue.getLength() > 0) {
                var reqObj = this.reqQueue.dequeue();
                var req = reqObj.reqGen(reqObj.args);

                if (req) {
                    this.webSocketManager.sendData([req], tradeConstants.SocketConnectionType.OMS);

                    utils.logger.logDebug('Message sent to server : ' + req);
                }
            }
        }
    },

    notifyOrderStatusChange: function (order) {
        var tradeUIServiceMobile = sharedService.getService('tradeUI');

        if (order && !this.get('isBulkOrderAdditionInProgress') && tradeUIServiceMobile && tradeUIServiceMobile.showOrderStatusMessage) {
            tradeUIServiceMobile.showOrderStatusMessage(order);
        }
    },

    _modifyAuthCallback: function (authCallback) {
        var origAuthCallback = authCallback;
        var that = this;

        return function (response) {
            if (Ember.$.isFunction(origAuthCallback)) {
                that.set('isAuthResponseReceived', true);
                origAuthCallback(response);
            }
        };
    },

    _checkAuthResponse: function (authParams) {
        var that = this;

        Ember.run.later(function () {
            if (!that.get('isAuthResponseReceived')) {
                var authTimeoutMessage = that.get('app').lang.messages.authFailed;
                var origAuthFailed = authParams.authFailed;

                sharedService.userSettings.clearLoginToken();

                if (Ember.$.isFunction(origAuthFailed)) {
                    origAuthFailed(authTimeoutMessage);
                }
            }
        }, tradeConstants.TimeIntervals.AuthenticationTimeout);
    },

    sendBrokerageConnectivityRequest: function (successCallBackFn) {
        mixRequestHandler.loadBrokerageConnectivityData(successCallBackFn);
    },

    beginBulkOrderAddition: function () {
        this.set('isBulkOrderAdditionInProgress', true);
    },

    endBulkOrderAddition: function () {
        this.set('isBulkOrderAdditionInProgress', false);
    }
});

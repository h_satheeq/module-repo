import Ember from 'ember';
import Transaction from '../business-entities/transaction';

export default Ember.Object.extend({
    store: {},
    transactionsByPortfolio: {},
    transactionsCollection: Ember.A(),

    getTransaction: function (tranId) {
        var transactionStore = this.get('store');
        var transaction = transactionStore[tranId];

        if (!transaction) {
            transaction = Transaction.create({chTranId: tranId});
            transactionStore[tranId] = transaction;
            this.addToOtherCollections(transaction.secAccNum, transaction);
        }

        return transaction;
    },

    getTransactionsByPortfolio: function (accNo) {
        var transactionPortMap = this.get('transactionsByPortfolio');

        if (!transactionPortMap[accNo]) {
            transactionPortMap[accNo] = Ember.A([]);
        }

        return transactionPortMap[accNo];
    },

    addToOtherCollections: function (acc, transaction) {
        var transactionPortMap = this.get('transactionsByPortfolio');
        var transactionsCollection = this.get('transactionsCollection');

        transactionsCollection.pushObject(transaction);

        if (transactionPortMap[acc]) {
            transactionPortMap[acc].pushObject(transaction);
        } else {
            transactionPortMap[acc] = Ember.A([transaction]);
        }
    },

    getTransactionCollection: function () {
        return this.get('transactionsCollection');
    },

    clearCollection: function () {
        this.set('store', {});
        this.set('transactionsCollection', Ember.A());
    }
});

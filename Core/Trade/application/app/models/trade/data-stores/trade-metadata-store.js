import Ember from 'ember';
import tradeMeta from '../business-entities/trade-meta';
import KeyValue from '../../../models/shared/business-entities/key-value';
import ruleVariables from '../../../controllers/trade/widgets/order-ticket/rule-variables';
import priceConstants from '../../../models/price/price-constants';

export default Ember.Object.extend({
    tradeMeta: tradeMeta,
    metaMapping: {
        orderType: 'orderType',
        orderSide: 'orderSide',
        tifType: 'tifType',
        gtdMax: 'gtdMax',
        priceLimitCheck: 'priceLimitCheck'
    },

    app: undefined,
    store: {},
    orderTypeByExg: {},
    orderSideByExg: {},
    tifTypeByExg: {},
    gtdMaxByExg: {},
    priceLimitCheckByExg: {},

    payMethods: [{Id: 1, DisplayName: 'Bank Transfer'}],
    withdrawalReasons: [{Id: 0, DisplayName: 'Forex Trading'}, {Id: 1, DisplayName: 'Cash Out'}],

    amendAllowedStsByExg: {},
    cancelAllowedStsByExg: {},
    tifMktStatusDependencyMap: {},

    defaultTifType: '0,1,2,3,4,5,6,7,8,9,10',
    defaultOrderType: '1,2',
    defaultOrderSide: '1,2',
    defaultGtdMax: '',
    defaultPrcLmtChk: 0,

    amendEnableConfig: {
        // This object will update with relevant configuration from rule variables at runtime
    },

    initialize: function (appLanguage) {
        this.set('app', appLanguage);

        if (ruleVariables.amendEnableConfig) {
            this.set('amendEnableConfig', ruleVariables.amendEnableConfig);
        }
    },

    setOrderTypes: function (exchange, orderTypes) {
        var that = this;

        this._setExchangeConfigData(exchange, this.metaMapping.orderType, orderTypes, 'orderTypeByExg', function (orderTypeObj, propertyName) {
            that._addToConfigCollection(exchange, propertyName, orderTypeObj);
        });
    },

    setOrderSides: function (exchange, orderSides) {
        var that = this;

        this._setExchangeConfigData(exchange, this.metaMapping.orderSide, orderSides, 'orderSideByExg', function (orderSideObj, propertyName) {
            that._addToConfigCollection(exchange, propertyName, orderSideObj);
        });
    },

    setTifTypes: function (exchange, orderType, mktSts, tifTypes) {
        var that = this;

        this._setTifConfigData(exchange, this.metaMapping.tifType, tifTypes, 'tifTypeByExg', function (tifTypeObj, propertyName) {
            that._addToTifCollection(exchange, propertyName, tifTypeObj, orderType, mktSts);
        }, orderType, mktSts);
    },

    setTifByType: function (exg, type, tifs) {
        var that = this;
        var orderTypeMap = this.getOrderTypeMapByExchange(exg);
        var orderType = orderTypeMap[type];
        var tifTypes = tifs.split(',');
        var supportedTif = [];

        Ember.$.each(tifTypes, function (index, tifKey) {
            var tifType = {
                code: parseInt(tifKey, 10),
                des: that.app.lang.labels[[that.metaMapping.tifType, tifKey].join('_')]
            };

            supportedTif.pushObject(tifType);
        });

        if (orderType) {
            orderType.set('tifTypes', supportedTif);
        }
    },

    setExgGTDExpiry: function (exchange, gtdMax) {
        var metaStore = this.get('store');
        var configType = this.metaMapping.gtdMax;

        if (!metaStore[exchange]) {
            metaStore[exchange] = {};
        }

        if (!metaStore[exchange][configType]) {
            metaStore[exchange][configType] = {};
        }

        metaStore[exchange][configType].gtdMax = gtdMax;

        this._setExgGTDExpiry(exchange, gtdMax);
    },

    setExgPriceLimitCheck: function (exchange, prcLmtChk) {
        var metaStore = this.get('store');
        var configType = this.metaMapping.priceLimitCheck;

        if (!metaStore[exchange]) {
            metaStore[exchange] = {};
        }

        if (!metaStore[exchange][configType]) {
            metaStore[exchange][configType] = {};
        }

        metaStore[exchange][configType].prcLmtChk = prcLmtChk;

        this._setExgPriceLimitCheck(exchange, prcLmtChk);
    },

    _setExgGTDExpiry: function (exchange, gtdMax) {
        var gtdMaxByExg = this.get('gtdMaxByExg');

        if (!gtdMaxByExg[exchange]) {
            gtdMaxByExg[exchange] = {};
        }

        gtdMaxByExg[exchange].gtdMax = gtdMax;
    },

    _setExgPriceLimitCheck: function (exchange, prcLmtChk) {
        var priceLimitCheckByExg = this.get('priceLimitCheckByExg');

        if (!priceLimitCheckByExg[exchange]) {
            priceLimitCheckByExg[exchange] = {};
        }

        priceLimitCheckByExg[exchange].prcLmtChk = prcLmtChk;
    },

    getExgGTDExpiry: function (exg) {
        var gtdMaxByExg = this.get('gtdMaxByExg');

        if (!gtdMaxByExg[exg]) {
            gtdMaxByExg[exg] = {};
        }

        if (!gtdMaxByExg[exg].gtdMax) {
            gtdMaxByExg[exg].gtdMax = this.defaultGtdMax;
        }

        return gtdMaxByExg[exg].gtdMax;
    },

    getExgPriceLimitCheck: function (exg) {
        var priceLimitCheckByExg = this.get('priceLimitCheckByExg');

        if (!priceLimitCheckByExg[exg]) {
            priceLimitCheckByExg[exg] = {};
        }

        if (!priceLimitCheckByExg[exg].prcLmtChk) {
            priceLimitCheckByExg[exg].prcLmtChk = this.defaultPrcLmtChk;
        }

        return priceLimitCheckByExg[exg].prcLmtChk;
    },

    getOrderTypeCollectionByExchange: function (exchange) {
        return this._getConfigCollectionByExchange(exchange, 'orderTypeByExg');
    },

    getOrderSideCollectionByExchange: function (exchange) {
        return this._getConfigCollectionByExchange(exchange, 'orderSideByExg');
    },

    getTifTypeCollectionByExchange: function (exchange, orderType, mktSts) {
        var mktStatus = mktSts ? mktSts : priceConstants.MarketStatus.Open;
        var configByExg = this.get('tifTypeByExg');

        if (!configByExg[exchange]) {
            configByExg[exchange] = {};
        }

        if (!configByExg[exchange][orderType]) {
            configByExg[exchange][orderType] = {};
        }

        if (!configByExg[exchange][orderType][mktStatus]) {
            configByExg[exchange][orderType][mktStatus] = Ember.A([]);

            var tifArray = this.defaultTifType.split(',');
            var defaultTifArray = tifArray.map(function (val) {
                return parseInt(val, 10);
            });

            this.setTifTypes(exchange, orderType, mktStatus, defaultTifArray);
        }

        return configByExg[exchange][orderType][mktStatus];
    },

    isTifDependOnStatus: function (exg) {
        return this.tifMktStatusDependencyMap[exg];
    },

    getOrderTypeMapByExchange: function (exchange) {
        if (this.get('store')[exchange] && !this.get('store')[exchange][this.metaMapping.orderType]) {
            this.setOrderTypes(exchange);
        }

        return this.get('store')[exchange] ? this.get('store')[exchange][this.metaMapping.orderType] : [];
    },

    getOrderSideMapByExchange: function (exchange) {
        if (this.get('store')[exchange] && !this.get('store')[exchange][this.metaMapping.orderSide]) {
            this.setOrderSides(exchange);
        }

        return this.get('store')[exchange] ? this.get('store')[exchange][this.metaMapping.orderSide] : [];
    },

    getTifTypeMapByExchange: function (exchange) {
        if (this.get('store')[exchange] && !this.get('store')[exchange][this.metaMapping.tifType]) {
            this.setTifTypes(exchange);
        }

        return this.get('store')[exchange] ? this.get('store')[exchange][this.metaMapping.tifType] : {};
    },

    getPayMethods: function () {
        this.set('payMethods', [{Id: 1, DisplayName: this.app.lang.labels.bankTransfer}]);

        return this.get('payMethods');
    },

    getWithdrawalReasons: function () {
        return this.get('withdrawalReasons');
    },

    loadRuleVariables: function () {
        var amendAllowedStsByExg = ruleVariables.amendAllowedStsByExg;
        this.set('amendAllowedStsByExg', amendAllowedStsByExg);

        var cancelAllowedStsByExg = ruleVariables.cancelAllowedStsByExg;
        this.set('cancelAllowedStsByExg', cancelAllowedStsByExg);
    },

    getAmendAllowedStatus: function (exg) {
        var statusMap = this.get('amendAllowedStsByExg');

        return statusMap[exg] ? statusMap[exg] : statusMap.default;
    },

    getCancelAllowedStatus: function (exg) {
        var statusMap = this.get('cancelAllowedStsByExg');

        return statusMap[exg] ? statusMap[exg] : statusMap.default;
    },

    _getConfigCollectionByExchange: function (exchange, configType) {
        var configByExg = this.get(configType);

        if (!configByExg[exchange]) {
            configByExg[exchange] = Ember.A([]);

            if (configType === 'orderTypeByExg') {
                this.setOrderTypes(exchange, this.defaultOrderType.split(','));
            } else if (configType === 'orderSideByExg') {
                this.setOrderSides(exchange, this.defaultOrderSide.split(','));
            }
        }

        return configByExg[exchange];
    },

    _addToConfigCollection: function (exchange, propertyName, configObj) {
        var configByExg = this.get(propertyName);

        if (configByExg[exchange]) {
            configByExg[exchange].pushObject(configObj);
        } else {
            configByExg[exchange] = Ember.A([configObj]);
        }
    },

    _addToTifCollection: function (exchange, propertyName, configObj, orderType, mktSts) {
        var configByExg = this.get(propertyName);

        if (!configByExg[exchange]) {
            configByExg[exchange] = {};
        }

        if (!configByExg[exchange][orderType]) {
            configByExg[exchange][orderType] = {};
        }

        if (!configByExg[exchange][orderType][mktSts]) {
            configByExg[exchange][orderType][mktSts] = Ember.A([configObj]);
        } else {
            configByExg[exchange][orderType][mktSts].pushObject(configObj);
        }
    },

    _setExchangeConfigData: function (exchange, configType, configValues, propertyName, collectionExecutor) {
        var metaStore = this.get('store');

        if (!metaStore[exchange]) {
            metaStore[exchange] = {};
        }

        if (!metaStore[exchange][configType]) {
            metaStore[exchange][configType] = {};
        }

        // This is to reset exchange config while reconnecting and receiving configs repeatedly
        var configByExg = this.get(propertyName);
        configByExg[exchange] = Ember.A();

        if (configValues) {
            var configMap = metaStore[exchange][configType];
            this._createConfigObject(configValues, configMap, configType, propertyName, collectionExecutor);
        }
    },

    _setTifConfigData: function (exchange, configType, configValues, propertyName, collectionExecutor, orderType, mktSts) {
        var metaStore = this.get('store');

        if (!metaStore[exchange]) {
            metaStore[exchange] = {};
        }

        if (!metaStore[exchange][configType]) {
            metaStore[exchange][configType] = {};
        }

        if (!metaStore[exchange][configType][orderType]) {
            metaStore[exchange][configType][orderType] = {};
        }

        if (!metaStore[exchange][configType][orderType][mktSts]) {
            metaStore[exchange][configType][orderType][mktSts] = {};
        }

        // This is to reset exchange config while reconnecting and receiving configs repeatedly
        var configByExg = this.get(propertyName);

        if (orderType && mktSts && configByExg[exchange] && configByExg[exchange][orderType]) {
            configByExg[exchange][orderType][mktSts] = Ember.A();
        }

        if (configValues) {
            var configMap = metaStore[exchange][configType][orderType][mktSts];
            this._createConfigObject(configValues, configMap, configType, propertyName, collectionExecutor);
        }
    },

    _createConfigObject: function (configValues, configMap, configType, propertyName, collectionExecutor) {
        var that = this;

        Ember.$.each(configValues, function (index, configVal) {
            configMap[configVal] = KeyValue.create({
                code: configVal,
                des: that.app.lang.labels[[configType, configVal].join('_')]
            });

            collectionExecutor(configMap[configVal], propertyName);
        });
    }
});

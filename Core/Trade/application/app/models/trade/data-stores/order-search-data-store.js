import Ember from 'ember';
import Order from '../business-entities/order';
import sharedService from '../../shared/shared-service';

export default Ember.Object.extend({
    store: {},

    getOrder: function (orderNo, wkey, symbol, exchange, instruTyp) {
        var currentStore = this.get('store');
        var widgetSearchResult = currentStore[wkey];
        var orderObj;

        if (widgetSearchResult && widgetSearchResult.orderMap) {
            var orderMap = widgetSearchResult.orderMap;

            if (!orderMap[orderNo]) {
                orderObj = Order.create({
                    clOrdId: orderNo
                });

                orderMap[orderNo] = orderObj;
                widgetSearchResult.ordLst.pushObject(orderObj);
            } else {
                orderObj = orderMap[orderNo];
            }

            if (!orderObj.symbolInfo && symbol) {
                var priceService = sharedService.getService('price');
                var symbolInfo = priceService.stockDS.getStock(exchange, symbol, instruTyp);
                orderObj.set('symbolInfo', symbolInfo);
            }
        }

        return orderObj;
    },

    getWidgetSearchResult: function (wkey) {
        var currentStore = this.get('store');

        return currentStore[wkey] ? currentStore[wkey] : this.generateSearchResultObj(wkey);
    },

    clearWidgetSearchResult: function (wkey) {
        var currentStore = this.get('store');
        currentStore[wkey] = undefined;
    },

    generateSearchResultObj: function (wkey) {
        var currentStore = this.get('store');

        currentStore[wkey] = {
            isNxtPagAvail: -1,
            ordCatgry: 0,
            ordGrp: 0,
            ordLst: [],
            orderMap: {},
            pagType: -1,
            pagWid: 0,
            srhMode: -1,
            startSeq: 1,
            totalNoRec: 0
        };

        return currentStore[wkey];
    }
});

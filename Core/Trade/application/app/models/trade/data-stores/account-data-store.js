import Ember from 'ember';
import CashAccount from '../business-entities/cash-account';
import TradingAccount from '../business-entities/trading-account';
import userSettings from '../../../config/user-settings';
import tradeConstants from '../trade-constants';

export default Ember.Object.extend({
    cashAccountsMap: {},
    tradingAccountsMap: {},
    cashAccountColl: Ember.A(),
    tradingAccountColl: Ember.A(),

    tradingAccMapByCashAcc: {},
    tradingAccMapByExg: {},
    tradingAccSubsMap: {},

    // Total account gain loss calculation
    totalGainLoss: 0,
    totalGainLossPer: 0,
    totalMktVal: 0,
    holdings: [],

    getCashAccount: function (accNo) {
        var accStore = this.get('cashAccountsMap');
        var accObj = accStore[accNo];

        if (!accObj && accNo) {
            accObj = CashAccount.create({cashAccId: accNo});
            accStore[accNo] = accObj;
            this.cashAccountColl.pushObject(accObj);
        }

        return accObj;
    },

    getTradingAccount: function (accNo, cashAccId, exgArray) {
        var accStore = this.get('tradingAccountsMap');
        var accObj = accStore[accNo];

        if (!accObj && accNo) {
            accObj = TradingAccount.create({tradingAccId: accNo, cashAccountId: cashAccId});
            accStore[accNo] = accObj;
            accObj.set('cashAccount', this.getCashAccount(cashAccId));

            this.tradingAccountColl.pushObject(accObj);
            this._addToTradingAccCollections(accObj, cashAccId, exgArray);
        }

        return accObj;
    },

    getCashAccCollection: function () {
        return this.get('cashAccountColl');
    },

    getTradingAccCollection: function () {
        return this.get('tradingAccountColl');
    },

    getTradingAccCollByExchange: function (exg) {
        return this.get('tradingAccMapByExg')[exg];
    },

    _addToTradingAccCollections: function (accObj, cashAccId, exgArray) {
        var tradingAccMapByExg = this.get('tradingAccMapByExg');
        var tradingAccMapByCashAcc = this.get('tradingAccMapByCashAcc');

        if (!tradingAccMapByCashAcc[cashAccId]) {
            tradingAccMapByCashAcc[cashAccId] = Ember.A();
        }

        tradingAccMapByCashAcc[cashAccId].pushObject(accObj);

        if (exgArray && exgArray.length > 0) {
            Ember.$.each(exgArray, function (key, exg) {
                if (!tradingAccMapByExg[exg]) {
                    tradingAccMapByExg[exg] = Ember.A();
                }

                tradingAccMapByExg[exg].pushObject(accObj);
            });
        }
    },

    getCurrencyCollection: function (accId) {
        var tradingAccMap = this.get('tradingAccountsMap');
        var curr = accId && tradingAccMap[accId] && tradingAccMap[accId].cashAccount ? tradingAccMap[accId].cashAccount.curr : userSettings.customisation.defaultCurrency;

        return [{Id: 0, DisplayName: curr}];
    },

    getSubscribedAccMap: function () {
        return this.get('tradingAccSubsMap');
    },

    subscribeTradingAccountInfo: function (tradingAcc) {
        var tradingAccSubsMap = this.get('tradingAccSubsMap');

        if (tradingAcc && tradingAcc.tradingAccId) {
            this.tradeService.sendHoldingsRequest(tradingAcc);
            this.tradeService.sendBuyingPowerRequest(tradingAcc);

            var tradAccId = tradingAcc.tradingAccId;

            if (!tradingAccSubsMap[tradAccId]) {
                tradingAccSubsMap[tradAccId] = 0;

                this.tradeService.sendPushRequest(tradeConstants.AiolosProtocol.MsgType.Push.AccountSub);
                this.tradeService.sendPushRequest(tradeConstants.AiolosProtocol.MsgType.Push.HoldingSub);
            }

            tradingAccSubsMap[tradAccId] = tradingAccSubsMap[tradAccId] + 1;
        }
    },

    unSubscribeTradingAccountInfo: function (tradingAcc) {
        var tradingAccSubsMap = this.get('tradingAccSubsMap');

        if (tradingAcc && tradingAcc.tradingAccId) {
            var tradAccId = tradingAcc.tradingAccId;

            if (tradingAccSubsMap[tradAccId]) {
                var subsCount = tradingAccSubsMap[tradAccId];

                if (subsCount > 0) {
                    tradingAccSubsMap[tradAccId] = subsCount - 1;
                } else {
                    this.tradeService.sendPushRequest(tradeConstants.AiolosProtocol.MsgType.AccountUnSub);
                    this.tradeService.sendPushRequest(tradeConstants.AiolosProtocol.MsgType.HoldingUnSub);
                }
            }
        }
    },

    onHoldingGainLossChange: function () {
        Ember.run.once(this, this.setTotalMarketInfo);
    }.observes('holdings.@each.gainLoss'),

    subscribeHoldingCalculation: function () {
        var that = this;

        if (this.get('holdings').length === 0) {
            this.set('holdings', this.tradeService.holdingDS.getHoldingCollection());
        }

        var accounts = this.getTradingAccCollection();

        if (accounts && accounts.length > 0) {
            Ember.$.each(accounts, function (key, acc) {
                that.subscribeTradingAccountInfo(acc);
            });
        }
    },

    getTotalMktValue: function () {
        return this.get('totalMktVal');
    },

    getTotalGainLoss: function () {
        return this.get('totalGainLoss');
    },

    getTotalGainLossPer: function () {
        return this.get('totalGainLossPer');
    },

    setTotalMarketInfo: function () {
        var accColl = this.getTradingAccCollection();
        var totalGainLoss = 0;
        var totalGainLossPer = 0;
        var totalCostValue = 0;
        var totalMktVal = 0;

        if (accColl.length > 0) {
            Ember.$.each(accColl, function (key, acc) {
                totalCostValue += acc.get('costVal');
                totalMktVal += acc.get('mktVal');
            });

            totalGainLoss = totalMktVal - totalCostValue;

            if (totalCostValue && totalCostValue !== 0) {
                totalGainLossPer = totalGainLoss * 100 / totalCostValue;
            }

            this.set('totalMktVal', totalMktVal);
            this.set('totalGainLoss', totalGainLoss);
            this.set('totalGainLossPer', totalGainLossPer);
        }
    }
});

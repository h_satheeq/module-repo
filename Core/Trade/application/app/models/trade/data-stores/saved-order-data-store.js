import Ember from 'ember';
import PersistentObject from '../../../models/shared/business-entities/persistent-object';
import OrderParamsObject from '../../../models/trade/business-entities/order-params';
import sharedService from '../../../models/shared/shared-service';

export default Ember.Object.extend({
    savedOrderPerObj: undefined,
    savedOrders: [],
    savedOrderList: Ember.A([]),

    initialize: function () {
        var savedOrderPerObj = PersistentObject.create({cacheKey: 'savedOrders'});
        var savedOrdersCacheData = savedOrderPerObj.load();

        if (savedOrdersCacheData && savedOrdersCacheData.savedOrders) {
            this._generateCollection(savedOrdersCacheData.savedOrders);
        }

        this.set('savedOrderPerObj', savedOrderPerObj);
    },

    save: function () {
        var persistentObj = this.get('savedOrderPerObj');

        persistentObj.setData(this._getPersistentData());
        persistentObj.save();
    },

    saveOrder: function (orderObj) {
        this._addObject(orderObj);
        this.save();
    },

    removeOrder: function (orderObj) {
        this.savedOrderList.removeObject(orderObj);
        this.save();
    },

    getSavedOrderCollection: function () {
        return this.get('savedOrderList');
    },

    _generateCollection: function (ordList) {
        var that = this;

        Ember.$.each(ordList, function (key, savedOrder) {
            that._addObject(savedOrder);
        });
    },

    _addObject: function (order) {
        var orderParams = OrderParamsObject.create();
        orderParams.setData(order);

        if (order && order.symbol && order.exg) {
            var stock = sharedService.getService('price').stockDS.getStock(order.exg, order.symbol, order.instruTyp);
            orderParams.set('symbolInfo', stock);
        }

        this.savedOrderList.pushObject(orderParams);
    },

    _getPersistentData: function () {
        var savedOrdersCollection = this.get('savedOrderList');
        var cacheSavingList = [];

        Ember.$.each(savedOrdersCollection, function (key, order) {
            var orderParamsEntity = {
                ordSide: order.ordSide,
                ordTyp: order.ordTyp,
                tif: order.tif,
                ordQty: order.ordQty,
                secAccNum: order.secAccNum,
                symbol: order.symbol,
                exg: order.exg,
                price: order.price,
                dayOrd: order.dayOrd,
                instruTyp: order.instruTyp,
                isChecked: order.isChecked,
                date: order.date,
                expTime: order.expTime,
                unqId: order.unqId
            };

            cacheSavingList.pushObject(orderParamsEntity);
        });

        return {savedOrders: cacheSavingList};
    }
});
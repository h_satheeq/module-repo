import Ember from 'ember';
import Order from '../business-entities/order';
import sharedService from '../../shared/shared-service';
import utils from '../../../utils/utils';
import languageDataStore from '../../../models/shared/language/language-data-store';

export default Ember.Object.extend({
    store: {},
    orderMapByExg: {},
    orderStore: [],
    optionOrderCollection: Ember.A(),

    getOrder: function (orderNo, exchange, symbol, insTyp, orderStatus, orgClOrdId, orderData) {
        var key = orderNo;
        var currentStore = this.get('store');
        var orderObj = currentStore[key];
        var isInvalidatedOrder = this.isInvalidatedOrder(orderStatus, orgClOrdId);

        if (!orderObj && !isInvalidatedOrder) {
            orderObj = Order.create({
                mubOrdNum: key
            });

            this.addToOtherCollections(exchange, insTyp, orderObj);
            currentStore[key] = orderObj;
        }

        if (orderObj && !orderObj.symbolInfo && symbol) {
            this._setSymbolInfo(exchange, symbol, insTyp, orderObj);
        }

        if (!isInvalidatedOrder && orderObj && orderData) {
            if (utils.validators.isAvailable(orderData.txt)) {
                orderData.txt = languageDataStore.generateLangMessage(utils.formatters.convertUnicodeToNativeString(orderData.txt));
            }

            orderData.clOrdId = orderData.clOrdId === '-1' ? '' : orderData.clOrdId;
            orderData.disQty = orderData.disQty === -1 ? 0 : orderData.disQty;

            orderObj.setData(orderData);
        }

        if (isInvalidatedOrder && !this.tradeService.get('isBulkOrderAdditionInProgress') && orderData) {
            var tradeUIService = sharedService.getService('tradeUI');
            var invalidatedOrder = Order.create();

            invalidatedOrder.setData(orderData);
            this._setSymbolInfo(exchange, symbol, insTyp, invalidatedOrder);

            if (tradeUIService && tradeUIService.showOrderStatusMessage) {
                tradeUIService.showOrderStatusMessage(invalidatedOrder);
            }
        }

        return !isInvalidatedOrder ? orderObj : undefined;
    },

    addToOtherCollections: function (exchange, insTyp, orderObj) {
        var orderMapByExg = this.get('orderMapByExg');

        if (orderMapByExg[exchange]) {
            orderMapByExg[exchange].pushObject(orderObj);
        } else {
            orderMapByExg[exchange] = Ember.A([orderObj]);
        }

        var orderStore = this.get('orderStore');
        orderStore.pushObject(orderObj);

        // Check data type of the coming insTyp (allowed only int)
        var instrumentTyp = isNaN(insTyp) ? undefined : parseInt(insTyp, 10);

        if (instrumentTyp === utils.AssetTypes.Option) {
            this.optionOrderCollection.pushObject(orderObj);
        }
    },

    getOrderCollection: function () {
        return this.get('orderStore');
    },

    getOrdersByInstType: function (inst) {
        return inst === utils.AssetTypes.Option ? this.optionOrderCollection : this.orderStore;
    },

    isInvalidatedOrder: function (orderStatus, orgClOrdId) {
        var originalOrderId = utils.validators.isAvailable(orgClOrdId) ? orgClOrdId : 0;
        originalOrderId = parseInt(originalOrderId, 10);

        return originalOrderId >= 0 && orderStatus === this.tradeService.constants.OrderStatus.Rejected;
        // Keep below line without deleting
        // After OMS changes are done related to original order id missing, this logic can be finalized
        // return orgClOrdId !== undefined && orderStatus === this.tradeService.constants.OrderStatus.Rejected;
    },

    _setSymbolInfo: function (exchange, symbol, insTyp, orderObj) {
        var priceService = sharedService.getService('price');
        var symbolInfo = priceService.stockDS.getStock(exchange, symbol, insTyp);

        orderObj.set('symbolInfo', symbolInfo);
    }
});

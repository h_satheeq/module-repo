import Ember from 'ember';
import CashStatement from '../business-entities/cash-statement';

export default Ember.Object.extend({
    store: {},
    trnsCollection: [],
    sumInfo: Ember.Object.create({}),

    getCashStatement: function (trnsRef) {
        var currentStore = this.get('store');
        var stmnt = currentStore[trnsRef];

        if (!stmnt && trnsRef) {
            stmnt = CashStatement.create({
                trnsRef: trnsRef
            });

            currentStore[trnsRef] = stmnt;
            this.addToOtherCollections(stmnt);
        }

        return stmnt;
    },

    setSummationInfo: function (obj) {
        var sumInfoObj = this.get('sumInfo');
        Ember.set(sumInfoObj, 'curr', obj.curr);
        Ember.set(sumInfoObj, 'openBal', obj.openBal);
        Ember.set(sumInfoObj, 'closeBal', obj.closeBal);
        Ember.set(sumInfoObj, 'totBuy', obj.totBuy);
        Ember.set(sumInfoObj, 'totSell', obj.totSell);
        Ember.set(sumInfoObj, 'totOther', obj.totOther);
        Ember.set(sumInfoObj, 'totDeposit', obj.totDeposit);
        Ember.set(sumInfoObj, 'totWithdrawal', obj.totWithdrawal);
    },

    getCashStmSummation: function () {
        return this.get('sumInfo');
    },

    addToOtherCollections: function (stmnt) {
        var transCollection = this.get('trnsCollection');
        transCollection.pushObject(stmnt);
    },

    getCashStatementColl: function () {
        return this.get('trnsCollection');
    },

    clearCollection: function () {
        this.set('store', {});
        this.set('trnsCollection', Ember.A());
    }
});

import Ember from 'ember';
import BankAcc from '../business-entities/bank-account';
import utils from '../../../utils/utils';

export default Ember.Object.extend({
    store: {},
    bankDetailsCollection: Ember.A([]),
    desBankDetailsCollection: Ember.A([]),
    bankAccByCashAcc: {},

    getBankAcc: function (bankId, cashAccId) {
        var currentStore = this.get('store');
        var bank = currentStore[bankId];

        if (!bank && bankId) {
            bank = BankAcc.create({
                bnkAccNum: bankId
            });

            currentStore[bankId] = bank;

            var bankDetailsCollection = this.get('bankDetailsCollection');
            bankDetailsCollection.pushObject(bank);

            if (cashAccId) {
                this.addToOtherCollections(bank, cashAccId);
            }
        }

        return bank;
    },

    getDesBankAcc: function (bankId) {
        var currentStore = this.get('store');
        var bank = currentStore[bankId];

        if (!bank && bankId) {
            bank = BankAcc.create({
                bnkAccNum: bankId
            });

            currentStore[bankId] = bank;

            var bankDetailsCollection = this.get('desBankDetailsCollection');
            bankDetailsCollection.pushObject(bank);
        }

        return bank;
    },

    addToOtherCollections: function (bankAcc, cashAccId) {
        if (!this.bankAccByCashAcc[cashAccId]) {
            this.bankAccByCashAcc[cashAccId] = [];
        }

        this.bankAccByCashAcc[cashAccId].pushObject(bankAcc);
    },

    getBankAccColl: function (cashAccId) {
        var bankAccMap = this.bankAccByCashAcc;
        var globalMap = utils.Constants.StringConst.Asterisk;

        // Aiolos beneficiary details mapped to cashAccount while old OMS doesn't. Using * to get full collection.
        return cashAccId && bankAccMap[cashAccId] ? bankAccMap[cashAccId] : bankAccMap[globalMap] ? bankAccMap[globalMap] : [];
    },

    getDesBankColl: function () {
        return this.get('desBankDetailsCollection');
    },

    clearCollection: function () {
        this.set('store', {});
        this.set('bankDetailsCollection', Ember.A());
        this.set('desBankDetailsCollection', Ember.A());
    }
});
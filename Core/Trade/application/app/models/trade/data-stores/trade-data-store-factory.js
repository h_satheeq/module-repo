// Data stores
import orderDS from './order-data-store';
import holdingDS from './holding-data-store';
import tradeMetaDS from './trade-metadata-store';
import transDS from './transaction-data-store';
import orderSearchDS from './order-search-data-store';
import cashStatementDS from './cash-statement-data-store';
import bankAccountDS from './bank-account-data-store';
import savedOrdersDS from './saved-order-data-store';
import AccountDS from './account-data-store';

export default (function () {
    var createOrderDataStore = function (tradeService) {
        return orderDS.create({tradeService: tradeService});
    };

    var createHoldingDataStore = function (tradeService) {
        return holdingDS.create({tradeService: tradeService});
    };

    var createAccountDataStore = function (tradeService) {
        return AccountDS.create({tradeService: tradeService});
    };

    var createTradeMetaDataStore = function (tradeService) {
        return tradeMetaDS.create({tradeService: tradeService});
    };

    var createTransactionDataStore = function (tradeService) {
        return transDS.create({tradeService: tradeService});
    };

    var createOrderSearchDataStore = function (tradeService) {
        return orderSearchDS.create({tradeService: tradeService});
    };

    var createCashStatementDataStore = function (tradeService) {
        return cashStatementDS.create({tradeService: tradeService});
    };

    var createBankDataStore = function (tradeService) {
        return bankAccountDS.create({tradeService: tradeService});
    };

    var createSavedOrdersDataStore = function (tradeService) {
        var savedOrdersDataStore = savedOrdersDS.create({tradeService: tradeService});
        savedOrdersDataStore.initialize();

        return savedOrdersDataStore;
    };

    return {
        createOrderDataStore: createOrderDataStore,
        createHoldingDataStore: createHoldingDataStore,
        createAccountDataStore: createAccountDataStore,
        createTradeMetaDataStore: createTradeMetaDataStore,
        createTransactionDataStore: createTransactionDataStore,
        createOrderSearchDataStore: createOrderSearchDataStore,
        createCashStatementDataStore: createCashStatementDataStore,
        createBankDataStore: createBankDataStore,
        createSavedOrdersDataStore: createSavedOrdersDataStore
    };
})();

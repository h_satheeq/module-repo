import account from '../business-entities/account';
import Ember from 'ember';

export default Ember.Object.extend({
    store: Ember.A(),

    getStore: function () {
        return this.get('store');
    },

    loadAccount: function () {
        var i;
        for (i = 0; i < 5; i++) {
            var tmpAcc = account.create();
            tmpAcc.setData('11123' + i, 'SAR');
            this.get('store').pushObject(tmpAcc);
        }

    },

    clearData: function () {
        this.set('store', Ember.A());
    }

});

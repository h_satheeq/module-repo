import Ember from 'ember';
import Holding from '../business-entities/holding';
import utils from '../../../utils/utils';
import sharedService from '../../shared/shared-service';

export default Ember.Object.extend({
    store: {},
    holdings: Ember.A(),
    holdingByAccount: {},
    emptyHoldingPool: {},
    mfHolding: Ember.A(),

    calculationInterval: 1000,

    init: function () {
        // Start timer
        this.calcTimer();
    },

    getHolding: function (acc, symbol, exchange, insTyp, isUpdateAvailable, isEmptyFund) {
        // TODO: [satheeq] generate key with exchange
        var emptyHoldingPool = this.get('emptyHoldingPool');
        var key = utils.keyGenerator.getKey(acc, symbol);
        var holdingStore = this.get('store');
        var holdObj = holdingStore[key];

        if (!holdObj && utils.validators.isAvailable(acc) && utils.validators.isAvailable(symbol) && !isEmptyFund) {
            if (!emptyHoldingPool[key]) {
                holdObj = Holding.create({secAccNum: acc, symbol: symbol, exg: exchange, instruTyp: insTyp});
                var priceService = sharedService.getService('price');

                var symbolInfo = priceService.stockDS.getStock(exchange, symbol, insTyp);
                var portfolioInfo = this.tradeService.accountDS.getTradingAccount(acc);

                holdObj.set('symbolInfo', symbolInfo);
                holdObj.set('portfolioInfo', portfolioInfo);

                if (!isUpdateAvailable) {
                    emptyHoldingPool[key] = holdObj;
                }
            } else {
                holdObj = emptyHoldingPool[key];
            }

            if (isUpdateAvailable) {
                holdingStore[key] = holdObj;
                this.addToOtherCollections(acc, holdObj);
            }
        }

        return holdObj;
    },

    getHoldingsByInstType: function (inst) {
        return inst === utils.AssetTypes.MutualFund ? this.mfHolding : this.holdings;
    },

    getHoldingByAccount: function (accNo) {
        var holdingAccMap = this.get('holdingByAccount');

        if (!holdingAccMap[accNo]) {
            holdingAccMap[accNo] = Ember.A([]);
        }

        return holdingAccMap[accNo];
    },

    getHoldingCollection: function () {
        return this.get('holdings');
    },

    addToOtherCollections: function (acc, holdObj) {
        var holdingAccMap = this.get('holdingByAccount');
        var holdings = this.get('holdings');

        // TODO: [satheeqh] Temporary fix to update mutual fund holdings list. Need to revisit data store design
        if (holdObj.instruTyp === utils.AssetTypes.MutualFund) {
            this.mfHolding.pushObject(holdObj);
        } else {
            holdings.pushObject(holdObj);

            if (holdingAccMap[acc]) {
                holdingAccMap[acc].pushObject(holdObj);
            } else {
                holdingAccMap[acc] = Ember.A([holdObj]);
            }
        }
    },

    calcTimer: function () {
        var that = this;

        Ember.run.later(this, function () {
            that.calculateGainLoss();
            that.calcTimer();
        }, this.get('calculationInterval'));
    },

    calculateGainLoss: function () {
        var that = this;
        var subsAccounts = this.tradeService.accountDS.getSubscribedAccMap();

        if (subsAccounts) {
            Ember.$.each(subsAccounts, function (key, count) {
                if (key && count > 0) {
                    var tradingAcc = that.tradeService.accountDS.getTradingAccount(key);

                    if (tradingAcc && tradingAcc.tradingAccId) {
                        that.calculateHoldingGainLoss(tradingAcc);
                    }
                }
            });
        }
    },

    calculateHoldingGainLoss: function (tradingAcc) {
        var accCostVal = 0;
        var accMktVal = 0;
        var holdingList = this.get('holdingByAccount')[tradingAcc.get('tradingAccId')];

        if (holdingList) {
            Ember.$.each(holdingList, function (index, holdObj) {
                if (holdObj) {
                    holdObj.calculateChange();
                    accCostVal += holdObj.get('costVal');
                    accMktVal += holdObj.get('mktVal');
                }
            });
        }

        var accGL = accMktVal - accCostVal;
        var accGLPer = 0;

        if (accCostVal && accCostVal !== 0) {
            accGLPer = accGL * 100 / accCostVal;
        }

        tradingAcc.set('costVal', accCostVal);
        tradingAcc.set('mktVal', accMktVal);
        tradingAcc.set('gainLoss', accGL);
        tradingAcc.set('gainLossPer', accGLPer);
    },

    removeHolding: function (secAccNum, symbol) {
        var holdingStore = this.get('store');
        var holdingPortMap = this.get('holdingByAccount');
        var holdings = this.get('holdings');
        var key = utils.keyGenerator.getKey(secAccNum, symbol);
        var holdObj = holdingStore[key];

        if (holdObj) {
            var portfolioHoldings = holdingPortMap[secAccNum];
            portfolioHoldings.removeItems([holdObj]);
            holdingStore[key] = undefined;

            var index = holdings.indexOf(holdObj);
            holdings.removeItems([holdObj]);

            // This is to notify observers
            holdings.arrayContentDidChange(index, 1);
        }
    }
});

import Ember from 'ember';

export default Ember.Object.extend({
    chTranId: '',
    payMtd: '',
    secAccNum: '',
    cashAccId: '',
    curr: '',
    amt: '',
    cqNum: '',
    cqDte: '',
    valDte: '',
    bank: '',
    bnkAdr: '',
    nts: '',
    sMubNo: '',
    bId: '',
    bnkBrn: '',
    toAcc: '',
    refId: '',
    lctn: '',
    sts: '',
    bnkAccNum: '',
    trnCde: '',

    setData: function (orderMessage) {
        var that = this;

        Ember.$.each(orderMessage, function (key, value) {
            that.set(key, value);
        });
    }
});
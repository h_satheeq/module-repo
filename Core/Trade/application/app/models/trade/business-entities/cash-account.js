import Ember from 'ember';

export default Ember.Object.extend({
    cashAccId: '',
    cashAccName: '',
    buyPwr: 0,
    accTyp: -1,
    curr: '',
    balance: 0,
    blkAmt: 0,
    odLmt: 0,
    valuation: 0.0,
    unrealSales: 0,
    cashForWith: 0,
    netSecVal: 0.0,
    dayCashMar: 0.0,
    pendDept: 0.0,
    pendTrans: 0.0,
    penSet: 0.0,
    marBlk: 0.0,
    dyMarBlk: 0.0,
    marDue: 0.0,
    cashAmt: 0.0,
    payAmt: 0.0,
    t1BuyPowr: 0,
    t2BuyPowr: 0,
    t3BuyPowr: 0,
    isMar: false,
    invAccNo: '',
    marLmt: 0, // Margin limit field

    mrgLmt: function () {
        return this.get('marLmt');
    }.property('marLmt'),

    totVal: function () {
        return this.get('balance') + this.get('valuation') - this.get('payAmt');
    }.property('balance', 'valuation', 'payAmt'),

    setData: function (portMessage) {
        var that = this;

        Ember.$.each(portMessage, function (key, value) {
            that.set(key, value);
        });
    }
});
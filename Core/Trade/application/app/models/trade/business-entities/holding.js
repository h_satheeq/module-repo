import Ember from 'ember';
import utils from '../../../utils/utils';
import fieldMetaConfig from '../../../config/field-meta-config';
import sharedService from '../../shared/shared-service';

export default Ember.Object.extend({
    secAccNum: '',
    exg: '',
    symbol: '',
    fndName: '',
    arabicFundName: '',
    qty: 0.0,
    pendBuy: 0.0,
    pledged: 0.0,
    pendSell: 0.0,
    avgCst: 0.0,
    portAvgPri: 0.0,
    marDue: 0.0,
    dayMarDue: 0.0,
    netDayHold: 0.0,
    marHold: 0.0,
    dayMarHold: 0.0,
    instruTyp: 0,
    tplusDyNtHold: 0.0,
    tplusPendStk: 0.0,
    tplusDySellPend: 0.0,
    curr: '',
    avaiQty: 0.0,
    soldQty: 0.0,
    avgSldPri: 0.0,
    blkQty: 0.0,
    costBas: 0.0,
    availQtyForSell: 0.0,
    mktPri: 0.0,
    daySellPend: 0.0,
    dayMarNotiLvl: 0,
    lotSize: 0,
    holdingTyp: 0,
    outsBuy: 0.0,
    todayBuy: 0.0,
    outsSell: 0.0,
    todaySell: 0.0,
    settleQty: 0.0,
    symMar: 0.0,
    toDyDpst: 0.0,
    toDyWith: 0.0,
    portPer: 0.0,
    todayGainPer: 0.0,
    todayGain: 0.0,

    symbolInfo: undefined,
    gainLossPer: 0,
    gainLoss: 0,
    penHolding: 0,

    pldQty: 0,
    payQty: 0,
    recQty: 0,
    pendSubQty: 0,
    subQty: 0,
    wAvgPrice: 0.0,
    subPrice: 0.0,
    untPri: 0.0,
    fxRate: 0.0,
    subsRdmElgb: 1,
    ratioFactr: 1,

    quantityFields: ['avaiQty', 'availQtyForSell', 'blkQty', 'dayMarHold', 'daySellPend', 'marHold', 'netDayHold', 'payQty', 'pendBuy', 'pendSell', 'pendSettle', 'pendSubQty', 'pldQty', 'qty', 'recQty', 'settleQty', 'soldQty', 'subQty', 'todayBuy', 'todaySell', 'tplusDyNtHold', 'tplusDySellPend'],

    init: function () {
        this.set('userSettings', sharedService.userSettings);
    },

    lDes: function () {
        return this.get('symbolInfo').lDes;
    }.property('symbolInfo.lDes'),

    sDes: function () {
        return this.get('symbolInfo').sDes;
    }.property('symbolInfo'),

    dSym: function () {
        var symbolInformation = this.get('symbolInfo');
        return symbolInformation ? symbolInformation.dSym : this.get('symbol');
    }.property('symbolInfo', 'symbol'),

    multiFactor: function () {
        var exchange = this.get('exg');
        var multiFactors = fieldMetaConfig.multiFactors;
        var multiplicationFactor = 1;

        if (exchange && multiFactors) {
            var exchangeFieldMeta = multiFactors[exchange];

            if (exchangeFieldMeta && exchangeFieldMeta.multiFactor) {
                multiplicationFactor = exchangeFieldMeta.multiFactor;
            }
        }

        return multiplicationFactor;
    }.property('exg'),

    costVal: function () {
        var avgCst = this.get('avgCst');
        var qty = this.get('qty');

        if (avgCst > 0 && qty > 0) {
            return avgCst * qty;
        }

        return 0;
    }.property('avgCst', 'qty'),

    unitVal: function () {
        var unitPrice = this.get('untPri');
        var qty = this.get('qty');

        if (unitPrice > 0 && qty > 0) {
            return unitPrice * qty;
        }

        return 0;
    }.property('untPri', 'qty'),

    mktPrice: 0,
    mktVal: 0,

    calculateChange: function () {
        var symbolInfo = this.get('symbolInfo');
        var value = symbolInfo.ltp > 0 ? symbolInfo.ltp : symbolInfo.prvCls > 0 ? symbolInfo.prvCls : 0;
        var mktPrice = value * this.get('multiFactor');
        var mktVal = mktPrice > 0 && this.get('qty') > 0 ? this.get('qty') * mktPrice : 0;

        this.set('mktPrice', mktPrice);
        this.set('mktVal', mktVal);

        var costVal = this.get('costVal');
        var gainLoss = 0;
        var gainLossPer = 0;

        if (costVal > 0) {
            gainLoss = mktVal - costVal;
            gainLossPer = gainLoss * 100 / costVal;
        }

        this.set('gainLoss', gainLoss);
        this.set('gainLossPer', gainLossPer);
    },

    calTodayGain: function () {
        var currentAssests = this.get('todayBuy') - this.get('todaySell');
        var assestsVal = currentAssests * this.get('mktPrice');
        var avgVal = currentAssests * this.get('avgCst');
        var todayGain = assestsVal - avgVal;
        var todayGainPer = (assestsVal - avgVal) * 100 / assestsVal;

        this.set('todayGain', todayGain);
        this.set('todayGainPer', todayGainPer);
    }.property('todayBuy', 'todaySell', 'avgCst', 'mktPrice'),

    isRightSymbol: function () {
        return utils.AssetTypes.isRight(this.get('instruTyp'));
    }.property('instruTyp'),

    isSubRedEnabled: function () {
        return this.get('subsRdmElgb') === 1;
    }.property('subsRdmElgb'),

    fundName: function () {
        return this.get('userSettings.currentLanguage') === 'AR' ? this.get('arabicFundName') : this.get('fndName');
    }.property('fndName', 'arabicFundName', 'userSettings.currentLanguage'),

    setData: function (holdingMessage) {
        var that = this;

        Ember.$.each(holdingMessage, function (key, value) {
            var propValue = value;

            if (holdingMessage.instruTyp === utils.AssetTypes.MutualFund) {
                var ratioFactor = holdingMessage.ratioFactr && holdingMessage.ratioFactr >= 0 ? holdingMessage.ratioFactr : 1;

                if (that.get('quantityFields').indexOf(key) > -1) {
                    propValue = value * ratioFactor;
                }
            }

            that.set(key, propValue);
        });
    }
});
import PersistentObject from '../../../models/shared/business-entities/persistent-object';
import utils from '../../../utils/utils';

export default PersistentObject.extend({
    cacheKey: 'tradeMeta',
    isEncrypt: true,

    exgLevelConfig: {},
    customerAcc: {},

    save: function () {
        this._super(utils.Constants.StorageType.Local);
    },

    load: function () {
        return this._super(utils.Constants.StorageType.Local);
    }
}).create();

import Ember from 'ember';
import appConfig from '../../../config/app-config';
import languageDataStore from '../../shared/language/language-data-store';
import sharedService from '../../shared/shared-service';

export default Ember.Object.extend({
    tradingAccId: '',
    exg: '',
    cashAccountId: '',
    isDefault: false,
    cashAccount: undefined, // Reference to mapped cash-account entity
    pendOrdVal: 0.0,
    pendSell: 0.0,
    pendBuy: 0.0,
    gainLoss: 0,
    gainLossPer: 0,
    costVal: 0,
    mktVal: 0,
    exgArray: [],

    isMar: Ember.computed.alias('cashAccount.isMar'),
    userSettings: sharedService.userSettings,

    tradingAccName: function () {
        return this.portNme ? this.portNme : this.tradingAccId;
    }.property('tradingAccId', 'portNme'),

    dispProp: function () {
        var that = this;
        var dispValue = '';
        var dispValueArray = [];
        var dispConfig = appConfig.customisation.displayProperties;
        var property = (dispConfig && dispConfig.dispProp) ? dispConfig.dispProp : 'tradingAccId';

        if (Ember.$.isArray(property)) {
            Ember.$.each(property, function (indexValue, propValue) {
                var valueForProp = that.get(propValue);

                if (valueForProp) {
                    if (dispConfig.dispPropPrefix && dispConfig.dispPropPrefix[indexValue]) {
                        var disPropPrefix = languageDataStore.getLanguageObj().lang.labels[dispConfig.dispPropPrefix[indexValue]];
                        dispValueArray[dispValueArray.length] = [disPropPrefix, valueForProp].join(': ');
                    } else {
                        dispValueArray[dispValueArray.length] = valueForProp;
                    }
                }
            });

            dispValue = dispValueArray.join(' / ');
        } else {
            dispValue = this.get(property);
        }

        return dispValue;
    }.property('tradingAccId', 'userSettings.currentLanguage'),

    setData: function (portMessage) {
        var that = this;

        Ember.$.each(portMessage, function (key, value) {
            that.set(key, value);
        });
    }
});
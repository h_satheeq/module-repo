import Ember from 'ember';
import PersistentObject from '../../../models/shared/business-entities/persistent-object';
import utils from '../../../utils/utils';

export default PersistentObject.extend({
    cacheKey: 'tradeUser',
    isEncrypt: true,

    // Auth related params
    usrId: '',
    sessionId: '',
    authSts: 0,
    rejResn: '',
    lgnExpDte: '',
    lstLgnTme: '',
    instId: '',
    L2AuthTyp: '',
    prcUsr: '',
    brkId: '',
    cusNme: '',
    prefLang: '',
    lgnAls: '',
    cntryCode: '',
    dlrId: '',
    mubNo: '',
    userExchg: [],
    inactiveExchg: [],
    cusDetails: undefined,
    isVatApplicble: 0,
    failAtmps: 0,
    crntLgnDteTme: '', // Current login date / time

    setData: function (userParams) {
        var that = this;

        Ember.$.each(userParams, function (key, value) {
            that.set(key, value);
        });
    },

    save: function () {
        this._super(utils.Constants.StorageType.Local);
    },

    load: function () {
        return this._super(utils.Constants.StorageType.Local);
    },

    isTradeEnabledExchange: function (exg) {
        var userExchanges = this.get('userExchg');
        return userExchanges.length > 0 && userExchanges.contains(exg);
    }
}).create();

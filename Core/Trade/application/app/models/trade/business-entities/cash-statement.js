import Ember from 'ember';
import utils from '../../../utils/utils';
import sharedService from '../../../models/shared/shared-service';
import appConfig from '../../../config/app-config';

export default Ember.Object.extend({
    trnsRef: '',
    date: '',
    trnsType: '',
    particulars: '',
    amount: '',
    balance: '',
    comsn: 0,
    vatAmount: 0,
    settleDate: '',
    userSettings: null,

    init: function () {
        this._super();
        this.set('userSettings', sharedService.userSettings);
    },

    des: function () {
        var particulars = this.get('particulars');

        if (particulars) {
            particulars = utils.formatters.convertUnicodeToNativeString(particulars);

            var desArray = particulars.split('|');
            var currLang = sharedService.userSettings.currentLanguage;

            if (desArray && desArray.length > 0) {
                switch (currLang) {
                    case appConfig.customisation.supportedLanguages[0].code:
                        particulars = desArray[0];
                        break;

                    case appConfig.customisation.supportedLanguages[1].code:
                        particulars = desArray[1];
                        break;
                }
            }
        }

        return particulars;
    }.property('particulars', 'userSettings.currentLanguage'),

    setData: function (cashStatementDetails) {
        var that = this;

        Ember.$.each(cashStatementDetails, function (key, value) {
            that.set(key, value);
        });
    }
});
import Ember from 'ember';

export default Ember.Object.extend({
    bnkAccNum: '',
    curr: '',
    bnkAccTyp: '',
    allowDep: 0,
    allowChar: 0,
    allowWith: 0,
    bnkId: 0,
    instId: 0,
    sts: '',
    accID: '',

    setData: function (bankAccMsg) {
        var that = this;

        Ember.$.each(bankAccMsg, function (key, value) {
            that.set(key, value);
        });
    }
});
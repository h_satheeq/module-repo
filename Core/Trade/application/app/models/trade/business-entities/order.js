import Ember from 'ember';
import OrderParams from './order-params';
import sharedService from '../../shared/shared-service';
import utils from '../../../utils/utils';
import tradeConstants from '../trade-constants';

/* *
 * Extended order parameters to support order list
 */
export default OrderParams.extend({
    tradingAccId: '', // Trading Account Id instead of SecAccNum
    netStl: 0, // Order Net Settle
    ordSts: '', // Order Status
    cumOrdNetStl: 0, // Cumulative Order Net Settlement
    cumComsn: 0, // Cumulative Commission
    cumOrdVal: 0, // Cumulative Order Value
    cumOrdNetVal: 0, // Cumulative Order Net Value
    lstUptdTme: '', // Last Updated Time
    stmntTpe: 0, // Statement Type
    txt: '', // Order Text
    ltPrice: 0, // Last Price
    ltShare: 0, // Last Shares
    orgClOrdId: '', // Original Client Order ID
    ordRejReasn: '', // ORDER_REJECT_REASON
    maxFlr: 0, // Maximum Floor Value
    chnlId: 0, // Channel Id
    fillQty: 0, // Filled Qty
    ordCatgry: -1, // Order category
    intMatchSts: 0, // Initial Match Status
    soInterval: 0,
    soBlk: 0,
    algTyp: -1,
    isTimeTriggerede: 0,
    marTradSts: 0,
    ordId: '',
    mubOrdNum: '',
    symbolInfo: undefined,
    sttlmntDte: '',
    crdTime: '', // Order Created Date Time
    maxPrc: 0,
    avgPrice: 0,

    crdDte: function () { // Order Created Date
        return this.get('crdTime');
    }.property('crdTime'),

    symbolSDes: function () {
        var symbolObj = this.get('symbolInfo');
        return symbolObj ? symbolObj.sDes : this.get('symbol');
    }.property('symbolInfo', 'symbol'),

    pendQty: function () {
        var qty = this.get('ordQty');
        var cumQty = this.get('cumQty');

        if (qty > 0 && cumQty >= 0) {
            return qty - cumQty;
        }

        return 0;
    }.property('ordQty', 'cumQty'),

    isAmendEnabled: function () {
        var ordSts = this.get('ordSts');

        return sharedService.getService('trade').tradeMetaDS.getAmendAllowedStatus(this.get('exg')).indexOf(ordSts) >= 0;
    }.property('ordSts'),

    isCancelEnabled: function () {
        var ordSts = this.get('ordSts');

        return sharedService.getService('trade').tradeMetaDS.getCancelAllowedStatus(this.get('exg')).indexOf(ordSts) >= 0;
    }.property('ordSts'),

    adjustedCrdDte: function () {
        return utils.formatters.formatToDateTime(this.get('crdDte'), this.get('exg'));
    }.property('crdDte', 'exg'),

    adjustedExpTime: function () {
        return utils.formatters.formatToDate(this.get('expTime'), this.get('exg'));
    }.property('expTime', 'exg'),

    adjustedSttlmntDte: function () {
        return utils.formatters.formatToDate(this.get('sttlmntDte'), this.get('exg'));
    }.property('sttlmntDte', 'exg'),

    notifyOrderStatusChange: function () {
        Ember.run.debounce(this, this._notifyOrderStatusChange, tradeConstants.TimeIntervals.WebSocketInQueueProcessingInterval);
    }.observes('ordSts'),

    _notifyOrderStatusChange: function () {
        sharedService.getService('trade').notifyOrderStatusChange(this);
    },

    setData: function (orderMessage) {
        var that = this;

        Ember.$.each(orderMessage, function (key, value) {
            that.set(key, value);
        });
    }
});
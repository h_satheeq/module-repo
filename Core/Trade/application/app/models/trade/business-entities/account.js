import Ember from 'ember';

export default Ember.Object.extend({
    portfolioId: '',
    cur: '',
    buyPower: 0,
    cashBalance: 0,
    availCashBalance: 0,
    blockedAmnt: 0,
    cashAvailForWidr: 0,
    unsettledSale: 0,
    portfolioValue: 0,
    totalPortfolio: 0,
    odLimit: 0,
    accntType: 'A',
    eqPosition: '4',
    portPosition: '4',

    setData: function (s, e) {
        this.set('portfolioId', s);
        this.set('cur', e);
        this.set('buyPower', Math.random() * 100000);
        this.set('cashBalance', Math.random() * 10000);
        this.set('availCashBalance', Math.random() * 1000);
        this.set('blockedAmnt', Math.random() * 1000);
        this.set('cashAvailForWidr', Math.random() * 1000);
        this.set('unsettledSale', Math.random() * 1000);
        this.set('portfolioValue', Math.random() * 100000);
        this.set('totalPortfolio', Math.random() * 100000);
        this.set('odLimit', Math.random() * 100000);
    }
});
import Ember from 'ember';
import utils from '../../../utils/utils';
import fieldMetaConfig from '../../../config/field-meta-config';
import ruleValidator from '../../../controllers/trade/widgets/order-ticket/rule-validator';

/* *
 * Basic order parameters used for order ticket
 */
export default Ember.Object.extend({
    clOrdId: '', // Client Order ID
    secAccNum: '', // Security Account Number (Portfolio Num)
    symbol: '', // Symbol
    exg: '', // Exchange
    instruTyp: '', // Instrument Type
    ordTyp: '2', // Order type
    ordSide: '1', // Side
    ordQty: '', // Order Quantity
    tif: 0, // Time In Force
    disQty: '', // Disclosed Quantity
    minQty: '', // Minimum Quantity
    displayPrice: '', // Display price in order ticket
    price: 0, // Order price
    orderCat: 1, // Order category (General, Advanced, Algo)
    action: 1, // Order action (New, Amend, Cancel)
    cumQty: 0, // Cumulative Order Quantity
    dayOrd: 0, // Day Order
    curr: '', // Currency
    mktCode: '', // Market Code
    dlrId: '', // Dealer Id
    secTyp: '', // Symbol Security Type
    optionLotSize: 100, // Todo: [Dasun] extend the Field meta config
    isMar: false, // Symbol margin flag
    smblmgnBuyPwr: 0, // Symbol margin buying power
    expDte: '', // Expire Date
    ordVal: 0, // Order Value
    vatAmount: 0, // VAT
    comsn: 0, // Commission

    expTime: function () { // Expire Time
        return this.get('expDte');
    }.property('expDte'),

    dSym: function () {
        var symbolInformation = this.get('symbolInfo');
        return symbolInformation ? symbolInformation.dSym : this.get('symbol');
    }.property('symbolInfo', 'symbol'),

    sDes: function () {
        var symbolInformation = this.get('symbolInfo');
        return symbolInformation ? symbolInformation.sDes : this.get('symbol');
    }.property('symbolInfo', 'symbol'),

    setPrice: function () {
        this.set('price', this.get('displayPrice'));
    }.observes('displayPrice'),

    calcOrdVal: function () {
        var symbolInformation = this.get('symbolInfo');

        if (this.get('price') > 0 && this.get('ordQty') > 0) {
            var orderValue = this.get('price') * this.get('ordQty');
            var exchange = this.get('exg');
            var multiFactors = fieldMetaConfig.multiFactors;

            if (exchange && multiFactors) {
                var exchangeFieldMeta = multiFactors[exchange];

                if (exchangeFieldMeta && exchangeFieldMeta.multiFactor) {
                    orderValue = orderValue * exchangeFieldMeta.multiFactor;
                }
            }

            if (symbolInformation && utils.AssetTypes.isOption(symbolInformation.inst)) {
                orderValue = orderValue * this.get('optionLotSize');
            }

            return orderValue;
        }

        return 0;
    }.property('price', 'ordQty', 'symbolInfo'),

    calcNetOrdVal: function () {
        if (this.get('comsn') > 0 || this.get('calcOrdVal') > 0 || this.get('vatAmount') > 0) {
            var orderValue = this.get('calcOrdVal');
            var commission = this.get('comsn');
            var vat = this.get('vatAmount');
            var orderSide = this.get('ordSide');

            return orderSide === '2' ? orderValue - commission - vat : orderValue + commission + vat;
        }

        return 0;
    }.property('comsn', 'calcOrdVal', 'vatAmount'),

    getTickSize: function (isDecrement) {
        return ruleValidator.getTickSize(this.get('exg'), this.get('instruTyp'), this.get('displayPrice'), isDecrement);
    },

    setData: function (orderMessage) {
        var that = this;

        Ember.$.each(orderMessage, function (key, value) {
            that.set(key, value);
        });
    }
});
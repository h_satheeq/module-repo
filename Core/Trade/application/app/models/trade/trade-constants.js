export default {
    StorageKeys: {
        TradeMeta: 'tradeMeta'
    },

    SocketConnectionType: {
        OMS: 'oms'
    },

    // Pulse related constants
    Pulse: {
        PulseInterval: 5000, // Time interval - n * 1000 (n seconds)
        MissedPulseCount: 20, // After this number of missed pulses, application will trigger a disconnection
        ReconnectionTimeInterval: 5000, // Attempt reconnection in this time interval
        MaxRetryCount: 5
    },

    // All below intervals are in milli seconds
    TimeIntervals: {
        WebSocketOutQueueProcessingInterval: 100,
        WebSocketInQueueProcessingInterval: 100,
        CommissionRequestDebounceInterval: 400,
        OrderNotificationTimeout: 10000,
        CashStatementDebounceInterval: 400,
        LoadingInterval: 4,
        AuthenticationTimeout: 10000,
        DeviceWakeTimeout: 15000,
        ShowNotificationTimeout: 10000
    },

    Request: {
        Group: {
            Order: 1,
            OrderList: 2,
            Exchange: 3,
            Transaction: 4,
            Authentication: 5,
            Pulse: 6,
            Customer: 10,
            Registration: 5,
            Subscription: 11,
            RightsSubscription: 13,
            MutualFund: 21
        },

        Type: {
            Exchange: {
                BuyingPower: 1,
                Holdings: 2,
                BankAccounts: 3,
                DestBankAccounts: 4,
                SymbolMarginBP: 6,
                ExchangeConfig: 9,
                ExchangeRule: 13,
                CurrencyRate: 7
            },
            Authentication: {
                AuthenticationNormal: 1,
                AuthenticationLevel2: 2,
                Logout: 4,
                ChangePassword: 3,
                OtpEnabled: 9,
                UserType: 28,
                NinIqama: 42,
                CaptchaImage: 50,
                CaptchaText: 51
            },
            Customer: {
                CustomerDetails: 3,
                KYCDetails: 5,
                KYCMasterData: 6,
                KYCUpdate: 13
            },
            OrderList: {
                OrderSearch: 1,
                OrderList: 3,
                OrderCommission: 5,
                CashStatement: 7,
                StockStatement: 9,
                ConsolidatedReport: 11
            },
            Pulse: {
                Pulse: 1
            },
            Order: {
                NormalOder: 1,
                AmendOder: 7,
                CancelOder: 12,
                CancelBulkOrder: 51 // Panic Withdrawal
            },
            Transaction: {
                Deposit: 1,
                Withdrawal: 2,
                CashTransfer: 4,
                History: 6,
                CashTransferPortfolioHistory: 5
            },
            Registration: {
                RegCustomerDetail: 40,
                RegPasswordConfirmation: 21,
                CustomerDetail: 24,
                OtpLogin: 26,
                PasswordConfirmation: 27
            },
            Subscription: {
                ProductDetails: 1,
                UserRenewUpgradeProductList: 3,
                SubscriptionFee: 5,
                Subscription: 7
            },
            RightsSubscription: {
                Subscription: 17,
                SubscriptionList: 21
            },
            MutualFund: {
                FundList: 22,
                FundTrasactions: 12,
                FundHoldings: 3,
                NavFundDate: 4,
                SubscriptionFee: 5,
                RedemptionFee: 6,
                Subscription: 8,
                Redemption: 7,
                TermsAndConditionGet: 21,
                TermsAndConditionUpdate: 18,
                VatAmount: 23
            }
        }
    },

    Response: {
        Group: {
            Order: 1,
            OrderList: 2,
            Exchange: 3,
            Transaction: 4,
            Authentication: 5,
            Pulse: 6,
            Customer: 10,
            Registration: 5,
            Subscription: 11,
            RightsSubscription: 13,
            MutualFund: 21
        },

        Type: {
            Exchange: {
                BuyingPower: 101,
                Holdings: 102,
                BankAccounts: 103,
                DestBankAccounts: 104,
                SymbolMarginBP: 106,
                ExchangeConfig: 109,
                ExchangeRule: 113,
                FxStatus: 107
            },
            Authentication: {
                AuthenticationNormal: 101,
                AuthenticationLevel2: 102,
                Logout: 104,
                ChangePassword: 103,
                UserType: 128,
                NinIqama: 142,
                CaptchaImage: 150,
                CaptchaText: 151
            },
            Customer: {
                CustomerDetails: 103,
                KYCDetails: 105,
                KYCMasterData: 106,
                KYCUpdate: 113,
                CurrencyRate: 114
            },
            OrderList: {
                OrderSearch: 101,
                OrderList: 103,
                OrderCommission: 105,
                CashStatement: 107,
                StockStatement: 109,
                ConsolidatedReport: 111
            },
            Pulse: {
                Pulse: 101
            },
            Order: {
                NormalOder: 101,
                CancelBulkOrder: 151 // Panic Withdrawal
            },
            Transaction: {
                Deposit: 101,
                Withdrawal: 102,
                CashTransfer: 104,
                History: 106,
                CashTransferPortfolioHistory: 105
            },
            Registration: {
                RegCustomerDetail: 140,
                RegPasswordConfirmation: 121,
                CustomerDetail: 124,
                OtpLogin: 126,
                PasswordConfirmation: 127
            },
            Subscription: {
                ProductDetails: 101,
                UserRenewUpgradeProductList: 103,
                SubscriptionFee: 105,
                Subscription: 107
            },
            RightsSubscription: {
                Subscription: 117,
                SubscriptionList: 121
            },
            MutualFund: {
                FundList: 122,
                FundTrasactions: 112,
                FundHoldings: 103,
                NavFundDate: 104,
                SubscriptionFee: 105,
                RedemptionFee: 106,
                Subscription: 108,
                Redemption: 107,
                TermsAndConditionGet: 121,
                TermsAndConditionUpdate: 118,
                VatAmount: 123
            }
        }
    },

    AiolosProtocol: {
        MsgType: {
            AuthenticationNormal: 1,
            NormalOder: 2,
            Pulse: 3,
            Holdings: 4,
            BuyingPower: 5,
            CustomerDetails: 6,
            ExchangeConfig: 7,
            ExchangeRule: 8,
            BeneficiaryDetails: 9,
            Deposit: 10,
            Withdrawal: 11,
            CashTransfer: 12,
            TransactionHistory: 13,
            OrderList: 23,

            Push: {
                MsgType: 200,
                OrderSub: 200,
                OrderUnSub: 201,
                HoldingSub: 300,
                HoldingUnSub: 301,
                AccountSub: 302,
                AccountUnSub: 303
            }
        }
    },

    BeneficiaryType: {
        CashAccount: 1,
        BrokerageAccount: 2,
        OtherAccount: 3
    },

    OrderSide: {
        Buy: '1',
        Sell: '2'
    },

    OrderType: {
        Market: '1',
        Limit: '2',
        StopLoss: '3',
        StopLimit: '4',
        MarketOnClose: '5',
        SquareOff: 'c'
    },

    OrderStatus: {
        New: '0',
        PartiallyFilled: '1',
        Filled: '2',
        DoneForDay: '3',
        Canceled: '4',
        Replaced: '5',
        PendingCancel: '6',
        Stopped: '7',
        Rejected: '8',
        Suspended: '9',
        PendingNew: 'A',
        OrderSplit: 'B',
        Expired: 'C',
        OrderSplitRejected: 'D',
        PendingReplace: 'E',
        SendToExchangeCancel: 'F',
        SendToExchangeAmend: 'G',
        RequestFailed: 'H',
        AmendRejected: 'I',
        CancelRejected: 'J',
        BrokerReceived: 'K',
        Validated: 'L',
        SendToBrokerNew: 'M',
        SendToExchangeNew: 'N',
        BrokerAccepted: 'O',
        SendToBrokerCancel: 'P',
        SendToBrokerAmend: 'Q',
        Received: 'R',
        NewWaiting: 'T',
        CancelWaiting: 'U',
        NewDeleted: 'V',
        AmendWaiting: 'W',
        Default: 'X',
        Executed: 'Y',
        Initiated: 'Z',
        Approved: 'a',
        ApprovalRejected: 'b',
        WaitingForApproval: 'c',
        ApprovalValidationRejected: 'd',
        InvalidatedByReplace: 'e',
        InvalidatedByChange: 'f',
        CanceledByExchange: 'h',
        SendToDealerNew: 'i',
        SendToDealerAmend: 'j',
        SendToDealerCancel: 'k',
        InvalidatedByCancel: 'm',
        Processed: 'n',
        Processing: 'p',
        PFExpired: 'q',
        PFCanceled: 'r',
        Completed: 's',
        PendingTrigger: 't',
        Sending: 'y'
    },

    SubscriptionStatus: {
        Rejected: '6',
        SentToExchange: '8',
        Approved: '4'
    },

    OrderAction: {
        New: 1,
        Amend: 2,
        Cancel: 3
    },

    OrderSides: {
        1: {DisplayName: 'Buy', value: '1'},
        2: {DisplayName: 'Sell', value: '2'}
    },

    OrderStatusMapping: {
        0: {DisplayName: 'New', value: '0'},
        1: {DisplayName: 'Partially Filled', value: '1'},
        2: {DisplayName: 'Filled', value: '2'},
        3: {DisplayName: 'DoneForDay', value: '3'},
        4: {DisplayName: 'Canceled', value: '4'},
        5: {DisplayName: 'Replaced', value: '5'},
        6: {DisplayName: 'Pending Cancel', value: '6'},
        7: {DisplayName: 'Stopped', value: '7'},
        8: {DisplayName: 'Rejected', value: '8'},
        9: {DisplayName: 'Suspended', value: '9'},
        A: {DisplayName: 'Pending New', value: 'A'},
        B: {DisplayName: 'Calculated', value: 'B'},
        C: {DisplayName: 'Expired', value: 'C'},
        D: {DisplayName: 'Accepted For Bidding', value: 'D'},
        E: {DisplayName: 'Pending Replace', value: 'E'},
        X: {DisplayName: 'Default', value: 'X'},
        L: {DisplayName: 'Validated', value: 'L'},
        R: {DisplayName: 'Received', value: 'R'},
        O: {DisplayName: 'OMS Accepted', value: 'O'},
        N: {DisplayName: 'Send to Execution New', value: 'N'},
        F: {DisplayName: 'Send to Execution Cancel', value: 'F'},
        G: {DisplayName: 'Send to Execution Amend', value: 'G'},
        V: {DisplayName: 'New Deleted', value: 'V'},
        Y: {DisplayName: 'Filled & Partially Filled', value: 'Y'},
        K: {DisplayName: 'OMS Received', value: 'K'},
        U: {DisplayName: 'Order Unplaced', value: 'U'},
        M: {DisplayName: 'Send To OMS New', value: 'M'},
        P: {DisplayName: 'Send To OMS Cancel', value: 'P'},
        Q: {DisplayName: 'Send To OMS Amend', value: 'Q'},
        S: {DisplayName: 'Send To OMS Delete', value: 'S'},
        T: {DisplayName: 'New Initiated', value: 'T'},
        W: {DisplayName: 'Amend Initiated', value: 'W'},
        Z: {DisplayName: 'Initiated', value: 'Z'},
        H: {DisplayName: 'Request Failed', value: 'H'},
        I: {DisplayName: 'Amend Rejected', value: 'I'},
        J: {DisplayName: 'Cancel Rejected', value: 'J'},
        a: {DisplayName: 'Approved', value: 'a'},
        b: {DisplayName: 'Approval Rejected', value: 'b'},
        c: {DisplayName: 'Waiting For Approval', value: 'c'},
        d: {DisplayName: 'Approval Validation Rejected', value: 'd'},
        e: {DisplayName: 'invalidated By Replace', value: 'e'},
        f: {DisplayName: 'Invalidated', value: 'f'},
        h: {DisplayName: 'Trade Canceled', value: 'h'},
        j: {DisplayName: 'Trade Expired PFill', value: 'j'},
        u: {DisplayName: 'Unsolicited Change', value: 'u'},
        m: {DisplayName: 'Invalidated By Cancel', value: 'm'},
        i: {DisplayName: 'Send To MCD', value: 'i'},
        k: {DisplayName: 'Sell Alloc Approved', value: 'k'},
        l: {DisplayName: 'Sell Alloc Rejected', value: 'l'},
        n: {DisplayName: 'Processed', value: 'n'},
        p: {DisplayName: 'Processing', value: 'p'},
        r: {DisplayName: 'Working', value: 'r'},
        s: {DisplayName: 'Completed', value: 's'},
        t: {DisplayName: 'Pending Trigger', value: 't'}
    },

    OrderTypes: {
        1: {DisplayName: 'Market', value: '1'},
        2: {DisplayName: 'Limit', value: '2'}
    },

    ordStatusLabel: 'orderStatus',

    OrderTiffs: {
        DAY: 0,
        GTC: 1,
        AT_OPENING: 2,
        IOC: 3,
        FOK: 4,
        GT_CROSSING: 5,
        GTD: 6,
        EOW: 7,
        EOM: 8,
        SESSION: 9,
        GTT: 10
    },

    OrderDefaultQty: 1000,
    OrderQtyDisQtyPercentage: 5,
    MultiFactor: 100,

    AuthStatus: {
        LoginMismatch: 0,
        Success: 1,
        FirstTimeLogin: 2,
        AccountLocked: 3,
        AlreadyLoggedIn: 4,
        NotActivated: 5,
        VersionNotSupported: 6,
        Expired: 7,
        OtpEnabled: 9
    },

    TransactionStatus: {
        Pending: 1,
        Validated: 2,
        L1Approved: 3,
        L2Approved: 4,
        Cancelled: 5,
        Rejected: 6,
        Approved: 7
    },

    MutualFundTransactionStatus: {
        Initiated: '0',
        Registered: '1',
        AwaitingPayment: '2',
        MoneyReceived: '3',
        Cancelled: '4',
        NoMoney: '5',
        Done: '6',
        PortfolioTransactions: '7',
        WaitingCutoff: '8',
        WaitingForNAV: '9',
        Settlement: '10'
    },

    CashTransferPortfolioStatus: {
        Validated: 1,
        InProgress: 2,
        Cancelled: 4,
        Approved: 5
    },

    TransactionTypes: {
        Deposit: 0,
        Withdrawal: 1,
        Transfer: 2
    },

    MFSubType: {
        Subscription: 1,
        Redemption: 2
    },

    RedemptionType: {
        Full: 1,
        Part: 3
    },

    RedemptionByOption: {
        ByAmount: 1,
        ByUnit: 2
    },

    RegistrationStatus: {
        Success: '1',
        Failed: '-1'
    },

    MFSubRedStatus: {
        Success: 1,
        Failed: -1
    },

    VATStatus: {
        Enabled: 1,
        Disabled: 0
    },

    PriceLimitCheck: {
        Enabled: 1,
        Disabled: 0
    },

    OtpStatus: {
        OtpDisabled: 0,
        OtpEnabled: '1',
        OtpPending: 2,
        OtpSuccess: 3
    }
};

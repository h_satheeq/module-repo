import Ember from 'ember';

export default Ember.Object.extend({
    convertLessThanRule: function (ruleObject, value, isDecrement) {
        var tickSize = ruleObject['*'];
        var floatValue = parseFloat(value);

        Ember.$.each(ruleObject, function (prop, val) {
            var propertyValue = parseFloat(prop);

            if (floatValue < parseFloat(prop) || isDecrement && floatValue === propertyValue) {
                tickSize = val;

                return false;
            }
        });

        return tickSize;
    }
}).create();

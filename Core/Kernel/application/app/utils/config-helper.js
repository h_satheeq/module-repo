import Ember from 'ember';

export default (function () {
    var maxLevel = 3;

    var mergeConfigSettings = function (configInstance, changedSettings) {
        if (configInstance && changedSettings) {
            if (Ember.$.type(changedSettings) === 'object') {
                _scanSetting(configInstance, '', changedSettings, 1);
            }
        }
    };

    var _scanSetting = function (configInstance, settingsProp, settingsObj, level) {
        Ember.$.each(settingsObj, function (prop, val) {
            if (Ember.$.type(val) === 'object' && level <= maxLevel) {
                if (!configInstance[prop]) {
                    configInstance[prop] = {};
                }

                _scanSetting(configInstance[prop], prop, val, level + 1);
            } else {
                configInstance[prop] = val;
            }
        });
    };

    return {
        mergeConfigSettings: mergeConfigSettings
    };
})();

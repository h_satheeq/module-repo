import TableRow from 'ember-table/views/table-row';
import appConfig from '../../config/app-config';

export default TableRow.extend({
    classNameBindings: [],

    setClassNameBindings: function () {
        var rowConfig;

        if (appConfig.customisation.isMobile) {
            rowConfig = ['row.isHovered:ember-table-hover', 'row.isSelected:ember-table-selected', 'row.rowStyle', 'isLastRow:ember-table-last-row',
                'row.isOddRow:panel-table-row-odd:panel-table-row-even', 'row.isEmptyRow:display-none'];
        } else {
            rowConfig = ['row.isHovered:ember-table-hover', 'row.isSelected:ember-table-selected', 'row.rowStyle', 'isLastRow:ember-table-last-row',
                'row.isOddRow:panel-table-row-odd:panel-table-row-even', 'row.is52WeekLow:watchlist-row-back-red',
                'row.is52WeekHigh:watchlist-row-back-green', 'row.isSymSuspended:watchlist-row-strike',
                'row.isTodaysLow:watchlist-row-back-red', 'row.isTodaysHigh:watchlist-row-back-green', 'row.isEmptyRow:display-none'];
        }

        this.set('classNameBindings', rowConfig);
    }.on('init')
});

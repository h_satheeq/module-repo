import HeaderCell from './header-cell';
import ExpandedColumnMixin from '../table-mixins/expanded-column-mixin';

export default HeaderCell.extend(ExpandedColumnMixin, {
    templateName: 'table/views/expanded-header-cell',

    thirdValue: function () {
        return this.get('content.headerCellThirdName');
    }.property('content.headerCellThirdName'),

    isExpandedHeader: function () {
        return this.get('isExpandedView') && this.get('secondValue');
    }.property('isExpandedView'),

    secondHeaderStyle: function () {
        return this.get('content.secondHeaderStyle');
    }.property('content.secondHeaderStyle')
});
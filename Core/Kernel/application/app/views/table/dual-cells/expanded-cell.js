import Ember from 'ember';
import DualCell from './dual-cell';
import ExpandedColumnMixin from '../table-mixins/expanded-column-mixin';
import userSettings from '../../../config/user-settings';

export default DualCell.extend(ExpandedColumnMixin, {
    templateName: 'table/views/expanded-cell',

    formattedThirdValue: Ember.computed(function () {
        return this.addFormat(this.get('cellContent') ? this.get('cellContent').thirdValue : undefined, this.get('row') && !isNaN(this.get('row.deci')) ? this.get('row.deci') : userSettings.displayFormat.decimalPlaces);
    }).property('cellContent'),

    styleThirdValue: Ember.computed(function () {
        return this.get('column.thirdValueStyle') ? this.get('column.thirdValueStyle') : '';
    }).property('formattedThirdValue')
});
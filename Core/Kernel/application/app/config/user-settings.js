export default {
    customisation: {
        defaultExchange: 'TDWL',
        defaultIndex: 'TASI',
        defaultTheme: 'theme2',
        defaultLanguage: 'EN',
        secondaryExchanges: [],
        defaultCurrency: 'SAR'
    },

    displayFormat: {
        dateMonthFormat: 'MMM Do YY',
        dateFormat: 'DD-MM-YYYY',
        monthFormat: 'MMM YYYY',
        timeFormat: 'HH:mm:ss',
        dateTimeFormat: 'DD-MM-YYYY HH:mm:ss',
        dateTimeMinuteFormat: 'DD-MM-YYYY HH:mm',
        dayMonthTimeFormat: 'DD-MM HH:mm:ss',
        noValue: '--',
        decimalPlaces: 2
    }
};
import PriceDataModuleInitializer from '../models/price/initializers/price-data-module-initializer';
import SharedUIModuleInitializer from '../controllers/shared/initializers/shared-ui-module-initializer';
import sharedDataModuleInitializer from '../models/shared/initializers/shared-data-module-initializer';
import PriceUIModuleInitializer from '../controllers/price/initializers/price-ui-module-initializer';
// Please don't delete below place holders - Used by build scripts.
//{{IMPORT}}

export default {
    modules: [
        sharedDataModuleInitializer,
        SharedUIModuleInitializer.create(),
        PriceDataModuleInitializer.create(),
        PriceUIModuleInitializer.create()//{{CREATE}}
    ]
};

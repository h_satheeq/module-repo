import BootstrapDropdownSelect from './bootstrap-dropdown-select';

export default BootstrapDropdownSelect.extend({
    layoutName: 'components/link-dropdown',

    actions: {
        loadLinkDropDownList: function () {
            var viewName = 'components/link-dropdown-list';
            var instanceName = 'component:link-dropdown-list';

            this.createDropDown(viewName, instanceName);
        }
    }
});

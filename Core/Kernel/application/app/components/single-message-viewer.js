import Ember from 'ember';
import utils from '../utils/utils';
import appConfig from '../config/app-config';

export default Ember.Component.extend({
    layoutName: 'components/single-message-viewer',

    showMessage: false,
    message: '',
    type: '',
    icon: '',
    backgroundCss: '',
    messageCss: '',

    isTablet: appConfig.customisation.isTablet,

    setMessageType: function () {
        var messageType = this.get('type');
        var iconCss = '';

        switch (messageType) {
            case utils.Constants.MessageTypes.Error:
                iconCss = 'icon-exclamation-circle';
                break;
        }

        this.set('icon', iconCss);
    }.observes('type'),

    isMobile: function () {
        return appConfig.customisation.isMobile;
    }.property()
});

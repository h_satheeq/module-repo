import Formatter from '../../utils/formatters';
import utils from '../../utils/utils';
import userSettings from '../../config/user-settings';

export default (function () {
    var overwritePrototypes = function (priceService) {
        Formatter.formatToDateTime = function (dateTime, exg, format) {
            var exchange = exg ? priceService.exchangeDS.getExchange(exg) : undefined;
            var offset = exchange ? exchange.get('tzo') : 0;
            var adjustedDate = this.getAdjustedDateTime(dateTime, offset);

            return adjustedDate ? utils.moment(adjustedDate).format(format ? format : userSettings.displayFormat.dateTimeFormat) : userSettings.displayFormat.noValue;
        };

        Formatter.formatToDate = function (date, exg, format) {
            var exchange = exg ? priceService.exchangeDS.getExchange(exg) : undefined;
            var offset = exchange ? exchange.get('tzo') : 0;
            var adjustedDate = this.getAdjustedDateTime(date, offset);

            return adjustedDate ? utils.moment(adjustedDate).format(format ? format : userSettings.displayFormat.dateFormat) : userSettings.displayFormat.noValue;
        };

        Formatter.formatToDateMonth = function (date, exg) {
            var exchange = exg ? priceService.exchangeDS.getExchange(exg) : undefined;
            var offset = exchange ? exchange.get('tzo') : 0;
            var adjustedDate = this.getAdjustedDateTime(date, offset);

            return adjustedDate ? utils.moment(adjustedDate).format(userSettings.displayFormat.dateMonthFormat) : userSettings.displayFormat.noValue;
        };

        Formatter.formatToTime = function (time, exg) {
            var exchange = exg ? priceService.exchangeDS.getExchange(exg) : undefined;
            var offset = exchange ? exchange.get('tzo') : 0;
            var adjustedDate = this.getAdjustedDateTime(time, offset);

            return adjustedDate ? utils.moment(adjustedDate).format(userSettings.displayFormat.timeFormat) : userSettings.displayFormat.noValue;
        };

        Formatter.formatToDateTimeMinute = function (dateTimeMinute, exg, format) {
            var exchange = exg ? priceService.exchangeDS.getExchange(exg) : undefined;
            var offset = exchange ? exchange.get('tzo') : 0;
            var adjustedDate = this.getAdjustedDateTime(dateTimeMinute, offset);

            return adjustedDate ? utils.moment(adjustedDate).format(format ? format : userSettings.displayFormat.dateTimeMinuteFormat) : userSettings.displayFormat.noValue;
        };

        Formatter.formatToDayMonthTime = function (dayTime, exg, format) {
            var exchange = exg ? priceService.exchangeDS.getExchange(exg) : undefined;
            var offset = exchange ? exchange.get('tzo') : 0;
            var adjustedDate = this.getAdjustedDateTime(dayTime, offset);

            return adjustedDate ? utils.moment(adjustedDate).format(format ? format : userSettings.displayFormat.dayMonthTimeFormat) : userSettings.displayFormat.noValue;
        };
    };

    return {
        overwritePrototypes: overwritePrototypes
    };
})();

export default {
    UnixTimestampByMinutes: 60,
    UnixTimestampByMilliSeconds: 1000,
    AnnouncementNewsCashSize: 10,
    SymbolSearchPageSize: 15,
    AnnouncementSearchPageSize: 10,
    NewsSearchPageSize: 10,

    MarketStatus: {
        PreOpen: 1,
        Open: 2,
        Close: 3,
        PreClose: 4,
        CallMarket: 21,
        SessionClosed: 22,
        Break: 33,
        Enquiry: 23,
        OrderAcceptance: 24,
        Trading: 25,
        TradingFWD: 31,
        PreAuction: 26,
        ClosingAuction: 27,
        Closing: 277,
        QuoteEntry: 29,
        QuoteEnquiry: 30,
        ClosingPriceTrading: 28,
        PostClose: 12,
        EndOfDay: 32,
        Unknown: -1,

        isOfflineStatus: function (mktStatus) {
            return mktStatus && (mktStatus === this.Close || mktStatus === this.PreClose);
        }
    },

    // TODO: [Amila] Response and request types needs to be merged as per the protocol suggestion (item 08 of the google doc)
    RequestType: {
        Authentication: 150,

        Data: {
            RequestEquity: 0,
            RequestIndex: 7,
            RequestOption: 0,
            RequestOHLC: 17,
            RequestTOPVOHLC: 270,
            RequestAnnouncement: 27,
            RequestMarketDepthByOrder: 29,
            RequestMarketDepthByPrice: 30,
            RequestExchange: 32,
            RequestTopStocks: 64,
            RequestNews: 77,
            RequestTimeAndSales: 1,
            RequestAlertPlace: 0,   // Message Type key is different for Alert
            RequestAlertUpdate: 1
        }
    },

    ResponseType: {
        Authentication: 150,
        Pulse: 0,
        MessageType: '1',

        Data: {
            ResponseEquity: 3,
            ResponseIndex: 4,
            ResponseExchange: 5,
            ResponseOHLC: 6,
            ResponseTOVPOHLC: 175,
            ResponseMarketDepthByOrder: 7,
            ResponseMarketDepthByPrice: 9,
            ResponseAnnouncement: 11,
            ResponseTopStocks: 64,
            ResponseNews: 77,
            ResponseTimeAndSales: 2,
            ResponseFullMarketEnd: 500,
            ResponseAlert: '225',
            ResponseAlertTrigger: '226',
            ResponseAlertHistory: '227',
            ResponseAlertUnsubscribe: '228'
        }
    },

    MixRequest: {
        SymbolValidation: {
            RT: 52
        },
        SymbolSearch: {
            RT: 53,
            IFLD: 'MC,DES,ISINC,DEP,CFID,TC,DS,SYMT,SYMC',
            ST: 'S,SDES,DES,SYMC',
            XFLD: 'SEC'
        },
        SymbolMetaDetails: {
            RT: 301
        },
        FullSymbolDescription: {
            RT: 303
        },
        ExchangeFullMeta: {
            RT: 306
        },
        ExchangeStockSubMktDetails: {
            RT: 308
        },
        Chart: {
            RT: 37
        },
        TOPVChart: {
            RT: 233
        },
        AnnouncementBody: {
            RT: 26
        },
        NewsSearch: {
            RT: 27
        },
        NewsBody: {
            RT: 28
        },
        AnnouncementSearch: {
            RT: 25
        },
        CompanyProfile: {
            RT: 32,
            CIT: 'CP,INMGT,STK,OWN_IND,CPCLS,SUBS,AUD'
        },
        TimeAndSalesBacklog: {
            RT: 12,
            ChartType: {
                TickCount: 1
            }
        },
        FairValue: {
            RT: 30
        },
        FairValueReport: {
            RT: 97
        },
        FairValueReportLink: {
            RT: 87
        },
        CorporateAction: {
            RT: 30
        },
        VolumeWatcher: {
            RT: 11
        },
        OptionChain: {
            RT: 58,
            IFLD: 'S,E'
        },
        AlertSummary: {
            RT: 84
        },
        ProductSubscription: {
            RT: 1005
        }
    },

    MarketDepthSide: {
        Bid: 0,
        Offer: 1
    },

    MixRequestParameters: {
        AllExchange: 1,
        AllSymbol: 1,
        EnableUnicode: 1,
        EnableMetaTag: 1,
        EnableHeaderTag: 1,
        None: 0
    },

    TimeAndSales: {
        BacklogRequestDelay: 2500,
        MinItemCount: 5
    },

    AssetTypes: {
        0: 'equity',
        7: 'indices',
        75: 'sukukBonds',
        66: 'right',
        86: 'etf',
        11: 'currency',
        68: 'future',
        79: 'corpBond',
        108: 'treasuryBill',
        78: 'govBond',
        76: 'convertibleBond',
        2: 'mutualFund',
        14: 'forex',
        121: 'sukuk',
        73: 'futureOption'
    },

    TopStocksTypes: {
        TopGainersByChange: 0,
        TopGainersByPercentageChange: 1,
        TopLosersByChange: 2,
        TopLosersByPercentageChange: 3,
        MostActiveByVolume: 4,
        MostActiveByTrades: 5,
        MostActiveByValue: 6
    },

    MetaVersionKeys: {
        WatchList: 'WL',
        ExchangeDefinitions: 'SRC'
    },

    // All below intervals are in milli seconds
    TimeIntervals: {
        AnnouncementCachingInterval: 60000, // Time interval - 60 * 1000 (1 min)
        AnnouncementCacheDelay: 15000, // 15 Seconds
        WebSocketOutQueueProcessingInterval: 100,
        WebSocketInQueueProcessingInterval: 100,
        TopStocksUpdateInterval: 60000,
        SearchAutoCompleteInterval: 400,
        AjaxRequestTimeout: 30000,
        TimeAndSalesRealTimeDebounce: 200,
        CashMapChartRefreshInterval: 1000,
        ContentSortInterval: 500,
        AntiScrollInitInterval: 200,
        AlertNotificationInterval: 5000,
        AuthenticationTimeout: 10000,
        DeviceWakeTimeout: 15000,
        ShowNotificationTimeout: 10000
    },

    // Pulse related constants
    Pulse: {
        PulseInterval: 15000, // Time interval - n * 1000 (n seconds)
        MissedPulseCount: 5, // After this number of missed pulses, application will trigger a disconnection
        ReconnectionTimeInterval: 5000, // Attempt reconnection in this time interval
        MaxRetryCount: 10
    },

    SocketConnectionType: {
        QuoteServer: 'qs'
    },

    PriceComponent: {
        DaysRange: 1,
        FiftyTwoWeekHighLow: 2,
        CashMap: 3
    },

    MarketDepthType: {
        DepthByPrice: 1,
        DepthByOrder: 2
    },

    TOPVWidgetType: {
        Stock: 1,
        Index: 2
    },

    // DCFS constants
    DcfsConstants: {
        DcfsLessThanFifty: '0',
        DcfsLessThanSeventyFive: '1',
        DcfsLessThanHundred: '2',
        DcfsGreaterThanHundred: '3'
    },

    DateTimeFormat: {
        ShortDate: 'YYYYMMDD'
    },

    CountryFlags: {
        'af': {column: 1, row: 1},
        'al': {column: 2, row: 1},
        'dz': {column: 3, row: 1},
        'ad': {column: 4, row: 1},
        'ao': {column: 5, row: 1},
        'ag': {column: 6, row: 1},
        'ar': {column: 7, row: 1},
        'am': {column: 8, row: 1},
        'au': {column: 9, row: 1},
        'at': {column: 10, row: 1},
        'az': {column: 11, row: 1},
        'bs': {column: 12, row: 1},
        'bh': {column: 13, row: 1},
        'bd': {column: 14, row: 1},
        'bb': {column: 15, row: 1},
        'by': {column: 16, row: 1},
        'be': {column: 17, row: 1},
        'bz': {column: 18, row: 1},
        'bj': {column: 19, row: 1},
        'bt': {column: 20, row: 1},

        'bo': {column: 1, row: 2},
        'ba': {column: 2, row: 2},
        'bw': {column: 3, row: 2},
        'br': {column: 4, row: 2},
        'bn': {column: 5, row: 2},
        'bg': {column: 6, row: 2},
        'bf': {column: 7, row: 2},
        'mm': {column: 8, row: 2},
        'bi': {column: 9, row: 2},
        'kh': {column: 10, row: 2},
        'cm': {column: 11, row: 2},
        'ca': {column: 12, row: 2},
        'cv': {column: 13, row: 2},
        'cf': {column: 14, row: 2},
        'td': {column: 15, row: 2},
        'cl': {column: 16, row: 2},
        'cn': {column: 17, row: 2},
        'co': {column: 18, row: 2},
        'km': {column: 19, row: 2},
        'cd': {column: 20, row: 2},

        'cg': {column: 1, row: 3},
        'cr': {column: 2, row: 3},
        'ci': {column: 3, row: 3},
        'hr': {column: 4, row: 3},
        'cu': {column: 5, row: 3},
        'cy': {column: 6, row: 3},
        'cz': {column: 7, row: 3},
        'dk': {column: 8, row: 3},
        'dj': {column: 9, row: 3},
        'dm': {column: 10, row: 3},
        'do': {column: 11, row: 3},
        'll': {column: 12, row: 3}, // Dummy country code
        'ec': {column: 13, row: 3},
        'eg': {column: 14, row: 3},
        'sv': {column: 15, row: 3},
        'gq': {column: 16, row: 3},
        'er': {column: 17, row: 3},
        'ee': {column: 18, row: 3},
        'et': {column: 19, row: 3},
        'fj': {column: 20, row: 3},

        'fi': {column: 1, row: 4},
        'fr': {column: 2, row: 4},
        'ga': {column: 3, row: 4},
        'gm': {column: 4, row: 4},
        'ge': {column: 5, row: 4},
        'de': {column: 6, row: 4},
        'gh': {column: 7, row: 4},
        'gr': {column: 8, row: 4},
        'gd': {column: 9, row: 4},
        'gt': {column: 10, row: 4},
        'gn': {column: 11, row: 4},
        'gw': {column: 12, row: 4},
        'gy': {column: 13, row: 4},
        'ht': {column: 14, row: 4},
        'hn': {column: 15, row: 4},
        'hu': {column: 16, row: 4},
        'is': {column: 17, row: 4},
        'in': {column: 18, row: 4},
        'id': {column: 19, row: 4},
        'ir': {column: 20, row: 4},

        'iq': {column: 1, row: 5},
        'ie': {column: 2, row: 5},
        'il': {column: 3, row: 5},
        'it': {column: 4, row: 5},
        'jm': {column: 5, row: 5},
        'jp': {column: 6, row: 5},
        'jo': {column: 7, row: 5},
        'kz': {column: 8, row: 5},
        'ke': {column: 9, row: 5},
        'kr': {column: 10, row: 5},
        'kp': {column: 11, row: 5},
        'kw': {column: 12, row: 5},
        'kg': {column: 13, row: 5},
        'la': {column: 14, row: 5},
        'lv': {column: 15, row: 5},
        'lb': {column: 16, row: 5},
        'ls': {column: 17, row: 5},
        'lr': {column: 18, row: 5},
        'ly': {column: 19, row: 5},
        'li': {column: 20, row: 5},

        'lt': {column: 1, row: 6},
        'lu': {column: 2, row: 6},
        'mk': {column: 3, row: 6},
        'mg': {column: 4, row: 6},
        'mw': {column: 5, row: 6},
        'my': {column: 6, row: 6},
        'mv': {column: 7, row: 6},
        'ml': {column: 8, row: 6},
        'mt': {column: 9, row: 6},
        'mh': {column: 10, row: 6},
        'mr': {column: 11, row: 6},
        'mu': {column: 12, row: 6},
        'mx': {column: 13, row: 6},
        'fm': {column: 14, row: 6},
        'md': {column: 15, row: 6},
        'mc': {column: 16, row: 6},
        'mn': {column: 17, row: 6},
        'me': {column: 18, row: 6},
        'ma': {column: 19, row: 6},
        'mz': {column: 20, row: 6},

        'na': {column: 1, row: 7},
        'nr': {column: 2, row: 7},
        'np': {column: 3, row: 7},
        'nl': {column: 4, row: 7},
        'nz': {column: 5, row: 7},
        'ni': {column: 6, row: 7},
        'ne': {column: 7, row: 7},
        'ng': {column: 8, row: 7},
        'no': {column: 9, row: 7},
        'om': {column: 10, row: 7},
        'pk': {column: 11, row: 7},
        'pw': {column: 12, row: 7},
        'pa': {column: 13, row: 7},
        'pg': {column: 14, row: 7},
        'py': {column: 15, row: 7},
        'pe': {column: 16, row: 7},
        'ph': {column: 17, row: 7},
        'pl': {column: 18, row: 7},
        'pt': {column: 19, row: 7},
        'qa': {column: 20, row: 7},

        'ro': {column: 1, row: 8},
        'ru': {column: 2, row: 8},
        'rw': {column: 3, row: 8},
        'kn': {column: 4, row: 8},
        'wc': {column: 5, row: 8},
        'vc': {column: 6, row: 8},
        'ws': {column: 7, row: 8},
        'sm': {column: 8, row: 8},
        'st': {column: 9, row: 8},
        'sa': {column: 10, row: 8},
        'sn': {column: 11, row: 8},
        'rs': {column: 12, row: 8},
        'sc': {column: 13, row: 8},
        'sl': {column: 14, row: 8},
        'sg': {column: 15, row: 8},
        'sk': {column: 16, row: 8},
        'sb': {column: 17, row: 8},
        'so': {column: 18, row: 8},
        'za': {column: 19, row: 8},
        'es': {column: 20, row: 8},

        'lk': {column: 1, row: 9},
        'sd': {column: 2, row: 9},
        'sr': {column: 3, row: 9},
        'sz': {column: 4, row: 9},
        'se': {column: 5, row: 9},
        'ch': {column: 6, row: 9},
        'sy': {column: 7, row: 9},
        'tj': {column: 8, row: 9},
        'tz': {column: 9, row: 9},
        'th': {column: 10, row: 9},
        'tg': {column: 11, row: 9},
        'to': {column: 12, row: 9},
        'tt': {column: 13, row: 9},
        'tn': {column: 14, row: 9},
        'tr': {column: 15, row: 9},
        'tm': {column: 16, row: 9},
        'tv': {column: 17, row: 9},
        'ug': {column: 18, row: 9},
        'ua': {column: 19, row: 9},
        'ae': {column: 20, row: 9},

        'uk': {column: 1, row: 10},
        'us': {column: 2, row: 10},
        'uy': {column: 3, row: 10},
        'uz': {column: 4, row: 10},
        'vu': {column: 5, row: 10},
        've': {column: 6, row: 10},
        'vn': {column: 7, row: 10},
        'ye': {column: 8, row: 10},
        'zm': {column: 9, row: 10},
        'zw': {column: 10, row: 10},
        'va': {column: 11, row: 10},
        'ab': {column: 12, row: 10},
        'ps': {column: 13, row: 10},
        'tw': {column: 14, row: 10},
        'hk': {column: 15, row: 10},
        'eu': {column: 16, row: 10}
    },

    GmsType: {
        Summary: 0,
        Commodities: 4,
        Currencies: 7,
        Indices: 8
    },

    WindowType: {
        MarketDepthByPrice: '3',
        MarketDepthByOrder: '20',
        MarketDepthByPriceAdvanced: '60',
        MarketDepthByOrderAdvanced: '61',
        News: '213'
    },

    SymbolStatus: {
        Available: '0',
        Expired: '1',
        Delisted: '2',
        Suspended: '3',
        Halted: '4',
        Delayed: '5',
        ShuttingDown: '6'
    },

    AlertStatus: {
        Active: 'active',
        Triggered: 'triggered',
        Rejected: 'rejected'
    },

    AlertParamMap: {
        BEST_BID: 'bestBid',
        BEST_ASK: 'bestOffer',
        BEST_ASK_QTY: 'offerQty',
        BEST_BID_QTY: 'bidQty',
        VOLUME: 'volume',
        CHANGE: 'change',
        LAST_TRADE_PRICE: 'last',
        LAST_TRADE_QTY: 'lastQty',
        PCT_CHANGE: 'perChange'
    }
};

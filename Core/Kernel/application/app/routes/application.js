import Ember from 'ember';
import appEvents from '../app-events';
import utils from '../utils/utils';
import sharedService from '../models/shared/shared-service';
import layoutHandler from '../controllers/layout/layout-handler';

export default Ember.Route.extend({
    appLayout: {},
    clickEventHandler: null,

    beforeModel: function (transition) {
        if (transition.queryParams.loading) {
            this.set('templateName', 'application-child');
        } else {
            this.set('templateName', 'application');
        }
    },

    renderTemplate: function (controller, model) {
        this._super(controller, model); // Render the base template
        this._initializeTemplate();
    },

    addVisibilityChangeListener: function () {
        if (document.addEventListener) {
            document.addEventListener('visibilitychange', function () {
                appEvents.onVisibilityChanged(document.hidden);
            });
        }
    },

    addAppCloseListener: function () {
        window.onbeforeunload = function () {
            utils.webStorage.addString(utils.webStorage.getKey(utils.Constants.CacheKeys.LoggedIn), utils.Constants.No, utils.Constants.StorageType.Session);
            appEvents.onAppClose();
        };
    },

    addOrientationChangeListener: function () {
        if (window.addEventListener && window.DeviceOrientationEvent) {
            window.addEventListener('orientationchange', function () {
                appEvents.onOrientationChanged();
            });

            appEvents.onOrientationChanged(); // To set current orientation on first time load
        }
    },

    addDocumentClickListener: function () {
        this.clickEventHandler = this.onDocumentClick.bind(this);
        document.addEventListener('mousedown', this.clickEventHandler, true);
    },

    onDocumentClick: function (e) {
        Ember.appGlobal.events.mousedown = e;
    },

    _initializeTemplate: function () {
        var layoutConfig = layoutHandler.getLayoutConfig('u1', Ember.appGlobal.queryParams[utils.Constants.EmbeddedModeParams.ChildWindowId]); // TODO: [DineshS] user ID need to pass
        sharedService.getService('sharedUI').registerService('appLayoutConfig', layoutConfig);

        this.get('controller').mainOutletStyle = 'col-xs-10 full-height';
        this.get('controller').rightPanelStyle = '';

        this.addDomReadyListener();
        this.addVisibilityChangeListener();
        this.addAppCloseListener();
        this.addDocumentClickListener();
        this.addOrientationChangeListener();

        layoutHandler.renderTemplate(layoutConfig, this);
        this._initializeSharedComponents();
        appEvents.onLayoutReady(this);
    },

    _initializeSharedComponents: function () {
        var messageViewer = this.container.lookupFactory('component:single-message-viewer').create();
        sharedService.getService('sharedUI').registerService('single-message-viewer', messageViewer);
    },

    addDomReadyListener: function () {
        window.addEventListener('load', function () {
            appEvents.onDomReady();

            window.removeEventListener('load', function () {
            }); // Remove listener, no longer needed
        });
    },

    actions: {
        renderMenuItems: function (menuContent) {
            sharedService.getService('sharedUI').getService('mainPanel').onRenderMenuItems(menuContent);
        },

        renderDirectTabItems: function (menuContent, tabContent) {
            sharedService.getService('sharedUI').getService('mainPanel').onRenderMenuItems(menuContent, tabContent.id);
        },

        renderTabItems: function (tabContent) {
            sharedService.getService('sharedUI').getService('mainPanel').onRenderTabItems(tabContent);
        },

        renderRightPanelItems: function (rightPanelContent) {
            sharedService.getService('sharedUI').getService('rightPanel').renderRightPanelView(rightPanelContent, {isUserClick: true});
            utils.analyticsService.trackEvent('right-panel', utils.Constants.GAActions.viewChanged, ['tab:', rightPanelContent.layoutTemplate].join(''));
        }
    }
});
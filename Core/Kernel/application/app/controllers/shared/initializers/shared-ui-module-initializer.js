import Ember from 'ember';
import BaseModuleInitializer from '../../../models/shared/initializers/base-module-initializer';
import sharedDataModuleInitializer from '../../../models/shared/initializers/shared-data-module-initializer';
import languageInitializer from '../../../controllers/shared/initializers/language-initializer';
import themeInitializer from '../../../controllers/shared/initializers/theme-initializer';
import LoginViewController from '../../../controllers/authentication/login-view-controller';
import sharedService from '../../../models/shared/shared-service';
import utils from '../../../utils/utils';
import appConfig from '../../../config/app-config';
import LanguageDataStore from '../../../models/shared/language/language-data-store';
import SharedUIService from '../shared-ui-service';
import appEvents from '../../../app-events';

export default BaseModuleInitializer.extend({
    preInitialize: function () {
        var service = this.createService();

        sharedService.registerService(service.subscriptionKey, service);
        appEvents.subscribeLayoutReady(service.subscriptionKey, service);
    },

    postInitialize: function () {
        var that = this;

        this.loginViewController = new LoginViewController();
        this.loginViewController.initialize(sharedDataModuleInitializer.authType, sharedDataModuleInitializer.authController);
        languageInitializer.prepareLanguageView();

        sharedService.getService('sharedUI').set('loginViewController', this.loginViewController);

        themeInitializer.prepareThemeView(function () {
            that._showLoginView();
        });

        this._showLandingPage();
    },

    createService: function () {
        return SharedUIService.create();
    },

    _showLoginView: function () {
        if (sharedDataModuleInitializer.authType === sharedDataModuleInitializer.authTypes.retail) {
            this._prepareLoginView(false);
        } else {
            this._authenticateSsoUser(appConfig.ssoToken);
        }
    },

    _showLandingPage: function () {
        if (sharedDataModuleInitializer.authType === sharedDataModuleInitializer.authTypes.retail) {
            this._showRetailLandingPage();
        } else {
            this._showSsoLandingPage();
        }
    },

    _showRetailLandingPage: function () {
        var that = this;
        var password = '';
        var username = sharedService.userSettings.username;
        var storedPwd = sharedService.userSettings.password;
        var isRemember = sharedService.userSettings.rememberMe === utils.Constants.Yes;

        if (isRemember && utils.validators.isAvailable(storedPwd)) {
            password = utils.crypto.decryptText(storedPwd, utils.Constants.Encryption.TDesSecondaryKey, utils.Constants.Encryption.TDesSecondaryIv);
        }

        var isUserAvailable = utils.validators.isAvailable(username) && utils.validators.isAvailable(password);

        if (!Ember.appGlobal.multiScreen.isParentWindow) {
            this.loginViewController.showHomePage();
        } else if (isUserAvailable && sharedService.userSettings.currentLoginStatus === sharedDataModuleInitializer.loginStatus.loggedIn) {
            sharedDataModuleInitializer.authController.authenticateUser(username, password, true, function (_username, _password, allowInit) {
                that._retailAuthSuccess(_username, _password, allowInit);
            }, function (reason, _username, _password) {
                that._retailAuthFail(reason, _username, _password);
            });
        } else if (sharedService.userSettings.currentLoginStatus >= sharedDataModuleInitializer.loginStatus.prevLoggedIn) {
            this.loginViewController.setLastLoggedInUser(username, password);
            this._prepareLoginView(true);
        } else {
            if (!appConfig.customisation.hidePreLogin) {
                if (appConfig.customisation.supportedLanguages.length > 1) {
                    languageInitializer.showLanguageView();
                } else {
                    themeInitializer.showThemeView();
                }
            } else {
                this._prepareLoginView(true);
            }
        }
    },

    _prepareLoginView: function (animateOnLoad) {
        var that = this;
        this._showAuthFailMessage();

        this.loginViewController.prepareLoginView(function (_username, _password, allowInit) {
            that._retailAuthSuccess(_username, _password, allowInit);
        }, function (reason, _username, _password) {
            that._retailAuthFail(reason, _username, _password);
        }, animateOnLoad);
    },

    _showSsoLandingPage: function () {
        var ssoToken = appConfig.ssoToken;
        var queryParamSession = Ember.appGlobal.queryParams.appParams.sid;
        var authToken = appConfig.customisation.isEmbeddedMode ? queryParamSession : ssoToken;

        if (utils.validators.isAvailable(authToken)) {
            if (sharedService.userSettings.currentLoginStatus === sharedDataModuleInitializer.loginStatus.loggedIn ||
                sharedService.userSettings.currentLoginStatus === sharedDataModuleInitializer.loginStatus.prevLoggedIn) {
                this._authenticateSsoUser(authToken);
            } else {
                if (appConfig.customisation.supportedLanguages.length > 1 && !this._isLanguageSupported(sharedService.userSettings.get('currentLanguage'))) {
                    languageInitializer.showLanguageView();
                } else if (appConfig.customisation.supportedThemes.length > 1 && !this._isThemeSupported(sharedService.userSettings.get('currentTheme'))) {
                    themeInitializer.showThemeView();
                } else {
                    this._authenticateSsoUser(authToken);
                }
            }
        } else {
            this._showAuthFailMessage();
        }
    },

    _showAuthFailMessage: function () {
        var loginMsgKey = utils.webStorage.getKey(utils.Constants.CacheKeys.LoginErrorMsg);
        var authFailReason = utils.webStorage.getString(loginMsgKey, utils.Constants.StorageType.Session);

        if (sharedDataModuleInitializer.authType === sharedDataModuleInitializer.authTypes.sso) {
            authFailReason = utils.validators.isAvailable(authFailReason) ? authFailReason : LanguageDataStore.getLanguageObj().lang.messages.loggedOut;
        }

        if (utils.validators.isAvailable(authFailReason)) {
            utils.webStorage.remove(loginMsgKey, utils.Constants.StorageType.Session);
            this.loginViewController.showLoginView(utils.formatters.convertUnicodeToNativeString(authFailReason), true); // Show authentication failure reason
        }
    },

    _authenticateSsoUser: function (ssoToken) {
        var that = this;

        sharedDataModuleInitializer.authController.authenticateUser(ssoToken, function () {
            that._ssoAuthSuccess();
        }, function (reason) {
            that._ssoAuthFail(reason);
        });
    },

    _retailAuthSuccess: function (username, password, allowInit) {
        if (allowInit) {
            this.loginViewController.storeUserData(username, password);

            var tradeService = sharedService.getService('trade');

            if (tradeService && tradeService.userDS && tradeService.userDS.authSts === tradeService.constants.AuthStatus.OtpEnabled) {
                this.loginViewController.showOtpLogin();
            } else {
                this.loginViewController.showHomePage();

                if (Ember.$.isFunction(utils.nativeHelper.initializeWindows)) { // DT multiple window creation at user login
                    Ember.run.later(function () {
                        utils.nativeHelper.initializeWindows();
                    }, 2000);
                }

                Ember.$(window).unbind('keypress'); // Unbind enter key pressing event from window
            }
        }
    },

    _retailAuthFail: function (reason, username, password) {
        var that = this;

        sharedService.userSettings.clearLoginToken();
        this.loginViewController.showAuthFailMessage(reason);

        this.loginViewController.prepareLoginFailedView(username, password, function (_username, _password, allowInit) {
            that._retailAuthSuccess(_username, _password, allowInit);
        }, function (_reason, _username, _password) {
            that._retailAuthFail(_reason, _username, _password);
        });
    },

    _ssoAuthSuccess: function () {
        this.loginViewController.showHomePage();
    },

    _ssoAuthFail: function (reason) {
        this.loginViewController.showAuthFailMessage(reason);
    },

    _isLanguageSupported(language) {
        var isLanguageSupported = false;

        Ember.$.each(appConfig.customisation.supportedLanguages, function (index, lang) {
            if (language.toUpperCase() === lang.code) {
                isLanguageSupported = true;
                return false;
            }
        });

        return isLanguageSupported;
    },

    _isThemeSupported(themeToSupport) {
        var isThemeSupported = false;

        Ember.$.each(appConfig.customisation.supportedThemes, function (index, theme) {
            if (themeToSupport.toLowerCase() === theme.code) {
                isThemeSupported = true;
                return false;
            }
        });

        return isThemeSupported;
    }
});

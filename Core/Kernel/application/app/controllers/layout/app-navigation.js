import Ember from 'ember';
import BaseWidgetContainer from '../base-widget-container';

export default BaseWidgetContainer.extend({
    // Subscription key
    containerKey: 'leftPanel',

    onLoadContainer: function () {
        this._super();
        this._setMenuTitle();
    },

    onRenderUI: function () {
        this._setMenuTitle();
    },

    languageChanged: function (language) {
        this._super(language);

        var menuArray = this.get('appLayout').layout.mainPanel.content;
        var that = this;

        Ember.$.each(menuArray, function (key, menuObj) {
            if (that.utils.validators.isAvailable(menuObj.customTitle)) {
                Ember.set(menuObj, 'displayTitle', menuObj.customTitle); // Rename custom workspace; Get from local storage
            } else {
                Ember.set(menuObj, 'displayTitle', that.get('app').lang.labels[menuObj.title]);
            }
        });
    }
});
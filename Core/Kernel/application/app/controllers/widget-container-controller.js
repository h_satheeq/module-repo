import Ember from 'ember';
import BaseWidgetContainer from '../controllers/base-widget-container';
import sharedService from '../models/shared/shared-service';
import controllerFactory from './controller-factory';
import utils from '../utils/utils';

export default BaseWidgetContainer.extend({
    onLoadContainer: function () {
        this._super();
        this.set('tabCacheMap', {});
    },

    onRenderUI: function () {
        var defaultMenu = this._getDefaultMenu();
        this.renderMenuView(defaultMenu, true, {isFirstTime: true, isBackground: false, isUserClick: false});
    },

    onRenderMenuItems: function (menuContent, tabId, args) {
        // Not allowed to load same menu more than once consecutively
        if (this.menuContent && this.menuContent.id === menuContent.id) {
            var menuPanelContainer = sharedService.getService('sharedUI').getService('menuPanel');

            if (menuPanelContainer.isHorizontalPanel && !tabId && menuContent.tab.length > 1) {
                menuPanelContainer.setSubMenu(menuContent, !menuContent.isExpanded); // When we click on same main menu again menu will collapsed
            }

            if (!tabId || this.tabContent.id === tabId) {
                return;
            }
        }

        this.toggleWorkSpaceTabState(menuContent, tabId);
        this.renderMenuView(menuContent, false, {isFirstTime: false, isBackground: false, isUserClick: true}, tabId, args);
    },

    toggleWorkSpaceTabState: function (menuContent, tabId) {
        var menuPanelContainer = sharedService.getService('sharedUI').getService('menuPanel');

        if (this.menuContent && this.menuContent.custom && this.menuContent.id !== menuContent.id && !this.menuContent.isEdited) {
            if (!this.menuContent.displayTitle) {
                Ember.set(this.menuContent, 'displayTitle', this.app.lang.labels.workspace);
            }

            Ember.set(this.menuContent, 'isEdited', true);

            this.saveMenuArgs({isEdited: true});
            menuPanelContainer.getSubMenuWidths();
        }

        if (this.tabContent && this.tabContent.custom && this.tabContent.id !== tabId && !this.tabContent.isEdited) {
            if (!this.tabContent.displayTitle) {
                Ember.set(this.tabContent, 'displayTitle', this.app.lang.labels.custom);
            }

            Ember.set(this.tabContent, 'isEdited', true);

            this.saveTabArgs({isEdited: true});
            menuPanelContainer.getSubMenuWidths();
        }
    },

    onRenderTabItems: function (tabContent) {
        var preparedTab = this.onPrepareTab(tabContent, this.menuContent, this.widgetArgs, sharedService.userState.defaultWS[this.get('containerKey')]);
        this.renderTabView(this.menuContent, preparedTab, false, {isFirstTime: false, isBackground: false, isUserClick: true});
    },

    getContainerArgs: function (menuContent) {
        // TODO: [Bashitha] Remove container level arguments and introduce to widget level

        var symbol, exchange, insType;
        var containerArgs = this._super(menuContent); // Call the getContainerArgs() function in base controller
        var savedSettings = sharedService.userState.lastArgs;

        if (savedSettings && savedSettings[menuContent.id] && savedSettings[menuContent.id].sym && savedSettings[menuContent.id].exg) {
            symbol = savedSettings[menuContent.id].sym;
            exchange = savedSettings[menuContent.id].exg;
            insType = savedSettings[menuContent.id].inst;
        } else {
            var store = sharedService.getService('price').stockDS.get('stockMapByExg');

            if (store) {
                exchange = sharedService.userSettings.currentExchange;
                var symbolList = store[exchange];

                if (symbolList && symbolList.length > 0) {
                    var defaultSymbol = symbolList[0];

                    symbol = defaultSymbol.sym; // Get first symbol as the default symbol
                    insType = defaultSymbol.inst;
                }
            }
        }

        containerArgs.sym = symbol;
        containerArgs.exg = exchange;
        containerArgs.inst = insType;

        return containerArgs;
    },

    renderMenuView: function (menuContent, isReRender, renderOptions, tabId, args) {
        // TODO: [Bashitha] Implement a component for main menu and set active css within the component and raise the event
        var menuPanelContainer = sharedService.getService('sharedUI').getService('menuPanel');
        var isActiveMenu = renderOptions && (renderOptions.isFirstTime || renderOptions.isUserClick);

        if (menuPanelContainer && isActiveMenu) {
            menuPanelContainer.setActiveMenu(menuContent);
        }

        // Prepare tabs by merging with saved arguments
        var that = this;
        var preparedTabs = [];

        Ember.$.each(menuContent.tab, function (key, tabContent) {
            preparedTabs[preparedTabs.length] = that.onPrepareTab(tabContent, menuContent, that.widgetArgs, sharedService.userState.defaultWS[that.get('containerKey')]);
        });

        this._prepareMenu(menuContent, preparedTabs, isActiveMenu);

        if (isActiveMenu) {
            var defaultTab = this._getDefaultTab(menuContent.id, preparedTabs, tabId);
            this.renderTabView(menuContent, defaultTab, isReRender, renderOptions, args);
        }
    },

    renderTabView: function (menuContent, tabContent, isReRender, renderOptions, args) {
        // Not allowed to load same tab more than once consecutively
        if (!isReRender && sharedService.userState.lastMenu === menuContent.id &&
            sharedService.userState.lastTab && sharedService.userState.lastTab[menuContent.id] === tabContent.id) {
            return;
        }

        var that = this;
        var menuPanel = sharedService.getService('sharedUI').getService('menuPanel');
        var isActiveMenu = renderOptions && (renderOptions.isFirstTime || renderOptions.isUserClick);
        var renderDelay = !renderOptions.isFirstTime && menuPanel && menuPanel.isHorizontalPanel && menuContent.id !== this.menuContent.id && menuContent.tab.length > 1 ? 50 : 1;
        var prevMenu = this.menuContent;
        var prevTab = this.tabContent;

        if (isActiveMenu) {
            this.saveLastActiveTab(menuContent, tabContent);
            this._setActiveTab(menuContent, tabContent);
        }

        Ember.run.later(this, function () {
            that._renderViewport(menuContent, tabContent, renderOptions);

            // Selected layout may change for each tab
            if (tabContent.custom) {
                that.router.controllerFor(tabContent.outlet).onLoadLayoutContainer(that.router, menuContent, tabContent, that);
            }

            if (isActiveMenu) {
                that.menuContent = menuContent;
                that.tabContent = tabContent;
            }

            that._prepareTab(menuContent, tabContent, renderOptions, args);
            that.initializePostRender(menuContent, tabContent, renderOptions);

            // Remove existing widgets
            if (prevTab && renderOptions && renderOptions.isUserClick) {
                Ember.run.later(function () {
                    that.closeContainerWidgets(prevMenu, prevTab);
                }, 250);
            }

            that.utils.analyticsService.trackPage([menuContent.title, tabContent.outlet.split('.').pop()].join(':'));
        }, renderDelay);
    },

    renderWidget: function (menuContent, tabContent, widgetDef, args, innerWidgetParams) {
        var widgetArgs, widgetController;
        var activeWidget = innerWidgetParams ? innerWidgetParams.activeWidget : widgetDef;
        activeWidget.cacheInBackground = widgetDef.cacheInBackground;
        activeWidget.loadFromCache = widgetDef.loadFromCache && !activeWidget.isInnerWidget;

        if (!activeWidget.loadFromCache) {
            var templateName = tabContent.custom ? 'custom-workspace.custom-layout-view' : tabContent.outlet;
            widgetController = controllerFactory.createController(this.container, 'controller:' + activeWidget.wn);

            this.router.render(activeWidget.wn, {
                into: templateName,
                outlet: 'w' + widgetDef.id,
                controller: widgetController
            });

            if (innerWidgetParams) {
                this.saveLastActiveInnerWidget(widgetDef, innerWidgetParams.activeWidget);
            }

            this.setContainerWidget(menuContent.id, tabContent.id, widgetDef.id, widgetController);

            // Call initializeWidget() in each loaded widget with arguments
            widgetArgs = this.getWidgetArgs(activeWidget, tabContent, menuContent);
            widgetArgs = this._setWidgetArgs(args, widgetArgs); // 'args' will contain default arguments passed in to widgets. Ex: mode: 1 for MDP
            widgetArgs = this._setWidgetArgs(innerWidgetParams ? innerWidgetParams.customArgs : undefined, widgetArgs);
        } else {
            widgetController = this.controllers[this.menuContent.id][this.tabContent.id][widgetDef.id];
        }

        widgetController.initializeWidget(activeWidget, widgetArgs, this, menuContent, tabContent);
        return widgetController;
    },

    prepareWidget: function (menuContent, tabContent, widgetDef, args) {
        this.renderWidget(menuContent, tabContent, widgetDef, args);
    },

    languageChanged: function (language) {
        this.set('widgetTitle', this.get('app').lang.labels[this.menuContent.widgetTitle]);

        this._setTabDisplayTitle(this.get('tabList'));
        this._super(language);
    },

    _setWidgetArgs: function (args, widgetArgs) {
        if (args) {
            // args will contain default arguments passed in to widgets. Ex: mode: 1 for MDP
            widgetArgs.widgetArgs = widgetArgs.widgetArgs || {};

            Ember.$.each(args, function (prop, val) {
                widgetArgs.widgetArgs[prop] = val;
            });
        }

        return widgetArgs;
    },

    _renderViewport: function (menuContent, tabContent, renderOptions) {
        if (renderOptions && (renderOptions.isFirstTime || (renderOptions.isBackground && tabContent.cache) ||
            (renderOptions.isUserClick && tabContent.cache && !this.tabCacheMap[tabContent.title]) ||
            (renderOptions.isUserClick && !tabContent.cache))) {
            this.router.render(tabContent.outlet, {
                into: this.get('appLayout').layout[this.get('containerKey')].template,
                outlet: tabContent.title
            });

            var containerController = this.router.controllerFor(tabContent.outlet);
            containerController.set('containerOutletName', tabContent.outlet.split('.').pop());

            this.addContainerToMap(containerController, menuContent, tabContent); // For custom workspace

            Ember.run.later(function () {
                containerController.onAfterRender();
            }, 500);
        }

        if (renderOptions && renderOptions.isFirstTime) {
            Ember.$('div[name=mpl-' + tabContent.title + ']').css('z-index', 0);
        }

        if (renderOptions && renderOptions.isUserClick) {
            var zIndexPrev = -(this.menuContent.id * 100 + this.tabContent.id * 10);

            Ember.$('div[name=mpl-' + this.tabContent.title + ']').css('z-index', zIndexPrev);
            Ember.$('div[name=mpl-' + tabContent.title + ']').css('z-index', 0);

            if (!this.tabContent.cache && !this.tabContent.custom) {
                this.clearRender(this.menuContent, this.tabContent);

                this.router.disconnectOutlet({
                    parentView: this.get('appLayout').layout[this.get('containerKey')].template,
                    outlet: this.tabContent.title
                });
            }
        }

        if (renderOptions && renderOptions.isBackground) {
            var zIndexCurrent = -(menuContent.id * 100 + tabContent.id * 10);
            Ember.$('div[name=mpl-' + tabContent.title + ']').css('z-index', zIndexCurrent);
        }
    },

    _prepareTab: function (menuContent, tabContent, renderOptions, args) {
        if (renderOptions && (!renderOptions.isBackground || tabContent.cache)) {
            if (tabContent.w !== undefined) {
                var isCached = this.tabCacheMap[tabContent.title];
                var that = this;

                Ember.$.each(tabContent.w, function (wKey, widgetDef) {
                    if (widgetDef) {
                        widgetDef.cacheInBackground = renderOptions && renderOptions.isBackground && tabContent.cache;
                        widgetDef.loadFromCache = renderOptions && renderOptions.isUserClick && tabContent.cache && isCached;

                        if (!tabContent.custom) {
                            that.prepareWidget(menuContent, tabContent, widgetDef, args);
                        }
                    }
                });

                this.tabCacheMap[tabContent.title] = tabContent.cache;
            }
        }
    },

    _setActiveTab: function (menuContent, currentTab) {
        var menuArray = this.get('appLayout').layout.mainPanel.content;

        Ember.$.each(menuArray, function (key, menuObj) {
            Ember.$.each(menuObj.tab, function (tabId, tabObj) {
                Ember.set(tabObj, 'css', '');

                if (menuObj.id === menuContent.id && tabObj.id === currentTab.id) {
                    Ember.set(tabObj, 'css', 'active');
                }
            });
        });
    },

    _getDefaultMenu: function () {
        var savedLayout, configLayout;
        var contentArray = this.get('appLayout').layout[this.get('containerKey')].content;
        var savedMenuId = this.getLastActiveMenu(contentArray);

        savedMenuId = savedMenuId ? parseInt(savedMenuId, 10) : -1;

        if (contentArray && contentArray.length > 0) {
            Ember.$.each(contentArray, function (index, layout) {
                if (savedMenuId > -1 && layout.id === savedMenuId) {
                    savedLayout = layout;
                    return false;
                }

                if (layout.def) {
                    configLayout = layout;
                }
            });
        }

        // noinspection JSUnusedAssignment
        return savedLayout ? savedLayout : configLayout;
    },

    _getDefaultTab: function (menuId, menuTabs, tabId) {
        var defTab = {};
        var savedTabObj = this.getLastActiveTab();
        var savedTabId = tabId ? tabId : (savedTabObj && savedTabObj[menuId]) ? savedTabObj[menuId] : -1;

        if (menuTabs && menuTabs.length > 0) {
            Ember.$.each(menuTabs, function (index, tab) {
                if (savedTabId > -1) {
                    if (tab.id === savedTabId) {
                        defTab = tab;
                        return false;
                    }
                } else if (tabId > -1 && tab.id === tabId) {
                    defTab = tab;

                    return false;
                } else if (tab.def) {
                    defTab = tab;
                    return false;
                }
            });
        }

        return defTab;
    },

    _prepareMenu: function (menuContent, menuTabs, isActiveMenu) {
        var isHorizontalPanel = sharedService.getService('sharedUI').getService('menuPanel').isHorizontalPanel;

        this.set('showTitle', menuContent.isShowTitle);
        this.set('widgetTitle', this.get('app').lang.labels[menuContent.widgetTitle]);

        if (menuTabs) {
            if (isActiveMenu) {
                this.set('tabList', menuTabs);
            }

            this._setTabDisplayTitle(menuTabs);
            this.set('showTabs', menuTabs.length > 1 && !menuContent.isHideTab);

            Ember.set(this.get('displayTabs'), menuContent.title, !isHorizontalPanel && menuTabs.length > 1);
            Ember.set(this.get('menuTabMap'), menuContent.title, menuTabs);
        }
    },

    _setTabDisplayTitle: function (menuTabs) {
        var that = this;

        if (menuTabs) {
            Ember.$.each(menuTabs, function (key, tab) {
                try {
                    if (utils.validators.isAvailable(tab.customTitle)) {
                        try {
                            Ember.set(tab, 'displayTitle', tab.customTitle); // Rename custom workspace; Get from local storage
                        } catch (e) {
                            tab.displayTitle = tab.customTitle;
                        }
                    } else {
                        Ember.set(tab, 'displayTitle', that.get('app').lang.labels[tab.title]);
                    }
                } catch (e) {
                    tab.displayTitle = that.get('app').lang.labels[tab.title];
                }
            });
        }
    }
});

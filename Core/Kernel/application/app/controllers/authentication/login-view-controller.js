/* global jQKeyboard */
import Ember from 'ember';
import sharedService from '../../models/shared/shared-service';
import languageDataStore from '../../models/shared/language/language-data-store';
import utils from '../../utils/utils';
import appConfig from '../../config/app-config';
import sharedDataModuleInitializer from '../../models/shared/initializers/shared-data-module-initializer';

export default function () {
    var that = this;
    var authType;
    var authController;
    var controllerKey = 'loginViewController';

    var initialize = function (authTyp, authCtrl) {
        authController = authCtrl;
        authType = authTyp;

        Ember.$('#appName').text(appConfig.customisation.appName);

        if (!appConfig.customisation.isPoweredByEnabled) {
            Ember.$('#poweredByText').hide();
        }
    };

    var prepareLoginView = function (authSuccess, authFail, animateOnLoad) {
        _bindEvents();
        _setDisplayTexts();
        setSelectedLanguageInLogin();

        Ember.$('button#btnLogin').bind('click', function () {
            goToOtpLoginOrHome(authSuccess, authFail);
        });

        Ember.$('a#registerLink').bind('click', function () {
            showRegistration();
        });

        Ember.$('a#lnkForgotPwd').bind('click', function () {
            showForgotPassword();
        });

        Ember.$('a#lnkOnlineAcc').bind('click', function () {
            showOnlineAccountConfirm();
        });

        // Bind enter key to login
        bindEnterKeyLogin(authSuccess, authFail);
        _showLoginPage(animateOnLoad);
    };

    var showLoginView = function (loginMsg, animateOnLoad, isHideControls) {
        if (authType === 2) { // TODO: [Bashitha] Access authTypes enum from here
            Ember.$('[authType="retail"]').hide();
        }

        _showLoginPage(animateOnLoad, authType, isHideControls);

        var loginMsgElem = Ember.$('div#loginMsg');

        loginMsgElem.html(loginMsg).show(); // Show request error message
        loginMsgElem.css('background-color', '#e15848');
    };

    var _showLoginPage = function (animateOnLoad, authenticationType, isHideControls) {
        Ember.$('div#divLogin').show(); // Show login page

        Ember.$('#txtUsername').hide();
        Ember.$('#txtPassword').hide();
        Ember.$('#loginButton').hide();
        Ember.$('#btnLogin').hide();
        Ember.$('#chkRemember').hide();
        Ember.$('#spanRemember').hide();
        Ember.$('#virtualKeyboard').hide();
        Ember.$('#registerDiv').hide();
        Ember.$('#register').hide();
        Ember.$('#forgotPwd').hide();
        Ember.$('#onlineAcc').hide();
        Ember.$('#poweredByText').hide();
        Ember.$('#spanCopyright').hide();
        Ember.$('#termsAndConditions').hide();
        Ember.$('#help').hide();

        if (appConfig.customisation.isMobile) {
            Ember.$('#lnkSignUp').hide();
            Ember.$('#poweredByText').hide();
            Ember.$('#forgotPasswordUrl').hide();
            Ember.$('#register').hide();
        }

        if (!isHideControls) {
            if (animateOnLoad) {
            Ember.$(window).load(function () {
                _showLoginControls(authenticationType);
            });
        } else {
            _showLoginControls(authenticationType);
        }
        }
    };

    var _showLoginControls = function (authenticationType) {
        if (authenticationType !== 2) {
            Ember.$('#txtUsername').show();
            Ember.$('#txtPassword').show();
            Ember.$('#loginButton').show();
            Ember.$('#btnLogin').show();
            Ember.$('#chkRemember').show();
            Ember.$('#spanRemember').show();
            Ember.$('#spanCopyright').show();

            if (appConfig.customisation.isVirtualKeyboardEnable) {
                Ember.$('#virtualKeyboard').show();
            }

            if (appConfig.customisation.loginViewSettings.showTermsAndConditions) {
                Ember.$('#termsAndConditions').show();
            }

            var loginViewSettings = appConfig.customisation.loginViewSettings;

            if (loginViewSettings) {
                if (loginViewSettings.isSignUpEnabled) {
                    Ember.$('#lnkSignUp').show();
                }

                if (loginViewSettings.isPoweredByEnabled) {
                    Ember.$('#poweredByText').show();
                }

                if (loginViewSettings.isForgotPasswordEnabled) {
                    Ember.$('#registerDiv').show();
                    Ember.$('#forgotPwd').show();
                }

                if (loginViewSettings.isRegisterEnabled) {
                    Ember.$('#registerDiv').show();
                    Ember.$('#register').show();
                }

                if (loginViewSettings.isForgotPasswordEnabled) {
                    Ember.$('#forgotPasswordUrl').show();
                }

                if (loginViewSettings.isOnlineAccEnabled) {
                    Ember.$('#onlineAcc').show();
                }

                if (loginViewSettings.isHelpEnable) {
                    Ember.$('#help').show();
                }
            }

            _toggleLanguageSelection();
        }
    };

    /* *
     * Show authentication failure message
     * @param reason Reason for authentication failure
     * @private
     */
    var showAuthFailMessage = function (reason, isHideControls) {
        Ember.$('div#ember-app-root').css('visibility', 'hidden'); // Hide ember application view
        Ember.$('div#mainIndexChart').hide(); // Hide main index chart container

        _setDisplayTexts();

        showLoginView(utils.formatters.convertUnicodeToNativeString(reason), false, isHideControls); // Show authentication failure reason

        utils.webStorage.addString(utils.webStorage.getKey(utils.Constants.CacheKeys.LoggedIn), utils.Constants.No, utils.Constants.StorageType.Session);
    };

    /* *
     * Show Registration page
     */
    var showRegistration = function () {
        var controllerString = 'controller:trade/widgets/registration-forgot-password/registration';
        var routeString = 'trade/widgets/registration-forgot-password/registration';
        var popUpName = 'registrationPopUp';

        showFullScreenWidget(controllerString, routeString, popUpName);
    };

    /* *
     * Show Online Account Opening page
     */
    var showOnlineAccountConfirm = function () {
        var controllerString = 'controller:trade/widgets/online-account-opening';
        var routeString = 'trade/widgets/online-account-opening';
        var popUpName = 'onlineAccountPopUp';

        showFullScreenWidget(controllerString, routeString, popUpName);
    };

    /* *
     * Show Forgot Password page
     */
    var showForgotPassword = function () {
        var controllerString = 'controller:trade/widgets/registration-forgot-password/forgot-password';
        var routeString = 'trade/widgets/registration-forgot-password/forgot-password';
        var popUpName = 'forgotPasswordPopUp';

        showFullScreenWidget(controllerString, routeString, popUpName);
    };

    /* *
     * Show OTP Login page
     */
    var showOtpLogin = function () {
        if (sharedService.userSettings.currentLoginStatus !== sharedDataModuleInitializer.loginStatus.loggedIn) {
            var tradeUIService = sharedService.getService('tradeUI');

            if (tradeUIService) {
                tradeUIService.loadOtpLogin();
                bindEnterKeyOtpLogin();
            }
        }

        // Timer to avoid showing the home page before OTP Login page loaded.
        Ember.run.later(function () {
            showHomePage();
        }, 1);
    };

    var showFullScreenWidget = function (controllerString, routeString, popUpName) {
        if (sharedService.userSettings.currentLoginStatus !== sharedDataModuleInitializer.loginStatus.loggedIn) {
            sharedService.getService('sharedUI').renderFullScreenWidget(controllerString, routeString, popUpName, function () {
                hideRenderedPage();
            });
        }

        // Timer to avoid showing the home page before popup page loaded.
        Ember.run.later(function () {
            showHomePage();
        }, 1);
    };

    /* *
     * Hide Rendered page and show Login page
     */
    var hideRenderedPage = function () {
        _showLoginPage(false);

        Ember.$('div#ember-app-root').css('visibility', 'hidden'); // Hide ember application view

        var modal = sharedService.getService('sharedUI').getService('modalPopupId');

        modal.set('modalPopupStyle', '');
        modal.send('disableOverlay');
        modal.send('closeModalPopup');
    };

    /* *
     * Show home page
     * @private
     */
    var showHomePage = function () {
        sharedService.userSettings.previousLoggedIn = utils.Constants.Yes;
        sharedService.userSettings.save();

        utils.webStorage.addString(utils.webStorage.getKey(utils.Constants.CacheKeys.LoggedIn), utils.Constants.Yes, utils.Constants.StorageType.Session);

        if (appConfig.customisation.applicationIdleCheckConfig.isEnabledInactiveLogout) {
            utils.applicationSessionHandler.initializeApplicationIdleCheck();
        }

        Ember.$('input#txtPassword').val(''); // Clear user typed password
        Ember.$('div#divLogin').hide(); // Hide login page

        Ember.$('div#ember-app-root').css('visibility', 'visible'); // Show ember application view
        Ember.$('div#mainIndexChart').show(); // Show main index chart container
    };

    var prepareLoginFailedView = function (username, password, authSuccess, authFail) {
        _closeOtpPopup();
        _bindEvents();
        _setDisplayTexts();
        setLastLoggedInUser(username, password);
        setSelectedLanguageInLogin();
        bindEnterKeyLogin(authSuccess, authFail);
    };

    /* *
     * Saves last logged-in user credentials
     * @param username username
     * @param password password
     * @private
     */
    var setLastLoggedInUser = function (username, password) {
        var isRemember = sharedService.userSettings.rememberMe === utils.Constants.Yes;
        Ember.$('input#chkRemember').prop('checked', isRemember);

        if (isRemember && utils.validators.isAvailable(username)) {
            Ember.$('input#txtUsername').val(username);
        }

        if (isRemember && appConfig.loginConfig.isRememberPassword && utils.validators.isAvailable(password)) {
            Ember.$('input#txtPassword').val(password);
        }
    };

    /* *
     * Set selected language in login page language selection controls
     * @private
     */
    var setSelectedLanguageInLogin = function () {
        if (appConfig.customisation.supportedLanguages.length > 1) {
            Ember.$('input[type="radio"][name="loginLang"]').val([sharedService.userSettings.currentLanguage]);
        } else {
            Ember.$('div#divLoginLang').hide(); // Hide login option
        }
    };

    /* *
     * Bind enter key to window
     * This enables the login at pressing enter key on any control
     * @private
     */
    var bindEnterKeyLogin = function (authSuccess, authFail) {
        Ember.$(window).unbind('keypress'); // Unbind key press events if bound previously

        Ember.$(window).bind('keypress', function (e) {
            if (e.which === utils.Constants.KeyCodes.Enter) { // Key code for 'Enter' key
                goToOtpLoginOrHome(authSuccess, authFail);
            }
        });
    };

    /* *
     * Bind enter key to window
     * This enables the otp login at pressing enter key on any control
     * @private
     */
    var bindEnterKeyOtpLogin = function () {
        Ember.$(window).unbind('keypress'); // Unbind key press events if bound previously

        Ember.$(window).bind('keypress', function (e) {
            if (e.which === utils.Constants.KeyCodes.Enter) { // Key code for 'Enter' key
                var indexOtpLoginPopup = sharedService.getService('sharedUI').getService('indexOtpLoginPopup');

                if (indexOtpLoginPopup) {
                    indexOtpLoginPopup.onOtpSubmit();
                }
            }
        });
    };

    /* *
     * Go to home page
     * @private
     */
    var goToOtpLoginOrHome = function (authSuccess, authFail) {
        var username = Ember.$('input#txtUsername').val();
        var password = Ember.$('input#txtPassword').val();
        var loginMsgElem = Ember.$('div#loginMsg');
        var priceService = sharedService.getService('price');
        var currentLangObj = languageDataStore.getLanguageObj().lang;

        if (utils.validators.isAvailable(username) && utils.validators.isAvailable(password)) {
            if (appConfig.customisation.loginViewSettings.showTermsAndConditions && !Ember.$('input#chkTermsAndConditions').is(':checked')) {
                loginMsgElem.html(currentLangObj.messages.acceptTermsAndConditions).show();
            } else if (priceService.isPriceMetadataReady()) {
            authController.authenticateUser(username, password, _isInitAllowed(username, password), authSuccess, authFail);

            loginMsgElem.html(currentLangObj.messages.authenticating).show();
                loginMsgElem.addClass('info-msg-background');
            } else if (priceService.get('isDefaultMetaRequestFail')) {
                loginMsgElem.html(currentLangObj.messages.metaFail).show();
                loginMsgElem.addClass('error-msg-background');
            } else {
                that.authSuccessMetaReady = authSuccess;
                that.authFailMetaReady = authFail;

                priceService.subscribePriceMetaReady(that, controllerKey);
                loginMsgElem.html(currentLangObj.messages.authenticating).show();
                loginMsgElem.addClass('info-msg-background');
            }
        } else {
            loginMsgElem.html(currentLangObj.messages.unmPwdNotEmpty).show();
        }

        loginMsgElem.show();
    };

    this.onPriceMetaReady = function () {
        goToOtpLoginOrHome(that.authSuccessMetaReady, that.authFailMetaReady);
        sharedService.getService('price').unSubscribePriceMetaReady(controllerKey);
    };

    /* *
     * Store user data in browser local storage
     * @param username Username
     * @param password Password
     * @private
     */
    var storeUserData = function (username, password) {
        var storingPwd = '';

        // Set user selected language
        var lang = Ember.$('input[type="radio"][name="loginLang"]:checked').val();
        if (lang) {
            sharedService.userSettings.set('currentLanguage', lang);
        }

        // Set remember me option
        var isRemember = Ember.$('input#chkRemember').is(':checked');
        sharedService.userSettings.set('rememberMe', isRemember ? utils.Constants.Yes : utils.Constants.No);

        // Set username and password
        sharedService.userSettings.set('appToken', _getLoginToken(username)); // Dynamically injected to user settings
        sharedService.userSettings.set('verToken', _getLoginToken(password)); // Dynamically injected to user settings
        sharedService.userSettings.set('username', isRemember ? username : '');

        if (isRemember && appConfig.loginConfig.isRememberPassword) {
            storingPwd = utils.crypto.encryptText(password, utils.Constants.Encryption.TDesSecondaryKey, utils.Constants.Encryption.TDesSecondaryIv);
        }

        sharedService.userSettings.set('password', storingPwd);
        sharedService.userSettings.save();

        Ember.set(Ember.appGlobal.session, 'id', username);
        Ember.appGlobal.session.token = utils.crypto.generateHashedText(password);
    };

    /* *
     * Checks whether application should be initialized for the given user before login
     * @param username User provided username
     * @param password User provided password
     * @returns {boolean} True if current credentials equals to stored credentials, false otherwise
     * @private
     */
    var _isInitAllowed = function (username, password) {
        var isInitAllowed = false;

        if (appConfig.customisation.smartLoginEnabled) {
            var storedUnmToken = sharedService.userSettings.appToken;
            var storedPwdToken = sharedService.userSettings.verToken;
            var isUserAvailable = utils.validators.isAvailable(storedUnmToken) && utils.validators.isAvailable(storedPwdToken);

            isInitAllowed = isUserAvailable && storedUnmToken === _getLoginToken(username) &&
                storedPwdToken === _getLoginToken(password) && utils.browser.isNetworkConnected();
        }

        return isInitAllowed;
    };

    /* *
     * Generate login token
     * @param loginValue Username or password
     * @returns {string} Token generated based on username or password
     * @private
     */
    var _getLoginToken = function (loginValue) {
        var loginToken = '';

        if (utils.validators.isAvailable(loginValue)) {
            var charCount = loginValue.length;
            var firstChar = loginValue.charAt(0);
            var lastChar = loginValue.charAt(charCount - 1);

            loginToken = utils.crypto.hashMd5([firstChar, lastChar, charCount].join(''));
        }

        return loginToken;
    };

    /* *
     * Change display texts on language change
     * @private
     */
    var _setDisplayTexts = function () {
        var currentLangObj = languageDataStore.getLanguageObj().lang;

        // Set button texts
        var loginText = currentLangObj.labels.login;
        loginText = appConfig.customisation.isMobile ? loginText.toUpperCase() : loginText;

        Ember.$('button#btnLogin').text(loginText);

        // Set placeholder texts
        Ember.$('input#txtUsername').attr('placeholder', currentLangObj.labels.username);
        Ember.$('input#txtPassword').attr('placeholder', currentLangObj.labels.password);

        // Set labels
        Ember.$('span#spanRemember').text(currentLangObj.labels.rememberMe);
        Ember.$('span#spanNotMember').text(currentLangObj.labels.notMember);
        Ember.$('span#spanCannotLogin').text(currentLangObj.labels.cannotLogin);
        Ember.$('span#spanCallUs').text(currentLangObj.labels.callUs);
        Ember.$('span#spanVirtualKeyboard').text(currentLangObj.labels.virtualKeyboard);
        Ember.$('span#spanTermsAndConditions').text(currentLangObj.labels.termsAndConditions);

        // Set titles
        Ember.$('#loginTitle').text(currentLangObj.labels.login);

        // Set link texts
        Ember.$('a#lnkSignUp').text(currentLangObj.labels.signUp);
        Ember.$('a#lnkForgotPwd').text(currentLangObj.labels.forgotPwd);

        Ember.$('a#registerLink').text(currentLangObj.labels.register);
        Ember.$('a#lnkOnlineAcc').text(currentLangObj.labels.openOnlineAccount);
        Ember.$('a#linkRegister').text(currentLangObj.labels.createFreeAccount);
        Ember.$('span#delayedPrices').text('(' + currentLangObj.labels.delayedPrices + ')');
        Ember.$('a#lnkHelp').text(currentLangObj.labels.help);

        // Set copyright
        var copyrightText = currentLangObj.labels.copyright.replace('[CurrentYear]', new Date().getFullYear());
        Ember.$('span#spanCopyright').text(copyrightText);

        // Set appVersion
        if (appConfig.customisation.isMobile) {
            var appVersion = 'V ' + appConfig.appVersion;
            Ember.$('#appVersion').text(appVersion);
        }

        // Set best view
        Ember.$('span#spanBestView').text(currentLangObj.labels.bestViewResolution);
    };

    var _bindEvents = function () {
        var virtualKeypadEnabler = Ember.$('input[type="checkbox"][name="virtualKeyboard"]');

        Ember.$('input[type="radio"][name="loginLang"]').bind('change', function () {
            Ember.$('div#loginMsg').html('').hide();

            languageDataStore.changeLanguage(this.value);
            _setDisplayTexts();
            _toggleLanguageSelection();
        });

        virtualKeypadEnabler.bind('change', function () {
            if (Ember.$(this).is(':checked')) {
                _showVirtualKeyPad();
            } else {
                jQKeyboard.removeKeypad();
            }
        });

        Ember.$('a#linkRegister').bind('click', function () {
            window.open(appConfig.subscriptionConfig.registrationPath + sharedService.userSettings.currentLanguage.toLowerCase(), '_blank');
        });

        Ember.$('#txtPassword').bind('click', function () {
            if (appConfig.customisation.isVirtualKeyboardEnable) {
                if (virtualKeypadEnabler.is(':checked')) {
                    _showVirtualKeyPad();
                } else {
                    jQKeyboard.removeKeypad();
                }
            }
        });

        Ember.$('#primaryLang').bind('click', function () {
            Ember.$('div#loginMsg').html('').hide();

            languageDataStore.changeLanguage(appConfig.customisation.supportedLanguages[0].code);
            Ember.$('#primaryLang').hide();
            Ember.$('#secondaryLang').show();
            _setDisplayTexts();
        });

        Ember.$('#secondaryLang').bind('click', function () {
            Ember.$('div#loginMsg').html('').hide();

            var languageCode = appConfig.customisation.supportedLanguages.length > 1 ? appConfig.customisation.supportedLanguages[1].code : '';

            if (languageCode) {
                languageDataStore.changeLanguage(appConfig.customisation.supportedLanguages[1].code);
                Ember.$('#secondaryLang').hide();
                Ember.$('#primaryLang').show();
                _setDisplayTexts();
            }
        });

        Ember.$('#spanTermsAndConditions').bind('click', function () {
            window.open(appConfig.customisation.loginViewSettings.termsAndConditionURL, '_blank');
        });

        Ember.$('#lnkHelp').bind('click', function () {
            window.open(appConfig.customisation.loginViewSettings.helpURL, '_blank');
        });
    };

    var _toggleLanguageSelection = function () {
        if (appConfig.customisation.supportedLanguages.length > 1) {
            if (sharedService.userSettings.currentLanguage === appConfig.customisation.supportedLanguages[0].code) {
                Ember.$('#primaryLang').hide();
                Ember.$('#secondaryLang').show();
            } else {
                Ember.$('#secondaryLang').hide();
                Ember.$('#primaryLang').show();
            }
        } else {
            Ember.$('#primaryLang').hide();
            Ember.$('#secondaryLang').hide();
        }
    };

    var _showVirtualKeyPad = function () {
        var targtElm = Ember.$('#txtPassword');
        var input = targtElm.get(0);

        jQKeyboard.activateKeypad(targtElm);

        var element = Ember.$('#jQKeyboardContainer');
        var checkBoxElement = Ember.$('span#spanVirtualKeyboard');
        var checkBoxTopVal = checkBoxElement.get(0).offsetTop;
        var vKeyboardTopVal = checkBoxTopVal + 25;

        element.get('0').style.top = vKeyboardTopVal + 'px';
        input.focus();
    };

    /* *
     * Close modal popup if loaded on session expired
     * @private
     */
    var _closeOtpPopup = function () {
        var modal = sharedService.getService('sharedUI').getService('modalPopupId');

        if (modal) {
            modal.set('modalPopupStyle', '');
            modal.send('disableOverlay');
            modal.send('closeModalPopup');
        }
    };

    return {
        initialize: initialize,
        prepareLoginView: prepareLoginView,
        showLoginView: showLoginView,
        showHomePage: showHomePage,
        showAuthFailMessage: showAuthFailMessage,
        prepareLoginFailedView: prepareLoginFailedView,
        setLastLoggedInUser: setLastLoggedInUser,
        setSelectedLanguageInLogin: setSelectedLanguageInLogin,
        storeUserData: storeUserData,
        showOtpLogin: showOtpLogin
    };
}

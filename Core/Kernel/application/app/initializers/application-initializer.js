import Ember from 'ember';
import utils from '../utils/utils';
import moduleInitializerConfig from '../config/module-initializer-config';
import appConfig from '../config/app-config';
import sharedService from '../models/shared/shared-service';

export default (function () {
    var name = 'application-initializer';

    var initialize = function (container, application) {
        _initializeProperties();
        application.deferReadiness();

        // Calling preInitialize of all module initializers
        Ember.$.each(moduleInitializerConfig.modules, function (key, config) {
            try {
                config.preInitialize(application);
            } catch (e) {
                utils.logger.logError('Error in pre-initialize : ' + e);
            }
        });

        // Calling postInitialize of all module initializers
        Ember.$.each(moduleInitializerConfig.modules, function (key, config) {
            try {
                config.postInitialize(application);
            } catch (e) {
                utils.logger.logError('Error in post-initialize : ' + e);
            }
        });

        application.advanceReadiness();

        if (Ember.appGlobal.multiScreen.isParentWindow) {
            Ember.appGlobal.multiScreen.parentSharedService = sharedService;
        }

        if (appConfig.customisation.isTablet) {
            utils.screenHelper.setZoomingFactor(); // Zoom layout for high resolution device
        }
    };

    var _initializeProperties = function () {
        Ember.appGlobal = {};
        Ember.appGlobal.events = {};
        Ember.appGlobal.priceUser = {};
        Ember.appGlobal.orientation = {};
        Ember.appGlobal.session = {};
        Ember.appGlobal.tableCache = [];
        Ember.appGlobal.isSortingProgress = false;
        Ember.appGlobal.tabletConfig = {
            zoomingFact: 1 // For default screen (resolution: 1024x600, dppx: 1)
        };
        Ember.appGlobal.multiScreen = {
            isParentWindow: true,
            childWindowArray: [],
            isParentLogoutFired: false
        };
    };

    return {
        name: name,
        initialize: initialize
    };
})();
import Ember from 'ember';

export default Ember.Object.extend({
    marketId: '',
    lDes: '',
    def: '',
    isMktSummary: '',

    setData: function (subMktMsg) {
        var that = this;

        Ember.$.each(subMktMsg, function (key, value) {
            that.set(key, value);
        });
    }
});

import Ember from 'ember';

export default Ember.Object.extend({
    exg: '',
    id: '',
    title: '',
    url: '',
    date: '',

    setData: function (data) {
        var that = this;

        Ember.$.each(data, function (key, value) {
            that.set(key, value);
        });
    }
});
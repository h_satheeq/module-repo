import Ember from 'ember';
import searchResultItem from '../../../models/price/business-entities/search-result-item';
import priceWidgetConfig from '../../../config/price-widget-config';
import PriceConstants from '../../../models/price/price-constants';
import utils from '../../../utils/utils';
import languageDataStore from '../../../models/shared/language/language-data-store';

export default function (priceService) {
    var symbolSearchResults = Ember.A();
    var recentSearchedItems = Ember.A();
    var app = languageDataStore.getLanguageObj();

    var initialFilteredContent = Ember.A();
    var initialFilteredMap = {};
    var previousSearchNumber = {};

    var _filterSymbolFromLocalStorage = function (searchKey, exclInst, searchNumber) {
        var stocks = priceService.stockDS.getStockCollection();
        var config = priceWidgetConfig.globalSearch.groups;
        var resultItem, groupingObj;

        if (stocks) {
            var filteredContent = Ember.$.map(stocks, function (value) {
                var companyId = value.cid.toString();

                if (((utils.validators.isAvailable(value.sym) && value.sym.isExist(searchKey)) ||
                    (utils.validators.isAvailable(value.lDes) && value.lDes.isExist(searchKey)) ||
                    (utils.validators.isAvailable(value.sDes) && value.sDes.isExist(searchKey)) ||
                    (utils.validators.isAvailable(value.cid) && companyId.isExist(searchKey))) &&
                    utils.validators.isAvailable(value.inst) &&
                    (!exclInst || exclInst.length <= 0 || exclInst.indexOf(value.inst) < 0) && value.exg !== 'GLOBAL') {
                    // TODO: [Eranga G] Instrument types filtering is added temorary in order to avoid duplicating TASI
                    // symbol in search result. This statement can be removed after stopping adding index to stock collection

                    resultItem = searchResultItem.create();
                    groupingObj = config[value.ast] ? config[value.ast] : config.other;

                    resultItem.setData({
                        sym: value.sym,
                        exg: value.exg,
                        dSym: value.dSym,
                        inst: value.inst,
                        lDes: value.lDes,
                        sDes: value.sDes,
                        ast: value.ast,
                        subMkt: value.subMkt,
                        dispProp1: value.get('dispProp1'),
                        groupingObj: groupingObj
                    });

                    var exchange = priceService.exchangeDS.getExchange(value.exg);
                    resultItem.set('de', exchange.de);

                    return resultItem;
                }
            });

            if (filteredContent && filteredContent.length > 0) {
                var mergedContent = _mergeServerLocalContent(filteredContent, searchNumber);

                _groupFilteredContent(mergedContent, searchKey);
            } else {
                _setSearchResultUnavailability();
            }
        }
    };

    var _groupFilteredContent = function (filteredContent, searchKey) {
        var hasType, type, groupSetting, assetType, resultArray, priority;
        var config = priceWidgetConfig.globalSearch.groups;
        var resultLimit = priceWidgetConfig.globalSearch.maxResultsForGroup;

        Ember.$.each(filteredContent, function (key, item) {
            assetType = item.get('ast');
            groupSetting = config[assetType] ? config[assetType] : config.other;

            /* if (item.sym.isExactMatch(searchKey) || item.lDes.isExactMatch(searchKey) || item.sDes.isExactMatch(searchKey)) {
             groupSetting = config.topHits;
             } else if (item.sym.isStartedWith(searchKey) || item.lDes.isStartedWith(searchKey) || item.sDes.isStartedWith(searchKey)) {
             hasStartingChar = true;
             }*/

            // TODO: [Nipun] Top Hits implementation should be done

            if (utils.validators.isAvailable(item.sym) && item.sym.isExactMatch(searchKey)) {
                priority = 1;
            } else if (utils.validators.isAvailable(item.sDes) && item.sDes.isExactMatch(searchKey)) {
                priority = 2;
            } else if (utils.validators.isAvailable(item.lDes) && item.lDes.isExactMatch(searchKey)) {
                priority = 3;
            } else if (utils.validators.isAvailable(item.sym) && item.sym.isStartedWith(searchKey)) {
                priority = 4;
            } else if (utils.validators.isAvailable(item.sDes) && item.sDes.isStartedWith(searchKey)) {
                priority = 5;
            } else if (utils.validators.isAvailable(item.lDes) && item.lDes.isStartedWith(searchKey)) {
                priority = 6;
            } else {
                priority = 7;
            }

            type = groupSetting.type;
            hasType = symbolSearchResults.findBy('type', type);

            if (!hasType) {
                symbolSearchResults.pushObject(Ember.Object.create({
                    type: type,
                    rank: groupSetting.rank,
                    name: app.lang.labels[groupSetting.lanKey],
                    colorCss: groupSetting.colorCss,
                    contents: Ember.A()
                }));
            }

            resultArray = symbolSearchResults.findBy('type', type).get('contents');

            item.set('priority', priority);
            resultArray.pushObject(item);

            if (resultArray.length > resultLimit) {
                resultArray.pop();
            }
        });

        Ember.$.each(symbolSearchResults, function (key, item) {
            item.set('contents', item.get('contents').sortBy('priority'));
        });

        var sortedSymbolSearchResult = symbolSearchResults.sortBy('rank');

        symbolSearchResults.clear();
        symbolSearchResults.pushObjects(sortedSymbolSearchResult);
    };

    var _setSearchResultUnavailability = function () {
        if (symbolSearchResults.length === 0) {
            symbolSearchResults.pushObject(Ember.Object.create({
                isDataUnavailable: true
            }));
        }
    };

    var _mergeServerLocalContent = function (filteredContent, searchNumber) {
        symbolSearchResults.clear();

        if (previousSearchNumber !== searchNumber) {
            initialFilteredContent.clear();
            initialFilteredMap = {};
            previousSearchNumber = searchNumber;
        }

        if (initialFilteredContent.length > 0) {
            Ember.$.each(filteredContent, function (key, item) {
                var itemKey = [item.sym, item.exg].join('~');

                if (!initialFilteredMap[itemKey]) {
                    initialFilteredContent.pushObject(item);
                }
            });
        } else {
            Ember.$.each(filteredContent, function (key, item) {
                var itemKey = [item.sym, item.exg].join('~');
                initialFilteredMap[itemKey] = item;
            });

            initialFilteredContent.pushObjects(filteredContent);
        }

        return initialFilteredContent;
    };

    var getSymbolSearchResults = function () {
        return symbolSearchResults;
    };

    var filterSymbolSearchResults = function (searchKey, language, exclInst, params) {
        symbolSearchResults.clear();

        var searchNumber = Math.random();
        var userDataStore = priceService.userDS;

        _filterSymbolFromLocalStorage(searchKey, exclInst, searchNumber);

        if (userDataStore.get('isMultipleUserExchangesAvailable') || userDataStore.isNonDefaultExchangesAvailable()) {
            priceService.sendSymbolSearchRequest(searchKey, language, PriceConstants.SymbolSearchPageSize, getFilteredContentFromServer, params, searchNumber);
        }
    };

    var getFilteredContentFromServer = function (isSuccess, searchKey, content, searchNumber) {
        if (isSuccess) {
            var mergedContent = _mergeServerLocalContent(content, searchNumber);
            _groupFilteredContent(mergedContent, searchKey);
        } else {
            _setSearchResultUnavailability();
        }
    };

    var addRecentSearchedItem = function (searchedItem) {
        if (searchedItem.sym) {
            var isAvailableInRecentArray = false;

            Ember.$.each(recentSearchedItems, function (key, recentSymbol) {
                if (recentSymbol.sym === searchedItem.sym) {
                    isAvailableInRecentArray = true;
                }
            });

            if (!isAvailableInRecentArray) {
                recentSearchedItems.pushObject(searchedItem);
            }
        }
    };

    var getRecentSearchedItems = function () {
        return recentSearchedItems;
    };

    return {
        addRecentSearchedItem: addRecentSearchedItem,
        filterSymbolSearchResults: filterSymbolSearchResults,
        getSymbolSearchResults: getSymbolSearchResults,
        getRecentSearchedItems: getRecentSearchedItems
    };
}

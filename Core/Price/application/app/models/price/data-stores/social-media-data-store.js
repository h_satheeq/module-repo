import Ember from 'ember';
import Socialmedia from '../business-entities/social-media';

export default Ember.Object.extend({
    newsEvents: Ember.A(),
    pressReleases: Ember.A(),
    facebookPostsArray: Ember.A(),
    youtubePostsArray: Ember.A(),
    instagramPostsArray: Ember.A(),

    socialMediaSettings: {
        newsEventsEnable: true,
        pressReleasesEnable: true,
        facebookEnable: true,
        twitterEnable: true,
        instagramEnable: true,
        youtubeEnable: true
    },

    getNewsEvent: function (type) {
        var currentNewsEvent = Socialmedia.create({
            type: type
        });

        this.addToNewsEventsCollections(currentNewsEvent);
        return currentNewsEvent;
    },

    getPressRelease: function (type) {
        var currentPressRelease = Socialmedia.create({
            type: type
        });

        this.addToPressReleasesCollections(currentPressRelease);
        return currentPressRelease;
    },

    getYoutubePost: function () {
        var youtubePost = Socialmedia.create({});
        this.storeYoutubeArray(youtubePost);

        return youtubePost;
    },

    getFacebookPost: function () {
        var facebookPost = Socialmedia.create({});
        this.storeFacebookArray(facebookPost);

        return facebookPost;
    },

    getInstagramPost: function () {
        var instagramPost = Socialmedia.create({});
        this.storeInstagramArray(instagramPost);

        return instagramPost;
    },

    getNewsEventsArray: function () {
        return this.get('newsEvents');
    },

    getPressReleasesArray: function () {
        return this.get('pressReleases');
    },

    getYoutubePostsArray: function () {
        return this.get('youtubePostsArray');
    },

    getFacebookPostsArray: function () {
        return this.get('facebookPostsArray');
    },

    getInstagramPostsArray: function () {
        return this.get('instagramPostsArray');
    },

    addToNewsEventsCollections: function (currentNewsEvent) {
        this.get('newsEvents').pushObject(currentNewsEvent);
    },

    addToPressReleasesCollections: function (currentpressRelease) {
        this.get('pressReleases').pushObject(currentpressRelease);
    },

    storeYoutubeArray: function (youtubePost) {
        this.get('youtubePostsArray').pushObject(youtubePost);
    },

    storeFacebookArray: function (facebookPost) {
        this.get('facebookPostsArray').pushObject(facebookPost);
    },

    storeInstagramArray: function (instagramPost) {
        this.get('instagramPostsArray').pushObject(instagramPost);
    },

    getSocialMediaSettings: function () {
        return this.get('socialMediaSettings');
    },

    setSocialMediaSettings: function (settings) {
        if (settings) {
            Ember.set(this.get('socialMediaSettings'), 'facebookEnable', settings.facebook);
            Ember.set(this.get('socialMediaSettings'), 'twitterEnable', settings.twitter);
            Ember.set(this.get('socialMediaSettings'), 'instagramEnable', settings.instagram);
            Ember.set(this.get('socialMediaSettings'), 'youtubeEnable', settings.youtube);
        }
    }
});

export default{
    Indicators: {
        MovingAverage: {
            ID: 1,
            LanguageTag: 'chartIndiMV',
            Category: 1,
            ChartIQID: 'ma'
        },
        TimeSeriesForecast: {
            ID: 2,
            LanguageTag: 'chartIndiTSF',
            Category: 1,
            ChartIQID: 'Time Fcst'
        },
        WildersSmoothing: {
            ID: 3,
            LanguageTag: 'chartIndiWS',
            Category: 1,
            ChartIQID: 'wilders smoothing'
        },
        BollingerBands: {
            ID: 4,
            LanguageTag: 'chartIndiBB',
            Category: 2,
            ChartIQID: 'Bollinger Bands'
        },
        AccumulationDistribution: {
            ID: 5,
            LanguageTag: 'chartIndiAcDe',
            Category: 3,
            ChartIQID: 'W Acc Dist'
        },
        AverageTrueRange: {
            ID: 6,
            LanguageTag: 'chartIndiATR',
            Category: 3,
            ChartIQID: 'ATR'
        },
        ChandeMomentumOscillator: {
            ID: 7,
            LanguageTag: 'chartIndiCMO',
            Category: 3,
            ChartIQID: 'Chande Mtm'
        },
        CommodityChannelIndex: {
            ID: 8,
            LanguageTag: 'chartIndiCCI',
            Category: 3,
            ChartIQID: 'CCI'
        },
        DirectionalMovementPlusDI: {
            ID: 9,
            LanguageTag: 'chartIndiDMPlusDI',
            Category: 3,
            ChartIQID: '+DI'
        },
        DirectionalMovementMinusDI: {
            ID: 10,
            LanguageTag: 'chartIndiDMMinDI',
            Category: 3,
            ChartIQID: '-DI'
        },
        DirectionalMovementADX: {
            ID: 11,
            LanguageTag: 'chartIndiDMA',
            Category: 3,
            ChartIQID: 'ADX'
        },
        DirectionalMovementADXR: {
            ID: 12,
            LanguageTag: 'chartIndiDMADXR',
            Category: 3,
            ChartIQID: 'ADXR'
        },
        DirectionalMovementDX: {
            ID: 13,
            LanguageTag: 'chartIndiDMDX',
            Category: 3,
            ChartIQID: 'DX'
        },
        MACD: {
            ID: 14,
            LanguageTag: 'chartIndiMACD',
            Category: 3,
            ChartIQID: 'macd'
        },
        Momentum: {
            ID: 15,
            LanguageTag: 'chartIndiMomentum',
            Category: 3,
            ChartIQID: 'Momentum'
        },
        MoneyFlowIndex: {
            ID: 16,
            LanguageTag: 'chartIndiMFI',
            Category: 3,
            ChartIQID: 'M Flow'
        },
        RelativeStrengthIndex: {
            ID: 17,
            LanguageTag: 'chartIndiRSI',
            Category: 3,
            ChartIQID: 'rsi'
        },
        StochasticOscillator: {
            ID: 18,
            LanguageTag: 'chartIndiSO',
            Category: 3,
            ChartIQID: 'stochastics'
        },
        WilliamsPerR: {
            ID: 19,
            LanguageTag: 'chartIndiWPerR',
            Category: 3,
            ChartIQID: 'Williams %R'
        },
        ChaikinMF: {
            ID: 20,
            LanguageTag: 'chartIndChaikinMF',
            Category: 3,
            ChartIQID: 'Chaikin MF'
        },
        PSAR: {
            ID: 21,
            LanguageTag: 'chartIndPSAR',
            Category: 3,
            ChartIQID: 'PSAR'
        },
        TRIX: {
            ID: 22,
            LanguageTag: 'chartIndTRIX',
            Category: 3,
            ChartIQID: 'TRIX'
        },
        VolOsc: {
            ID: 23,
            LanguageTag: 'chartIndVolOsc',
            Category: 3,
            ChartIQID: 'Vol Osc'
        }
    },

    IndicatorCategories: {
        Averages: 1,
        Bands: 2,
        Others: 3
    }
};
export default(function () {
    // CharIQ expects Pascal case references as its data format
    var getOHLCObj = function (args) {
        var ohlcObj = null;
        if (args) {
            ohlcObj = {
                'DT': args.dt,      // Javascript Date
                'Open': args.open,
                'High': args.high,
                'Low': args.low,
                'Close': args.close,
                'Volume': args.volume,
                'Turnover': args.turnover
            };
        }
        return ohlcObj;
    };
    return {
        getOHLCObj: getOHLCObj
    };
})();


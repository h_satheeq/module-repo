import Ember from 'ember';
import ohlc from './ohlc';
import utils from '../../../utils/utils';

export default Ember.Object.extend({
    sym: '',                    // Symbol
    exg: '',                    // Exchange
    min: Number.MAX_VALUE,      // Min val of the series
    arrOHLCObservers: [],       // Array of OHLC data observers
    ohlcDataPoints: null,       // OHLC data points

    max: 0,                     // Max val of the series
    minVol: Number.MAX_VALUE,   // Min volume of the series
    chartDataLevel: -1,         // Chart data level

    maxVol: 0,                  // Max volume of the series

    stockRef: null,             // Previous close

    init: function () {
        this._super();
        this.set('arrOHLCObservers', []);
        this.set('ohlcDataPoints', []);
    },

    setData: function (ohlcMessage, isRealTime) {
        try {
            var ohlcArray = this.get('ohlcDataPoints');
            var count = ohlcArray.length;
            var currMin = this.get('min');
            var currMax = this.get('max');
            var currMinVol = this.get('minVol');
            var currMaxVol = this.get('maxVol');

            if (count === 0 || ohlcArray[count - 1].DT.getTime() < ohlcMessage.dt.getTime()) {
                // Add elements to the end of array
                var ohlcPoint = ohlc.getOHLCObj(ohlcMessage);
                ohlcArray[count] = ohlcPoint;

                if (ohlcMessage.close < currMin) {
                    this.set('min', ohlcMessage.close);
                }

                if (ohlcMessage.close > currMax) {
                    this.set('max', ohlcMessage.close);
                }

                if (ohlcMessage.volume < currMinVol) {
                    this.set('minVol', ohlcMessage.volume);
                }

                if (ohlcMessage.volume > currMaxVol) {
                    this.set('maxVol', ohlcMessage.volume);
                }

                // Notify the observers
                if (isRealTime) {
                    var arr = this.get('arrOHLCObservers');
                    for (var a = 0; a < arr.length; a++) {
                        arr[a].onDataFromRealTime(ohlcPoint, this.get('exg'), this.get('sym'));
                    }
                }
            }

            /** else if (ohlcArray[0].TimeStamp > ohlcMessage.TimeStamp) {
            // Add elements to the begin of the array
            ohlcArray.unshift(ohlcPoint);
        } else {
            // If something comes in-between, can't help, go for the costly search and insert
            alert('comes in the middle');
        }*/
        } catch (ex) {
            utils.logger.logError('Ohlc series set data is giving error ' + ex);
        }
    },

    indexOfElement: function (key) {
        var ohlcArray = this.get('ohlcDataPoints');
        var lo = 0, hi = ohlcArray.length - 1, mid, element;

        while (lo <= hi) {
            mid = ((lo + hi) >> 1);
            element = ohlcArray[mid];
            if (element.DT.getTime() < key) {
                lo = mid + 1;
            } else if (element.DT.getTime() > key) {
                hi = mid - 1;
            } else {
                return mid;
            }
        }

        // If not found, return lo and the period will be calculated from that element
        return lo;
    },

    queryPointsForRange: function (fromTime, toTime) {
        if (fromTime && fromTime === -1) {
            return [];
        }

        var startIndex = this.indexOfElement(fromTime);
        var ohlcArray = this.get('ohlcDataPoints');

        if (toTime) {
            var endIndex = this.indexOfElement(toTime);
            return ohlcArray.slice(startIndex, endIndex);
        }

        return ohlcArray.slice(startIndex);
    },

    registerForRealtimeData: function (gdm) {
        if (gdm && Ember.$.isFunction(gdm.onDataFromRealTime)) {
            var arrObservers = this.get('arrOHLCObservers');
            arrObservers[arrObservers.length] = gdm;
        }
    }
});

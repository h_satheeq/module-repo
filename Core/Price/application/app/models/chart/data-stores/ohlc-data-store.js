import Ember from 'ember';
import ChartConstants from '../chart-constants';
import ohlcSeries from '../business-entities/ohlc-series';
import utils from '../../../utils/utils';

export default Ember.Object.extend({
    intradayStore: undefined,
    historyStore: undefined,
    chartDataSubscription: undefined,

    initialize: function () {
        this.set('intradayStore', {});
        this.set('historyStore', {});
        this.set('chartDataSubscription', {});
    },

    getOHLCSeries: function (exchange, symbol, chartCategory) {
        var ohlcStore = (ChartConstants.ChartCategory.Intraday.ID === chartCategory.ID) ? this.get('intradayStore') : this.get('historyStore');
        var key = utils.keyGenerator.getKey(exchange, symbol);

        var ohlcSeriesObj = ohlcStore[key];
        if (!ohlcSeriesObj) {
            ohlcSeriesObj = ohlcSeries.create({
                sym: symbol,
                exg: exchange,
                stockRef: this.priceService.stockDS.getStock(exchange, symbol)
            });

            ohlcStore[key] = ohlcSeriesObj;
        }

        return ohlcSeriesObj;
    },

    removeOHLCSeries: function (exchange, symbol, chartCategory) {
        // Todo [Ravindu] check request count before removing data stores
        var ohlcStore = (ChartConstants.ChartCategory.Intraday.ID === chartCategory.ID) ? this.get('intradayStore') : this.get('historyStore');
        if (ohlcStore) {
            var key = utils.keyGenerator.getKey(exchange, symbol);
            if (ohlcStore[key]) {
                ohlcStore[key] = null;
            }
        }
    },

    sendIntraDayOHLCDataRequest: function (gdm, chartSymbolObj) {
        this.priceService.downloadIntradayOHLCData({
            exchange: chartSymbolObj.exg,
            symbol: chartSymbolObj.sym,
            chartDataLevel: gdm.chartDataLevel,
            begin: undefined,

            reqSuccessFn: function () {
                gdm.onDataDownloadedFromMix(gdm, chartSymbolObj);
            },

            reqFailureFn: function () {
                if (gdm.onErrorFn && Ember.$.isFunction(gdm.onErrorFn)) {
                    gdm.onErrorFn();
                }

                utils.logger.logDebug('Data Unavailable Intraday');
            }
        });
    },

    sendHistoryOHLCDataRequest: function (gdm, chartSymbolObj, begin) {
        this.priceService.downloadHistoryOHLCData({
            exchange: chartSymbolObj.exg,
            symbol: chartSymbolObj.sym,
            begin: begin,

            reqSuccessFn: function () {
                gdm.onDataDownloadedFromMix(gdm, chartSymbolObj);
            },

            reqFailureFn: function () {
                if (gdm.onErrorFn && Ember.$.isFunction(gdm.onErrorFn)) {
                    gdm.onErrorFn();
                }

                utils.logger.logDebug('Data Unavailable For History');
            }
        });
    },

    subscribeChartDataReady: function (gdm, symObj, key, wKey) {
        var chartDataSubscription = this.get('chartDataSubscription');

        if (utils.validators.isAvailable(key) && utils.validators.isAvailable(wKey)) {
            chartDataSubscription[key] = chartDataSubscription[key] || {};
            chartDataSubscription[key][wKey] = {gdm: gdm, symObj: symObj};
        }
    },

    onChartDataReady: function (exgSymKey) {
        var chartDataSubscription = this.get('chartDataSubscription');

        if (utils.validators.isAvailable(exgSymKey) && chartDataSubscription[exgSymKey]) {
            Ember.$.each(chartDataSubscription[exgSymKey], function (key, subscriber) {
                if (subscriber && subscriber.gdm && Ember.$.isFunction(subscriber.gdm.onDataDownloadedFromMix)) {
                    subscriber.gdm.onDataDownloadedFromMix(subscriber.gdm, subscriber.symObj);
                }
            });
        }
    },

    unSubscribeChartDataReady: function (key, wKey) {
        var chartDataSubscription = this.get('chartDataSubscription');

        if (utils.validators.isAvailable(key) && utils.validators.isAvailable(wKey) && chartDataSubscription[key]) {
            chartDataSubscription[key][wKey] = undefined;
        }
    }
});

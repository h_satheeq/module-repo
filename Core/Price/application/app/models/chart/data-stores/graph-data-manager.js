import Ember from 'ember';
import sharedService from '../../shared/shared-service';
import ChartConstants from '../chart-constants';
import utils from '../../../utils/utils';

export default Ember.Object.extend({
    // Time Settings
    chartCategory: null,
    chartDataLevel: -1,
    chartViewPeriod: null,

    // Symbol Data
    chartSymbolArray: [],

    // Call backs
    onData: undefined,
    onDataChunk: undefined,
    onErrorFn: undefined,

    // Flag
    isDifferentPeriod: true, // query first time data
    wKey: undefined,

    init: function () {
        this._super();
        this.priceService = sharedService.getService('price');
        this.set('chartSymbolArray', []);
    },

    addChartSymbol: function (exchange, symbol, isBaseSymbol) {
        var indexToAdd = 0;
        var symbolArray = this.get('chartSymbolArray');
        var chartSymbolObj = {
            exg: exchange,
            sym: symbol,
            chartPointArray: [],
            isBaseSymbol: isBaseSymbol
        };

        if (!isBaseSymbol) {
            indexToAdd = symbolArray.length;
        }

        symbolArray[indexToAdd] = chartSymbolObj;
        return chartSymbolObj;
    },

    removeChartSymbol: function (exg, sym) {
        var symbolArray = this.get('chartSymbolArray');

        if (exg && sym) {
            var indexToRemove = -1;

            for (var a = 0; a < symbolArray.length; a++) {
                var chartSymbolObj = symbolArray[a];

                if (chartSymbolObj.exg === exg && chartSymbolObj.sym === sym) {
                    indexToRemove = a;
                    break;

                }
            }

            if (indexToRemove > -1) {
                symbolArray.splice(indexToRemove, 1);
            }
        } else {
            symbolArray.length = 0;
        }
    },

    getDataArray: function (exg, sym) {
        var symbObj = this.getCharSymbol(exg, sym);

        if (symbObj) {
            return symbObj.chartPointArray;
        }

        return [];
    },

    getCharSymbol: function (exg, sym) {
        var symbolArray = this.get('chartSymbolArray');

        if (exg && sym) {
            for (var a = 0; a < symbolArray.length; a++) {
                var chartSymbolObj = symbolArray[a];

                if (chartSymbolObj.exg === exg && chartSymbolObj.sym === sym) {
                    return chartSymbolObj;
                }
            }
        } else if (symbolArray.length > 0) {
            return symbolArray[0];
        } else {
            return null;
        }
    },

    removeChartSymbols: function () {
        this.removeChartSymbol();
    },

    addChartDataSubscription: function (symbolObj, begin) {
        var symbolArray = this.get('chartSymbolArray');

        if (symbolArray.length === 0) {
            return;
        }

        if (symbolObj) {
            this.addSubscription(symbolObj, begin);
        } else {
            for (var a = 0; a < symbolArray.length; a++) {
                this.addSubscription(symbolArray[a], begin);
            }
        }
    },

    addSubscription: function (chartSymbolObj, begin) {
        if (this.get('chartCategory').ID === ChartConstants.ChartCategory.Intraday.ID) {
            var ohlcSeries = this.priceService.ohlcDS.getOHLCSeries(chartSymbolObj.exg, chartSymbolObj.sym, this.get('chartCategory'));

            ohlcSeries.registerForRealtimeData(this);
            // this.priceService.addSymbolRequest(chartSymbolObj.exg, chartSymbolObj.sym);
            this.priceService.addIntradayChartRequest(chartSymbolObj.exg, chartSymbolObj.sym);
        }

        this.downloadGraphData(chartSymbolObj, begin);
    },

    removeChartDataSubscription: function (symbolObj) {
        var symbolArray = this.get('chartSymbolArray');

        if (symbolArray.length === 0) {
            return;
        }

        if (symbolObj) {
            this.removeSubscription(symbolObj);
        } else {
            for (var a = 0; a < symbolArray.length; a++) {
                this.removeSubscription(symbolArray[a]);
            }
        }
    },

    removeSubscription: function (chartSymbolObj) {
        if (this.get('chartCategory').ID === ChartConstants.ChartCategory.Intraday.ID) {
            // this.priceService.removeSymbolRequest(chartSymbolObj.exg, chartSymbolObj.sym);
            this.priceService.removeIntradayChartRequest(chartSymbolObj.exg, chartSymbolObj.sym);
        }

        // remove all stores when un-subscription is invoked
        chartSymbolObj.chartPointArray.length = 0;
        this.priceService.ohlcDS.removeOHLCSeries(chartSymbolObj.exg, chartSymbolObj.sym, this.get('chartCategory'));

        this.priceService.ohlcDS.unSubscribeChartDataReady(utils.keyGenerator.getKey(chartSymbolObj.exg, chartSymbolObj.sym), this.wKey);
    },

    refineGraphData: function (params) {
        var symbolArray = this.get('chartSymbolArray');
        var chartSymbolObj;

        this.set('chartCategory', params.chartCategory);
        this.set('chartDataLevel', params.chartDataLevel);
        this.set('chartViewPeriod', params.chartViewPeriod);

        for (var a = 0; a < symbolArray.length; a++) {
            chartSymbolObj = symbolArray[a];

            if (this.get('chartCategory').ID === ChartConstants.ChartCategory.History.ID && params.chartCategory.ID === ChartConstants.ChartCategory.Intraday.ID) {
                this.priceService.addIntradayChartRequest(chartSymbolObj.exg, chartSymbolObj.sym);
            } else if (this.get('chartCategory').ID === ChartConstants.ChartCategory.Intraday.ID && params.chartCategory.ID === ChartConstants.ChartCategory.History.ID) {
                this.priceService.removeIntradayChartRequest(chartSymbolObj.exg, chartSymbolObj.sym);
            }

            this.downloadGraphData(chartSymbolObj);
            // call twice
            // this.queryData();
        }
    },

    downloadGraphData: function (chartSymbolObj, begin) {
        var ohlcSeries = this.priceService.ohlcDS.getOHLCSeries(chartSymbolObj.exg, chartSymbolObj.sym, this.chartCategory);

        if (ohlcSeries.chartDataLevel < this.chartDataLevel) {
            ohlcSeries.set('chartDataLevel', this.chartDataLevel);

            if (this.chartCategory.ID === ChartConstants.ChartCategory.Intraday.ID) {
                this.priceService.ohlcDS.sendIntraDayOHLCDataRequest(this, chartSymbolObj);
            } else {
                this.priceService.ohlcDS.sendHistoryOHLCDataRequest(this, chartSymbolObj, begin);
            }
        } else if (ohlcSeries && ohlcSeries.ohlcDataPoints.length > 0) {
            this.onDataDownloadedFromMix(this, chartSymbolObj);
        } else if (ohlcSeries) {
            var key = utils.keyGenerator.getKey(chartSymbolObj.exg, chartSymbolObj.sym);
            this.priceService.ohlcDS.subscribeChartDataReady(this, chartSymbolObj, key, this.wKey);
        }
    },

    onDataDownloadedFromMix: function (gdm, chartSymbolObj) {
        // First re-build the DataArray
        gdm.queryData(chartSymbolObj);

        if (gdm.onDataChunk && Ember.$.isFunction(gdm.onDataChunk)) {
            gdm.onDataChunk(chartSymbolObj);
        }
    },

    onDataFromRealTime: function (ohlcPoint, exg, sym) {
        if (this.onData && Ember.$.isFunction(this.onData)) {
            this.onData(ohlcPoint, exg, sym);
        }
    },

    queryData: function (chartSymbolObj) {
        try {
            if (chartSymbolObj) {
                var ohlcSeries = this.priceService.ohlcDS.getOHLCSeries(chartSymbolObj.exg, chartSymbolObj.sym, this.chartCategory);

                // Todo [Ravindu] this loads category max data. change it to load maximum history data.
                if (ChartConstants.ChartViewPeriod.All.ID === this.chartViewPeriod.ID) {
                    chartSymbolObj.chartPointArray = ohlcSeries.ohlcDataPoints.slice(); // It is important to get a copy as chart IQ use the same array as its master data array
                } else if (ohlcSeries.ohlcDataPoints.length > 0) {
                    // var date = new Date();
                    var newestPoint = ohlcSeries.ohlcDataPoints[ohlcSeries.ohlcDataPoints.length - 1];
                    var date = new Date(newestPoint.DT.getTime());

                    date.setHours(0);
                    date.setMinutes(0);
                    date.setSeconds(0);

                    switch (this.chartViewPeriod.ID) {
                        case ChartConstants.ChartViewPeriod.OneDay.ID:
                            // date.getTime() : relevant code line is set in below
                            break;

                        case ChartConstants.ChartViewPeriod.TwoDay.ID:
                            date.setDate(date.getDate() - ChartConstants.ChartDefaultDataPeriod.TwoDay);
                            break;

                        case ChartConstants.ChartViewPeriod.FiveDay.ID:
                            date.setDate(date.getDate() - ChartConstants.ChartDefaultDataPeriod.FiveDay);
                            break;

                        case ChartConstants.ChartViewPeriod.OneMonth.ID:
                            date.setMonth(date.getMonth() - 1);
                            break;

                        case ChartConstants.ChartViewPeriod.ThreeMonth.ID:
                            date.setMonth(date.getMonth() - 3);
                            break;

                        case ChartConstants.ChartViewPeriod.SixMonth.ID:
                            date.setMonth(date.getMonth() - 6);
                            break;

                        case ChartConstants.ChartViewPeriod.OneYear.ID:
                            date.setYear(date.getFullYear() - 1);
                            break;

                        case ChartConstants.ChartViewPeriod.TwoYear.ID:
                            date.setYear(date.getFullYear() - 2);
                            break;

                        case ChartConstants.ChartViewPeriod.ThreeYear.ID:
                            date.setYear(date.getFullYear() - 3);
                            break;

                        case ChartConstants.ChartViewPeriod.FiveYear.ID:
                            date.setYear(date.getFullYear() - 5);
                            break;

                        case ChartConstants.ChartViewPeriod.TenYear.ID:
                            date.setYear(date.getFullYear() - 10);
                            break;

                        case ChartConstants.ChartViewPeriod.YTD.ID:
                            date.setMonth(0);
                            date.setDate(1);
                            break;

                        default:
                            utils.logger.logWarning('Unknown chart view period...!');
                    }

                    chartSymbolObj.chartPointArray = ohlcSeries.queryPointsForRange(date.getTime());
                }
            }
        } catch (e) {
            utils.logger.logError('Error in querying ohlc data : ' + e);
        }
    }
})
;

import Ember from 'ember';
import sharedService from '../models/shared/shared-service';

export default Ember.Component.extend({
    layoutName: 'components/sub-markets-dropdown',

    exchange: {},
    exg: '',
    subMarkets: [],
    currentMarketId: '',

    isSubMarketsAvailable: function () {
        return this.get('subMarkets.length') >= 1;
    }.property('subMarkets.length'),

    isDisableSubMarkets: function () {
        return this.get('subMarkets.length') <= 1;
    }.property('subMarkets.length'),

    didInsertElement: function () {
        sharedService.getService('price').subscribePriceExchangeSummaryMetaReady(this, ['sub-market', this.get('key')].join('-'));
        this.setSubMarkets();
    },

    willDestroyElement: function () {
        this.set('subMarkets', []);
        this.set('exchange', {});
        this.set('currentMarketId', '');

        this._super();
    },

    onPriceExchangeSummaryMetaReady: function () {
        this.setSubMarkets();
    },

    setSubMarkets: function () {
        Ember.run.once(this, this._loadSubMarkets);
    }.observes('exchange.subMarketArray.@each'),

    _loadSubMarkets: function () {
        var currentSubMkt = this.get('currentMarketId');
        var prevExg = this.get('exg');
        var currentExg = this.get('exchange.exg');

        if (!this._isDestroying()) {
            this.set('subMarkets', Ember.A());

            if (!currentSubMkt || currentSubMkt === -1 || (prevExg !== '' && prevExg !== undefined && prevExg !== currentExg)) {
                this.set('currentMarketId', sharedService.getService('price').exchangeDS.getDefaultSubMarket(currentExg));
                this.set('exg', this.get('exchange.exg'));
            } else if (prevExg === '') {
                this.set('exg', this.get('exchange.exg'));
            }

            if (this.get('exchange.subMarketArray')) {
                this.subMarkets.pushObjects(this.get('exchange.subMarketArray'));
            }
        }
    },

    _isDestroying: function () {
        return this.get('isDestroyed') || this.get('isDestroying'); // TODO: [Bashitha] Move this implementation to a common place
    },

    actions: {
        setSubMarket: function (option) {
            this.sendAction('onSubMarketChanged', option.marketId);
        }
    }
});

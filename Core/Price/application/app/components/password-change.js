import ModalPopup from './modal-popup';
import appConfig from '../config/app-config';
import utils from '../utils/utils';
import sharedService from '../models/shared/shared-service';
import languageDataStore from '../models/shared/language/language-data-store';

export default ModalPopup.extend({
    layoutName: 'components/password-change',
    app: languageDataStore.getLanguageObj(),

    service: {},
    isEnabledTransactionPw: appConfig.customisation.isEnabledTransactionPw,
    isEnabledPwRules: appConfig.customisation.isEnabledPwRules,
    isCancelEnabled: true,
    forceChgPwAuthTyp: 2,

    ChangePwStatus: {
        Success: 1,
        Failed: 0,
        FailedLocked: 3
    },

    L2AuthType: {
        NoPassword: 1,
        PasswordOnce: 2,
        Password: 3
    },

    /* *
     * Temporarily overwriting this method to register  this component to sharedService.
     */
    expose: function () {
        var parentController = this.get('targetObject');
        var exposedName = this.get('id');
        parentController.set(exposedName, this);

        if (this.get('isRegistered')) {
            sharedService.getService('sharedUI').registerService('changePasswordPopup', this);
        }
    }.on('didInsertElement'),

    _onChangePasswordResponse: function (chgPwdSts, chgPwMsg) {
        if (chgPwdSts === this.ChangePwStatus.Success) {
            var app = this.get('app');

            this.send('closeModalPopup');
            this.set('message', '');

            utils.messageService.showMessage(app.lang.messages.passwordReset, utils.Constants.MessageTypes.Info, false, app.lang.labels.changePassword);

            if (this.get('isMobile')) {
                sharedService.getService('priceUI').closeChildView('components/password-change');
            }
        } else {
            this.set('message', chgPwMsg);
        }
    },

    isPopupEnabled: function () {
        if (!appConfig.customisation.isMobile) {
            this.set('popupCss', this.get('isEnabledTransactionPw') ? 'change-tx-password' : 'change-login-password');
        }

        return this.get('isEnabled') || appConfig.customisation.isMobile;
    }.property('isEnabled'),

    _changePassword: function () {
        var that = this;
        this.set('message', '');
        var app = this.get('app');
        var currentTxPwd, newTxPwd, confirmTxPwd;

        var currentPwd = this.get('currentPassword');
        var newPwd = this.get('newPassword');
        var confirmPwd = this.get('confirmPassword');

        var isBlank = !(utils.validators.isAvailable(currentPwd) && utils.validators.isAvailable(newPwd) && utils.validators.isAvailable(confirmPwd));
        var isMismatch = newPwd !== confirmPwd;

        if (this.get('isEnabledTransactionPw') && !isBlank && !isMismatch) {
            currentTxPwd = this.get('currentTxPassword');
            newTxPwd = this.get('newTxPassword');
            confirmTxPwd = this.get('confirmTxPassword');

            isBlank = isBlank || !(utils.validators.isAvailable(currentTxPwd) && utils.validators.isAvailable(newTxPwd) && utils.validators.isAvailable(confirmTxPwd));
            isMismatch = isMismatch || (newTxPwd !== confirmTxPwd);
        }

        if (isBlank) {
            this.set('message', app.lang.messages.mandatoryFields);
            return;
        } else if (isMismatch) {
            this.set('message', app.lang.messages.passwordMismatch);
            return;
        }

        var encryptedOldPw = utils.crypto.generateHashedText(currentPwd);
        var encryptedNewPw = utils.crypto.generateHashedText(newPwd);

        var reqObj = {oldPwd: encryptedOldPw, newPwd: encryptedNewPw};

        if (this.get('isEnabledTransactionPw')) {
            reqObj.oldTxPwd = utils.crypto.generateHashedText(currentTxPwd);
            reqObj.newTxPwd = utils.crypto.generateHashedText(newTxPwd);
        }

        if (!this.isEnabledPwRules || ((!this.isEnabledTransactionPw || this.isTxPwdRulesMatch) && this.isPwdRulesMatch)) {
            this.get('service').changePassword(reqObj, function (authSts, authMsg) {
                that._onChangePasswordResponse(authSts, authMsg);
            });

            this.set('message', app.lang.messages.pleaseWait);
        } else {
            this.set('message', app.lang.messages.didNotMeetPwRules);
        }

        this.set('currentPassword', '');
        this.set('newPassword', '');
        this.set('confirmPassword', '');

        if (this.get('isEnabledTransactionPw')) {
            this.set('currentTxPassword', '');
            this.set('newTxPassword', '');
            this.set('confirmTxPassword', '');
        }
    },

    isMobile: function () {
        return appConfig.customisation.isMobile;
    }.property(),

    actions: {
        onSave: function () {
            this._changePassword();
        },

        onCancel: function () {
            this.set('currentPassword', '');
            this.set('newPassword', '');
            this.set('confirmPassword', '');

            if (this.get('isEnabledTransactionPw')) {
                this.set('currentTxPassword', '');
                this.set('newTxPassword', '');
                this.set('confirmTxPassword', '');
            }

            this.send('closeModalPopup');
        },

        showModalPopup: function (isCancelEnabled) {
            var service, userName;

            if (appConfig.customisation.isTradingEnabled) {
                service = sharedService.getService('trade');
                userName = service.userDS.lgnAls;
            } else {
                service = sharedService.getService('price');
                userName = service.userDS.username;
            }

            this.set('isCancelEnabled', isCancelEnabled);
            this.set('service', service);

            if (!this.get('isEnabled')) {
                this.set('isEnabled', true);
                this.set('message', '');
                this.set('username', sharedService.userSettings.username ? sharedService.userSettings.username : userName);

                // TODO [Arosha] Enable below after testing transaction password logic
                // var l2AuthTyp = this.get('service').userDS.l2AuthTyp;
                //
                // if (l2AuthTyp === this.L2AuthType.PasswordOnce || l2AuthTyp === this.L2AuthType.Password) {
                //    this.set('isEnabledTransactionPw', true);
                // }
            }
        }
    }
});
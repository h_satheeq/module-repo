import sharedService from '../../../../models/shared/shared-service';
import ExchangeAnnouncement from './exchange-announcement';
import appConfig from '../../../../config/app-config';

export default ExchangeAnnouncement.extend({
    wkey: 'right-Ann-Popup',    // TODO [Eranga G] remove this when 'wkey' is implemented to right panel
    exg: sharedService.userSettings.currentExchange,   // TODO [Eranga G] remove this when 'exg' para is implemented to right panel
    showAnnTabs: true,
    isMobile: false,

    onLoadWidget: function () {
        this._super();
        this.set('isMobile', appConfig.customisation.isMobile);
    },

    onLanguageChanged: function (lang) {
        this._super(lang);
    }
});
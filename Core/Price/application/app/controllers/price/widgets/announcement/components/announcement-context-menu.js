import Ember from 'ember';

export default Ember.Component.extend({
    layoutName: 'price/widgets/announcement/components/announcement-context-menu',

    click: function (event) {
        var target = Ember.$(event.target);

        if (!target.hasClass('btn')) {
            event.stopPropagation();
        }
    },

    actions: {
        save: function (item) {
            this.sendAction('saveAction', item);
        },

        cancel: function (item) {
            this.sendAction('cancelAction', item);
        }
    }
});

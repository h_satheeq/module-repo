import Ember from 'ember';
import BaseArrayController from '../../base-array-controller';
import sharedService from '../../../models/shared/shared-service';

export default BaseArrayController.extend({
    comps: [],

    widgets: [
        {
            id: 5,
            wn: 'price.widgets.gms-summary-table',
            args: {assetType: 8, selectedLink: 1}
        },
        {
            id: 6,
            wn: 'price.widgets.gms-summary-table',
            args: {assetType: 7, selectedLink: 1}
        },
        {
            id: 7,
            wn: 'price.widgets.gms-summary-table',
            args: {assetType: 4, selectedLink: 1}
        },
        {
            id: 8,
            wn: 'price.widgets.indices-world-map'
        },
        {
            id: 9,
            wn: 'chart.regular-chart',
            args: {selectedLink: 1}
        }
    ],

    onAfterRender: function () {
        var widgets = this.get('widgets');
        var widgetContainer = this.widgetContainer;
        var widgetComp = [];

        Ember.$.each(widgets, function (key, value) {
            widgetComp[widgetComp.length] = sharedService.getService('sharedUI').getService('mainPanel').renderWidget(widgetContainer.menuContent, {
                id: widgetContainer.tabContent.id,
                bid: widgetContainer.tabContent.bid,
                wn: widgetContainer.tabContent.wn,
                outlet: 'price.widgets.gms-container'
            }, value, value.args);
        });

        this.set('comps', widgetComp);
    },

    onClearData: function () {
        var comps = this.get('comps');

        Ember.$.each(comps, function (key, value) {
            value.closeWidget();
        });
    }
});
import BaseModuleInitializer from '../../../models/shared/initializers/base-module-initializer';
import PriceUIService from '../../../controllers/price/price-ui-service';
import appEvents from '../../../app-events';
import languageDataStore from '../../../models/shared/language/language-data-store';
import sharedService from '../../../models/shared/shared-service';

export default BaseModuleInitializer.extend({
    preInitialize: function () {
        var service = this.createService();
        service.initialize(languageDataStore.getLanguageObj());

        sharedService.registerService(service.subscriptionKey, service);
        appEvents.subscribeLayoutReady(service.subscriptionKey, service);
    },

    createService: function () {
        return PriceUIService.create();
    }
});

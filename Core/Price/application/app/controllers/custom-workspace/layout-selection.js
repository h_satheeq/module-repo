import Ember from 'ember';
import BaseController from '../base-controller';
import controllerFactory from '../controller-factory';

export default BaseController.extend({
    router: undefined,
    menuContent: undefined,
    tabContent: undefined,
    widgetContainer: undefined,

    layoutContent: [
        {r: [1], c: [1], w: '200px', h: '164px'},
        {r: [1], c: [1, 1], w: '100px', h: '164px'},
        {r: [1, 1], c: [1], w: '200px', h: '80px'},
        {r: [1, 1], c: [1, 1], w: '100px', h: '80px'},
        {r: [1, 1], c: [1, 1, 1], w: '67px', h: '80px'}
    ],

    // TODO [Atheesan]: Combine layoutContent and complexLayoutContent
    complexLayoutContent1: {coordinates: [{x: 0, y: 0}, {x: 6, y: 0}, {x: 0, y: 24}], yCoordinates: [0, 0, 1], widths: [6, 6, 12], widgetCount: 3, rowCounts: [2, 2, 2]},
    complexLayoutContent2: {coordinates: [{x: 0, y: 0}, {x: 6, y: 0}, {x: 0, y: 24}], yCoordinates: [0, 0, 1], widths: [6, 6, 6], widgetCount: 3, rowCounts: [2, 1, 2]},
    complexLayoutContent3: {coordinates: [{x: 0, y: 0}, {x: 0, y: 24}, {x: 6, y: 24}], yCoordinates: [0, 1, 1], widths: [12, 6, 6], widgetCount: 3, rowCounts: [2, 2, 2]},
    complexLayoutContent4: {coordinates: [{x: 0, y: 0}, {x: 4, y: 0}, {x: 8, y: 0}, {x: 0, y: 24}], yCoordinates: [0, 0, 0, 1], widths: [4, 4, 4, 12], widgetCount: 4, rowCounts: [2, 2, 2, 2]},
    complexLayoutContent5: {coordinates: [{x: 0, y: 0}, {x: 4, y: 0}, {x: 8, y: 0}, {x: 4, y: 24}], yCoordinates: [0, 0, 0, 1], widths: [4, 4, 4, 8], widgetCount: 4, rowCounts: [1, 2, 2, 2]},

    onLoadLayoutContainer: function (router, menuContent, tabContent, widgetContainer) {
        this.router = router;
        this.menuContent = menuContent;
        this.tabContent = tabContent;
        this.widgetContainer = widgetContainer;

        var customLayoutController = this.router.controllerFor('custom-workspace.custom-layout');
        customLayoutController.onLoadLayout(router, menuContent, tabContent, widgetContainer);
    },

    _selectLayout: function (rCount, cCount, complexLayoutContent) {
        var rowCount = rCount ? rCount : this.tabContent.row;
        var colCount = cCount ? cCount : this.tabContent.col;
        var outletName = 'custom-workspace.custom-layout';

        this.tabContent.outlet = outletName;
        this.tabContent.layoutSelection = false;

        var layoutController = this.router.controllerFor(outletName);
        layoutController.onLoadLayout(this.router, this.menuContent, this.tabContent, this.widgetContainer);

        layoutController.set('rowCount', rowCount); // To ensure row count & column count are available before custom-layout-view init
        layoutController.set('colCount', colCount);
        layoutController.set('complexLayoutContent', complexLayoutContent);

        this.router.render(outletName, {
            into: 'main-panel-container-controller',
            outlet: this.tabContent.title,
            controller: layoutController
        });

        this._loadLayout(layoutController, this.widgetContainer);

        layoutController.saveTabArgs({
            outlet: outletName,
            row: rowCount,
            col: colCount,
            cache: false,
            title: 'custom',
            custom: true,
            layoutSelection: false
        });

        Ember.run.later(function () {
            layoutController.saveWorkspace();
        }, 200);
    },

    _loadLayout: function (customLayout, widgetContainer) {
        var route = customLayout.container.lookup('route:application');

        var customLayoutView = controllerFactory.createController(widgetContainer.container, 'view:custom-workspace/custom-layout-view');
        customLayoutView.set('targetController', customLayout);

        route.render('custom-workspace/custom-layout-view', {
            into: 'custom-workspace/custom-layout',
            outlet: 'custom-outlet',
            controller: customLayoutView
        });
    },

    actions: {
        selectLayout: function (layout) {
            this._selectLayout(layout.r.length, layout.c.length);
        },

        selectComplexLayout: function (complexLayoutContent) {
            this._selectLayout(1, 1, complexLayoutContent);
        }
    }
});

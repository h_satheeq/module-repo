import Constants from '../../utils/constants';

export default (function () {
    /* *
     * Generate custom workspace template
     * @param index Index of the widget container in the workspace
     * @param height Height of the widget container
     * @param width Width of the widget container
     * @param coordinates X and Y coordinates
     * @param isActive True if active widget container, false if extra widget container
     * @returns {String} Generate template
     */
    var generateWorkspaceWidgetContainer = function (index, height, width, coordinates, isActive) {
        var layoutHtml = [];

        layoutHtml[layoutHtml.length] = '<div style={{controller.targetController.outletMap.o';
        layoutHtml[layoutHtml.length] = index;
        layoutHtml[layoutHtml.length] = '}} id="gs-w-div-';
        layoutHtml[layoutHtml.length] = index;
        layoutHtml[layoutHtml.length] = '" grid-name="gs-w-c" grid-index="';
        layoutHtml[layoutHtml.length] = index;
        layoutHtml[layoutHtml.length] = '" class="grid-stack-item" data-gs-width="';
        layoutHtml[layoutHtml.length] = width;
        layoutHtml[layoutHtml.length] = '" data-gs-height="';
        layoutHtml[layoutHtml.length] = height;
        layoutHtml[layoutHtml.length] = '" data-gs-y="';
        layoutHtml[layoutHtml.length] = coordinates.y;
        layoutHtml[layoutHtml.length] = '" data-gs-x="';
        layoutHtml[layoutHtml.length] = coordinates.x;
        layoutHtml[layoutHtml.length] = '" active-c="';
        layoutHtml[layoutHtml.length] = isActive ? Constants.Yes : Constants.No;
        layoutHtml[layoutHtml.length] = '"><div class="grid-stack-item-content">{{outlet "w';
        layoutHtml[layoutHtml.length] = index;
        layoutHtml[layoutHtml.length] = '"}}</div></div>';

        return layoutHtml.join(Constants.StringConst.Empty);
    };

    return {
        generateWorkspaceWidgetContainer: generateWorkspaceWidgetContainer
    };
})();

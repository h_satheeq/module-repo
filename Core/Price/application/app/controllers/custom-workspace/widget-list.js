import BasePopup from '../../components/base-popup';

export default BasePopup.extend({
    layoutName: 'controllers/custom-workspace/widget-list'
});

import WidgetContainerController from '../widget-container-controller';

export default WidgetContainerController.extend({
    outletMap: {
        o1: 'height: calc(100% - 25px)'
    }
});

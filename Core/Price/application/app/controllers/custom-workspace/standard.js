import WidgetContainerController from '../widget-container-controller';

export default WidgetContainerController.extend({
    outletMap: {
        o1: 'height: calc(100% - 240px)',
        o2: 'height: 208px',
        o3: 'height: 208px'
    }
});

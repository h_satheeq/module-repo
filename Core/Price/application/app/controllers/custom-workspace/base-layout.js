import Ember from 'ember';
import MainPanelContainerController from '../main-panel-container-controller';
import sharedService from '../../models/shared/shared-service';
import ControllerFactory from '../controller-factory';

export default MainPanelContainerController.extend({
    router: undefined,
    menuContent: undefined,
    tabContent: undefined,
    widgetContainer: undefined,

    onLoadLayout: function (router, menuContent, tabContent, widgetContainer) {
        this.router = router;
        this.menuContent = menuContent;
        this.tabContent = tabContent;
        this.widgetContainer = widgetContainer;

        this.addContainerToMap(this, menuContent, tabContent);
        this.initializeCustomWorkspaceWidget();
    },

    initializeCustomWorkspaceWidget: function () {
        this.innerWidgetMap[this.menuContent.id] = this.innerWidgetMap[this.menuContent.id] || {};
        this.innerWidgetMap[this.menuContent.id][this.tabContent.id] = this.tabContent.w || [];
    },

    onAfterRender: function () {
        // Base override
    },

    onLoadLayoutContainer: function (router, menuContent, tabContent, widgetContainer) {
        var that = this;

        this.router = router;
        this.menuContent = menuContent;
        this.tabContent = tabContent;
        this.widgetContainer = widgetContainer;

        this.initializeCustomWorkspaceWidget();
        this._loadLayout(this, widgetContainer);

        if (tabContent.w) {
            // Wait till DOM is ready with loaded template
            Ember.run.later(function () {
                Ember.$.each(tabContent.w, function (key, widget) {
                    if (widget) {
                        if (that.outletMap) {
                            Ember.set(that.outletMap, 'w' + widget.id, '');
                        }

                        if (that.widgetMap) {
                            Ember.set(that.widgetMap, 'w' + widget.id, true);
                        }
                    }
                });
            }, 200);
        }
    },

    getVacantCoordinates: function () {
        var that = this;
        var maxY = 0;
        var workspace = this.getCustomWorkSpace(sharedService.userState.customWS);

        Ember.$.each(workspace, function (key, containerObj) {
            var verticalGap = containerObj.y + containerObj.h;

            if (containerObj.act === that.utils.Constants.Yes && verticalGap > maxY) {
                maxY = verticalGap;
            }
        });

        return {
            x: 0,
            y: maxY // Maximum Y coordinate is the next vacant Y coordinate as coordinate is zero (0) based
        };
    },

    _loadLayout: function (customLayout, widgetContainer) {
        var route = customLayout.container.lookup('route:application');

        var customLayoutView = ControllerFactory.createController(widgetContainer.container, 'view:custom-workspace/custom-layout-view');
        customLayoutView.set('targetController', customLayout);

        route.render('custom-workspace/custom-layout-view', {
            into: 'custom-workspace/custom-layout',
            outlet: 'custom-outlet',
            controller: customLayoutView
        });
    },

    actions: {
        closeInnerWidget: function (widgetId) {
            this.closeInnerWidget(widgetId);
        }
    }
});

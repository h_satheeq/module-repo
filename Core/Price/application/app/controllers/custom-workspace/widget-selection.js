/* global Draggabilly */

import Ember from 'ember';
import BaseController from '../base-controller';
import sharedService from '../../models/shared/shared-service';
import priceWidgetConfig from '../../config/price-widget-config';
import appConfig from '../../config/app-config';

// TODO: [Atheesan] Refactor after requirements finalized
export default BaseController.extend({
    widgetList: priceWidgetConfig.WidgetList,
    isTradingEnabled: false,

    onLoadWidget: function () {
        this.set('isTradingEnabled', appConfig.customisation.isTradingEnabled);

        this.set('quoteContent', this.widgetList.quote);
        this.set('companyContent', this.widgetList.company);
        this.set('marketContent', this.widgetList.market);
        this.set('tradeContent', this.widgetList.trade);
        this.set('transferContent', this.widgetList.transfer);
        this.set('mutualFundContent', this.widgetList.mutualFund);
        this.set('optionChainContent', this.widgetList.optionChain);
        this.set('userProfileContent', this.widgetList.userProfile);

        this._setLanguageFields(this.app.lang.labels);
    },

    _setLanguageFields: function (langObj) {
        var widgetList = this.widgetList;

        Ember.$.each(widgetList, function (type, widgetByTypeArray) {
            if (widgetByTypeArray.length > 0) {
                widgetByTypeArray.forEach(function (widget) {
                    var description = langObj[widget.desc] ? langObj[widget.desc] : widget.des;
                    Ember.set(widget, 'des', description);
                });
            }
        });
    },

    filterWidget: function () {
        var quoteWidgetList = this._getFilteredWidget(this.widgetList.quote);
        var companyWidgetList = this._getFilteredWidget(this.widgetList.company);
        var marketWidgetList = this._getFilteredWidget(this.widgetList.market);
        var tradeWidgetList = this._getFilteredWidget(this.widgetList.trade);
        var transferWidgetList = this._getFilteredWidget(this.widgetList.transfer);
        var mutualFundWidgetList = this._getFilteredWidget(this.widgetList.mutualFund);
        var optionChainWidgetList = this._getFilteredWidget(this.widgetList.optionChain);
        var userProfileWidgetList = this._getFilteredWidget(this.widgetList.userProfile);

        this.set('quoteContent', quoteWidgetList);
        this.set('companyContent', companyWidgetList);
        this.set('marketContent', marketWidgetList);
        this.set('tradeContent', tradeWidgetList);
        this.set('transferContent', transferWidgetList);
        this.set('mutualFundContent', mutualFundWidgetList);
        this.set('optionChainContent', optionChainWidgetList);
        this.set('userProfileContent', userProfileWidgetList);
    },

    onSearchKeyChanged: function () {
        Ember.run.debounce(this, this.filterWidget, 500);
    }.observes('widgetSearchKey'),

    _getFilteredWidget: function (widgetList) {
        var that = this;
        var filteredList = [];

        widgetList.forEach(function (widget) {
            if (widget.des.toLowerCase().includes(that.get('widgetSearchKey').toLowerCase())) {
                filteredList[filteredList.length] = widget;
            }
        });

        return filteredList;
    },

    _closeWidgetList: function () {
        this.set('widgetSearchKey', '');

        var modal = sharedService.getService('sharedUI').getService('modalPopupId');

        if (modal) {
            modal.send('closeModalPopup');
        }
    },

    showWidgetList: function () {
        var viewName = 'custom-workspace/widget-list';
        var modal = sharedService.getService('sharedUI').getService('modalPopupId');
        var popup = this.container.lookupFactory('controller:custom-workspace/widget-list').create();

        popup.showPopup(this, viewName, modal);
        this._dragResizePopup();
    },

    _dragResizePopup: function () {
        Ember.run.schedule('afterRender', this, function () {
            var elem = document.querySelector('.widget-list-popup');

            new Draggabilly(elem, { // eslint-disable-line
                handle: '#custom-widget-list',
                x: 100,
                y: 200
            });

            Ember.$('.widget-list-popup').resizable();
        });
    },

    actions: {
        showWidgetList: function () {
            this.showWidgetList();
        },

        selectWidget: function (widget) {
            var widgetId = this.get('widgetId') ? this.get('widgetId') : this.bid;

            if (this.widgetContainer) {
                this.widgetContainer.setWidgetToContainer(widgetId, widget);
            }

            this._closeWidgetList();
        },

        closeWidgetList: function () {
            this._closeWidgetList();
        }
    }
});

Ember.Handlebars.helper('isWidgetAvailable', function (widgetArray) {
    return widgetArray.length > 1;
});
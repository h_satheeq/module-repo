import Ember from 'ember';
import BaseLayout from './base-layout';
import sharedService from '../../models/shared/shared-service';
import controllerFactory from '../controller-factory';
import priceWidgetConfig from '../../config/price-widget-config';

export default BaseLayout.extend({
    widgetList: priceWidgetConfig.WidgetList,
    innerWidgetMap: {},
    outletMap: {},
    widgetMap: {},
    widgetControllerMap: {}, // To keep the rendered widget controllers' details to call resize

    /* *
     * A callback function of the grid container resize
     */
    onResizeContainer: function (event, ui) {
        var activeContainerId = ui.element[0].attributes['grid-index'].value;
        var innerWidgetObj = this.getInnerWidgetMap(activeContainerId);
        var activeWidget = this._getDefaultInnerWidget(this.menuContent, this.tabContent, innerWidgetObj); // private fn of parent

        if (activeContainerId && activeWidget && this.widgetControllerMap[activeContainerId] && this.widgetControllerMap[activeContainerId][activeWidget.id]) {
            this.widgetControllerMap[activeContainerId][activeWidget.id].resizeWidget();
        }
    },

    /* *
     * Create the map with inner widget related details
     */
    getInnerWidgetMap: function (widgetId) {
        var innerWidgetMap = this.innerWidgetMap;

        innerWidgetMap[this.menuContent.id] = innerWidgetMap[this.menuContent.id] || {};
        innerWidgetMap[this.menuContent.id][this.tabContent.id] = innerWidgetMap[this.menuContent.id][this.tabContent.id] || {};
        innerWidgetMap[this.menuContent.id][this.tabContent.id][widgetId] = innerWidgetMap[this.menuContent.id][this.tabContent.id][widgetId] || {};

        return innerWidgetMap[this.menuContent.id][this.tabContent.id][widgetId];
    },

    /* *
     * Prepare the object and function to render the inner widget inside the grid container
     */
    setWidgetToContainer: function (widgetId, widget) {
        var isDefaultTemplate = widget.code === 'custom-workspace.widget-selection';
        var widgetDef = this.addInnerWidget(widgetId, widget, isDefaultTemplate); // widgetDef[customWidget, activeWidget]

        this.set('isSelectionWidget', isDefaultTemplate);
        this.saveLastActiveInnerWidget(widgetDef.customWidget, widgetDef.innerWidget);
        this.saveWorkspace();
        this.prepareWidget(this.menuContent, this.tabContent, widgetDef.customWidget, undefined, undefined, widgetId);
    },

    /* *
     * Render the selected widget inside the grid container
     */
    prepareWidget: function (menuContent, tabContent, widgetDef, args, innerWidgetContent, wId) {
        var widgetId = wId ? wId : widgetDef.id;

        if (innerWidgetContent && innerWidgetContent.isSelect) {
            var selectionWidgetController = controllerFactory.createController(this.container, 'controller:custom-workspace.widget-selection');

            selectionWidgetController.initializeWidget(innerWidgetContent, {widgetArgs: {widgetContainer: this, widgetId: widgetId}});
            selectionWidgetController.showWidgetList();
        } else {
            var innerWidgetParams = this._prepareInnerWidget(menuContent, tabContent, widgetDef, innerWidgetContent, args); // Parent's private fn
            var activeInnerWidget = innerWidgetParams ? innerWidgetParams.activeWidget : widgetDef.iw[widgetDef.iw.length - 1];
            var innerWidgetId = activeInnerWidget.id;

            var widgetArgs = {
                isWidgetHeaderAvailable: true,
                isWidgetCloseAvailable: true,
                widgetId: widgetId,
                innerWidgetId: innerWidgetId,
                hideSymbol: true,
                isSearchAvailable: activeInnerWidget && activeInnerWidget.wn !== 'price.widgets.announcement.exchange-announcement',
                searchID: {popup: 'customWorkspacePopup-' + widgetId, search: 'customWorkspaceSearch-' + widgetId},
                cursorMoveCss: 'cursor-move',
                wkey: 'customWorkspace-' + widgetId,
                isSelectionWidget: widgetDef.iw.length === 1 ? widgetDef.iw[widgetDef.iw.length - 1].wn === 'custom-workspace.widget-selection' : false
            };

            widgetArgs = Ember.$.extend({}, widgetArgs, activeInnerWidget.args);

            var widgetController = this.renderWidget(menuContent, tabContent, widgetDef, widgetArgs, innerWidgetParams);

            this.widgetControllerMap[widgetId] = this.widgetControllerMap[widgetId] || {};
            this.widgetControllerMap[widgetId][innerWidgetId] = widgetController;

            // TODO [Atheesan] Enable this is if the container should resize according to widget size
            /* if (widgetController && widgetController.dimensions) {
             var widgetContainer = Ember.$('div#gs-w-div-' + widgetId);
             var grid = Ember.$('.grid-stack').data('gridstack');
             grid.resize(widgetContainer, undefined, widgetController.dimensions.h);
             } */

            Ember.set(this.outletMap, 'o' + widgetId, '');
            Ember.set(this.widgetMap, 'w' + widgetId, true);

            this.controllers[widgetId] = this.controllers[widgetId] ? this.controllers[widgetId] : {};
            this.controllers[widgetId][innerWidgetId] = widgetController;
        }
    },

    /* *
     * Create the inner widget object and add it to the inner widget map
     */
    addInnerWidget: function (widgetId, widget, isSelectionWidget) {
        var customWidget = this.getInnerWidgetMap(widgetId);
        var innerWidget = {};

        customWidget.iw = customWidget.iw ? customWidget.iw : [];
        customWidget.id = parseInt(widgetId, 10);

        var innerWidgetArrayLength = customWidget.iw.length;

        innerWidget.wn = widget.code;
        innerWidget.def = true;
        innerWidget.isShow = true;
        innerWidget.id = innerWidgetArrayLength + 1;
        innerWidget.desc = widget.desc ? widget.desc : 'selectWidget';
        innerWidget.icon = widget.icon;
        innerWidget.isSelect = isSelectionWidget;
        innerWidget.args = widget.args;

        var lastInnerWidget = customWidget.iw[innerWidgetArrayLength - 1];

        if (innerWidgetArrayLength > 0) {
            customWidget.iw.forEach(function (innerCustomWidget) {
                innerCustomWidget.def = false;
            });
        }

        if (lastInnerWidget && lastInnerWidget.isSelect) {
            customWidget.iw[innerWidgetArrayLength - 1] = innerWidget;
            innerWidget.id = innerWidgetArrayLength;

            var selectionInnerWidget = {};
            selectionInnerWidget.wn = 'custom-workspace.widget-selection';
            selectionInnerWidget.def = true;
            selectionInnerWidget.isShow = true;
            selectionInnerWidget.id = innerWidgetArrayLength + 1;
            selectionInnerWidget.desc = 'selectWidget';
            selectionInnerWidget.isSelect = true;

            customWidget.iw[innerWidgetArrayLength] = selectionInnerWidget;
        } else {
            customWidget.iw[innerWidgetArrayLength] = innerWidget;
        }

        this.innerWidgetMap[this.menuContent.id][this.tabContent.id][widgetId] = customWidget;
        this.saveTabWidget(customWidget);

        return {
            customWidget: customWidget,
            innerWidget: innerWidget
        };
    },

    /* *
     * Close inner widget loaded in a container
     */
    closeInnerWidget: function (widgetId, innerWidgetId) {
        if (this.widgetMap['w' + widgetId]) {
            var widgetController = this.controllers[widgetId][innerWidgetId];
            var innerWidgetObj = this.getInnerWidgetMap(widgetId);
            var innerWidget = innerWidgetObj.iw;

            if (widgetController) {
                widgetController.closeWidget();
            }

            if (innerWidget.length > 1) {
                if (!innerWidget[innerWidgetId - 1].isSelect) {
                    innerWidget[innerWidgetId - 1] = undefined;

                    var newInnerWidget = [];

                    innerWidget.forEach(function (widgetObj) {
                        if (widgetObj !== undefined) {
                            widgetObj.id = newInnerWidget.length + 1;
                            newInnerWidget[newInnerWidget.length] = widgetObj;
                        }
                    });

                    innerWidgetObj.iw = newInnerWidget;
                    this.innerWidgetMap[this.menuContent.id][this.tabContent.id][widgetId] = innerWidgetObj;

                    this.widgetContainer.saveTabWidget(innerWidgetObj);

                    if (newInnerWidget[newInnerWidget.length - 1].isSelect) {
                        this.saveLastActiveInnerWidget(innerWidgetObj, newInnerWidget[0]);
                    }
                } else {
                    this.saveLastActiveInnerWidget(innerWidgetObj, innerWidget[innerWidget.length - 2]);
                }

                this.prepareWidget(this.menuContent, this.tabContent, innerWidgetObj, undefined, undefined, widgetId);
            } else {
                this.innerWidgetMap[this.menuContent.id][this.tabContent.id][widgetId] = undefined;
                this.closeWidgetContainer(widgetId);
            }
        } else {
            this.innerWidgetMap[this.menuContent.id][this.tabContent.id][widgetId] = undefined;
            this.closeWidgetContainer(widgetId);
        }

        this.saveWorkspace();
    },

    /* *
     * Remove the grid widget container from the view of the custom workspace if all the widgets are closed
     */
    closeWidgetContainer: function (widgetId) {
        this.widgetContainer.removeTabWidget(widgetId);

        this.router.render('shared.empty', {
            into: 'custom-workspace.custom-layout',
            outlet: 'w' + widgetId
        });

        var grid, coordinates;
        var containerToRemove = Ember.$('div#gs-w-div-' + widgetId);
        containerToRemove.attr('active-c', this.utils.Constants.No);

        coordinates = this.getVacantCoordinates();

        grid = Ember.$('.grid-stack').data('gridstack');
        grid.min_height(containerToRemove, 1);
        grid.update(containerToRemove, coordinates.x, coordinates.y, 12, 1);

        Ember.set(this.outletMap, 'o' + widgetId, 'display: none');
        Ember.set(this.widgetMap, 'w' + widgetId, false);
    },

    /* *
     * Load the active custom workspace related option popup
     */
    customizeCustomWorkspace: function (isMenu) {
        var viewName = 'components/bootstrap-dropdown-select-list';
        var instanceName = 'component:bootstrap-dropdown-select';

        var menuOptions = [{code: 'rename', des: this.app.lang.labels.renameWorkspace}, {code: 'delete', des: this.app.lang.labels.deleteWorkspace}];
        var tabOptions = [{code: 'rename', des: this.app.lang.labels.renameActiveWorkspace}, {code: 'delete', des: this.app.lang.labels.deleteActiveWorkspace}];
        var options = isMenu ? menuOptions : tabOptions;

        if (!this.tabContent.layoutSelection) {
            options[options.length] = {code: 'add', des: this.app.lang.labels.addContainer};
        }

        Ember.set(this, 'options', options);

        var bootstrapDropdown = controllerFactory.createController(this.container, instanceName);
        bootstrapDropdown.set('options', this.options);

        this.set('labelKey', 'des');
        this.set('isMenu', isMenu);

        var modal = sharedService.getService('sharedUI').getService('modalPopupId');
        bootstrapDropdown.showPopup(this, viewName, modal, undefined, true);
    },

    /* *
     * Add a new widget container with the widget selection page in the active custom workspace
     */
    addWidgetContainer: function () {
        var index = 0;
        var containerToAdd;
        var activeContainer = true;

        while (activeContainer) {
            index++;
            containerToAdd = Ember.$('div#gs-w-div-' + index);
            activeContainer = containerToAdd && containerToAdd.attr('active-c') === this.utils.Constants.Yes;
        }

        this.makeContainerActive(index, containerToAdd);
        this.saveWorkspace();

        // Scroll to the added widget's position; make it visible int viewport
        var customWSContent = Ember.$('div#custom-workspace-layout');
        customWSContent[0].scrollTop = customWSContent[0].scrollHeight - customWSContent[0].clientHeight;
    },

    /* *
     * Function to delete the Active Custom Workspace
     */
    deleteCustomWorkspace: function () {
        var menuContentArray = this.widgetContainer.get('appLayout').layout.mainPanel.content;
        var userStateObj = sharedService.userState;
        var that = this;

        Ember.$.each(menuContentArray, function (key, menuContentObj) {
            if (that.menuContent.id === menuContentObj.id) {
                if (that.get('isMenu')) {
                    menuContentArray.removeObject(menuContentObj);

                    userStateObj.defaultWS[that.get('containerKey')][that.menuContent.id] = undefined;

                    if (userStateObj.customWS && userStateObj.customWS[that.menuContent.id]) {
                        userStateObj.customWS[that.menuContent.id] = undefined;
                    }

                    if (userStateObj.lastInnerWidget && userStateObj.lastInnerWidget[that.menuContent.id]) {
                        userStateObj.lastInnerWidget[that.menuContent.id] = undefined;
                    }

                    if (userStateObj.lastTab && userStateObj.lastTab[that.menuContent.id]) {
                        userStateObj.lastTab[that.menuContent.id] = undefined;
                    }

                    userStateObj.save();

                    sharedService.getService('sharedUI').getService('mainPanel').onRenderMenuItems(menuContentArray[0], menuContentArray[0].tab[0].id);
                } else {
                    Ember.$.each(menuContentObj.tab, function (index, tabContentObj) {
                        if (tabContentObj.id === that.tabContent.id) {
                            menuContentObj.tab.removeObject(tabContentObj);
                            return false;
                        }
                    });

                    userStateObj.defaultWS[that.get('containerKey')][that.menuContent.id].tab[that.tabContent.id] = undefined;

                    if (userStateObj.customWS && userStateObj.customWS[that.menuContent.id] && userStateObj.customWS[that.menuContent.id][that.tabContent.id]) {
                        userStateObj.customWS[that.menuContent.id][that.tabContent.id] = undefined;
                    }

                    if (userStateObj.lastInnerWidget && userStateObj.lastInnerWidget[that.menuContent.id] && userStateObj.lastInnerWidget[that.menuContent.id][that.tabContent.id]) {
                        userStateObj.lastInnerWidget[that.menuContent.id][that.tabContent.id] = undefined;
                    }

                    userStateObj.save();

                    sharedService.getService('sharedUI').getService('mainPanel').onRenderTabItems(menuContentObj.tab[0]);

                    if (menuContentObj.tab.length === 1) {
                        sharedService.getService('sharedUI').getService('menuPanel').setSubMenu(menuContentObj, false);
                    }
                }

                return false;
            }
        });

        sharedService.getService('sharedUI').getService('menuPanel').getSubMenuWidths();
        userStateObj.save();
    },

    /* *
     * Make the newly added grid container active
     */
    makeContainerActive: function (index, containerToActive) {
        var coordinates = this.getVacantCoordinates();

        var grid = Ember.$('.grid-stack').data('gridstack');
        grid.min_height(containerToActive, 5); // Number of rows for minimum height
        grid.resizable(containerToActive, true);
        grid.update(containerToActive, coordinates.x, coordinates.y, 6, 25);

        containerToActive.attr('active-c', this.utils.Constants.Yes);

        this.setWidgetToContainer(index, this.widgetList.selection);

        Ember.set(this.outletMap, 'o' + index, '');
        Ember.set(this.widgetMap, 'w' + index, false);
    },

    /* *
     * Close drop-downs loaded in modal popup
     */
    _closePopup: function () {
        var modal = sharedService.getService('sharedUI').getService('modalPopupId');

        if (modal) {
            modal.send('closeModalPopup');
        }
    },

    actions: {
        /* *
         * To select active custom workspace related functions like add, rename & delete
         */
        select: function (item) {
            this._closePopup();

            if (item.code === 'add') {
                this.addWidgetContainer();
            } else if (item.code === 'rename') {
                var content = this.get('isMenu') ? this.menuContent : this.tabContent;
                this.editWorkspaceName(content, this.get('isMenu'));
            } else if (item.code === 'delete') {
                var that = this;

                this.utils.messageService.showMessage(this.app.lang.messages.deleteConfirmation, this.utils.Constants.MessageTypes.Question, false, this.app.lang.labels.deleteWorkspace, [{
                    type: 'yes', btnAction: function () {
                        that.deleteCustomWorkspace();
                    }
                }, {type: 'no'}], null);
            }
        }
    }
});

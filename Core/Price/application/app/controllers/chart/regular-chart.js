import Ember from 'ember';
import appConfig from '../../config/app-config';
import ChartConstants from '../../models/chart/chart-constants';
import ChartBase from './chart-base';

/* global $$$ */

export default ChartBase.extend({
    dimensions: {
        w: 4,
        h: 24
    },

    // Full Screen parameters
    minHeightForVolumeStudy: 200,
    resizeHandler: null,
    activeTab: null,
    // Flag
    isShowTitle: true,
    curruntActiveTabClass: 'active',
    unactiveTabClass: '',
    showVolumeChart: true,
    isDisableChartControls: true,

    isTablet: appConfig.customisation.isTablet,

    arrDisplayPeriods: function () {
        var arrTabs = Ember.A();
        var langObj = this.get('app').lang.labels;

        Ember.$.each(ChartConstants.DetaiQuoteChartPeriodTab, function (key, value) {
            if (value) {
                var regChartPeriod = ChartConstants.ChartViewPeriod[key];
                Ember.set(regChartPeriod, 'chartperiodTitle', langObj[regChartPeriod.title]);
                arrTabs.pushObject(regChartPeriod);
            }
        });

        Ember.set(arrTabs[0], 'css', 'active');

        return arrTabs;
    }.property(),

    chartContainer: function () {
        return ['full-width dq_chart', this.get('wkey')].join('');
    }.property(),

    // Base overrides
    onAfterRender: function () {
        // Top panel chart and quote tab chart both are shared same dq_chart dom container since both use same class to fetch container.
        // It causes second loaded chart is painted on first loaded chart.
        var containerClass = ['.', 'dq_chart', this.get('wkey')].join('');

        this.initChart($$$(containerClass));

        // Listener on the window object that will be called when window resize
        this.resizeHandler = Ember.$.proxy(this.get('_onChartResize'), this);
        Ember.$(window).on('resize', this.resizeHandler);
    },

    // Base overrides
    onUnloadWidget: function () {
        this._super();

        Ember.$(window).off('resize', this.resizeHandler);
        this.setActive(this.activeTab, this.unactiveTabClass);
    },

    // Chart Base overrides
    onGetChartInitialBulkRecords: function (params, cb) {
        var chartDataArray = this.get('graphDataManager').getDataArray();

        cb({
            quotes: chartDataArray,
            moreAvailable: true
        });
    },

    // Chart Base overrides
    onFinishedLoadingNewChart: function () {
        var that = this;

        return function (error) {
            if (!error) {
                var baseChart = that.get('baseChart');

                if (baseChart.masterData && baseChart.masterData.length > 0) {
                    baseChart.setRange({
                        dtLeft: baseChart.masterData[0].DT, // Set this to the date to appear on the left edge of chart
                        dtRight: baseChart.masterData[baseChart.masterData.length - 1].DT,  // Set this to the date to appear on the right edge of chart
                        padding: 30 // Set this to the number of pixels of padding to leave between the chart and y-axis
                    });
                }

                // Todo: [Ravindu] check below method is needed
                baseChart.draw();
            }

        };
    },

    // Chart Base overrides
    onLoadLayout: function () {
        var defaultLayout = {
            'chartType': this.get('chartStyle').ChartIQChartType
        };

        this.get('baseChart').importLayout(defaultLayout);
    },

    // Chart Base overrides
    onCheckVolumeChartEnabler: function () {
        var containerHeight = Ember.$('#dq_chart').height();

        return containerHeight > this.minHeightForVolumeStudy && this.get('showVolumeChart');
    },

    _onChartResize: function () {
        if (this.onCheckVolumeChartEnabler()) {
            this.set('volumeViewEnabled', false);
        } else {
            this.set('volumeViewEnabled', true);
        }

        this.onDisplayVolume();
    },

    setActive: function (currentTab, active) {
        var tabArray = this.get('arrDisplayPeriods');

        Ember.$.each(tabArray, function (key, tabObj) {

            if (tabObj.ID === currentTab.ID) {
                Ember.set(tabObj, 'css', active);
            } else {
                Ember.set(tabObj, 'css', '');
            }
        });
    },

    _chartTypeSelected: function (tabItem) {
        var gdm = this.get('graphDataManager');
        var baseChart = this.get('baseChart');

        this.set('chartViewPeriod', tabItem);
        this.set('activeTab', tabItem);
        this.set('chartCategory', (tabItem.IsHistory) ? ChartConstants.ChartCategory.History : ChartConstants.ChartCategory.Intraday);

        if (baseChart) {
            baseChart.setPeriodicityV2(ChartConstants.ChartViewInterval[tabItem.DefaultInterval].Value, this.get('chartCategory').RowTickFormat);
        }

        if (gdm) {
            gdm.refineGraphData({
                chartCategory: this.get('chartCategory'),
                chartDataLevel: this.get('chartViewPeriod').ChartDataLevel,
                chartViewPeriod: this.get('chartViewPeriod')
            });
        }

        this.setActive(tabItem, this.curruntActiveTabClass);
    },

    actions: {
        chartTypeSelected: function (tabItem) {
            this._chartTypeSelected(tabItem);
        },

        setLink: function (option) {
            this.setWidgetLink(option);
        }
    }
});
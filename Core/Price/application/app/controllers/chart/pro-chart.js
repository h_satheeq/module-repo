import Ember from 'ember';
import ChartConstants from '../../models/chart/chart-constants';
import ChartStudies from '../../models/chart/chart-studies';
import GlobalSearch from '../../components/global-search';
import userSettings from '../../config/user-settings';
import languageDataStore from '../../models/shared/language/language-data-store';
import ChartBase from './chart-base';
import ControllerFactory from '../controller-factory';
import responsiveHandler from '../../helpers/responsive-handler';
import appConfig from '../../config/app-config';

// import techStudyRecord from '../../models/chart/business-entities/tech-study-record';

/* global STXChart */
/* global STXSocial */
/* global STX */
/* global $$$ */
/* global $$ */

export default ChartBase.extend({
    dimensions: {
        w: 6,
        h: 36
    },

    app: languageDataStore.getLanguageObj(),
    // Default settings
    chartInterval: ChartConstants.ChartViewInterval.EveryMinutes,
    selectedGridType: ChartConstants.ProChartGridStyle.Both,

    // Data label on Chart
    pointOpen: '',
    pointHigh: '',
    pointLow: '',
    pointClose: '',
    pointDate: '',
    pointVolume: '',

    // Drop down lists
    definedPeriodsMap: {},
    definedIntervalsMap: {},
    definedIndicatorsMap: {},

    // Symbol label on Chart
    symbolLabel: '',

    // Volume label position from top
    volumeToTop: 50,

    // Tool bar settings
    crossHairEnabled: true,
    selectedLineStudy: '',

    // Observable for language changes
    currentLang: languageDataStore.getChangeLanguageObj(),  // Todo: [Ravindu] use onLangChange()

    // activeTechStudies: [],   // Todo [Ravindu] related to displaying indicator values on chart
    // Full Screen parameters
    previousParent: null,
    previousWatchListStyleAttribute: null,
    previousFullScreenContainerStyleAttribute: null,
    isFullScreenMode: false,

    // Tab Dropdown parameters
    isPeriodsDropdown: false,
    isChartRespDisabled: true,

    isTablet: appConfig.customisation.isTablet,
    showWidgetButtons: true,

    searchSymbolKey: function () {
        return 'searchSym' + this.get('wkey');
    }.property(),

    compareSymbolKey: function () {
        return 'compareSym' + this.get('wkey');
    }.property(),

    chartCategories: function () {
        var arrCategories = Ember.A();
        var that = this;
        var category = null;
        // Display name is changed when changing a language
        var selectedCategory = that.get('chartCategory');

        Ember.set(selectedCategory, 'DisplayName', that.get('app').lang.labels[selectedCategory.LanguageTag]);

        Ember.$.each(ChartConstants.ChartCategory, function (key) {
            category = ChartConstants.ChartCategory[key];
            Ember.set(category, 'DisplayName', that.get('app').lang.labels[category.LanguageTag]);
            arrCategories.pushObject(category);
        });

        return arrCategories;
    }.property('currentLang.lang'),

    setTabletConfigs: function () {
        this.set('showWidgetButtons', !appConfig.customisation.isTablet);
    }.on('init'),

    initializeResponsive: function () {
        this.set('responsive', responsiveHandler.create({controller: this, widgetId: 'proChartContainer-' + this.get('wkey'), callback: this.onResponsive}));

        this.responsive.addList('chart-right', [
            {id: 'period-dropdown', width: 5},
            {id: 'chartCategories-dropdown', width: 5},
            {id: 'intervals-dropdown', width: 5},
            {id: 'chartStyles-dropdown', width: 5},
            {id: 'chart-search', width: 5},
            {id: 'chart-compare', width: 5},
            {id: 'chart-compare', width: 5}
        ]);

        this.responsive.initialize();
    },

    onResponsive: function (responsiveArgs) {
        var controller = responsiveArgs.controller;
        var chartContainer = Ember.$('#proChartContainer-' + controller.get('wkey'));

        // Periods Dropdown
        var periodsDropdown = chartContainer.find('#periodsDropdown-' + controller.get('wkey'));
        var periodsDropdownResLocation = chartContainer.find('#periodsDropdownResLocation-' + controller.get('wkey'));
        var periodsDropdownContainer = chartContainer.find('#periodsDropdownContainer-' + controller.get('wkey'));

        // Chart Compare
        var chartCompare = chartContainer.find('#chartCompare-' + controller.get('wkey'));
        var chartCompareResLocation = chartContainer.find('#chartCompareResLocation-' + controller.get('wkey'));
        var chartCompareContainer = chartContainer.find('#chartCompareContainer-' + controller.get('wkey'));

        if (responsiveArgs.responsiveLevel >= 1) {
            controller.set('isPeriodsDropdown', true);
        } else {
            controller.set('isPeriodsDropdown', false);
        }

        if (responsiveArgs.responsiveLevel >= 7) {
            controller.set('isPeriodsDropdown', false);
            controller.set('isChartRespDisabled', false);
            periodsDropdown.appendTo(periodsDropdownResLocation);
            chartCompare.appendTo(chartCompareResLocation);
        } else {
            controller.set('isChartRespDisabled', true);
            periodsDropdown.appendTo(periodsDropdownContainer);
            chartCompare.appendTo(chartCompareContainer);
        }
    },

    periods: function () {
        var arrPeriods = Ember.A();
        var vid = this.get('app').lang.labels;

        Ember.$.each(ChartConstants.ProChartPeriodTab, function (key, value) {
            if (value) {
                var chartperiod = ChartConstants.ChartViewPeriod[key];
                Ember.set(chartperiod, 'chartperiodTitle', vid[chartperiod.title]);
                arrPeriods.pushObject(chartperiod);
            }
        });

        return arrPeriods;
    }.property(),

    intervals: function () {
        var selectedArr = this.get('definedIntervalsMap')[this.get('chartCategory').ID];
        var that = this;
        var chartInterval = null;
        // Display name is changed when changing a language
        var selectedInterval = that.get('chartInterval');

        Ember.set(selectedInterval, 'DisplayName', that.get('app').lang.labels[selectedInterval.LanguageTag]);

        if (selectedArr) {
            selectedArr.forEach(function (item) {
                chartInterval = item;
                Ember.set(chartInterval, 'DisplayName', that.get('app').lang.labels[chartInterval.LanguageTag]);
            });
        }

        return selectedArr;
    }.property('chartCategory', 'currentLang.lang'),

    chartStyles: function () {
        var arrStyles = Ember.A();
        var that = this;
        var chartStyle = null;
        // Display name is changed when changing a language
        var selectedStyle = that.get('chartStyle');

        Ember.set(selectedStyle, 'DisplayName', that.get('app').lang.labels[selectedStyle.LanguageTag]);

        Ember.$.each(ChartConstants.ChartStyle, function (key) {
            chartStyle = ChartConstants.ChartStyle[key];
            Ember.set(chartStyle, 'DisplayName', that.get('app').lang.labels[chartStyle.LanguageTag]);
            arrStyles.pushObject(chartStyle);
        });

        return arrStyles;
    }.property('currentLang.lang'),

    allIndicators: function () {
        var that = this;
        var arrIndicators = Ember.A();

        if (appConfig.chartConfig && appConfig.chartConfig.chartIndicators && appConfig.chartConfig.chartIndicators.length > 0) {
            Ember.$.each(appConfig.chartConfig.chartIndicators, function (key, value) {
                var indicatorObj = ChartStudies.Indicators[value];
                Ember.set(indicatorObj, 'DisplayName', that.get('app').lang.labels[indicatorObj.LanguageTag]);
                arrIndicators.pushObject(indicatorObj);
            });
        }

        return arrIndicators;
    }.property('currentLang.lang'),

    averageIndicators: function () {
        var avgArr = this.get('definedIndicatorsMap')[ChartStudies.IndicatorCategories.Averages];
        var that = this;
        var avgIndi = null;

        if (avgArr) {
            avgArr.forEach(function (item) {
                avgIndi = item;
                Ember.set(avgIndi, 'DisplayName', that.get('app').lang.labels[avgIndi.LanguageTag]);
            });
        }

        return avgArr;
    }.property('currentLang.lang'),

    bandIndicators: function () {
        var bandArr = this.get('definedIndicatorsMap')[ChartStudies.IndicatorCategories.Bands];
        var that = this;
        var bandIndi = null;

        if (bandArr) {
            bandArr.forEach(function (item) {
                bandIndi = item;
                Ember.set(bandIndi, 'DisplayName', that.get('app').lang.labels[bandIndi.LanguageTag]);
            });
        }

        return bandArr;
    }.property('currentLang.lang'),

    otherIndicators: function () {
        var otherArr = this.get('definedIndicatorsMap')[ChartStudies.IndicatorCategories.Others];
        var that = this;
        var otherIndi = null;

        if (otherArr) {
            otherArr.forEach(function (item) {
                otherIndi = item;
                Ember.set(otherIndi, 'DisplayName', that.get('app').lang.labels[otherIndi.LanguageTag]);
            });
        }

        return otherArr;
    }.property('currentLang.lang'),

    gridTypes: function () {
        var arrGridTypes = Ember.A();
        var that = this;
        var grid = null;

        Ember.$.each(ChartConstants.ProChartGridStyle, function (key) {
            grid = ChartConstants.ProChartGridStyle[key];
            Ember.set(grid, 'DisplayName', that.get('app').lang.labels[grid.LanguageTag]);
            arrGridTypes.pushObject(grid);
        });

        return arrGridTypes;
    }.property('currentLang.lang'),

    // This is showing latest LTP price. but this will mismatch with current drawn data.
    symbolLTP: function () {
        if (this.get('symbolObj')) {
            return this.utils.formatters.formatNumber(this.get('symbolObj').ltp, this.get('symbolObj').deci);
        }

        return userSettings.displayFormat.noValue;
    }.property('symbolObj.ltp'),

    // This is showing latest volume. but this will mismatch with current drawn data.
    lastVolume: function () {
        if (this.get('symbolObj')) {
            return this.get('symbolObj').vol;
        }

        return userSettings.displayFormat.noValue;
    }.property('symbolObj.vol'),

    volumeVisibility: Ember.computed('volumeViewEnabled', function () {
        return this.get('volumeViewEnabled') === false ? 'hidden' : 'visible';
    }),

    // Base overrides
    onLoadWidget: function () {
        this._super();

        // Initial load from saved profile
        var previousLayout = this.get('chartLayout');
        var gDM = this.get('graphDataManager');

        if (previousLayout) {
            // Loading last saved data
            this.set('chartCategory', previousLayout.chartCategory);
            this.set('chartViewPeriod', previousLayout.chartPeriod);
            gDM.set('chartCategory', previousLayout.chartCategory);
            gDM.set('chartDataLevel', previousLayout.chartPeriod.ChartDataLevel);
            gDM.set('chartViewPeriod', previousLayout.chartPeriod);
        }

        // Initialize interval and indicator items for drop-downs
        this._loadChartIntervals();
        this._loadIndicators();
    },

    onLanguageChanged: function () {
        STX.DFNLang.langObj = languageDataStore.getLanguageObj();
        STX.DialogManager.dismissDialog();

        var baseChart = this.get('baseChart');
        var chartLayout = this.get('chartLayout');

        if (baseChart && chartLayout) {
            this._loadPanels(baseChart, chartLayout);
        }
    },

    // Base overrides
    onUnloadWidget: function () {
        this._super();
        this.set('chartDrawings', null);
    },

    // Chart Base overrides
    onDataFromMix: function (chartSymbolObj) {
        var baseChart = this.get('baseChart');

        if (baseChart) {
            if (!chartSymbolObj.isBaseSymbol) {
                // This is a compare symbol
                STX.Comparison.addCompareSymbol(
                    baseChart,
                    chartSymbolObj.sym,

                    function () {
                        // that.showLoader(false);
                    },

                    chartSymbolObj.sym);
            } else {
                baseChart.newChart(this.get('sym'), null, null, this.onFinishedLoadingNewChart(baseChart.chart.symbol));
            }
        } else {
            this.set('reloadChartData', chartSymbolObj);
        }
    },

    chartContainer: function () {
        return ['chartContainer pro-chart-container', this.get('wkey')].join('');
    }.property(),

    // Base overrides
    onAfterRender: function () {
        // All pro chart instances shared the same dom container since both use same class to fetch container.
        // It causes second loaded chart is painted on first loaded chart.
        var that = this;

        Ember.run.later(function () {
            var chartContainer = $$$(['.', 'pro-chart-container', that.get('wkey')].join(''));

            if (chartContainer) {
                that.initChart(chartContainer);
            } else {
                that.utils.logger.logError('Pro-chart container not found.');
            }
        }, 1000);
    },

    // Chart Base overrides
    initChart: function (chartContainer) {
        this._super(chartContainer);
        STX.DFNLang.langObj = languageDataStore.getLanguageObj();

        var that = this;
        var baseChart = that.get('baseChart');

        // Select tab for initial view period
        that._setSelectedTab();

        STXChart.prototype.prepend('headsUpHR', function () {
            that._onPrependHeadsUpHR();
        });

        STXChart.prototype.prepend('handleMouseOut', function () {
            that.prependMouseOut();
        });

        baseChart.editCallback = function (stx, sd) {
            var potentialTitles = document.querySelectorAll('#studies li');
            var name = null;

            if (sd && sd.libraryEntry.languageTag) {
                name = that.get('app').lang.labels[sd.libraryEntry.languageTag];
            } else {
                name = that.get('app').lang.labels.chartLabels[sd.libraryEntry.display];
            }

            $$('studyDialog').querySelectorAll('.title')[0].innerHTML = name;

            for (var i = 0; i < potentialTitles.length; i++) {
                name = sd.name;
                var libraryEntry = sd.libraryEntry;

                if (libraryEntry && libraryEntry.name) {
                    name = that.get('app').lang.labels[libraryEntry.languageTag];
                }

                $$('studyDialog').querySelectorAll('.title')[0].innerHTML = name;
            }

            STX.DialogManager.displayDialog('studyDialog');
            return $$('studyDialog');
        };

        if (baseChart.chart && baseChart.chart.context) {
            STXSocial.brandMyChart(baseChart, 'assets/images/chartIQ/chart_watermark.png', [10, -30]);
        }

        STX.Comparison.initialize(baseChart, false);

        baseChart.changeCallback = function (chartObj, change) {
            if (change === 'layout') {
                that._saveLayout();
            } else if (change === 'vector') {
                that._saveDrawings();
            }
        };
    },

    prependMouseOut: function () {
        this.set('pointOpen', '');
        this.set('pointHigh', '');
        this.set('pointLow', '');
        this.set('pointClose', '');
        this.set('pointDate', '');
        this.set('pointVolume', '');
    },

    // Chart Base overrides
    onFinishedLoadingNewChart: function (currentSymbol) {
        var that = this;

        return function (error) {
            // that.showLoader(false);
            if (!error) {
                var baseChart = that.get('baseChart');
                var newSymbol = that.get('sym');

                that._restoreDrawings();
                baseChart.setPeriodicityV2(that.get('chartInterval').Value, that.get('chartCategory').RowTickFormat);

                if (currentSymbol !== newSymbol) {
                    STX.Comparison.reset(baseChart);
                }

                if (baseChart.masterData && baseChart.masterData.length > 0) {
                    baseChart.setRange({
                        dtLeft: baseChart.masterData[0].DT,        // Set this to the date to appear on the left edge of chart
                        dtRight: baseChart.masterData[baseChart.masterData.length - 1].DT,        // Set this to the date to appear on the right edge of chart
                        padding: 30         // Set this to the number of pixels of padding to leave between the chart and y-axis
                    });
                }

                // Todo: [Ravindu] check below method is need
                baseChart.draw();
            }
        };
    },

    // Chart Base overrides
    onLoadLayout: function () {
        var baseChart = this.get('baseChart');
        var chartLayout = this.get('chartLayout');
        var that = this;

        // Todo [Ravindu] don't save entire param object save ids only
        if (baseChart) {
            if (chartLayout) {
                baseChart.importLayout(chartLayout.baseLayout);

                // Display name is changed when changing a language
                var style = chartLayout.chartStyle;

                Ember.set(style, 'DisplayName', that.get('app').lang.labels[style.LanguageTag]);
                // Set chart style
                that.set('chartStyle', style);

                // Set crosshair
                if (chartLayout.crossHairVisibility) {
                    baseChart.showCrosshairs();
                    Ember.$('#crosshair-btn').toggleClass('active');
                } else {
                    baseChart.hideCrosshairs();
                }

                // Display name is changed when changing a language
                var interval = chartLayout.chartInterval;

                Ember.set(interval, 'DisplayName', that.get('app').lang.labels[interval.LanguageTag]);
                // Set chart interval
                that.set('chartInterval', interval);

                // Set volume chart
                if (chartLayout.baseLayout.studies) {
                    that.set('volumeViewEnabled', chartLayout.baseLayout.studies.vchart ? true : false);
                }

                // Set grid style
                baseChart.chart.xAxis.displayGridLines = chartLayout.verticalGrid;
                baseChart.chart.yAxis.displayGridLines = chartLayout.horizontalGrid;

                // Maximize or minimize window
                if (chartLayout.isMaximize) {
                    that.toggleFullScreen();
                }
            } else {
                var defaultLayout = {
                    'chartType': that.get('chartStyle').ChartIQChartType,
                    'crosshair': that.get('crossHairEnabled')
                };

                baseChart.importLayout(defaultLayout);

                if (that.get('crossHairEnabled')) {
                    Ember.$('#crosshair-btn').toggleClass('active');
                }
            }
        }
    },

    _loadPanels: function (baseChart, chartLayout) {
        var that = this;

        Ember.$.each(chartLayout.baseLayout.panels, function (key, value) {
            var displayName = value.display;

            if(displayName) {
                var indicatorID = value.display;

                if (displayName.indexOf(' (') > -1) {
                    indicatorID = displayName.substring(0, displayName.indexOf(' ('));
                }

                var res = displayName.replace(indicatorID, that.get('app').lang.labels.chartLabels[value.chartLangKey]);
                var category = chartLayout.baseLayout.panels[key];

                Ember.set(category, 'display', res);
            }
        });
    },

    // Chart Base overrides
    onGetRangeOfChartRecords: function (params, cb) {
        var chartDataArray = this.get('graphDataManager').getDataArray(this.get('exg'), params.symbol);
        var newQuotes = [];
        var index = 0;
        var quote = chartDataArray[index];

        while (quote && (quote.DT.getTime() <= params.endDate.getTime())) {
            if (quote.DT.getTime() >= params.startDate.getTime()) {
                newQuotes.push(quote);
            }

            quote = chartDataArray[++index];
        }

        cb({
            quotes: newQuotes,
            moreAvailable: true
        });
    },

    // Chart Base overrides
    onGetChartInitialBulkRecords: function (params, cb) {
        var gdMgr = this.get('graphDataManager');
        var dataArray = gdMgr.getDataArray(this.get('exg'), params.symbol);
        var symbolList = gdMgr.get('chartSymbolArray');

        cb({
            quotes: dataArray,
            moreAvailable: true
        });

        // Todo [Ravindu] come up better solution after code refactoring
        for (var i = 0; i < symbolList.length; i++) {
            var symbolObj = symbolList[i];

            if (!symbolObj.isBaseSymbol) {
                // Compare symbol
                STX.Comparison.quoteFeedFetch(this.get('baseChart'), symbolObj.sym, this._onProcessCompareData(symbolObj.sym));
            }
        }
    },

    // Chart Base overrides
    onAfterFinishedDrawingChart: function () {
        this._setVolumeLabelPosition();
    },

    onDisplayVolume: function () {
        this._super();

        Ember.$('#volume-btn').toggleClass('active');
    },

    _onProcessCompareData: function (symbol) {
        var that = this;

        return function (err, comparisonData) {
            if (err) {
                return;
            }

            var baseChart = that.get('baseChart');

            // Todo [Ravindu] no need to loop every data
            STX.addMemberToMasterdata(baseChart, symbol, comparisonData);
        };
    },

    _onPrependHeadsUpHR: function () {
        try {
            var baseChart = this.get('baseChart');

            if (baseChart) {
                var tick = Math.floor((STXChart.crosshairX - baseChart.chart.left) / baseChart.layout.candleWidth);
                var prices = baseChart.chart.xaxis[tick];
                // Todo [Ravindu] related to displaying indicator values on chart
                // var activeTStudies = this.get('activeTechStudies');

                this.prependMouseOut();

                if (prices && prices.data) {
                    this.set('pointOpen', baseChart.formatPrice(prices.data.Open));
                    this.set('pointHigh', baseChart.formatPrice(prices.data.High));
                    this.set('pointLow', baseChart.formatPrice(prices.data.Low));
                    this.set('pointClose', baseChart.formatPrice(prices.data.Close));
                    this.set('pointDate', this.utils.formatters.formatDateToDisplayDate(prices.data.DT));
                    this.set('pointVolume', STX.condenseInt(prices.data.Volume));

                    // Todo [Ravindu] related to displaying indicator values on chart
                    /* var studyRecord = null;
                     var recordValue = '';

                     if (activeTStudies.length > 0) {
                     for (var i = 0; i < activeTStudies.length; i++) {
                     studyRecord = activeTStudies[i];
                     recordValue = prices.data[studyRecord.get('key')] ? baseChart.formatPrice(prices.data[studyRecord.get('key')]) : 0;
                     studyRecord.set('value', recordValue);
                     }
                     }*/
                }
            }
        } catch (e) {
            this.utils.logger.logError('Error in prependHeadsUpHR of Pro Chart : ' + e);
        }
    },

    _loadChartIntervals: function () {
        var intraArray = Ember.A();
        var hisArray = Ember.A();
        var intervalsMap = this.get('definedIntervalsMap');
        var viewIntervalObj = null;

        intervalsMap[ChartConstants.ChartCategory.Intraday.ID] = intraArray;
        intervalsMap[ChartConstants.ChartCategory.History.ID] = hisArray;

        Ember.$.each(ChartConstants.ProChartViewInterval, function (key, value) {
            if (value) {
                viewIntervalObj = ChartConstants.ChartViewInterval[key];

                if (viewIntervalObj.IsHistory) {
                    hisArray.pushObject(viewIntervalObj);
                } else {
                    intraArray.pushObject(viewIntervalObj);
                }
            }
        });
    },

    _loadIndicators: function () {
        var averagesArray = Ember.A();
        var bandsArray = Ember.A();
        var otherIndicator = Ember.A();
        var indicatorsMap = this.get('definedIndicatorsMap');

        indicatorsMap[ChartStudies.IndicatorCategories.Averages] = averagesArray;
        indicatorsMap[ChartStudies.IndicatorCategories.Bands] = bandsArray;
        indicatorsMap[ChartStudies.IndicatorCategories.Others] = otherIndicator;

        // Adding indicators
        if (appConfig.chartConfig && appConfig.chartConfig.chartIndicators && appConfig.chartConfig.chartIndicators.length > 0) {
            Ember.$.each(appConfig.chartConfig.chartIndicators, function (key, value) {
                var indicatorObj = ChartStudies.Indicators[value];

                switch (indicatorObj.Category) {
                    case ChartStudies.IndicatorCategories.Averages:
                        averagesArray.pushObject(indicatorObj);
                        break;

                    case ChartStudies.IndicatorCategories.Bands:
                        bandsArray.pushObject(indicatorObj);
                        break;

                    default:
                        otherIndicator.pushObject(indicatorObj);
                }
            });
        }
    },

    _onChangeChartInterval: function () {
        var baseChart = this.get('baseChart');

        if (baseChart) {
            baseChart.setPeriodicityV2(this.get('chartInterval').Value, this.get('chartCategory').RowTickFormat);
        }
    }.observes('chartInterval'),

    _onChangeCategory: function (chartCategory) {
        try {
            var defaultPeriod = ChartConstants.ChartViewPeriod[chartCategory.DefaultChartViewPeriod];
            var defaultInterval = ChartConstants.ChartViewInterval[defaultPeriod.DefaultInterval];
            var previousViewPeriod = this.get('chartViewPeriod');
            var gdm = this.get('graphDataManager');

            this.set('chartCategory', chartCategory);
            this.set('chartViewPeriod', defaultPeriod);
            this.set('chartInterval', defaultInterval);

            STX.clearCanvas(this.get('baseChart').chart.canvas, this.get('baseChart'));

            gdm.refineGraphData({
                chartCategory: chartCategory,
                chartDataLevel: defaultPeriod.ChartDataLevel,
                chartViewPeriod: defaultPeriod
            });

            this._setSelectedTab(previousViewPeriod);
            this.utils.analyticsService.trackEvent(this.get('gaKey'), this.utils.Constants.GAActions.viewChanged, ['mode:', chartCategory.LanguageTag].join(''));
        } catch (e) {
            this.utils.logger.logError('[pro Chart] setChartCategory() ' + e);
        }
    },

    _onChangePeriod: function (chartPeriod) {
        try {
            var isDifferentPeriod = this.get('chartViewPeriod').ID !== chartPeriod.ID;

            if (isDifferentPeriod) {
                var gdm = this.get('graphDataManager');
                var differentCategory = this.get('chartViewPeriod').IsHistory !== chartPeriod.IsHistory;

                this.set('chartViewPeriod', chartPeriod);

                if (differentCategory) {
                    this.set('chartCategory', chartPeriod.IsHistory ? ChartConstants.ChartCategory.History : ChartConstants.ChartCategory.Intraday);
                    this.set('chartInterval', ChartConstants.ChartViewInterval[chartPeriod.DefaultInterval]);
                }

                if (this.get('baseChart.chart.canvas')) {
                    STX.clearCanvas(this.get('baseChart').chart.canvas, this.get('baseChart'));
                }

                gdm.refineGraphData({
                    chartCategory: this.get('chartCategory'),
                    chartDataLevel: chartPeriod.ChartDataLevel,
                    chartViewPeriod: chartPeriod
                });

                this.utils.analyticsService.trackEvent(this.get('gaKey'), this.utils.Constants.GAActions.viewChanged, ['period:', chartPeriod.DisplayName].join(''));
            }
        } catch (e) {
            this.utils.logger.logError('[Pro Chart] setChartPeriod() ' + e);
        }
    },

    /* *
     * select relevance tab for selected period
     * @param previous view period : deselection
     */
    _setSelectedTab: function () {
        this.set('chartPeriodNewActive', this.get('chartViewPeriod'));
    },

    _setVolumeLabelPosition: function () {
        var baseChart = this.get('baseChart');

        if (baseChart) {
            var vChart = baseChart.panels.vchart;

            if (vChart && this.get('volumeViewEnabled')) {
                this.set('volumeToTop', vChart.top);
            }
        }
    },

    _restoreDrawings: function () {
        try {
            var baseChart = this.get('baseChart');

            if (baseChart) {
                var chartDrawings = this.get('chartDrawings');

                if (chartDrawings) {
                    baseChart.reconstructDrawings(chartDrawings);
                    baseChart.draw();
                }
            }
        } catch (e) {
            this.utils.logger.logError('[Pro Chart] _restoreDrawings() ' + e);
        }
    },

    _saveLayout: function () {
        var baseChart = this.get('baseChart');
        var that = this;

        if (baseChart) {
            var exportedLayout = baseChart.exportLayout();

            var layout = {
                baseLayout: exportedLayout,
                chartCategory: that.get('chartCategory'),
                chartPeriod: that.get('chartViewPeriod'),
                chartInterval: that.get('chartInterval'),
                crossHairVisibility: that.get('crossHairEnabled'),
                verticalGrid: baseChart.chart.xAxis.displayGridLines,
                horizontalGrid: baseChart.chart.yAxis.displayGridLines,
                isMaximize: that.get('previousParent') ? true : false,
                chartStyle: that.get('chartStyle')
            };

            this.saveWidget({chartLayout: layout});
        }
    },

    _saveDrawings: function () {
        var baseChart = this.get('baseChart');

        if (baseChart) {
            var serializedDrawings = baseChart.serializeDrawings();

            if (serializedDrawings.lenght === 0) {
                this.saveWidget({chartDrawings: null});
            } else {
                this.set('chartDrawings', serializedDrawings);

                this.saveWidget({chartDrawings: serializedDrawings});
            }
        }
    },

    _enableCrossHair: function () {
        var baseChart = this.get('baseChart');

        if (baseChart) {
            if (!this.get('crossHairEnabled')) {
                baseChart.layout.crosshair = true;
                this.set('crossHairEnabled', true);

                this.utils.analyticsService.trackEvent(this.get('gaKey'), this.utils.Constants.GAActions.viewChanged, 'cross-hair:checked');
            } else {
                baseChart.layout.crosshair = false;
                this.set('crossHairEnabled', false);

                this.utils.analyticsService.trackEvent(this.get('gaKey'), this.utils.Constants.GAActions.viewChanged, 'cross-hair:unchecked');
            }

            Ember.$('#crosshair-btn').toggleClass('active');
        }
    },

    toggleFullScreen: function () {
        this._super('proChartContainer-' + this.get('wkey'), this.get('wkey'));
    },

    setWidgetForScreenMode: function () {
        var viewName = 'price/widgets/watch-list/index-watch-list';
        var watchListController = this.get('watchListController') || ControllerFactory.createController(this.container, 'controller:' + viewName);

        if (this.get('isFullScreenMode')) {
            watchListController.initializeWidget();
            var route = this.container.lookup('route:application');

            route.render(viewName, {
                into: 'chart/pro-chart',
                outlet: 'wlWidgetOutlet',
                controller: watchListController
            });
        } else {
            watchListController.closeWidget();
        }
    },

    showSearchPopup: function () {
        var modal = this.get(this.get('searchSymbolKey'));
        modal.send('showModalPopup');
    },

    showSearchComparePopup: function () {
        var modal = this.get(this.get('compareSymbolKey'));
        modal.send('showModalPopup');
    },

    searchKeyDidChange: function () {
        var searchKey = this.get('searchKey');

        if (searchKey && searchKey.length) {
            Ember.run.debounce(this, this.showSearchPopup, 300);
        }
    }.observes('searchKey'),

    compareSearchKeyDidChange: function () {
        var searchKey = this.get('searchKeyCompare');

        if (searchKey && searchKey.length) {
            Ember.run.debounce(this, this.showSearchComparePopup, 300);
        }
    }.observes('searchKeyCompare'),

    actions: {
        setChartCategory: function (chartCategory) {
            this._onChangeCategory(chartCategory);
        },

        setChartPeriod: function (chartPeriod) {
            this._onChangePeriod(chartPeriod);
        },

        setChartInterval: function (option) {
            this.set('chartInterval', option);
            this.utils.analyticsService.trackEvent(this.get('gaKey'), this.utils.Constants.GAActions.viewChanged, ['interval:', option.LanguageTag].join(''));
        },

        setChartStyle: function (option) {
            var baseChart = this.get('baseChart');

            this.set('chartStyle', option);
            baseChart.setChartType(option.ChartIQChartType);
            this.utils.analyticsService.trackEvent(this.get('gaKey'), this.utils.Constants.GAActions.viewChanged, ['style:', option.LanguageTag].join(''));
        },

        setChartGridStyle: function (option) {
            var baseChart = this.get('baseChart');

            if (baseChart) {
                switch (option.ID) {
                    case 0:
                        baseChart.chart.xAxis.displayGridLines = true;
                        baseChart.chart.yAxis.displayGridLines = true;
                        break;

                    case 2:
                        baseChart.chart.xAxis.displayGridLines = false;
                        baseChart.chart.yAxis.displayGridLines = true;
                        break;

                    case 3:
                        baseChart.chart.xAxis.displayGridLines = true;
                        baseChart.chart.yAxis.displayGridLines = false;
                        break;

                    default:    // None = 1 (all other cases will be handled as None)
                        baseChart.chart.xAxis.displayGridLines = false;
                        baseChart.chart.yAxis.displayGridLines = false;
                        break;
                }

                this.set('selectedGridType', option);

                baseChart.draw();
                baseChart.changeOccurred('layout');
                this.utils.analyticsService.trackEvent(this.get('gaKey'), this.utils.Constants.GAActions.viewChanged, ['grid:', option.LanguageTag].join(''));
            }
        },

        dismissStudyDialog: function () {
            STX.DialogManager.dismissDialog();
        },

        createStudy: function () {
            var baseChart = this.get('baseChart');

            STX.Studies.go($$('studyDialog'), baseChart);
            // Todo [Ravindu] related to displaying indicator values on chart
            /* var activeTStudies = this.get('activeTechStudies');

             for (var item in sd.outputMap) {
             if (!Ember.isEmpty(item)) {
             activeTStudies.pushObject(techStudyRecord.create({
             key: item,
             value: 0
             }));
             }
             }*/
            STX.DialogManager.dismissDialog();
        },

        studyDialog: function (option) {
            var that = this;
            var baseChart = this.get('baseChart');

            if (!baseChart || !baseChart.chart.dataSet) {
                return;
            }

            var name = option.ChartIQID;
            var libraryEntry = STX.Studies.studyLibrary[option.ChartIQID];

            if (libraryEntry && libraryEntry.name) {
                name = that.get('app').lang.labels[libraryEntry.languageTag];
            }

            $$('studyDialog').querySelectorAll('.title')[0].innerHTML = name;

            STX.Studies.studyDialog(baseChart, option.ChartIQID, $$('studyDialog'));
            var delay = STX.ipad ? 400 : 0;  // So that ipad doesn't register taps from menu selection on dialog

            setTimeout(function () {
                STX.DialogManager.displayDialog('studyDialog');
            }, delay);

            this.utils.analyticsService.trackEvent(this.get('gaKey'), ChartConstants.ChartGAActions.indAdded, ['ind:', option.LanguageTag].join(''));
        },

        showSearchPopup: function () {
            this.showSearchPopup();
        },

        closeSearchPopup: function () {
            var modal = this.get(this.get('searchSymbolKey'));
            modal.send('closeModalPopup');
        },

        onSearchSymbolSelected: function (item) {
            var symbolArgs = {sym: item.sym, exg: item.exg, inst: item.inst};

            this.saveWidget(symbolArgs);
            this.refreshWidget(symbolArgs);
        },

        showSearchComparePopup: function () {
            this.showSearchComparePopup();
        },

        closeSearchComparePopup: function () {
            var modal = this.get(this.get('compareSymbolKey'));
            modal.send('closeModalPopup');
        },

        showComparePopup: function () {
            var modal = this.get('chartSymbolCompare');
            modal.send('showModalPopup');
        },

        closeComparePopup: function () {
            var modal = this.get('chartSymbolCompare');
            modal.send('closeModalPopup');
        },

        onCompareSymbol: function (item) {
            var baseChart = this.get('baseChart');

            if (baseChart.chart.symbol !== item.sym) {
                var gDM = this.get('graphDataManager');
                var chartSymbol = gDM.addChartSymbol(item.exg, item.sym);

                gDM.addChartDataSubscription(chartSymbol);
                this.utils.analyticsService.trackEvent(this.get('gaKey'), ChartConstants.ChartGAActions.compare, ['sym:', this.get('sym'), '~', this.get('exg')].join(''));
            }
        },

        onToggleCrossHair: function () {
            var baseChart = this.get('baseChart');

            if (baseChart) {
                this._enableCrossHair();
                baseChart.changeOccurred('layout');
            }
        },

        onToggleVolume: function () {
            this.onDisplayVolume();
        },

        setLineStudy: function (vector) {
            var baseChart = this.get('baseChart');

            // Todo: [Ravindu] Hot fix.
            if (baseChart) {
                if (this.get('selectedLineStudy') !== vector) {
                    this.set('selectedLineStudy', vector);
                    Ember.$('#' + this.get('selectedLineStudy')).toggleClass('active');
                    this.utils.analyticsService.trackEvent(this.get('gaKey'), this.utils.Constants.GAActions.viewChanged, [this.get('selectedLineStudy'), ':checked'].join(''));
                } else {
                    Ember.$('#' + this.get('selectedLineStudy')).toggleClass('active');
                    this.utils.analyticsService.trackEvent(this.get('gaKey'), this.utils.Constants.GAActions.viewChanged, [this.get('selectedLineStudy'), ':unchecked'].join(''));
                    this.set('selectedLineStudy', '');
                }

                baseChart.changeVectorType(this.get('selectedLineStudy'));
            }
        },

        // Todo [Ravindu] move below method to outside of action : readability
        onToggleFullScreenMode: function () {
            this.toggleFullScreen();
            this.get('baseChart').changeOccurred('layout');
        },

        setLink: function (option) {
            this.setWidgetLink(option);
        }
    }
});

Ember.Handlebars.helper('global-search', GlobalSearch);

import Ember from 'ember';
import BaseArrayController from '../base-array-controller';
import sharedService from '../../models/shared/shared-service';
import ChartConstants from '../../models/chart/chart-constants';
import graphDM from '../../models/chart/data-stores/graph-data-manager';
import userSettings from '../../config/user-settings';
import appEvents from '../../app-events';
import ThemeDataStore from '../../models/shared/data-stores/theme-data-store';
import languageDataStore from '../../models/shared/language/language-data-store';

/* global STXChart */
/* global STX */

export default BaseArrayController.extend({
    baseChart: null,
    graphDataManager: null,
    symbolObj: null,
    chartStyle: ChartConstants.ChartStyle.Area,
    chartViewPeriod: ChartConstants.ChartViewPeriod.OneDay,
    chartCategory: ChartConstants.ChartCategory.Intraday,
    volumeStudyDescriptor: null,
    volumeViewEnabled: false,
    reloadChartData: null,
    isDisableChartControls: false,

    // Base overrides
    onLoadWidget: function () {
        this._loadWidget();
        appEvents.subscribeSymbolChanged(this.get('wkey'), this, this.get('selectedLink'));
    },

    // Base overrides
    onUnloadWidget: function () {
        this.set('graphDataManager', null);
        this.set('baseChart', null);

        appEvents.unSubscribeSymbolChanged(this.get('wkey'), this.get('selectedLink'));
    },

    // Base overrides
    onPrepareData: function () {
        var insType = this.get('inst');

        if (insType === this.utils.AssetTypes.Option) {
            var optionStock = sharedService.getService('price').optionStockDS.getOptionStock(this.get('exg'), this.get('sym'));

            if (optionStock) {
                this.set('exg', optionStock.get('trdExg'));
                this.set('sym', optionStock.get('baseSym'));
                this.set('inst', optionStock.get('inst'));
            }
        }

        this.prepareChartData();
    },

    // Base overrides
    onClearData: function () {
        var baseChart = this.get('baseChart');
        var gdm = this.get('graphDataManager');

        if (gdm) {
            gdm.removeChartDataSubscription();
        }

        if (baseChart && baseChart.chart && baseChart.chart.canvas) {
            STX.clearCanvas(baseChart.chart.canvas, baseChart);
        }

        if (gdm) {
            gdm.removeChartSymbols();
        }
    },

    prepareChartData: function () {
        var gdm = this.get('graphDataManager');

        if (gdm) {
            this.set('symbolObj', sharedService.getService('price').stockDS.getStock(this.get('exg'), this.get('sym')));
            gdm.addChartSymbol(this.get('exg'), this.get('sym'), true);
            gdm.addChartDataSubscription();
            this.utils.analyticsService.trackEvent(this.get('gaKey'), this.utils.Constants.GAActions.show, ['sym:', this.get('sym'), '~', this.get('exg')].join(''));
        }
    },

    onDataFromMix: function (chartSymbolObj) {
        var baseChart = this.get('baseChart');

        if (baseChart) {
            baseChart.newChart(this.get('sym'), null, null, this.onFinishedLoadingNewChart(baseChart.chart.symbol));
        } else {
            this.set('reloadChartData', chartSymbolObj);
        }
    },

    onDataFromRealtime: function (ohlcPoint, exg, sym) {
        var baseChart = this.get('baseChart');

        if (baseChart) {
            if (this._isBaseSymbol(exg, sym) && baseChart.masterData && baseChart.masterData.length > 0) {
                baseChart.appendMasterData([ohlcPoint]);
            }
        }
    },

    onFinishedLoadingNewChart: function () {
        // This is a call-back function that will be triggered after finished chart drawing
    },

    onLanguageChanged: function () {
        STX.DFNLang.langObj = languageDataStore.getLanguageObj();
    },

    initChart: function (chartContainer) {
        var that = this;

        STX.DFNLang.langObj = languageDataStore.getLanguageObj();
        STX.QuoteFeed.chartDataLoader = function () {};
        STX.QuoteFeed.chartDataLoader.stxInheritsFrom(STX.QuoteFeed);

        STX.QuoteFeed.chartDataLoader.prototype.fetch = function (params, cb) {
            if (params.startDate && params.endDate) {
                that.onGetRangeOfChartRecords(params, cb); // Query data range e.g: compare symbol
                return;
            } else if (params.startDate || params.endDate) {
                // params.startDate --> Query new data. This is not used since we are providing the data in the streaming mode
                // params.endDate --> Query old data (pagination)
            } else {
                that.onGetChartInitialBulkRecords(params, cb);
            }
        };

        var baseChart = new STXChart({
            container: chartContainer
        });

        that.set('baseChart', baseChart);
        baseChart.chart.xAxis.axisType = 'ntb';
        baseChart.goldenRatioYAxis = true;
        that.onLoadLayout();
        baseChart.attachQuoteFeed(new STX.QuoteFeed.chartDataLoader(), {'refreshInterval': 0});

        var defaultChartDecimals = 2;
        baseChart.chart.yAxis.decimalPlaces = that.get('symbolObj') && that.get('symbolObj').deci >= 0 ? that.get('symbolObj').deci : userSettings.displayFormat.decimalPlaces >= 0 ? userSettings.displayFormat.decimalPlaces : defaultChartDecimals;

        if (this.get('isDisableChartControls')) {
            baseChart.controls.chartControls = null;
            baseChart.controls.home = null;
        }

        // [Investigations] chartIQ expects default periods for first time period loading
        baseChart.setPeriodicityV2(ChartConstants.ChartViewInterval[that.get('chartViewPeriod').DefaultInterval].Value, that.get('chartCategory').RowTickFormat);
        this.onThemeChanged(sharedService.userSettings.currentTheme);

        STXChart.prototype.append('draw', function () {
            that.onAfterFinishedDrawingChart();
        });

        STXChart.prototype.prepend('draw', function () {
            that.onBeforeFinishedDrawingChart();
            that._preventScroll();
        });

        if (that.onCheckVolumeChartEnabler()) {
            that.onDisplayVolume();
        }

        // InitChart and onDataFromMix are parallel activities. If onDataFromMix is invoked before initChart, loading params are stored and invoke again onDataFromMix in end of initChart
        var reloadParams = this.get('reloadChartData');

        if (reloadParams) {
            this.onDataFromMix(reloadParams);
            this.set('reloadChartData', null);
        }
    },

    onGetRangeOfChartRecords: function () {
        // Subclass may overrides to provide required data range for chart
    },

    onGetChartInitialBulkRecords: function () {
        // Subclass should overrides to provide initial data set for chart
    },

    onAfterFinishedDrawingChart: function () {
        // Subclasses can override this method for call their actions after finished chart drawing
    },

    onBeforeFinishedDrawingChart: function () {
        // Subclasses can override this method for call their actions before finished chart drawing
    },

    /* *
     * Load layout from given priority.
     *  01) User Layout
     *  02) Default Layout
     */
    onLoadLayout: function () {
        // Subclasses should override this to load layout
    },

    onCheckVolumeChartEnabler: function () {
        return true; // Subclasses may override this hook with their own implementation
    },

    onDisplayVolume: function () {
        var baseChart = this.get('baseChart');
        var vSD;

        if (baseChart) {
            if (!this.get('volumeViewEnabled')) {
                vSD = STX.Studies.quickAddStudy(baseChart, 'vchart', {});
                vSD.permanent = true;

                this.set('volumeStudyDescriptor', vSD);
                this.set('volumeViewEnabled', true);
                this.utils.analyticsService.trackEvent(this.get('gaKey'), this.utils.Constants.GAActions.viewChanged, ['volume', ':checked'].join(''));
            } else {
                vSD = this.get('volumeStudyDescriptor');

                if (vSD) {
                    STX.Studies.removeStudy(baseChart, vSD);
                }

                this.set('volumeViewEnabled', false);
                this.utils.analyticsService.trackEvent(this.get('gaKey'), this.utils.Constants.GAActions.viewChanged, ['volume', ':unchecked'].join(''));
            }
        }
    },

    // Base overrides
    onThemeChanged: function (theme) {
        var baseChart = this.get('baseChart');

        if (baseChart) {
            if (theme === 'theme1') {
                STX.ThemeManager.loadBuiltInTheme(baseChart, 'Dark', null);
            } else {
                STX.ThemeManager.loadBuiltInTheme(baseChart, 'Light', null);
            }
        }

    },

    _loadWidget: function () {
        var that = this;

        that.set('graphDataManager', graphDM.create({
            chartCategory: that.chartCategory,
            chartDataLevel: that.chartViewPeriod.ChartDataLevel,
            chartViewPeriod: that.chartViewPeriod,
            wKey: that.get('wkey'),

            onData: function (ohlcPoint, exg, sym) {
                that.onDataFromRealtime(ohlcPoint, exg, sym);
            },

            onDataChunk: function (chartSymbolObj) {
                that.onDataFromMix(chartSymbolObj);
            },

            onErrorFn: function () {
                // TODO: [Ravindu] implement this
            }
        }));

        // This is to support latest chartiq inbuilt themes
        var userThemes = ThemeDataStore.getUserThemes();

        if (userThemes) {
            var builtInThemes = [];

            Ember.$.each(userThemes, function (key, val) {
                if (val && val.category && !builtInThemes.contains(val.category)) {
                    builtInThemes.pushObject(val.category);
                }
            });

            STX.ThemeManager.builtInThemes = builtInThemes;
        }
    },

    _preventScroll: function () {
        var baseChart = this.get('baseChart');

        if (baseChart) {
            var rightTick = baseChart.chart.dataSet.length - baseChart.chart.scroll + baseChart.chart.maxTicks;

            if (rightTick > baseChart.chart.dataSet.length || baseChart.chart.dataSet.length < baseChart.chart.maxTicks) {
                baseChart.chart.scroll = baseChart.chart.maxTicks;
            } else if (baseChart.chart.scroll > baseChart.chart.dataSet.length) {
                baseChart.chart.scroll = baseChart.chart.dataSet.length;
            }

            if (baseChart.chart.dataSet.length < baseChart.chart.maxTicks) {
                baseChart.chart.scroll = baseChart.chart.dataSet.length;

                // Stop zooming also
                baseChart.setCandleWidth((baseChart.chart.width - baseChart.preferences.whitespace) / baseChart.chart.dataSet.length);
            } else if (baseChart.chart.scroll > baseChart.chart.dataSet.length) {
                baseChart.chart.scroll = baseChart.chart.dataSet.length;
            }
        }
    },

    _isBaseSymbol: function (exg, sym) {
        if (exg && sym) {
            return (this.get('exg') === exg && this.get('sym') === sym);
        }

        return false;
    }
});

import ButtonCell from '../button-cell';

export default ButtonCell.extend({
    templateName: 'table/views/alert/alert-menu-cell'
});
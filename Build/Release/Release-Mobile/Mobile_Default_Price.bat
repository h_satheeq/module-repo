#echo off

set "customizationpath=Default/Default/Mobile/Default/Price"

set "releaseType=%1"
set "isAutomatedPath=%2"

if "%releaseType%"=="" set "releaseType=production"
if "%isAutomatedPath%"=="" set "isAutomatedPath=false"

cd ..\..\

@echo Executing main.js file.....
node Build_Scripts\BuildMain.js %customizationpath% %releaseType% %isAutomatedPath% true

Pause...



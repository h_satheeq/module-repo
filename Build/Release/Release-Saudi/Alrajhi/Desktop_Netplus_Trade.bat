#echo off

set "customizationpath=Saudi/Alrajhi/Desktop/Netplus/Trade"

set "releaseType=%1"
set "isAutomatedPath=%2"

if "%releaseType%"=="" set "releaseType=production"

if "%isAutomatedPath%"=="" set "isAutomatedPath=false"

cd ..\..\..\

nvm use 6.9.2

@echo Executing main.js file.....
node Build_Scripts\BuildMain.js %customizationpath% %releaseType% %isAutomatedPath% true

if "%isAutomatedPath%"=="false" (
Pause...
)

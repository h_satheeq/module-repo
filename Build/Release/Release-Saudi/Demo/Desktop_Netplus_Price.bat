#echo off

set "customizationpath=Saudi/Demo/Desktop/Netplus/Price"

set "releaseType=%1"
set "isAutomatedPath=%2"

if "%releaseType%"=="" set "releaseType=production"
if "%isAutomatedPath%"=="" set "isAutomatedPath=false"

cd ..\..\..\

@echo Executing main.js file.....
node Build_Scripts\\ReleaseAutomation\main.js %customizationpath% %releaseType% %isAutomatedPath%

Pause...



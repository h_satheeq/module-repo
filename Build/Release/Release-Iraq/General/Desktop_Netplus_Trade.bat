#echo off
set "customizationpath=Iraq/General/Desktop/Netplus/Trade"

set "releaseType=%1"
if "%releaseType%"=="" set "releaseType=production"

cd ..\..\..\

nvm use 6.9.2

@echo Executing main.js file.....
node Build_Scripts\BuildMain.js %customizationpath% %releaseType%

Pause...

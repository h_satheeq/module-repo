ECHO Build Started......

set ServerURL=%1
set SecureLevel=%2
set LoginName=%3
set Password=%4

ECHO "%ServerURL%"
ECHO "%SecureLevel%"
ECHO "%LoginName%"
ECHO "%Password%"

node Test\ServerTestMain.js %ServerURL% %SecureLevel% %LoginName% %Password%
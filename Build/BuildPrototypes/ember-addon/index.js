/* jshint node: true */
'use strict';

module.exports = {
  name: '{{ua-addon-name}}',

  // To support live reload in debug mode
  isDevelopingAddon: function() {
    return true;
  }
};

ECHO Build Started......

set Country=%1
set Brokerage=%2
set Device=%3
set Prodcut=%4
set PriceOrTrade=%5
set Version=%6
set IsLabelRequired=%7
set "Separator=/"
set "VersionSep=_" 
set "Exchanges=M"
set "releaseType=production"
set "isAutomatedPath=true"

ECHO "%Country%"
ECHO "%Brokerage%"
ECHO "%Device%"
ECHO "%Prodcut%"
ECHO "%PriceOrTrade%"
ECHO "%Version%"
ECHO "%VersionPrefix%"
ECHO "%IsLabelRequired%"

set "VersionPrefix="

if %Device% == Desktop set VersionPrefix=DFNUAWEB_
	
if %Device% == Mobile set VersionPrefix=DFNUAMOB_
	

	
set "FullVersion=%VersionPrefix%%Brokerage%%VersionSep%%Exchanges%%VersionSep%%Version%"
set "CustmizationPath=%Country%%Separator%%Brokerage%%Separator%%Device%%Separator%%Prodcut%%Separator%%PriceOrTrade%"

ECHO. %CustmizationPath%
ECHO. %FullVersion%

node Build_Scripts\BuildMain.js %CustmizationPath% %releaseType% %isAutomatedPath% true %FullVersion% %IsLabelRequired%
var jsonFile = require('jsonfile');
var filePath = require('filepath');
var directoryPathHelper = require('./DirectoryPathHelper');
var svnAccess = require('./SvnAccess');
var wait = require('wait.for');

class GetVersion {
    constructor() {
    }

    execute() {
        var customizationPath = null;

        if (process.argv.length <= 2) {
            console.log('Input arguments not received.');
            process.exit(-1);
        } else {
            customizationPath = process.argv[2];
        }

        var constFilePath = filePath.create('Build_Scripts/constants.json');
        var constants = null;

        if (constFilePath.exists()) {
            constants = jsonFile.readFileSync(constFilePath.toString());
        } else {
            console.log('Constants file not available in path : ' +constFilePath.toString());
            process.exit(-1);
        }

        var versionFileSvnPath = directoryPathHelper.getSVNVersionFilePath(constants, customizationPath);

        console.log('Downloading version file.');
        wait.for(svnAccess.getVersionFileFromSVN, versionFileSvnPath, "temp/", "rasikea", "svn@dfn");
        console.log('Download version file completed.');

    }
}
const buildMain = new GetVersion();
wait.launchFiber(buildMain.execute);
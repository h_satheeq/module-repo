var jsonFile = require('jsonfile');
var filePath = require('filepath');
var directoryPathHelper = require('./DirectoryPathHelper');
var fse = require('nodejs-fs-utils');
var cmd = require('node-cmd');
var wait = require('wait.for');
var userInputReader = require('./UserInputReader');
var gitAccess = require('./GitAccess');
var fileCopy = require('./FileCopy');
var pathExists = require('path-exists');
var utility = require('./Utility');
var updateAppCache = require('./UpdateAppCache');
var updateModuleInitializer = require('./UpdateModuleInitializer');
var buildAppTask = require('./BuildTask');
var mobileTask = require('./MobileTasks');
var constants = require('./constants.json');

class BuildMain {
    constructor() {

    }

    invokeBuild() {
        var customizationPath = undefined;
        var releaseType = 'production';
        var isAutomatedPath = 'true';
        var versionValue = null;
        var isWindows = true;
        var isLabelRequired = false;
        var isInstallPackages = false;

        if (process.argv.length <= 2) {
            console.log('Input arguments not received.');
            // process.exit(-1);
        } else {
            customizationPath = process.argv[2];

            if (process.argv.length > 4) {
                releaseType = process.argv[3];
                isAutomatedPath = process.argv[4];
                if (process.argv[5]) {
                    isWindows = process.argv[5];
                    if (process.argv[6]) {
                        versionValue = process.argv[6];
                        if (process.argv[7]) {
                            isLabelRequired = process.argv[7];
                        }
                    }
                }
            }

            console.log("Customization path :" + customizationPath + " - Release Type : " + releaseType + " - Is Automated Path : " + isAutomatedPath + " - Version value : " + versionValue + " isWindows : " + isWindows);

            buildMain.execute(customizationPath, releaseType, isAutomatedPath, versionValue, isWindows, isLabelRequired, false);
        }
    }

    execute(customizationPath, releaseType, isAutomatedPath, versionValue, isWindows, isLabelRequired, isInstallPackages, checkoutPath) {
        console.log("Customization path :" + customizationPath + " - Release Type : " + releaseType + " - Is Automated Path : " + isAutomatedPath + " - Version value : " + versionValue + " isWindows : " + isWindows);

        var versionType = userInputReader.getVersionType(isAutomatedPath, releaseType);

        //Create Required paths folders
        var customizationDestinationPath = directoryPathHelper.getCustomizationDestinationPath(constants, customizationPath);
        var coreTempCheckoutPath = directoryPathHelper.getCoreTemporaryPath(constants, customizationPath);
        var customizationsTempCheckoutPath = directoryPathHelper.getCustomizationTemporaryPath(constants, customizationPath);
        var sourceTempCheckoutPath = directoryPathHelper.getSourceTemporaryPath(constants, customizationPath);
        var defaultCustomizationTempPath = directoryPathHelper.getDefaultCustomizationTemporaryPath(constants, customizationPath);
        var defaultCountryCustomizationTempPath = directoryPathHelper.getDefaultCountryCustomizationTemporaryPath(constants, customizationPath);
        var clientCustomizationTempPath = directoryPathHelper.getClientCustomizationTemporaryPath(constants, customizationPath);

        if (!sourceTempCheckoutPath.exists()) {
           filePath.create(sourceTempCheckoutPath).mkdir();
        }

        console.log('Clearing temporary checkout path', sourceTempCheckoutPath.toString());
        fse.rmdirsSync(sourceTempCheckoutPath.toString());

        if (releaseType !== 'dev' || (releaseType === 'dev' && !checkoutPath)) {
            // Get latest
            console.log('Git checkout');
            gitAccess.getLatestUsingHTTPS(sourceTempCheckoutPath.toString());
            console.log('Download Source completed.');
        } else if (checkoutPath) {
            console.log('Copying checkout files to temp directory');
            fileCopy.copy(checkoutPath.toString(), sourceTempCheckoutPath.toString());
        }

        //Remove SVN files
        console.log('Removing .git files in core checkout location.');
        fileCopy.removeGITFiles(coreTempCheckoutPath.toString());
        console.log('Remove .git files Completed.');
        //
        console.log('Removing .git files in customization checkout location.');
        fileCopy.removeGITFiles(customizationsTempCheckoutPath.toString());
        console.log('Remove .git files Completed.');

        // Create customization folder if not exist
        fileCopy.createFolder(customizationDestinationPath);

        if (customizationDestinationPath.exists()) {
            console.log('Clearing customization build folder', customizationDestinationPath.toString());

            fileCopy.getFilesInDir(customizationDestinationPath.toString()).forEach(function (fp) {
                fileCopy.emptyDestinationFolder(customizationDestinationPath.append(fp));
            });
        }

        //Copying core
        console.log('Copying core');
        var kernelTempPath = coreTempCheckoutPath.append('Kernel');
        console.log('Destination path', customizationDestinationPath.toString());

        fileCopy.copyFolder(kernelTempPath, customizationDestinationPath.append(constants.KernelBuild));

        var priceTempPath = coreTempCheckoutPath.append('Price');
        fileCopy.copyFolder(priceTempPath, customizationDestinationPath.append(constants.PriceBuild));

        var tradeTempPath = coreTempCheckoutPath.append('Trade');
        fileCopy.copyFolder(tradeTempPath, customizationDestinationPath.append(constants.TradeBuild));
        console.log('Copying core completed.');

        //Copying modules
        var moduleFileTempPath = directoryPathHelper.getModuleFileTemporaryPath(constants, customizationPath);
        var isMobileRelease = utility.isMobileRelease(customizationPath);
        var isTabRelease = utility.isTabRelease(customizationPath);
        var isDTRelease = utility.isDTRelease(customizationPath);
        var moduleArray = utility.getPreCopyModuleArray(moduleFileTempPath);

        fileCopy.copyModuleFiles(moduleArray, customizationsTempCheckoutPath, customizationDestinationPath, isMobileRelease);

        console.log('Copying modules completed.');

        //Copying customizations
        console.log('Copying default customizations.');
        fileCopy.copyCustomizations(customizationDestinationPath, defaultCustomizationTempPath);
        console.log('Copy default customizations completed.');

        console.log('Copying default country customizations.');
        fileCopy.copyCustomizations(customizationDestinationPath, defaultCountryCustomizationTempPath);
        console.log('Copy default country customizations completed.');

        console.log('Copying customizations.');
        fileCopy.copyCustomizations(customizationDestinationPath, clientCustomizationTempPath);
        console.log('Copy customizations completed.');

        var postCopyModuleArray = utility.getPostCopyModuleArray(moduleFileTempPath);

        fileCopy.copyModuleFiles(postCopyModuleArray, customizationsTempCheckoutPath, customizationDestinationPath, isMobileRelease);

        console.log('Copying widgets customization');

        // Widget folders are located in customization root path
        fileCopy.copyModuleFiles(moduleArray, defaultCustomizationTempPath.append('../'), customizationDestinationPath, isMobileRelease);
        fileCopy.copyModuleFiles(moduleArray, defaultCountryCustomizationTempPath.append('../'), customizationDestinationPath, isMobileRelease);
        fileCopy.copyModuleFiles(moduleArray, clientCustomizationTempPath.append('../'), customizationDestinationPath, isMobileRelease);

        fileCopy.copyModuleFiles(postCopyModuleArray, defaultCustomizationTempPath.append('../'), customizationDestinationPath, isMobileRelease);
        fileCopy.copyModuleFiles(postCopyModuleArray, defaultCountryCustomizationTempPath.append('../'), customizationDestinationPath, isMobileRelease);
        fileCopy.copyModuleFiles(postCopyModuleArray, clientCustomizationTempPath.append('../'), customizationDestinationPath, isMobileRelease);

        // Temporary script to move all styles and configs to kernel folder
        fileCopy.moveBuildFilesToKernel(customizationDestinationPath);

        //Update module initializer
        if(moduleArray && moduleArray.length > 0) {
            updateModuleInitializer.update(moduleArray, customizationDestinationPath);
        }

        var kernelFolder = customizationDestinationPath.append(constants.KernelBuild);
        var kernelFolderStr = customizationDestinationPath.append(constants.KernelBuild).toString();
        var modules = fileCopy.getModuleDir(customizationDestinationPath);

        console.log('Applying addon patch');
        fileCopy.applyAddonPatch(customizationDestinationPath);

        // Install module dependencies
        if (isInstallPackages) {
            console.log('Installing dependencies for build');
            buildAppTask.installBroccoliCli();

            modules.forEach(function (module) {
                var modulePath = customizationDestinationPath.append(module).toString();
                buildAppTask.npmInstall(modulePath);
                buildAppTask.bowerInstall(modulePath);
            });
        }

        buildAppTask.setGitConfig();

        // Install kernel dependencies
        if (isInstallPackages) {
            buildAppTask.npmInstall(kernelFolderStr);
            buildAppTask.bowerInstall(kernelFolderStr);
            buildAppTask.npmInstall(kernelFolder.append('node_modules/ember-cli-ic-ajax').toString());

            var coreKernel = coreTempCheckoutPath.append(constants.Kernel, constants.Application);
            fileCopy.copy(coreKernel.append('bower_components').toString(), kernelFolder.append('bower_components').toString());
            fileCopy.copy(coreKernel.append('node_modules').toString(), kernelFolder.append('node_modules').toString());
        }

        if (releaseType === 'dev') {
            modules.forEach(function (module) {
                // buildAppTask.copyNodeModules(kernelFolderStr); // TODO: [satheeqh] Find a way to copy node modules from cache

                buildAppTask.npmLink(customizationDestinationPath.append(module).toString());
                buildAppTask.npmLink(kernelFolderStr, module);
            });

            console.log('Setting up development environment completed.');

            return;
        }

        var appFolderPath = customizationDestinationPath.append('../').append(constants.Application).toString();
        var versionFilePath = directoryPathHelper.getVersionFilePath(appFolderPath);
        var packageJsonFilePath = directoryPathHelper.getPackageJsonFilePath(kernelFolderStr);
        userInputReader.updateSemanticVersion(packageJsonFilePath);

        if (versionValue == null) {
            if (pathExists.sync(versionFilePath.toString())) {
                versionValue = userInputReader.getVersionNumberFromVersionFile(versionFilePath, versionType);
                userInputReader.updateVersion(versionValue, versionFilePath, packageJsonFilePath);
            } else {
                if (isAutomatedPath === 'false') {
                    versionValue = userInputReader.getVersionNumber();
                    userInputReader.updateVersion(versionValue, versionFilePath, packageJsonFilePath);
                }
            }
        } else {
            userInputReader.updateVersion(versionValue, versionFilePath, packageJsonFilePath);
        }

        buildAppTask.buildSass(kernelFolderStr);

        var distFolderPath = null;

        if (isAutomatedPath === 'false') {
            console.log('Build started');
            buildAppTask.buildApp(customizationDestinationPath, modules);
            console.log('Build completed');

            distFolderPath = directoryPathHelper.getDistFolderPathLocal(customizationPath, versionValue);
        } else {
            console.log('Ember CLI build started. Automation mode.');
            buildAppTask.buildApp(customizationDestinationPath, modules);
            console.log('Ember CLI build completed.');

            if (isMobileRelease) {
                distFolderPath = directoryPathHelper.getDistFolderPathServerMobile(customizationPath);
            } else if (isTabRelease) {
                distFolderPath = directoryPathHelper.getDistFolderPathServerTablet(customizationPath);
            } else if (isDTRelease) {
                distFolderPath = directoryPathHelper.getDistFolderPathServerDT(customizationPath);
            } else {
                distFolderPath = directoryPathHelper.getDistFolderPathServerDesktop(customizationPath);
            }

            if (pathExists.sync(distFolderPath)) {
                console.log('Removing content of release folder.');
                fse.emptyDirSync(distFolderPath);
                console.log('Content of the release folder removed.');
            }
        }

        if (isLabelRequired) {
            if (versionValue) {
                wait.for(gitAccess.addTag, versionValue);
                wait.for(gitAccess.pushTags, versionValue);
            }
        }

        // TODO: [satheeqh] Enable check-in once moved to main branch

        // console.log('Version file check in started.');
        // wait.for(gitAccess.addRemoteRepo, versionFilePath.toString());
        // wait.for(gitAccess.addVersionFile, versionFilePath.toString());
        // wait.for(gitAccess.commitVersionFile, versionFilePath.toString());
        // wait.for(gitAccess.pushVersion, versionFilePath.toString());
        // console.log('Version file check in completed.');

        if (isMobileRelease || isTabRelease) {
            mobileTask.update(kernelFolder);
            mobileTask.updateMetaContent(kernelFolder);
        }

        updateAppCache.update(kernelFolder, versionValue);

        console.log('Release folder:', distFolderPath.toString());
        fileCopy.createFolder(distFolderPath.toString());

        console.log('Copying files to the destination.');
        fileCopy.copy(kernelFolder.append('dist').toString(), distFolderPath.toString());
        console.log('Copy files to the destination completed.');
    }
}

const buildMain = new BuildMain();
// wait.launchFiber(buildMain.invokeBuild);
buildMain.invokeBuild();

module.exports = new BuildMain();
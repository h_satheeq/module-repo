var jsonFile = require('jsonfile');
var filePath = require('filepath');
var format = require('string-format');

class DirectoryPathHelper {
    constructor() {

    }

    getCustomizationDestinationPath(constant, customizationPath) {
        // var customizationDestinationPath = format('{0}{1}{2}', '../', constant.Customizations, customizationPath);
        return filePath.create('../', constant.Customizations, customizationPath, constant.BuildFolder);
    }

    getSourceTemporaryPath(constant, customizationPath) {
        // var inputFolderArr = this.getCustomizationFolderArray(customizationPath);
        // var intermediateCorePath = filePath.create(constant.Temp);
        // var pathCountry = intermediateCorePath.append(inputFolderArr[0]);
        // var pathClient = pathCountry.append(inputFolderArr[1]);
        var pathCore = filePath.create(constant.Temp).append(constant.Checkout);
        return pathCore;
    }

    getCoreTemporaryPath(constant, customizationPath) {
        // var inputFolderArr = this.getCustomizationFolderArray(customizationPath);
        // var intermediateCorePath = filePath.create(constant.Temp);
        // var pathCountry = intermediateCorePath.append(inputFolderArr[0]);
        // var pathClient = pathCountry.append(inputFolderArr[1]);
        var pathCore = filePath.create(constant.Temp).append(constant.CheckoutCorePath);
        return pathCore;
    }

    getCustomizationTemporaryPath(constant, customizationPath) {
        // var inputFolderArr = this.getCustomizationFolderArray(customizationPath);
        // var intermediateCorePath = filePath.create(constant.Temp);
        // var pathCountry = intermediateCorePath.append(inputFolderArr[0]);
        // var pathClient = pathCountry.append(inputFolderArr[1]);

        return filePath.create(constant.Temp).append(constant.CheckoutCustomizationPath);
    }

    getDefaultCustomizationTemporaryPath(constant, customizationPath) {
        var inputFolderArr = this.getCustomizationFolderArray(customizationPath);
        // var defaultFolderPath = format('{0}{1}', constant.Default, constant.Default);
        //
        // for (var i = 2; i < inputFolderArr.length; i++) {
        //     defaultFolderPath = format('{0}{1}{2}', defaultFolderPath, inputFolderArr[i], '/');
        // }
        //
        // var defaultCustomizationRelativePath = format('{0}{1}{2}{3}{4}{5}{6}{7}{8}', constant.BuildFolder, constant.Temp, inputFolderArr[0], '/', inputFolderArr[1], '/', constant.Checkout, constant.Customizations, defaultFolderPath);
        // return defaultCustomizationRelativePath;
        // var defaultFolderPath = format('{0}{1}', constant.Default, constant.Default);

        var defPath = filePath.create(constant.Temp, constant.Checkout, constant.Customizations, constant.Default, constant.Default);

        for (var i = 2; i < inputFolderArr.length; i++) {
            defPath = defPath.append(inputFolderArr[i]);
        }

        return defPath;
    }

    getDefaultCountryCustomizationTemporaryPath(constant, customizationPath) {
        var inputFolderArr = this.getCustomizationFolderArray(customizationPath);

        var defCountryPath = filePath.create(constant.Temp, constant.Checkout, constant.Customizations, inputFolderArr[0], constant.Default);

        // var defaultCountryFolderPath = format('{0}{1}{2}', inputFolderArr[0], '/', constant.Default);

        for (var j = 2; j < inputFolderArr.length; j++) {
            // defaultCountryFolderPath = format('{0}{1}{2}', defaultCountryFolderPath, inputFolderArr[j], '/');
            defCountryPath = defCountryPath.append(inputFolderArr[j]);
        }

        // var defaultCountryCustomizationRelativePath = format('{0}{1}{2}{3}{4}{5}{6}{7}{8}', constant.BuildFolder, constant.Temp, inputFolderArr[0], '/', inputFolderArr[1], '/', constant.Checkout, constant.Customizations, defaultCountryFolderPath);
        // return defaultCountryCustomizationRelativePath;

        return defCountryPath;
    }

    getClientCustomizationTemporaryPath(constant, customizationPath) {
        // var inputFolderArr = this.getCustomizationFolderArray(customizationPath);
        // var customizationRelativePath = format('{0}{1}{2}{3}{4}{5}{6}{7}{8}', constant.BuildFolder, constant.Temp, inputFolderArr[0], '/', inputFolderArr[1], '/', constant.Checkout, constant.Customizations, customizationPath);

        return filePath.create(constant.Temp, constant.Checkout, constant.Customizations, customizationPath);
    }

    getModuleFileTemporaryPath(constant, customizationPath) {
        return filePath.create(constant.Temp, constant.Checkout, constant.Customizations, customizationPath, 'module.json');
    }

    getCustomizationFolderArray(customizationPath) {
        return customizationPath.split('/');
    }

    getAppFolderPath(customizationDestinationPath) {
        var appFolderPath = filePath.create(customizationDestinationPath).append('application');
        return appFolderPath;
    }

    getVersionFilePath(appFolderPath) {
        var versionFilePath = filePath.create(appFolderPath).append('version.json');
        return versionFilePath;
    }

    getPackageJsonFilePath(appFolderPath) {
        var packageJSONFilePath = filePath.create(appFolderPath).append('package.json');
        return packageJSONFilePath;
    }

    getDistFolderPathLocal(customizationPath, updatedVersion) {
        var inputFolderArr = this.getCustomizationFolderArray(customizationPath);

        // var distFolderPath = format('{0}{1}{2}{3}{4}{5}{6}{7}', "../../../../../../../dist/",
        //     inputFolderArr[0], '/', inputFolderArr[1], '/', inputFolderArr[4], '/', updatedVersion);

        return filePath.create('dist', inputFolderArr[0], inputFolderArr[1], inputFolderArr[4], updatedVersion);
    }

    getDistFolderPathServerDesktop(customizationPath) {
        var inputFolderArr = this.getCustomizationFolderArray(customizationPath);
        var distFolderPath = format('{0}{1}{2}{3}{4}{5}{6}{7}{8}{9}', "E://Releases/",inputFolderArr[0],'/',
            inputFolderArr[1], '/', 'dev', '/', 'retail', '/', inputFolderArr[4]);
        return distFolderPath;
    }

    getDistFolderPathServerMobile(customizationPath) {
        var inputFolderArr = this.getCustomizationFolderArray(customizationPath);
        var distFolderPath = format('{0}{1}{2}{3}{4}{5}{6}{7}{8}{9}', "E://Releases/mobile/",inputFolderArr[0],'/',
            inputFolderArr[1], '/', 'dev', '/', 'retail', '/', inputFolderArr[4]);
        return distFolderPath;
    }

    getDistFolderPathServerTablet(customizationPath) {
        var inputFolderArr = this.getCustomizationFolderArray(customizationPath);
        var distFolderPath = format('{0}{1}{2}{3}{4}{5}{6}{7}{8}{9}', "E://Releases/tab/",inputFolderArr[0],'/',
            inputFolderArr[1], '/', 'dev', '/', 'retail', '/', inputFolderArr[4]);
        return distFolderPath;
    }

    getDistFolderPathServerDT(customizationPath) {
        var inputFolderArr = this.getCustomizationFolderArray(customizationPath);
        var distFolderPath = format('{0}{1}{2}{3}{4}{5}{6}{7}{8}{9}', "E://Releases/dt/",inputFolderArr[0],'/',
            inputFolderArr[1], '/', 'dev', '/', 'retail', '/', inputFolderArr[4]);
        return distFolderPath;
    }

    getSVNVersionFilePath(constant, customizationPath) {
        var customizationDestinationPath = format('{0}{1}{2}', constant.SvnCustomizationPath,'/', customizationPath);
        var versionFilePath = format('{0}{1}{2}{3}', customizationDestinationPath,'/', "application/","version.json");
        return versionFilePath;
    }

    getWidgetCustomizationPath (constant, customPath) {
        return format('{0}{1}', customPath, constant.Widgets)
    }
}

module.exports = new DirectoryPathHelper();
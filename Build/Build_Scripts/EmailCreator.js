class EmailCreator {

    constructor() {
    }

    createEmail(err) {
        try {
            var mailOptions = {
                from: 'no-reply@directfn.com', // sender address
                to: 'b.uditha@directfn.com,a.rasika@directfn.com,p.subodha@directfn.com', // list of receivers
                subject: 'Continuous Delivery Notification.', // Subject line
                text: 'Continuous Delivery Node Process Exit. Please check build Server.' + err   // plaintext body
            };

            return mailOptions;
        } catch (e) {
            // this.logger.logError(e + 'inside create Email');
        }
    }
}
module.exports = new EmailCreator();

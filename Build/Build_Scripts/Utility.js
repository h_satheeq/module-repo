var jsonFile = require('jsonfile');

class Utility {
    constructor() {

    }

    isMobileRelease(customizationPath) {
        var inputFolderArr = customizationPath.split('/');
        if(inputFolderArr && inputFolderArr.length > 2){
            if(inputFolderArr[2] === 'Mobile'){
                return true;
            }
        }
        return false;
    }

    isTabRelease(customizationPath) {
        var inputFolderArr = customizationPath.split('/');
        if (inputFolderArr && inputFolderArr.length > 2) {
            if (inputFolderArr[2] === 'Tab') {
                return true;
            }
        }
        return false;
    }

    isDTRelease(customizationPath) {
        var inputFolderArr = customizationPath.split('/');
        if (inputFolderArr && inputFolderArr.length > 3) {
            if (inputFolderArr[3] === 'DT') {
                return true;
            }
        }
        return false;
    }

    getModuleArray(moduleFilePath) {
        if (moduleFilePath.exists()) {
            console.log('Module path exists : ' + moduleFilePath.toString());
            var moduleArray = jsonFile.readFileSync(moduleFilePath.toString());
            return moduleArray;
        }
    }

    getPreCopyModuleArray(moduleFilePath) {
        if (moduleFilePath.exists()) {
            console.log('Module path exists : ' + moduleFilePath.toString());
            var moduleArray = jsonFile.readFileSync(moduleFilePath.toString());

            for (var module in moduleArray) {
                if (moduleArray.hasOwnProperty(module)) {

                    var preCopyModuleArray = [];

                    if (module == 'preCopy') {
                        for (var line in moduleArray[module]) {
                            preCopyModuleArray[preCopyModuleArray.length] = moduleArray[module][line];
                        }

                        return preCopyModuleArray;
                    }
                }
            }
        }
    }

    getPostCopyModuleArray(moduleFilePath) {
        if (moduleFilePath.exists()) {
            console.log('Module path exists : ' + moduleFilePath.toString());
            var moduleArray = jsonFile.readFileSync(moduleFilePath.toString());

            for (var module in moduleArray) {
                if (moduleArray.hasOwnProperty(module)) {

                    var postCopyModuleArray = [];

                    if (module == 'postCopy') {
                        for (var line in moduleArray[module]) {
                            postCopyModuleArray[postCopyModuleArray.length] = moduleArray[module][line];
                        }

                        return postCopyModuleArray;
                    }
                }
            }
        }
    }

    getModuleArray(moduleFilePath) {
        if (moduleFilePath.exists()) {
            console.log('Module path exists : ' + moduleFilePath.toString());
            var moduleArray = jsonFile.readFileSync(moduleFilePath.toString());
            return moduleArray;
        }
    }

    getSplitByForwardSlash(stringToSplit) {
        try {
            if (stringToSplit) {
                var strArr = stringToSplit.split('/');
                return strArr;
            }
        } catch (e) {
            console.log(e);
        }
    }

    appendPropertiesToObject(constant, objectWithProperties) {
        for (var propertyName in objectWithProperties) {
            if (objectWithProperties.hasOwnProperty(propertyName)) {
                constant[propertyName] = objectWithProperties[propertyName];
            }
        }

        return constant;
    }

    appendKeyValuePairToObject(constant, key, value) {
        constant[key] = value;
        return constant;
    }

    getJsonString(object) {
        var jsonStr = JSON.stringify(object);
        return jsonStr;
    }

    getJsonObject(jsonStr) {
        var object = JSON.parse(jsonStr);
        return object;
    }

    readJsonFile(filePath) {
        var obj = jsonFile.readFileSync(filePath.toString());
        var userName = obj.username;
        var password = obj.password;
    }
}
module.exports = new Utility();
var fs = require('fs');
var fse = require('nodejs-fs-utils');
var filepath = require('filepath');
var fsex = require('fs-extra');
var p = require('path');
var jsonFile = require('jsonfile');
var pathExists = require('path-exists');
var format = require('string-format');
var constants = require('./constants.json');

class FileCopy {
    constructor() {

    }

    copyFolder(scrPath, destinationPath) {
        this._copySrcToDestination(filepath.create(scrPath), filepath.create(destinationPath));
    }

    copyCustomizations(destination, customizationPath, isWindows) {
        var destinationPath = destination;
        var path = filepath.create().relative(customizationPath.toString());
        console.log('Customizations relative path : ' + path.toString());
        var baseName = filepath.create(customizationPath).basename();
        var folderArr = path.split('\\');

        var prevPath = filepath.create(folderArr[0]);
        prevPath = prevPath.append(folderArr[1]);
        prevPath = prevPath.append(folderArr[2]);
        prevPath = prevPath.append(folderArr[3]);
        console.log("Previous path : " + prevPath.toString());

        for (var i = 4; i < folderArr.length; i++) {
            var currentPath = prevPath.append(folderArr[i]);
            console.log(currentPath.toString() + " Current path i =" + i);
            var defPath = currentPath.append('Default');
            console.log(defPath.toString() + " i =" + i);

            defPath = filepath.create(defPath);

            if (defPath.exists()) {
                console.log('File path exists inside defpath.' + defPath.toString());
                if (baseName.toString() === 'Price') {
                    var kernalPath = defPath.append('Kernel');
                    if(kernalPath.exists()) {
                        this._copySrcToDestination(kernalPath, destinationPath);
                        console.log('Kernel files copied. path : ' + kernalPath.toString());
                    }
                    console.log(destinationPath);
                    defPath = defPath.append('Price');

                    if (defPath.exists()) {
                        console.log('Folder Exists : ' + defPath.toString());
                        this._copySrcToDestination(defPath, destinationPath);
                    } else {
                        console.log('Folder does not Exists : ' + defPath.toString());
                    }
                } else {
                    var kernalPath2 = defPath.append('Kernel');
                    var pricePath = defPath.append('Price');
                    var tradePath = defPath.append('Trade');

                    if (kernalPath2.exists()) {
                        console.log('KernelPath2 Folder Exists : ' + kernalPath2.toString());
                        this._copySrcToDestination(kernalPath2, destinationPath.append(constants.KernelBuild));
                    } else {
                        console.log('KernelPath2 Folder does not Exists : ' + kernalPath2.toString());
                    }

                    if (pricePath.exists()) {
                        console.log('Price Folder Exists : ' + pricePath.toString());
                        this._copySrcToDestination(pricePath, destinationPath.append(constants.PriceBuild));
                    } else {
                        console.log('Price Folder does not Exists : ' + pricePath.toString());
                    }

                    if (tradePath.exists()) {
                        console.log('Trade Folder Exists : ' + tradePath.toString());
                        this._copySrcToDestination(tradePath, destinationPath.append(constants.TradeBuild));
                    } else {
                        console.log('Trade Folder does not Exists : ' + tradePath.toString());
                    }

                    console.log("Destination path :" + destinationPath);
                }
            } else {
                var currentFolderName = currentPath.basename();
                var currentFolderStr = currentFolderName.toString().replace(/ /g, '');
                var baseNameStr = baseName.toString().replace(/ /g, '');

                var kernalPath = prevPath.append('Kernel');

                if (currentFolderStr === baseNameStr) {
                    console.log(currentPath.toString());
                    currentPath = filepath.create(currentPath);

                    if(kernalPath.exists()) {
                        console.log('Kernel Path :' +kernalPath);
                        this._copySrcToDestination(kernalPath, destinationPath.append(constants.KernelBuild));
                    }

                    if (currentPath.exists()) {
                        if (baseName.toString() === 'Price') {
                            console.log(destinationPath);
                            this._copySrcToDestination(currentPath, destinationPath.append(constants.PriceBuild));
                        } else {
                            var pricePath = prevPath.append('Price');

                            // Copy only application folder as this may contain Test related files.
                            if(pricePath.exists()) {
                                this._copySrcToDestination(pricePath, destinationPath.append(constants.PriceBuild));
                            }

                            this._copySrcToDestination(currentPath, destinationPath.append(constants.TradeBuild));
                        }
                    }
                }
            }

            prevPath = currentPath;
        }
    }

    createFolder(filePath) {
        var path = filepath.create(filePath);

        if (!path.exists()) {
            path.mkdir();
        }
    }

    emptyDestinationFolder(fp, isWindows) {
        this.getFilesInDir(fp.toString()).forEach(function (path) {
            var fPath = fp.append(path).toString();

            if (fPath.match(/\\node_modules/) || fPath.match(/\\bower_components/) || fPath.match(/\\.git/) || fPath.match(/\\tmp/)) {
            } else {
                fse.rmdirsSync(fPath);
            }
        });
    }

    getFilesInDir(folderPath) {
        return fs.readdirSync(folderPath);
    }

    removeGITFiles(folderPath, isWindows) {
        fse.walkSync(folderPath, {
            skipErrors: true,
            logErrors: true
        }, function (err, path, stats, next, cache) {
            if (!err) {
                if (stats.isDirectory()) {
                    if (path.match(/\\.git/)) {
                        fse.rmdirsSync(path);
                    }
                }

                next();
            } else {
                next();
            }
        });
    }

    copyFiles(srcPath, destinationPath) {
        try {
            fsex.copySync(srcPath, destinationPath);
            console.log('File Copy Success');
        } catch (e) {
            console.log(e);
        }
    }

    readFileSync(filePath) {
        try {
            var content = fs.readFileSync(filePath, 'utf8');
            return content;
        } catch (e) {
            console.log('File read failed :' + e);
        }
    }

    writeFileSync(filePath, content) {
        try {
            fs.writeFileSync(filePath, content, 'utf8');
        } catch (e) {
            console.log('File write failed. ' + filePath);
            console.log(e);
        }
    }

    copyModuleFiles(moduleArray, customizationsTempCheckoutPath, customizationDestinationPath, isMobileRelease) {
        if (moduleArray && moduleArray.length > 0) {
            console.log('Module array length : ' + moduleArray.length);

            for (var k = 0; k < moduleArray.length;     k++) {
                var moduleFile = moduleArray[k];
                var sourceFolderPathDefault = customizationsTempCheckoutPath.append('Widgets', 'Default', moduleFile.path).toString();

                // console.log('Module path : ' + sourceFolderPathDefault);

                if (pathExists.sync(sourceFolderPathDefault)) {
                    this.copyFolder(sourceFolderPathDefault, customizationDestinationPath.append(moduleFile.moduleName).toString());
                }
            }

            if (isMobileRelease) {
                for (var l = 0; l < moduleArray.length; l++) {
                    var modulePathMobile = moduleArray[l];
                    var sourceFolderPathMobile = customizationsTempCheckoutPath.append('Widgets', 'Mobile', modulePathMobile.path).toString();

                    console.log('Mobile Module path : ' + sourceFolderPathMobile);

                    if (pathExists.sync(sourceFolderPathMobile)) {
                        this.copyFolder(sourceFolderPathMobile, customizationDestinationPath.append(modulePathMobile.moduleName).toString());
                    }
                }
            }
        } else {
            console.log('Module Array Not received.');
        }
    }

    _copySrcToDestination(scrPath, destinationPath) {
        scrPath = scrPath.append(constants.Application);
        var from = scrPath.toString();
        var to = destinationPath.toString();

        if (scrPath.exists() && !fse.isEmptySync(from)) {
            this.copy(from, to);
        } else {
            console.log(from.toString() + ' Folder empty');
        }
    }

    copy(from, to) {
        fsex.copySync(from, to);
        console.log(from + ' Copied to ' + to);
    }

    moveBuildFilesToKernel(destinationPath) {
        var that = this;
        var kernel = destinationPath.append(constants.KernelBuild);
        var lookupPath = this.getModuleDir(destinationPath);

        var pathsToMove = [
            'app/config/extended-settings-level-1.js',
            'app/config/extended-settings-level-2.js',
            'app/config/field-meta-config.js',
            'app/config/language-extended.js',
            'app/config/layout-config.js',
            'app/config/layout-config-secondary.js',
            'app/config/module-initializer-config.js',
            'app/index.html',
            'app/styles',
            'public',
            'vendor',
            'package.json',
            'bower.json',
            'version.json',
            'Brocfile.js',
            'config'
        ];

        console.log('Moving kernel build files from modules', pathsToMove.toString());

        lookupPath.forEach(function (path) {
            pathsToMove.forEach(function (files) {
                var from = destinationPath.append(path, files);

                if (from.exists()) {
                    var fromStr = from.toString();

                    // Move permission not supported in every scenario, so copy and remove
                    fsex.copySync(fromStr, kernel.append(files).toString());
                    fsex.removeSync(fromStr);
                }
            });
        });
    }

    applyAddonPatch(destinationPath) {
        var that = this;
        var addonProtoPath = filepath.create(constants.AddonPrototype);

        console.log('Addon prototype path', addonProtoPath.toString());

        var lookupPath = this.getModuleDir(destinationPath);

        lookupPath.forEach(function (moduleName) {
            var addonPath = destinationPath.append(moduleName);
            console.log('Copying addon prototype to:', addonPath.toString());
            fsex.copySync(addonProtoPath.toString(), addonPath.toString());

            constants.AddonPatchFiles.forEach(function (file) {
                // Apply addon name
                that._updateModuleName(addonPath.append(file).toString(), moduleName);
            });
        });
    }

    getModuleDir(destinationPath) {
        return fs.readdirSync(destinationPath.toString()).filter(function (file) {
            return fs.statSync(destinationPath.append(file).toString()).isDirectory() && file !== constants.KernelBuild
                && file !== '.idea' && file !== '.git' && file !== 'node_modules' && file !== 'bower_components' && file.startsWith('ua');
        });
    }

    _updateModuleName(filePath, moduleName) {
        var content = fs.readFileSync(filePath, 'utf8');

        content = content.replace('{{ua-addon-name}}', moduleName);
        fs.writeFileSync(filePath, content);
    }
}
module.exports = new FileCopy();
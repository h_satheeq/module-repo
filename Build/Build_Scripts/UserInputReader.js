var filePath = require('filepath');
var promt = require('prompt-sync')();
var jsonFile = require('jsonfile');
var format = require('string-format');

class UserInputReader {
    constructor() {
    }

    getSvnInfo(constant) {
        var svnFilePath = filePath.create(constant.SvnInfoFilePath);

        if (svnFilePath.exists()) {
            console.log('svn info file exists');
        } else {
            var usr = promt('enter svn user name: ');
            console.log(usr);
            var pwd = promt('enter svn password: ');
            console.log(pwd);

            console.log('Writing to a svn info file');
            jsonFile.writeFileSync(svnFilePath.toString(), {username: usr, password: pwd});
        }

        var svnFileObj = jsonFile.readFileSync(svnFilePath.toString());
        return svnFileObj;
    }

    getVersionType(isAutomatedPath, releaseType) {
        // if (isAutomatedPath === 'false') {
        //     if (releaseType === 'production') {
        //         var versionType = promt('Enter version type : (1 - major, 2 - minor, 3 - current, 4 - patch / build)');
        //         return versionType;
        //     }
        // }

        return '4';
    }

    getVersionNumber() {
        var newVersion = promt('Please enter new version : major.minor.patch : ');
        console.log('You entered version : ' + newVersion);
        return newVersion;
    }

    readUpdatedVersionNumber(versionFilePath) {
        var updatedVersion = null;
        var versionObj = jsonFile.readFileSync(versionFilePath.toString());
        console.log('App version : ' + versionObj.appVersion.toString());
        if(versionObj.appVersion) {
            updatedVersion = versionObj.appVersion.toString();
        }

        return updatedVersion;
    }

    getVersionNumberFromVersionFile(versionFilePath, versionType) {
        var versionObj = jsonFile.readFileSync(versionFilePath.toString());
        console.log('App version : ' + versionObj.appVersion.toString());
        var versionItemArrayWithPrefix = versionObj.appVersion.toString().split('_');
        var versionItemArray = versionItemArrayWithPrefix[3].split('.');
        console.log('Version Type: ' + versionType);
        var updatedVersion = '';

        switch (versionType) {
            case '1':
                versionItemArray[0] = parseInt(versionItemArray[0]) + 1;
                versionItemArray[1] = "000";
                versionItemArray[2] = "000";
                versionItemArray[3] = 0;
                break;

            case '2':
                versionItemArray[1] = parseInt(versionItemArray[1]) + 1;
                if(versionItemArray[1].toString().length == 1) {
                    versionItemArray[1] = format('{0}{1}',"00",versionItemArray[1]);
                } else if (versionItemArray[1].toString().length == 2) {
                    versionItemArray[1] = format('{0}{1}',"0",versionItemArray[1]);
                }
                versionItemArray[2] = "000";
                versionItemArray[3] = 0;
                break;

            case '3':
                versionItemArray[2] = parseInt(versionItemArray[2]) + 1;
                if(versionItemArray[2].toString().length == 1) {
                    versionItemArray[2] = format('{0}{1}',"00",versionItemArray[2]);
                } else if (versionItemArray[2].toString().length == 2) {
                    versionItemArray[2] = format('{0}{1}',"0",versionItemArray[2]);
                }
                versionItemArray[3] = 0;

                break;
            case '4':
                versionItemArray[3] = parseInt(versionItemArray[3]) + 1;
                break;

            default:
                versionItemArray[3] = parseInt(versionItemArray[3]) + 1;
                console.log('version updated. consider as a patch release.');
        }

        updatedVersion = format('{0}_{1}_{2}_{3}.{4}.{5}.{6}',versionItemArrayWithPrefix[0], versionItemArrayWithPrefix[1],
            versionItemArrayWithPrefix[2], versionItemArray[0], versionItemArray[1], versionItemArray[2], versionItemArray[3]);
        console.log('Updated version: ' + updatedVersion.toString());
        return updatedVersion;
    }

    getUpdatedBambooVersionNumber(versionFilePath, bambooBuildNumber) {
        var versionObj = jsonFile.readFileSync(versionFilePath.toString());
        console.log('App version : ' + versionObj.appVersion.toString());
        var versionItemArrayWithPrefix = versionObj.appVersion.toString().split('_');
        var versionItemArray = versionItemArrayWithPrefix[3].split('.');
        var updatedVersion = '';
        versionItemArray[3] = bambooBuildNumber;
        updatedVersion = format('{0}_{1}_{2}_{3}.{4}.{5}.{6}',versionItemArrayWithPrefix[0], versionItemArrayWithPrefix[1],
            versionItemArrayWithPrefix[2], versionItemArray[0], versionItemArray[1], versionItemArray[2], versionItemArray[3]);
        console.log('Updated version : ' + updatedVersion.toString());
        return updatedVersion;
    }

    updateReleaseVersionNumber(releaseVersion, bambooBuildNumber) {
        console.log('App version : ' + releaseVersion.toString());
        var versionItemArrayWithPrefix = releaseVersion.toString().split('_');
        var versionItemArray = versionItemArrayWithPrefix[3].split('.');
        versionItemArray[3] = bambooBuildNumber;
        var updatedVersion = format('{0}_{1}_{2}_{3}.{4}.{5}.{6}',versionItemArrayWithPrefix[0], versionItemArrayWithPrefix[1],
            versionItemArrayWithPrefix[2], versionItemArray[0], versionItemArray[1], versionItemArray[2], versionItemArray[3]);
        console.log('Updated version : ' + updatedVersion.toString());
        return updatedVersion;
    }

    updateSemanticVersion(packageJsonFilePath) {
        var packageJSONObj = jsonFile.readFileSync(packageJsonFilePath.toString());
        packageJSONObj.version = '1.0.1';
        jsonFile.writeFileSync(packageJsonFilePath.toString(), packageJSONObj);
    }

    updateVersion(updatedVersion, versionFilePath, packageJsonFilePath) {
        var packageJSONObj = jsonFile.readFileSync(packageJsonFilePath.toString());
        packageJSONObj.version = updatedVersion;
        jsonFile.writeFileSync(packageJsonFilePath.toString(), packageJSONObj);
        jsonFile.writeFileSync(versionFilePath.toString(), {appVersion: updatedVersion});
    }
}
module.exports = new UserInputReader();
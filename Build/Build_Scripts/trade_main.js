var svnaccess = require('./SvnAccess');
var fse = require('nodejs-fs-utils');
var existsFile = require('exists-file');
var jsonfile = require('jsonfile');
var filepath = require('filepath');
var fs = require('fs');
var promt = require('prompt-sync')();
var wait = require('wait.for');
var svnUltimate = require('node-svn-ultimate');
var fileCopy = require('./FileCopy');
var format = require('string-format');
var cmd=require('node-cmd');
var buildAppTask = require('./BuildTask');
var childProcess = require('child_process');

function main(){

    var constFilePath = filepath.create('Build_Scripts/constants.json');

    if (process.argv.length <= 1) {
        console.log('Input arguments not received.');
        process.exit(-1);
    }

    var constant = '';
    if(constFilePath.exists()) {
        constant = jsonfile.readFileSync(constFilePath.toString());
    } else {
        console.log('Constants file not available.');
        process.exit(-1);
    }
    debugger;

    var customizationPath = process.argv[2];
    var customizationDestinationPath = format('{0}{1}{2}','../',constant.Customizations, customizationPath);
    console.log('Customization Destination Path : ' +customizationDestinationPath);

    var customizationRelativePath = format('{0}{1}{2}{3}{4}{5}{6}{7}{8}',constant.BuildFolder,constant.Temp,'Default','/','Trade','/', constant.Checkout,constant.Customizations, customizationPath);
    console.log('Customization Relative Path : ' + customizationRelativePath);
    debugger;

    var svnFilePath = filepath.create(constant.SvnInfoFilePath);
    //Read svn info file
    var obj = jsonfile.readFileSync(svnFilePath.toString());
    var userName = obj.username;
    var password = obj.password;
    console.log(userName);
    console.log(password);
    debugger;

    var intermediateCorePath = filepath.create('temp');
    var pathCountry = intermediateCorePath.append('Default');
    var pathClient = pathCountry.append('Trade');
    var pathCore = pathClient.append(constant.CheckoutCorePath);
    console.log(pathCore.toString());
    if(!pathCore.exists()){
        filepath.create(pathCore).mkdir();
        debugger;
    }
    console.log(pathCore.toString());
    //Removing content of the core and customizations files.
    console.log('removing content of the core.');
    fse.emptyDirSync(pathCore.toString());
    console.log('content of the core removed.');
    var pathCheckoutCustomization = pathClient.append(constant.CheckoutCustomizationPath);
    fileCopy.createFolder(pathCheckoutCustomization.toString());
    console.log('removing content of the customizations.');
    fse.emptyDirSync(pathCheckoutCustomization.toString());
    console.log('content of the customizations removed.');
    fileCopy.createFolder(customizationDestinationPath);
    debugger;

    var appFolderPath = filepath.create(customizationDestinationPath).append('application');
    console.log('removing content of the ' + appFolderPath.toString());
    if(appFolderPath.exists()) {
        fileCopy.emptyDestinationFolder(appFolderPath.toString());
    }
    debugger;

    //Downloading core files and copying those to correct customization folder.
    console.log('Downloading core');
    var result = wait.for(svnaccess.getSvnSourceCore,constant.SvnCorePath, pathCore.toString(), userName, password);
    console.log('Download core completed.' + result);

    console.log('Downloading customizations.');
    wait.for(svnaccess.getSvnSourceCustomizations, constant.SvnCustomizationPath, pathCheckoutCustomization.toString(), userName, password);
    console.log('Download customizations completed.');

    console.log('Removing SVN Files in core checkout location.');
    fileCopy.removeSVNFiles(pathCore.toString());
    console.log('Remove SVN Files Completed.');

    console.log('Removing SVN Files in customization checkout location.');
    fileCopy.removeSVNFiles(pathCheckoutCustomization.toString());
    console.log('Remove SVN Files Completed.');

    console.log('Copying core');
    fileCopy.copyFolder(pathCore, customizationDestinationPath);
    console.log('Copying core completed.');

    console.log('Copying default customizations.');
    fileCopy.copyCustomizations(customizationDestinationPath, customizationRelativePath);
    console.log('Copy default customizations completed.');
    debugger;

    //childProcess.exec('start "" "C:\\Test"');

    var appPathDev = filepath.create(customizationDestinationPath).append('application').toString();
    appPathDev = appPathDev.replace(/\\/g, "\\\\");

    console.log('App  path : ' + appPathDev);
    childProcess.exec('start "" "'+ appPathDev +'"');

    console.log('Setting up development environment completed.');
}

function getUserData(){
    //var SVN_INFO_FILE_PATH = 'Build_Scripts/Core/svninfo.json';
    var svnFilePath = filepath.create('Build_Scripts/svninfo.json');
    var releaseType = 'production';

      //check for svn info file.
    if(svnFilePath.exists()){
        console.log('svn info file exists');
    } else {
        var usr = promt('enter svn user name: ');
        console.log(usr);
        var pwd = promt('enter svn password: ');
        console.log(pwd);

        //write user name and password to svn info file.
        console.log('Writing to a svn infofile');
        jsonfile.writeFileSync(svnFilePath.toString(), {username: usr, password: pwd});
    }
    //console.log('You entered version type : ' + versionType);
    if(releaseType === 'production') {
        console.log('app start');
        wait.launchFiber(main);
        console.log('after launch');
    } else {
        console.log('app start');
        wait.launchFiber(main);
        console.log('after launch');
    }
}

getUserData();

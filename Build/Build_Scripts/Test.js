var buildAppTask = require('./BuildTask');
var wait = require('wait.for');
var fileCopy = require('./FileCopy');

class Test {
    constructor() {
    }

    execute() {
        process.chdir("D:\\Projects\\Scripts_Test\\trunk\\Technical_Solution\\Source\\Customizations\\SriLanka\\CSE\\Desktop\\Netplus\\Trade\\application");
        console.log('Ember CLI build started.');
        wait.for(buildAppTask.buildApp, 'aaaaaa');
        console.log('Ember CLI build completed.');

        var distCopyFolder = '../../Release';
        fileCopy.createFolder(distCopyFolder);

        console.log('Copying files to the destination.');
        fileCopy.copyFolder('dist', distCopyFolder);
        console.log('Copy files to the destination completed.');
    }
}
const test = new Test();
wait.launchFiber(test.execute);
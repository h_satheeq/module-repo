var fse = require('nodejs-fs-utils');
var filepath = require('filepath');
var fsex = require('fs-extra');
var p = require('path');
var fs = require('fs');

function copySrcToDestination() {

    var scrPath = null;
    var destinationPath = null;

    if (process.argv.length > 3) {
        scrPath = process.argv[2];
        destinationPath = process.argv[3];
    }
	
	console.log(scrPath);
	console.log(destinationPath);
	
	scrPath = scrPath.toLowerCase();
	destinationPath = destinationPath.toLowerCase();
	
	var path = filepath.create(destinationPath);
	
	console.log(path);

    if(!path.exists()) {
        path.mkdir();
    }

    if (!fse.isEmptySync(scrPath.toString())) {
        fsex.copySync(scrPath, destinationPath);
        console.log(scrPath + ' Copied to ' + destinationPath);
    } else {
        console.log(scrPath.toString() + ' Folder empty');
    }
};

copySrcToDestination();

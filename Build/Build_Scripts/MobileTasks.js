var fileCopy = require('./FileCopy');
var fs = require('fs');
const cheerio = require('cheerio');

class MobileTasks {

    constructor() {

    }

    update(kernelFolder) {
        console.log('File copy hits.');
        fileCopy.copyFiles(kernelFolder.append('app', 'index-native.html').toString(), kernelFolder.append('dist', 'index-native.html').toString());
        console.log('copy completed.');

        var files = fileCopy.getFilesInDir(kernelFolder.append('dist', 'assets').toString());

        for (var i in  files) {
            console.log(files[i]);
            if (files[i].startsWith('universal')) {
                var universalAppFileName = files[i];
                var withOutExt = universalAppFileName.split('.');

                if (withOutExt[0]) {
                    var splitWithHyphen = withOutExt[0].split('-');
                    if (splitWithHyphen[2]) {
                        var timeStamp = splitWithHyphen[2];

                        if (timeStamp) {
                            console.log(timeStamp);
                            var indexNative = kernelFolder.append('dist', 'index-native.html').toString();
                            var fileContent = fileCopy.readFileSync(indexNative);
                            var results = fileContent.replace(/{{TIMESTAMP}}/g, timeStamp);
                            fileCopy.writeFileSync(indexNative, results);
                            console.log('Hast patch applied to index-native.html successfully.');
                            break;
                        } else {
                            console.log('Time stamp part missing. Check weather is this a production build.')
                        }
                    }
                }
            }
        }
    }

    updateMetaContent(kernelFolder) {
        var content = fs.readFileSync(kernelFolder.append('dist', 'index.html').toString(), 'utf8');
        var indexNativeFile = kernelFolder.append('dist', 'index-native.html').toString();
        const $ = cheerio.load(content);

        $('meta').each(function(i, elem) {
            if($(this).attr('name') == "universal-app/config/environment") {
                var nativeContent = fs.readFileSync(indexNativeFile, 'utf8');
                var results = nativeContent.replace(/{{META-CONTENT}}/g, $(this).attr('content'));
                var success = fs.writeFileSync(indexNativeFile, results);

                console.log($(this).attr('content'));
                console.log('Update meta content successfully.');
            }
        });
    }
}

module.exports = new MobileTasks();

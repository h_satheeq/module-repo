var cmd = require('node-cmd');
var wait = require('wait.for');

class ServerRemoteSupportAgent {
    constructor() {

    }

    installBuildDependencies(workingFolder, callback) {
        cmd.get(`npm install`,
            function(data){
                console.log('Step 1 completed. : ',data);
                callback(null);
            }
        );
    }

    prepareTestEnvironment(customizationPath, callback) {
        cmd.get(`node Build_Scripts/BuildMain.js ` + customizationPath +` dev true`,
            function(data){
                console.log('Step 2 completed. : ',data);
                callback(null);
            }
        );
    }

    installTestDependencies (workingFolder, callback) {
        cmd.get(`call "../Test/Install.bat"`,
            function(data){
                console.log('Step 3 completed. : ',data);
                callback(null);
            }
        );
    }

    executeTest(testPath, isSmokeTest, callback) {
        cmd.get(`START /wait "demo" CMD  /K "node ../Test/TestMain.js" ` +testPath + ` ` + isSmokeTest,
            function(data){
                console.log('Step 4 completed. : ',data);
                callback(null);
            }
        );
    }
}
module.exports = new ServerRemoteSupportAgent();

var jsonFile = require('jsonfile');
var filePath = require('filepath');
var directoryPathHelper = require('../DirectoryPathHelper');
var fse = require('nodejs-fs-utils');
var cmd = require('node-cmd');
var wait = require('wait.for');
var userInputReader = require('../UserInputReader');
var gitAccess = require('../GitAccess');
var pathExists = require('path-exists');
var utility = require('../Utility');
var updateAppCache = require('../UpdateAppCache');
var mobileTask = require('../MobileTasks');

class PostBuild {
    constructor() {

    }

    execute() {
        //Process Command line args - which is passed from bat.
        var customizationPath = null;
        var versionValue = null;
        var releaseVersionNumber = null;

        if (process.argv.length < 2) {
            console.log('Input arguments not received.');
            process.exit(-1);
        } else {
            customizationPath = process.argv[2];
            console.log("Customization path :" + customizationPath);

            if(process.argv[3]) {
                releaseVersionNumber = process.argv[3];
                console.log("releaseVersionNumber :" + releaseVersionNumber);
            }
        }

        var constFilePath = filePath.create('Build_Scripts/constants.json');
        var constants = null;

        if (constFilePath.exists()) {
            constants = jsonFile.readFileSync(constFilePath.toString());
        } else {
            console.log('Constants file not available in path : ' + constFilePath.toString());
            process.exit(-1);
        }

        //Create Required paths folders
        var customizationDestinationPath = directoryPathHelper.getCustomizationDestinationPath(constants, customizationPath);
        var appFolderPath = directoryPathHelper.getAppFolderPath(customizationDestinationPath);

        var isMobileRelease = utility.isMobileRelease(customizationPath);
        var isTabRelease = utility.isTabRelease(customizationPath);

        process.chdir(appFolderPath.toString());

        var versionFilePath = './version.json';

        if (versionValue == null) {
            if (pathExists.sync(versionFilePath)) {
                versionValue = userInputReader.readUpdatedVersionNumber(versionFilePath);
            }
        }

        console.log('Version file check in started.');
        wait.for(gitAccess.addVersionFile, versionFilePath.toString());
        wait.for(gitAccess.commitVersionFile, versionFilePath.toString());
        wait.for(gitAccess.pullBeforeVersionPush, versionFilePath.toString());
        wait.for(gitAccess.pushVersion, versionFilePath.toString());
        console.log('Version file check in completed.');

        if(releaseVersionNumber != null) {
            console.log('Add tag to Bitbucket started.');
            wait.for(gitAccess.addTag, versionValue);
            wait.for(gitAccess.pullBeforeVersionPush, versionValue);
            wait.for(gitAccess.pushTags, versionValue);
            console.log('Tagging completed.');
        }

        if (isMobileRelease || isTabRelease) {
            mobileTask.update();
            mobileTask.updateMetaContent();
        }

        updateAppCache.update(versionValue);

        console.log('Post build tasks completed.');
    }
}
const postBuild = new PostBuild();
wait.launchFiber(postBuild.execute);

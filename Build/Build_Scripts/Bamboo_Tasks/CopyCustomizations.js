var jsonFile = require('jsonfile');
var filePath = require('filepath');
var directoryPathHelper = require('../DirectoryPathHelper');
var fse = require('nodejs-fs-utils');
var cmd = require('node-cmd');
var wait = require('wait.for');
var userInputReader = require('../UserInputReader');
var gitAccess = require('../GitAccess');
var fileCopy = require('../FileCopy');
var utility = require('../Utility');
var updateModuleInitializer = require('../UpdateModuleInitializer');

class CopyCustomizations {
    constructor() {

    } 

    execute() {
        //Process Command line args - which is passed from bat.
        var customizationPath = null;
        var releaseType = 'production';
        var isAutomatedPath = 'false';

        if (process.argv.length < 2) {
            console.log('Input arguments not received.');
            process.exit(-1);
        } else {
            customizationPath = process.argv[2];
            console.log("Customization path :" + customizationPath);
        }

        var constFilePath = filePath.create('Build_Scripts/constants.json');
        var constants = null;

        if (constFilePath.exists()) {
            constants = jsonFile.readFileSync(constFilePath.toString());
        } else {
            console.log('Constants file not available in path : ' +constFilePath.toString());
            process.exit(-1);
        }

        //Create Required paths folders
        var customizationDestinationPath = directoryPathHelper.getCustomizationDestinationPath(constants, customizationPath);
        var coreTempCheckoutPath = directoryPathHelper.getCoreTemporaryPath(constants, customizationPath);
        var customizationsTempCheckoutPath = directoryPathHelper.getCustomizationTemporaryPath(constants, customizationPath);
        var sourceTempCheckoutPath = directoryPathHelper.getSourceTemporaryPath(constants, customizationPath);
        var defaultCustomizationTempPath = directoryPathHelper.getDefaultCustomizationTemporaryPath(constants, customizationPath);
        var defaultCountryCustomizationTempPath = directoryPathHelper.getDefaultCountryCustomizationTemporaryPath(constants, customizationPath);
        var clientCustomizationTempPath = directoryPathHelper.getClientCustomizationTemporaryPath(constants, customizationPath);

        if (!sourceTempCheckoutPath.exists()) {
            filePath.create(sourceTempCheckoutPath).mkdir();
        }

        console.log('removing content of the core temp path.');
        fse.emptyDirSync(sourceTempCheckoutPath.toString());
        console.log('content of the core temp path removed.');

        //Get latest
        console.log('Downloading Source.');
        wait.for(gitAccess.getLatest, sourceTempCheckoutPath.toString());
        console.log('Download Source completed.');

        //Remove SVN files
        console.log('Removing .git files in core checkout location.');
        fileCopy.removeGITFiles(coreTempCheckoutPath.toString());
        console.log('Remove .git files Completed.');

        console.log('Removing .git files in customization checkout location.');
        fileCopy.removeGITFiles(customizationsTempCheckoutPath.toString());
        console.log('Remove .git files Completed.');

        //Removing Customization Destination Folder.
        fileCopy.createFolder(customizationDestinationPath);

        var appFolderPath = directoryPathHelper.getAppFolderPath(customizationDestinationPath);

        if (appFolderPath.exists()) {
            console.log('removing content of the ' + appFolderPath.toString());
            fileCopy.emptyDestinationFolder(appFolderPath.toString());
        }

        //Copying core
        console.log('Copying core');
        fileCopy.copyFolder(coreTempCheckoutPath.toString(), customizationDestinationPath);
        console.log('Copying core completed.');

        //Copying modules
        var moduleFolderTempPath = directoryPathHelper.getModuleTemporaryPath(constants, customizationPath);
        var moduleFileTempPath = directoryPathHelper.getModuleFileTemporaryPath(moduleFolderTempPath);
        var isMobileRelease = utility.isMobileRelease(customizationPath);
        var moduleArray = utility.getPreCopyModuleArray(moduleFileTempPath);

        fileCopy.copyModuleFiles(moduleArray, customizationsTempCheckoutPath, customizationDestinationPath, isMobileRelease);

        console.log('Copying modules completed.');

        //Copying customizations
        console.log('Copying default customizations.');
        fileCopy.copyCustomizations(customizationDestinationPath, defaultCustomizationTempPath);
        console.log('Copy default customizations completed.');

        console.log('Copying default country customizations.');
        fileCopy.copyCustomizations(customizationDestinationPath, defaultCountryCustomizationTempPath);
        console.log('Copy default country customizations completed.');

        console.log('Copying customizations.');
        fileCopy.copyCustomizations(customizationDestinationPath, clientCustomizationTempPath);
        console.log('Copy customizations completed.');

        var postCopyModuleArray = utility.getPostCopyModuleArray(moduleFileTempPath);

        fileCopy.copyModuleFiles(postCopyModuleArray, customizationsTempCheckoutPath, customizationDestinationPath, isMobileRelease);

        if(moduleArray && moduleArray.length > 0) {
            updateModuleInitializer.update(moduleArray, customizationDestinationPath);
        }

        console.log('Copying files completed.');
    }
}
const buildMain = new CopyCustomizations();
wait.launchFiber(buildMain.execute);

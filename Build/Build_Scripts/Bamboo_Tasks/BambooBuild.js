var jsonFile = require('jsonfile');
var filePath = require('filepath');
var directoryPathHelper = require('../DirectoryPathHelper');
var cmd = require('node-cmd');
var wait = require('wait.for');
var fileCopy = require('../FileCopy');
var buildAppTask = require('../BuildTask');

class BambooBuild {
    constructor() {

    }

    execute() {
        //Process Command line args - which is passed from bat.
        var customizationPath = null;
        if (process.argv.length < 2) {
            console.log('Input arguments not received.');
            process.exit(-1);
        } else {
            customizationPath = process.argv[2];
            console.log("Customization path :" + customizationPath);
        }

        var constFilePath = filePath.create('Build_Scripts/constants.json');
        var constants = null;

        if (constFilePath.exists()) {
            constants = jsonFile.readFileSync(constFilePath.toString());
        } else {
            console.log('Constants file not available in path : ' + constFilePath.toString());
            process.exit(-1);
        }

        //Create Required paths folders
        var customizationDestinationPath = directoryPathHelper.getCustomizationDestinationPath(constants, customizationPath);
        var appFolderPath = directoryPathHelper.getAppFolderPath(customizationDestinationPath);

        process.chdir(appFolderPath.toString());
        wait.for(buildAppTask.buildSass);

        console.log('Ember CLI build started. Automation mode.');
        wait.for(buildAppTask.buildAppTest, appFolderPath);
        console.log('Ember CLI build completed.');
    }
}
const bambooBuild = new BambooBuild();
wait.launchFiber(bambooBuild.execute);




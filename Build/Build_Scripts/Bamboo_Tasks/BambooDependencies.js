var jsonFile = require('jsonfile');
var filePath = require('filepath');
var directoryPathHelper = require('../DirectoryPathHelper');
var cmd = require('node-cmd');
var wait = require('wait.for');
var userInputReader = require('../UserInputReader');
var buildAppTask = require('../BuildTask');
var fileCopy = require('../FileCopy');

class BambooDependencies {

    constructor() {

    }

    execute() {
        //Process Command line args - which is passed from bat.l
        var customizationPath = null;
        var bambooBuildNumber = null;
        var updatedVersionNumber = null;
        var releaseVersionNumber = null;

        if (process.argv.length < 2) {
            console.log('Input arguments not received.');
            process.exit(-1);
        } else {
            customizationPath = process.argv[2];
            console.log("Customization path :" + customizationPath);

            if(process.argv[3]) {
                bambooBuildNumber = process.argv[3];
                console.log("Bamboo build no :" + bambooBuildNumber);

                if(process.argv[4]) {
                    releaseVersionNumber = process.argv[4];
                    console.log("releaseVersionNumber :" + releaseVersionNumber);
                }
            }
        }

        var constFilePath = filePath.create('Build_Scripts/constants.json');
        var constants = null;

        if (constFilePath.exists()) {
            constants = jsonFile.readFileSync(constFilePath.toString());
        } else {
            console.log('Constants file not available in path : ' +constFilePath.toString());
            process.exit(-1);
        }

        //Create Required paths folders
        var customizationDestinationPath = directoryPathHelper.getCustomizationDestinationPath(constants, customizationPath);
        var appFolderPath = directoryPathHelper.getAppFolderPath(customizationDestinationPath);

        process.chdir(appFolderPath.toString());

        var versionFilePath = './version.json';
        var packageJsonFilePath = './package.json';

        try {
            if(releaseVersionNumber == null) {
                updatedVersionNumber = userInputReader.getUpdatedBambooVersionNumber(versionFilePath, bambooBuildNumber);
            } else {
                updatedVersionNumber = userInputReader.updateReleaseVersionNumber(releaseVersionNumber, bambooBuildNumber);
            }
            userInputReader.updateSemanticVersion(packageJsonFilePath);
        } catch (e) {
            console.log(e);
        }

        console.log('New working directory:' + process.cwd());

        wait.for(buildAppTask.downGradeNodeVersion, appFolderPath);
        wait.for(buildAppTask.setGitConfig, appFolderPath);
        wait.for(buildAppTask.installNodeDependencies, appFolderPath);
        wait.for(buildAppTask.installBowerDependencies, appFolderPath);
        wait.for(buildAppTask.upGradeNodeVersion, appFolderPath);
        //fileCopy.copyFolder(coreTempCheckoutPath.toString(), customizationDestinationPath);
        fileCopy.copyFolder('../../../../../../Default/Default/Default/Price/application/bower_components', 'bower_components');
        fileCopy.copyFolder('../../../../../../Default/Default/Default/Price/application/node_modules', 'node_modules');
        //wait.for(buildAppTask.copyNode, appFolderPath);

        var cliPath = filePath.create(appFolderPath).append('node_modules/ember-cli-ic-ajax');
        process.chdir(cliPath.toString());
        wait.for(buildAppTask.installNodeDependencies, cliPath);
        console.log('Install dependencies completed.');

        process.chdir(appFolderPath.toString());

        userInputReader.updateVersion(updatedVersionNumber, versionFilePath, packageJsonFilePath);
    }
}
const bambooDependencies = new BambooDependencies();
wait.launchFiber(bambooDependencies.execute);

var fse = require('nodejs-fs-utils');
var fs = require('fs');
var filepath = require('filepath');
var fsex = require('fs-extra');
var p = require('path');

class  DirectoryHelper {
    copyFolder(scrPath, destinationPath) {
        this._copySrcToDestination(scrPath, destinationPath);
    }

    copyCustomizations(destination, customizationPath) {
        var destinationPath = destination;
        var currentDir = process.cwd();
        var path = filepath.create(currentDir).relative(customizationPath);
        console.log('Customizations temp path : ' + path.toString());
        var baseName = filepath.create(customizationPath).basename();
        var folderArr = path.split('\\');

        var prevPath = filepath.create(folderArr[0]);
        prevPath = prevPath.append(folderArr[1]);
        prevPath = prevPath.append(folderArr[2]);
        prevPath = prevPath.append(folderArr[3]);
        console.log("Previous path : " + prevPath.toString());

        for (var i = 4; i < folderArr.length; i++) {
            var currentPath = prevPath.append(folderArr[i]);
            console.log(currentPath.toString() + " Current path i =" + i);
            var defPath = currentPath.append('Default');
            console.log(defPath.toString() + " i =" + i);

            defPath = filepath.create(defPath);

            if (defPath.exists()) {
                if (baseName.toString() === 'Price') {
                    console.log(destinationPath);
                    defPath = defPath.append('Price');

                    if (defPath.exists()) {
                        console.log('Folder Exists : ' + defPath.toString());
                        this._copySrcToDestination(defPath.toString(), destinationPath);
                    } else {
                        console.log('Folder does not Exists : ' + defPath.toString());
                    }
                } else {
                    var pricePath = defPath.append('Price');
                    var tradePath = defPath.append('Trade');

                    if (pricePath.exists()) {
                        console.log('Price Folder Exists : ' + pricePath.toString());
                        this._copySrcToDestination(pricePath.toString(), destinationPath);
                    } else {
                        console.log('Price Folder does not Exists : ' + pricePath.toString());
                    }

                    if (tradePath.exists()) {
                        console.log('Trade Folder Exists : ' + tradePath.toString());
                        this._copySrcToDestination(tradePath.toString(), destinationPath);
                    } else {
                        console.log('Trade Folder does not Exists : ' + tradePath.toString());
                    }

                    console.log("Destination path :" + destinationPath);
                }
            } else {
                var currentFolderName = currentPath.basename();
                var currentFolderStr = currentFolderName.toString().replace(/ /g,'');
                var baseNameStr = baseName.toString().replace(/ /g,'');

                if (currentFolderStr === baseNameStr) {
                    console.log(currentPath.toString());
                    currentPath = filepath.create(currentPath);

                    if (currentPath.exists()) {
                        if (baseName.toString() === 'Price') {
                            console.log(destinationPath);
                            this._copySrcToDestination(currentPath.toString(), destinationPath);
                        } else {
                            var pricePath = prevPath.append('Price');
                            if (pricePath.exists()) {
                            this._copySrcToDestination(pricePath.toString(), destinationPath);
                            }
                            this._copySrcToDestination(currentPath.toString(), destinationPath);
                        }
                    }
                }
            }

            prevPath = currentPath;
        }
    }

    createFolder(filePath) {
        var path = filepath.create(filePath);

        if(!path.exists()) {
            path.mkdir();
        }
    }

    emptyDestinationFolder(folderPath) {
        fse.walkSync(folderPath, {
            skipErrors  : true,
            logErrors   : true
        }, function (err, path, stats, next, cache) {
            if (!err) {
                    if (path.match(/\\node_modules/) || path.match(/\\bower_components/) || path.match(/\\.svn/)) {

                    } else {
                        if (p.basename(path) === 'application') {
                        } else {
                            fse.rmdirsSync(path);
                        }
                    }

                next();
            } else {
                next();
            }
        });
    }

    removeSVNFiles(folderPath){
        fse.walkSync(folderPath, {
            skipErrors  : true,
            logErrors   : true
        }, function (err, path, stats, next, cache) {
            if (!err) {
                if(stats.isDirectory()) {
                    if (path.match(/\\.svn/)) {
                        fse.rmdirsSync(path);
                    }
                }

                next();
            } else {
                next();
            }
        });
    }

    getFilesInDir(folderPath) {
        var files = fs.readdirSync(folderPath);
        return files;
    }

    copyFiles(srcPath,destinationPath) {
        try {
            fsex.copySync(srcPath, destinationPath);
            console.log('File Copy Success');
        } catch(e) {
            console.log(e);
        }
    }

    readFileSync(filePath) {
        try {
            var content = fs.readFileSync(filePath, 'utf8');
            return content;
        } catch (e) {
            console.log('File read failed :' +e);
        }
    }

    writeFileSync(filePath, content) {
        try {
            fs.writeFileSync(filePath, content, 'utf8');
        } catch (e) {
            console.log('File write failed. ' + filePath);
            console.log(e);
        }
    }

    _copySrcToDestination(scrPath, destinationPath) {
        if (!fse.isEmptySync(scrPath.toString())) {
            fsex.copySync(scrPath, destinationPath);
            console.log(scrPath+ ' Copied to ' + destinationPath);
        } else {
            console.log(scrPath.toString() + ' Folder empty');
        }
    }
}
module.exports = new DirectoryHelper();




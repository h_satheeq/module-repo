var simpleGit = require('simple-git');
var cmd = require('node-cmd');
var childProcess = require('child_process');

class GitAccess {
    constructor() {

    }

    getLatest(localFolderPath, callback) {
        cmd.get(`git clone --progress -v --depth 1 "git@bitbucket.org:dfndc/gbl_x_ua.git" ` +localFolderPath  ,
            function(data){
                console.log('Source download completed. : ',data);
                callback(null);
            }
        );
    }


    getLatestUsingHTTPS(localFolderPath) { // TODO: [satheeqh] Update branch once moved to main branch
        var stdOut = childProcess.execSync(`git clone --progress -v --depth 1 -b ua-modules --single-branch  "https://a_rasika:thanushinew1@bitbucket.org/dfndc/gbl_x_ua.git" ` + localFolderPath);

        if (stdOut) {
            console.log(stdOut.toString());
        }

        // cmd.get(`git clone --progress -v --depth 1 "https://a_rasika:thanushinew1@bitbucket.org/dfndc/gbl_x_ua.git" ` +localFolderPath  ,
        //     function(data){
        //         console.log('Source download completed. : ',data);
        //         callback(null);
        //     }
        // );
    }

    //getLatest(localFolderPath, callback) {
    //    simpleGit().clone('git@bitbucket.org:dfndc/gbl_x_ua.git', localFolderPath, {'--depth':1}, function () {
    //        console.log('finished');
    //        //https://a_rasika@bitbucket.org/dfndc/universal_app.git
    //        callback(null);
    //    });
    //}

    addRemoteRepo(localFilePath, callback) {
        simpleGit().addRemote('origin', 'git@bitbucket.org:dfndc/gbl_x_ua.git', function () {
            callback(null);
        });
    }

    pushVersion(bambooPassword, callback) {
        //simpleGit().add(localFilePath);
        //simpleGit().commit('Commit version file.', localFilePath);
        //simpleGit().pull('origin', 'master');
        simpleGit().push('origin', 'master', function () {
            callback(null);
        });
    }

    pullBeforeVersionPush(localFilePath, callback) {
        simpleGit().pull('origin', 'master', function() {
            callback(null);
        });
    }

    addVersionFile(localFilePath, callback) {
        simpleGit().add(localFilePath, function () {
            callback(null);
        });
    }

    commitVersionFile(localFilePath, callback) {
        simpleGit().commit('Commit version file.', localFilePath, function () {
            callback(null);
        });
    }

    addTag(tagName, callback) {
        simpleGit().addTag(tagName, function () {
            callback(null);
        });
    }

    pushTags(tagName, callback) {
        simpleGit().pushTags('origin', function () {
            callback(null);
        });
    }
}
module.exports = new GitAccess();
//gitAccess.getLatest('aaa');
var net = require('net');

var client = new net.Socket();
client.connect(5005, '192.168.13.96', function() {
    console.log('Connected');
    var customizationPath = process.argv[2];
    console.log(customizationPath);
    client.write(customizationPath);
});

client.on('data', function(data) {
    console.log('Received: ' + data);
    client.destroy(); // kill client after server's response
});

client.on('close', function() {
    console.log('Connection closed');
});


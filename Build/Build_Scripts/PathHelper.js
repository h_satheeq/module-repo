var filepath = require('filepath');
var fs = require('fs');
var format = require('string-format');
var utility = require('./Utility');

class PathHelper {
    constructor() {
    }

    getCustomizationDestinationPath(customizationPath, constant) {
        var customizationDestinationPath = format('{0}{1}{2}','../',constant.Customizations, customizationPath);
        //console.log('Customization destination path : ' +customizationDestinationPath);
        return customizationDestinationPath;
    }

    getCustomizationsTempPath(customizationPath, constant) {
        var inputFolderArr = utility.getSplitByForwardSlash(customizationPath);
        var country = inputFolderArr[0];
        var client = inputFolderArr[1];

        //console.log('country :' + country);
        var customizationRelativePath = format('{0}{1}{2}{3}{4}{5}{6}{7}', constant.Temp,country,'/',client,'/', constant.Checkout,constant.Customizations, customizationPath);
        //console.log('Customization Relative Path : ' + customizationRelativePath);
        return customizationRelativePath;
    }

    getDefaultCustomizationTempPath(customizationPath, constant) {
        var inputFolderArr = utility.getSplitByForwardSlash(customizationPath);
        var country = inputFolderArr[0];
        var client = inputFolderArr[1];

        var defaultFolderPath = format('{0}{1}','Default/','Default/');

        for(var i = 2; i < inputFolderArr.length; i++) {
            defaultFolderPath = format('{0}{1}{2}',defaultFolderPath,inputFolderArr[i],'/');
        }

        var defaultCustomizationRelativePath = format('{0}{1}{2}{3}{4}{5}{6}{7}', constant.Temp, country,'/',client,'/', constant.Checkout,constant.Customizations, defaultFolderPath);
        //console.log('Default Customization Relative Path : ' + defaultCustomizationRelativePath);
        return defaultCustomizationRelativePath;
    }

    getDefaultCountryCustomizationTempPath(customizationPath, constant) {
        try {
            if (!customizationPath) {
                console.log('Not a valid customization path.');
                return;
            }

            if (!constant) {
                console.log('Not a valid constants file.');
                return;
            }

            var inputFolderArr = utility.getSplitByForwardSlash(customizationPath);

            var country = inputFolderArr[0];
            var client = inputFolderArr[1];

            var defaultCountryFolderPath = format('{0}{1}{2}', inputFolderArr[0], '/', 'Default/');

            for (var j = 2; j < inputFolderArr.length; j++) {
                defaultCountryFolderPath = format('{0}{1}{2}', defaultCountryFolderPath, inputFolderArr[j], '/');
            }

            var defaultCountryCustomizationRelativePath = format('{0}{1}{2}{3}{4}{5}{6}{7}{8}', constant.Temp, country, '/', client, '/', constant.Checkout, constant.Customizations, defaultCountryFolderPath);
            //console.log('Default Country Customization Relative Path : ' + defaultCountryCustomizationRelativePath);
            return defaultCountryCustomizationRelativePath;
        } catch (e) {
            console.log(e);
        }
    }

    getCoreTempPath(customizationPath, constant) {
        try {
            var pathClient = this._getCheckoutPathForClientBuild(customizationPath, constant);

            if(pathClient) {
                var pathCore = pathClient.append(constant.CheckoutCorePath);
                //console.log(pathCore.toString());
                return pathCore;
            } else {
                console.log('Core checkout path invalid.');
            }
        } catch (e) {
            console.log(e);
        }
    }

    getSourceTempPath(customizationPath, constant) {
        try {
            var pathClient = this._getCheckoutPathForClientBuild(customizationPath, constant);

            if(pathClient) {
                var pathCore = pathClient.append(constant.Checkout);
                //console.log(pathCore.toString());
                return pathCore;
            } else {
                console.log('Core checkout path invalid.');
            }
        } catch (e) {
            console.log(e);
        }
    }

    getCustomizationCheckoutTempPath(customizationPath, constant) {
        try {
            var pathClient = this._getCheckoutPathForClientBuild(customizationPath, constant);

            if(pathClient) {
                var pathCheckoutCustomization = pathClient.append(constant.CheckoutCustomizationPath);
                //console.log(pathCheckoutCustomization.toString());
                return pathCheckoutCustomization;
            } else {
                console.log('Customization checkout path invalid.');
            }
        } catch (e) {
            console.log(e);
        }
    }

    getAppFolderPath(customizationDestinationPath) {
        var appFolderPath = filepath.create(customizationDestinationPath).append('application');
        return appFolderPath;
    }

    _getCheckoutPathForClientBuild(customizationPath, constant) {
        try {
            if (!customizationPath) {
                console.log('Not a valid customization path.');
                return;
            }

            var inputFolderArr = utility.getSplitByForwardSlash(customizationPath);

            var country = inputFolderArr[0];
            var client = inputFolderArr[1];

            var intermediateCorePath = filepath.create('temp');
            var pathCountry = intermediateCorePath.append(country);
            var pathClient = pathCountry.append(client);
            return pathClient;
        } catch (e) {
            console.log(e);
        }
    }
}
module.exports = new PathHelper();
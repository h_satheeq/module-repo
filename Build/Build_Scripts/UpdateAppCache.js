var fileCopy = require('./FileCopy');

class UpdateAppCache {
    constructor() {

    }

    update(kernelFolder, version) {
        console.log('Update app cache hash');
        //fileCopy.copyFiles('app/index-native.html','dist/index-native.html');
        //console.log('copy completed.');

        var files = fileCopy.getFilesInDir(kernelFolder.append('dist', 'assets').toString());

        for (var i in  files) {
            if(files[i].startsWith('universal')) {
                var universalAppFileName = files[i];
                var withOutExt = universalAppFileName.split('.');

                if(withOutExt[0]) {
                    var splitWithHyphen = withOutExt[0].split('-');
                    if (splitWithHyphen[2]) {
                        var timeStamp = splitWithHyphen[2];

                        if(timeStamp) {
                            console.log('App-cache timestamp', timeStamp);
                            var appCacheFile = kernelFolder.append('app', 'universal-app.appcache').toString();
                            var appCacheDistFilePath = kernelFolder.append('dist', 'universal-app.appcache').toString();
                            var fileContent = fileCopy.readFileSync(appCacheFile);
                            var results = fileContent.replace(/{{TIMESTAMP}}/g, timeStamp);
                            var dateTime = new Date();
                            var dateStr = dateTime.toDateString();
                            var timeStr = dateTime.toTimeString();
                            results = results.replace(/{{DATE}}/g, dateStr);
                            results = results.replace(/{{TIME}}/g, timeStr);
                            results = results.replace(/{{VERSION}}/g, version);
                            fileCopy.writeFileSync(appCacheDistFilePath, results);
                            fileCopy.writeFileSync(appCacheFile, results);
                            console.log('App-cache hash updated successfully');
                            break;
                        } else  {
                            console.log('Time stamp part missing. Check weather is this a production build.')
                        }
                    }
                }
            }
        }
    }
}
module.exports = new UpdateAppCache();

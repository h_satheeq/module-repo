var pathExists = require('path-exists');
var fileCopy = require('./FileCopy');
var filepath = require('filepath');
var jsonfile = require('jsonfile');
var format = require('string-format');
var constants = require('./constants.json');

class UpdateModuleInitializer {
    constructor() {

    }

    update(moduleArray, customizationDestinationPath) {
        //var moduleFilePath = filepath.create('module.json');

        var moduleInitializerPath = customizationDestinationPath.append(constants.KernelBuild, 'app/config/module-initializer-config.js').toString();
            // format('{0}{1}{2}', customizationDestinationPath, '/', 'Kernel/application/app/config/module-initializer-config.js');
        console.log('module initializer path : '+ moduleInitializerPath);

        if (moduleArray && moduleArray.length > 0) {
            //console.log('Module path exists : ' + moduleFilePath.toString());
            //var moduleArray = jsonfile.readFileSync(moduleFilePath.toString());

            var moduleInitializer = fileCopy.readFileSync(moduleInitializerPath);
            console.log(moduleInitializer);
            console.log('Module array length : ' + moduleArray.length);

            for (var k = 0; k < moduleArray.length; k++) {
                var moduleFile = moduleArray[k];                

                if(moduleFile.importName && moduleFile.initPath) {
                    var importPlaceHolder = "import " + moduleFile.importName + " from '" + moduleFile.initPath + "'; \n//{{IMPORT}}";
                    var createPlaceHolder = ",\n \t \t" + moduleFile.importName + ".create()//{{CREATE}}";
                    //var createPlaceHolderFinal = moduleFile.importName + ".create()";

                    console.log(importPlaceHolder);
                    moduleInitializer = moduleInitializer.replace("//{{IMPORT}}", importPlaceHolder);
                    moduleInitializer = moduleInitializer.replace("//{{CREATE}}", createPlaceHolder);
                }
            }          

            fileCopy.writeFileSync(moduleInitializerPath, moduleInitializer);

        } else {
            console.log('Module Array not exists');
        }
    }
}
module.exports = new UpdateModuleInitializer();
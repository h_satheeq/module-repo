var wait = require('wait.for');
var utility = require('../Utility');
var filepath = require('filepath');
var fs = require('fs');
var fileCopy = require('../DirectoryHelper');
var copyFiles = require('../FileCopy');
var jsonfile = require('jsonfile');
var format = require('string-format');
var pathExists = require('path-exists');
var updateModuleInitializer = require('../UpdateModuleInitializer');

class CopyCustomizations {
    constructor() {

    }

    execute() {
        if (process.argv.length <= 2) {
            console.log('Input arguments not received.');
            process.exit(-1);
        }

        var constantJsonStr = process.argv[2];
        var jsonStr = constantJsonStr.replace(/@/g, '\"');

        //console.log(jsonStr);

        var constant = utility.getJsonObject(jsonStr);
		var customizationPath = constant.customizationPath;

        var inputFolderArr = utility.getSplitByForwardSlash(customizationPath);

        fs.chmodSync(constant.coreTempPath, '0777');

        console.log('Removing .git Files in core checkout location.');
        copyFiles.removeGITFiles(constant.coreTempPath);
        console.log('Remove .git Files Completed.');

        fs.chmodSync(constant.pathCheckoutCustomization, '0777');

        console.log('Removing .git Files in customization checkout location.');
        copyFiles.removeGITFiles(constant.pathCheckoutCustomization);
        console.log('Remove .git Files Completed.');

        console.log('Copying core');
        fileCopy.copyFolder(constant.coreTempPath, constant.customizationDestinationPath);
        console.log('Copying core completed.');

        console.log('Copying modules');
        //var moduleFilePath = filepath.create(constant.customizationDestinationPath).append('module.json');
        var isMobileRelease = utility.isMobileRelease(customizationPath);
        var moduleTempPath = format('{0}{1}{2}', constant.customizationTempPath,'/', 'module.json');
        var moduleFilePath = filepath.create(moduleTempPath);

        var moduleArray = utility.getPreCopyModuleArray(moduleFilePath);

        copyFiles.copyModuleFiles(moduleArray, constant.pathCheckoutCustomization, constant.customizationDestinationPath, isMobileRelease);

        console.log('Copying modules completed.');

        console.log('Copying default customizations.');
        fileCopy.copyCustomizations(constant.customizationDestinationPath, constant.defaultCustomizationTempPath);
        console.log('Copy default customizations completed.');

        console.log('Copying default country customizations.');
        fileCopy.copyCustomizations(constant.customizationDestinationPath, constant.defaultCountryCustomizationTempPath);
        console.log('Copy default country customizations completed.');

        console.log('Copying customizations.');
        fileCopy.copyCustomizations(constant.customizationDestinationPath, constant.customizationTempPath);
        console.log('Copy customizations completed.');

        var postModuleArray = utility.getPreCopyModuleArray(moduleFilePath);
        copyFiles.copyModuleFiles(postModuleArray, constant.pathCheckoutCustomization, constant.customizationDestinationPath, isMobileRelease);

        if(moduleArray && moduleArray.length > 0) {
            updateModuleInitializer.update(moduleArray, constant.customizationDestinationPath);
        }
    }
}
const copyCustomizations = new CopyCustomizations();
copyCustomizations.execute();

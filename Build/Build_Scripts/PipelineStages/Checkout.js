var gitAccess = require('../GitAccess');
var wait = require('wait.for');
var utility = require('../Utility');
var filepath = require('filepath');
var fileCopy = require('../FileCopy');

class Checkout {
   constructor() {

   }

    execute() {
        if (process.argv.length <= 2) {
            console.log('Input arguments not received.');
            process.exit(-1);
        }

        var constantJsonStr = process.argv[2];
        var jsonStr = constantJsonStr.replace(/@/g, '\"');

        //console.log(jsonStr);

        var constant = utility.getJsonObject(jsonStr);

       // var svnFilePath = filepath.create(constant.SvnInfoFilePath);
        //var svnFileObj = utility.readJsonFile(constant.SvnInfoFilePath);

        //Get latest
        console.log('Downloading Source.');

        wait.launchFiber(function () {wait.for(gitAccess.getLatestUsingHTTPS, constant.sourceTempPath.toString())});

        console.log('Download Source completed.');

        //Remove SVN files
        console.log('Removing .git files in core checkout location.');
        fileCopy.removeGITFiles(constant.coreTempPath.toString());
        console.log('Remove .git files Completed.');

        console.log('Removing .git files in customization checkout location.');
        fileCopy.removeGITFiles(constant.pathCheckoutCustomization.toString());
        console.log('Remove .git files Completed.');

        //console.log('Downloading core');
        //wait.launchFiber(function () {wait.for(svnAccess.getSvnSourceCore, constant.SvnCorePath, constant.coreTempPath, svnFileObj.username, svnFileObj.password)});
        //console.log('Download core completed.');
        //
        //console.log('Downloading customizations.');
        //wait.launchFiber(function () {wait.for(svnAccess.getSvnSourceCustomizations, constant.SvnCustomizationPath, constant.pathCheckoutCustomization, svnFileObj.username, svnFileObj.password)});
        //console.log('Download customizations completed.');
    }
}
const checkOut = new Checkout();
checkOut.execute();

var gitAccess = require('../GitAccess');
var wait = require('wait.for');
var cmd = require('node-cmd');
var fse = require('nodejs-fs-utils');
var utility = require('../Utility');
var buildAppTask = require('../BuildTask');
var filepath = require('filepath');
var format = require('string-format');
var pathExists = require('path-exists');
var fileCopy = require('../DirectoryHelper');
var userInputReader = require('../UserInputReader');
var directoryPathHelper = require('../DirectoryPathHelper');

class Build {
    constructor() {

    }

    execute() {
        if (process.argv.length <= 2) {
            console.log('Input arguments not received.');
            process.exit(-1);
        }

        var constantJsonStr = process.argv[2];
        var jsonStr = constantJsonStr.replace(/@/g, '\"');
        var constant = utility.getJsonObject(jsonStr);
        //var svnFileObj = utility.readJsonFile(constant.SvnInfoFilePath);

        // var svnFilePath = filepath.create(constant.SvnInfoFilePath);
        var appPath = constant.appFolderPath;//filepath.create(constant.customizationDestinationPath).append('application');
        process.chdir(appPath.toString());
        console.log('New working directory:' + process.cwd());

        var distFolderPath = null;
        var customizationPath = constant.customizationPath;

        //console.log('isAutomatedPath jb : ' + isAutomatedPath);
        var versionFilePath = directoryPathHelper.getVersionFilePath(appPath);
        var packageJsonFilePath = directoryPathHelper.getPackageJsonFilePath(appPath);

        if (versionFilePath.exists()) {
            var versionValue = userInputReader.getVersionNumberFromVersionFile(versionFilePath, 4);
            userInputReader.updateVersion(versionValue, versionFilePath, packageJsonFilePath);
        } else {
            console.log('Version file not exists.')
        }

        console.log('Ember CLI build started. Automation mode.');
        wait.for(buildAppTask.buildAppTest, appPath);

        console.log('Version file check in started.');
        wait.for(gitAccess.addVersionFile, versionFilePath.toString());
        wait.for(gitAccess.commitVersionFile, versionFilePath.toString());
        wait.for(gitAccess.pushVersion, versionFilePath.toString());
        console.log('Version file check in completed.');


        console.log('Ember CLI build completed.');

        var inputFolderArr = utility.getSplitByForwardSlash(customizationPath);

        if (inputFolderArr[2] === 'Mobile') {
            distFolderPath = format('{0}{1}{2}{3}{4}{5}{6}{7}', "E://mobile/",
                inputFolderArr[1], '/', 'dev', '/', 'retail', '/', inputFolderArr[4]);
            distFolderPath = distFolderPath.toLowerCase();
        } else {
            distFolderPath = format('{0}{1}{2}{3}{4}{5}{6}{7}', "E://",
                inputFolderArr[1], '/', 'dev', '/', 'retail', '/', inputFolderArr[4]);
            distFolderPath = distFolderPath.toLowerCase();
        }
    }
}
const build = new Build();
wait.launchFiber(build.execute);
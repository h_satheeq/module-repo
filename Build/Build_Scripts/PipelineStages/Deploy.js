var wait = require('wait.for');
var cmd = require('node-cmd');
var fse = require('nodejs-fs-utils');
var filepath = require('filepath');
var format = require('string-format');
var pathExists = require('path-exists');

var utility = require('../Utility');
var fileCopy = require('../DirectoryHelper');
var updateAppCache = require('../UpdateAppCache');

class Build {
    constructor() {
    }

    execute() {
        if (process.argv.length <= 2) {
            console.log('Input arguments not received.');
            process.exit(-1);
        }

        var constantJsonStr = process.argv[2];
        var jsonStr = constantJsonStr.replace(/@/g, '\"');
        var constant = utility.getJsonObject(jsonStr);
        var appPath = constant.appFolderPath;
        process.chdir(appPath.toString());

        var distFolderPath = null;
        var customizationPath = constant.customizationPath;

        var inputFolderArr = utility.getSplitByForwardSlash(customizationPath);

        if (inputFolderArr[2] === 'Mobile') {
            distFolderPath = format('{0}{1}{2}{3}{4}{5}{6}{7}{8}{9}', "E://Releases/mobile/",inputFolderArr[0],'/',
                inputFolderArr[1], '/', 'dev', '/', 'retail', '/', inputFolderArr[4]);
            distFolderPath = distFolderPath.toLowerCase();
        } else {
            distFolderPath = format('{0}{1}{2}{3}{4}{5}{6}{7}{8}{9}', "E://Releases/",inputFolderArr[0],'/',
                inputFolderArr[1], '/', 'dev', '/', 'retail', '/', inputFolderArr[4]);
            distFolderPath = distFolderPath.toLowerCase();
        }

        if (pathExists.sync(distFolderPath)) {
            console.log('Removing content of release folder.');
            fse.emptyDirSync(distFolderPath);
            console.log('Content of the release folder removed.');
        }


        if (inputFolderArr[2] === 'Mobile') {
            console.log('File copy hits.');
            fileCopy.copyFiles('app/index-native.html', 'dist/index-native.html');
            console.log('copy completed.');

            var files = fileCopy.getFilesInDir('dist/assets');

            for (var i in  files) {
                console.log(files[i]);
                if (files[i].startsWith('universal')) {
                    var universalAppFileName = files[i];
                    var withOutExt = universalAppFileName.split('.');

                    if (withOutExt[0]) {
                        var splitWithHyphen = withOutExt[0].split('-');
                        if (splitWithHyphen[2]) {
                            var timeStamp = splitWithHyphen[2];

                            if (timeStamp) {
                                console.log(timeStamp);
                                var indexNative = 'dist/index-native.html';
                                var fileContent = fileCopy.readFileSync(indexNative);
                                var results = fileContent.replace(/{{TIMESTAMP}}/g, timeStamp);
                                fileCopy.writeFileSync(indexNative, results);
                                console.log('Updated the time stamp of index-native successfully.');
                                break;
                            } else {
                                console.log('Time stamp part missing. Check weather is this a production build.')
                            }
                        }
                    }
                }
            }
        }
        updateAppCache.update('1.0.7');

        console.log('dist folder : ' + distFolderPath);
        //fileCopy.createFolder(distFolderPath);

        console.log('Copying files to the destination.');
        fileCopy.copyFolder('dist',distFolderPath);
        console.log('Copy files to the destination completed.');
    }
}
const build = new Build();
build.execute();
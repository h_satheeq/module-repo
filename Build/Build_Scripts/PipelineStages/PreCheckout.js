var fse = require('nodejs-fs-utils');
var jsonfile = require('jsonfile');
var filepath = require('filepath');
var promt = require('prompt-sync')();
var format = require('string-format');

var fileCopy = require('../DirectoryHelper');
var pathHelper = require('../PathHelper');
var utility = require('../Utility');
var constant = require('../constants');

class PreCheckout {
    constructor() {

    }

    execute() {
        var svnFilePath = filepath.create('Build_Scripts/svninfo.json');
        var constFilePath = filepath.create('Build_Scripts/constants.json');
        var releaseType = 'production';
        var isAutomatedPath = 'false';
        var newVersion = null;
        var customizationTempPath = "";
        var customizationDestinationPath = "";
        var defaultCustomizationTempPath = "";
        var defaultCountryCustomizationTempPath = "";
        var coreTempPath = "";
        var pathCheckoutCustomization = "";
        var sourceTempPath = "";

        if (process.argv.length === 5) {
            releaseType = process.argv[3];
            isAutomatedPath = process.argv[4];
            //console.log('isAutomatedPath from args :' + isAutomatedPath);
        }

        //if (isAutomatedPath === 'false') {
        //    newVersion = this._getVersionInfo(releaseType);
        //}

        //if (svnFilePath.exists()) {
        //    //console.log('svn info file exists');
			//
        //} else {
        //    if(isAutomatedPath === 'false') {
        //        this._promptForSvnUser(svnFilePath);
        //    } else {
        //        //console.log('svninfo.json file exists.');
			//	//console.log(svnFilePath);
        //        process.exit(-1);
        //    }
        //}

        if (process.argv.length <= 2) {
            console.log('Input arguments not received.');
            process.exit(-1);
        }

        //var constant = '';

        //if (constFilePath.exists()) {
        //    //constant = jsonfile.readFileSync(constFilePath.toString());
        //} else {
        //    console.log('Constants file not available.');
        //    process.exit(-1);
        //}

        var customizationPath = process.argv[2];

        if(customizationPath) {
            customizationDestinationPath = pathHelper.getCustomizationDestinationPath(customizationPath, constant);
            //console.log('Customization destination path : ' + customizationDestinationPath);
            customizationTempPath = pathHelper.getCustomizationsTempPath(customizationPath, constant);
            //console.log('Customization Temporary Path : ' + customizationTempPath);
            defaultCustomizationTempPath = pathHelper.getDefaultCustomizationTempPath(customizationPath, constant);
            //console.log('Default Customization Temporary Path : ' + defaultCustomizationTempPath);
            defaultCountryCustomizationTempPath =  pathHelper.getDefaultCountryCustomizationTempPath(customizationPath, constant);
            //console.log('Default Country Customization Temporary Path : ' + defaultCountryCustomizationTempPath);
            coreTempPath = pathHelper.getCoreTempPath(customizationPath, constant);
            sourceTempPath = pathHelper.getSourceTempPath(customizationPath, constant);
            //console.log(coreTempPath.toString());
            pathCheckoutCustomization = pathHelper.getCustomizationCheckoutTempPath(customizationPath, constant);

            utility.appendKeyValuePairToObject(constant, 'customizationDestinationPath', customizationDestinationPath);
            utility.appendKeyValuePairToObject(constant, 'customizationTempPath', customizationTempPath);
            utility.appendKeyValuePairToObject(constant, 'defaultCustomizationTempPath', defaultCustomizationTempPath);
            utility.appendKeyValuePairToObject(constant, 'defaultCountryCustomizationTempPath', defaultCountryCustomizationTempPath);
            utility.appendKeyValuePairToObject(constant, 'coreTempPath', coreTempPath.path);
            utility.appendKeyValuePairToObject(constant, 'sourceTempPath', sourceTempPath.path);
            utility.appendKeyValuePairToObject(constant, 'pathCheckoutCustomization', pathCheckoutCustomization.path);
            utility.appendKeyValuePairToObject(constant, 'customizationPath', customizationPath);
        } else {
            console.log('Customization path not provided.');
            process.exit(-1);
        }

        if (!sourceTempPath.exists()) {
            filepath.create(sourceTempPath).mkdir();
        }

        //console.log('removing content of the core temp path.');
        fse.emptyDirSync(sourceTempPath.toString());
        //console.log('content of the core temp path removed.');


        //if (!coreTempPath.exists()) {
        //    filepath.create(coreTempPath).mkdir();
        //}
        //
        ////console.log('removing content of the core.');
        //fse.emptyDirSync(coreTempPath.toString());
        ////console.log('content of the core removed.');
        //
        //fileCopy.createFolder(pathCheckoutCustomization.toString());
        //
        ////console.log('removing content of the customizations.');
        //fse.emptyDirSync(pathCheckoutCustomization.toString());
        ////console.log('content of the customizations removed.');

        fileCopy.createFolder(customizationDestinationPath);

        var appFolderPath = pathHelper.getAppFolderPath(customizationDestinationPath);
        utility.appendKeyValuePairToObject(constant, 'appFolderPath', appFolderPath.path);

        if (appFolderPath.exists()) {
            //console.log('Removing content of the ' + appFolderPath.toString());
            fileCopy.emptyDestinationFolder(appFolderPath.toString());
        }

        var jsonStr = utility.getJsonString(constant);

        console.log(jsonStr);
    }

    _getVersionInfo(releaseType) {
        if (releaseType === 'production') {
            var versionType = promt('Enter version type : (1 - major, 2 - minor, 3 - current, 4 - patch)');
        }
    }

    _promptForSvnUser(svnFilePath) {
        try {
            var usr = promt('enter svn user name: ');
            //console.log(usr);
            var pwd = promt('enter svn password: ');
            //console.log(pwd);
            //console.log('Writing to a svn infofile');
            jsonfile.writeFileSync(svnFilePath.toString(), {username: usr, password: pwd});
        } catch (e) {
            console.log(e);
        }
    }
}
const preCheckOut = new PreCheckout();
preCheckOut.execute();

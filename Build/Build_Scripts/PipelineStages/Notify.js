var fse = require('nodejs-fs-utils');
var format = require('string-format');
var utility = require('../Utility');
var emailCreator = require('../EmailCreator');
var emailSender = require('../EmailSender');

class Notify {
    constructor() {

    }

    execute() {
        if (process.argv.length <= 2) {
            console.log('Input arguments not received.');
            process.exit(-1);
        }

        var constantJsonStr = process.argv[2];
        var jsonStr = constantJsonStr.replace(/@/g, '\"');
        var constant = utility.getJsonObject(jsonStr);
        var customizationPath = constant.customizationPath;

        var message = "Build or deployment failed. Customization path : " + customizationPath;
        console.log('Sending EMail.');
        var textString = emailCreator.createEmail(message);
        emailSender.sendNotificationEmail(textString);
        console.log('EMail Sent.');
    }
}
const notify = new Notify();
notify.execute();



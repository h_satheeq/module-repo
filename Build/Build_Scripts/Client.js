var net = require('net');

var client = new net.Socket();
client.connect(5000, '192.168.13.96', function() {
    console.log('Connected');
    var customizationPath = process.argv[2];
    var isSmokeTest = false;
    if(process.argv.length > 2){
        isSmokeTest = process.argv[3];
        customizationPath = customizationPath + '|' + isSmokeTest;
        console.log(customizationPath);
        client.write(customizationPath);
    } else {
        console.log(customizationPath);
        client.write(customizationPath);
    }
});

client.on('data', function(data) {
    console.log('Received: ' + data);
    client.destroy(); // kill client after server's response
});

client.on('close', function() {
    console.log('Connection closed');
});

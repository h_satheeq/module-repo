var svnUltimate = require('node-svn-ultimate');

class SvnAccess {
    constructor() {

    }

    getSvnSourceCore(svnSvrPath, svnLocalPath, userName, password, callback) {
        svnUltimate.commands.checkout(svnSvrPath, svnLocalPath,
            {	// optional options object - can be passed to any command not just update		
                username: userName,
                password: password
            },
            function (err) {
                if(err){
                    console.log('SVN get latest failed.');
                    process.exit(-1);
                }
                callback(null, 'Download Complete : ' + svnSvrPath);
            });
    }

    getSvnSourceCustomizations(svnSvrPath, svnLocalPath, userName, password, callback) {
        svnUltimate.commands.checkout(svnSvrPath, svnLocalPath,
            {	// optional options object - can be passed to any command not just update		
                username: userName,
                password: password
            }, function (err) {
                if(err){
                    console.log('SVN get latest failed.');
                    process.exit(-1);
                }

                callback(null, 'Download Complete : ' + svnSvrPath);
            });
    }

    checkInVersionFile(filePath, userName, password,callback) {
        svnUltimate.commands.commit(filePath,{
            username: userName,
            password: password,
            params: [ '-m "version updated"' ]
        }, function(err){
            if(err){
                console.error(err);
            }

            callback(null, 'Version file checked in : ' + filePath);
        });

        console.log('File checkin success..');
    }

    getVersionFileFromSVN(svnSvrPath,svnLocalPath, userName, password,callback) {
        svnUltimate.commands.export(svnSvrPath, svnLocalPath,
            {	// optional options object - can be passed to any command not just update
                username: userName,
                password: password
            }, function (err) {
                if(err){
                    console.log('SVN export latest failed.');
                    process.exit(-1);
                }

                callback(null, 'Download Complete : ' + svnSvrPath);
            });

        console.log('File export success..');
    }

    getSVNLatestInfoObj(folderPath, userName, password, watcherObj, callback) {
        svnUltimate.commands.info(folderPath,
            {	// optional options object - can be passed to any command not just update
                username: userName,
                password: password
            }, function (err, infoObj) {
                if (err) {
                    console.log('SVN check status failed.');
                    process.exit(-1);
                }

                watcherObj[folderPath] = infoObj;
            });
    }
}
module.exports = new SvnAccess();


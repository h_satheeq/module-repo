var wait = require('wait.for');
var supportAgent = require('./ServerRemoteSupportAgent');


class ServerRemoteAgent {
    constructor() {

    }

    execute(customizationPath) {
        //customizationPath = '../Customizations/Saudi/ICAP/Desktop/RIAWeb/Trade';
		console.log('Received data :' + customizationPath);
        var receivedData = [];
        var isSmokeTest = false;

        var customizationPathStr = customizationPath.toString();

        if(customizationPathStr.indexOf('|') != -1){
            receivedData = customizationPathStr.split('|');
            if(receivedData.length > 1){
                customizationPath = receivedData[0];
                isSmokeTest = receivedData[1];
            }
        }

        var testPath = '../Customizations/' + customizationPath + '/Test';

        wait.for(supportAgent.installBuildDependencies, 'serverAgent');
        wait.for(supportAgent.prepareTestEnvironment, customizationPath);
        wait.for(supportAgent.installTestDependencies, 'serverAgent');

        if(isSmokeTest) {
            wait.for(supportAgent.executeTest, testPath, isSmokeTest);
        } else {
            wait.for(supportAgent.executeTest, testPath);
        }
    }
}
module.exports = new ServerRemoteAgent();
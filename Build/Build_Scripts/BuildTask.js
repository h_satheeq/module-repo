var cmd = require('node-cmd');
var childProcess = require('child_process');
var fse = require('fs-extra');
var constants = require('./constants.json');
var fileCopy = require('./FileCopy');

class BuildTask {
    constructor() {
    }

    buildApp(dest, modules, buildEnv) {
        var that = this;

        modules.forEach(function (module) {
            that.buildModule(dest, module);
        });

        var buildCommand = ['ember build --environment=', buildEnv ? buildEnv : 'production'].join('');

        console.log('Executing command ', buildCommand);

        try {
            var stdOut = childProcess.execSync(buildCommand, {
                cwd: dest.append(constants.KernelBuild).toString(),
                timeout: 100000000,
                maxBuffer: 2000 * 1024
            });

            console.log(stdOut.toString());
        } catch (e) {
        }
    }

    buildModule(dest, module) {
        console.log('Building module', module);

        var disFolder = dest.append(module, 'packaging', 'dist');

        if (disFolder.exists()) {
            fse.removeSync(disFolder.toString());
        }

        try {
            var stdOut = childProcess.execSync('broccoli build dist', {
                cwd: dest.append(module, 'packaging').toString(),
                timeout: 100000000
            });

            console.log(stdOut.toString());
        } catch (e) {
        }

        fse.copySync(disFolder.toString(), dest.append(constants.KernelBuild, 'public/assets/addons').toString());
    }

    downGradeNodeVersion(appPath, callback) {
        cmd.get(`nvm use 4.4.5`,
            function(data) {
                console.log('Node version set to 4.4.5. : ',data);
                callback(null);
            }
        );
    }

    upGradeNodeVersion(appPath, callback) {
        cmd.get(`nvm use 6.9.2`,
            function(data) {
                console.log('Node version set to 6.9.2. : ',data);
                callback(null);
            }
        );
    }

    setGitConfig(appPath, callback) {
        childProcess.execSync(`git config --global url.https://github.com/.insteadOf git://github.com/`);
        console.log('Set git config completed');

        // TODO: [satheeqh] Check this usages

        // cmd.get(`git config --global url.https://github.com/.insteadOf git://github.com/`,
        //     function(data){
        //         callback(null);
        //     }
        // );
    }

    installNodeDependencies(appPath, callback) {
        cmd.get('npm install',
            function(data){
                console.log('call npm install completed. : ',data);
                callback(null);
            }
        );
    }

    installBowerDependencies(appPath, callback) {
        cmd.get(`bower install`,
            function(data){
                console.log('call bower install completed. : ',data);
                callback(null);
            }
        );
    }

    copyBower(appPath, callback) {
        cmd.get('xcopy ../../../../../../Default/Default/Default/Price/application/bower_components bower_components /E /I /H /Y',
            function(data){
                console.log('bower copy completed. : ',data);
                callback(null);
            }
        );
    }

    copyNode(appPath, callback) {
        cmd.get('xcopy ../../../../../../Default/Default/Default/Price/application/node_modules node_modules /E /I /H /Y',
            function(data){
                console.log('node copy completed. : ',data);
                callback(null);
            }
        );
    }

    buildAppTest(appPath, callback) {
        cmd.get(`ember build --environment=automation`,
            function(data){
                console.log('Build execution completed. : ',data);
                callback(null);
            }
        );
    }

    buildSass(destination) {
        var command = 'npm rebuild node-sass';

        console.log('Executing command', command);
        console.log(childProcess.execSync(command, {cwd: destination}).toString());

        // cmd.get(`npm rebuild node-sass`,
        //     function(data){
        //         console.log('Sass build execution completed. : ',data);
        //         callback(null);
        //     }
        // );
    }

    testExecution(testFolderPath, callback) {
        cmd.get('start node TestMain.js ' + testFolderPath,
            function(data){
                console.log('Build execution completed. : ',data);
                callback(null);
            }
        );
    }

    createProject(cordovaFolderName, cordovaProjectName, cordovaIdentifierName, callback) {
        cmd.get(
            `cordova create ` + cordovaFolderName + ` ` + cordovaProjectName + ` ` + cordovaIdentifierName,

            function (data, err) {
                if (err) {
                    console.error();
                    console.log('Build Error Occured.');
                    process.exit(-1);
                }

                callback(null);
            }
        );
    }

    cordovaAndroidPlatform(callback) {
        console.log('cordova android platform');

        cmd.get(
            'cordova platform add android@5.1.0 --force',

            function (data, err) {
                if (err) {
                    console.error();
                    console.log('Build Error Occured.');
                    process.exit(-1);
                }

                callback(null);
            }
        );
    }

    cordovaIOSPlatform(callback) {
        console.log('cordova ios platform');

        cmd.get(
            'cordova platform add ios --save',

            function (data, err) {
                if (err) {
                    console.error();
                    console.log('Build Error Occured.');
                    process.exit(-1);
                }

                callback(null);
            }
        );
    }

    cordovaPlugin(callback) {
        console.log('cordova-plugin add cordova-plugin-dialog');

        cmd.get(
            'cordova plugin add cordova-plugin-dialogs',

            function (data, err) {
                if (err) {
                    console.error();
                    console.log('Build Error Occured.');
                    process.exit(-1);
                }

                callback(null);
            }
        );
    }

    cordovaPluginScreen(callback) {
        console.log('cordova-plugin-screen orientation Executed');

        cmd.get(
            'cordova plugin add cordova-plugin-screen-orientation',

            function (data, err) {
                if (err) {
                    console.error();
                    console.log('Build Error Occured.');
                    process.exit(-1);
                }

                callback(null);
            }
        );
    }

    cordovaPluginSplashScreen(callback) {
        console.log('cordova-plugin-splashscreen Executed');

        cmd.get(
            'cordova plugin add cordova-plugin-splashscreen',

            function (data, err) {
                if (err) {
                    console.error();
                    console.log('Build Error Occured.');
                    process.exit(-1);
                }

                callback(null);
            }
        );
    }

    cordovaPluginScreenShot(callback) {
        console.log('cordova-plugin-screenshot Executed');

        cmd.get(
            'cordova plugin add https://github.com/gitawego/cordova-screenshot.git',

            function (data, err) {
                if (err) {
                    console.error();
                    console.log('Build Error Occured.');
                    process.exit(-1);
                }

                callback(null);
            }
        );
    }

    cordovaPluginSocialSharing(callback) {
        console.log('cordova-plugin-social-sharing');

        cmd.get(
            'cordova plugin add cordova-plugin-x-socialsharing',

            function (data, err) {
                if (err) {
                    console.error();
                    console.log('Build Error Occured.');
                    process.exit(-1);
                }

                callback(null);
            }
        );
    }

    cordovaPluginDatePicker(callback) {
        console.log('cordova-plugin-datepicker Executed');

        cmd.get(
            'cordova plugin add cordova-plugin-datepicker',

            function (data, err) {
                if (err) {
                    console.error();
                    console.log('Build Error Occured.');
                    process.exit(-1);
                }

                callback(null);
            }
        );
    }

    cordovaPluginListPicker(callback) {
        console.log('cordova-plugin-listpicker Executed');

        cmd.get(
            'cordova plugin add cordova-plugin-listpicker',

            function (data, err) {
                if (err) {
                    console.error();
                    console.log('Build Error Occured.');
                    process.exit(-1);
                }

                callback(null);
            }
        );
    }

    cordovaPrepare(callback) {
        console.log('Cordova Prepare Executed');

        cmd.get(
            `cordova prepare`,

            function (data, err) {
                if (err) {
                    console.error();
                    console.log('Build Error Occured.');
                    process.exit(-1);
                }

                callback(null, 'Cordova prepare executed.');
            }
        );
    }

    cordovaAndroidBuild(callback) {
        console.log('Build Executed');

        cmd.get(
            `cordova build android --release`,

            function (data, err) {
                if (err) {
                    console.error();
                    console.log('Build Error Occured.');
                    process.exit(-1);
                }

                callback(null, 'Production build completed.');
            }
        );
    }

    cordovaIOSBuild(callback) {
        console.log('Build Executed');

        cmd.get(
            `cordova build ios --release`,

            function (data, err) {
                if (err) {
                    console.error();
                    console.log('Build Error Occured.');
                    process.exit(-1);
                }

                callback(null, 'Production build completed.');
            }
        );
    }

    installBroccoliCli() {
        var command = 'npm install -g broccoli-cli';

        console.log('Executing command', command);
        childProcess.execSync(command);
    }

    npmInstall(destination) {
        var command = 'npm install';

        console.log('Executing command', destination, command);
        childProcess.execSync(command, {cwd: destination});
    }

    bowerInstall(destination) {
        var command = 'bower install';

        console.log('Executing command', destination, command);
        childProcess.execSync(command, {cwd: destination});
    }

    npmLink(destination, linkAddon) {
        var command = 'npm link ' + (linkAddon ? linkAddon : '');

        console.log('Executing command', destination, command);
        console.log(childProcess.execSync(command, {cwd: destination}).toString());
    }

    copyNodeModules(destination) {
        var command = ['node', './node_modules/copy-node-modules/bin/copy-node-modules.js', destination, 'D:\\workspace\\icap-trade', '--dev', '-v'].join(' ');

        // console.log('Executing command', command);
        // console.log(childProcess.execSync(command).toString());

        copyNodeModule(destination, 'D:\\workspace\\icap-trade', {devDependencies: true}, function(err, results) {
            if (err) {
                console.error(err);
                return;
            }

            console.log('cmpl', results);

            for (var i in results) {
                console.log('package name:' + results[i].name + ' version:' + results[i].version);
            }
        });
    }
}

module.exports = new BuildTask();




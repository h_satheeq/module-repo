var fs = require('fs-extra');
var cmd = require('node-cmd');
var p = require('path');
var fse = require('nodejs-fs-utils');
var buildMain = require('./Build_Scripts/BuildMain');
var buildTask = require('./Build_Scripts/BuildTask');
var directoryPathHelper = require('./Build_Scripts/DirectoryPathHelper');
var fileCopy = require('./Build_Scripts/FileCopy');

class UpdateDevEnv {
    constructor() {
    }

    test(input) {
        console.log(input);
    }

    clearDirectory(directory, cb) {
        console.log('Start cleaning');

        fs.readdir(directory, (err, files) => {
            files.forEach(file => {
                var path = directory + '\\' + file;

                if (path.match(/\\node_modules/) || path.match(/\\bower_components/) || path.match(/\\.svn/) || path.match(/\\.idea/)
                    || path.match(/\\dist/) || path.match(/\\tmp/) || path.match(/\\custom-script.js/) || path.match(/\\clone-script.js/)) {
                    console.log('ignored', path);
                } else {
                    fse.rmdirsSync(path);
                }
            });

            cb();
        });
    }

    getLatest(customization, cusPath, currPath) {

        // var nodeCmd = 'cd D:/workspace/module-repo/Build/ && node Build_Scripts/BuildMain.js ' + customization + ' dev false true';
        // console.log('executing command:', nodeCmd);

        // cmd.get('cd D:/workspace/module-repo/Build/', function () { //todo: make sure process starts after the 'cd'
        //     console.log(process.cwd());


        // });

        process.chdir('D:/workspace/module-repo/Build/');
        console.log('Working directory: ' + process.cwd());

        var releaseType = 'dev',
            isAutomatedPath = 'false',
            versionValue = null,
            isWindows = true,
            isLabelRequired = false,
            isInstallPackages= false,
            isBuildDebug = true;

        var checkoutPath = 'D:/workspace/ua-modules-branch';
        // var checkoutPath = undefined;


        buildMain.execute(customization, releaseType, isAutomatedPath, versionValue, isWindows, isLabelRequired, isInstallPackages, checkoutPath);

        if (isBuildDebug) {
            var custDest = directoryPathHelper.getCustomizationDestinationPath(require('./Build_Scripts/constants.json'), customization);

            console.log('Debug build started');
            buildTask.buildApp(custDest, fileCopy.getModuleDir(custDest), 'dev');
            process.exit(0);
        }

        /*cmd.get(nodeCmd,
            function (err, data) {
				console.log(err);
                console.log('build execution completed.', data);

                console.log('copying', cusPath, currPath);

                // fs.copy(cusPath, currPath, function (err) {
                //     if (err) {
                //         console.error(err);
                //     } else {
                //         /!*cmd.get('git init', function () {
                //             console.log('git initialized');
                //
                //             cmd.get('git add ./app', function () {
                //                 console.log('app folder added to git');
                //
                //                 cmd.get('git commit -m "init commit"', function () {
                //                     console.log('init commit completed');
                //                     console.log('*********Setup Completed**********');
                //                 });
                //             });
                //         });*!/
                //     }
                // });
            }
        );*/
    }
}

module.exports = new UpdateDevEnv();
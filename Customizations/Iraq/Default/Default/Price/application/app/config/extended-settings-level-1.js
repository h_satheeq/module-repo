export default {
    appConfig: {
        customisation: {
            clientPrefix: 'iraq'
        }
    },

    priceSettings: {
        connectionParameters: {
            primary: {
                ip: '31.222.133.25',
                port: '9018',
                secure: false
            },

            secondary: {
                ip: '31.222.133.25',
                port: '9018',
                secure: false
            }
        }
    },

    userSettings: {
        customisation: {
            defaultExchange: 'ISX',
            defaultIndex: 'ISX60',
            defaultCurrency: 'IQD'
        }
    }
};

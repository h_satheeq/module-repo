import OrderList from '../order-list';

export default OrderList.extend({
    onLoadWidget: function () {
        this._super();
        this.set('title', this.get('app').lang.labels.optionOrderList);
    },

    loadContent: function () {
        this.loadDropDowns();

        var orders = this.tradeService.orderDS.getOrdersByInstType(this.utils.AssetTypes.Option);

        this.set('content', orders);
        this.set('masterContent', orders);
        this.set('filteredContent', orders);
    }
});
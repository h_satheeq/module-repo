import Ember from 'ember';
import MutualFund from '../business-entities/mutual-fund';
import tradeConstants from '../trade-constants';

export default Ember.Object.extend({
    fundStore: {},
    holdingStore: {},
    transactionStore: {},
    fundCollection: Ember.A(),
    holdingCollection: Ember.A(),
    transactionCollection: Ember.A(),

    initialize: function () {
        // Implement initialization code here
    },

    getFund: function (code, isEmptyFund, viewStatus) {
        var currentStore = this.get('fundStore');
        var fundObj = currentStore[code];

        if (!fundObj && code && !isEmptyFund) {
            fundObj = MutualFund.create({
                symbol: code
            });

            currentStore[code] = fundObj;

            var fundCollection = this.get('fundCollection');

            if (fundCollection[viewStatus]) {
                fundCollection[viewStatus].pushObject(fundObj);
            } else {
                fundCollection[viewStatus] = Ember.A([fundObj]);
            }
        }

        return fundObj;
    },

    getHolding: function (code) {
        var currentStore = this.get('holdingStore');
        var holdingObj = currentStore[code];

        if (!holdingObj && code) {
            holdingObj = MutualFund.create({
                symbol: code
            });

            currentStore[code] = holdingObj;
            this.get('holdingCollection').pushObject(holdingObj);
        }

        return holdingObj;
    },

    getTransaction: function (code, sts) {
        var currentStore = this.get('transactionStore');
        var transactionObj = currentStore[code];

        if (transactionObj) {
            this.updateTransactionCollection(transactionObj, code, sts);
        }

        if (!transactionObj && code) {
            transactionObj = MutualFund.create({
                symbol: code
            });

            currentStore[code] = transactionObj;

            var mfTransSts = tradeConstants.MutualFundTransactionStatus;
            var status = sts === mfTransSts.Done ? mfTransSts.Done : mfTransSts.Registered;

            var transCollection = this.get('transactionCollection');
            var transCollectionBySts = transCollection[status] ? transCollection[status] : Ember.A([]);

            transCollectionBySts.pushObject(transactionObj);
        }

        return transactionObj;
    },

    updateTransactionCollection: function (transactionObj, code, sts) {
        if (transactionObj.sts !== sts) {
            var mfTransSts = tradeConstants.MutualFundTransactionStatus;
            var oldStatus = transactionObj.sts === mfTransSts.Done ? mfTransSts.Done : mfTransSts.Registered;
            var newStatus = sts === mfTransSts.Done ? mfTransSts.Done : mfTransSts.Registered;
            var transCollection = this.get('transactionCollection');

            var transCollectionByOldSts = transCollection[oldStatus] ? transCollection[oldStatus] : Ember.A([]);
            transCollectionByOldSts.removeObject(transactionObj);

            var transCollectionByNewSts = transCollection[newStatus] ? transCollection[newStatus] : Ember.A([]);
            transCollectionByNewSts.pushObject(transactionObj);
        }
    },

    getFundCollection: function (viewStatus) {
        var fundCollection = this.get('fundCollection');

        if (!fundCollection[viewStatus]) {
            fundCollection[viewStatus] = Ember.A([]);
        }

        return fundCollection[viewStatus];
    },

    getHoldingCollection: function () {
        return this.get('holdingCollection');
    },

    getTransactionCollection: function (ststus) {
        var transactionCollection = this.get('transactionCollection');

        if (!transactionCollection[ststus]) {
            transactionCollection[ststus] = Ember.A([]);
        }

        return transactionCollection[ststus];
    },

    clearCollection: function () {
        this.set('fundStore', {});
        this.set('holdingStore', {});
        this.set('transactionStore', {});
        this.set('fundCollection', Ember.A());
        this.set('holdingCollection', Ember.A());
        this.set('transactionCollection', Ember.A());
    }
});

import Ember from 'ember';
import languageDataStore from '../../shared/language/language-data-store';
import sharedService from '../../shared/shared-service';

export default Ember.Object.extend({
    code: '',
    name: '',
    fndName: '',
    arabicFundName: '',
    baseCurr: '',
    curr: '',
    qty: 0.0,
    avaiQty: 0,
    pldgQty: 0,
    unitCost: 0.0,
    mktValLCY: 0,
    untPri: 0.0,
    navPri: 0.0,
    subsFee: 0.0,
    vatAmt: 0.0,

    availUnits: 0,
    numOfUnits: 0,
    amount: 0,
    amountCur: 0,
    redFee: 0.0,
    redemptionType: undefined,
    redemptionBy: undefined,

    amt: 0,
    amtCur: 0,
    transType: '',
    traDte: '', // Transaction Date
    fndRiskProName: '',
    fxRate: 0.0,
    isCrossCurrEnabled: false,

    // TODO: [satheeqh] Need to verify language specific event here

    init: function () {
        this.set('userSettings', sharedService.userSettings);
    },

    totalValue: function () {
        return this.get('avaiQty') * this.get('untPri');
    }.property('avaiQty', 'untPri'),

    redNetAmount: function () {
        var amount = this.get('isCrossCurrEnabled') ? this.get('amountCur') : this.get('amount');
        var redPec = this.get('redPec');
        var redPercent = redPec ? redPec : 0;

        if (amount > 0) {
            return amount - (amount * redPercent / 100);
        }

        return 0;
    }.property('amount', 'amountCur', 'redPec'),

    redPercentFee: function () {
        var amount = this.get('isCrossCurrEnabled') ? this.get('amountCur') : this.get('amount');
        var redPec = this.get('redPec');
        var redPercent = redPec ? redPec : 0;

        if (amount > 0) {
            return amount * redPercent / 100;
        }

        return 0;
    }.property('amount', 'amountCur', 'redPec'),

    netAmount: function () {
        var amt = this.get('isCrossCurrEnabled') ? this.get('amtCur') : this.get('amt');
        var subPec = this.get('subPec');
        var subPercent = subPec ? subPec : 0;
        var vatAmt = this.get('vatAmt') ? this.get('vatAmt') : 0;

        if (amt > 0) {
            return amt + (amt * subPercent / 100) + vatAmt;
        }

        return 0;
    }.property('amt', 'amtCur', 'subPec', 'vatAmt'),

    subPercentFee: function () {
        var amt = this.get('isCrossCurrEnabled') ? this.get('amtCur') : this.get('amt');
        var subPec = this.get('subPec');
        var subPercent = subPec ? subPec : 0;

        if (amt > 0) {
            return amt * subPercent / 100;
        }

        return 0;
    }.property('amt', 'amtCur', 'subPec'),

    transStatus: function () {
        var sts = this.get('sts');
        var label = languageDataStore.getLanguageObj().lang.labels[['mfTransactionSts', sts].join('_')];

        return sts && label ? label : sts;
    }.property('sts', 'userSettings.currentLanguage'),

    transTypeLabel: function () {
        var transType = this.get('transType');
        var label = languageDataStore.getLanguageObj().lang.labels[['mfTransType', transType].join('_')];

        this.set('labelCss', transType === '1' ? 'up-fore-color' : 'down-fore-color');

        return transType && label ? label : transType;
    }.property('transType', 'userSettings.currentLanguage'),

    fundName: function () {
        return this.get('userSettings.currentLanguage') === 'AR' ? this.get('arabicFundName') : this.get('fndName');
    }.property('fndName', 'arabicFundName', 'userSettings.currentLanguage'),

    setData: function (data) {
        var that = this;

        Ember.$.each(data, function (key, value) {
            that.set(key, value);
        });
    }
});
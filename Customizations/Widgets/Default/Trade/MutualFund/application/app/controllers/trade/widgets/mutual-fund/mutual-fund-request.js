import Ember from 'ember';
import BaseController from '../../../base-controller';
import MutualFund from '../../../../models/trade/business-entities/mutual-fund';
import sharedService from '../../../../models/shared/shared-service';
import tradeConstants from '../../../../models/trade/trade-constants';
import mfStrategyFactory from './strategies/mutual-fund-strategy-factory';
import appEvents from '../../../../app-events';

export default BaseController.extend({
    title: '',
    actionLabel: '',
    message: '',
    currMessage: '',
    fxConvertRate: '',
    currConvert: undefined,

    accountColl: [],
    currentAccount: {},
    redemptionDropdownOptions: [],
    currentRedemptionType: {},
    redemptionByOptions: [],
    currentRedemptionBy: {},
    cusDetails: {},

    messagePopupId: '',
    messagePopup: {},

    strategy: {},
    fundReqObj: {},

    redemptionType: '',
    redemptionBy: '',

    tradeService: sharedService.getService('trade'),
    portfolioDes: sharedService.getService('trade').fieldConfigs.portfolioDes,

    mode: 1,

    popupId: '',
    subAmountId: '',
    subCurrAmountId: '',
    redAmountId: '',

    isPortCurrEnabled: true,
    isCrossCurrEnabled: false,
    isCalDisabled: false,
    isSubmitDisabled: false,

    vatRequestTimeInterval: 400,

    currencyConvertStatus: {
        Success: 1,
        Fail: 0
    },

    isVATEnabled: function () {
        return this.tradeService.userDS.isVatApplicble === tradeConstants.VATStatus.Enabled;
    }.property('tradeService.userDS.isVatApplicble'),

    transactionType: {
        Subscription: 1,
        Redemption: 2
    },

    onLoadWidget: function () {
        var wkey = this.get('wkey');
        var currentTime = new Date().getTime();

        this.set('messagePopupId', ['messagePopup', currentTime].join('-'));
        this.set('title', this.get('mode') === tradeConstants.MFSubType.Subscription ? this.app.lang.labels.subscription : this.app.lang.labels.redemption);

        this.popupId = 'mf-' + currentTime;
        this.subAmountId = 'mf-sub-' + currentTime;
        this.subCurrAmountId = 'mf-sub-curr-' + currentTime;
        this.redAmountId = 'mf-red-curr-' + currentTime;

        this.tradeService.subscribeTradeMetaReady(this, wkey);
        this.tradeService.subscribeTradeDisconnect(this, wkey);

        appEvents.subscribeLanguageChanged(this, wkey);
    },

    onAfterRender: function () {
        this.set('messagePopup', this.get(this.get('messagePopupId')));
    },

    onPrepareData: function () {
        this.prepare();
    },

    onTradeMetaReady: function () {
        this.loadContent();
    },

    onTradeDisconnect: function () {
        this.onClearData();
    },

    onLanguageChanged: function () {
        var strategy = this.get('strategy');

        this.set('title', this.get('mode') === tradeConstants.MFSubType.Subscription ? this.app.lang.labels.subscription : this.app.lang.labels.redemption);
        this.set('actionLabel', this.app.lang.labels[strategy.executeButtonLabel]);

        if (!strategy.isSubscription && strategy.redemptionTypes) {
            this.loadRedemptionTypes(strategy.redemptionTypes);
            this.loadRedemptionByOptions(strategy.redemptionByOptions);
        }

        this._setCusFundDetail();
    },

    onClearData: function () {
        this.set('accountColl', []);
        this.set('currentAccount', {});
        this.set('strategy', {});
        this.set('fundReqObj', {});
        this.set('actionLabel', '');
        this.set('title', '');
        this.set('redemptionDropdownOptions', []);
        this.set('currentRedemptionType', {});
        this.set('redemptionByOptions', []);
        this.set('currentRedemptionBy', {});
    },

    onUnloadWidget: function () {
        var wkey = this.get('wkey');

        this.set('messagePopupId', '');

        this.tradeService.unSubscribeTradeMetaReady(wkey);
        this.tradeService.unSubscribeTradeDisconnect(wkey);

        appEvents.unSubscribeLanguageChanged(wkey);
    },

    prepare: function () {
        var fund = this.get('fund');
        var mfObj = this.get('mfObj');

        this.initializeValidation();

        if (fund && fund.symbol) {
            var fundName = fund.get('fundName') ? fund.get('fundName') : fund.get('lDes');
            var availQty = fund.avaiQty;
            var fundObj = MutualFund.create(fund);

            if (mfObj) {
                availQty = mfObj.avaiQty ? mfObj.avaiQty : fund.avaiQty;
                fundObj.setData(mfObj);
            }

            fundObj.setData({
                avaiQty: availQty,
                desc: fundName
            });

            this.set('fundReqObj', fundObj);

            this.setStrategy();
            var strategy = this.get('strategy');

            this.set('actionLabel', this.app.lang.labels[strategy.executeButtonLabel]);

            if (!strategy.isSubscription && strategy.redemptionTypes) {
                this.loadRedemptionTypes(strategy.redemptionTypes);
                this.loadRedemptionByOptions(strategy.redemptionByOptions);
                this.isRedemptionFull();
            }

            this.loadPortfolioOptions(fundObj.secAccNum);
            this._setCusFundDetail();
            this._validateRequestCutoffDate();
            this.loadCurrencyOptions();
            this.changeCurrency();
        }
    },

    _updateMFObject: function () {
        var tradingAccId = this.get('currentAccount.tradingAccId');
        var fundSymbol = this.get('fundReqObj.symbol');

        if (tradingAccId && fundSymbol) {
            var mfHoldingObj = this.tradeService.holdingDS.getHolding(tradingAccId, fundSymbol, undefined, this.utils.AssetTypes.MutualFund, undefined, true);
            var avaiQty = mfHoldingObj ? mfHoldingObj.avaiQty : 0;
            var pendBuy = mfHoldingObj ? mfHoldingObj.pendBuy : 0;

            this.set('fundReqObj.avaiQty', avaiQty);
            this.set('fundReqObj.pendBuy', pendBuy);
            this.set('fundReqObj.numOfUnits', this.get('strategy.isRedemptionFull') ? avaiQty : 0);
        }
    }.observes('currentAccount'),

    loadRedemptionTypes: function (redemptionTypes) {
        var that = this;
        var types = [];

        if (redemptionTypes.length > 0) {
            Ember.$.each(redemptionTypes, function (index, option) {
                Ember.set(option, 'desc', that.app.lang.labels[option.labelKey]);
                types.pushObject(option);

                if (option.type === that.get('fundReqObj.redemptionType')) {
                    that.set('redemptionType', option.type);
                    that.set('currentRedemptionType', option);
                }
            });

            if (!this.get('currentRedemptionType.type')) {
                this.set('currentRedemptionType', types[0]);
            }
        }

        this.set('redemptionDropdownOptions', types);
    },

    loadRedemptionByOptions: function (byOptions) {
        var that = this;
        var options = [];

        if (byOptions.length > 0) {
            Ember.$.each(byOptions, function (index, option) {
                Ember.set(option, 'desc', that.app.lang.labels[option.labelKey]);
                options.pushObject(option);

                if (option.type === that.get('fundReqObj.redemptionBy')) {
                    that.set('currentRedemptionBy', option);
                }
            });

            if (!this.get('currentRedemptionBy.type')) {
                this.set('currentRedemptionBy', options[0]);
            }
        }

        this.set('redemptionByOptions', options);
    },

    setStrategy: function () {
        var strategy = mfStrategyFactory.getMFStrategy(this.get('mode'), this.get('fundReqObj'));
        this.set('strategy', strategy);

        if (!strategy.isSubscription) {
            this.set('redemptionType', tradeConstants.RedemptionType.Full);
            this.set('fundReqObj.redemptionType', tradeConstants.RedemptionType.Full);
            this.set('redemptionBy', tradeConstants.RedemptionByOption.ByUnit);
            this.set('fundReqObj.redemptionBy', tradeConstants.RedemptionByOption.ByUnit);
        }
    },

    loadPortfolioOptions: function (tradingAccId) {
        var that = this;
        var accountColl = this.tradeService.accountDS.getTradingAccCollection();
        var currentAccount = tradingAccId ? this.tradeService.accountDS.getTradingAccount(tradingAccId) : accountColl.length > 0 ? accountColl[0] : {};

        this.set('currentAccount', currentAccount);

        if (accountColl && accountColl.length > 0) {
            Ember.$.each(accountColl, function (key, port) {
                that.tradeService.sendBuyingPowerRequest(port);
            });
        }

        this.set('accountColl', accountColl);
        this._subscribePortfolioInfo();
    },

    loadCurrencyOptions: function () {
        var currencyArray = [];
        var portfolioCurrency = this.get('currentAccount.cashAccount.curr');
        var fundCurrency = this.get('fundReqObj.curr');

        if (portfolioCurrency) {
            currencyArray = [{Id: 0, DisplayName: portfolioCurrency}];
        }

        if (fundCurrency && portfolioCurrency !== fundCurrency) {
            currencyArray[currencyArray.length] = {Id: 1, DisplayName: fundCurrency};
        }

        this.set('currencyDropdownOptions', currencyArray);
        this.set('currentCurrency', currencyArray[0]);
        this.set('isPortCurrEnabled', true);
    },

    enableCrossCurrency: function () {
        Ember.run.once(this, this.changeCurrency);
    }.observes('currentAccount.cashAccount.curr', 'fundReqObj.curr', 'redemptionBy', 'redemptionType'),

    changeCurrency: function () {
        var portfolioCurrency = this.get('currentAccount.cashAccount.curr');
        var fundCurrency = this.get('fundReqObj.curr');

        this.set('fxConvertRate', '');
        this.set('isCrossCurrEnabled', portfolioCurrency !== fundCurrency);
        this.set('fundReqObj.isCrossCurrEnabled', portfolioCurrency !== fundCurrency);
        this.set('isSubmitDisabled', portfolioCurrency !== fundCurrency && !(this.get('strategy.isRedemptionFull') || this.get('fundReqObj.redemptionBy') === tradeConstants.RedemptionByOption.ByUnit));
    },

    executeRequest: function () {
        // Handled in mobile controller for now
        var fundReqObj = this.get('fundReqObj');

        if (this.get('strategy.isSubscription')) {
            fundReqObj.set('amt', this.get('isCrossCurrEnabled') ? fundReqObj.amtCur : fundReqObj.amt);
        } else {
            fundReqObj.set('amount', this.get('isCrossCurrEnabled') ? fundReqObj.amountCur : fundReqObj.amount);
        }

        var args = {fundReqObj: fundReqObj,
            fundObj: {
                portNo: this.get('currentAccount.tradingAccId'),
                curr: this.get('currentAccount.cashAccount.curr'),
                redTyp: this.get('redemptionType'),
                amtUntTyp: this.get('redemptionBy')
            }, strategy: this.get('strategy'), wkey: 'mf-' + this.get('wkey')};

        var config = {
            controllerString: 'controller:trade/widgets/mutual-fund/mutual-fund-terms-condition',
            routeString: 'trade/widgets/mutual-fund/mutual-fund-terms-condition',
            viewName: 'view:widget-popup-view',
            container: this.container
        };

        sharedService.getService('tradeUI').showPopupWidget(config, args);
    },

    _subscribePortfolioInfo: function () {
        var currentAccount = this.get('currentAccount');

        if (currentAccount && currentAccount.tradingAccId) {
            this.tradeService.accountDS.subscribeTradingAccountInfo(currentAccount);
        }
    },

    showMessagePopup: function () {
        var modal = this.get('messagePopup');
        var messagePopup = Ember.$('#' + this.get('messagePopupId'));

        modal.send('showModalPopup');
        messagePopup.draggable();
    },

    validateConditions: function () {
        var errors = [];
        var fundReqObj = this.get('fundReqObj');
        var currentAccount = this.get('currentAccount');
        var strategy = this.get('strategy');

        if (!fundReqObj.amt && !fundReqObj.amount && !fundReqObj.numOfUnits && !fundReqObj.subAmount) {
            if ((this.get('strategy.isRedemptionFull') || fundReqObj.redemptionBy === tradeConstants.RedemptionByOption.ByUnit) &&
                fundReqObj.avaiQty === 0) {
                errors[errors.length] = this.app.lang.messages.availableQtyGTZero;
            } else {
                if (!strategy.isSubscription && fundReqObj.redemptionBy === tradeConstants.RedemptionByOption.ByUnit) {
                    errors[errors.length] = this.app.lang.messages.quantityGTZero;
                } else {
                    errors[errors.length] = this.app.lang.messages.amountGTZero;
                }
            }
        }

        if (!this.get('cusDetails.Adr')) {
            errors[errors.length] = this.app.lang.messages.addressNotFound;
        }

        if (strategy.isSubscription) {
            var cashSubAmount = fundReqObj.amt + fundReqObj.subPercentFee;

            if (cashSubAmount > currentAccount.cashAccount.cashAmt) {
                errors[errors.length] = this.app.lang.messages.noSufficientCashBal;
            }

            if (fundReqObj.amt < fundReqObj.minSubAmt) {
                errors[errors.length] = this.app.lang.messages.minSubAmount + fundReqObj.curr + ' ' + fundReqObj.minSubAmt;
            }

            if (fundReqObj.maxSubAmt && fundReqObj.amt > fundReqObj.maxSubAmt) {
                errors[errors.length] = this.app.lang.messages.maxSubAmount + fundReqObj.curr + ' ' + fundReqObj.maxSubAmt;
            }

            if (fundReqObj.percentageOfHolding) {
                var maxHolding = fundReqObj.fndNav * fundReqObj.percentageOfHolding / 100;
                var holdingAmount = fundReqObj.avaiQty * fundReqObj.untPri;
                var buyPendingAmount = fundReqObj.pendBuy ? fundReqObj.pendBuy * fundReqObj.untPri : 0;
                var eligibleHoldingAmount = maxHolding - (holdingAmount + buyPendingAmount);
                var requestedAmount = fundReqObj.amt;

                if (requestedAmount > eligibleHoldingAmount) {
                    var percentageOfHolding = this.app.lang.messages.percentageOfHolding;
                    percentageOfHolding = percentageOfHolding.replace('[PercentOfHolding]', fundReqObj.percentageOfHolding + '%');

                    errors[errors.length] = percentageOfHolding;
                }
            }
        } else {
            if (fundReqObj.redemptionBy === tradeConstants.RedemptionByOption.ByAmount) {
                var totalValue = fundReqObj.get('totalValue');

                if (fundReqObj.amount > totalValue) {
                    errors[errors.length] = this.app.lang.messages.noSufficientAmount;
                }

                var minAvlAmtForRdm = totalValue - fundReqObj.amount;

                if (minAvlAmtForRdm > 0 && minAvlAmtForRdm < fundReqObj.minAvblAmtForRdm) {
                    var minRedAmountMessage = this.app.lang.messages.minRedAmount;
                    minRedAmountMessage = minRedAmountMessage.replace('****', fundReqObj.minAvblAmtForRdm);

                    errors[errors.length] = minRedAmountMessage;
                }
            } else {
                if (fundReqObj.numOfUnits > fundReqObj.avaiQty) {
                    errors[errors.length] = this.app.lang.messages.noSufficientUnits;
                }

                var minAvlUtsForRdm = fundReqObj.avaiQty - fundReqObj.numOfUnits;
                var ratioFactor = fundReqObj.ratioFactr ? fundReqObj.ratioFactr : 1;
                var minAvlUtsAfterRdm = fundReqObj.minAvblUntsForRdm * ratioFactor;

                if (minAvlUtsForRdm > 0 && minAvlUtsForRdm < minAvlUtsAfterRdm) {
                    var minRedUnitMessage = this.app.lang.messages.minRedUnit;
                    minRedUnitMessage = minRedUnitMessage.replace('****', minAvlUtsAfterRdm);

                    errors[errors.length] = minRedUnitMessage;
                }
            }
        }

        return errors;
    },

    _validateRequestCutoffDate: function () {
        var fundReqObj = this.get('fundReqObj');

        if (fundReqObj.cutoffDate) {
            var dateTimeFormat = 'YYYY-MM-DD HH:mm:ss';
            var reqCutOffDate = this.utils.formatters.formatToDateTime(fundReqObj.cutoffDate, undefined, dateTimeFormat);
            var cutOffDate = new Date(reqCutOffDate);
            var today = new Date();
            var nextValDate = '';

            if (today > cutOffDate) {
                if (fundReqObj.nextNavDte) {
                    nextValDate = this.utils.formatters.formatToDateTime(fundReqObj.nextNavDte);
                }

                this.set('message', [this.app.lang.messages.lowReqCutOffDt, nextValDate].join(''));
            }
        }
    },

    isRedemptionFull: function () {
        var fundReqObj = this.get('fundReqObj');

        if (this.get('redemptionType') === tradeConstants.RedemptionType.Full) {
            fundReqObj.set('numOfUnits', fundReqObj.get('avaiQty'));
        } else {
            fundReqObj.set('numOfUnits', 0);
        }
    }.observes('redemptionType'),

    _setCusFundDetail: function () {
        var cusDetails = this.tradeService.userDS.cusDetails;
        var fundReqObj = this.get('fundReqObj');
        var currentLanguage = sharedService.userSettings.currentLanguage;

        this.set('fundReqObj.desc', currentLanguage === 'AR' ? fundReqObj.arabicFundName : fundReqObj.fndName);
        this.set('fundReqObj.fndRiskProName', this.app.lang.labels[['fndRiskPro', fundReqObj.fndRiskPro].join('_')]);

        if (cusDetails) {
            this.set('cusDetails', cusDetails);
            this.set('cusDetails.cusAccTyp', this.app.lang.labels[['cusAccType', cusDetails.CusAccTyp].join('_')]);
            this.set('cusDetails.fullName', currentLanguage === 'AR' && cusDetails.arFullName ? cusDetails.arFullName : cusDetails.name);
            this.set('cusDetails.cusAddress', currentLanguage === 'AR' && cusDetails.arAddress ? cusDetails.arAddress : cusDetails.Adr);
        }
    },

    setFxStatus: function () {
        var sts = this.get('currConvert.sts');

        if (sts === this.currencyConvertStatus.Success) {
            this.set('msgCss', 'highlight-fore-color');
            this.set('currMessage', this.app.lang.messages.currConvertWaiting);

            var timer = Ember.run.later(this, this.onFxFail, 60000);
            this.set('timer', timer);
        } else if (sts === this.currencyConvertStatus.Fail) {
            this.onFxFail();
        }
    }.observes('currConvert.sts'),

    onFxFail: function () {
        this.set('msgCss', 'down-fore-color');
        this.set('currMessage', this.app.lang.messages.currConvertFailed);
        this.set('isCalDisabled', false);
    },

    setFxRate: function () {
        var amnt = this.get('currConvert.amnt');
        var fxRate = this.get('currConvert.fxRate');

        if (amnt !== undefined && fxRate !== undefined) {
            if (this.get('strategy.isSubscription')) {
                if (this.get('isPortCurrEnabled')) {
                    this.set('fundReqObj.amt', amnt);
                } else {
                    this.set('fundReqObj.amtCur', amnt);
                }
            } else {
                if (this.get('isPortCurrEnabled')) {
                    this.set('fundReqObj.amount', amnt);
                } else {
                    this.set('fundReqObj.amountCur', amnt);
                }
            }

            this.set('msgCss', 'fore-color bold');
            this.set('fxConvertRate', this.app.lang.labels.fxRate + ' ' + fxRate);
            this.set('isCalDisabled', false);
            this.set('isSubmitDisabled', false);
            this.set('currMessage', '');

            Ember.run.cancel(this.get('timer'));
        }
    }.observes('currConvert.amnt', 'currConvert.fxRate'),

    _resetFields: function () {
        if (this.get('strategy.isSubscription')) {
            this.set('fundReqObj.amt', 0);
            this.set('fundReqObj.amtCur', 0);
        } else {
            this.set('fundReqObj.amount', 0);
            this.set('fundReqObj.amountCur', 0);
        }

        this.set('fundReqObj.vatAmt', 0);
    },

    initializeValidation: function () {
        this.set('isPortCurrEnabled', true);
        this.set('isCrossCurrEnabled', false);
        this.set('isCalDisabled', false);
        this.set('isSubmitDisabled', false);
        this.set('message', '');
        this.set('currMessage', '');
        this.set('fxConvertRate', '');

        this.set('currConvert', undefined);
        Ember.run.cancel(this.get('timer'));
    },

    calculateVAT: function () {
        if (this.get('isVATEnabled')) {
            Ember.run.debounce(this, this.sendVATRequest, this.vatRequestTimeInterval);
        }
    }.observes('fundReqObj.amt', 'fundReqObj.amtCur'),

    sendVATRequest: function () {
        var fundObj = this.get('fundReqObj');
        var subAmount = this.get('isCrossCurrEnabled') ? this.get('fundReqObj.amtCur') : this.get('fundReqObj.amt');
        var unqReqId = new Date().getTime().toString();

        if (subAmount > 0) {
            this.tradeService.sendMutualFundVATRequest(unqReqId, fundObj, {fndId: fundObj.symbol, amt: subAmount, portNo: this.get('currentAccount.tradingAccId'), unqReqId: unqReqId});
        } else {
            this.set('fundReqObj.vatAmt', 0);
        }
    },

    actions: {
        onExecute: function () {
            var errors = this.validateConditions();

            if (errors && errors.length > 0) {
                this.set('errorMessages', errors);
                this.showMessagePopup();
            } else {
                this.executeRequest();

                if (this.widgetPopupView) {
                    this.widgetPopupView.send('closePopup');
                }
            }
        },

        onPortfolioChanged: function (account) {
            this.set('currentAccount', account);

            this.loadCurrencyOptions();
            this._resetFields();
        },

        onCurrencyChanged: function (currency) {
            this.set('currentCurrency', currency);
            this.set('isPortCurrEnabled', currency.DisplayName === this.get('currentAccount.cashAccount.curr'));

            this._resetFields();
        },

        onRedemptionTypeChanged: function (option) {
            var fundReqObj = this.get('fundReqObj');

            this.set('currentRedemptionType', option);
            this.set('redemptionType', option.type);
            fundReqObj.set('redemptionType', option.type);

            if (option.type === tradeConstants.RedemptionType.Full) {
                // To reset by unit on redemption type full selected
                fundReqObj.set('redemptionBy', tradeConstants.RedemptionByOption.ByUnit);
                fundReqObj.set('amount', 0);
                this.set('redemptionBy', tradeConstants.RedemptionByOption.ByUnit);
                this.loadRedemptionByOptions(this.get('strategy.redemptionByOptions'));
            }
        },

        onRedemptionByChanged: function (option) {
            var fundReqObj = this.get('fundReqObj');

            this.set('currentRedemptionBy', option);
            this.set('redemptionBy', option.type);
            fundReqObj.set('redemptionBy', option.type);
            fundReqObj.set('amount', 0);

            if (!this.get('strategy.isRedemptionFull')) {
                fundReqObj.set('numOfUnits', 0);
            }
        },

        onCalculateCrossCurrency: function () {
            var amount;
            var transType = this.transactionType.Subscription;

            if (this.get('strategy.isSubscription')) {
                amount = this.get('isPortCurrEnabled') ? this.get('fundReqObj.amtCur') : this.get('fundReqObj.amt');
            } else {
                amount = this.get('isPortCurrEnabled') ? this.get('fundReqObj.amountCur') : this.get('fundReqObj.amount');
                transType = this.transactionType.Redemption;
            }

            if (amount > 0) {
                var unqReqId = Math.floor(Date.now()).toString();
                var currConvert = Ember.Object.create({id: unqReqId});
                var fromCurrency = this.get('isPortCurrEnabled') ? this.get('currentAccount.cashAccount.curr') : this.get('fundReqObj.curr');
                var toCurrency = this.get('isPortCurrEnabled') ? this.get('fundReqObj.curr') : this.get('currentAccount.cashAccount.curr');

                this.set('currConvert', currConvert);
                this.set('currMessage', this.app.lang.messages.pleaseWait);
                this.set('msgCss', 'highlight-fore-color');
                this.set('isCalDisabled', true);
                this.set('currConvert.sts', '');
                this.set('isSubmitDisabled', true);

                this.tradeService.sendCurrenyRateRequest(unqReqId, currConvert, {
                    fromCurr: fromCurrency,
                    toCurr: toCurrency,
                    transType: transType,
                    amnt: amount,
                    sMubNo: this.tradeService.userDS.get('mubNo'),
                    unqReqId: unqReqId,
                    usrId: this.tradeService.userDS.usrId
                });
            } else {
                this.set('currMessage', this.app.lang.messages.amountGTZero);
                this.set('msgCss', 'down-fore-color');
            }
        }
    }
});
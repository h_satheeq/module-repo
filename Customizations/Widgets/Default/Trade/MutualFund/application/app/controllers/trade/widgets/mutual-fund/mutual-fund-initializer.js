import BaseModuleInitializer from '../../../../models/shared/initializers/base-module-initializer';
import sharedService from '../../../../models/shared/shared-service';
import MutualFundDataStore from '../../../../models/trade/data-stores/mutual-fund-data-store';

export default BaseModuleInitializer.extend({
    postInitialize: function () {
        var tradeService = sharedService.getService('trade');
        var fundDS = MutualFundDataStore.create({tradeService: tradeService});

        fundDS.initialize();
        tradeService.set('mutualFundDS', fundDS);
    }
});
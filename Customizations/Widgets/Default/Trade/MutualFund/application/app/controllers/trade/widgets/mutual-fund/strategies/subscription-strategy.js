import Ember from 'ember';

export default Ember.Object.extend({
    panelClass: 'up-back-color',
    btnClass: 'btn-buy',
    executeButtonLabel: 'subscribe',
    titleKey: 'subscription',

    isSubscription: true,

    reqObj: {}
});
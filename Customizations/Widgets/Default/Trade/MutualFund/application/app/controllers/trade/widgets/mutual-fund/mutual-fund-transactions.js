import Ember from 'ember';
import TradeTableController from '../../controllers/trade-table-controller';
import sharedService from '../../../../models/shared/shared-service';
import tradeWidgetConfig from '../../../../config/trade-widget-config';

// Cell Views
import ClassicHeaderCell from '../../../../views/table/classic-header-cell';
import ButtonCell from '../../../../views/table/button-cell';
import ClassicCell from '../../../../views/table/classic-cell';
import ClassicMappingCell from '../../../../views/table/trade/classic-mapping-cell';
import ClassicStatusCell from '../../../../views/table/trade/classic-status-cell';
import OrderPriceCell from '../../../../views/table/trade/order-price-cell';

export default TradeTableController.extend({
    DefaultSearchDays: 365,
    mode: null,

    fundList: Ember.A(),
    fromDate: '',
    toDate: '',

    isRenderingEnabled: false,
    fixedColumns: tradeWidgetConfig.mutualFundTransaction.tableParams.numOfFixedColumns,
    defaultColumnIds: [],

    sortProperties: ['traDte'],
    sortAscending: false,

    transactionMode: {
        Pending: 1,
        Done: 6
    },

    tradeService: sharedService.getService('trade'),

    onAfterRender: function () {
        this.set('isRenderingEnabled', true);
    },

    onLoadWidget: function () {
        var mode = this.get('mode');
        this.set('title', mode === this.transactionMode.Done ? this.app.lang.labels.transactions : this.app.lang.labels.pendingTransactions);
        this.set('defaultColumnIds', mode === this.transactionMode.Done ? tradeWidgetConfig.mutualFundTransaction.defaultColumnIds : tradeWidgetConfig.mutualFundSubRedem.defaultColumnIds);
        this.set('defaultColumnMapping', mode === this.transactionMode.Done ? tradeWidgetConfig.mutualFundTransaction.defaultColumnMapping : tradeWidgetConfig.mutualFundSubRedem.defaultColumnMapping);

        var today = new Date();
        this.set('toDate', today);

        this.setErrorMessage();
        this.setCellViewsScopeToGlobal();
        this.setLangLayoutSettings(sharedService.userSettings.currentLanguage);

        this.setDefaultColumns();
        this.setDefaultColumnDeclarations();

        // Get from date using default search days
        var fromDate = new Date();
        fromDate.setDate(fromDate.getDate() - this.DefaultSearchDays);
        this.set('fromDate', fromDate);
    },

    onPrepareData: function () {
        this.set('sortProperties', ['transNo']);
        this.setRequestTimeout(4, 'content.length');

        var transactions = this.tradeService.mutualFundDS.getTransactionCollection(this.get('mode'));
        this.set('content', transactions);
        this.set('masterContent', transactions);
    },

    onCheckDataAvailability: function () {
        return this.get('content').length !== 0;
    },

    onAddSubscription: function () {
        // TODO: [satheeqh] Need to add fromDate, toDate date-picker
        this.tradeService.sendFundTransactionsRequest({
            fromDate: this.utils.formatters.convertToDisplayTimeFormat(this.get('fromDate'), 'YYYYMMDD'),
            toDate: this.utils.formatters.convertToDisplayTimeFormat(this.get('toDate'), 'YYYYMMDD')});
    },

    onLanguageChanged: function () {
        this.set('columnDeclarations', []);
        this.onLoadWidget();
    },

    onClearData: function () {
        this.set('content', Ember.A());
        this.set('masterContent', Ember.A());
        this.set('columnDeclarations', []);
    },

    onReloadData: function () {
        this.onAddSubscription();
        this.setRequestTimeout(4, 'content.length');
    },

    setCellViewsScopeToGlobal: function () {
        Ember.ClassicHeaderCell = ClassicHeaderCell;
        Ember.ClassicCell = ClassicCell;
        Ember.ClassicMappingCell = ClassicMappingCell;
        Ember.ClassicStatusCell = ClassicStatusCell;
        Ember.ButtonCell = ButtonCell;
        Ember.OrderPriceCell = OrderPriceCell;
    },

    cellViewsForColumns: {
        button: 'Ember.ButtonCell',
        classicMappingCell: 'Ember.ClassicMappingCell',
        classicStatusCell: 'Ember.ClassicStatusCell',
        classicCell: 'Ember.ClassicCell',
        orderPriceCell: 'Ember.OrderPriceCell'
    },

    actions: {
        sort: function (column) {
            if (!column.get('isSortSupported')) {
                return;
            }

            if (this.get('sortColumn') !== column) {
                this.get('columns').setEach('isSorted', false);
                column.set('isSorted', true);
                this.set('sortColumn', column);
                this.set('sortProperties', [column.get('sortKey')]);
                this.set('isSortApplied', true);
            } else if (this.get('sortColumn') === column) {
                // Handle disabling sorts
                if (this.get('sortAscending') === true) {
                    this.set('sortColumn', undefined);
                    this.set('sortAscending', false);
                    column.set('isSorted', false);
                    this.set('isSortApplied', false);
                    this.set('sortProperties', []);
                } else {
                    this.set('sortProperties', [column.get('sortKey')]);
                    this.toggleProperty('sortAscending');
                }
            }
        }
    }
});

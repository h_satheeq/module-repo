import SubscriptionStrategy from './subscription-strategy';
import tradeConstants from '../../../../../models/trade/trade-constants';

export default SubscriptionStrategy.extend({
    panelClass: 'down-back-color',
    btnClass: 'btn-sell',
    executeButtonLabel: 'redeem',
    titleKey: 'redemption',

    isSubscription: false,

    redemptionTypes: [{type: tradeConstants.RedemptionType.Full, labelKey: 'fullRedemption'},
        {type: tradeConstants.RedemptionType.Part, labelKey: 'partRedemption'}],

    redemptionByOptions: [{type: tradeConstants.RedemptionByOption.ByUnit, labelKey: 'byUnits'},
        {type: tradeConstants.RedemptionByOption.ByAmount, labelKey: 'byAmount'}],

    isRedemptionFull: function () {
        var reqObj = this.get('reqObj');

        return reqObj && reqObj.redemptionType === tradeConstants.RedemptionType.Full;
    }.property('reqObj.redemptionType'),

    isByAmount: function () {
        var reqObj = this.get('reqObj');

        return reqObj && reqObj.redemptionBy === tradeConstants.RedemptionByOption.ByAmount;
    }.property('reqObj.redemptionBy')
});
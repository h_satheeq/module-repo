import Ember from 'ember';
import SubscriptionStrategy from './subscription-strategy';
import RedemptionStrategy from './redemption-strategy';
import tradeConstants from '../../../../../models/trade/trade-constants';

export default Ember.Object.extend({
    getMFStrategy: function (mode, reqObj) {
        var strategy;

        switch (mode) {
            case tradeConstants.MFSubType.Subscription:
                strategy = SubscriptionStrategy.create({reqObj: reqObj});
                break;

            case tradeConstants.MFSubType.Redemption:
                strategy = RedemptionStrategy.create({reqObj: reqObj});
                break;

            default:
                strategy = SubscriptionStrategy.create({reqObj: reqObj});
                break;
        }

        return strategy;
    }
}).create();
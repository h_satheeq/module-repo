import Ember from 'ember';
import TradeTableController from '../../controllers/trade-table-controller';
import tradeConstants from '../../../../models/trade/trade-constants';
import sharedService from '../../../../models/shared/shared-service';
import tradeWidgetConfig from '../../../../config/trade-widget-config';
import utils from '../../../../utils/utils';

// Cell Views
import ClassicHeaderCell from '../../../../views/table/classic-header-cell';
import ButtonCell from '../../../../views/table/button-cell';
import ClassicCell from '../../../../views/table/classic-cell';
import ClassicMappingCell from '../../../../views/table/trade/classic-mapping-cell';
import ClassicStatusCell from '../../../../views/table/trade/classic-status-cell';
import OrderPriceCell from '../../../../views/table/trade/order-price-cell';
import ClassicLinkCell from '../../../../views/table/trade/classic-link-cell';

export default TradeTableController.extend({
    masterContent: Ember.A(),
    textFilter: '',

    isRenderingEnabled: false,
    fixedColumns: tradeWidgetConfig.mutualFundHolding.tableParams.numOfFixedColumns,
    defaultColumnIds: tradeWidgetConfig.mutualFundHolding.defaultColumnIds,

    tradeService: sharedService.getService('trade'),

    onLoadWidget: function () {
        this.tradeService.subscribeTradeMetaReady(this, this.get('wkey'));

        this.setErrorMessage();
        this.setCellViewsScopeToGlobal();
        this.set('defaultColumnMapping', tradeWidgetConfig.mutualFundHolding.defaultColumnMapping);
        this.setLangLayoutSettings(sharedService.userSettings.currentLanguage);

        this.setDefaultColumns();
        this.setDefaultColumnDeclarations();
    },

    onAddSubscription: function () {
        this._sendHoldingRequest();
        this.tradeService.sendFundListRequest();
    },

    onPrepareData: function () {
        this.set('sortProperties', ['symbol']);
        this.set('sortAscending', true);
        this.setRequestTimeout(tradeConstants.TimeIntervals.LoadingInterval, 'content.length');

        var mfHoldings = this.tradeService.holdingDS.getHoldingsByInstType(utils.AssetTypes.MutualFund);
        this.set('content', mfHoldings);
        this.set('masterContent', mfHoldings);
    },

    onCheckDataAvailability: function () {
        return this.get('content').length !== 0;
    },

    onAfterRender: function () {
        this.set('isRenderingEnabled', true);
    },

    onLanguageChanged: function () {
        this.set('columnDeclarations', []);
        this.onLoadWidget();
    },

    onTradeMetaReady: function () {
        this.onPrepareData();
        this.onAddSubscription();
    },

    onClearData: function () {
        this.set('content', Ember.A());
        this.set('masterContent', Ember.A());
        this.set('textFilter', '');
        this.set('columnDeclarations', []);
    },

    onReloadData: function () {
        this.onAddSubscription();
        this.setRequestTimeout(4, 'content.length');
    },

    setCellViewsScopeToGlobal: function () {
        Ember.ClassicHeaderCell = ClassicHeaderCell;
        Ember.ClassicCell = ClassicCell;
        Ember.ClassicMappingCell = ClassicMappingCell;
        Ember.ClassicStatusCell = ClassicStatusCell;
        Ember.ButtonCell = ButtonCell;
        Ember.OrderPriceCell = OrderPriceCell;
        Ember.ClassicLinkCell = ClassicLinkCell;
    },

    cellViewsForColumns: {
        button: 'Ember.ButtonCell',
        classicMappingCell: 'Ember.ClassicMappingCell',
        classicStatusCell: 'Ember.ClassicStatusCell',
        classicCell: 'Ember.ClassicCell',
        orderPriceCell: 'Ember.OrderPriceCell',
        classicLinkCell: 'Ember.ClassicLinkCell'
    },

    filterList: (function () {
        Ember.run.once(this, this._filterList);
    }).observes('textFilter'),

    _filterList: function () {
        var textFilter = this.utils.validators.isAvailable(this.get('textFilter')) ? this.get('textFilter') : false;  // If any filter is false, that means that filter is not applied

        if (this.utils.validators.isAvailable(textFilter)) {
            var filteredStocks = this.get('masterContent').filter((function (that) {    //eslint-disable-line
                return function (stock) {
                    return that.checkFilterMatch(stock, textFilter);
                };
            })(this));

            this.set('content', filteredStocks);        // Need to capture filter removing event to avoid 'set' without filters
        } else {
            this.set('content', this.get('masterContent'));
        }
    },

    checkFilterMatch: function checkFilterMatch(stock, textFilter) {
        var field;
        var isMatchedTextFilter = !textFilter;  // If a argument is false, that means that filter is not applied

        if (!isMatchedTextFilter) {
            for (field in stock) {
                if (stock.hasOwnProperty(field) && (field === 'symbol' || field === 'fndName' || field === 'arabicFundName' || field === 'code')) {
                    var fieldValue = stock[field];

                    if (fieldValue) {
                        var stringValue = fieldValue.toString();
                        var value = stringValue.trim();

                        if (value && value.slice(0, textFilter.length).toLowerCase() === textFilter.toLowerCase()) {
                            isMatchedTextFilter = true;
                        }
                    }
                }
            }
        }

        return isMatchedTextFilter;
    },

    loadMFRequestMenu: function (fund, mode) {
        if (fund.get('isSubRedEnabled')) {
            var popupDimensions;
            var mutualFundSubscriptionPopup = {w: 345, h: 490};
            var mutualFundRedemptionPopup = {w: 345, h: 570};

            if (tradeConstants.MFSubType.Subscription === mode) {
                popupDimensions = mutualFundSubscriptionPopup;
            } else {
                popupDimensions = mutualFundRedemptionPopup;
            }

            var mfObj = this.tradeService.mutualFundDS.getFund(fund.symbol, true);

            var args = {
                fund: fund,
                mfObj: mfObj,
                mode: mode,
                wkey: 'mf-' + this.get('wkey'),
                dimensions: popupDimensions
            };
            var config = {
                controllerString: 'controller:trade/widgets/mutual-fund/mutual-fund-request',
                routeString: 'trade/widgets/mutual-fund/mutual-fund-request',
                viewName: 'view:widget-popup-view',
                container: this.container
            };

            sharedService.getService('tradeUI').showPopupWidget(config, args);
        }
    },

    loadAssetManagement: function (row, language) {
        var windowSize = 'width=700,height=500';
        window.open(sharedService.getService('price').settings.urlTypes['astMgmt' + language], 'AssetManagementWindow', windowSize);
    },

    _sendHoldingRequest: function () {
        var that = this;
        var portfolios = this.tradeService.accountDS.getTradingAccCollection();

        if (portfolios && portfolios.length > 0) {
            Ember.$.each(portfolios, function (key, port) {
                that.tradeService.sendHoldingsRequest(port);
            });
        }
    },

    actions: {
        sort: function (column) {
            if (!column.get('isSortSupported')) {
                return;
            }

            if (this.get('sortColumn') !== column) {
                this.get('columns').setEach('isSorted', false);
                column.set('isSorted', true);
                this.set('sortColumn', column);
                this.set('sortProperties', [column.get('sortKey')]);
                this.set('isSortApplied', true);
            } else if (this.get('sortColumn') === column) {
                // Handle disabling sorts
                if (this.get('sortAscending') === true) {
                    this.set('sortColumn', undefined);
                    this.set('sortAscending', false);
                    column.set('isSorted', false);
                    this.set('isSortApplied', false);
                    this.set('sortProperties', []);
                } else {
                    this.set('sortProperties', [column.get('sortKey')]);
                    this.toggleProperty('sortAscending');
                }
            }
        },

        clickRow: function (selectedRow) {
            this.set('rowData', selectedRow ? selectedRow.content : {});
        },

        onSubscription: function () {
            this.loadMFRequestMenu(this.get('rowData'), tradeConstants.MFSubType.Subscription);
        },

        onRedemption: function () {
            this.loadMFRequestMenu(this.get('rowData'), tradeConstants.MFSubType.Redemption);
        },

        onAssetManagement: function () {
            this.loadAssetManagement(this.get('rowData'), sharedService.userSettings.currentLanguage);
        }
    }
});

import Ember from 'ember';
import BaseController from '../../../base-controller';
import appEvents from '../../../../app-events';
import sharedService from '../../../../models/shared/shared-service';
import tradeConstants from '../../../../models/trade/trade-constants';

export default BaseController.extend({
    fundReqObj: {},
    strategy: {},
    messagePopup: {},

    isBtnDisabled: true,
    isTransExecuted: false,
    isTCNotAccepted: undefined,
    isTCNotClicked: true,
    confirmClose: true,

    messagePopupId: '',
    title: '',

    success: 1,

    termsAndCondition: {
        tCNotAccepted: -2,
        tCChanged: 0,
        tCAlreadyAccepted: 1
    },

    fundNameLabel: {
        EN: 'fndName',
        AR: 'arabicFundName'
    },

    tradeService: sharedService.getService('trade'),

    redemptionType: function () {
        var that = this;
        var redType = this.get('fundReqObj.redemptionType');
        var strategy = this.get('strategy');
        var typeDesc;

        if (redType && strategy && !strategy.isSubscription && strategy.redemptionTypes) {
            Ember.$.each(strategy.redemptionTypes, function (key, option) {
                if (option.type === redType) {
                    typeDesc = that.app.lang.labels[option.labelKey];

                    return false;
                }
            });
        }

        return typeDesc;
    }.property('fundReqObj.redemptionType'),

    redemptionBy: function () {
        var that = this;
        var redType = this.get('fundReqObj.redemptionBy');
        var strategy = this.get('strategy');
        var byDesc;

        if (redType && strategy && !strategy.isSubscription && strategy.redemptionByOptions) {
            Ember.$.each(strategy.redemptionByOptions, function (key, option) {
                if (option.type === redType) {
                    byDesc = that.app.lang.labels[option.labelKey];

                    return false;
                }
            });
        }

        return byDesc;
    }.property('fundReqObj.redemptionBy'),

    isVATEnabled: function () {
        return this.tradeService.userDS.isVatApplicble === tradeConstants.VATStatus.Enabled;
    }.property('tradeService.userDS.isVatApplicble'),

    onLoadWidget: function () {
        var wkey = this.get('wkey');
        var currentTime = new Date().getTime(); // Need when creating multiple popups at the same time

        appEvents.subscribeLanguageChanged(this, wkey);

        this._setTitle();

        this.set('isTCNotClicked', true);
        this.set('message', '');
        this.set('msgColor', '');
        this.set('messagePopupId', ['messagePopup', currentTime].join('-'));
    },

    onAddSubscription: function () {
        var that = this;
        var fundReqObj = this.get('fundReqObj');

        this.tradeService.sendFundTermsConditionGetRequest({fundName: fundReqObj.code}, function (respObj) {
            that.onTermsConditionGetResponse(respObj);
        });
    },

    onAfterRender: function () {
        this.set('messagePopup', this.get(this.get('messagePopupId')));
    },

    onTermsConditionGetResponse: function (respObj) {
        if (respObj) {
            var acceptedDate = this.utils.formatters.formatToDateTime(respObj.acceptedDate);

            if (respObj.tcAcceptedStatus === this.termsAndCondition.tCAlreadyAccepted) {
                var tCAlreadyAcceptedMessage = [this.app.lang.messages.tCAlreadyAccepted, acceptedDate].join('');

                this.set('termsConditionMessage', tCAlreadyAcceptedMessage);
                this.set('isTCNotAccepted', false);
                this.set('isBtnDisabled', false);
            } else if (respObj.tcAcceptedStatus === this.termsAndCondition.tCChanged) {
                var tCChangedMessage = this.app.lang.messages.tCChanged;
                tCChangedMessage = tCChangedMessage.replace('*****', acceptedDate);

                this.set('termsConditionMessage', tCChangedMessage);
                this.set('isTCNotAccepted', true);
            } else if (respObj.tcAcceptedStatus === this.termsAndCondition.tCNotAccepted) {
                this.set('termsConditionMessage', this.app.lang.messages.tCNotAccepted);
                this.set('isTCNotAccepted', true);
            } else {
                this.set('message', this.app.lang.messages.tCGetError);
                this.set('msgColor', 'down-fore-color');
            }
        }
    },

    onTermsConditionUpdateResponse: function (respObj) {
        if (respObj) {
            if (respObj.sts === this.success) {
                this.set('isTCNotAccepted', false);
                this.set('isBtnDisabled', false);
                this.set('termsConditionMessage', this.app.lang.messages.tCAccepted);
            } else {
                this.set('message', this.app.lang.messages.tCUpdateError);
                this.set('msgColor', 'down-fore-color');
            }
        }
    },

    onSubscriptionResponse: function (respObj, rejRsn) {
        if (respObj) {
            if (respObj.sts === this.success) {
                this.set('message', this.app.lang.messages.subscriptionSuccessMessage);
                this.set('msgColor', 'up-fore-color');
            } else {
                this.set('message', rejRsn);
                this.set('msgColor', 'down-fore-color');
            }
        }
    },

    onRedemptionResponse: function (respObj, rejRsn) {
        if (respObj) {
            if (respObj.sts === this.success) {
                this.set('message', this.app.lang.messages.redemptionSuccessMessage);
                this.set('msgColor', 'up-fore-color');
            } else {
                this.set('message', rejRsn);
                this.set('msgColor', 'down-fore-color');
            }
        }
    },

    onLanguageChanged: function () {
        this._setTitle();
    },

    onUnloadWidget: function () {
        this.set('messagePopupId', '');

        appEvents.unSubscribeLanguageChanged(this.get('wkey'));
    },

    showConfirmationPopup: function () {
        var that = this;

        Ember.run.next(function () {
            that.utils.messageService.showMessage(that.app.lang.messages.confirmTransactionCancel,
                that.utils.Constants.MessageTypes.Warning,
                false,
                that.app.lang.labels.confirm,
                [{type: that.utils.Constants.MessageBoxButtons.Yes, btnAction: that.onConfirmCancel.bind(that)},
                    {type: that.utils.Constants.MessageBoxButtons.No}]
            );
        });
    },

    executeTransaction: function () {
        var that = this;

        this.set('isBtnDisabled', true);

        Ember.run.next(function () {
            var fundReqObj = that.get('fundReqObj');
            var fundObj = that.get('fundObj');

            if (that.get('strategy.isSubscription')) {
                that.tradeService.sendMFSubscriptionRequest({
                    mubNo: that.tradeService.userDS.get('mubNo'),
                    fndId: fundReqObj.symbol,
                    amt: fundReqObj.amt,
                    subsFee: fundReqObj.get('subPercentFee'),
                    portNo: fundObj.portNo
                }, function (respObj, rejRsn) {
                    that.onSubscriptionResponse(respObj, rejRsn);
                });
            } else {
                var ratioFactor = fundReqObj.ratioFactr ? fundReqObj.ratioFactr : 1;
                var numOfUnits = parseInt(fundReqObj.numOfUnits / ratioFactor, 10);

                that.tradeService.sendMFRedemptionRequest({
                    mubNo: that.tradeService.userDS.get('mubNo'),
                    fndId: fundReqObj.symbol,
                    amt: fundReqObj.amount,
                    redFee: fundReqObj.get('redPercentFee'),
                    portNo: fundObj.portNo,
                    redTyp: fundObj.redTyp,
                    amtUntTyp: fundObj.amtUntTyp,
                    units: numOfUnits
                }, function (respObj, rejRsn) {
                    that.onRedemptionResponse(respObj, rejRsn);
                });
            }

            that.set('isTransExecuted', true);
            that.set('confirmClose', false);
        });

        this.set('message', this.app.lang.messages.pleaseWait);
        this.set('msgColor', 'highlight-fore-color');
    },

    onConfirmCancel: function () {
        var fundReqObj = this.get('fundReqObj');

        this.set('confirmClose', false);

        if (this.get('strategy.isSubscription')) {
            fundReqObj.set('amt', 0);
        } else {
            fundReqObj.set('amount', 0);

            if (!this.get('strategy.isRedemptionFull')) {
                fundReqObj.set('numOfUnits', 0);
            }
        }

        if (this.widgetPopupView) {
            this.widgetPopupView.send('closePopup');
        }
    },

    _setTitle: function () {
        var fundReqObj = this.get('fundReqObj');

        this.set('title', this.app.lang.labels.termAndCondition);
        this.set('fundReqObj.desc', fundReqObj[this.fundNameLabel[sharedService.userSettings.currentLanguage]]);
    },

    showMessagePopup: function () {
        var modal = this.get('messagePopup');
        var messagePopup = Ember.$('#' + this.get('messagePopupId'));

        modal.send('showModalPopup');
        messagePopup.draggable();
    },

    actions: {
        onContinue: function () {
            this.executeTransaction();
        },

        onAccept: function () {
            if (this.get('isTCNotClicked')) {
                this.set('errorMessages', [this.app.lang.messages.downloadTermsConditions]);

                this.showMessagePopup();
            } else {
                var that = this;
                var fundReqObj = this.get('fundReqObj');

                this.tradeService.sendFundTermsConditionUpdateRequest({fundName: fundReqObj.code}, function (respObj) {
                    that.onTermsConditionUpdateResponse(respObj);
                });
            }
        },

        onCancel: function () {
            if (!this.get('isTransExecuted')) {
                this.showConfirmationPopup();
            } else {
                this.onConfirmCancel();
            }

            this.set('isTransExecuted', false);
        },

        onClickTC: function () {
            this.set('isTCNotClicked', false);

            var agreementPdfName = ['tcPath', sharedService.userSettings.currentLanguage].join('');
            var agreementPdf = this.get('fundReqObj')[agreementPdfName];

            window.open('fundTC/' + agreementPdf, '_system', 'location=yes');
        }
    }
});
/* global Queue */
import Ember from 'ember';
import SocketResponseHandler from '../../../shared/protocols/web-socket/socket-response-handler';
import tradeConstants from '../../trade-constants';
import sharedService from '../../../shared/shared-service';
import utils from '../../../../utils/utils';
import languageDataStore from '../../../../models/shared/language/language-data-store';

export default SocketResponseHandler.extend({
    exchangeConfig: {
        tif: '1',
        orderSide: '2',
        orderType: '3'
    },

    exchangeRule: {
        tifByType: '0'
    },

    init: function () {
        var that = this;

        this.set('tradeService', sharedService.getService('trade'));
        this.lang = languageDataStore.getLanguageObj().get('lang');

        this._super();
        this.inputQueue = new Queue();

        this.processTimer = setTimeout(function () {
            that.processResponse();
        }, tradeConstants.TimeIntervals.WebSocketInQueueProcessingInterval);
    },

    processResponse: function () {
        while (this.inputQueue.getLength() > 0) {
            var frameBuffer = this.inputQueue.dequeue();

            utils.logger.logDebug('Received frame : ' + frameBuffer);

            try {
                var frameMessage = JSON.parse(frameBuffer);

                try {
                    this._processMessage(frameMessage, this.onSocketReady);
                } catch (e) {
                    utils.logger.logError('Response processing failed : ' + e);
                }
            } catch (e) {
                utils.logger.logError('Json parse failed : ' + e);
            }
        }

        var that = this;

        setTimeout(function () {
            that.processResponse();
        }, tradeConstants.TimeIntervals.WebSocketInQueueProcessingInterval);
    },

    /* *
     * Processes message frames from the server
     */
    _processMessage: function (message, onSocketReady) {
        var msgType = message.HED.msgTyp;

                switch (msgType) {
                    case tradeConstants.AiolosProtocol.MsgType.AuthenticationNormal:
                        this.processAuthOtpResponse(message, this.callbacks.auth, onSocketReady);
                        break;

                    case tradeConstants.Response.Type.Authentication.AuthenticationLevel2:
                        this._processLevel2AuthResponse(message);
                        break;

                    case tradeConstants.Response.Type.Authentication.Logout:
                        this._processLogoutResponse(message, this.callbacks.auth, onSocketReady);
                        break;

                    case tradeConstants.Response.Type.Authentication.ChangePassword:
                        this._processChangePassword(message);
                        break;

                    case tradeConstants.Response.Type.Authentication.UserType:
                        this._processUserType(message);
                        break;

                    case tradeConstants.Response.Type.Registration.RegCustomerDetail:
                    case tradeConstants.Response.Type.Registration.CustomerDetail:
                        this._processRegistrationCustomerDetail(message);
                        break;

                    case tradeConstants.Response.Type.Registration.OtpLogin:
                        var authCallback = this.callbacks ? this.callbacks.auth : undefined;

                        this._processRegistrationOtpLoginDetail(message, authCallback, onSocketReady);
                        break;

                    case tradeConstants.Response.Type.Registration.RegPasswordConfirmation:
                    case tradeConstants.Response.Type.Registration.PasswordConfirmation:
                        this._processRegistrationPasswordConfirmation(message);
                        break;

                    case tradeConstants.AiolosProtocol.MsgType.CustomerDetails:
                        this._processCustomerDetails(message);
                        break;

                    case tradeConstants.Response.Type.Customer.KYCDetails:
                        this._processKYCDetails(message);
                        break;

                    case tradeConstants.Response.Type.Customer.KYCMasterData:
                        this._processKYCMasterData(message);
                        break;

                    case tradeConstants.Response.Type.Customer.KYCUpdate:
                        this._processKYCUpdate(message);
                        break;

                    case tradeConstants.AiolosProtocol.MsgType.ExchangeConfig:
                        this._processConfigForExchange(message);
                        break;

                    case tradeConstants.AiolosProtocol.MsgType.BuyingPower:
                        this._processBuyingPower(message);
                        break;

                    case tradeConstants.AiolosProtocol.MsgType.Holdings:
                        this._processHoldings(message);
                        break;

                    case tradeConstants.Response.Type.Exchange.SymbolMarginBP:
                        this._processSymbolMarginBP(message);
                        break;

                    case tradeConstants.AiolosProtocol.MsgType.ExchangeRule:
                        this._processExchangeRules(message);
                        break;

                    case tradeConstants.AiolosProtocol.MsgType.BeneficiaryDetails:
                        this._processBankAccounts(message);
                        break;

                    case tradeConstants.Response.Type.Exchange.DestBankAccounts:
                        this._processDesBankAccounts(message);
                        break;

                    case tradeConstants.Response.Type.Exchange.FxStatus:
                        this._processCurrConvertRequest(message);
                        break;

                    case tradeConstants.Response.Type.OrderList.OrderSearch:
                        this._processOrderSearch(message);
                        break;

                    case tradeConstants.AiolosProtocol.MsgType.OrderList:
                        this._processOrderList(message);
                        break;

                    case tradeConstants.Response.Type.OrderList.OrderCommission:
                        this._processOrderCommission(message);
                        break;

                    case tradeConstants.Response.Type.OrderList.CashStatement:
                        this._processCashStatement(message);
                        break;

                    case tradeConstants.Response.Type.OrderList.StockStatement:
                        this._processStockStatement(message);
                        break;

                    case tradeConstants.Response.Type.OrderList.ConsolidatedReport:
                        this._processConsolidatedReport(message);
                        break;

                    case tradeConstants.AiolosProtocol.MsgType.Pulse:
                        break;

                    case tradeConstants.AiolosProtocol.MsgType.Push.MsgType:
                        // TODO: [satheeqh] Do nothing for now. Process subscribed services and validate below requesting again.
                        break;

                    case tradeConstants.AiolosProtocol.MsgType.NormalOder:
                        this._processNormalOrderResponse(message);
                        break;

                    case tradeConstants.Response.Type.Order.CancelBulkOrder:
                        this._processBulkOrderCancelResponse(message);
                        break;

                    case tradeConstants.AiolosProtocol.MsgType.Deposit:
                    case tradeConstants.AiolosProtocol.MsgType.Withdrawal:
                        this._processTransactionResponse(message);
                        break;

                    case tradeConstants.AiolosProtocol.MsgType.CashTransfer:
                        this._processCashTranfer(message);
                        break;

                    case tradeConstants.AiolosProtocol.MsgType.TransactionHistory:
                        this._processTransactionList(message);
                        break;

                    case tradeConstants.Response.Type.Subscription.ProductDetails:
                        this._processProductDetails(message);
                        break;

                    case tradeConstants.Response.Type.Subscription.UserRenewUpgradeProductList:
                        this._processUserRenewUpgradeProductList(message);
                        break;

                    case tradeConstants.Response.Type.Subscription.SubscriptionFee:
                        this._processSubscriptionFee(message);
                        break;

                    case tradeConstants.Response.Type.Subscription.Subscription:
                        this._processSubscription(message);
                        break;

                    case tradeConstants.Response.Type.RightsSubscription.Subscription:
                        this._processRightsSubscription(message);
                        break;

                    case tradeConstants.Response.Type.RightsSubscription.SubscriptionList:
                        this._processRightsSubscriptionList(message);
                        break;

                    case tradeConstants.Response.Type.MutualFund.FundList:
                        this._processFundList(message);
                        break;

                    case tradeConstants.Response.Type.MutualFund.FundTrasactions:
                        this._processFundTransactions(message);
                        break;

                    case tradeConstants.Response.Type.MutualFund.FundHoldings:
                        this._processFundHoldings(message);
                        break;

                    case tradeConstants.Response.Type.MutualFund.NavFundDate:
                        this._processMFNavFundDate(message);
                        break;

                    case tradeConstants.Response.Type.MutualFund.SubscriptionFee:
                        this._processMFSubscriptionFee(message);
                        break;

                    case tradeConstants.Response.Type.MutualFund.RedemptionFee:
                        this._processMFRedemptionFee(message);
                        break;

                    case tradeConstants.Response.Type.MutualFund.Subscription:
                        this._processMFSubscription(message);
                        break;

                    case tradeConstants.Response.Type.MutualFund.Redemption:
                        this._processMFRedemption(message);
                        break;

                    case tradeConstants.Response.Type.MutualFund.TermsAndConditionGet:
                        this._processGetTermsCondition(message);
                        break;

                    case tradeConstants.Response.Type.MutualFund.TermsAndConditionUpdate:
                        this._processUpdateTermsCondition(message);
                        break;

                    default:
                        utils.logger.logWarning('Unsupported message type : ' + msgType);
                        break;
                }
    },

    _processLogoutResponse: function (message, authCallbacks, onSocketReady) {
        // TODO: [Bashitha] Check whether we can get the error message from logout response

        var errorMsg = this.lang.messages.sessionExpired;
        utils.logger.logInfo('User logged out.' + errorMsg);

        if (Ember.$.isFunction(onSocketReady)) {
            onSocketReady(false);
        }

        this._logoutApplication(errorMsg);
    },

    _processLevel2AuthResponse: function () {
        // TODO: [Bashitha] Implement
    },

    _processAuthResponse: function (message, authCallbacks, onSocketReady) {
        var authSuccess = false;
        var isLoggedIn = this.tradeService.isAuthenticated();
        var isOtpEnabled = message.DAT.authSts === tradeConstants.AuthStatus.OtpEnabled;

        if (message.DAT.authSts === tradeConstants.AuthStatus.Success || isOtpEnabled || message.DAT.authSts === tradeConstants.AuthStatus.FirstTimeLogin) {
            utils.logger.logInfo('User authenticated successfully.');

            authSuccess = true;
            this.tradeService.userDS.setData(Ember.$.extend({}, message.DAT, {sessionId: message.HED.sesnId, loginId: message.HED.loginId}));
            this.tradeService.userDS.save();

            if (isOtpEnabled) {
                this.tradeService.userDS.set('isOtpEnabled', true);

                if (Ember.$.isFunction(authCallbacks.successFn)) {
                    authCallbacks.successFn(tradeConstants.OtpStatus.OtpPending);
                }
            } else {
                this._processAuthSuccess(authCallbacks, tradeConstants.OtpStatus.OtpDisabled);
            }
        } else {
            var authMsg = message.DAT.rejResn;
            authMsg = this._getLangMessage(authMsg);

            utils.logger.logInfo('User authentication failed.' + authMsg);

            if (isLoggedIn) {
                this._logoutApplication(authMsg);
            } else if (Ember.$.isFunction(authCallbacks.errorFn)) {
                // Simply set error message in login page
                authCallbacks.errorFn(authMsg);
            }
        }

        if (!isOtpEnabled && Ember.$.isFunction(onSocketReady)) {
            onSocketReady(authSuccess);
        }
    },

    _processOtpLoginResponse: function (message, authCallbacks, onSocketReady) {
        if (message && message.DAT) {
            var otpStatus = message.DAT.authSts;
            var indexOtpLoginPopup = sharedService.getService('sharedUI').getService('indexOtpLoginPopup');

            if (otpStatus === tradeConstants.AuthStatus.Success || otpStatus === tradeConstants.AuthStatus.FirstTimeLogin) {
                this.tradeService.userDS.setData(Ember.$.extend({}, message.DAT, {sessionId: message.HED.sesnId}));
                this.tradeService.userDS.save();

                if (authCallbacks) {
                    this._processAuthSuccess(authCallbacks, tradeConstants.OtpStatus.OtpSuccess);

                    if (Ember.$.isFunction(onSocketReady)) {
                        onSocketReady(true);
                    }
                }
            } else {
                var rejectedReason = this._getLangMessage(message.DAT.rejResn);
                Ember.set(indexOtpLoginPopup, 'message', rejectedReason);
            }

            Ember.set(indexOtpLoginPopup, 'otpSts', otpStatus);
            indexOtpLoginPopup.onOtpValidationResponse();
        }
    },

    processAuthOtpResponse: function (message, authCallbacks, onSocketReady) {
        if (this.tradeService.userDS.isOtpEnabled) {
            this._processOtpLoginResponse(message, authCallbacks, onSocketReady);
        } else {
            this._processAuthResponse(message, authCallbacks, onSocketReady);
        }
    },

    _processAuthSuccess: function (authCallbacks, otpStatus) {
        if (Ember.$.isFunction(authCallbacks.successFn)) {
            authCallbacks.successFn(otpStatus);
        }

        if (Ember.$.isFunction(authCallbacks.postSuccessFn)) {
            authCallbacks.postSuccessFn();
        }

        this.tradeService.onAuthenticationSuccess();
    },

    _getLangMessage: function (message) {
        if (message && message.includes('|')) {
            var langDesMap = languageDataStore.generateLangMessage(message);
            return langDesMap[sharedService.userSettings.currentLanguage];
        } else {
            return message;
        }
    },

    _logoutApplication: function (message) {
        // Logout from the application with error message set in login page
        utils.webStorage.addString(utils.webStorage.getKey(utils.Constants.CacheKeys.LoginErrorMsg), message, utils.Constants.StorageType.Session);
        utils.applicationSessionHandler.logout(message);
    },

    _processChangePassword: function (message) {
        var callbackFn = this.tradeService.changePasswordCallback;
        var rejectedReason = message.DAT.chgPWdMsg;
        rejectedReason = this._getLangMessage(rejectedReason);

        if (callbackFn && Ember.$.isFunction(callbackFn)) {
            callbackFn(message.DAT.chgPwdSts, rejectedReason);
        }
    },

    _processUserType: function (message) {
        var callbackFn = this.tradeService.userTypeCallback;

        if (callbackFn && Ember.$.isFunction(callbackFn)) {
            callbackFn(message.DAT.stats);
        }
    },

    _processCustomerDetails: function (message) {
        var that = this;
        var userExchanges = [];

        if (message && message.DAT && message.DAT.cashAccounts) {
            Ember.$.each(message.DAT.cashAccounts, function (key, cashAcc) {
                if (cashAcc) {
                    var accObj = that.tradeService.accountDS.getCashAccount(cashAcc.cashAccId);

                    if (accObj) {
                        accObj.setData(cashAcc);
                    }
                }
            });

            Ember.$.each(message.DAT.tradingAccounts, function (key, trAcc) {
                if (trAcc) {
                    that._setCustomerExgLst(trAcc, userExchanges);

                    var accObj = that.tradeService.accountDS.getTradingAccount(trAcc.tradingAccId, trAcc.cashAccountId, trAcc.exgArray);

                    if (accObj) {
                        accObj.setData(trAcc);
                    }
                }
            });

            // TODO: [satheeqh] Get customer details from OMS
            // if (message.DAT.cusDetails) {
            //     this.tradeService.userDS.set('cusDetails', message.DAT.cusDetails);
            // }

            this.tradeService.userDS.set('userExchg', userExchanges);
            this.tradeService.onCustomerDetailSuccess();
        }
    },

    _processKYCDetails: function (message) {
        if (message && message.DAT) {
            var cusData = message.DAT;

            if (cusData) {
                var customer = this.tradeService.customerDS.getKYCDetails(message.DAT.cusId);

                if (customer) {
                    customer.setData(cusData);
                }
            }
        }
    },

    _processProductDetails: function (message) {
        if (message && message.DAT) {
            var proData = message.DAT.prdcts;
            var that = this;

            Ember.$.each(proData, function (proId, productObj) {
                if (productObj) {
                    var product = that.tradeService.productDS.getProduct(productObj.proID);

                    if (product) {
                        product.setData(productObj);
                    }
                }
            });
        }
    },

    _processFundList: function (message) {
        if (message && message.DAT) {
            var that = this;
            var fundData = message.DAT.fndPdctDetrl;

            Ember.$.each(fundData, function (fundId, fundObj) {
                if (fundObj) {
                    var fund = that.tradeService.mutualFundDS.getFund(fundObj.code);

                    if (fund) {
                        fund.setData(fundObj);
                    }
                }
            });
        }
    },

    _processGetTermsCondition: function (message) {
        if (message && message.DAT) {
            var callbackFn = this.tradeService.tCGetCallback;

            if (callbackFn && Ember.$.isFunction(callbackFn)) {
                callbackFn(message.DAT);
            }
        }
    },

    _processUpdateTermsCondition: function (message) {
        if (message && message.DAT) {
            var callbackFn = this.tradeService.tCUpdateCallback;

            if (callbackFn && Ember.$.isFunction(callbackFn)) {
                callbackFn(message.DAT);
            }
        }
    },

    _processFundTransactions: function (message) {
        if (message && message.DAT) {
            var that = this;
            var fundTransData = message.DAT.statmntRes;

            Ember.$.each(fundTransData, function (fundTransId, fundTransObj) {
                if (fundTransObj) {
                    var fundTrans = that.tradeService.mutualFundDS.getTransaction(fundTransObj.transNo, fundTransObj.sts);

                    if (fundTrans) {
                        fundTrans.setData(fundTransObj);
                    }
                }
            });
        }
    },

    _processFundHoldings: function (message) {
        if (message && message.DAT) {
            var fundHoldingData = message.DAT.holdings;
            var that = this;

            Ember.$.each(fundHoldingData, function (fundTransId, fundHoldingsObj) {
                if (fundHoldingsObj) {
                    var fundHolding = that.tradeService.mutualFundDS.getHolding(fundHoldingsObj.code);

                    if (fundHolding) {
                        fundHolding.setData(fundHoldingsObj);
                    }
                }
            });
        }
    },

    _processMFSubscriptionFee: function (message) {
        if (message && message.DAT) {
            var fundHoldingData = message.DAT.holdings;
            var that = this;

            Ember.$.each(fundHoldingData, function (fundTransId, fundHoldingsObj) {
                if (fundHoldingsObj) {
                    var fundHolding = that.tradeService.mutualFundDS.getHolding(fundHoldingsObj.code);

                    if (fundHolding) {
                        fundHolding.setData(fundHoldingsObj);
                    }
                }
            });
        }
    },

    _processMFRedemptionFee: function (message) {
        if (message && message.DAT) {
            var fundHoldingData = message.DAT.holdings;
            var that = this;

            Ember.$.each(fundHoldingData, function (fundTransId, fundHoldingsObj) {
                if (fundHoldingsObj) {
                    var fundHolding = that.tradeService.mutualFundDS.getHolding(fundHoldingsObj.code);

                    if (fundHolding) {
                        fundHolding.setData(fundHoldingsObj);
                    }
                }
            });
        }
    },

    _processMFSubscription: function (message) {
        if (message && message.DAT) {
            var callbackFn = this.tradeService.subscriptionCallback;
            var rejectedReason = message.DAT.rsn;

            rejectedReason = this._getLangMessage(rejectedReason);

            if (callbackFn && Ember.$.isFunction(callbackFn)) {
                callbackFn(message.DAT, rejectedReason);
            }
        }
    },

    _processMFRedemption: function (message) {
        if (message && message.DAT) {
            var callbackFn = this.tradeService.redemptionCallback;
            var rejectedReason = message.DAT.rsn;

            rejectedReason = this._getLangMessage(rejectedReason);

            if (callbackFn && Ember.$.isFunction(callbackFn)) {
                callbackFn(message.DAT, rejectedReason);
            }
        }
    },

    _processUserRenewUpgradeProductList: function (message) {
        if (message && message.DAT) {
            var proData = message.DAT.prdcts;
            var that = this;

            Ember.$.each(proData, function (proId, productObj) {
                if (productObj) {
                    var feeDetails = productObj.feeDetails ? productObj.feeDetails.split(',') : [];
                    var feeValues = [];
                    var feeField;

                    if (feeDetails && feeDetails.length > 0) {
                        Ember.$.each(feeDetails, function (index, field) {
                            feeField = field ? field.split('|') : [];

                            if (feeField.length > 1) {
                                feeValues[feeValues.length] = {
                                    des: parseInt(feeField[1], 10),
                                    feeId: parseInt(feeField[0], 10)
                                };
                            }
                        });
                    }

                    productObj.proNmeAR = utils.formatters.convertUnicodeToNativeString(productObj.proNmeAR);
                    productObj.feeDetails = feeValues;

                    var product = that.tradeService.productDS.getProduct(productObj.proID);

                    if (product) {
                        product.setData(productObj);
                    }
                }
            });
        }
    },

    _processSubscription: function (message) {
        if (message && message.DAT) {
            var rejctdRsn = message.DAT.rjetResn;
            var stats = message.DAT.sts;
            var prdcts = message.DAT.prdcts;
            var proId, product;

            if (prdcts && prdcts.length > 0) {
                proId = prdcts[0].proID;
                product = this.tradeService.productDS.getProductById(proId);
            }

            if (product) {
                product.setData({sts: parseInt(stats, 10), rjetResn: rejctdRsn});
            }
        }
    },

    _processRightsSubscription: function (message) {
        if (message && message.DAT) {
            var rightsSubData = message.DAT;
            var unqReqId = rightsSubData.unqReqId;

            if (utils.validators.isAvailable(unqReqId)) {
                var rightsOrder = this.tradeService.rightSubscriptionRequestMap[unqReqId];

                rightsOrder.set('rsn', rightsSubData.rsn);
                rightsOrder.set('sts', rightsSubData.sts);
            }
        }
    },

    _processRightsSubscriptionList: function (message) {
        var that = this;

        if (message && message.DAT && message.DAT.stockList) {
            Ember.$.each(message.DAT.stockList, function (index, rightsSubscription) {
                if (rightsSubscription) {
                    var rightsSubscriptionObj = that.tradeService.rightsSubscriptionDS.getRightsSubscription(rightsSubscription.exg, rightsSubscription.sym, rightsSubscription.InstTyp, rightsSubscription.subNo);

                    if (rightsSubscriptionObj) {
                        rightsSubscriptionObj.setData(rightsSubscription);
                    }
                }
            });
        }
    },

    _processSubscriptionFee: function (message) {
        if (message && message.DAT) {
            var expExpDate = message.DAT.expeExpDte;
            var expFromDate = message.DAT.expeFrmDte;
            var feeCurrency = message.DAT.feeCurr;
            var fee = message.DAT.exgFee + message.DAT.srvceFee + message.DAT.thdPrtyFee + message.DAT.othrFee;
            var products = message.DAT.prdcts;
            var proId, product;

            if (products && products.length > 0) {
                proId = products[0].proID;
                product = this.tradeService.productDS.getProductById(proId);
            }

            if (product) {
                product.setData({expExpDte: expExpDate, expFrmDte: expFromDate, feeCurr: feeCurrency, fee: fee});
            }
        }
    },

    _processConsolidatedReport: function (message) {
        if (message && message.DAT) {
            var conRepData = message.DAT.consoldRep;
            var that = this;

            Ember.$.each(conRepData, function (repId, repObj) {
                var consolidatedReport = that.tradeService.consolidatedReportDS.getConReport(repId);

                if (consolidatedReport) {
                    consolidatedReport.setData(repObj);
                }
            });
        }
    },

    _processKYCMasterData: function (message) {
        if (message && message.DAT) {
            var masterData = message.DAT.mstData;
            var that = this;

            if (masterData) {
                Ember.$.each(masterData, function (index, field) {
                    var dataColl = field.dataCol;

                    Ember.$.each(dataColl, function (indexOption, option) {
                        option.des = utils.formatters.convertUnicodeToNativeString(option.des);
                    });

                    that.tradeService.kycMasterDS.setTypes(field.typId, dataColl, message.DAT.langKey);
                });
            }
        }
    },

    _setCustomerExgLst: function (message, userExchanges) {
        var exgArray = [];
        var exg = message.exchange;

        if (exg) {
            if (exg && userExchanges.indexOf(exg) === -1) {
                userExchanges[userExchanges.length] = exg;
            }

            exgArray[exgArray.length] = exg;
        }

        message.exgArray = exgArray;
    },

    _processConfigForExchange: function (message) {
        if (message && message.DAT) {
            var that = this;

            this.tradeService.tradeMetaDS.tradeMeta.setData({exgLevelConfig: message});
            this.tradeService.tradeMetaDS.tradeMeta.save();

            if (message.DAT.exgConf) {
                Ember.$.each(message.DAT.exgConf, function (exgConfIndex, exgConfData) {
                    if (exgConfData) {
                        var tradeMetaDS = that.tradeService.tradeMetaDS;
                        var orderSide = exgConfData.side ? exgConfData.side : tradeMetaDS.defaultOrderSide;
                        var orderType = exgConfData.type ? exgConfData.type : tradeMetaDS.defaultOrderType;

                        tradeMetaDS.setOrderSides(exgConfData.exg, orderSide.replace(/\s/g, '').split(','));
                        tradeMetaDS.setOrderTypes(exgConfData.exg, orderType.replace(/\s/g, '').split(','));
                    }
                });
            }

            if (message.DAT.tifRuleSet) {
                Ember.$.each(message.DAT.tifRuleSet, function (tifRuleIndex, tifRuleData) {
                    if (tifRuleData) {
                        var tifStrValues = tifRuleData.tifTyps.split(',');

                        if (tifStrValues && tifStrValues.length > 0) {
                            var tifIntValues = [];

                            Ember.$.each(tifStrValues, function (index, tif) {
                                tifIntValues[tifIntValues.length] = parseInt(tif, 10);
                            });

                            that.tradeService.tradeMetaDS.setTifTypes(tifRuleData.exg, tifRuleData.ordTyp, tifRuleData.mktSts, tifIntValues);
                        }
                    }
                });

                this._generateTifMktStatusMap(message.DAT.tifRuleSet);
            }

            this.tradeService.onExchangeConfigSuccess(this.callbacks.auth.isReconnection);
        }
    },

    _generateTifMktStatusMap: function (tifData) {
        var tifMktStatusMap = this.tradeService.tradeMetaDS.tifMktStatusDependencyMap;
        var exgTifMap = {};

        Ember.$.each(tifData, function (idx, tifRuleData) {
            if (!exgTifMap[tifRuleData.exg]) {
                exgTifMap[tifRuleData.exg] = [];
            }

            exgTifMap[tifRuleData.exg][exgTifMap[tifRuleData.exg].length] = tifRuleData;
        });

        Ember.$.each(exgTifMap, function (exgIdx, tifDataObj) {
            if (!tifMktStatusMap[exgIdx] && tifMktStatusMap[exgIdx] !== false) {
                tifMktStatusMap[exgIdx] = false;
            }

            Ember.$.each(tifDataObj, function (tIdx, tRuleData) {
                Ember.$.each(tifDataObj, function (idx, ruleData) {
                    if (tRuleData.mktSts !== ruleData.mktSts && tRuleData.tifTyps !== ruleData.tifTyps) {
                        tifMktStatusMap[tRuleData.exg] = true;
                        return false;
                    }
                });

                return false;
            });
        });
    },

    _processExchangeRules: function (message) {
        var that = this;

        if (message && message.DAT && message.DAT.ruleLst) {
            Ember.$.each(message.DAT.ruleLst, function (exgIndex, exgRule) {
                if (exgRule && exgRule.grpId) {
                    switch (exgRule.grpId) {
                        case that.exchangeRule.tifByType:
                            var conditions = exgRule.condition ? exgRule.condition.split('|') : [];

                            Ember.$.each(conditions, function (ruleIndex, ruleSet) {
                                var rule = ruleSet ? ruleSet.split(':') : [];

                                if (rule.length > 1) {
                                    that.tradeService.tradeMetaDS.setTifByType(exgRule.exg, rule[0], rule[1]);
                                }
                            });
                            break;
                    }
                }
            });
        }
    },

    _processBuyingPower: function (message) {
        if (message && message.DAT && message.DAT.cashAcntId) {
            var cashAcc = this.tradeService.accountDS.getCashAccount(message.DAT.cashAcntId);

            if (cashAcc) {
                cashAcc.setData(message.DAT);
            }
        }
    },

    _processBankAccounts: function (message) {
        if (message && message.DAT && message.DAT.beneficiary) {
            var bankData = message.DAT.beneficiary;
            var that = this;

            Ember.$.each(bankData, function (bankIndex, bankAcc) {
                if (bankAcc.accTyp === tradeConstants.BeneficiaryType.BrokerageAccount) { // Allow only brokerage accounts for current requirement
                    var bank = that.tradeService.bankAccountDS.getBankAcc(bankAcc.bnkAccNum, bankAcc.cashAccId);

                    if (bank) {
                        bank.setData(bankAcc);
                    }
                }
            });
        }
    },

    _processDesBankAccounts: function (message) {
        if (message && message.DAT && message.DAT.bankDtails) {
            var bankData = message.DAT.bankDtails;
            var that = this;

            Ember.$.each(bankData, function (bankIndex, bankField) {
                var bankId = bankField.bankCode;
                var accList = bankField.bnkAccs;

                if (accList) {
                    Ember.$.each(accList, function (accIndex, accField) {

                        var bank = that.tradeService.bankAccountDS.getDesBankAcc(accField.bnkAccNum);
                        Ember.set(accField, 'bankId', bankId);

                        if (bank) {
                            bank.setData(accField);
                        }

                    });
                }
            });
        }
    },

    _processHoldings: function (message) {
        if (message && message.DAT && message.DAT.holdings) {
            var that = this;

            Ember.$.each(message.DAT.holdings, function (holdingIndex, holdingData) {
                if (holdingData) {
                    var holding = that.tradeService.holdingDS.getHolding(holdingData.tradingAccId, holdingData.symbol, holdingData.exg, holdingData.instruTyp, true);

                    if (holding) {
                        holding.setData(holdingData);

                        if (holding.qty === 0 && holding.tradingAccId && holding.symbol) {
                            that.tradeService.holdingDS.removeHolding(holding.tradingAccId, holding.symbol);
                        }
                    }
                }
            });
        }
    },

    _processOrderList: function (message) {
        if (message && message.DAT && message.DAT.orderList) {
            var that = this;

            Ember.$.each(message.DAT.orderList, function (orderIndex, orderData) {
                if (orderData) {
                    var order = that.tradeService.orderDS.getOrder(orderData.ordNo, orderData.exg, orderData.symbol, orderData.instruTyp);

                    if (utils.validators.isAvailable(orderData.txt)) {
                        orderData.txt = that._getLangMessage(utils.formatters.convertUnicodeToNativeString(orderData.txt));
                    }

                    if (order) {
                        order.setData(Ember.$.extend({}, orderData, {secAccNum: orderData.tradingAccId}));
                    }
                }
            });

            Ember.run.later(function () { // Response message is not coming in a single message, but in ms range intervals
                that.tradeService.set('isOrderListLoaded', true);
            }, 500);
        }
    },

    _processOrderSearch: function (message) {
        if (message && message.DAT) {
            var wkey = message.DAT.unqReqId;
            var widgetSearchResult = this.tradeService.orderSearchDS.getWidgetSearchResult(wkey);
            var that = this;

            Ember.$.each(message.DAT, function (key, value) {
                if (key !== 'ordLst') {
                    Ember.set(widgetSearchResult, key, value);
                } else {
                    Ember.$.each(message.DAT.ordLst, function (orderIndex, orderData) {
                        if (orderData) {
                            var order = that.tradeService.orderSearchDS.getOrder(orderData.clOrdId, wkey, orderData.symbol, orderData.exg, orderData.instruTyp);

                            if (utils.validators.isAvailable(orderData.txt)) {
                                orderData.txt = that._getLangMessage(utils.formatters.convertUnicodeToNativeString(orderData.txt));
                            }

                            if (order) {
                                order.setData(orderData);
                            }
                        }
                    });
                }
            });
        }
    },

    _processNormalOrderResponse: function (message) {
        if (message && message.DAT && message.DAT.order) {
            var orderData = message.DAT.order;
            var order = this.tradeService.orderDS.getOrder(orderData.ordNo, orderData.exg, orderData.symbol, orderData.instruTyp);

            if (utils.validators.isAvailable(orderData.txt)) {
                orderData.txt = this._getLangMessage(utils.formatters.convertUnicodeToNativeString(orderData.txt));
            }

            if (order) {
                order.setData(Ember.$.extend({}, orderData, {secAccNum: orderData.tradingAccId}));
            }
        }
    },

    _processBulkOrderCancelResponse: function (message) {
        if (message && message.DAT) {
            var bulkOrderCanceldata = message.DAT;
            var unqReqId = bulkOrderCanceldata.unqReqId;
            var sts = bulkOrderCanceldata.resSts;
            var rsn = bulkOrderCanceldata.resRes;

            if (utils.validators.isAvailable(unqReqId)) {
                var bulkOrder = this.tradeService.bulkOrderCancelRequestMap[unqReqId];
                bulkOrder.set('sts', sts);
                bulkOrder.set('rsn', rsn);
            }
        }
    },

    _processTransactionResponse: function () {
        // Not required in current widgets
    },

    _processTransactionList: function (message) {
        if (message && message.DAT && message.DAT.cshTrans) {
            var that = this;

            Ember.$.each(message.DAT.cshTrans, function (tranIndx, transactionData) {
                if (transactionData) {
                    var transaction = that.tradeService.transDS.getTransaction(transactionData.chTranId);
                    transactionData.secAccNum = transactionData.cashAccId; // To support filters

                    if (transaction) {
                        transaction.setData(transactionData);
                    }
                }
            });
        }
    },

    _processOrderCommission: function (message) {
        if (message && message.DAT && message.DAT.comsn) {
            var orderData = message.DAT;
            var comsn = orderData.comsn === -1 ? 0 : orderData.comsn;
            var unqReqId = orderData.unqReqId;

            if (utils.validators.isAvailable(unqReqId)) {
                var order = this.tradeService.orderCommissionRequestMap[unqReqId];

                if (order && !(order.get('isDestroyed') || order.get('isDestroying'))) {
                    order.set('comsn', comsn);
                    delete this.tradeService.orderCommissionRequestMap[unqReqId];
                }
            }
        }
    },

    _processSymbolMarginBP: function (message) {
        if (message && message.DAT && message.DAT.smblmgnBuyPwr) {
            var dat = message.DAT;
            var smblmgnBuyPwr = dat.smblmgnBuyPwr;
            var unqReqId = dat.unqReqId;

            if (utils.validators.isAvailable(unqReqId)) {
                var order = this.tradeService.symbolMarginRequestMap[unqReqId];

                if (order && !(order.get('isDestroyed') || order.get('isDestroying'))) {
                    order.set('smblmgnBuyPwr', smblmgnBuyPwr);
                    order.set('isMar', true);

                    delete this.tradeService.symbolMarginRequestMap[unqReqId];
                }
            }
        }
    },

    _processCashTranfer: function (message) {
        if (message && message.DAT) {
            var cashTranferData = message.DAT;
            var sts = cashTranferData.responseCode;
            var rsn = cashTranferData.description ? cashTranferData.description : '';
            var unqReqId = cashTranferData.unqReqId;

            if (utils.validators.isAvailable(unqReqId)) {
                var cashTransfer = this.tradeService.cashTransferRequestMap[unqReqId];
                cashTransfer.set('sts', sts);
                cashTransfer.set('rsn', rsn);
            }
        }
    },

    _processCurrConvertRequest: function (message) {
        if (message && message.DAT) {
            var currRateData = message.DAT;
            var sts = currRateData.status;
            var unqReqId = currRateData.unqReqId;

            if (utils.validators.isAvailable(unqReqId)) {
                var currRate = this.tradeService.currConvertRequestMap[unqReqId];
                currRate.set('sts', sts);
            }
        }
    },

    _processCurrencyRate: function (message) {
        if (message && message.DAT) {
            var currRateData = message.DAT;
            var fxRate = currRateData.currRate;
            var amnt = currRateData.amnt;
            var transType = currRateData.transType;
            var unqReqId = currRateData.unqReqId;

            if (utils.validators.isAvailable(unqReqId)) {
                var currRate = this.tradeService.currConvertRequestMap[unqReqId];
                currRate.set('fxRate', fxRate);
                currRate.set('amnt', amnt);
                currRate.set('transType', transType);
            }
        }
    },

    _processCashStatement: function (message) {
        try {
            if (message && message.DAT) {
                if (message.DAT.headColumns) {
                    this._processCashStatementMixProtocol(message.DAT);
                } else {
                    this._processCashStatementArray(message.DAT);
                }
            }
        } catch (e) {
            utils.logger.logError('Error in processing cash statement response : ' + e);
        }

    },

    _processCashStatementMixProtocol: function (message) {
        var headerFields = ['date', 'trxType', 'tranRef', 'particulars', 'amount', 'balance', 'settleDate'];
        var cashStatementHedIdxList = message.headColumns.split(utils.Constants.StringConst.Comma).indicesOf(headerFields);
        var cashStatements = message.dataSet ? message.dataSet.split(utils.Constants.StringConst.SemiColon) : [];
        var that = this;

        Ember.$.each(cashStatements, function (key, value) {
            var cashStatementData = value.split(utils.Constants.StringConst.Comma);
            var date = cashStatementData[cashStatementHedIdxList.date];
            var particulars = cashStatementData[cashStatementHedIdxList.particulars];
            var amount = cashStatementData[cashStatementHedIdxList.amount];
            var balance = cashStatementData[cashStatementHedIdxList.balance];
            var transactionType = cashStatementData[cashStatementHedIdxList.trxType];
            var transactionRef = cashStatementData[cashStatementHedIdxList.tranRef];
            var settleDate = cashStatementData[cashStatementHedIdxList.settleDate];

            if (transactionRef) {
                var cashStatement = that.tradeService.cashStatementDS.getCashStatement(transactionRef);

                cashStatement.setData({
                    date: date,
                    trnsType: transactionType,
                    particulars: particulars,
                    amount: amount,
                    balance: balance,
                    settleDate: settleDate
                });
            }
        });
    },

    _processCashStatementArray: function (data) {
        var currency = data.curr;
        var openingBal = data.openBal;
        var totDeposit = data.totDeposit;
        var totWithdrawal = data.totWithdrawal;
        var totBuy = data.totBuy;
        var totSell = data.totSell;
        var totOther = data.totOther;
        var closeBal = data.closeBal;
        var that = this;

        if (data.cashLogs) {
            Ember.$.each(data.cashLogs, function (tranIndx, transactionData) {
                if (transactionData) {
                    var transaction = that.tradeService.cashStatementDS.getCashStatement(transactionData.trnsRef);

                    if (transaction) {
                        transaction.setData(transactionData);
                    }
                }
            });
        }

        that.tradeService.cashStatementDS.setSummationInfo({
            curr: currency,
            openBal: openingBal,
            totDeposit: totDeposit,
            totWithdrawal: totWithdrawal,
            totBuy: totBuy,
            totSell: totSell,
            totOther: totOther,
            closeBal: closeBal
        });
    },

    _processStockStatement: function (message) {
        try {
            if (message && message.DAT) {
                if (message.DAT.headColumns && message.DAT.dataSet) {
                    this._processStockStatementMixProtocol(message.DAT);
                } else {
                    this._processStockStatementArray(message.DAT);
                }
            }
        } catch (e) {
            utils.logger.logError('Error in processing stock statement response : ' + e);
        }
    },

    _processStockStatementMixProtocol: function (data) {
        var headerFields = ['date', 'sym', 'tranRef', 'symName', 'des', 'qty'];
        var stockStatementHedIdxList = data.headColumns.split(utils.Constants.StringConst.Comma).indicesOf(headerFields);
        var stockStatements = data.dataSet.split(utils.Constants.StringConst.SemiColon);
        var that = this;

        Ember.$.each(stockStatements, function (key, value) {
            var stockStatementData = value.split(utils.Constants.StringConst.Comma);
            var date = stockStatementData[stockStatementHedIdxList.date];
            var des = stockStatementData[stockStatementHedIdxList.des];
            var transactionRef = stockStatementData[stockStatementHedIdxList.tranRef];
            var sym = stockStatementData[stockStatementHedIdxList.sym];
            var symName = stockStatementData[stockStatementHedIdxList.symName];
            var qty = stockStatementData[stockStatementHedIdxList.qty];

            if (transactionRef) {
                var stockStatement = that.tradeService.stockStatementDS.getStockStatement(transactionRef);

                stockStatement.setData({
                    date: date,
                    sym: sym,
                    symName: symName,
                    des: des,
                    qty: qty
                });
            }
        });
    },

    _processStockStatementArray: function (data) {
        if (data.holdingLogs) {
            var that = this;

            Ember.$.each(data.holdingLogs, function (tranIndx, transactionData) {
                if (transactionData) {
                    var transaction = that.tradeService.stockStatementDS.getStockStatement(transactionData.tranRef, transactionData.exg, transactionData.sym);

                    if (transaction) {
                        transaction.setData(transactionData);
                    }
                }
            });
        }
    },

    _processKYCUpdate: function (message) {
        if (message && message.DAT) {
            var kycUpdateData = message.DAT;
            var unqReqId = kycUpdateData.unqReqId;

            if (utils.validators.isAvailable(unqReqId)) {
                var kycUpdateReq = this.tradeService.kycUpdateRequestMap[unqReqId]; // Map

                kycUpdateReq.set('rsn', kycUpdateData.rsn);
                kycUpdateReq.set('sts', parseInt(kycUpdateData.sts, 10));
            }
        }
    },

    // Registration Step 1 - Registration Detail
    _processRegistrationCustomerDetail: function (message) {
        if (message && message.DAT) {
            var registrationObj = this.tradeService.registrationDS.getRegistration();
            var cusDetailStatus = message.DAT.custStus;

            if (registrationObj) {
                if (cusDetailStatus === tradeConstants.RegistrationStatus.Success) {
                    this.tradeService.registrationDS.setData(message.DAT);
                } else {
                    var rejectedReason = this._getLangMessage(message.DAT.rejResn);
                    Ember.set(registrationObj, 'cusDetailStsMsg', rejectedReason);
                }

                Ember.set(registrationObj, 'cusDetailSts', cusDetailStatus);
            }
        }
    },

    // Registration Step 2 - OTP Login
    _processRegistrationOtpLoginDetail: function (message) {
        if (message && message.DAT) {
            var registrationObj = this.tradeService.registrationDS.getRegistration();
            var otpStatus = message.DAT.stats;

            if (registrationObj) {
                if (otpStatus !== tradeConstants.RegistrationStatus.Success) {
                    var rejectedReason = this._getLangMessage(message.DAT.rejctResn);
                    Ember.set(registrationObj, 'otpStsMsg', rejectedReason);
                }

                Ember.set(registrationObj, 'otpSts', otpStatus); // To fire the reg-otp observer
            }
        }
    },

    // Registration Step 4 - Password Verification
    _processRegistrationPasswordConfirmation: function (message) {
        if (message && message.DAT) {
            var registrationObj = this.tradeService.registrationDS.getRegistration();
            var pwdStatus = message.DAT.stats;

            if (registrationObj) {
                if (pwdStatus !== tradeConstants.RegistrationStatus.Success) {
                    var rejectedReason = this._getLangMessage(message.DAT.rejctResn);
                    Ember.set(registrationObj, 'pwdStsMsg', rejectedReason);
                }

                Ember.set(registrationObj, 'pwdSts', pwdStatus);
            }
        }
    }
});
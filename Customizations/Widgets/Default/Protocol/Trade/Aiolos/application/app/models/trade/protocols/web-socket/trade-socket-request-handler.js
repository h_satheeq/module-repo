import Ember from 'ember';
import utils from '../../../../utils/utils';
import sharedService from '../../../../models/shared/shared-service';
import tradeConstants from '../../trade-constants';
import environmentConfig from '../../../../config/environment';
import tradeSettings from '../../../../config/trade-settings';

export default (function () {
    var reqGroup = tradeConstants.Request.Group;
    var reqType = tradeConstants.Request.Type;
    var aiolosMsgTyp = tradeConstants.AiolosProtocol.MsgType;
    var unqReqIdPref = 0;
    var unqReqIdSufx = Date.now();

    // Authentication related requests
    var generateRetailAuthRequest = function (authParams, isReconnection) {
        var datElements = [];

        datElements[datElements.length] = '{"lgnNme":"';
        datElements[datElements.length] = authParams.username;
        datElements[datElements.length] = '","pwd":"';
        datElements[datElements.length] = isReconnection ? authParams.password : utils.crypto.generateHashedText(authParams.password);
        datElements[datElements.length] = '"}';

        var req = _addRequestHeader(datElements.join(''), aiolosMsgTyp.AuthenticationNormal);

        utils.logger.logInfo('Retail Auth Request : ' + req);

        return req;
    };

    var generateSsoAuthRequest = function () {
        return '';
    };

    var generateLevel2AuthRequest = function (password) {
        var datElements = [];

        datElements[datElements.length] = '{"l2Pwd":"';
        datElements[datElements.length] = utils.crypto.generateHashedText(password);
        datElements[datElements.length] = '","l2PwdTyp": 1}';

        var req = _addRequestHeader(datElements.join(''), reqGroup.Authentication, reqType.Authentication.AuthenticationLevel2);

        utils.logger.logInfo('Level 2 Auth Request : ' + req);

        return req;
    };

    var generateReconnectionAuthRequest = function () {
        return generateRetailAuthRequest({
            username: Ember.appGlobal.session.id,
            password: Ember.appGlobal.session.token
        }, true);
    };

    var generateLogoutRequest = function () {
        return _addRequestHeader('{}', reqGroup.Authentication, reqType.Authentication.Logout);
    };

    var generateCustomerDetailsRequest = function () {
        var userId = sharedService.getService('trade').userDS.usrId;
        var datElements = [];

        unqReqIdPref = [userId, tradeSettings.channelId].join(utils.Constants.StringConst.Underscore);

        datElements[datElements.length] = '{"cusId":"';
        datElements[datElements.length] = userId;
        datElements[datElements.length] = '"}';

        return _addRequestHeader(datElements.join(''), aiolosMsgTyp.CustomerDetails);
    };

    var generateExchangeConfigRequest = function () {
        var datElements = [];

        datElements[datElements.length] = '{"exgs":"';
        datElements[datElements.length] = sharedService.getService('trade').userDS.userExchg.join(utils.Constants.StringConst.Comma);
        datElements[datElements.length] = '"}';

        return _addRequestHeader(datElements.join(''), aiolosMsgTyp.ExchangeConfig);
    };

    var generateRuleRequest = function () {
        return _addRequestHeader('{}', aiolosMsgTyp.ExchangeRule);
    };

    var generateBuyingPowerRequest = function (args) {
        var datElements = [];

        datElements[datElements.length] = '{"tradingAccountId":';
        datElements[datElements.length] = args.tradingAccId;
        datElements[datElements.length] = ',"cashAcntId":';
        datElements[datElements.length] = args.cashAccountId;
        datElements[datElements.length] = ',"cusId":';
        datElements[datElements.length] = sharedService.getService('trade').userDS.usrId;
        datElements[datElements.length] = '}';

        return _addRequestHeader(datElements.join(''), aiolosMsgTyp.BuyingPower);
    };

    var generateHoldingsRequest = function (args) {
        var datElements = [];

        datElements[datElements.length] = '{"tradingAccId":';
        datElements[datElements.length] = args.tradingAccId;
        datElements[datElements.length] = '}';

        return _addRequestHeader(datElements.join(''), aiolosMsgTyp.Holdings);
    };

    var generateOrderListRequest = function (args) {
        var datElements = [];

        datElements[datElements.length] = '{"tradingAccId":"';
        datElements[datElements.length] = args.tradingAccId;
        datElements[datElements.length] = '","ordCatgry":';
        datElements[datElements.length] = args.ordCategory;
        datElements[datElements.length] = '}';

        return _addRequestHeader(datElements.join(''), aiolosMsgTyp.OrderList);
    };

    var generatePushRequest = function (serviceType) {
        var datElements = [];

        datElements[datElements.length] = '{"service":';
        datElements[datElements.length] = serviceType;
        datElements[datElements.length] = '}';

        return _addRequestHeader(datElements.join(('')), aiolosMsgTyp.Push.MsgType);
    };

    var generatePulseMessage = function () {
        return _addRequestHeader('{}', aiolosMsgTyp.Pulse);
    };

    var generateNormalOrderRequest = function (order) {
        var ordObj = {
            tradingAccId: order.get('secAccNum'),
            symbol: order.get('symbol'),
            exg: order.get('exg'),
            ordTyp: order.get('ordTyp'),
            ordSide: order.get('ordSide'),
            ordQty: order.get('ordQty'),
            tif: order.get('tif'),
            disQty: order.get('disQty') > 0 ? order.get('disQty') : 0,
            minQty: order.get('minQty') > 0 ? order.get('minQty') : 0,
            price: order.get('price'), // Format to one decimal to support TDWL with Colombo OMS
            dayOrd: 0,
            instruTyp: order.get('instruTyp'),
            expDte: order.get('expTime') ? order.get('expTime').substring(0, 8) : '',
            bkId: order.get('bkId'),
            marketCode: 'ALL',
            cusId: sharedService.getService('trade').userDS.usrId
        };

        return _addRequestHeader(utils.jsonHelper.convertToJson(ordObj), aiolosMsgTyp.NormalOder);
    };

    var generateAmendOrderRequest = function (order) {
        var ordObj = {
            secAccNum: order.get('secAccNum'),
            symbol: order.get('symbol'),
            exg: order.get('exg'),
            ordTyp: order.get('ordTyp'),
            ordSide: order.get('ordSide'),
            ordQty: order.get('ordQty'),
            tif: order.get('tif'),
            disQty: order.get('disQty') > 0 ? order.get('disQty') : 0,
            minQty: order.get('minQty') > 0 ? order.get('minQty') : 0,
            price: order.get('price'),
            dayOrd: 0,
            expTime: order.get('expTime'),
            bkId: order.get('bkId'),
            clOrdId: order.get('clOrdId'),
            secTyp: order.get('secTyp'),
            orgClOrdId: order.get('clOrdId'),
            crdDte: order.get('crdDte'),
            ordVal: order.get('calcOrdVal'),
            cumComsn: order.get('cumComsn'),
            netOrdVal: order.get('calcNetOrdVal'),
            stlCurrncy: 'LKR',
            mktCode: 'N',
            mubOrdNum: order.get('mubOrdNum'),
            curr: order.get('curr'),
            ordCatgry: 1,
            instruTyp: order.get('instruTyp'),
            isTimeTriggerede: 0,
            brokerClient: -1,
            cusId: sharedService.getService('trade').userDS.usrId
        };

        return _addRequestHeader(utils.jsonHelper.convertToJson(ordObj), reqGroup.Order, reqType.Order.AmendOder);
    };

    var generateCancelOrderRequest = function (order) {
        var ordObj = {
            secAccNum: order.get('secAccNum'),
            symbol: order.get('symbol'),
            exg: order.get('exg'),
            ordTyp: order.get('ordTyp'),
            ordSide: order.get('ordSide'),
            tif: order.get('tif'),
            bkId: order.get('bkId'),
            mubOrdNum: order.get('mubOrdNum'),
            orgClOrdId: order.get('clOrdId'),
            clOrdId: order.get('clOrdId'),
            cusId: sharedService.getService('trade').userDS.usrId
        };

        return _addRequestHeader(utils.jsonHelper.convertToJson(ordObj), reqGroup.Order, reqType.Order.CancelOder);
    };

    var generateDepositRequest = function (transaction) {
        transaction.custId = sharedService.getService('trade').userDS.u01CustomerId;
        return _addRequestHeader(utils.jsonHelper.convertToJson(transaction), aiolosMsgTyp.Deposit);
    };

    var generateWithdrawalRequest = function (transaction) {
        transaction.custId = sharedService.getService('trade').userDS.u01CustomerId;
        return _addRequestHeader(utils.jsonHelper.convertToJson(transaction), aiolosMsgTyp.Withdrawal);
    };

    var generateCashTransferRequest = function (transaction) {
        var custId = sharedService.getService('trade').userDS.u01CustomerId;

        var transObj = {
            custId: custId,
            cashAccId: transaction.frmAcc,
            amt: transaction.amnt,
            toAcc: transaction.toAcc,
            payMtd: transaction.transType,
            createdBy: custId,
            valDte: utils.formatters.convertToDisplayTimeFormat(new Date(), 'YYYYMMDD'),
            isOwnCashAccount: 1
        };

        return _addRequestHeader(utils.jsonHelper.convertToJson(transObj), aiolosMsgTyp.CashTransfer);
    };

    var generateCurrenyRateRequest = function (transaction) {
        return _addRequestHeader(utils.jsonHelper.convertToJson(transaction), reqGroup.Exchange, reqType.Exchange.CurrencyRate);
    };

    var generateBankAccountsRequest = function () {
        var custId = sharedService.getService('trade').userDS.u01CustomerId;
        var datElements = [];

        datElements[datElements.length] = '{"cusId":"';
        datElements[datElements.length] = custId;
        datElements[datElements.length] = '"}';

        return _addRequestHeader(datElements.join(''), aiolosMsgTyp.BeneficiaryDetails);
    };

    var generateDestBankAccountRequest = function (bankAcc) {
        return _addRequestHeader(utils.jsonHelper.convertToJson(bankAcc), reqGroup.Exchange, reqType.Exchange.DestBankAccounts);
    };

    var generateTransactionHistoryRequest = function (msgData) {
        var trnObj = {
            custId: sharedService.getService('trade').userDS.u01CustomerId,
            strDte: msgData.strDte,
            endDte: msgData.endDte
        };

        return _addRequestHeader(utils.jsonHelper.convertToJson(trnObj), aiolosMsgTyp.TransactionHistory);
    };

    var generateOrderCommissionRequest = function () {
        // var ordObj = {
        //     secAccNum: orderParams.get('secAccNum'),
        //     symbol: orderParams.get('symbol'),
        //     exg: orderParams.get('exg'),
        //     instruTyp: orderParams.get('instruTyp'),
        //     ordTyp: orderParams.get('ordTyp'),
        //     price: orderParams.get('price'),
        //     ordQty: orderParams.get('ordQty'),
        //     unqReqId: unqReqId
        // };
        //
        // return _addRequestHeader(utils.jsonHelper.convertToJson(ordObj), reqGroup.OrderList, reqType.OrderList.OrderCommission);

        // Blocking commission request until OMS support.

        return undefined;
    };

    var generateSymbolMarginRequest = function (unqReqId, orderParams, symbolInfo) {
        var reqObj = {
            secAccNum: orderParams.get('secAccNum'),
            symbol: symbolInfo.get('sym'),
            exg: symbolInfo.get('exg'),
            unqReqId: unqReqId
        };

        return _addRequestHeader(utils.jsonHelper.convertToJson(reqObj), reqGroup.Exchange, reqType.Exchange.SymbolMarginBP);
    };

    var generateOrderSearchRequest = function (reqObj) {
        return _addRequestHeader(utils.jsonHelper.convertToJson(reqObj), reqGroup.OrderList, reqType.OrderList.OrderSearch);
    };

    var generateKYCDetailsRequest = function (reqObj) {
        return _addRequestHeader(utils.jsonHelper.convertToJson(reqObj), reqGroup.Customer, reqType.Customer.KYCDetails);
    };

    var generateKYCMasterDataRequest = function (reqObj) {
        return _addRequestHeader(utils.jsonHelper.convertToJson(reqObj), reqGroup.Customer, reqType.Customer.KYCMasterData);
    };

    var generateKYCUpdateRequest = function (reqObj) {
        return _addRequestHeader(utils.jsonHelper.convertToJson(reqObj), reqGroup.Customer, reqType.Customer.KYCUpdate);
    };

    var generateCashStatementRequest = function (reqObj) {
        return _addRequestHeader(utils.jsonHelper.convertToJson(reqObj), reqGroup.OrderList, reqType.OrderList.CashStatement);
    };

    var generateStockStatementRequest = function (reqObj) {
        return _addRequestHeader(utils.jsonHelper.convertToJson(reqObj), reqGroup.OrderList, reqType.OrderList.StockStatement);
    };

    var generateChangePasswordRequest = function (reqObj) {
        return _addRequestHeader(utils.jsonHelper.convertToJson(reqObj), reqGroup.Authentication, reqType.Authentication.ChangePassword);
    };

    var generateUserTypeRequest = function (reqObj) {
        return _addRequestHeader(utils.jsonHelper.convertToJson(reqObj), reqGroup.Authentication, reqType.Authentication.UserType);
    };

    var generateFundListRequest = function () {
        var datElements = [];

        datElements[datElements.length] = '{';
        datElements[datElements.length] = '}';

        return _addRequestHeader(datElements.join(''), reqGroup.MutualFund, reqType.MutualFund.FundList);
    };

    var generateTermsConditionGetRequest = function (reqObj) {
        var datElements = [];

        datElements[datElements.length] = '{"usrId":"';
        datElements[datElements.length] = sharedService.getService('trade').userDS.get('usrId');
        datElements[datElements.length] = '","fndName":"';
        datElements[datElements.length] = reqObj.fundName;
        datElements[datElements.length] = '"}';

        return _addRequestHeader(datElements.join(''), reqGroup.MutualFund, reqType.MutualFund.TermsAndConditionGet);
    };

    var generateTermsConditionUpdateRequest = function (reqObj) {
        var datElements = [];

        datElements[datElements.length] = '{"usrId":"';
        datElements[datElements.length] = sharedService.getService('trade').userDS.get('usrId');
        datElements[datElements.length] = '","fndName":"';
        datElements[datElements.length] = reqObj.fundName;
        datElements[datElements.length] = '"}';

        return _addRequestHeader(datElements.join(''), reqGroup.MutualFund, reqType.MutualFund.TermsAndConditionUpdate);
    };

    var generateFundTransactionsRequest = function (reqObj) {
        var datElements = [];

        datElements[datElements.length] = '{"mubNo":"';
        datElements[datElements.length] = sharedService.getService('trade').userDS.get('mubNo');
        datElements[datElements.length] = '","frmDate":"';
        datElements[datElements.length] = reqObj.fromDate;
        datElements[datElements.length] = '","toDate":"';
        datElements[datElements.length] = reqObj.toDate;
        datElements[datElements.length] = '"}';

        return _addRequestHeader(datElements.join(''), reqGroup.MutualFund, reqType.MutualFund.FundTrasactions);
    };

    var generateFundHoldingsRequest = function () {
        var datElements = [];

        datElements[datElements.length] = '{"cusRefNum":"';
        datElements[datElements.length] = sharedService.getService('trade').userDS.get('mubNo');
        datElements[datElements.length] = '"}';

        return _addRequestHeader(datElements.join(''), reqGroup.MutualFund, reqType.MutualFund.FundHoldings);
    };

    var generateNavFundDateRequest = function (reqObj) {
        return _addRequestHeader(utils.jsonHelper.convertToJson(reqObj), reqGroup.MutualFund, reqType.MutualFund.NavFundDate);
    };

    var generateMFSubscriptionFeeRequest = function (reqObj) {
        return _addRequestHeader(utils.jsonHelper.convertToJson(reqObj), reqGroup.MutualFund, reqType.MutualFund.SubscriptionFee);
    };

    var generateRedemptionFeeRequest = function (reqObj) {
        return _addRequestHeader(utils.jsonHelper.convertToJson(reqObj), reqGroup.MutualFund, reqType.MutualFund.RedemptionFee);
    };

    var generateMFRedemptionRequest = function (reqObj) {
        return _addRequestHeader(utils.jsonHelper.convertToJson(reqObj), reqGroup.MutualFund, reqType.MutualFund.Redemption);
    };

    var generateMFSubscriptionRequest = function (reqObj) {
        return _addRequestHeader(utils.jsonHelper.convertToJson(reqObj), reqGroup.MutualFund, reqType.MutualFund.Subscription);
    };

    var generateRightsSubscriptionRequest = function (reqObj) {
        return _addRequestHeader(utils.jsonHelper.convertToJson(reqObj), reqGroup.RightsSubscription, reqType.RightsSubscription.Subscription);
    };

    var generateRightsSubscriptionListRequest = function (reqObj) {
        return _addRequestHeader(utils.jsonHelper.convertToJson(reqObj), reqGroup.RightsSubscription, reqType.RightsSubscription.SubscriptionList);
    };

    var generateProductDetailsRequest = function (reqObj) {
        return _addRequestHeader(utils.jsonHelper.convertToJson(reqObj), reqGroup.Subscription, reqType.Subscription.ProductDetails);
    };

    var generateUserRenewUpgradeProductListRequest = function (reqObj) {
        return _addRequestHeader(utils.jsonHelper.convertToJson(reqObj), reqGroup.Subscription, reqType.Subscription.UserRenewUpgradeProductList);
    };

    var generateSubscriptionFeeRequest = function (reqObj) {
        return _addRequestHeader(utils.jsonHelper.convertToJson(reqObj), reqGroup.Subscription, reqType.Subscription.SubscriptionFee);
    };

    var generateBulkOrderCancelRequest = function (reqObj) {
        return _addRequestHeader(utils.jsonHelper.convertToJson(reqObj), reqGroup.Order, reqType.Order.CancelBulkOrder);
    };

    var generateSubscriptionRequest = function (reqObj) {
        return _addRequestHeader(utils.jsonHelper.convertToJson(reqObj), reqGroup.Subscription, reqType.Subscription.Subscription);
    };

    var generateConsolidatedReportRequest = function (reqObj) {
        return _addRequestHeader(utils.jsonHelper.convertToJson(reqObj), reqGroup.OrderList, reqType.OrderList.ConsolidatedReport);
    };

    // Registration / Forgot Password - Step 1
    var generateCusDetailValidationRequest = function (reqObj, isForgotPwd) {
        if (isForgotPwd) {
            return _addRequestHeader(utils.jsonHelper.convertToJson(reqObj), reqGroup.Registration, reqType.Registration.CustomerDetail);
        } else {
            return _addRequestHeader(utils.jsonHelper.convertToJson(reqObj), reqGroup.Registration, reqType.Registration.RegCustomerDetail);
        }
    };

    // Registration / Forgot Password - Step 2
    var generateOtpValidationRequest = function (reqObj) {
        return _addRequestHeader(utils.jsonHelper.convertToJson(reqObj), reqGroup.Registration, reqType.Registration.OtpLogin);
    };

    // Login OTP
    var generateLoginOtpValidationRequest = function (reqObj) {
        return _addRequestHeader(utils.jsonHelper.convertToJson(reqObj), reqGroup.Authentication, reqType.Authentication.OtpEnabled);
    };

    // Registration - Step 3 / Forgot Password - Step 3
    var generatePasswordValidationRequest = function (reqObj, isForgotPwd) {
        if (isForgotPwd) {
            return _addRequestHeader(utils.jsonHelper.convertToJson(reqObj), reqGroup.Registration, reqType.Registration.PasswordConfirmation);
        } else {
            return _addRequestHeader(utils.jsonHelper.convertToJson(reqObj), reqGroup.Registration, reqType.Registration.RegPasswordConfirmation);
        }
    };

    var _addRequestHeader = function (msgData, msgType) {
        var reqElements = [];
        var userDS = sharedService.getService('trade').userDS;

        reqElements[reqElements.length] = '{"HED":{"msgTyp":';
        reqElements[reqElements.length] = msgType;
        reqElements[reqElements.length] = ',"channel":';
        reqElements[reqElements.length] = tradeSettings.channelId;
        reqElements[reqElements.length] = ',"commVer":"';
        reqElements[reqElements.length] = environmentConfig.APP.version;
        reqElements[reqElements.length] = '","loginId":"';
        reqElements[reqElements.length] = userDS.loginId;
        reqElements[reqElements.length] = '","sesnId":"';
        reqElements[reqElements.length] = userDS.sessionId;
        reqElements[reqElements.length] = '","clientIp":"';
        reqElements[reqElements.length] = '192.168.0.1';
        reqElements[reqElements.length] = '","tenantCode":';
        reqElements[reqElements.length] = '"DEFAULT_TENANT"';
        reqElements[reqElements.length] = ',"unqReqId":"';
        reqElements[reqElements.length] = [unqReqIdPref, unqReqIdSufx].join(utils.Constants.StringConst.Underscore);
        reqElements[reqElements.length] = '"},"DAT":';
        reqElements[reqElements.length] = msgData;
        reqElements[reqElements.length] = '}';

        unqReqIdSufx += 1;

        return reqElements.join('');
    };

    return {
        generateRetailAuthRequest: generateRetailAuthRequest,
        generateSsoAuthRequest: generateSsoAuthRequest,
        generateLevel2AuthRequest: generateLevel2AuthRequest,
        generateReconnectionAuthRequest: generateReconnectionAuthRequest,
        generateCustomerDetailsRequest: generateCustomerDetailsRequest,
        generateLogoutRequest: generateLogoutRequest,
        generateExchangeConfigRequest: generateExchangeConfigRequest,
        generateBuyingPowerRequest: generateBuyingPowerRequest,
        generateHoldingsRequest: generateHoldingsRequest,
        generateOrderListRequest: generateOrderListRequest,
        generatePushRequest: generatePushRequest,
        generatePulseMessage: generatePulseMessage,
        generateNormalOrderRequest: generateNormalOrderRequest,
        generateAmendOrderRequest: generateAmendOrderRequest,
        generateCancelOrderRequest: generateCancelOrderRequest,
        generateDepositRequest: generateDepositRequest,
        generateWithdrawalRequest: generateWithdrawalRequest,
        generateBankAccountsRequest: generateBankAccountsRequest,
        generateDestBankAccountRequest: generateDestBankAccountRequest,
        generateTransactionHistoryRequest: generateTransactionHistoryRequest,
        generateOrderCommissionRequest: generateOrderCommissionRequest,
        generateSymbolMarginRequest: generateSymbolMarginRequest,
        generateRuleRequest: generateRuleRequest,
        generateOrderSearchRequest: generateOrderSearchRequest,
        generateCashStatementRequest: generateCashStatementRequest,
        generateStockStatementRequest: generateStockStatementRequest,
        generateChangePasswordRequest: generateChangePasswordRequest,
        generateCusDetailValidationRequest: generateCusDetailValidationRequest,
        generateOtpValidationRequest: generateOtpValidationRequest,
        generateLoginOtpValidationRequest: generateLoginOtpValidationRequest,
        generatePasswordValidationRequest: generatePasswordValidationRequest,
        generateUserTypeRequest: generateUserTypeRequest,
        generateKYCDetailsRequest: generateKYCDetailsRequest,
        generateKYCMasterDataRequest: generateKYCMasterDataRequest,
        generateKYCUpdateRequest: generateKYCUpdateRequest,
        generateProductDetailsRequest: generateProductDetailsRequest,
        generateUserRenewUpgradeProductListRequest: generateUserRenewUpgradeProductListRequest,
        generateSubscriptionFeeRequest: generateSubscriptionFeeRequest,
        generateSubscriptionRequest: generateSubscriptionRequest,
        generateBulkOrderCancelRequest: generateBulkOrderCancelRequest,
        generateConsolidatedReportRequest: generateConsolidatedReportRequest,
        generateCashTransferRequest: generateCashTransferRequest,
        generateFundListRequest: generateFundListRequest,
        generateTermsConditionGetRequest: generateTermsConditionGetRequest,
        generateTermsConditionUpdateRequest: generateTermsConditionUpdateRequest,
        generateFundTransactionsRequest: generateFundTransactionsRequest,
        generateFundHoldingsRequest: generateFundHoldingsRequest,
        generateNavFundDateRequest: generateNavFundDateRequest,
        generateMFSubscriptionFeeRequest: generateMFSubscriptionFeeRequest,
        generateRedemptionFeeRequest: generateRedemptionFeeRequest,
        generateMFRedemptionRequest: generateMFRedemptionRequest,
        generateMFSubscriptionRequest: generateMFSubscriptionRequest,
        generateRightsSubscriptionRequest: generateRightsSubscriptionRequest,
        generateRightsSubscriptionListRequest: generateRightsSubscriptionListRequest,
        generateCurrenyRateRequest: generateCurrenyRateRequest
    };
})();
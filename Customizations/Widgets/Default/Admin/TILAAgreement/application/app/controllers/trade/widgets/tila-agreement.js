import Ember from 'ember';
import sharedService from '../../../models/shared/shared-service';
import BaseArrayController from '../../base-array-controller';

export default BaseArrayController.extend({
    filePath: undefined,
    userTypObj: Ember.A(),
    userTypResponseSts: {
        Success: '1',
        Failed: 0
    },

    onPrepareData: function () {
        this._setFilepath();
    },

    onLanguageChanged: function () {
        this._setFilepath();
    },

    _setFilepath: function () {
        var filePath = '/plugin.privateUserAgreement_EN.htm';
        if (sharedService.userSettings.currentLanguage === 'AR') {
            filePath = '/plugin.privateUserAgreement_AR.htm';
        }

        this.set('filePath', filePath);
    },

    _onClosePopup: function () {
        var modal = sharedService.getService('sharedUI').getService('modalPopupId');
        modal.set('isEnabled', false);
    },

    _sendUserTypReq: function () {
        var reqObj = this.get('userTypObj');
        var that = this;

        sharedService.getService('trade').sendUserTypeRequest(reqObj, function (sts) {
            that._onUserTypResponse(sts);
        });
    },

    _onUserTypResponse: function (sts) {
        if (sts === this.userTypResponseSts.Success) {
            this._onClosePopup();
        } else {
            this.set('message', this.app.lang.messages.tilaAgreementFaild);
        }
    },

    actions: {
        onBusinessUser: function () {
            this.set('userTypObj', {userTyp: 0}); // Business User
            this._sendUserTypReq();
        },

        onPrivateUser: function () {
            this.set('userTypObj', {userTyp: 1}); // Private User
            this._sendUserTypReq();
        }
    }
});
import Ember from 'ember';
import BaseController from '../../base-controller';
import sharedService from '../../../models/shared/shared-service';
import tradeConstants from '../../../models/trade/trade-constants';
import utils from '../../../utils/utils';

export default BaseController.extend({
    symbolSearchId: '',
    bulkOrder: Ember.Object.create({}),
    dropDownBuySell: [],

    tradeService: sharedService.getService('trade'),

    bulkOrderCancelStatus: {
        Success: 1,
        Fail: -1
    },

    setbulkOrderCancelStatus: function () {
        var sts = this.get('bulkOrder').resSts;
        var msg = '';

        if (sts === this.bulkOrderCancelStatus.Fail) {
            msg = this.get('bulkOrder').resRes;
        }

        this._showMessage(sts, msg);
    }.observes('bulkOrder.resSts', 'bulkOrder.resRes'),

    onPrepareData: function () {
        this.loadPortfolioOptions();
        this.loadOrderSideOptions();

        this.set('symbolSearchId', ['symbolSearch', this.get('wkey')].join('-'));
    },

    onTradeMetaReady: function () {
        this.loadPortfolioOptions();
        this.loadOrderSideOptions();
    },

    onTradeDisconnect: function () {
        this.onClearData();
    },

    onClearData: function () {
        this.set('portfolios', Ember.A());
        this.set('dropDownBuySell', Ember.A());
    },

    onLoadWidget: function () {
        this.tradeService.subscribeTradeMetaReady(this, this.get('wkey'));
        this.tradeService.subscribeTradeDisconnect(this, this.get('wkey'));
    },

    loadOrderSideOptions: function () {
        var exg = this.tradeService.userDS.get('userExchg')[0];
        var orderSides = this.tradeService.tradeMetaDS.getOrderSideCollectionByExchange(exg);
        var sides = [];

        this._updateDescription(orderSides, this.tradeService.tradeMetaDS.metaMapping.orderSide);

        sides.pushObject({des: this.get('app').lang.labels.all, isShowAll: true});
        sides.pushObjects(orderSides);

        this.set('dropDownBuySell', sides);
        this.set('currentSide', sides[0]);
    },

    loadPortfolioOptions: function () {
        var portfolioList = [];
        var portCollection = this.tradeService.accountDS.getTradingAccCollection();

        portfolioList.pushObject({tradingAccId: this.get('app').lang.labels.all, isShowAll: true});
        portfolioList.pushObjects(portCollection);

        this.set('portfolios', portfolioList);

        if (portfolioList.length > 0) {
            this.set('currentPortfolio', portfolioList[0]);
        }
    },

    _updateDescription: function (options, type) {
        var that = this;

        if (options) {
            Ember.$.each(options, function (index, option) {
                Ember.set(option, 'des', that.app.lang.labels[[type, option.code].join('_')]);
            });
        }
    },

    _updateDropdownDescription: function (options) {
        var that = this;

        if (options) {
            Ember.$.each(options, function (index, option) {
                Ember.set(option, 'des', that.app.lang.labels[option.langKey]);
            });
        }
    },

    _validateSearchKey: function () {
        var searchKey = this.get('searchKey');
        var invoker = this.get('invoker');

        if (invoker && invoker.symbolInfo) {
            var symbol = this.get('invoker').symbolInfo;

            if (!utils.validators.isAvailable(searchKey) || symbol.dSym) {
                if (symbol && utils.validators.isAvailable(searchKey)) {
                    this.set('searchKey', symbol.dSym);
                } else {
                    this.sendAction(this.get('onSymbolChanged'));
                }
            } else {
                this.set('searchKey', '');
            }
        }
    },

    _showMessage: function (sts, msg) {
        var app = this.get('app');
        var message = sts === this.bulkOrderCancelStatus.Success ? app.lang.messages.requestSuccess : (sts === this.bulkOrderCancelStatus.Fail ? msg : '');
        var messageCss = sts === this.bulkOrderCancelStatus.Success ? 'up-fore-color' : (sts === this.bulkOrderCancelStatus.Fail ? 'down-fore-color' : '');

        this.set('message', message);
        this.set('msgCss', messageCss);
    },

    onSymbolChanged: function (symbol) {
        var exg, sym, inst;

        if (symbol && symbol.sym) {
            exg = symbol.get('exg');
            sym = symbol.get('sym');
            inst = symbol.get('inst');
        }

        this.set('currentSym', {exg: exg, sym: sym, inst: inst});
    },

    onCreateReqObject: function (portfolio, currentSymbol, orderSide) {
       return {cusId: this.tradeService.userDS.mubNo, secAccNum: portfolio.secAccNum, sym: currentSymbol.sym, exg: currentSymbol.exg, side: orderSide};
    },

    actions: {
        setPortfolio: function (option) {
            this.set('currentPortfolio', option);
        },

        setSide: function (option) {
            this.set('currentSide', option);
        },

        onSubmit: function () {
            var currentPortfolio = this.get('currentPortfolio');
            var currentSide = this.get('currentSide');
            var currentSymbol = this.get('currentSym');
            var portfolioList = this.get('portfolios');

            var unqReqId = Math.floor(Date.now()).toString();
            var bulkOrder = this.get('bulkOrder');
            var that = this;

            this.set('bulkOrder.sts', 0);
            this.set('bulkOrder.unqReqId', unqReqId);
            this.set('bulkOrder.rsn', '');

            if (currentPortfolio.isShowAll && currentSide.isShowAll) {
                Ember.$.each(portfolioList, function (id, field) {
                    unqReqId += field.secAccNum;
                    if (!field.isShowAll) {
                        that.tradeService.sendBulkOrderCancelRequest(unqReqId, bulkOrder, that.onCreateReqObject(field.secAccNum, currentSymbol, tradeConstants.OrderSide.Sell));
                        unqReqId += '1';
                        that.tradeService.sendBulkOrderCancelRequest(unqReqId, bulkOrder, that.onCreateReqObject(field.secAccNum, currentSymbol, tradeConstants.OrderSide.Buy));
                    }
                });
            } else if (currentPortfolio.isShowAll) {
                Ember.$.each(portfolioList, function (id, field) {
                    that.tradeService.sendBulkOrderCancelRequest(unqReqId, bulkOrder, that.onCreateReqObject(field.secAccNum, currentSymbol, currentSide.code));
                });
            } else if (currentSide.isShowAll) {
                this.tradeService.sendBulkOrderCancelRequest(unqReqId, bulkOrder, this.onCreateReqObject(currentPortfolio.secAccNum, currentSymbol, tradeConstants.OrderSide.Sell));
                unqReqId += '1';
                this.tradeService.sendBulkOrderCancelRequest(unqReqId, bulkOrder, this.onCreateReqObject(currentPortfolio.secAccNum, currentSymbol, tradeConstants.OrderSide.Buy));
            } else {
                this.tradeService.sendBulkOrderCancelRequest(unqReqId, bulkOrder, this.onCreateReqObject(currentPortfolio.secAccNum, currentSymbol, currentSide.code));
            }
        },

        showSearchPopup: function () {
            var modal = this.get(this.get('symbolSearchId'));
            modal.send('showModalPopup');
        },

        closeSearchPopup: function () {
            var modal = this.get(this.get('symbolSearchId'));
            modal.send('closeModalPopup');
        },

        onSymbolSelected: function (symbol) {
            this.set('searchKey', symbol.dSym);
            this.set('enableSearch', false);

            this.onSymbolChanged(symbol);
        },

        validateSearchKey: function () {
            this._validateSearchKey();
        }
    }
});
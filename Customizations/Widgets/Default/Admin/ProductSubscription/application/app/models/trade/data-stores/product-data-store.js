import Ember from 'ember';
import Product from '../business-entities/product';

export default Ember.Object.extend({
    store: {},
    productDetailsCollection: Ember.A([]),
    productMapById: {},

    getProduct: function (proId) {
        var currentStore = this.get('store');
        var product = currentStore[proId];

        if (!product && proId) {
            product = Product.create({
                proId: proId
            });

            currentStore[proId] = product;

            var productDetailsCollection = this.get('productDetailsCollection');
            productDetailsCollection.pushObject(product);
        }

        return product;
    },

    getProductColl: function () {
        return this.get('productDetailsCollection');
    },

    clearCollection: function () {
        this.set('store', {});
        this.set('productDetailsCollection', Ember.A());
    },

    getProductById: function (proId) {
        var currentStore = this.get('store');
        return currentStore[proId];
    }
});

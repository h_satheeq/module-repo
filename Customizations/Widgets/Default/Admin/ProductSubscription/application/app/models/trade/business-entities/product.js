import Ember from 'ember';

export default Ember.Object.extend({
    proId: '',
    proName: '',
    proNmeAR: '',
    rnk: '',
    mubNo: '',
    feeDetails: [],
    isRealTime: 0,
    mktDpth: 0,
    expDte: '',
    feeCurr: 0,
    expFrmDte: '',
    expExpDte: '',
    rjetResn: '',
    sts: 0,
    fee: '',

    setData: function (proMessage) {
        var that = this;

        Ember.$.each(proMessage, function (key, value) {
            that.set(key, value);
        });
    }
});
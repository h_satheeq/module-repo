import Ember from 'ember';
import BaseController from '../../../base-controller';
import sharedService from '../../../../models/shared/shared-service';
import tradeConstants from '../../../../models/trade/trade-constants';

export default BaseController.extend({
    currentProduct: {},
    currentType: {},
    products: [],

    tradeService: sharedService.getService('trade'),
    isEndOfSubscription: false,
    productLabelKey: undefined,

    proLabel: {
        EN: 'proName',
        AR: 'proNmeAR'
    },

    subscriptionStatus: {
        Success: 1,
        Fail: -1
    },

    subscriptionType: {
        Upgrade: 2,
        Renew: 1
    },

    isSubmitDisabled: function () {
        return !(this.get('currentProduct') && this.get('currentPeriod') && this.get('currentType'));
    }.property('currentProduct', 'currentType', 'currentPeriod'),

    isVATEnabled: function () {
        return this.tradeService.userDS.isVatApplicble === tradeConstants.VATStatus.Enabled;
    }.property('tradeService.userDS.isVatApplicble'),

    setSubscriptionStatus: function () {
        var currentProduct = this.get('currentProduct');

        if (currentProduct) {
            var sts = currentProduct.sts;
            var msg = '';

            if (sts === this.subscriptionStatus.Fail) {
                msg = currentProduct.rjetResn;
            } else if (sts === this.subscriptionStatus.Success) {
                sharedService.getService('price').sendProductSubscriptionRequest(currentProduct.proId, currentProduct.encryptedToken);
            }

            this._showMessage(sts, msg);
        }
    }.observes('currentProduct.sts'),

    setCurrentProduct: function () {
        Ember.run.once(this, this._applyProduct);
    }.observes('products.@each'),

    setFeeDetails: function () {
        var currentProduct = this.get('currentProduct');

        if (currentProduct && currentProduct.feeDetails) {
            var periodArray = currentProduct.feeDetails;

            if (periodArray.length > 0) {
                this._setPeriod(periodArray[0]);
            }
        }
    }.observes('currentProduct.feeDetails'),

    setEndOfSubscription: function () {
        var currentProduct = this.get('currentProduct');
        var isEndOfSubscription = this.get('isEndOfSubscription') ? 1 : 0;
        var currentPeriod = this.get('currentPeriod');

        if (currentProduct && currentPeriod) {
            this.tradeService.sendSubscriptionFeeRequest({
                cusId: this.tradeService.userDS.usrId,
                proID: currentProduct.proId,
                activOnly: 1,
                feeType: this.get('currentType').code,
                dueInMnth: currentPeriod.des,
                feeID: currentPeriod.feeId,
                isEndOfSub: isEndOfSubscription
            });
        }
    }.observes('isEndOfSubscription'),

    onPrepareData: function () {
        this._loadTypeDropDown();
    },

    onClearData: function () {
        this.set('isProductsAvailable', false);
        this.set('isNoProductsMsg', false);
        this.set('noProductsMsg', '');
        this.set('msgCss', '');
        this.set('currentProduct', null);
        this.set('message', null);
    },

    onLanguageChanged: function (language) {
        this._loadTypeDropDown();
        this.set('productLabelKey', this.proLabel[language]);

        if (this.get('currentType')) {
            this._loadProducts(this.get('currentType'));
        }
    },

    _applyProduct: function () {
        var productsArray = this.get('products');

        if (productsArray.length > 0) {
            this.set('currentProduct', productsArray[0]);
            this.set('isProductsAvailable', true);
            this.set('isNoProductsMsg', false);
        } else {
            var app = this.get('app');

            this.set('isNoProductsMsg', true);
            this.set('noProductsMsg', app.lang.messages.noProductsAvailable);
            this.set('msgCss', 'down-fore-color');
        }
    },

    _loadTypeDropDown: function () {
        var app = this.get('app');
        this.set('typeCollection', [{des: app.lang.labels.upgrade, code: 2}, {des: app.lang.labels.renewal, code: 1}]);
        this.set('defaultType', {des: app.lang.labels.pleaseSelect, code: 0});
    },

    _showMessage: function (sts, msg) {
        var app = this.get('app');
        var message = sts === this.subscriptionStatus.Success ? app.lang.messages.subscriptionSuccess : (sts === this.subscriptionStatus.Fail ? msg : '');
        var messageCss = sts === this.subscriptionStatus.Success ? 'up-fore-color pad-m-t' : (sts === this.subscriptionStatus.Fail ? 'down-fore-color  pad-m-t' : '');

        this.set('message', message);
        this.set('msgCss', messageCss);
    },

    _loadProducts: function (option) {
        this.tradeService.productDS.clearCollection();

        if (option.code) {
            switch (option.code) {
                case this.subscriptionType.Renew:
                    this.tradeService.sendUserRenewUpgradeProductListRequest({
                        cusId: this.tradeService.userDS.usrId,
                        activOnly: 1,
                        feeType: this.subscriptionType.Renew
                    });
                    break;

                case this.subscriptionType.Upgrade:
                    this.tradeService.sendUserRenewUpgradeProductListRequest({
                        cusId: this.tradeService.userDS.usrId,
                        activOnly: 1,
                        feeType: this.subscriptionType.Upgrade
                    });
                    break;
            }

            var productCollection = this.tradeService.productDS.getProductColl();

            this.set('currentType', option);
            this.set('products', productCollection);
            this.set('productLabelKey', this.proLabel[sharedService.userSettings.currentLanguage]);
        }
    },

    _setPeriod: function (option) {
        var currProduct = this.get('currentProduct');
        var currType = this.get('currentType');
        var isEndOfSubscription = this.get('isEndOfSubscription') ? 1 : 0;

        this.set('currentPeriod', option);

        if (currProduct && currType) {
            this.tradeService.sendSubscriptionFeeRequest({
                cusId: this.tradeService.userDS.usrId,
                proID: currProduct.proId,
                activOnly: 1,
                feeType: currType.code,
                dueInMnth: option.des,
                feeID: option.feeId,
                isEndOfSub: isEndOfSubscription
            });
        }
    },

    actions: {
        setType: function (type) {
            this.onClearData();
            this._loadProducts(type);
        },

        setProduct: function (option) {
            this.set('currentProduct', option);
        },

        setPeriod: function (option) {
            this._setPeriod(option);
        },

        onSubmit: function () {
            var currentPeriod = this.get('currentPeriod');
            var currProduct = this.get('currentProduct');
            var currentType = this.get('currentType');
            var isEndOfSubscription = this.get('isEndOfSubscription') ? 1 : 0;

            if (currentPeriod && currentType && currProduct) {
                this.tradeService.sendSubscriptionRequest({
                    cusId: this.tradeService.userDS.usrId,
                    proID: currProduct.proId,
                    activOnly: 1,
                    feeType: currentType.code,
                    dueInMnth: currentPeriod.des,
                    feeID: currentPeriod.feeId,
                    isEndOfSub: isEndOfSubscription
                });
            }
        }
    }
});
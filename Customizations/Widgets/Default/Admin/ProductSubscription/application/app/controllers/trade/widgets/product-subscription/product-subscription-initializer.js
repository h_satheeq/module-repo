import BaseModuleInitializer from '../../../../models/shared/initializers/base-module-initializer';
import sharedService from '../../../../models/shared/shared-service';
import ProductDataStore from '../../../../models/trade/data-stores/product-data-store';

export default BaseModuleInitializer.extend({
    postInitialize: function () {
        var tradeService = sharedService.getService('trade');
        tradeService.set('productDS', ProductDataStore.create({tradeService: tradeService}));
    }
});

import BaseModuleInitializer from '../../../../models/shared/initializers/base-module-initializer';
import sharedService from '../../../../models/shared/shared-service';
import RightsSubscriptionDataStore from '../../../../models/trade/data-stores/rights-subscription-data-store';

export default BaseModuleInitializer.extend({
    postInitialize: function () {
        var tradeService = sharedService.getService('trade');
        var rightsSubscriptionDS = RightsSubscriptionDataStore.create({tradeService: tradeService});

        rightsSubscriptionDS.initialize();
        tradeService.set('rightsSubscriptionDS', rightsSubscriptionDS);
    }
});
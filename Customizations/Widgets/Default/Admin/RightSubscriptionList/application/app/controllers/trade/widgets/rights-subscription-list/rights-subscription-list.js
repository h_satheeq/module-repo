import Ember from 'ember';
import TradeTableController from '../../controllers/trade-table-controller';
import tradeWidgetConfig from '../../../../config/trade-widget-config';
import sharedService from '../../../../models/shared/shared-service';
import userSettings from '../../../../config/user-settings';
import tradeConstants from '../../../../models/trade/trade-constants';

// Cell Views
import ClassicHeaderCell from '../../../../views/table/classic-header-cell';
import ClassicCell from '../../../../views/table/classic-cell';
import ClassicStatusCell from '../../../../views/table/trade/classic-status-cell';

export default TradeTableController.extend({
    isRenderingEnabled: false,

    DefaultSearchDays: 7,
    toEndDate: '',
    fromDate: '',
    toDate: '',
    lan: '',
    datePickerFormat: userSettings.displayFormat.dateFormat.toLowerCase(),
    defaultColumnIds: tradeWidgetConfig.rightsSubscriptionList.defaultColumnIds,

    onLoadWidget: function () {
        var wkey = this.get('wkey');

        sharedService.getService('trade').subscribeTradeMetaReady(this, wkey);
        sharedService.getService('trade').subscribeTradeDisconnect(this, wkey);

        this.set('defaultColumnMapping', tradeWidgetConfig.rightsSubscriptionList.defaultColumnMapping);
        this.setCellViewsScopeToGlobal();
        this.setErrorMessage();

        this.setDefaultColumns();
        this.setDefaultColumnDeclarations();

        var today = new Date();

        this.set('toEndDate', today);
        this.set('toDate', today);
        this.set('lan', sharedService.userSettings.currentLanguage.toLowerCase());

        // Get from date using default search days
        var fromDate = new Date();

        fromDate.setDate(fromDate.getDate() - this.DefaultSearchDays);
        this.set('fromDate', fromDate);
    },

    setCellViewsScopeToGlobal: function () {
        Ember.ClassicHeaderCell = ClassicHeaderCell;
        Ember.ClassicCell = ClassicCell;
        Ember.ClassicStatusCell = ClassicStatusCell;
    },

    cellViewsForColumns: {
        classicStatusCell: 'Ember.ClassicStatusCell',
        classicCell: 'Ember.ClassicCell'
    },

    onPrepareData: function () {
        this.loadContent();
    },

    onAfterRender: function () {
        this.set('isRenderingEnabled', true);
    },

    onAddSubscription: function () {
        this._sendSearchRequest();
    },

    onLanguageChanged: function () {
        this.set('columnDeclarations', []);
        this.set('lan', sharedService.userSettings.currentLanguage.toLowerCase());

        this.loadContent();
        this.onLoadWidget();
    },

    loadContent: function () {
        var rightsSubscriptionList = sharedService.getService('trade').rightsSubscriptionDS.getRightsSubscriptionCollection();
        this.set('content', rightsSubscriptionList);
    },

    onTradeMetaReady: function () {
        this.onPrepareData();
        this.onAddSubscription();
    },

    onUnloadWidget: function () {
        var wkey = this.get('wkey');
        this.set('columnDeclarations', []);

        sharedService.getService('trade').unSubscribeTradeMetaReady(wkey);
        sharedService.getService('trade').unSubscribeTradeDisconnect(wkey);
    },

    _sendSearchRequest: function () {
        this.setRequestTimeout(tradeConstants.TimeIntervals.LoadingInterval, 'content.length');
        sharedService.getService('trade').rightsSubscriptionDS.clearCollection();
        this.loadContent();

        var tradeService = sharedService.getService('trade');
        var fromDate = this.get('fromDate');
        var toDate = this.get('toDate');

        fromDate = this.utils.validators.isAvailable(fromDate) ? this.utils.formatters.convertToDisplayTimeFormat(fromDate, 'YYYYMMDD') : '';
        toDate = this.utils.validators.isAvailable(toDate) ? this.utils.formatters.convertToDisplayTimeFormat(toDate, 'YYYYMMDD') : '';

        if (tradeService.userDS.get('mubNo')) {
            tradeService.sendRightsSubscriptionListRequest({
                mubNo: tradeService.userDS.get('mubNo'),
                strtDte: fromDate,
                endDte: toDate
            });
        }
    },

    onCheckDataAvailability: function () {
        return this.get('content').length !== 0;
    },

    actions: {
        onSearch: function () {
            this._sendSearchRequest();
        }
    }
});

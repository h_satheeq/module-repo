import Ember from 'ember';
import languageDataStore from '../../shared/language/language-data-store';
import sharedService from '../../shared/shared-service';

export default Ember.Object.extend({
    code: '',
    exg: '',
    sym: '',
    status: 0,
    qty: 0,
    avgPrice: 0.0,
    netValue: 0,
    subNo: 0,
    subDate: 0,

    symbolInfo: undefined,
    languageKey: 'rightSubscription',

    init: function () {
        this.set('userSettings', sharedService.userSettings);
    },

    statusLabel: function () {
        var app = languageDataStore.getLanguageObj();
        return app.lang.labels[[this.languageKey, this.get('status')].join('_')];
    }.property('status', 'userSettings.currentLanguage'),

    dSym: function () {
        var symbolInformation = this.get('symbolInfo');
        return symbolInformation ? symbolInformation.dSym : this.get('symbol');
    }.property('symbolInfo', 'sym'),

    setData: function (data) {
        var that = this;

        Ember.$.each(data, function (key, value) {
            that.set(key, value);
        });
    }
});
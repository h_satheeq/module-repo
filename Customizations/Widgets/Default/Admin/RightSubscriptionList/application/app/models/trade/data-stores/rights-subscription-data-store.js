import Ember from 'ember';
import RightsSubscription from '../business-entities/rights-subscription';
import sharedService from '../../shared/shared-service';

export default Ember.Object.extend({
    rightsStore: {},
    rightsCollection: Ember.A(),

    initialize: function () {
        // Implement initialization code here
    },

    getRightsSubscription: function (exchange, symbol, insTyp, subNo) {
        var currentStore = this.get('rightsStore');
        var rightsSubscriptionObj = currentStore[subNo];

        if (!rightsSubscriptionObj) {
            rightsSubscriptionObj = RightsSubscription.create({
                code: subNo
            });

            var symbolInfo = sharedService.getService('price').stockDS.getStock(exchange, symbol, insTyp);
            rightsSubscriptionObj.set('symbolInfo', symbolInfo);

            currentStore[subNo] = rightsSubscriptionObj;
            this.get('rightsCollection').pushObject(rightsSubscriptionObj);
        }

        return rightsSubscriptionObj;
    },

    getRightsSubscriptionCollection: function () {
        return this.get('rightsCollection');
    },

    clearCollection: function () {
        this.set('rightsStore', {});
        this.set('rightsCollection', Ember.A());
    }
});
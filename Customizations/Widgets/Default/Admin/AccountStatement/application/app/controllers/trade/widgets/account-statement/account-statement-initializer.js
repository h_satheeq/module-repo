import BaseModuleInitializer from '../../../../models/shared/initializers/base-module-initializer';
import sharedService from '../../../../models/shared/shared-service';
import StockStatementDataStore from '../../../../models/trade/data-stores/stock-statement-data-store';

export default BaseModuleInitializer.extend({
    postInitialize: function () {
        var tradeService = sharedService.getService('trade');
        tradeService.set('stockStatementDS', StockStatementDataStore.create({tradeService: tradeService}));
    }
});
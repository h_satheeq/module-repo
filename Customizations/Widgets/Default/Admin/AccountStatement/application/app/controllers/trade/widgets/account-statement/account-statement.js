import Ember from 'ember';
import TableController from '../../../../controllers/shared/table-controller';
import tradeWidgetConfig from '../../../../config/trade-widget-config';
import sharedService from '../../../../models/shared/shared-service';
import tradeConstants from '../../../../models/trade/trade-constants';
import userSettings from '../../../../config/user-settings';

// Cell Views
import ClassicHeaderCell from '../../../../views/table/classic-header-cell';
import HeaderCell from '../../../../views/table/dual-cells/header-cell';
import ClassicCell from '../../../../views/table/classic-cell';
import DualChangeCell from '../../../../views/table/dual-cells/dual-change-cell';
import HoldingDesCell from '../../../../views/table/trade/holding-statement-des-cell';
import TableRow from '../../../../views/table/table-row';

export default TableController.extend({
    defaultStatementPeriod: 90,
    sortMode: 0,
    dateFormat: 'YYYYMMDD',
    isCashStatementEnable: false,
    isStatementDropDownEnable: false,
    tradeService: sharedService.getService('trade'),
    mode: undefined,

    accountColl: Ember.A(),
    cashStatements: Ember.A(),
    sortByOptions: Ember.A(),

    defaultOptionForSorting: {},
    defaultOptionForStatements: {},
    currentAccount: {},
    dataObj: {},

    fromDate: '',
    toDate: '',
    toEndDate: '',
    lan: '',
    datePickerFormat: userSettings.displayFormat.dateFormat.toLowerCase(),
    defaultColumnIds: [],

    statementMode: {
        cash: 1,
        stock: 2
    },

    tableHeight: function () {
        return this.get('isCashStatementEnable') ? 'height: calc(100% - 90px);' : 'height: calc(100% - 47px);';
    }.property('isCashStatementEnable'),

    onLoadWidget: function () {
        var mode = this.get('mode');
        this.set('isCashStatementEnable', mode === this.statementMode.cash);

        if (mode === this.statementMode.cash) {
            this.set('title', this.app.lang.labels.cashStatement);
        } else if (mode === this.statementMode.stock) {
            this.set('title', this.app.lang.labels.stockStatement);
        } else {
            this.set('title', this.app.lang.labels.accStatement);
            this.set('isStatementDropDownEnable', true);
        }

        this.tradeService.subscribeTradeMetaReady(this, this.get('wkey'));
        this.tradeService.subscribeTradeDisconnect(this, this.get('wkey'));

        this.setLangLayoutSettings(sharedService.userSettings.currentLanguage);
        this.setCellViewsScopeToGlobal();
        this.setDropDownOptions();
        this.setDatePeriods();
        this.setColumnsConfigs();
    },

    setDropDownOptions: function () {
        var app = this.get('app');
        this.set('statementTypes', [{label: app.lang.labels.cashStatement, code: 'cash'}, {label: app.lang.labels.stockStatement, code: 'stock'}]);
        this.set('sortByOptions', [{sortMode: 0}, {sortMode: 1}]);
        this.set('defaultOptionForStatement', this.get('isCashStatementEnable') ? this.get('statementTypes')[0] : this.get('statementTypes')[1]);
    },

    setDatePeriods: function () {
        var today = new Date();
        this.set('toDate', today);
        this.set('toEndDate', today);

        var fromDate = new Date();
        fromDate.setDate(fromDate.getDate() - this.defaultStatementPeriod);
        this.set('fromDate', fromDate);
    },

    setColumnsConfigs: function () {
        this.set('defaultColumnMapping', this.get('isCashStatementEnable') ? tradeWidgetConfig.cashStatement.defaultColumnMapping : tradeWidgetConfig.stockStatement.defaultColumnMapping);
        this.set('defaultColumnIds', this.get('isCashStatementEnable') ? tradeWidgetConfig.cashStatement.defaultColumnIds : tradeWidgetConfig.stockStatement.defaultColumnIds);

        this.setDefaultColumns();
        this.setDefaultColumnDeclarations();
    },

    onPrepareData: function () {
        this.loadContent();
        this.setErrorMessage();
    },

    onLanguageChanged: function () {
        var mode = this.get('mode');

        this.set('columnDeclarations', []);
        this.set('defaultOptionForStatement', {});

        if (mode === this.statementMode.cash) {
            this.set('title', this.app.lang.labels.cashStatement);
        } else if (mode === this.statementMode.stock) {
            this.set('title', this.app.lang.labels.stockStatement);
        } else {
            this.set('title', this.app.lang.labels.accStatement);
            this.set('isStatementDropDownEnable', true);
        }

        this.setColumnsConfigs();
        this.setDropDownOptions();
        this.setErrorMessage();

        Ember.run.next(this, this.setLangLayoutSettings, sharedService.userSettings.currentLanguage);
    },

    onTradeMetaReady: function () {
        this.loadContent();
    },

    onTradeDisconnect: function () {
        this.onClearData();
    },

    onClearData: function () {
        this.set('accountColl', Ember.A());
        this.set('content', Ember.A());
        this.set('sortByOptions', Ember.A());
        this.set('masterContent', Ember.A());
        this.set('filteredContent', Ember.A());
        this.set('currentAccount', {});
        this.set('columnDeclarations', []);
    },

    loadContent: function () {
        var accountColl = this.tradeService.accountDS.getTradingAccCollection();
        var currentAccount = this.get('currentAccount');

        if (!currentAccount || !currentAccount.tradingAccId) {
            this.set('currentAccount', accountColl.length > 0 ? accountColl[0] : {});
        }

        this.set('accountColl', accountColl);
        this._loadData();
        this.setRequestTimeout(tradeConstants.TimeIntervals.LoadingInterval, 'content.length');
    },

    onUnloadWidget: function () {
        this.tradeService.unSubscribeTradeMetaReady(this, this.get('wkey'));
        this.tradeService.unSubscribeTradeDisconnect(this, this.get('wkey'));
    },

    setCellViewsScopeToGlobal: function () {
        Ember.HeaderCell = HeaderCell;
        Ember.ClassicHeaderCell = ClassicHeaderCell;
        Ember.ClassicCell = ClassicCell;
        Ember.DualChangeCell = DualChangeCell;
        Ember.HoldingDesCell = HoldingDesCell;
        Ember.TableRow = TableRow;
    },

    cellViewsForColumns: {
        classicCell: 'Ember.ClassicCell',
        dualChange: 'Ember.DualChangeCell',
        holdingDesCell: 'Ember.HoldingDesCell'
    },

    onCheckDataAvailability: function () {
        return this.get('content').length !== 0;
    },

    _loadData: function () {
        if (this.get('isCashStatementEnable')) {
            this._sendCashStatementRequest();
        } else {
            this._sendStockStatementRequest();
        }
    },

    _sendStockStatementRequest: function () {
        this.tradeService.stockStatementDS.clearCollection(this.get('wkey'));

        this.setDefaultColumns();
        this.setDefaultColumnDeclarations();

        var sortMode = this.get('sortMode');
        var fromDate = this.get('fromDate');
        var toDate = this.get('toDate');
        var stockStatements = this.tradeService.stockStatementDS.getStockStatementColl();

        this.set('filteredContent', stockStatements);
        this.set('content', stockStatements);
        this.set('masterContent', stockStatements);

        this.setRequestTimeout(tradeConstants.TimeIntervals.LoadingInterval, 'content.length');

        if (!this.utils.validators.isAvailable(toDate)) {
            toDate = new Date();
        }

        if (!this.utils.validators.isAvailable(fromDate)) {
            var defaultFromDate = new Date(toDate);
            defaultFromDate.setDate(defaultFromDate.getDate() - this.defaultStatementPeriod);
            fromDate = defaultFromDate;
        }

        var reqObj = {
            secAccNum: this.get('currentAccount.tradingAccId'),
            frmDate: this.utils.formatters.convertToDisplayTimeFormat(fromDate, this.dateFormat),
            toDate: this.utils.formatters.convertToDisplayTimeFormat(toDate, this.dateFormat),
            sortMode: sortMode
        };

        this.tradeService.sendStockStatementRequest(reqObj);
    },

    _sendCashStatementRequest: function () {
        this.tradeService.cashStatementDS.clearCollection(this.get('wkey'));

        this.setDefaultColumns();
        this.setDefaultColumnDeclarations();

        var sortMode = this.get('sortMode');
        var fromDate = this.get('fromDate');
        var toDate = this.get('toDate');
        var summation = this.tradeService.cashStatementDS.getCashStmSummation();
        var cashStatements = this.tradeService.cashStatementDS.getCashStatementColl();

        this.set('filteredContent', cashStatements);
        this.set('content', cashStatements);
        this.set('masterContent', cashStatements);

        if (!this.utils.validators.isAvailable(toDate)) {
            toDate = new Date();
        }

        if (!this.utils.validators.isAvailable(fromDate)) {
            var defaultFromDate = new Date(toDate);
            defaultFromDate.setDate(defaultFromDate.getDate() - this.defaultStatementPeriod);
            fromDate = defaultFromDate;
        }

        var reqObj = {
            secAccNum: this.get('currentAccount.tradingAccId'),
            frmDate: this.utils.formatters.convertToDisplayTimeFormat(fromDate, this.dateFormat),
            toDate: this.utils.formatters.convertToDisplayTimeFormat(toDate, this.dateFormat),
            sortMode: sortMode,
            startSeq: 0,
            totalNoRec: 20
        };

        this.tradeService.sendCashStatementRequest(reqObj);
        this.set('dataObj', summation);
    },

    _exportHeader: function () {
        var mode = this.get('mode');
        var cusDetails = this.tradeService.userDS;
        var fromDate = this.utils.formatters.convertToDisplayTimeFormat(this.get('fromDate'), userSettings.displayFormat.dateFormat);
        var toDate = this.utils.formatters.convertToDisplayTimeFormat(this.get('toDate'), userSettings.displayFormat.dateFormat);

        var header = '<tbody style="width:100%; font-size: 11px">';
        header += '<tr><td>' + this.app.lang.labels.name + '</td><td>' + cusDetails.cusNme + '</td></tr>';
        header += '<tr><td>' + this.app.lang.labels.period + '</td><td>' + this.app.lang.labels.from + ' ' + fromDate + ' ' + this.app.lang.labels.to + ' ' + toDate + '</td></tr>';
        header += '<tr><td>' + this.app.lang.labels.cusNumber + '</td><td>' + cusDetails.mubNo + '</td></tr>';

        if (mode === this.statementMode.cash) {
            header += '<tr><td>' + this.app.lang.labels.cashACNo + '</td><td>' + this.get('currentAccount.dispProp') + '</td></tr>';
            header += '<tr><td>' + this.app.lang.labels.curr + '</td><td>' + this.get('dataObj.curr') + '</td></tr>';
        } else if (mode === this.statementMode.stock) {
            header += '<tr><td>' + this.app.lang.labels.portfolioId + '</td><td>' + this.get('currentAccount.tradingAccId') + '</td></tr>';
        }

        header += '</tbody>';

        return header;
    },

    _cashStatementSummary: function () {
        var cashHeader = '<tbody style="width:100%; font-size: 11px">';
        cashHeader += '<tr>';
        cashHeader += '<th>' + this.app.lang.labels.openingBal + '</th><th>' + this.app.lang.labels.deposits + '</th><th>' + this.app.lang.labels.withdrawals + '</th><th>' + this.app.lang.labels.buy + '</th><th>' + this.app.lang.labels.sell + '</th><th>' + this.app.lang.labels.other + '</th><th>' + this.app.lang.labels.closingBal + '</th></tr>';
        cashHeader += '<tr><td>' + this.utils.formatters.formatNumber(this.get('dataObj.openBal'), 2) + '</td><td>' + this.utils.formatters.formatNumber(this.get('dataObj.totDeposit'), 2) + '</td><td>' + this.utils.formatters.formatNumber(this.get('dataObj.totWithdrawal'), 2) + '</td><td>' + this.utils.formatters.formatNumber(this.get('dataObj.totBuy'), 2) + '</td><td>' + this.utils.formatters.formatNumber(this.get('dataObj.totSell'), 2) + '</td><td>' + this.utils.formatters.formatNumber(this.get('dataObj.totOther'), 2) + '</td><td>' + this.utils.formatters.formatNumber(this.get('dataObj.closeBal'), 2) + '</td></tr>';
        cashHeader += '</tbody>';
        cashHeader += '</tbody></br>';

        return cashHeader;
    },

    actions: {
        onSubmit: function () {
            this.set('content', Ember.A());
            this._loadData();
        },

        onPrint: function () {
            var mode = this.get('mode');

            this.utils.exportHelper.exportToPrint(this.get('columnDeclarations'), this.get('content'), this.get('title'), this._exportHeader(), mode === this.statementMode.cash ? this._cashStatementSummary() : '');
        },

        // onPdf: function () {
        //    this.utils.exportHelper.exportToPdf(this.get('columnDeclarations'), this.get('content'), this.get('title'), this._exportHeader());
        // },

        onExcel: function () {
            var mode = this.get('mode');

            this.utils.exportHelper.exportToExcel(this.get('columnDeclarations'), this.get('content'), this.get('title'), this._exportHeader(), mode === this.statementMode.cash ? this._cashStatementSummary() : '');
        },

        setSortOption: function (item) {
            this.set('sortMode', item.sortMode);
        },

        setStatementType: function (option) {
            this.set('isCashStatementEnable', option.code === 'cash');
            this.set('content', Ember.A());

            this._loadData();
        },

        setPortfolio: function (option) {
            this.set('currentAccount', option);
        }
    }
});
import Ember from 'ember';
import StockStatement from '../business-entities/stock-statement';
import sharedService from '../../shared/shared-service';

export default Ember.Object.extend({
    store: {},
    trnsCollection: [],

    getStockStatement: function (trnsRef, exg, symbol) {
        var currentStore = this.get('store');
        var stmnt = currentStore[trnsRef];

        if (!stmnt && trnsRef) {
            stmnt = StockStatement.create({
                trnsRef: trnsRef
            });

            if (symbol && exg) {
                var symbolInfo = sharedService.getService('price').stockDS.getStock(exg, symbol);

                if (symbolInfo) {
                    stmnt.set('symbolInfo', symbolInfo);
                }
            }

            currentStore[trnsRef] = stmnt;
            this.addToOtherCollections(stmnt);
        }

        return stmnt;
    },

    addToOtherCollections: function (stmnt) {
        var transCollection = this.get('trnsCollection');
        transCollection.pushObject(stmnt);
    },

    getStockStatementColl: function () {
        return this.get('trnsCollection');
    },

    clearCollection: function () {
        this.set('store', {});
        this.set('trnsCollection', Ember.A());
    }
});

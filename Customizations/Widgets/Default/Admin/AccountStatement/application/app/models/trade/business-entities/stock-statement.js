import Ember from 'ember';

export default Ember.Object.extend({
    trnsRef: '',
    sym: '',
    symName: '',
    date: '',
    des: '',
    qty: '',
    exg: '',
    avgPrice: 0.0,
    symbolInfo: {},

    setData: function (stockStatementDetails) {
        var that = this;

        Ember.$.each(stockStatementDetails, function (key, value) {
            that.set(key, value);
        });
    }
});
export default {
    kycInfo: {
        tabArray: [
            // Field Types: textField: 0, dropDownField: 1, dateField: 2, textAreaField: 3
            // Data Types: string: 0, int: 1, email: 2, double: 3
            {ID: 0, langKey: 'personalInfo', DisplayName: 'Personal Info', key: 'personalInfo', isDynamic: true, dataCollTab: [
                {grpId: 0, langKey: 'personalInfo', grpDes: 'Personal Info', dataCollGroup: [
                    {id: '0', displayDes: 'Gender', langKey: 'gender', fieldType: 1, prop: 'gender', dataType: 0, isDisabled: true, isRequired: true, collection: [{displayDes: 'Male'}, {displayDes: 'Female'}]},
                    {id: '1', displayDes: 'Title', langKey: 'title', fieldType: 1, value: 'Mr.', isRequired: false, dataType: 0, isDisabled: false, collection: [{displayDes: 'Mr.'}, {displayDes: 'Miss.'}, {displayDes: 'Mrs.'}, {displayDes: 'Ref.'}], prop: 'title'},
                    {id: '2', displayDes: 'Family', langKey: 'family', fieldType: 0, prop: 'family', dataType: 0, isDisabled: true, isRequired: true},
                    {id: '3', displayDes: 'First Name', langKey: 'firstName', fieldType: 0, prop: 'firstName', dataType: 0, isDisabled: true, isRequired: true},
                    {id: '4', displayDes: 'Second Name', langKey: 'secondName', fieldType: 0, prop: 'secondName', dataType: 0, isDisabled: true, isRequired: true},
                    {id: '5', displayDes: 'Third Name', langKey: 'thirdName', fieldType: 0, prop: 'thirdName', dataType: 0, isDisabled: true, isRequired: false},
                    {id: '6', displayDes: 'Passport No', langKey: 'passportNo', fieldType: 0, prop: 'passportNo', isRequired: false, dataType: 0, isDisabled: false},
                    {id: '7', displayDes: 'Nationality', langKey: 'nationality', fieldType: 0, prop: 'nationality', isRequired: true, dataType: 0, isDisabled: true},
                    {id: '8', displayDes: 'Date of Birth(Hijiri)', langKey: 'dobHijri', fieldType: 0, prop: 'dob', isRequired: false, dataType: 0, isDisabled: true},
                    {id: '9', displayDes: 'Country of Birth', langKey: 'cob', fieldType: 0, prop: 'cob', isRequired: true, dataType: 0, isDisabled: true},
                    {id: '10', displayDes: 'Place of Birth', langKey: 'pob', fieldType: 0, prop: 'pob', isRequired: false, dataType: 0, isDisabled: true},
                    {id: '11', displayDes: 'Marital Status', langKey: 'maritalSts', fieldType: 1, prop: 'maritalStatus', isRequired: true, dataType: 0, isDisabled: false, collection: [{displayDes: 'Single'}, {displayDes: 'Married'}]},
                    {id: '12', displayDes: 'Profession', langKey: 'profession', fieldType: 0, prop: 'profession', isRequired: false, dataType: 0, isDisabled: false},
                    {id: '13', displayDes: 'No of dependents', langKey: 'noOfDependencies', fieldType: 0, prop: 'nod', isRequired: false, dataType: 1, isDisabled: false}
                ]},
                {grpId: 1, langKey: 'idInfo', grpDes: 'Identity Info', dataCollGroup: [
                    {id: '0', displayDes: 'Identity Type', langKey: 'idType', fieldType: 1, dataType: 0, isDisabled: true, prop: 'identityType', collection: [{displayDes: 'NIN'}]},
                    {id: '1', displayDes: 'Identity Number', langKey: 'idNo', fieldType: 0, dataType: 0, isDisabled: true, prop: 'identityNumber'},
                    {id: '2', displayDes: 'Issue Place', langKey: 'issuePlace', fieldType: 1, dataType: 0, isDisabled: false, prop: 'issuePlace', collection: [{displayDes: 'Riyadh'}]},
                    {id: '3', displayDes: 'ID Expiry Date(Hijri)', langKey: 'idExpDateHijri', fieldType: 2, dataType: 0, isDisabled: false, prop: 'idExpDateHijri'},
                    {id: '4', displayDes: 'ID Expiry Date', langKey: 'idExpDate', fieldType: 2, dataType: 0, isDisabled: false, prop: 'idExpDate'}
                ]}
            ]},
            {ID: 1, langKey: 'contactInfo', DisplayName: 'Contact Info', key: 'contactInfo', isDynamic: true, dataCollTab: [
                {grpId: 0, langKey: 'contactInfo', grpDes: 'Contact Info', dataCollGroup: [
                    {id: '0', displayDes: 'P.O Box', langKey: 'poBox', fieldType: 0, prop: 'pobox', dataType: 0, isDisabled: true, isRequired: true},
                    {id: '1', displayDes: 'Zip', langKey: 'zip', fieldType: 0, prop: 'zip', isRequired: true, dataType: 0, isDisabled: false},
                    {id: '2', displayDes: 'Building No', langKey: 'buildingNo', fieldType: 0, prop: 'buildingNo', dataType: 0, isDisabled: false, isRequired: false},
                    {id: '3', displayDes: 'Fax', langKey: 'fax', fieldType: 0, prop: 'fax', dataType: 0, isDisabled: false, isRequired: false},
                    {id: '4', displayDes: 'Country', langKey: 'country', fieldType: 1, prop: 'country', dataType: 0, isDisabled: true, isRequired: true, collection: [{displayDes: 'Saudi Arabia / Null'}]},
                    {id: '5', displayDes: 'City', langKey: 'city', fieldType: 1, prop: 'city', dataType: 0, isDisabled: false, isRequired: true, collection: [{displayDes: 'Riyadh'}, {displayDes: 'Riyadh Town'}]},
                    {id: '6', displayDes: 'District', langKey: 'district', fieldType: 0, prop: 'district', isRequired: true, dataType: 0, isDisabled: false},
                    {id: '7', displayDes: 'Street Name', langKey: 'streetName', fieldType: 0, prop: 'streetName', isRequired: false, dataType: 0, isDisabled: false},
                    {id: '8', displayDes: 'Home Tel', langKey: 'homeTel', fieldType: 0, prop: 'homeTel', isRequired: true, dataType: 0, isDisabled: true},
                    {id: '9', displayDes: 'Mobile No', langKey: 'mobileNo', fieldType: 0, prop: 'mobileNo', isRequired: true, dataType: 0, isDisabled: true},
                    {id: '10', displayDes: 'Office Tel', langKey: 'officeTel', fieldType: 0, prop: 'officeTel', isRequired: false, dataType: 0, isDisabled: false},
                    {id: '11', displayDes: 'Email Address', langKey: 'emailAddress', fieldType: 0, prop: 'email', isRequired: true, dataType: 2, isDisabled: true}
                ]}
            ]},
            {ID: 2, langKey: 'bankAccount', DisplayName: 'Bank Account', key: 'bankInfo', isDynamic: true, dataCollTab: [
                {grpId: 0, langKey: 'bankInfo', grpDes: 'Bank Information', isSubHeadingEnable: true, dataCollGroup: [
                    {id: '0', displayDes: 'Bank Name', langKey: 'bnkName', fieldType: 1, prop: 'bnkName', dataType: 0, isDisabled: true, isRequired: false, collection: [{displayDes: 'Bank Al-bilad'}, {displayDes: 'Anb-Invest'}]},
                    {id: '1', displayDes: 'Main Account Number', langKey: 'mainAccNo', fieldType: 0, prop: 'bnkMainAcc', dataType: 0, isDisabled: true, isRequired: false},
                    {id: '2', displayDes: 'Branch Name', langKey: 'branchName', fieldType: 0, prop: 'bnkBranch', isRequired: false, dataType: 0, isDisabled: true}
                ]}
            ]},
            {ID: 3, langKey: 'empInfo', DisplayName: 'Employement Info', key: 'empInfo', isDynamic: true, dataCollTab: [
                {grpId: 0, langKey: 'contactInfo', grpDes: 'Contact Info', dataCollGroup: [
                    {id: '0', displayDes: 'Date of Employment', langKey: 'dateOfEmp', fieldType: 2, prop: 'empDate', dataType: 0, isDisabled: false, isRequired: false},
                    {id: '1', displayDes: 'Employer\'s Name', langKey: 'empName', fieldType: 0, prop: 'empZip', isRequired: false, dataType: 0, isDisabled: false},
                    {id: '2', displayDes: 'P.O Box', langKey: 'poBox', fieldType: 0, prop: 'empPOBox', dataType: 0, isDisabled: false, isRequired: false},
                    {id: '3', displayDes: 'Country', langKey: 'country', fieldType: 1, prop: 'empCountry', dataType: 0, isDisabled: false, isRequired: true, collection: [{displayDes: 'Saudi Arabia / Null'}]},
                    {id: '4', displayDes: 'City', langKey: 'city', fieldType: 1, prop: 'empCity', dataType: 0, isDisabled: false, isRequired: true, collection: [{displayDes: 'Riyadh'}, {displayDes: 'Riyadh Town'}]},
                    {id: '5', displayDes: 'Zip', langKey: 'zip', fieldType: 0, prop: 'empZip', dataType: 0, isDisabled: false, isRequired: false},
                    {id: '6', displayDes: 'Phone No', langKey: 'phoneNo', fieldType: 0, prop: 'empPhone', isRequired: false, dataType: 0, isDisabled: false},
                    {id: '7', displayDes: 'Email Address', langKey: 'emailAddress', fieldType: 0, prop: 'empEmail', isRequired: true, dataType: 2, isDisabled: false}
                ]}
            ]},
            {ID: 4, langKey: 'attrnyTrustee', DisplayName: 'Attorney / Trustee', key: 'attorneyInfo', isDynamic: true, dataCollTab: [
                {grpId: 0, langKey: 'contactInfoAttTrs', grpDes: 'Contact Information (Attorney / Trustee)', isSubHeadingEnable: true, dataCollGroup: [
                    {id: '0', displayDes: 'Name of the Contact', langKey: 'nameOfContact', fieldType: 0, prop: 'attrContactName', dataType: 0, isDisabled: true, isRequired: false},
                    {id: '1', displayDes: 'Address for Correspondence Line 1', langKey: 'addrsCorrspLine1', fieldType: 0, prop: 'attrAddress1', isRequired: false, dataType: 0, isDisabled: true},
                    {id: '2', displayDes: 'Address for Correspondence Line 2', langKey: 'addrsCorrspLine2', fieldType: 0, prop: 'attrAddress2', dataType: 0, isDisabled: true, isRequired: false},
                    {id: '3', displayDes: 'Business phone number', langKey: 'businessPhoneNo', fieldType: 0, prop: 'attrBusinessPhone', dataType: 0, isDisabled: true, isRequired: false},
                    {id: '4', displayDes: 'Business Fax number', langKey: 'businessFaxNo', fieldType: 0, prop: 'attrBusinessFax', dataType: 0, isDisabled: true, isRequired: false, collection: [{displayDes: 'Saudi Arabia / Null'}]},
                    {id: '5', displayDes: 'Mobile No', langKey: 'mobileNo', fieldType: 0, prop: 'attrMobileNo', dataType: 0, isDisabled: true, isRequired: false, collection: [{displayDes: 'Riyadh'}, {displayDes: 'Riyadh Town'}]},
                    {id: '6', displayDes: 'Custodian Name', langKey: 'custodianName', fieldType: 0, prop: 'attrCustodianName', isRequired: false, dataType: 0, isDisabled: true},
                    {id: '7', displayDes: 'Account Number', langKey: 'accNo', fieldType: 0, prop: 'attrAccNo', isRequired: false, dataType: 0, isDisabled: true}
                ]},
                {grpId: 0, langKey: 'idInfoAttrTrustee', grpDes: 'ID Information (Attorney / Trustee)', isSubHeadingEnable: true, dataCollGroup: [
                    {id: '0', displayDes: 'Identity Number', langKey: 'idNo', fieldType: 0, prop: 'attrID', dataType: 0, isDisabled: true, isRequired: false},
                    {id: '1', displayDes: 'ID Expiry Date', langKey: 'idExpDate', fieldType: 2, prop: 'attrIDExpDate', isRequired: false, dataType: 0, isDisabled: true}
                ]}
            ]},
            {ID: 5, langKey: 'financialInfo', DisplayName: 'Financial Info', key: 'financial', isDynamic: true, dataCollTab: [
                {grpId: 0, langKey: 'financialInfo', grpDes: 'Financial Info', dataCollGroup: [
                    {id: '0', displayDes: 'Approximate Annual Income(In SAR)', langKey: 'appAnnualIncome', fieldType: 1, prop: 'annualIncome', dataType: 0, isDisabled: false, isRequired: true, collection: [{displayDes: '0 - 100,000'}, {displayDes: '100,001 - 250,000'}, {displayDes: '250,001 - 500,000'}, {displayDes: '500,001 - 1,000,000'}]},
                    {id: '1', displayDes: 'Approximate Net Worth in SAR(Excluding Residence)', langKey: 'appNetworth', fieldType: 1, prop: 'netWorth', dataType: 0, isDisabled: false, isRequired: true, collection: [{displayDes: '0 - 100,000'}, {displayDes: '100,001 - 250,000'}, {displayDes: '250,001 - 500,000'}, {displayDes: '500,001 - 1,000,000'}]},
                    {id: '2', displayDes: 'Is the Client a Director or Officer of a publicly Listed Company', langKey: 'isClientDirectorCom', fieldType: 1, prop: 'directorOfficer', dataType: 0, isDisabled: false, isRequired: true, collection: [{displayDes: 'Yes'}, {displayDes: 'No'}]},
                    {id: '3', displayDes: 'Specify the source of wealth/income and any additional information on the client\'s financial situation', langKey: 'specifySourceOfWealth', fieldType: 3, prop: 'sourceWealth', isRequired: false, dataType: 0, isDisabled: false}
                ]}
            ]},
            {ID: 6, langKey: 'investmentInfo', DisplayName: 'Investment Info', isDynamic: false, outlet: 'investment-info', key: 'investmentInfo', dataCollTab: [{grpId: 0, langKey: 'gender', grpDes: 'Preferences', dataCollGroup: [
                {id: '0', displayDes: 'Cus ID', langKey: 'gender', fieldType: 0, prop: 'a'},
                {id: '1', displayDes: 'Cus Type', langKey: 'gender', fieldType: 0, prop: 'b'},
                {id: '2', displayDes: 'Cus Address', langKey: 'gender', fieldType: 0, prop: 'c'},
                {id: '3', displayDes: 'Cus Province', langKey: 'gender', fieldType: 0, prop: 'a'}
            ]}]},
            {ID: 7, langKey: 'clientIdealInvestment', DisplayName: 'Client\'s Ideal Investments', isDynamic: false, outlet: 'investments', key: 'investments', dataCollTab: [{grpId: 0, langKey: 'gender', grpDes: 'Preferences', dataCollGroup: [
                {id: '0', displayDes: 'Cus ID', langKey: 'gender', fieldType: 0, prop: 'sharesHighRisk'},
                {id: '1', displayDes: 'Cus Type', langKey: 'gender', fieldType: 0, prop: 'sharesMedRisk'},
                {id: '2', displayDes: 'Cus Address', langKey: 'gender', fieldType: 0, prop: 'debtInstrmntsHighRisk'},
                {id: '3', displayDes: 'Cus Province', langKey: 'gender', fieldType: 0, prop: 'debtInstrmntsMedRisk'},
                {id: '4', displayDes: 'Cus Province', langKey: 'gender', fieldType: 0, prop: 'debtInstrmntsLowRisk'},
                {id: '5', displayDes: 'Cus Province', langKey: 'gender', fieldType: 0, prop: 'invstmntFundHighRisk'},
                {id: '6', displayDes: 'Cus Province', langKey: 'gender', fieldType: 0, prop: 'invstmntFundMedRisk'},
                {id: '7', displayDes: 'Cus Province', langKey: 'gender', fieldType: 0, prop: 'invstmntFundLowRisk'},
                {id: '8', displayDes: 'Cus Province', langKey: 'gender', fieldType: 0, prop: 'tradeFinanceLowRisk'},
                {id: '9', displayDes: 'Cus Province', langKey: 'gender', fieldType: 0, prop: 'commdtsHighRisk'},
                {id: '10', displayDes: 'Cus Province', langKey: 'gender', fieldType: 0, prop: 'optionsHighRisk'}
            ]}]},
            {ID: 8, langKey: 'preferences', DisplayName: 'Preferences', isDynamic: false, outlet: 'preferences', key: 'preferences', dataCollTab: [{grpId: 0, langKey: 'gender', grpDes: 'Preferences', dataCollGroup: [
                {id: '0', displayDes: 'Cus ID', langKey: 'gender', fieldType: 0, prop: 'a'},
                {id: '1', displayDes: 'Cus Type', langKey: 'gender', fieldType: 0, prop: 'b'},
                {id: '2', displayDes: 'Cus Address', langKey: 'gender', fieldType: 0, prop: 'c'},
                {id: '3', displayDes: 'Cus Province', langKey: 'gender', fieldType: 0, prop: 'a'}
            ]}]}
        ]
    }
};

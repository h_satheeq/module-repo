import BaseArrayController from '../../../base-array-controller';
import utils from '../../../../utils/utils';

export default BaseArrayController.extend({
    knowledgeLevelCollection: [{displayDes: 'Extensive'}, {displayDes: 'High'}, {displayDes: 'Low'}, {displayDes: 'Medium'}],
    riskTypeCollection: [{displayDes: 'High'}, {displayDes: 'Medium'}, {displayDes: 'Low'}],
    investmentObjectiveCollection: [{displayDes: 'Income'}],
    assetCollection: [{displayDes: 'Denominations in SAR'}],
    clientClassificationCollection: [{displayDes: 'An Execution Only Client'}],
    isRequiredFieldsNotFilled: false,
    dataObj: {shares: '', investmentFunds: '', debtInstrument: '', commodities: '', foreignExchanges: '', derivateAndOptions: '', deposits: '', realEstate: '', tradeFinance: '', investKnowledge: '', investProfile: '', generalInvestObj: '', clientPreferedInvest: '', foreignCurr: '', clientClass: ''},

    totVal: function () {
        return parseInt(this.get('dataObj.shares') ? this.get('dataObj.shares') : 0, 10) + parseInt(this.get('dataObj.investmentFunds') ? this.get('dataObj.investmentFunds') : 0, 10) + parseInt(this.get('dataObj.debtInstrument') ? this.get('dataObj.debtInstrument') : 0, 10) + parseInt(this.get('dataObj.commodities') ? this.get('dataObj.commodities') : 0, 10) + parseInt(this.get('dataObj.foreignExchanges') ? this.get('dataObj.foreignExchanges') : 0, 10) + parseInt(this.get('dataObj.derivateAndOptions') ? this.get('dataObj.derivateAndOptions') : 0, 10) + parseInt(this.get('dataObj.deposits') ? this.get('dataObj.deposits') : 0, 10) + parseInt(this.get('dataObj.realEstate') ? this.get('dataObj.realEstate') : 0, 10) + parseInt(this.get('dataObj.tradeFinance') ? this.get('dataObj.tradeFinance') : 0, 10);
    }.property('dataObj.shares', 'dataObj.investmentFunds', 'dataObj.debtInstrument', 'dataObj.commodities', 'dataObj.foreignExchanges', 'dataObj.derivateAndOptions', 'dataObj.deposits', 'dataObj.realEstate', 'dataObj.tradeFinance'),

    onPrepareData: function () {
        var businessObj = this.get('businessObj');

        this.set('dataObj.shares', businessObj.shares);
        this.set('dataObj.investmentFunds', businessObj.investmentFunds);
        this.set('dataObj.debtInstrument', businessObj.debtInstrument);
        this.set('dataObj.commodities', businessObj.commodities);
        this.set('dataObj.foreignExchanges', businessObj.foreignExchanges);
        this.set('dataObj.derivateAndOptions', businessObj.derivateAndOptions);
        this.set('dataObj.deposits', businessObj.deposits);
        this.set('dataObj.realEstate', businessObj.realEstate);
        this.set('dataObj.tradeFinance', businessObj.tradeFinance);
        this.set('dataObj.investKnowledge', businessObj.investKnowledge);
        this.set('dataObj.investProfile', businessObj.investProfile);
        this.set('dataObj.generalInvestObj', businessObj.generalInvestObj);
        this.set('dataObj.clientPreferedInvest', businessObj.clientPreferedInvest);
        this.set('dataObj.foreignCurr', businessObj.foreignCurr);
        this.set('dataObj.clientClass', businessObj.clientClass);
    },

    actions: {
        onSaveInvestmentsInfo: function () {
            var isRequiredFieldsAvailable = utils.validators.isAvailable(this.get('dataObj.investKnowledge')) && utils.validators.isAvailable(this.get('dataObj.generalInvestObj')) && utils.validators.isAvailable(this.get('dataObj.clientClass'));

            if (isRequiredFieldsAvailable) {
                this.set('isRequiredFieldsNotFilled', false);
                this.onSave(this.get('dataObj'));
            } else {
                this.set('isRequiredFieldsNotFilled', true);
            }
        }
    }
});
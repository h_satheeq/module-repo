import Ember from 'ember';
import appConfig from '../../../../config/app-config';
import BaseController from '../../../base-controller';
import Captcha from '../../../../components/captcha-component';
import sharedService from '../../../../models/shared/shared-service';
import utils from '../../../../utils/utils';

export default BaseController.extend({
    dataObj: {preferredLanguage: '', callingMethod: '', isTermConditionsAccepted: ''},
    languageCollection: [],
    callingMethodCollection: [],
    errorCollection: [],

    isCaptchaValReceived: false,
    validationCallbackFn: undefined,

    onPrepareData: function () {
        var app = this.get('app');
        var supportedLanguages = appConfig.customisation.supportedLanguages;
        var languageCollection = [{code: supportedLanguages[0].code, displayDes: supportedLanguages[0].desc}, {code: supportedLanguages[1].code, displayDes: supportedLanguages[1].desc}];
        var callingMethodCollection = [{displayDes: app.lang.labels.mobile}, {displayDes: app.lang.labels.home}, {displayDes: app.lang.labels.office}];
        var preferredLanguage = this.get('dataObj.preferredLanguage') ? this.get('dataObj.preferredLanguage') : supportedLanguages[0].code;
        var callingMethod = this.get('dataObj.callingMethod') ? this.get('dataObj.callingMethod') : app.lang.labels.mobile;

        this.set('languageCollection', languageCollection);
        this.set('callingMethodCollection', callingMethodCollection);
        this.set('dataObj.preferredLanguage', preferredLanguage);
        this.set('dataObj.callingMethod', callingMethod);
    },

    showTermsAndConditions: function () {
        window.open('/userTC_' + sharedService.userSettings.currentLanguage, '_blank');
    },

    changeTermConditionsAccepted: function () {
        this.set('dataObj.isTermConditionsAccepted', !('dataObj.isTermConditionsAccepted'));
    },

    getDataObject: function () {
        return this.get('dataObj');
    },

    onValidate: function (validationCallbackFn) {
        var errorCollection = [];
        var isRequiredFieldsAvailable = utils.validators.isAvailable(this.get('dataObj.preferredLanguage')) && utils.validators.isAvailable(this.get('dataObj.callingMethod')) && this.get('dataObj.isTermConditionsAccepted');

        if (!isRequiredFieldsAvailable) {
            errorCollection[errorCollection.length] = this.get('app').lang.messages.acceptTermsConditions;
        }

        this.toggleProperty('isCaptchaValReceived');
        this.set('validationCallbackFn', validationCallbackFn);
        this.set('errorCollection', errorCollection);
    },

    _onCaptchaValidationReceived: function (errorCol) {
        var errors = this.get('errorCollection').concat(errorCol);
        var callbackFn = this.get('validationCallbackFn');

        if (callbackFn && Ember.$.isFunction(callbackFn)) {
            callbackFn(errors);
        }
    },

    actions: {
        onURL: function () {
            this.showTermsAndConditions();
        },

        verifyCaptcha: function (errorCol) {
            this._onCaptchaValidationReceived(errorCol);
        }
    }
});

Ember.Handlebars.helper('captcha-component', Captcha);

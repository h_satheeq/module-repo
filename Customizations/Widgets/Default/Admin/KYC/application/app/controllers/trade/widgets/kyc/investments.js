import BaseArrayController from '../../../base-array-controller';
import utils from '../../../../utils/utils';

export default BaseArrayController.extend({
    headerAvailable: false,
    isRequiredFieldsNotFilled: false,
    dataObj: {sharesHighRisk: 0, sharesMedRisk: 0, debtInstrmntsHighRisk: 0, debtInstrmntsMedRisk: 0, debtInstrmntsLowRisk: 0, invstmntFundHighRisk: 0, invstmntFundMedRisk: 0, invstmntFundLowRisk: 0, tradeFinanceLowRisk: 0, commdtsHighRisk: 0, optionsHighRisk: 0},

    totVal: function () {
        return parseInt(this.get('dataObj.sharesHighRisk') ? this.get('dataObj.sharesHighRisk') : 0, 10) + parseInt(this.get('dataObj.sharesMedRisk') ? this.get('dataObj.sharesMedRisk') : 0, 10) + parseInt(this.get('dataObj.debtInstrmntsHighRisk') ? this.get('dataObj.debtInstrmntsHighRisk') : 0, 10) + parseInt(this.get('dataObj.debtInstrmntsMedRisk') ? this.get('dataObj.debtInstrmntsMedRisk') : 0, 10) + parseInt(this.get('dataObj.debtInstrmntsLowRisk') ? this.get('dataObj.debtInstrmntsLowRisk') : 0, 10) + parseInt(this.get('dataObj.invstmntFundHighRisk') ? this.get('dataObj.invstmntFundHighRisk') : 0, 10) + parseInt(this.get('dataObj.invstmntFundMedRisk') ? this.get('dataObj.invstmntFundMedRisk') : 0, 10) + parseInt(this.get('dataObj.invstmntFundLowRisk') ? this.get('dataObj.invstmntFundLowRisk') : 0, 10) + parseInt(this.get('dataObj.tradeFinanceLowRisk') ? this.get('dataObj.tradeFinanceLowRisk') : 0, 10) + parseInt(this.get('dataObj.commdtsHighRisk') ? this.get('dataObj.commdtsHighRisk') : 0, 10) + parseInt(this.get('dataObj.optionsHighRisk') ? this.get('dataObj.optionsHighRisk') : 0, 10);
    }.property('dataObj.sharesHighRisk', 'dataObj.sharesMedRisk', 'dataObj.debtInstrmntsHighRisk', 'dataObj.debtInstrmntsMedRisk', 'dataObj.debtInstrmntsLowRisk', 'dataObj.invstmntFundHighRisk', 'dataObj.invstmntFundMedRisk', 'dataObj.invstmntFundLowRisk', 'dataObj.tradeFinanceLowRisk', 'dataObj.commdtsHighRisk', 'dataObj.optionsHighRisk'),

    onPrepareData: function () {
        var businessObj = this.get('businessObj');

        this.set('dataObj.sharesHighRisk', businessObj.sharesHighRisk);
        this.set('dataObj.sharesMedRisk', businessObj.sharesMedRisk);
        this.set('dataObj.debtInstrmntsHighRisk', businessObj.debtInstrmntsHighRisk);
        this.set('dataObj.debtInstrmntsMedRisk', businessObj.debtInstrmntsMedRisk);
        this.set('dataObj.debtInstrmntsLowRisk', businessObj.debtInstrmntsLowRisk);
        this.set('dataObj.invstmntFundHighRisk', businessObj.invstmntFundHighRisk);
        this.set('dataObj.invstmntFundMedRisk', businessObj.invstmntFundMedRisk);
        this.set('dataObj.invstmntFundLowRisk', businessObj.invstmntFundLowRisk);
        this.set('dataObj.tradeFinanceLowRisk', businessObj.tradeFinanceLowRisk);
        this.set('dataObj.commdtsHighRisk', businessObj.commdtsHighRisk);
        this.set('dataObj.optionsHighRisk', businessObj.optionsHighRisk);
    },

    actions: {
        onSaveInvestments: function () {
            var isRequiredFieldsAvailable = utils.validators.isAvailable(this.get('dataObj.sharesHighRisk')) && utils.validators.isAvailable(this.get('dataObj.sharesMedRisk')) && utils.validators.isAvailable(this.get('dataObj.debtInstrmntsHighRisk')) && utils.validators.isAvailable(this.get('dataObj.debtInstrmntsMedRisk')) && utils.validators.isAvailable(this.get('dataObj.debtInstrmntsLowRisk')) && utils.validators.isAvailable(this.get('dataObj.invstmntFundHighRisk')) && utils.validators.isAvailable(this.get('dataObj.invstmntFundMedRisk')) && utils.validators.isAvailable(this.get('dataObj.invstmntFundLowRisk')) && utils.validators.isAvailable(this.get('dataObj.tradeFinanceLowRisk')) && utils.validators.isAvailable(this.get('dataObj.commdtsHighRisk')) && utils.validators.isAvailable(this.get('dataObj.optionsHighRisk'));

            if (isRequiredFieldsAvailable) {
                this.set('isRequiredFieldsNotFilled', false);
                this.onSave(this.get('dataObj'));
            } else {
                this.set('isRequiredFieldsNotFilled', true);
            }
        }
    }
});
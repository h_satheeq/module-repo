import BaseModuleInitializer from '../../../../models/shared/initializers/base-module-initializer';
import sharedService from '../../../../models/shared/shared-service';
import CustomerDataStore from '../../../../models/trade/data-stores/customer-data-store';
import KYCMasterDataStore from '../../../../models/trade/data-stores/kyc-master-data-store';

export default BaseModuleInitializer.extend({
    postInitialize: function () {
        var tradeService = sharedService.getService('trade');
        var kycMasterDSInstance = KYCMasterDataStore.create({tradeService: tradeService});

        kycMasterDSInstance.initialize();

        tradeService.set('kycMasterDS', kycMasterDSInstance);
        tradeService.set('customerDS', CustomerDataStore.create({tradeService: tradeService}));
    }
});

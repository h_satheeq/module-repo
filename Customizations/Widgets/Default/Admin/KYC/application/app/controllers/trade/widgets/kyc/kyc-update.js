import Ember from 'ember';
import BaseController from '../../../base-controller';
import kycConfig from '../../../../config/kyc-config';
import utils from '../../../../utils/utils';
import ControllerFactory from '../../../../controllers/controller-factory';
import sharedService from '../../../../models/shared/shared-service';
import userSettings from '../../../../config/user-settings';
import islamicDateConverter from './islamic-date-converter';

export default BaseController.extend({
    isTabsAvailable: true,
    isKYCDataUpdated: false,
    isDynamic: true,
    isFirstTab: true,
    isNextTab: false,
    isLastTab: false,
    isNinIqamaSuccess: true,

    currentID: undefined,
    nationalityId: '2',
    kycArr: undefined,
    changedSet: undefined,
    defaultOption: undefined,
    invalidDate: 'Invalid date',
    isUpdated: false,
    isUpdateSuccess: false,

    groupCollection: [],
    kycTabsArr: [],
    errorCollection: [],
    messageCollection: [],

    kycUpdate: Ember.Object.create({}),

    kycWidgetConfig: Ember.A(kycConfig.kycInfo.tabArray),
    tradeService: sharedService.getService('trade'),

    title: undefined,

    datePickerFormat: userSettings.displayFormat.dateFormat.toLowerCase(),
    lan: '',
    nin: '',
    isKYC: undefined,
    ninIqamaSuccessCount: 0,

    fieldTypes: {
        textField: 0,
        dropDownField: 1,
        dateField: 2,
        textAreaField: 3,
        checkBoxField: 4
    },

    dataTypes: {
        string: 0,
        int: 1,
        email: 2,
        double: 3
    },

    kycUpdateStatus: {
        Success: 1,
        SystemError: -1,
        PartiallyUpdated: -2,
        RequiredDataMissing: -3
    },

    kycUpdateType: {
        KYC: 2,
        OAO: 3
    },

    ninIqamaStatus: {
        Success: '1',
        Fail: '-1'
    },

    setKYCDetails: function () {
        if(!this.get('isKYCDataUpdated') || !this.get('isKYC')) {
            Ember.run.once(this, this._loadDataContent);
        }
    }.observes('kycArr.@each'),

    updateKYCMasterData: function () {
        Ember.run.once(this, this.loadMasterContent);
    }.observes('tradeService.kycMasterDS.isRefreshed'),

    isSaveEnabled: function () {
        return this.get('isKYC') || this.get('isLastTab');
    }.property('isKYC', 'isLastTab'),

    onLoadWidget: function () {
        this.set('lan', sharedService.userSettings.currentLanguage.toLowerCase());
    },

    onPrepareData: function () {
        this.set('kycArr', this.tradeService.customerDS.getKYCColl(this.tradeService.userDS.get('mubNo')));

        if (this.get('isKYC')) {
            this.set('title', this.get('app').lang.labels.kycUpdate);
        }

        this.setLayout();
    },

    onAddSubscription: function () {
        if (this.get('isKYC')) {
            this.tradeService.sendKYCDetailsRequest({
                sMubNo: this.tradeService.userDS.get('mubNo'),
                lan: sharedService.userSettings.currentLanguage
            });
        }

        this.tradeService.kycMasterDS.sendMasterDataRequest();
    },

    onAfterRender: function () {
        this.toggleFullScreen();

        Ember.run.later(this, this._loadIslamicCalender, 3000); // Temporary fix: Delaying event registration on date picker
    },

    _loadIslamicCalender: function () {
        Ember.$('#dobHijri').calendarsPicker(Ember.$.extend({calendar: Ember.$.calendars.instance('islamic', 'ar'), maxDate: 0}, Ember.$.calendarsPicker.regionalOptions.ar));
        Ember.$('#issueDateHijri').calendarsPicker(Ember.$.extend({calendar: Ember.$.calendars.instance('islamic', 'ar'), maxDate: 0}, Ember.$.calendarsPicker.regionalOptions.ar));
        Ember.$('#idExpDateHijri').calendarsPicker(Ember.$.extend({calendar: Ember.$.calendars.instance('islamic', 'ar'), minDate: 0}, Ember.$.calendarsPicker.regionalOptions.ar));
        Ember.$('#empDateHijri').calendarsPicker(Ember.$.extend({calendar: Ember.$.calendars.instance('islamic', 'ar'), maxDate: 0}, Ember.$.calendarsPicker.regionalOptions.ar));
    },

    toggleFullScreen: function () {
        this._super(this.get('widgetContainerKey'), this.get('wkey'));
    },

    displayTabs: function () {
        var arrTabs = Ember.A();
        var tabCollection = this.get('kycWidgetConfig');
        var kycTab = {};
        var that = this;

        var lanStore = this.get('app').lang.labels;

        Ember.$.each(tabCollection, function (key, value) {
            if (value) {
                kycTab = tabCollection[key];
                Ember.set(kycTab, 'DisplayName', lanStore[kycTab.langKey]);
                if (value.ID === 8 && that.get('isKYC')) {
                    // Preference tab not showing.
                } else {
                    arrTabs.pushObject(kycTab);
                }
            }
        });

        if (!this.get('currentID')) {
            Ember.set(arrTabs[0], 'css', 'active');
            this.set('currentID', 0);
        }

        this.set('kycTabsArr', arrTabs);
    },

    setLayout: function () {
        var kycConfiguration = this.get('kycWidgetConfig');
        var that = this;

        Ember.$.each(kycConfiguration, function (tabId) {
            if (kycConfiguration[tabId].isDynamic) {
                var tabData = kycConfiguration[tabId].dataCollTab;

                Ember.$.each(tabData, function (grpId) {
                    var dataCol = tabData[grpId].dataCollGroup;

                    Ember.$.each(dataCol, function (fieldId) {
                        var fieldType = dataCol[fieldId].fieldType;
                        var field = dataCol[fieldId];

                        if (!that.get('isKYC')) {
                            if (field.prop !== 'ibanNo') {
                                Ember.set(field, 'isDisabled', false);
                            }
                        }

                        switch (fieldType) {
                            case that.fieldTypes.textField :
                                Ember.set(field, 'isText', true);
                                break;

                            case that.fieldTypes.dropDownField :
                                Ember.set(field, 'isDropdown', true);
                                break;

                            case that.fieldTypes.textAreaField :
                                Ember.set(field, 'isTextArea', true);
                                break;

                            case that.fieldTypes.dateField :
                                Ember.set(field, 'isDate', true);

                                if (utils.validators.isAvailable(field.endDate)) {
                                    var endDate = new Date();

                                    endDate.setDate(endDate.getDate() + field.endDate);
                                    Ember.set(field, 'toEndDate', endDate);
                                }

                                if (utils.validators.isAvailable(field.startDate)) {
                                    var startDate = new Date();

                                    startDate.setDate(startDate.getDate() + dataCol[fieldId].startDate);
                                    Ember.set(field, 'fromStartDate', startDate);
                                }

                                break;

                            case that.fieldTypes.checkBoxField :
                                Ember.set(field, 'isCheckBox', true);
                                break;
                        }

                        var dataTyp = field.dataType;

                        switch (dataTyp) {
                            case that.dataTypes.string :
                                Ember.set(field, 'isString', true);
                                break;

                            case that.dataTypes.int:
                                Ember.set(field, 'isInt', true);
                                break;

                            case that.dataTypes.email:
                                Ember.set(field, 'isEmail', true);
                                break;
                        }
                    });
                });
            }
        });

        if (kycConfiguration.length > 1) {
            this.set('isTabsAvailable', true);
        } else {
            this.set('isTabsAvailable', false);
        }

        this.displayTabs();
        this._tabSelected(kycConfiguration[0]);
    },

    loadContent: function (tabId) {
        var kycWidgetConfig = this.get('kycWidgetConfig');
        var cusObj = this.get('kycArr');
        var businessObj = cusObj.get('0');
        var that = this;

        var tabData = kycWidgetConfig[tabId].dataCollTab;

        Ember.$.each(tabData, function (grpId) {
            var dataCol = tabData[grpId].dataCollGroup;

            Ember.$.each(dataCol, function (fieldId, field) {
                var dataRow = field;
                var proprtyName = sharedService.userSettings.currentLanguage === 'AR' && dataRow.arProp ? dataRow.arProp : dataRow.prop;

                Ember.set(dataRow, 'isMandatory', false);

                if (dataCol[fieldId].fieldType === that.fieldTypes.dateField && that.get('isKYC')) {
                    var dateInMixFormat = businessObj[proprtyName];
                    var dateObject = utils.formatters.convertStringToDate(dateInMixFormat);

                    Ember.set(dataRow, 'value', dateObject);
                } else {
                    if (that.get('isKYC')) {
                        Ember.set(dataRow, 'value', businessObj[dataRow.prop]);

                        if (dataRow.arProp) {
                            Ember.set(dataRow, 'valueAR', businessObj[dataRow.arProp]);
                        }
                    }
                }

                // Dependent fields
                if (dataRow.depends) {
                    var updated = that.get('groupCollection');

                    Ember.$.each(updated, function (groupId) {
                        var groupColUpdated = updated[groupId].dataCollGroup;

                        Ember.$.each(groupColUpdated, function (dependentFieldId, fieldDependent) {
                            if (fieldDependent) {
                                var proName = sharedService.userSettings.currentLanguage === 'AR' && fieldDependent.arProp ? fieldDependent.arProp : fieldDependent.prop;

                                if (dataRow.depends.field === proName) {
                                    if (dataRow.depends.enabled) {
                                        var valuesArray = dataRow.depends.enabled;

                                        if (valuesArray.indexOf(fieldDependent.value) !== -1) {
                                            Ember.set(dataRow, 'isInvisible', false);
                                        } else {
                                            Ember.set(dataRow, 'isInvisible', true);
                                        }

                                        Ember.addObserver(fieldDependent, 'value', that, function () {
                                            var enablingArray = dataRow.depends.enabled;

                                            if (enablingArray.indexOf(fieldDependent.value) !== -1) {
                                                Ember.set(dataRow, 'isInvisible', false);
                                            } else {
                                                Ember.set(dataRow, 'isInvisible', true);
                                            }
                                        });
                                    } else if (dataRow.fieldType === that.fieldTypes.dateField) {
                                        Ember.addObserver(fieldDependent, 'value', that, function () {
                                            if (fieldDependent.value) {
                                                if (fieldDependent.isHijri) {
                                                    var gregorianDateObject = islamicDateConverter.getGregorianDate(fieldDependent.value);

                                                    if (!isNaN(gregorianDateObject)) {
                                                        var gregorianDate = utils.formatters.convertDateToString(gregorianDateObject);
                                                        var savedDate = utils.formatters.convertDateToString(field.value);

                                                        if (gregorianDate !== savedDate) {
                                                            Ember.set(field, 'value', gregorianDateObject);
                                                        }
                                                    } else {
                                                        Ember.set(field, 'value', '');
                                                    }
                                                } else {
                                                    var gDateString = utils.formatters.convertDateToString(fieldDependent.value);
                                                    var gDate = utils.formatters.formatToDate(gDateString, undefined, 'DD/MM/YYYY');

                                                    if (gDate !== this.invalidDate) {
                                                        var islamicDate = islamicDateConverter.getIslamicDate(gDate);

                                                        if (field.value !== islamicDate && gDate !== this.invalidDate) {
                                                            Ember.set(field, 'value', islamicDate);
                                                        }
                                                    } else {
                                                        Ember.set(field, 'value', '');
                                                    }
                                                }
                                            } else {
                                                Ember.set(field, 'value', '');
                                            }
                                        });
                                    } else if (field.prop === 'ibanNo') {
                                        Ember.addObserver(fieldDependent, 'value', that, function () {
                                            var bankAccNo = fieldDependent.value;
                                            var countryCode = that.tradeService.kycMasterDS.getCountryMap(businessObj ? businessObj.nationality : that.get('nationalityId'));
                                            var countryCodeSa = kycConfig.CountryCodeSA;
                                            var bankCode = kycConfig.BankCode;
                                            var ibandtxt = '';
                                            var bankAccNoTxt = bankAccNo ? bankAccNo.toString() : '';

                                            for (var i = 0; i < (kycConfig.BankAccNoLength - bankAccNoTxt.length); i++) {
                                                ibandtxt = [ibandtxt, '0'].join('');
                                            }

                                            var accountNumber = [ibandtxt, bankAccNoTxt].join('');

                                            ibandtxt = [ibandtxt, bankAccNoTxt].join('');
                                            ibandtxt = [bankCode, ibandtxt].join('');
                                            ibandtxt = [ibandtxt, countryCodeSa].join('');

                                            var result = that._calculateModulus(ibandtxt, kycConfig.IBANDevideNo);
                                            var ibanCheckSum = (kycConfig.IBANCheckSumNo - result);
                                            var ibanNo = [countryCode, ibanCheckSum, bankCode, accountNumber].join(''); // SA3565000000000000001234

                                            Ember.set(dataRow, 'value', ibanNo);
                                        });
                                    } else {
                                        Ember.addObserver(fieldDependent, 'value', that, function () {
                                            if (fieldDependent.value !== -1 && fieldDependent.value !== undefined) {
                                                var defaultOption = [{
                                                    des: that.get('app').lang.labels.pleaseSelect,
                                                    code: -1
                                                }];

                                                that.set('defaultOption', defaultOption);

                                                var dataColl = that.tradeService.kycMasterDS.getDependentMasterDataTypes(dataRow.typId, fieldDependent.value, sharedService.userSettings.currentLanguage);
                                                Ember.set(dataRow, 'collection', dataColl);

                                                Ember.addObserver(dataRow, 'collection.length', that, function () {
                                                    if (dataRow.collection.length > 0) {
                                                        Ember.set(dataRow, 'value', dataRow.collection[0] ? dataRow.collection[0].code : '');
                                                    }
                                                });
                                            }
                                        });
                                    }
                                }
                            }
                        });
                    });
                }

                if (dataRow.fieldType === that.fieldTypes.dropDownField) {
                    if (dataRow.value === undefined || dataRow.value === -1) {
                        if (!that.get('isKYC') && dataRow.typId === kycConfig.KYCMasterDataTypes.Country) {
                            Ember.set(dataRow, 'value', userSettings.customisation.defaultCountryID);
                        } else {
                            if (dataRow.defaultValue !== undefined) {
                                Ember.set(dataRow, 'value', dataRow.defaultValue);
                            } else {
                                var plsSelectOption = {des: that.get('app').lang.labels.pleaseSelect, code: -1};
                                that.set('defaultOption', plsSelectOption);
                            }
                        }
                    }

                    if (dataRow.depends) {
                        if (dataRow.value === undefined || dataRow.value === '0' || dataRow.value === '-1') {
                            if (dataRow.typId === kycConfig.KYCMasterDataTypes.Country && dataRow.depends.defaultVal === 0) {
                                Ember.set(dataRow, 'value', userSettings.customisation.defaultanotherNationalityID);
                            }
                        }
                    }

                    var dataCollection = Ember.A();

                    if (dataRow.typId === kycConfig.KYCMasterDataTypes.City && businessObj && dataRow.depends) {
                        dataCollection = that.tradeService.kycMasterDS.getDependentMasterDataTypes(dataRow.typId, businessObj[dataRow.depends.field], sharedService.userSettings.currentLanguage);
                    } else {
                        dataCollection = that.tradeService.kycMasterDS.getMasterDataTypes(dataRow.typId, sharedService.userSettings.currentLanguage);
                    }

                    Ember.set(dataRow, 'collection', dataCollection);
                }

                if (dataRow.fieldType === that.fieldTypes.checkBoxField) {
                    var optionArray = [];
                    var itemList = [];

                    var values = dataRow.value;
                    var valueArray = values ? values.split(',') : [];

                    optionArray.pushObjects(that.tradeService.kycMasterDS.getMasterDataTypes(dataRow.typId, sharedService.userSettings.currentLanguage));

                    Ember.$.each(optionArray, function (code, option) {
                        Ember.set(option, 'isSelected', false);
                    });

                    Ember.$.each(valueArray, function (index, value) {
                        var selectedOption = optionArray[parseInt(value, 10)];

                        Ember.set(selectedOption, 'isSelected', true);
                        itemList.push(selectedOption.displayName);
                    });

                    if (itemList.length) {
                        Ember.set(dataRow, 'itemList', itemList.join(','));
                    } else {
                        Ember.set(field, 'itemList', that.get('app').lang.labels.pleaseSelect);
                    }

                    Ember.set(dataRow, 'collection', optionArray);
                }
            });
        });
    },

    onLanguageChanged: function () {
        this.set('lan', sharedService.userSettings.currentLanguage.toLowerCase());

        if (this.get('isKYC')) {
            this.set('title', this.get('app').lang.labels.kycUpdate);
        }

        this.set('errorCollection', []);
        this.set('messageCollection', []);

        this.displayTabs();
        this._setCaptions();
        this.onAddSubscription();
    },

    loadMasterContent: function () {
        var kycWidgetConfig = this.get('kycWidgetConfig');
        var that = this;
        var tabId = this.get('currentID');

        var tabData = kycWidgetConfig[tabId].dataCollTab;

        Ember.$.each(tabData, function (grpId) {
            var dataCol = tabData[grpId].dataCollGroup;

            Ember.$.each(dataCol, function (fieldId, field) {
                var dataRow = field;

                if (dataRow.fieldType === that.fieldTypes.dropDownField) {
                    if (dataRow.value === undefined || dataRow.value === -1 && !dataRow.depends) {
                        if (dataRow.defaultValue !== undefined) {
                            Ember.set(dataRow, 'value', dataRow.defaultValue);
                        } else {
                            var plsSelectOption = {des: that.get('app').lang.labels.pleaseSelect, code: -1};
                            that.set('defaultOption', plsSelectOption);
                        }
                    }

                    var dataCollection = that.tradeService.kycMasterDS.getMasterDataTypes(dataRow.typId, sharedService.userSettings.currentLanguage);
                    Ember.set(dataRow, 'collection', dataCollection);
                }
            });
        });
    },

    createCollection: function (fieldCollection) {
        var totNumberOfFields = fieldCollection.length;
        var splittingPoint = totNumberOfFields / 2;
        var rightCollection, leftCollection;

        if (totNumberOfFields % 2 === 0) {
            rightCollection = fieldCollection.slice(0, splittingPoint);
            leftCollection = fieldCollection.slice(splittingPoint, totNumberOfFields);
        } else {
            rightCollection = fieldCollection.slice(0, splittingPoint + 1);
            leftCollection = fieldCollection.slice(splittingPoint + 1, totNumberOfFields);
        }

        return [leftCollection, rightCollection];
    },

    _setKYCUpdateStatus: function () {
        var kycUpdateObj = this.get('kycUpdate');

        if (kycUpdateObj) {
            var sts = kycUpdateObj.sts;
            var msg;

            if (sts !== this.kycUpdateStatus.Success) {
                msg = kycUpdateObj.rsn;
            }

            if (sts) {
                var app = this.get('app');
                var messageCollection = [];
                messageCollection[messageCollection.length] = sts === this.kycUpdateStatus.Success ? (this.get('isKYC') ? app.lang.messages.kycUpdateSuccess : '') : (msg ? msg : app.lang.labels.reqFailed);
                var messageCss = sts === this.kycUpdateStatus.Success ? 'up-fore-color' : 'down-fore-color';

                if (sts === this.kycUpdateStatus.Success && !this.get('isKYC') && this.get('isLastTab')) {
                    this.set('isUpdateSuccess', true);

                    messageCollection[messageCollection.length] = app.lang.messages.dearCustomer;
                    messageCollection[messageCollection.length] = app.lang.messages.bankSelectThankMsg;
                    messageCollection[messageCollection.length] = [app.lang.messages.accountReferenceNo, kycUpdateObj.refNo].join(' ');
                    messageCollection[messageCollection.length] = app.lang.messages.moreInfoBankDetails;
                }

                if (sts === this.kycUpdateStatus.Success && this.get('isKYC')) {
                    var changedSet = this.get('changedSet');
                    var cusObj = this.get('kycArr');
                    var businessObj = cusObj.get('0');

                    this.set('isUpdated', true);

                    Ember.$.each(changedSet, function (key, value) {
                        if (key !== 'unqReqId') {
                            Ember.set(businessObj, key, value);
                        }
                    });

                    if (this.get('isLastTab') && kycUpdateObj.nextDate) {
                        this.set('isUpdateSuccess', true);

                        var nextDate = this.utils.formatters.formatToDateTime(kycUpdateObj.nextDate);

                        messageCollection[messageCollection.length] = [app.lang.messages.kycUpdateNextDate, nextDate].join(' ');
                        messageCollection[messageCollection.length] = app.lang.messages.kycUpdateContactInfo;
                    }
                }

                this._showMessage(messageCollection, messageCss);
            }

            this.removeObserver('kycUpdate.sts', this, this._setKYCUpdateStatus);
        }
    },

    _loadDataContent: function () {
        var kycDetailsArr = this.get('kycArr');

        if (kycDetailsArr.length !== 0) {
            this.loadContent(this.get('currentID'));
            this.set('isKYCDataUpdated', true);
        }
    },

    _setCaptions: function () {
        var grpColl = this.get('groupCollection');
        var lanStore = this.get('app').lang.labels;
        var rgtColl, leftColl, field, grp;

        this.set('langStore', lanStore);

        Ember.$.each(grpColl, function (grpId) {
            leftColl = grpColl[grpId].leftCollection;
            grp = grpColl[grpId];

            Ember.set(grp, 'grpDes', lanStore[grp.langKey]);

            Ember.$.each(leftColl, function (fieldId) {
                field = leftColl[fieldId];
                Ember.set(field, 'displayDes', lanStore[field.langKey]);

                if (field.placeHolder) {
                    Ember.set(field, 'placeHolderTxt', field.isPHLang ? lanStore[field.placeHolder] : field.placeHolder);

                    if (field.arProp) {
                        Ember.set(field, 'placeHolderTxtAR', field.isPHLang ? lanStore[field.placeHolderAR] : field.placeHolderAR);
                    }
                }
            });
        });

        Ember.$.each(grpColl, function (grpId) {
            rgtColl = grpColl[grpId].rightCollection;
            grp = grpColl[grpId];

            Ember.set(grp, 'grpDes', lanStore[grp.langKey]);

            Ember.$.each(rgtColl, function (fieldId) {
                field = rgtColl[fieldId];
                Ember.set(field, 'displayDes', lanStore[field.langKey]);

                if (field.placeHolder) {
                    Ember.set(field, 'placeHolderTxt', field.isPHLang ? lanStore[field.placeHolder] : field.placeHolder);

                    if (field.arProp) {
                        Ember.set(field, 'placeHolderTxtAR', field.isPHLang ? lanStore[field.placeHolderAR] : field.placeHolderAR);
                    }
                }
            });
        });
    },

    _showMessage: function (messageCollection, messageCss) {
        this.set('isMsgEnable', true);
        this.set('messageCollection', messageCollection);
        this.set('msgCss', messageCss ? messageCss : 'down-fore-color');
    },

    _tabSelected: function (tabItem) {
        var kycWidgetConfig = this.get('kycWidgetConfig');
        var tabId = tabItem.ID;
        var tabData = kycWidgetConfig[tabId].dataCollTab;
        var that = this;
        var grpCon = [];

        Ember.$.each(tabData, function (grpId) {
            var dataCollction = tabData[grpId].dataCollGroup;
            var leftRgtArr = that.createCollection(dataCollction);
            var dataCol = tabData[grpId];

            try {
                kycWidgetConfig[tabId].dataCollTab[grpId].leftCollection = leftRgtArr[0];
                kycWidgetConfig[tabId].dataCollTab[grpId].rightCollection = leftRgtArr[1];
            } catch (e) {
                Ember.set(dataCol, 'leftCollection', leftRgtArr[0]);
                Ember.set(dataCol, 'rightCollection', leftRgtArr[1]);
            }

            grpCon[grpId] = kycWidgetConfig[tabId].dataCollTab[grpId];
        });

        this.set('groupCollection', grpCon);
        this.set('cusInfoTabsActive', tabItem);
        this.set('currentID', tabItem.ID);

        if (!tabItem.ID) {
            this.set('isFirstTab', true);
        } else if (tabItem.ID === (this.get('kycTabsArr').length - 1)) {
            this.set('isLastTab', true);
        } else {
            this.set('isFirstTab', false);
            this.set('isLastTab', false);
        }

        this._setCaptions();

        if (this.get('isKYCDataUpdated') || !this.get('isKYC')) {
            this.loadContent(tabItem.ID);
        }
    },

    _loadStaticTabs: function (outlet) {
        var controllerString = 'controller:trade/widgets/kyc/' + outlet;
        var routeString = 'trade/widgets/kyc/' + outlet;
        var route = this.container.lookup('route:application');
        var widgetController = ControllerFactory.createController(this.container, controllerString);

        if (this.get('isKYC')) {
            var cusObj = this.get('kycArr');
            var businessObj = cusObj.get('0');

            widgetController.set('businessObj', businessObj);
        }

        this.set('currentController', widgetController);

        route.render(routeString, {
            into: 'trade/widgets/kyc/kyc-update',
            outlet: 'staticOutlet',
            controller: widgetController
        });

        widgetController.initializeWidget({wn: controllerString});
    },

    _disconnectOutlet: function () {
        var route = this.container.lookup('route:application');

        route.disconnectOutlet({
            parentView: 'trade/widgets/kyc/kyc-update',
            outlet: 'staticOutlet'
        });

        this.set('staticOutlet', undefined);
    },

    _onSave: function (propertyObj) {
        var changedObj;

        if (this.get('isKYC')) {
            changedObj = this._getChangedSet(propertyObj);

            this.set('changedSet', changedObj);
        } else {
            var currentChangedSet = this.get('changedSet');

            if (currentChangedSet !== undefined) {
                this.set('changedSet', Ember.$.extend(currentChangedSet, propertyObj));
            } else {
                this.set('changedSet', propertyObj);
            }

            if (!this.get('isLastTab')) {
                return true;
            }
        }

        if (!Ember.$.isEmptyObject(changedObj) && this.get('isKYC') || !Ember.$.isEmptyObject(propertyObj) && !this.get('isKYC') || this.get('isLastTab')) {
            var unqReqId = Math.floor(Date.now()).toString();
            var kycStatusUpdateObj = this.get('kycUpdate');

            if (this.get('isKYC')) {
                Ember.set(changedObj, 'mubNo', this.tradeService.userDS.get('mubNo'));
                Ember.set(changedObj, 'updteTyp', this.kycUpdateType.KYC);
                Ember.set(changedObj, 'unqReqId', unqReqId);
            } else {
                var changedSet = this.get('changedSet');

                Ember.set(changedSet, 'updteTyp', this.kycUpdateType.OAO);
                Ember.set(changedSet, 'unqReqId', unqReqId);
            }

            this.set('kycUpdate.sts', 0);
            this.set('kycUpdate.unqReqId', unqReqId);
            this.set('kycUpdate.rsn', '');

            if (this.get('isKYC')) {
                this.tradeService.sendKYCUpdateRequest(unqReqId, kycStatusUpdateObj, changedObj);
            } else {
                this.tradeService.sendKYCUpdateRequest(unqReqId, kycStatusUpdateObj, this.get('changedSet'));
            }

            this.addObserver('kycUpdate.sts', this, this._setKYCUpdateStatus);
        } else {
            var app = this.get('app');
            var messageCollection = [];
            messageCollection[messageCollection.length] = app.lang.messages.noChangesAvailable;
            var messageCss = 'down-fore-color';

            this._showMessage(messageCollection, messageCss);
        }
    },

    _getChangedSet: function (propertyObj) {
        var businessObj = this.get('kycArr').get('0');
        var changedObj = {};

        if (businessObj) {
            Ember.$.each(propertyObj, function (key) {
                if ((propertyObj[key] !== undefined) && (propertyObj[key] !== -1)) {
                    if (businessObj[key] !== propertyObj[key]) {
                        changedObj[key] = propertyObj[key];
                    }
                }
            });
        }

        return changedObj;
    },

    _onNavigate: function (mode) {
        var currentID = this.get('currentID');
        var tabCollection = this.get('kycWidgetConfig');

        this._tabSelected(tabCollection[currentID + mode]);
        this.set('isUpdated', false);

        var that = this;

        Ember.run.later(function () {
            that._loadIslamicCalender();
        }, 1000);
    },

    _kycTabSelected: function (tabItem) {
        this.set('isDynamic', true);
        this.set('messageCollection', []);
        this.set('errorCollection', []);

        this._disconnectOutlet();

        if (tabItem.isDynamic) {
            this._tabSelected(tabItem);
        } else {
            this.set('isDynamic', false);
            this._loadStaticTabs(tabItem.outlet);
        }
    },

    _onCheckMandatoryFieldsValidity: function (field) {
        if (field.depends && field.isInvisible) {
            return false;
        } else {
            var fieldValue = sharedService.userSettings.currentLanguage === 'AR' && field.arProp ? field.valueAR : field.value;

            return !field.isDisabled && field.isRequired && (fieldValue === -1 || fieldValue === undefined || !utils.validators.isAvailable(fieldValue));
        }
    },

    _closePopup: function () {
        var modal = sharedService.getService('sharedUI').getService('modalPopupId');

        if (modal) {
            modal.send('closeModalPopup');
        }
    },

    _onValidateDynamic: function () {
        var updated = this.get('groupCollection');
        var propertyObj = {};
        var fieldCollection = [];
        var that = this;
        var errorCollection = [];
        var app = this.get('app');
        var mandatoryFieldCount = 0;

        Ember.$.each(updated, function (grpId) {
            var groupColUpdated = updated[grpId].dataCollGroup;

            Ember.$.each(groupColUpdated, function (fieldId, field) {
                var propertyName = field.prop;

                if (that._onCheckMandatoryFieldsValidity(field)) {
                    Ember.set(field, 'isMandatory', true);
                    mandatoryFieldCount++;
                } else if (field.isEmail && utils.validators.isAvailable(field.value)) {
                    Ember.set(field, 'isMandatory', false);

                    if (!utils.validators.isEmail(field.value)) {
                        errorCollection.pushObject(app.lang.messages.invalidEmail);
                    } else {
                        propertyObj[propertyName] = field.value;
                    }
                } else {
                    var updatedValue = field.value;

                    Ember.set(field, 'isMandatory', false);

                    if (field.fieldType === that.fieldTypes.dateField) {
                        if (field.prop === 'dob') {
                            var today = new Date();
                            var dob = field.value;
                            var age = today - dob;

                            if (age < kycConfig.AccOpenAgeLimit * 365 * 24 * 60 * 60 * 1000) { // Compare in milli-seconds
                                errorCollection.pushObject(app.lang.messages.accOpenAgeLimit);
                            }
                        }

                        if (!field.isHijri) {
                            updatedValue = field.value ? utils.formatters.convertDateToString(field.value) : '';
                        }
                    }

                    if (field.fieldType === that.fieldTypes.checkBoxField) {
                        var valuesArray = [];
                        var optionArray = field.collection;

                        Ember.$.each(optionArray, function (index, option) {
                            if (option.isSelected) {
                                valuesArray.push(index);
                            }
                        });

                        if (valuesArray.length > 0) {
                            updatedValue = valuesArray.join(',');
                        }
                    }

                    if (field.prop === 'nationality') {
                        that.set('nationalityId', field.value);
                    }

                    if (field.arProp) {
                        propertyObj[field.arProp] = field.valueAR;
                    }

                    if (!field.isHijri) {
                        propertyObj[propertyName] = updatedValue;
                    }

                    fieldCollection[fieldCollection.length] = field;
                }
            });
        });

        var groupId = this.get('currentID');

        if (groupId === kycConfig.GeneralInfoId) {
            errorCollection = this._validateGeneralInfoFields(fieldCollection, propertyObj, errorCollection);
        } else if (groupId === kycConfig.ContactInfoId) {
            errorCollection = this._validateContactInfoFields(fieldCollection, propertyObj, errorCollection);
        } else if (groupId === kycConfig.EmployInfoId) {
            errorCollection = this._validateEmploymentInfoFields(fieldCollection, propertyObj, errorCollection);
        }

        if (mandatoryFieldCount > 0) {
            errorCollection.unshiftObject(app.lang.messages.mandatoryFields);
        }

        if (errorCollection.length === 0) {
            return propertyObj;
        } else {
            this.set('errorCollection', errorCollection);
            return null;
        }
    },

    _validateGeneralInfoFields: function (fieldCollection, propertyObj, errorCollection) {
        var isErrorAdded = false;
        var app = this.get('app');
        var that = this;

        Ember.$.each(fieldCollection, function (fieldId, field) {
            if (!field.isDisabled && field.prop === 'identityNumber') {
                var depPropVal = propertyObj ? propertyObj[field.depends.field] : undefined;
                var fieldVal = field.value.toString();

                if (!that.get('isKYC')) {
                    that.set('isNinIqamaSuccess', false);

                    that.tradeService.validateNinIqama({
                        cusId: fieldVal
                    }, function (status, rejReason) {
                        that._onNinIqamaResponse(status, rejReason);
                    });
                }

                if (depPropVal === kycConfig.NationalId) { // National ID
                    if (propertyObj.nationality !== userSettings.customisation.defaultCountryID) {
                        errorCollection.pushObject(app.lang.messages.civilIdNationality);
                        Ember.set(field, 'isMandatory', true);
                    }

                    if (fieldVal.length !== kycConfig.IdNumberLength) {
                        errorCollection.pushObject(app.lang.messages.idNoDigit);
                        Ember.set(field, 'isMandatory', true);
                    }

                    if (fieldVal.substring(0, 1) !== kycConfig.NationalIdStart) {
                        errorCollection.pushObject(app.lang.messages.civilIdStart);
                        Ember.set(field, 'isMandatory', true);
                    }
                }

                if (depPropVal === kycConfig.IqamaId) { // Iqama
                    if (propertyObj.nationality === userSettings.customisation.defaultCountryID) {
                        errorCollection.pushObject(app.lang.messages.civilIdNationality);
                        Ember.set(field, 'isMandatory', true);
                    }

                    if (fieldVal.length !== kycConfig.IdNumberLength) {
                        errorCollection.pushObject(app.lang.messages.idNoDigit);
                        Ember.set(field, 'isMandatory', true);
                    }

                    if (fieldVal.substring(0, 1) !== kycConfig.IqamaIdStart) {
                        errorCollection.pushObject(app.lang.messages.iqamaStart);
                        Ember.set(field, 'isMandatory', true);
                    }

                    var issueDate = propertyObj.issueDate;
                    var expiryDate = propertyObj.idExpDate;

                    if (issueDate && expiryDate) {
                        var issueDateObj = utils.formatters.convertStringToDate(issueDate);
                        var expiryDateObj = utils.formatters.convertStringToDate(expiryDate);
                        var validPeriod = expiryDateObj - issueDateObj;

                        if (validPeriod > 365 * 24 * 60 * 60 * 1000) { // Compare in milli-seconds
                            errorCollection.pushObject(app.lang.messages.iqamaExpiry);
                            Ember.set(field, 'isMandatory', true);
                        }
                    }
                }
            }

            var enArValidCondition = sharedService.userSettings.currentLanguage !== 'AR' &&
                (field.prop === 'firstName' || field.prop === 'secondName' || field.prop === 'thirdName' || field.prop === 'familyName');

            if (!field.isDisabled && enArValidCondition && field.value && field.value.length < 2) {
                if (!isErrorAdded) {
                    errorCollection.pushObject(app.lang.messages.nameValidation);
                    isErrorAdded = true;
                }

                Ember.set(field, 'isMandatory', true);
            }
        });

        return errorCollection;
    },

    _validateContactInfoFields: function (fieldCollection, propertyObj, errorCollection) {
        var app = this.get('app');

        Ember.$.each(fieldCollection, function (fieldId, field) {
            if (!field.isDisabled && (field.prop === 'homeTel' || field.prop === 'mobileNo')) {
                var dependentPropVal = propertyObj ? propertyObj[field.depends.field] : undefined;
                var stringVal = field.value.toString();

                if (dependentPropVal === userSettings.customisation.defaultCountryID) {
                    if (field.prop === 'homeTel') {
                        if (stringVal.substring(0, kycConfig.HomeTelCode.length) !== kycConfig.HomeTelCode) {
                            errorCollection.pushObject(app.lang.messages.phoneNoStart);
                            Ember.set(field, 'isMandatory', true);
                        } else if (stringVal.length !== kycConfig.MobileLength) {
                            errorCollection.pushObject(app.lang.messages.phoneNoValid);
                            Ember.set(field, 'isMandatory', true);
                        }
                    }

                    if (field.prop === 'mobileNo') {
                        if (stringVal.substring(0, kycConfig.MobileTelCode.length) !== kycConfig.MobileTelCode) {
                            errorCollection.pushObject(app.lang.messages.mobileNoStart);
                            Ember.set(field, 'isMandatory', true);
                        } else if (stringVal.length !== kycConfig.MobileLength) {
                            errorCollection.pushObject(app.lang.messages.mobileNoValid);
                            Ember.set(field, 'isMandatory', true);
                        }
                    }
                } else if (stringVal.length !== kycConfig.MobileLength) {
                    if (field.prop === 'homeTel') {
                        errorCollection.pushObject(app.lang.messages.phoneNoValid);
                    }

                    if (field.prop === 'mobileNo') {
                        errorCollection.pushObject(app.lang.messages.mobileNoValid);
                    }

                    Ember.set(field, 'isMandatory', true);
                }
            }
        });

        return errorCollection;
    },

    _validateEmploymentInfoFields: function (fieldCollection, propertyObj, errorCollection) {
        var app = this.get('app');

        Ember.$.each(fieldCollection, function (fieldId, field) {
            if (!field.isDisabled && field.prop === 'phoneNoEmp') {
                var dependentPropVal = propertyObj ? propertyObj[field.depends.field] : undefined;
                var stringVal = field.value.toString();

                if (dependentPropVal === userSettings.customisation.defaultCountryID) {
                    if (field.prop === 'homeTel' || field.prop === 'phoneNoEmp') {
                        if (stringVal.substring(0, kycConfig.HomeTelCode.length) !== kycConfig.HomeTelCode) {
                            errorCollection.pushObject(app.lang.messages.phoneNoStart);
                            Ember.set(field, 'isMandatory', true);
                        } else if (stringVal.length !== kycConfig.MobileLength) {
                            errorCollection.pushObject(app.lang.messages.phoneNoValid);
                            Ember.set(field, 'isMandatory', true);
                        }
                    }
                } else if (stringVal.length < kycConfig.MobileLength) {
                    errorCollection.pushObject(app.lang.messages.phoneNoValid);
                    Ember.set(field, 'isMandatory', true);
                }
            }
        });

        return errorCollection;
    },

    _onNinIqamaResponse: function (status, rejReason) {
        if (status === this.ninIqamaStatus.Fail) {
            var rejectedReason = rejReason ? rejReason : this.app.lang.messages.alreadyRegistered;
            this.get('errorCollection').pushObject(rejectedReason);

            this.set('isNinIqamaSuccess', false);
        } else if (status === this.ninIqamaStatus.Success) {
            if (this.get('errorCollection').length === 0) {
                this.set('isNinIqamaSuccess', true);

                if (this.get('ninIqamaSuccessCount') === 0) {
                    this._onNavigate(1);
                    this.set('ninIqamaSuccessCount', 1);
                }
            }
        }
    },

    _onClearData: function () {
        this.set('errorCollection', []);
        this.set('messageCollection', []);
    },

    _calculateModulus: function (divident, divisor) {
        var cDivident = '';
        var cRest = '';

        if (divident) {
            for (var i = 0; i < divident.length; i++) {
                var cChar = divident[i];
                var cOperator = cRest + '' + cDivident + '' + cChar;

                if (cOperator < parseInt(divisor, 10)) {
                    cDivident += '' + cChar;
                } else {
                    cRest = cOperator % divisor;

                    if (cRest === 0) {
                        cRest = '';
                    }

                    cDivident = '';
                }
            }
        }

        cRest += '' + cDivident;

        if (cRest === '') {
            cRest = 0;
        }

        return cRest;
    },

    showMessagePopup: function () {
        var that = this;

        Ember.run.next(function () {
            var confirmMsg = that.app.lang.messages.noModifedDetails;

            that.utils.messageService.showMessage(confirmMsg,
                that.utils.Constants.MessageTypes.Question,
                false,
                that.app.lang.labels.confirm,
                [{type: that.utils.Constants.MessageBoxButtons.Yes, btnAction: that._onNavigate.bind(that, 1)},
                    {type: that.utils.Constants.MessageBoxButtons.No}]
            );
        });
    },

    onStaticTabValidated: function (errorCol) {
        if (errorCol.length > 0) {
            this.set('errorCollection', errorCol);
        } else {
            var propertyObj = this.get('currentController').getDataObject();
            this._onSave(propertyObj);

            if (this.get('isNextTab')) {
                this._onNavigate(1);
            }
        }
    },

    actions: {
        onSave: function () {
            var currentID = this.get('currentID');
            var tabCollection = this.get('kycWidgetConfig');
            var tabItem = tabCollection[currentID];

            this.set('isNextTab', false);
            this._onClearData();

            if (tabItem.isDynamic) {
                var validKYCCollection = this._onValidateDynamic();

                if (validKYCCollection) {
                    this._onSave(validKYCCollection);
                }
            } else {
                var that = this;

                this.get('currentController').onValidate(function (errorCol) {
                    that.onStaticTabValidated(errorCol);
                });
            }
        },

        kycTabSelected: function (tabItem) {
            this._kycTabSelected(tabItem);
        },

        onNext: function () {
            this.set('isNextTab', true);
            this._onClearData();

            var currentID = this.get('currentID');
            var tabCollection = this.get('kycWidgetConfig');
            var tabItem = tabCollection[currentID];

            if (tabItem.isDynamic) {
                var validKYCCollection = this._onValidateDynamic();

                if (validKYCCollection) {
                    var changedObj = this.get('isKYC') ? this._getChangedSet(validKYCCollection) : validKYCCollection;

                    if (Ember.$.isEmptyObject(changedObj)) {
                        if (!this.get('isUpdated') && this.get('isKYC')) {
                            this.showMessagePopup();
                        } else if (this.get('isKYC') || this.get('isNinIqamaSuccess')) {
                            this._onNavigate(1);
                        }
                    } else {
                        if (this.get('isKYC')) {
                            var messageCollection = [this.get('app').lang.messages.changesAvailable];

                            this._showMessage(messageCollection, 'down-fore-color');
                        } else {
                            this._onSave(changedObj);

                            if (this.get('isKYC') || this.get('isNinIqamaSuccess')) {
                                this._onNavigate(1);
                            }
                        }
                    }
                }
            } else {
                var that = this;

                this.get('currentController').onValidate(function (errorCol) {
                    that.onStaticTabValidated(errorCol);
                });
            }
        },

        onBack: function () {
            this.set('isUpdateSuccess', false);
            this.set('ninIqamaSuccessCount', 0);
            this._onNavigate(-1);
        },

        cancelOptions: function (dataRow) {
            var cusObj = this.get('kycArr');
            var kycDetails = cusObj.get('0');
            var propertyName = sharedService.userSettings.currentLanguage === 'AR' && dataRow.arProp ? dataRow.arProp : dataRow.prop;
            var values = kycDetails[propertyName];
            var valueArray = values ? values.split(',') : [];
            var optionArray = dataRow.collection;
            var itemList = [];

            Ember.$.each(optionArray, function (index, field) {
                Ember.set(field, 'isSelected', false);
            });

            if (optionArray.length > 0) {
                Ember.$.each(valueArray, function (index, value) {
                    var selectedOption = optionArray[parseInt(value, 10)];

                    Ember.set(selectedOption, 'isSelected', true);
                    itemList.push(selectedOption.displayName);
                });
            }

            if (itemList.length) {
                Ember.set(dataRow, 'itemList', itemList.join(','));
            } else {
                Ember.set(dataRow, 'itemList', this.get('app').lang.labels.pleaseSelect);
            }

            this._closePopup();
        },

        saveOptions: function (field) {
            var displayNamesArray = [];
            var codesArray = [];

            var optionArray = field.collection;

            Ember.$.each(optionArray, function (index, option) {
                if (option.isSelected) {
                    displayNamesArray.push(option.displayName);
                    codesArray.push(index);
                }
            });

            if (displayNamesArray.length) {
                Ember.set(field, 'itemList', displayNamesArray.join(','));
            } else {
                Ember.set(field, 'itemList', this.get('app').lang.labels.pleaseSelect);
            }

            Ember.set(field, 'value', codesArray.join(','));
        },

        onBackToLogin: function () {
            this.utils.applicationSessionHandler.logout();
        }
    }
});
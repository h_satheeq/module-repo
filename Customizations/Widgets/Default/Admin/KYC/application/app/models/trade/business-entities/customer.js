import Ember from 'ember';

export default Ember.Object.extend({
    cusId: '',
    sharesHighRisk: undefined,
    sharesMedRisk: undefined,
    debtInstrmntsHighRisk: undefined,
    debtInstrmntsMedRisk: undefined,
    debtInstrmntsLowRisk: undefined,
    invstmntFundHighRisk: undefined,
    invstmntFundMedRisk: undefined,
    invstmntFundLowRisk: undefined,
    tradeFinanceLowRisk: undefined,
    commdtsHighRisk: undefined,
    optionsHighRisk: undefined,
    sharesLowRisk: undefined,
    tradeFinanceHighRisk: undefined,
    tradeFinanceMedRisk: undefined,
    commdtsMedRisk: undefined,
    commdtsLowRisk: undefined,
    optionsMedRisk: undefined,
    optionsLowRisk: undefined,

    preferredLanguage: undefined,
    callingMethod: undefined,
    isTermConditionsAccepted: undefined,
    shares: undefined,
    investmentFunds: undefined,
    debtInstrument: undefined,
    commodities: undefined,
    foreignExchanges: undefined,
    derivateAndOptions: undefined,
    deposits: undefined,
    realEstate: undefined,
    tradeFinance: undefined,
    investKnowledge: undefined,
    investProfile: undefined,
    generalInvestObj: undefined,
    clientPreferedInvest: undefined,
    foreignCurr: undefined,
    clientClass: undefined,
    gender: undefined,
    title: undefined,
    family: undefined,
    firstName: undefined,
    secondName: undefined,
    thirdName: undefined,
    passportNo: undefined,
    nationality: undefined,
    dob: undefined,
    cob: undefined,
    pob: undefined,
    maritalStatus: undefined,
    profession: undefined,
    nod: undefined,
    identityType: undefined,
    identityNumber: undefined,
    issuePlace: undefined,
    pobox: undefined,
    zip: undefined,
    buildingNo: undefined,
    fax: undefined,
    country: undefined,
    city: undefined,
    district: undefined,
    streetName: undefined,
    homeTel: undefined,
    mobileNo: undefined,
    officeTel: undefined,
    email: undefined,
    annualIncome: undefined,
    netWorth: undefined,
    directorOfficer: undefined,
    sourceWealth: undefined,
    attrContactName: undefined,
    attrAddress1: undefined,
    attrAddress2: undefined,
    attrBusinessPhone: undefined,
    attrBusinessFax: undefined,
    attrMobileNo: undefined,
    attrCustodianName: undefined,
    attrAccNo: undefined,
    attrID: undefined,
    empZip: undefined,
    empPOBox: undefined,
    empCountry: undefined,
    empCity: undefined,
    empPhone: undefined,
    empEmail: undefined,
    bnkName: undefined,
    bnkMainAcc: undefined,
    bnkBranch: undefined,
    countryTest: undefined, // TODO: [Chamalee] Remove hardcoded arrays used for dependent dropdown functional testing after OMS Integration
    cityTest: undefined,

    setData: function (kycMessage) {
        var that = this;

        Ember.$.each(kycMessage, function (key, value) {
            that.set(key, value);
        });
    }
});
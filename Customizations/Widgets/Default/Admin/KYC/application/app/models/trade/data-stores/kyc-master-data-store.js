import Ember from 'ember';
import kycConfig from '../../../config/kyc-config';
import sharedService from '../../../models/shared/shared-service';
import languageDataStore from '../../../models/shared/language/language-data-store';
import userSettings from '../../../config/user-settings';

export default Ember.Object.extend({
    store: Ember.A(),
    isRefreshed: false,
    countryMap: {},

    initialize: function () {
        var storeCol = this.get('store');
        var app = languageDataStore.getLanguageObj();
        var labels = app.lang.labels;
        var lang = sharedService.userSettings.currentLanguage;

        var masterDataTypes = kycConfig.KYCMasterDataTypes;
        var masterData = kycConfig.kycMasterData;

        storeCol[lang] = {};

        if (labels) {
            Ember.$.each(masterData, function (groupKey, field) {
                Ember.$.each(field, function (optionKey, value) {
                    if (value.langKey) {
                        Ember.set(value, 'des', labels[value.langKey]);
                    }

                    if (groupKey === 'IncomeSources') {
                        Ember.set(value, 'displayName', labels[value.langKey]);
                    }
                });
            });
        }

        storeCol[lang][masterDataTypes.Gender] = Ember.A(masterData.Gender);
        storeCol[lang][masterDataTypes.Income] = Ember.A(masterData.Income);
        storeCol[lang][masterDataTypes.Networth] = Ember.A(masterData.Networth);
        storeCol[lang][masterDataTypes.YesNo] = Ember.A(masterData.YesNo);
        storeCol[lang][masterDataTypes.Residence] = Ember.A(masterData.Residence);
        storeCol[lang][masterDataTypes.EmployeeStatus] = Ember.A(masterData.EmployeeStatus);
        storeCol[lang][masterDataTypes.EducationLevel] = Ember.A(masterData.EducationLevel);
        storeCol[lang][masterDataTypes.IncomeSources] = Ember.A(masterData.IncomeSources);
        storeCol[lang][masterDataTypes.AccountType] = Ember.A(masterData.AccountType);
        storeCol[lang][masterDataTypes.IncomeSources] = Ember.A(masterData.IncomeSources);
        storeCol[lang][masterDataTypes.CapitalAmount] = Ember.A(masterData.CapitalAmount);
        storeCol[lang][masterDataTypes.InvestmentPeriod] = Ember.A(masterData.InvestmentPeriod);
        storeCol[lang][masterDataTypes.InvestmentObjectives] = Ember.A(masterData.InvestmentObjectives);
        storeCol[lang][masterDataTypes.InvestmentExperience] = Ember.A(masterData.InvestmentExperience);
        storeCol[lang][masterDataTypes.RiskTolerance] = Ember.A(masterData.RiskTolerance);
        storeCol[lang][masterDataTypes.CountryTest] = Ember.A(masterData.CountryTest);
        storeCol[lang][masterDataTypes.CityTest] = Ember.A(masterData.CityTest);
    },

    setTypes: function (configType, configValues, lang) {
        var masterDataStore = this.get('store');
        var masterDataTypes = kycConfig.KYCMasterDataTypes;

        if (masterDataStore[lang] === undefined) {
            masterDataStore[lang] = {};
        }

        if (configType === masterDataTypes.Country) {
            var sortedCountryListByCat = configValues.sortBy('natnltyCat');
            var sortedCountryListByDes, countryListByCat;

            for (var countryIdx = 0; countryIdx < sortedCountryListByCat.length; countryIdx++) {
                if (sortedCountryListByCat[countryIdx].natnltyCat !== kycConfig.CountryType.KSA && sortedCountryListByCat[countryIdx].natnltyCat !== kycConfig.CountryType.Arabic) {
                    countryListByCat = sortedCountryListByCat.slice(0, countryIdx);
                    sortedCountryListByDes = sortedCountryListByCat.slice(countryIdx, sortedCountryListByCat.length).sortBy('des');

                    break;
                }

            }

            var that = this;

            Ember.$.each(sortedCountryListByCat, function (id, country) {
                that.get('countryMap')[country.code] = country.countrycd;
            });

            var sortedCountryList = countryListByCat.concat(sortedCountryListByDes);
            masterDataStore[lang][configType] = Ember.A(sortedCountryList);
        } else if (configType === masterDataTypes.City) {
            if (configValues.length > 0) {
                var sortedCityList = configValues.sortBy('des');
                masterDataStore[lang][configType] = Ember.A(sortedCityList);
            } else {
                masterDataStore[lang][configType] = Ember.A();
            }
        } else if (configType === masterDataTypes.IdentityType) {
            if (configValues.length > 0) {
                var idTypeList = [];

                Ember.$.each(configValues, function (key, field) {
                    if (field.showIdTyp === 1) {
                        idTypeList[idTypeList.length] = field;
                    }
                });

                var sortedIdTypeList = idTypeList.sortBy('des');
                masterDataStore[lang][configType] = Ember.A(sortedIdTypeList);
            } else {
                masterDataStore[lang][configType] = Ember.A();
            }
        } else {
            var sortedList = configValues.sortBy('des');
            masterDataStore[lang][configType] = Ember.A(sortedList);
        }

        this.toggleProperty('isRefreshed');
    },

    getMasterDataTypes: function (typId, lang) {
        var storeCol = this.get('store');

        if (!storeCol[lang]) {
            storeCol[lang] = {};
        }

        if (!storeCol[lang][typId]) {
            storeCol[lang][typId] = Ember.A();
        }

        return storeCol[lang][typId];
    },

    getCountryMap: function (code) {
        return this.get('countryMap')[code];
    },

    getDependentMasterDataTypes: function (typId, dependentKey, lang) {
        var storeCol = {};
        storeCol[lang] = {};
        storeCol[lang][typId] = Ember.A();

        if (!dependentKey) {
            this._sendDefaultCityListRequest();
        } else {
            this.tradeService.sendKYCMasterDataRequest({
                lan: sharedService.userSettings.currentLanguage,
                countryId: dependentKey,
                typCol: typId
            });
        }

        return storeCol[lang][typId];
    },

    sendMasterDataRequest: function () {
        var masterDataTypes = kycConfig.KYCMasterDataTypes;
        var masterDataTypeIds = [masterDataTypes.Title, masterDataTypes.Country, masterDataTypes.Bank, masterDataTypes.CurrencyList, masterDataTypes.MaritalStatus, masterDataTypes.Profession, masterDataTypes.IdentityType];

        this.tradeService.sendKYCMasterDataRequest({
            lan: sharedService.userSettings.currentLanguage,
            typCol: masterDataTypeIds.join(',')
        });

        this.initialize();
        this._sendDefaultCityListRequest();
    },

    _sendDefaultCityListRequest: function () {
        this.tradeService.sendKYCMasterDataRequest({
            lan: sharedService.userSettings.currentLanguage,
            typCol: kycConfig.KYCMasterDataTypes.City,
            countryId: userSettings.customisation.defaultCountryID
        });
    }
});

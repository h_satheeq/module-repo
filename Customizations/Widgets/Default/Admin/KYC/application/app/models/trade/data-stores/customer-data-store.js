import Ember from 'ember';
import Customer from '../business-entities/customer';

export default Ember.Object.extend({
    store: {},
    kycDetailsCollection: [],
    kycMapById: {},
    isRefreshed: false,

    getKYCDetails: function (cusId) {
        var currentStore = this.get('store');
        var customer = currentStore[cusId];

        if (!customer && cusId) {
            customer = Customer.create({
                cusId: cusId
            });

            currentStore[cusId] = customer;
            this.addToOtherCollections(cusId, customer);
        }

        this.toggleProperty('isRefreshed');

        return customer;
    },

    addToOtherCollections: function (cusId, customer) {
        var kycDetailsCollection = this.get('kycDetailsCollection');
        kycDetailsCollection.pushObject(customer);

        var kycMapById = this.get('kycMapById');
        if (kycMapById[cusId]) {
            kycMapById[cusId].pushObject(customer);
        } else {
            kycMapById[cusId] = Ember.A([customer]);
        }
    },

    getKYCColl: function () {
        return this.get('kycDetailsCollection');
    },

    clearCollection: function () {
        this.set('store', {});
        this.set('kycDetailsCollection', Ember.A());
    },

    getKYCCollectionByCusId: function (cusId) {
        var kycMapById = this.get('kycMapById');

        if (!kycMapById[cusId]) {
            kycMapById[cusId] = Ember.A([]);
        }

        return kycMapById[cusId];
    }
});

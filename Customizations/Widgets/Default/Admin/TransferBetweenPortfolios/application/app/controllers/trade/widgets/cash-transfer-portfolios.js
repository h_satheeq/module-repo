import Ember from 'ember';
import BaseController from '../../base-controller';
import sharedService from '../../../models/shared/shared-service';
import utils from '../../../utils/utils';
import appConfig from '../../../config/app-config';

export default BaseController.extend({
    tradeService: sharedService.getService('trade'),

    message: '',
    msgCss: '',

    fromPortfolio: {},
    toPortfolio: {},
    defaultAccount: {},

    transfer: Ember.Object.create({}),
    accountColl: Ember.A(),
    currConvert: Ember.Object.create({}),

    fromRequired: '*',
    isFromAmountDisabled: false,
    isToAmountDisabled: true,
    isEnableFromAmount: true,
    isEnableToAmount: false,
    isSubmitDisabled: true,
    timer: undefined,
    isCalDisabled: true,
    isFromStaticLableEnabled: true,
    isToStaticLableEnabled: true,

    isMobile: appConfig.customisation.isMobile,

    currencyConvertStatus: {
        Success: 1,
        Fail: 0
    },

    cashTransferStatus: {
        Success: 1,
        Fail: -1
    },

    fromNetBalance: function () {
        var fromAmt = this.get('fromAmount') ? this.get('fromAmount') : 0;
        return this.get('fromPortfolio.cashAccount.cashForWith') - fromAmt;
    }.property('fromAmount', 'fromPortfolio'),

    toNetBalance: function () {
        var toAmt = this.get('toAmount') ? this.get('toAmount') : 0;
        return this.get('toPortfolio.cashAccount.cashForWith') - toAmt;
    }.property('toAmount', 'toPortfolio'),

    resetToAmount: function () {
        if (this.get('isEnableFromAmount') && this.get('toAmount') > 0) {
            this.set('toAmount', 0);
        }
    }.observes('fromAmount'),

    resetFromAmount: function () {
        if (this.get('isEnableToAmount') && this.get('fromAmount') > 0) {
            this.set('fromAmount', 0);
        }
    }.observes('toAmount'),

    // TODO: [Champaka] Need to refactor this
    disableCal: function () {
        var fromPortfolio = this.get('fromPortfolio');
        var toPortfolio = this.get('toPortfolio');

        if(fromPortfolio.curr === toPortfolio.curr) {
            this.set('isCalDisabled', true);
            this.set('isSubmitDisabled', false);
            this.set('isToCheckboxDisabled', true);
            this.set('isFromCheckboxDisabled', true);
            this.set('isEnableFromAmount', true);
        } else {
            this.set('isCalDisabled', false);
            this.set('isSubmitDisabled', true);
            this.set('isToCheckboxDisabled', false);
            this.set('isFromCheckboxDisabled', false);
            this.set('isToAmountDisabled', true);

            if (fromPortfolio.curr === undefined || toPortfolio.curr === undefined) {
                this.set('isCalDisabled', true);
            } else {
                this.set('isCalDisabled', false);
            }
        }
    }.observes('fromPortfolio', 'toPortfolio'),

    // TODO: [Champaka] Need to refactor this
    setFromAmount: function () {
        var isEnableFromAmount = this.get('isEnableFromAmount') ? 1 : 0;

        if (isEnableFromAmount === 1) {
            this.set('fromRequired', '*');
            this.set('toRequired', '');
            this.set('toAmount', 0);
            this.set('isFromAmountDisabled', false);
            this.set('isToAmountDisabled', true);
            this.set('isEnableToAmount', false);
        } else {
            this.set('isFromAmountDisabled', true);
            this.set('isEnableToAmount', true);
        }
    }.observes('isEnableFromAmount'),

    setToAmount: function () {
        var isEnableToAmount = this.get('isEnableToAmount') ? 1 : 0;

        if (isEnableToAmount === 1) {
            this.set('toRequired', '*');
            this.set('fromRequired', '');
            this.set('fromAmount', 0);
            this.set('isToAmountDisabled', false);
            this.set('isFromAmountDisabled', true);
            this.set('isEnableFromAmount', false);
        } else {
            this.set('isToAmountDisabled', true);
            this.set('isEnableFromAmount', true);
        }
    }.observes('isEnableToAmount'),

    setCashTransferStatus: function () {
        var sts = this.get('transfer.sts');
        var msg = '';

        if (sts === this.cashTransferStatus.Success) {
            this.set('fromAmount', 0);
            this.set('toAmount', 0);
            this.set('fxConvertRate', '');
        } else if (sts === this.cashTransferStatus.Fail) {
            msg = this.get('transfer.rsn');
            this.set('fxConvertRate', '');
        }

        this._showMessage(sts, msg);
    }.observes('transfer.sts', 'transfer.rsn'),

    setFxStatus: function () {
        var sts = this.get('currConvert.sts');

        if (sts === this.currencyConvertStatus.Success) {
            this.set('msgCss', 'highlight-fore-color');
            this.set('message', this.app.lang.messages.currConvertWaiting);

            var timer = Ember.run.later(this, this.onFxFail, 60000);
            this.set('timer', timer);
        } else if (sts === this.currencyConvertStatus.Fail) {
            this.onFxFail();
        }
    }.observes('currConvert.sts'),

    setFxRate: function () {
        var transType = this.get('currConvert.transType');
        var amnt = this.get('currConvert.amnt');
        var fxRate = this.get('currConvert.fxRate');

        if(amnt !== undefined && fxRate !== undefined) {
            if (transType === 1) {
                this.set('toAmount', amnt);
            } else if (transType === 2) {
                this.set('fromAmount', amnt);
            }

            this.set('msgCss', 'fore-color bold');
            this.set('fxConvertRate', this.app.lang.labels.fxRate + ' ' + fxRate);
            this.set('isCalDisabled', false);
            this.set('isSubmitDisabled', false);
            this.set('message', '');

            Ember.run.cancel(this.get('timer'));
        } else {
            Ember.run.cancel(this.get('timer'));
            this.onFxFail();
        }
    }.observes('currConvert.amnt', 'currConvert.fxRate'),

    onFxFail: function () {
        this.set('msgCss', 'down-fore-color');
        this.set('message', this.app.lang.messages.currConvertFailed);
        this.set('isCalDisabled', false);
    },

    onLoadWidget: function () {
        this.tradeService.subscribeTradeMetaReady(this, this.get('wkey'));
        this.tradeService.subscribeTradeDisconnect(this, this.get('wkey'));
    },

    onPrepareData: function () {
        this.loadContent();
    },

    onTradeMetaReady: function () {
        this.loadContent();
    },

    onTradeDisconnect: function () {
        this.onClearData();
    },

    onClearData: function () {
        this.set('portfolioCollection', []);
        this.set('fromAmount', 0);
        this.set('toAmount', 0);

        this._resetFields();
    },

    onLanguageChanged: function () {
        this.set('message', '');
        this.set('msgCss', '');
    },

    onUnloadWidget: function () {
        this.tradeService.unSubscribeTradeMetaReady(this, this.get('wkey'));
        this.tradeService.unSubscribeTradeDisconnect(this, this.get('wkey'));
    },

    loadContent: function () {
        var accountColl = this.tradeService.accountDS.getTradingAccCollection();
        var defaultAccount;

        if (accountColl.length > 1) {
            var defaultDescription = this.get('app').lang.labels.pleaseSelect;
            this.set('defaultAccount', Ember.Object.create({cashAccount: {invAccNo: defaultDescription}, dispProp: defaultDescription}));
        }

        if (accountColl && accountColl.length > 0) {
            Ember.$.each(accountColl, function (key, port) {
                sharedService.getService('trade').sendBuyingPowerRequest(port);
            });

            defaultAccount = defaultAccount ? defaultAccount : accountColl[0];
        }

        this.set('defaultAccount', defaultAccount);
        this.set('accountColl', accountColl);
        this.set('message', '');
    },

    setAccountCollection: function () {
        var accountCollection = this.tradeService.accountDS.getTradingAccCollection();

        Ember.$.each(accountCollection, function (key, port) {
            sharedService.getService('trade').sendBuyingPowerRequest(port);
        });
    },

    _showMessage: function (sts, msg) {
        var app = this.get('app');
        var message = sts === this.cashTransferStatus.Success ? app.lang.messages.fundTransferSuccess : (sts === this.cashTransferStatus.Fail ? msg : '');
        var messageCss = sts === this.cashTransferStatus.Success ? 'up-fore-color pad-m-b' : (sts === this.cashTransferStatus.Fail ? 'down-fore-color pad-m-b' : '');

        this.set('message', message);
        this.set('msgCss', messageCss);

        if (sts === this.cashTransferStatus.Success) {
            this.setAccountCollection();
            this.set('isFromStaticLableEnabled', true);
            this.set('isToStaticLableEnabled', true);
            this.set('fromPortfolio', '');
            this.set('toPortfolio', '');
        }
    },

    _checkWithdrawalRequestValidity: function () {
        return (utils.validators.isAvailable(this.get('fromAmount')) && this.get('fromAmount') > 0 && this.get('fromPortfolio.tradingAccId')) ||
            (utils.validators.isAvailable(this.get('toAmount')) && this.get('toAmount') > 0 && this.get('toPortfolio.tradingAccId'));
    },

    showMessagePopup: function () {
        var that = this;
        var transfer = this.get('transfer');
        var fromPortfolio = this.get('fromPortfolio');
        var toPortfolio = this.get('toPortfolio');

        Ember.run.next(function () {
            var confirmMsg = that.app.lang.messages.confirmPortTransferRequest;
            confirmMsg = confirmMsg.replace('[Amount]', utils.formatters.formatNumber(transfer.transferAmount, 2));
            confirmMsg = confirmMsg.replace('[Currency]', fromPortfolio.get('curr'));
            confirmMsg = confirmMsg.replace('[FromPortName]', fromPortfolio.get('tradingAccName'));
            confirmMsg = confirmMsg.replace('[FromInvAccNo]', fromPortfolio.get('cashAccount.invAccNo'));
            confirmMsg = confirmMsg.replace('[ToPortName]', toPortfolio.get('tradingAccName'));
            confirmMsg = confirmMsg.replace('[ToInvAccNo]', toPortfolio.get('cashAccount.invAccNo'));

            that.utils.messageService.showMessage(confirmMsg,
                that.utils.Constants.MessageTypes.Question,
                false,
                that.app.lang.labels.confirm,
                [{type: that.utils.Constants.MessageBoxButtons.Yes, btnAction: that.submitRequest.bind(that)},
                    {type: that.utils.Constants.MessageBoxButtons.No}]
            );
        });
    },

    calculateRequest: function (amnt, transType) {
        var app = this.get('app');
        var unqReqId = Math.floor(Date.now()).toString();
        var currConvert = this.get('currConvert');
        var fromPortfolio = this.get('fromPortfolio');
        var toPortfolio = this.get('toPortfolio');

        if (fromPortfolio && toPortfolio && fromPortfolio.get('secAccNum') !== toPortfolio.get('secAccNum')) {
            this.set('message', app.lang.messages.pleaseWait);
            this.set('msgCss', 'highlight-fore-color');
            this.set('isCalDisabled', true);

            this.tradeService.sendCurrenyRateRequest(unqReqId, currConvert, {
                frmAcc: fromPortfolio.get('cshAccNum'),
                toAcc: toPortfolio.get('cshAccNum'),
                fromCurr: fromPortfolio.get('curr'),
                toCurr: toPortfolio.get('curr'),
                transType: transType,
                amnt: amnt,
                sMubNo: this.tradeService.userDS.get('mubNo'),
                unqReqId: unqReqId,
                usrId: this.tradeService.userDS.usrId
            });
        }
    },

    submitRequest: function () {
        var unqReqId = Math.floor(Date.now()).toString();
        var transfer = this.get('transfer');
        var currConvert = this.get('currConvert');
        var fromPortfolio = this.get('fromPortfolio');
        var toPortfolio = this.get('toPortfolio');

        if (fromPortfolio && toPortfolio && fromPortfolio.get('tradingAccId') !== toPortfolio.get('tradingAccId')) {

            if (this.get('transfer.transType') === 2) {
                this.set('currConvert.amnt', this.get('toAmount'));
            }

            this.tradeService.sendCashTransferRequest(unqReqId, transfer, {
                frmAcc: fromPortfolio.get('cashAccountId'), // Test this with other protocol
                toAcc: toPortfolio.get('cashAccountId'),
                sMubNo: this.tradeService.userDS.get('mubNo'),
                unqReqId: unqReqId,
                amnt: transfer.transferAmount,
                transType: transfer.transType,
                convertAmt: currConvert.amnt,
                fxRate: currConvert.fxRate
            });
        }
    },

    onAfterRender: function () {
        this.generateScrollBar(undefined, 4000);
    },

    _resetFields: function () {
        this.set('message', '');
        this.set('fxConvertRate', '');
        this.set('msgCss', '');
    },

    actions: {
        setFromPortfolio: function (option) {
            this.set('fromPortfolio', option);
            this.set('isFromStaticLableEnabled', false);
            this.set('fromAmount', 0);

            this._resetFields();
        },

        setToPortfolio: function (option) {
            this.set('toPortfolio', option);
            this.set('isToStaticLableEnabled', false);
            this.set('toAmount', 0);

            this._resetFields();
        },

        onCalculate: function () {
            var app = this.get('app');
            var from = this.get('isEnableFromAmount') ? 1 : 0;
            var to = this.get('isEnableToAmount') ? 2 : 0;
            var fromAmount = this.get('fromAmount');
            var toAmount = this.get('toAmount');

            this.set('isSubmitDisabled', true);
            this.set('fxConvertRate', '');
            this.set('currConvert.transType', from || to);
            this.set('currConvert.sts', '');

            if (this._checkWithdrawalRequestValidity()) {
                if (from === 1) {
                    if (utils.validators.isAvailable(fromAmount)) {
                        this.calculateRequest(fromAmount, from);
                    }
                } else if (to === 2) {
                    if (utils.validators.isAvailable(toAmount)) {
                        this.calculateRequest(toAmount, to);
                    }
                }
            } else {
                this.set('msgCss', 'down-fore-color');

                if (fromAmount === 0.00 || toAmount === 0.00) {
                    this.set('message', app.lang.messages.amountGTZero);
                } else {
                    this.set('message', app.lang.messages.mandatoryFields);
                }
            }
        },

        onSubmit: function () {
            var unqReqId = Math.floor(Date.now()).toString();
            var app = this.get('app');
            var from = this.get('isEnableFromAmount') ? 1 : 0;
            var to = this.get('isEnableToAmount') ? 2 : 0;

            this.set('transfer.transType', from || to);
            this.set('transfer.transferAmount', this.get('fromAmount'));
            this.set('transfer.sts', 0);
            this.set('transfer.unqReqId', unqReqId);
            this.set('transfer.rsn', '');
            this.set('message', '');
            this.set('msgCss', '');

            if (this.get('fromPortfolio.secAccNum') !== undefined && this.get('fromPortfolio.secAccNum') === this.get('toPortfolio.secAccNum')) {
                this.set('message', app.lang.messages.cannotTransferSamePortfolio);
                this.set('msgCss', 'down-fore-color');
            } else if (this._checkWithdrawalRequestValidity()) {
                this.showMessagePopup();
            } else {
                this.set('msgCss', 'down-fore-color');

                if (this.get('fromAmount') === 0.00 || this.get('toAmount') === 0.00) {
                    this.set('message', app.lang.messages.amountGTZero);
                } else {
                    this.set('message', app.lang.messages.mandatoryFields);
                }
            }
        }
    }
});

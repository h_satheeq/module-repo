import Ember from 'ember';

export default Ember.Object.extend({
    repId: '',
    pdfName: '',
    link: '',

    setData: function (consolidatedReportMsg) {
        var that = this;

        Ember.$.each(consolidatedReportMsg, function (key, value) {
            that.set(key, value);
        });
    }
});
import Ember from 'ember';
import consolidatedReport from '../business-entities/consolidated-report';

export default Ember.Object.extend({
    store: {},
    consolidatedReportsCollection: Ember.A([]),
    conRepMapById: {},

    getConReport: function (repId) {
        var currentStore = this.get('store');
        var report = currentStore[repId];

        if (!report && repId) {
            report = consolidatedReport.create({
                repId: repId
            });

            currentStore[repId] = report;
            this.addToOtherCollections(repId, report);
        }

        return report;
    },

    addToOtherCollections: function (repId, conRep) {
        var productDetailsCollection = this.get('consolidatedReportsCollection');
        productDetailsCollection.pushObject(conRep);

        var conRepMapById = this.get('conRepMapById');
        if (conRepMapById[repId]) {
            conRepMapById[repId].pushObject(conRep);
        } else {
            conRepMapById[repId] = Ember.A([conRep]);
        }
    },

    getConReportColl: function () {
        return this.get('consolidatedReportsCollection');
    },

    clearCollection: function () {
        this.set('store', {});
        this.set('consolidatedReportsCollection', Ember.A());
    },

    getConReportByrepId: function (repId) {
        var currentStore = this.get('store');
        return currentStore[repId];
    }
});

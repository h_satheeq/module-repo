import BaseModuleInitializer from '../../../../models/shared/initializers/base-module-initializer';
import sharedService from '../../../../models/shared/shared-service';
import ConsolidatedReportDataStore from '../../../../models/trade/data-stores/consolidated-report-data-store';

export default BaseModuleInitializer.extend({
    postInitialize: function () {
        var tradeService = sharedService.getService('trade');
        tradeService.set('consolidatedReportDS', ConsolidatedReportDataStore.create({tradeService: tradeService}));
    }
});

import sharedService from '../../../../models/shared/shared-service';
import BaseController from '../../../base-controller';

export default BaseController.extend({
    statementType: 1,

    tradeService: sharedService.getService('trade'),

    onLoadWidget: function () {
        this.tradeService.subscribeTradeMetaReady(this, this.get('wkey'));
        this.setDropDownOptions();
    },

    onPrepareData: function () {
        this.loadContent();
    },

    onTradeMetaReady: function () {
        this.loadContent();
    },

    setDropDownOptions: function () {
        var app = this.get('app');
        var periodArray = [{label: app.lang.labels.chartMonthly, code: '1'}, {label: app.lang.labels.chartQuarterly, code: '2'}];

        this.set('statementTypes', [{label: app.lang.labels.consolidatedReports, code: '1'}]);
        this.set('periods', periodArray);
        this.set('currentPeriod', periodArray.length > 0 ? periodArray[0] : {});
    },

    onLanguageChanged: function () {
        this.setDropDownOptions();
    },

    loadContent: function () {
        var date = new Date();
        var year = date.getFullYear();
        var years = [], months = [], portfolios = [];
        var month;

        var portfolioCollection = this.tradeService.accountDS.getTradingAccCollection();
        var currentPortfolio = this.get('currentPortfolio');
        portfolios.pushObjects(portfolioCollection);

        if (!currentPortfolio || !currentPortfolio.secAccNum) {
            this.set('currentPortfolio', portfolios.length > 0 ? portfolios[0] : {});
        }

        for (var i = 1; i < 13; i++) {
            month = (i < 10 ? '0' : '') + i;
            months.pushObject({label: month});
        }

        for (var j = year, k = 0; k < 10; j--, k++) {
            years.pushObject({label: j});
        }

        this.set('currentMonth', months.length > 0 ? months[0] : {});
        this.set('currentYear', years.length > 0 ? years[0] : {});
        this.set('months', months);
        this.set('portfolios', portfolios);
        this.set('years', years);
    },

    actions: {
        onSubmit: function () {
            var reqObj = {
                secAccNum: this.get('currentPortfolio').secAccNum,
                cusId: this.tradeService.userDS.mubNo,
                period: this.get('currentPeriod').code,
                statementTyp: this.statementType,
                fromDate: this.get('currentMonth').label + '/' + this.get('currentYear').label
            };

            this.tradeService.sendConsolidatedReportRequest(reqObj);
            var reportArr = this.tradeService.consolidatedReportDS.getConReportColl();
            this.set('reportCollection', reportArr);
        },

        setMonth: function (option) {
            this.set('currentMonth', option);
        },

        setYear: function (option) {
            this.set('currentYear', option);
        },

        setPortfolio: function (option) {
            this.set('currentPortfolio', option);
        },

        setPeriod: function (option) {
            this.set('currentPeriod', option);
        }
    }
});
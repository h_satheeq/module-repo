import Ember from 'ember';
import BaseController from '../../base-controller';
import appConfig from '../../../config/app-config';
import utils from '../../../utils/utils';
import sharedService from '../../../models/shared/shared-service';
import ControllerFactory from '../../../controllers/controller-factory';

export default BaseController.extend({
    actions: {
        showAccountCreationPage: function () {
            var modal = sharedService.getService('sharedUI').getService('modalPopupId');

            modal.set('modalPopupStyle', '');
            modal.send('disableOverlay');
            modal.send('closeModalPopup');

            var controllerString = 'controller:trade/widgets/kyc/kyc-update';
            var kycWidgetController = ControllerFactory.createController(this.container, controllerString);
            var app = this.get('app');

            kycWidgetController.initializeWidget({wn: controllerString.split('/').pop()});

            kycWidgetController.set('widgetContainerKey', 'onlineAccOpenContainer-' + 'OAO');
            kycWidgetController.set('wkey', 'OAO');
            kycWidgetController.set('title', app.lang.labels.onlineAccOpening);

            var route = this.container.lookup('route:application');

            route.render('trade/widgets/kyc/kyc-update', {
                into: 'application',
                outlet: 'fullScreenWidgetOutlet',
                controller: kycWidgetController
            });
        },

        showBankHomePage: function () {
            window.open(utils.requestHelper.generateQueryString(appConfig.onlineAccountOpenURLs.newCustomer, {
                Lang: sharedService.userSettings.get('currentLanguage')
            }), '_blank');
        },

        onBackToLogin: function () {
            if (Ember.$.isFunction(this.get('loginViewCallbackFn'))) {
                this.get('loginViewCallbackFn')();
            }
        }
    }
});

import BaseArrayController from '../../base-array-controller';
import sharedService from '../../../models/shared/shared-service';
import appConfig from '../../../config/app-config';

export default BaseArrayController.extend({
    contentImgUrl: '',
    loginImgUrl: '',
    moreInfoImgUrl: '',

    onLoadWidget: function () {
        this._setImageUrl();
    },

    onLanguageChanged: function () {
        this._setImageUrl();
    },

    _setImageUrl: function () {
        if (sharedService.userSettings.currentLanguage === 'AR') {
            this.set('contentImgUrl', 'assets/images/tadawulity/ar/tdwl_content_ar.jpg');
            this.set('loginImgUrl', 'assets/images/tadawulity/ar/tdwl_login_btn_ar.jpg');
            this.set('moreInfoImgUrl', 'assets/images/tadawulity/ar/tdwl_minfo_btn_ar.jpg');
        } else {
            this.set('contentImgUrl', 'assets/images/tadawulity/en/tdwl_content_en.jpg');
            this.set('loginImgUrl', 'assets/images/tadawulity/en/tdwl_login_btn_en.jpg');
            this.set('moreInfoImgUrl', 'assets/images/tadawulity/en/tdwl_minfo_btn_en.jpg');
        }
    },

    actions: {
        tadawulLogin: function () {
            window.open(appConfig.tadawulityURLs.tadawulityLogin, 'TadawulLoginWindow');
        },

        tadawulMoreInfo: function () {
            window.open(appConfig.tadawulityURLs.tadawulityInfo, 'TadawulMoreInfoWindow');
        }
    }
});

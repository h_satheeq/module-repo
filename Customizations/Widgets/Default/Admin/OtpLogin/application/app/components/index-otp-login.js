import Ember from 'ember';
import languageDataStore from '../models/shared/language/language-data-store';
import utils from '../utils/utils';
import sharedService from '../models/shared/shared-service';
import tradeConstants from '../models/trade/trade-constants';
import appConfig from '../config/app-config';

export default Ember.Component.extend({
    layoutName: 'index-otp-login',
    app: languageDataStore.getLanguageObj(),
    tradeService: sharedService.getService('trade'),

    otpSts: 0,
    mobileNo: '',
    mobileNoShowLength: 4,

    isOTPSubmitDisabled: false,

    didInsertElement: function () {
        var mobileNum = this.tradeService.userDS.mobile;
        mobileNum = mobileNum && mobileNum.length > this.mobileNoShowLength ? 'xxxxxxx' + mobileNum.substr(mobileNum.length - (this.mobileNoShowLength - 1)) : 'xxxxxxxxxx';

        this.set('mobileNo', mobileNum);

        if (!this.get('isRegistered')) {
            sharedService.getService('sharedUI').registerService('indexOtpLoginPopup', this);
        }
    }.on('init'),

    validateOtpPin: function (mubNo, otpPin) {
        var reqObj = {mubNo: mubNo, usrNme: this.tradeService.userDS.lgnAls, pin: otpPin};
        this.tradeService.validateLoginOtp(reqObj);
    },

    onOtpValidationResponse: function () {
        var otpSts = this.get('otpSts');

        if (otpSts === tradeConstants.AuthStatus.Success || otpSts === tradeConstants.AuthStatus.FirstTimeLogin) {
            var modal = sharedService.getService('sharedUI').getService('modalPopupId');

            modal.set('modalPopupStyle', '');
            modal.send('disableOverlay');
            modal.send('closeModalPopup');

            this.set('message', '');
            this.set('isOTPSubmitDisabled', true);

            Ember.$('div#otpMsg').hide();
        } else {
            this.set('isOTPSubmitDisabled', false);

            this._showMessage();
        }
    },

    // Only for Resend OTP
    _resendOtp: function () {
        this.tradeService.resendOtpPin();
    },

    _showMessage: function () {
        if (!this.get('message')) {
            this.set('message', this.app.lang.messages.authFailed);
        }

        Ember.$('div#otpMsg').show();
    },

    onOtpSubmit: function () {
        var otpPin = this.get('otpPin');
        var isFieldsFilled = utils.validators.isAvailable(otpPin);

        if (isFieldsFilled) {
            this.validateOtpPin(this.tradeService.userDS.mubNo, otpPin);
            this.set('isOTPSubmitDisabled', true);
        } else {
            this.set('message', this.app.lang.messages.mandatoryFields);
            this._showMessage();
        }
    },

    isMobile: function () {
        return appConfig.customisation.isMobile;
    }.property(),

    actions: {
        onSubmit: function () {
            this.onOtpSubmit();
        },

        resendOtp: function () {
            this._resendOtp();
            this.set('message', this.app.lang.messages.requestSuccess);
            this._showMessage();
        },

        onBackToLogin: function () {
            utils.applicationSessionHandler.logout();
        }
    }
});
import Ember from 'ember';
import sharedService from '../../../models/shared/shared-service';
import OrderTicket from './order-ticket/order-ticket';
import tradeConstants from '../../../models/trade/trade-constants';
import utils from '../../../utils/utils';

export default OrderTicket.extend({
    symbolSearchId: '',
    dropDownOrderType: [],

    tradeService: sharedService.getService('trade'),

    onPrepareData: function () {
        this._super();
        this.loadPortfolioOptions();
        this.loadOrderTypeOptions();

        this.set('symbolSearchId', ['symbolSearch', this.get('wkey')].join('-'));
    },

    onTradeMetaReady: function () {
        this._super();

        this.loadPortfolioOptions();
        this.loadOrderTypeOptions();
    },

    onTradeDisconnect: function () {
        this.onClearData();
    },

    onClearData: function () {
        this._super();

        this.set('portfolios', Ember.A());
        this.set('dropDownOrderType', Ember.A());
    },

    onLoadWidget: function () {
        this._super();

        this.tradeService.subscribeTradeMetaReady(this, this.get('wkey'));
        this.tradeService.subscribeTradeDisconnect(this, this.get('wkey'));
    },

    loadOrderTypeOptions: function () {
        var exg = this.tradeService.userDS.get('userExchg')[0];
        var orderTypes = this.tradeService.tradeMetaDS.getOrderTypeCollectionByExchange(exg);
        var types = [];

        this._updateDescription(orderTypes, this.tradeService.tradeMetaDS.metaMapping.orderType);
        types.pushObjects(orderTypes);

        var defaultType = this.get('app').lang.labels[[this.tradeService.tradeMetaDS.metaMapping.orderType, '1'].join('_')];

        if (!utils.validators.isAvailable(defaultType) && orderTypes.length > 0) {
            defaultType = orderTypes[0].des;
        }

        this.set('dropDownOrderType', types);
        this.set('currentType', types[0]);
    },

    updateType: function () {
        var invoker = this.get('invoker');
        var orderType = this.get('currentType');
        orderType = invoker && invoker.orderParams && invoker.orderParams.ordTyp ? this.get('app').lang.labels[[this.tradeService.tradeMetaDS.metaMapping.orderType, invoker.orderParams.ordTyp].join('_')] : orderType;

        this.set('selectedType', orderType);
    }.observes('invoker.orderParams.ordTyp'),

    loadPortfolioOptions: function () {
        var invoker = this.get('invoker');
        var portfolios = [];
        var portfolioCollection = this.tradeService.accountDS.getTradingAccCollection();
        var currentPortfolio = this.get('currentPortfolio');

        portfolios.pushObjects(portfolioCollection);

        if (!currentPortfolio || !currentPortfolio.tradingAccId) {
            this.set('currentPortfolio', portfolios.length > 0 ? portfolios[0] : {});
        }

        this.set('portfolios', portfolios);

        if (portfolios.length > 0) {
            invoker.set('currentPortfolio', portfolioCollection[0]);
            this.tradeService.accountDS.subscribeTradingAccountInfo(invoker.get('currentPortfolio'));
        }
    },

    _updateDescription: function (options, type) {
        var that = this;

        if (options) {
            Ember.$.each(options, function (index, option) {
                Ember.set(option, 'des', that.app.lang.labels[[type, option.code].join('_')]);
            });
        }
    },

    _updateDropdownDescription: function (options) {
        var that = this;

        if (options) {
            Ember.$.each(options, function (index, option) {
                Ember.set(option, 'des', that.app.lang.labels[option.langKey]);
            });
        }
    },

    _validateSearchKey: function () {
        var searchKey = this.get('searchKey');
        var invoker = this.get('invoker');

        if (invoker && invoker.symbolInfo) {
            var symbol = this.get('invoker').symbolInfo;

            if (!utils.validators.isAvailable(searchKey) || symbol.dSym) {
                if (symbol && utils.validators.isAvailable(searchKey)) {
                    this.set('searchKey', symbol.dSym);
                } else {
                    this.sendAction(this.get('onSymbolChanged'));
                }
            } else {
                this.set('searchKey', '');
            }
        }
    },

    onSymbolChanged: function (symbol) {
        var exg, sym, inst;

        if (symbol && symbol.sym) {
            exg = symbol.get('exg');
            sym = symbol.get('sym');
            inst = symbol.get('inst');
        }

        this.prepare(sym, exg, inst);
    },

    actions: {
        setPortfolio: function (option) {
            this.set('currentPortfolio', option);
        },

        setType: function (option) {
            this.set('currentType', option);
        },

        showSearchPopup: function () {
            var modal = this.get(this.get('symbolSearchId'));
            modal.send('showModalPopup');
        },

        closeSearchPopup: function () {
            var modal = this.get(this.get('symbolSearchId'));
            modal.send('closeModalPopup');
        },

        onSymbolSelected: function (symbol) {
            this.set('searchKey', symbol.dSym);
            this.set('enableSearch', false);
            this.onSymbolChanged(symbol);
        },

        validateSearchKey: function () {
            this._validateSearchKey();
        },

        onBuy: function () {
            var that = this;
            var invoker = this.get('invoker');
            var side = tradeConstants.OrderSide.Buy;

            if (invoker && invoker.orderParams) {
                invoker.orderParams.set('ordSide', side);
            }

            this.get('invoker').executeOrder(
                function () {
                    that.showMessagePopup();
                },

                function () {
                    that.showOrderConfirmation();
                });
        },

        onSell: function () {
            var that = this;
            var invoker = this.get('invoker');
            var side = tradeConstants.OrderSide.Sell;

            if (invoker && invoker.orderParams) {
                invoker.orderParams.set('ordSide', side);
            }

            this.get('invoker').executeOrder(
                function () {
                    that.showMessagePopup();
                },

                function () {
                    that.showOrderConfirmation();
                });
        },

        onTypeChanged: function (item) {
            var invoker = this.get('invoker');
            this.set('selectedOrderType', item);
            invoker.get('orderParams').set('ordTyp', item.code);
        },

        onResetOrder: function () {
            this.resetOrderTicket();
        }
    }
});
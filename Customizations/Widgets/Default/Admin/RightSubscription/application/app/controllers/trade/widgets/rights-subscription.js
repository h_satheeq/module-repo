import Ember from 'ember';
import BaseController from '../../base-controller';
import sharedService from '../../../models/shared/shared-service';
import utils from '../../../utils/utils';

export default BaseController.extend({
    rightsOrder: Ember.Object.create({}),
    isMsgEnable: false,
    titleKey: 'rightsSubscription',
    tradeService: sharedService.getService('trade'),
    portfolioDes: sharedService.getService('trade').fieldConfigs.portfolioDes,

    rightSubscriptionStatus: {
        Success: 1,
        Fail: -1
    },

    setRightSubscriptionStatus: function () {
        var sts = this.get('rightsOrder').sts;
        var msg;

        if (sts === this.rightSubscriptionStatus.Fail) {
            msg = this.get('rightsOrder').rsn;
        }

        if (sts) {
            this._showMessage(sts, msg);
            this.set('isMsgEnable', true);
        }
    }.observes('rightsOrder.sts', 'rightsOrder.rsn'),

    onPrepareData: function () {
        this.loadPortfolioOptions();
    },

    onClearData: function () {
        this.set('portfolios', Ember.A());
        this.set('message', '');
        this.set('msgCss', '');
        this.set('isMsgEnable', false);
        this.set('quantity', 0);
    },

    loadPortfolioOptions: function () {
        var portCollection = this.tradeService.accountDS.getTradingAccCollection();
        this.set('portfolios', portCollection);

        if (portCollection.length > 0) {
            this.set('currentPortfolio', portCollection[0]);
        }
    },

    _showMessage: function (sts, msg) {
        var app = this.get('app');
        var message = sts === this.rightSubscriptionStatus.Success ? app.lang.messages.requestSuccess : (sts === this.rightSubscriptionStatus.Fail ? msg : '');
        var messageCss = sts === this.rightSubscriptionStatus.Success ? 'up-fore-color' : (sts === this.rightSubscriptionStatus.Fail ? 'down-fore-color' : '');

        this.set('message', message);
        this.set('msgCss', messageCss);
    },

    _checkSubscriptionRequestValidity: function () {
        return utils.validators.isAvailable(this.get('quantity')) && this.get('quantity') > 0 && this.get('currentPortfolio') && this.get('rowData');
    },

    _sendSubscriptionRequest: function () {
        var unqReqId = Math.floor(Date.now()).toString();
        var order = this.get('rightsOrder');

        this.set('rightsOrder.sts', 0);
        this.set('rightsOrder.unqReqId', unqReqId);
        this.set('rightsOrder.rsn', '');

        if (this._checkSubscriptionRequestValidity()) {
            this.tradeService.sendRightsSubscriptionRequest(unqReqId, order, {
                sym: this.get('rowData').symbol,
                secAccNum: this.get('currentPortfolio.tradingAccId'),
                cusId: this.tradeService.userDS.get('mubNo'),
                exg: this.get('rowData').exg,
                ordQty: this.get('quantity'),
                unqReqId: unqReqId
            });
        } else {
            var app = this.get('app');
            var message = this.get('quantity') === 0 ? app.lang.messages.quantityGTZero : app.lang.messages.mandatoryFields;

            this.set('msgCss', 'down-fore-color');
            this.set('isMsgEnable', true);
            this.set('message', message);
        }
    },

    actions: {
        setPortfolio: function (option) {
            this.set('currentPortfolio', option);
        },

        onSubmit: function () {
            this._sendSubscriptionRequest();
        }
    }
});
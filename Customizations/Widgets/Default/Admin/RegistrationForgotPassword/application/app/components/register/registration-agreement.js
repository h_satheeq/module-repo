import Ember from 'ember';
import languageDataStore from '../../models/shared/language/language-data-store';
import utils from '../../utils/utils';
import sharedService from '../../models/shared/shared-service';

export default Ember.Component.extend({
    layoutName: 'register/registration-agreement',
    app: languageDataStore.getLanguageObj(),
    agreementPath: '',

    initialize: function () {
        this.set('agreementPath', ['agreement_', sharedService.userSettings.currentLanguage.toLowerCase(), '.html'].join(''));
        this.bindEnterKeyLogin();
    }.on('init'),

    bindEnterKeyLogin: function () {
        var that = this;
        Ember.$(window).unbind('keypress');

        Ember.$(window).bind('keypress', function (e) {
            if (e.which === utils.Constants.KeyCodes.Enter) {
                that._onAccept();
            }
        });
    },

    _onAccept: function () {
        var targetController = this.get('targetController');

        if (targetController) {
            targetController.loadNextTab();
        }
    },

    actions: {
        onAccept: function () {
            this._onAccept();
        },

        onReject: function () {
            var registrationController = this.get('targetController');

            if (registrationController) {
                registrationController.loadHomeTab();
            }
        }
    }
});

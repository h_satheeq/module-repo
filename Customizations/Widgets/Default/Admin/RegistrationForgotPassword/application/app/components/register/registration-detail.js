import Ember from 'ember';
import languageDataStore from '../../models/shared/language/language-data-store';
import utils from '../../utils/utils';
import sharedService from '../../models/shared/shared-service';
import tradeConstants from '../../models/trade/trade-constants';
import appConfig from '../../config/app-config';

export default Ember.Component.extend({
    layoutName: 'register/registration-detail',
    app: languageDataStore.getLanguageObj(),

    isCDSubmitDisabled: true,
    cdTimer: undefined,

    registrationObj: Ember.A(),

    initialize: function () {
        this.set('registrationObj', sharedService.getService('trade').registrationDS.getRegistration());
        this.enableCusDetailSubmit();
        this._resetFields();
        this.addObserver('registrationObj.cusDetailSts', this, this._onCusDetailResponse);
        this.bindEnterKeyLogin();
    }.on('init'),

    bindEnterKeyLogin: function () {
        var that = this;
        Ember.$(window).unbind('keypress');

        Ember.$(window).bind('keypress', function (e) {
            if (e.which === utils.Constants.KeyCodes.Enter) {
                that._onSubmit();
            }
        });
    },

    enableCusDetailSubmit: function () {
        var idNumber = this.get('idNumber');
        var cusNumber = this.get('cusNumber');

        if (utils.validators.isAvailable(idNumber) && utils.validators.isAvailable(cusNumber)) {
            this.set('isCDSubmitDisabled', false);
        } else {
            this.set('isCDSubmitDisabled', true);
        }

        this.set('cdTimer', Ember.run.later(this, this.enableCusDetailSubmit, 1000));
    },

    validateCusDetail: function (idNumber, cusNumber) {
        var reqObj = {mubNo: cusNumber, cusId: idNumber};

        sharedService.getService('trade').validateCustomerDetail(reqObj, this.isForgotPwdComponent);
    },

    _onCusDetailResponse: function () {
        var detail = this.get('registrationObj.cusDetailSts');

        if (detail === tradeConstants.RegistrationStatus.Success) {
            var targetController = this.get('targetController');
            var nextTabId = this.get('registrationObj.isOTPEnabled') === tradeConstants.OtpStatus.OtpEnabled ? 0 : 1;

            if (targetController) {
                targetController.set('cusNumber', this.get('cusNumber'));
                targetController.loadNextTab(nextTabId);
            }

            Ember.set(this.get('registrationObj'), 'isOTPEnabled', 0);

            this._resetFields();
            this.removeObserver('registrationObj.cusDetailSts', this, this._onCusDetailResponse);
        } else {
            this.set('message', this.get('registrationObj.cusDetailStsMsg'));
        }

        Ember.set(this.get('registrationObj'), 'cusDetailSts', 0);
    },

    _resetFields: function () {
        this.set('idNumber', '');
        this.set('cusNumber', '');
        this.set('message', '');
    },

    _onSubmit: function () {
        var idNumber = this.get('idNumber');
        var cusNumber = this.get('cusNumber');

        var isFieldsFilled = utils.validators.isAvailable(idNumber) && utils.validators.isAvailable(cusNumber);

        if (isFieldsFilled) {
            Ember.run.cancel(this.get('cdTimer'));

            this.validateCusDetail(idNumber, cusNumber);
        } else if (!isFieldsFilled) {
            this.set('message', 'Fields cannot be empty');
        }
    },

    isMobile: function () {
        return appConfig.customisation.isMobile;
    }.property(),

    actions: {
        onSubmit: function () {
            this._onSubmit();
        },

        onReset: function () {
            this._resetFields();
        },

        onBackToLogin: function () {
            var targetController = this.get('targetController');

            if (targetController) {
                targetController.finishRegistration();
            }
        }
    }
});

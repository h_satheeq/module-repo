import Ember from 'ember';
import languageDataStore from '../../models/shared/language/language-data-store';
import sharedService from '../../models/shared/shared-service';
import utils from '../../utils/utils';
import tradeConstants from '../../models/trade/trade-constants';

export default Ember.Component.extend({
    layoutName: 'register/registration-password',
    app: languageDataStore.getLanguageObj(),
    registrationObj: Ember.A(),
    isPwdSubmitDisabled: true,
    isPwsInputDisabled: false,

    initialize: function () {
        this.set('registrationObj', sharedService.getService('trade').registrationDS.getRegistration());
        this._resetFields();

        this.set('userName', this.isForgotPwdComponent ? this.get('registrationObj.lginAlas') : '');
        this.addObserver('registrationObj.pwdSts', this, this._onPasswordDetailResponse);
        this.bindEnterKeyLogin();
    }.on('init'),

    bindEnterKeyLogin: function () {
        var that = this;
        Ember.$(window).unbind('keypress');

        Ember.$(window).bind('keypress', function (e) {
            if (e.which === utils.Constants.KeyCodes.Enter) {
                that._onSubmit();
            }
        });
    },

    validateRegPasswordDetail: function (cusNumber, username, password) {
        var reqObj = {mubNo: cusNumber, lginAlas: username, nwPswd: password};
        sharedService.getService('trade').validatePasswordDetail(reqObj, this.isForgotPwdComponent);
    },

    _onPasswordDetailResponse: function () {
        var pwdSts = this.get('registrationObj.pwdSts');

        if (pwdSts === tradeConstants.RegistrationStatus.Success) {
            if (this.isForgotPwdComponent) {
                this.set('password', '');
                this.set('retypePwd', '');
            } else {
                this._resetFields();
            }

            var pwdStsMsg = this.isForgotPwdComponent ? this.app.lang.messages.passwordReset : this.app.lang.messages.registrationSuccess;

            this.set('messageStsColor', 'up-fore-color');
            this.set('message', pwdStsMsg);
            this.set('isPwsInputDisabled', true);

            this.removeObserver('registrationObj.pwdSts', this, this._onPasswordDetailResponse);
        } else {
            this.set('message', this.get('registrationObj.pwdStsMsg'));
        }

        Ember.set(this.get('registrationObj'), 'pwdSts', 0);
    },

    _resetFields: function () {
        this.set('userName', '');
        this.set('password', '');
        this.set('retypePwd', '');
        this.set('message', '');
    },

    _onSubmit: function () {
        var username = this.get('userName');
        var password = this.get('password');
        var retypePwd = this.get('retypePwd');

        this.set('messageStsColor', 'down-fore-color');
        this.set('message', '');

        var encryptedSha1Pwd = utils.crypto.generateHashedText(password);

        var isFieldsFilled = utils.validators.isAvailable(username) && utils.validators.isAvailable(password) && utils.validators.isAvailable(retypePwd);
        var passWordsMatched = password === retypePwd;

        if (isFieldsFilled && passWordsMatched) {
            if (this.isUNameRulesMatch && this.isPwdRulesMatch) {
                this.validateRegPasswordDetail(this.get('targetController.cusNumber'), username, encryptedSha1Pwd);
            } else {
                this.set('message', this.app.lang.messages.didNotMeetPwRules);
            }
        } else if (!passWordsMatched) {
            this.set('message', 'Passwords Mismatch');
        }
    },

    actions: {
        onSubmit: function () {
            this._onSubmit();
        },

        onHome: function () {
            var registrationController = this.get('targetController');

            if (registrationController) {
                registrationController.loadHomeTab();
            }
        },

        onBackToLogin: function () {
            var targetController = this.get('targetController');

            if (targetController) {
                targetController.finishRegistration();
                Ember.run.cancel(this.get('rulesTimer'));
            }
        }
    }
});

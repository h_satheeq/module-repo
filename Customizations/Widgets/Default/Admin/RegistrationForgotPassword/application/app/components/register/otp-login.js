import Ember from 'ember';
import languageDataStore from '../../models/shared/language/language-data-store';
import utils from '../../utils/utils';
import sharedService from '../../models/shared/shared-service';
import tradeConstants from '../../models/trade/trade-constants';

export default Ember.Component.extend({
    layoutName: 'register/otp-login',
    app: languageDataStore.getLanguageObj(),
    registrationObj: Ember.A(),
    isOtpSubmitDisabled: true,
    otpTimer: undefined,
    mobileNo: '',
    mobileNoShowLength: 4,

    initialize: function () {
        this.set('registrationObj', sharedService.getService('trade').registrationDS.getRegistration());

        var mobileNum = this.get('registrationObj.mobile');
        mobileNum = mobileNum.length > this.mobileNoShowLength ? 'xxxxxxx' + mobileNum.substr(mobileNum.length - (this.mobileNoShowLength - 1)) : 'xxxxxxxxxx';

        this.set('mobileNo', mobileNum);
        this.enableOtpSubmit();
        this._resetFields();
        this.addObserver('registrationObj.otpSts', this, this._onOtpValidationResponse);
        this.bindEnterKeyLogin();
    }.on('init'),

    bindEnterKeyLogin: function () {
        var that = this;
        Ember.$(window).unbind('keypress');

        Ember.$(window).bind('keypress', function (e) {
            if (e.which === utils.Constants.KeyCodes.Enter) {
                that._onSubmit();
            }
        });
    },

    enableOtpSubmit: function () {
        var otpPin = this.get('otpPin');

        if (utils.validators.isAvailable(otpPin)) {
            this.set('isOtpSubmitDisabled', false);
        } else {
            this.set('isOtpSubmitDisabled', true);
        }

        this.set('otpTimer', Ember.run.later(this, this.enableOtpSubmit, 1000));
    },

    validateOtpPin: function (cusNumber, otpPin) {
        var reqObj = {mubNo: cusNumber, pin: otpPin};

        sharedService.getService('trade').validateOtpPinCode(reqObj);
    },

    // Only for Resend OTP
    _resendOtp: function () {
        sharedService.getService('trade').resendOtpPin();
    },

    _onOtpValidationResponse: function () {
        var otpSts = this.get('registrationObj.otpSts');

        if (otpSts === tradeConstants.RegistrationStatus.Success) {
            var targetController = this.get('targetController');

            if (targetController) {
                targetController.loadNextTab();
            }

            this._resetFields();
            this.removeObserver('registrationObj.otpSts', this, this._onOtpValidationResponse);
        } else {
            this.set('message', this.get('registrationObj.otpStsMsg'));
        }

        Ember.set(this.get('registrationObj'), 'otpSts', 0);
    },

    _resetFields: function () {
        this.set('otpPin', '');
        this.set('message', '');
    },

    _onSubmit: function () {
        var otpPin = this.get('otpPin');
        var isFieldsFilled = utils.validators.isAvailable(otpPin);

        if (isFieldsFilled) {
            this.validateOtpPin(this.get('targetController.cusNumber'), otpPin);

            Ember.run.cancel(this.get('otpTimer'));
        } else {
            this.set('message', 'Fields cannot be empty');
        }
    },

    actions: {
        onSubmit: function () {
            this._onSubmit();
        },

        onHome: function () {
            var registrationController = this.get('targetController');

            if (registrationController) {
                registrationController.loadHomeTab();
            }
        },

        resendOtp: function () {
            this._resendOtp();
            this.set('message', 'OTP is Sent Again');
        }
    }
});

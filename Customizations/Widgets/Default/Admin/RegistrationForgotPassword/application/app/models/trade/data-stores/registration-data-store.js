import Ember from 'ember';

export default Ember.Object.extend({
    registrationObj: undefined,

    getRegistration: function () {
        if (!this.get('registrationObj')) {
            var registerObj = Ember.Object.create(
                {
                    cusDetailSts: 0,
                    cusDetailStsMsg: '',
                    isOTPEnabled: 0,
                    mobile: '',
                    otpSts: 0,
                    otpStsMsg: '',
                    agreementSts: 0,
                    agreementStsMsg: '',
                    pwdSts: 0,
                    pwdStsMsg: ''
                }
            );

            this.set('registrationObj', registerObj);
        }

        return this.get('registrationObj');
    },

    setData: function (message) {
        var that = this;

        Ember.$.each(message, function (key, value) {
            that.registrationObj.set(key, value);
        });
    }
});

import BaseModuleInitializer from '../../../../models/shared/initializers/base-module-initializer';
import sharedService from '../../../../models/shared/shared-service';
import RegistrationDataStore from '../../../../models/trade/data-stores/registration-data-store';

export default BaseModuleInitializer.extend({
    postInitialize: function () {
        var tradeService = sharedService.getService('trade');
        tradeService.set('registrationDS', RegistrationDataStore.create({tradeService: tradeService}));
    }
});

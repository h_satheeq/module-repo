import Registration from './registration';
import appConfig from '../../../../config/app-config';

export default Registration.extend({
    registerTabArray: [],

    isMobile: function () {
        return appConfig.customisation.isMobile;
    }.property(),

    createTab: function () {
        var registerTabArray = [];
        var name = 'step';

        registerTabArray[registerTabArray.length] = {code: registerTabArray.length + 1, name: name + (registerTabArray.length + 1), componentString: 'component:register/registration-detail', routeString: 'components/register/registration-detail', popupName: 'forgotPwdDetailPopup', isActive: true};
        registerTabArray[registerTabArray.length] = {code: registerTabArray.length + 1, name: name + (registerTabArray.length + 1), componentString: 'component:register/otp-login', routeString: 'components/register/otp-login', popupName: 'forgotPwdOtpLoginPopup', isActive: false};
        registerTabArray[registerTabArray.length] = {code: registerTabArray.length + 1, name: name + (registerTabArray.length + 1), componentString: 'component:register/registration-password', routeString: 'components/register/registration-password', popupName: 'forgotPwdPasswordPopup', isActive: false};

        this.set('registerTabArray', registerTabArray);
    },

    renderComponent: function (forgotPwdComponent, routeString) {
        var route = this.container.lookup('route:application');

        forgotPwdComponent.set('isForgotPwdComponent', true);

        route.render(routeString, {
            into: 'trade/widgets/registration-forgot-password/forgot-password',
            outlet: 'forgotPasswordOutlet',
            controller: forgotPwdComponent
        });
    }
});

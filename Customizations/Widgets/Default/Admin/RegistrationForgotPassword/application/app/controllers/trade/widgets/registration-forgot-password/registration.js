import Ember from 'ember';
import BaseArrayController from '../../../base-array-controller';
import ControllerFactory from '../../../controller-factory';
import sharedService from '../../../../models/shared/shared-service';
import appConfig from '../../../../config/app-config';

export default BaseArrayController.extend({
    registerTabArray: [],

    isMobile: function () {
        return appConfig.customisation.isMobile;
    }.property(),

    onLoadWidget: function () {
        this.createTab(); // Always place at top

        var registerTabArray = this.get('registerTabArray');
        var that = this;

        registerTabArray.forEach(function (element) {
            Ember.set(element, 'displayName', that.get('app').lang.labels[element.name]);
        });

        this.loadRegisterTab(registerTabArray[0]);
        this.set('activeTab', registerTabArray[0]);

        Ember.set(registerTabArray[0], 'isActive', true);
    },

    createTab: function () {
        var registerTabArray = [];
        var name = 'step';

        registerTabArray[registerTabArray.length] = {code: registerTabArray.length + 1, name: name + (registerTabArray.length + 1), componentString: 'component:register/registration-detail', routeString: 'components/register/registration-detail', popupName: 'regDetailPopup', isActive: true};
        registerTabArray[registerTabArray.length] = {code: registerTabArray.length + 1, name: name + (registerTabArray.length + 1), componentString: 'component:register/otp-login', routeString: 'components/register/otp-login', popupName: 'regOtpLoginPopup', isActive: false};
        registerTabArray[registerTabArray.length] = {code: registerTabArray.length + 1, name: name + (registerTabArray.length + 1), componentString: 'component:register/registration-agreement', routeString: 'components/register/registration-agreement', popupName: 'regAgreementPopup', isActive: false};
        registerTabArray[registerTabArray.length] = {code: registerTabArray.length + 1, name: name + (registerTabArray.length + 1), componentString: 'component:register/registration-password', routeString: 'components/register/registration-password', popupName: 'regPasswordPopup', isActive: false};

        this.set('registerTabArray', registerTabArray);
    },

    onUnloadWidget: function () {
        Ember.set(this.get('activeTab'), 'isActive', false);
        this.set('registerTabArray', []);
    },

    // Call this function from the component on Submit button clicked
    loadNextTab: function (nextTabId) {
        var registerTabArray = this.get('registerTabArray');
        var activeTab = this.get('activeTab');
        var activeTabIndex = nextTabId ? nextTabId : registerTabArray.indexOf(activeTab);

        Ember.set(activeTab, 'isActive', false);

        if (activeTabIndex === registerTabArray.length - 1) {
            // Finish registration
        } else {
            var currentRegTab = registerTabArray[activeTabIndex + 1];

            this.loadRegisterTab(currentRegTab);
            this.set('activeTab', currentRegTab);

            Ember.set(currentRegTab, 'isActive', true);
        }
    },

    // Call this function from the component on Home button clicked
    loadHomeTab: function () {
        var registerTabArray = this.get('registerTabArray');
        var homeRegTab = registerTabArray[0];

        Ember.set(this.get('activeTab'), 'isActive', false);

        this.loadRegisterTab(homeRegTab);
        this.set('activeTab', homeRegTab);

        Ember.set(homeRegTab, 'isActive', true);
    },

    loadRegisterTab: function (option) {
        var registerTabArray = this.get('registerTabArray');
        var currentRegisterObj = registerTabArray[option.code - 1];
        var componentString = currentRegisterObj.componentString;
        var routeString = currentRegisterObj.routeString;

        var registrationComponent = sharedService.getService('sharedUI').getService(currentRegisterObj.popupName);

        if (!registrationComponent) {
            registrationComponent = ControllerFactory.createController(this.container, componentString);
            sharedService.getService('sharedUI').registerService(currentRegisterObj.popupName, registrationComponent);
        }

        this.renderComponent(registrationComponent, routeString);

        registrationComponent.set('targetController', this);
        registrationComponent.initialize();
    },

    finishRegistration: function () {
        if (Ember.$.isFunction(this.get('loginViewCallbackFn'))) {
            this.get('loginViewCallbackFn')();
        }
    },

    renderComponent: function (registrationComponent, routeString) {
        var route = this.container.lookup('route:application');

        route.render(routeString, {
            into: 'trade/widgets/registration-forgot-password/registration',
            outlet: 'registrationTabOutlet',
            controller: registrationComponent
        });
    }
});

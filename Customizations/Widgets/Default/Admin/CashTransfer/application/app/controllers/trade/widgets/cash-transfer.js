import Ember from 'ember';
import BaseController from '../../base-controller';
import sharedService from '../../../models/shared/shared-service';
import utils from '../../../utils/utils';
import appConfig from '../../../config/app-config';
import userSettings from '../../../config/user-settings';

export default BaseController.extend({
    tradeService: sharedService.getService('trade'),
    dateFormat: 'YYYYMMDD',
    datePickerFormat: userSettings.displayFormat.dateFormat.toLowerCase(),
    fromDate: undefined,

    msgCss: '',
    rejectionRsn: '',
    lan: '',

    accountColl: Ember.A(),
    payMethodCollection: Ember.A(),
    currencyCollection: Ember.A(),
    bankAccCollection: Ember.A(),

    currentAccount: {},
    currentBankAcc: {},
    currentPayMtd: {},
    currentCurrency: {},

    isNativeDevice: navigator.isNativeDevice,
    isMobile: false,
    isStaticLableEnabled: false,

    withdrawalStatus: {
        Success: 1,
        Fail: -1
    },

    setCurrentBankAcc: function () {
        Ember.run.once(this, this._setDefaultBankAccount);
    }.observes('bankAccCollection.@each'),

    onLoadWidget: function () {
        this.tradeService.subscribeTradeMetaReady(this, this.get('wkey'));
        this.tradeService.subscribeTradeDisconnect(this, this.get('wkey'));

        this.set('isMobile', appConfig.customisation.isMobile);
        this.set('lan', sharedService.userSettings.currentLanguage.toLowerCase());
    },

    onLanguageChanged: function () {
        this._setRejectionReason();
        this._loadPaymentMethod();

        this.set('message', '');
        this.set('lan', sharedService.userSettings.currentLanguage.toLowerCase());
    },

    onPrepareData: function () {
        this.loadContent();
    },

    onTradeMetaReady: function () {
        this.loadContent();
    },

    onTradeDisconnect: function () {
        this.onClearData();
    },

    onClearData: function () {
        this.set('portfolioCollection', []);
        this.set('payMethodCollection', []);
        this.set('currencyCollection', []);
        this.set('bankAccCollection', []);
        this.set('message', '');
        this.set('amount', '');
    },

    onUnloadWidget: function () {
        this.tradeService.unSubscribeTradeMetaReady(this, this.get('wkey'));
        this.tradeService.unSubscribeTradeDisconnect(this, this.get('wkey'));
    },

    loadContent: function () {
        var accountColl = this.tradeService.accountDS.getTradingAccCollection();
        var currentAccount = this.get('currentAccount');
        var today = new Date();

        if (accountColl.length > 1) {
            var defaultDescription = this.get('app').lang.labels.pleaseSelect;
            currentAccount = Ember.Object.create({cashAccount: {invAccNo: defaultDescription}, dispProp: defaultDescription});

            this.set('isStaticLableEnabled', true);
        }

        if (accountColl && accountColl.length > 0) {
            Ember.$.each(accountColl, function (key, port) {
                sharedService.getService('trade').sendBuyingPowerRequest(port);
            });
        }

        if (!currentAccount || !currentAccount.cashAccount) {
            currentAccount = accountColl.length > 0 ? accountColl[0] : {};
        }

        this._loadPaymentMethod();

        this.set('currentAccount', currentAccount);
        this.set('withdrawalDate', today);
        this.set('displayWithdrawalDate', utils.formatters.convertToDisplayTimeFormat(today, 'DD-MM-YYYY'));
        this.set('fromDate', today);
        this.set('currencyDropdownOptions', currentAccount.tradingAccId ? this.tradeService.accountDS.getCurrencyCollection() : []);
        this.set('bankAccCollection', this.tradeService.bankAccountDS.getBankAccColl());
        this.set('currentCurrency', currentAccount.tradingAccId ? this.get('currencyDropdownOptions')[0] : {});
        this.set('accountColl', accountColl);
        this.set('message', '');
    },

    _loadPaymentMethod: function () {
        var payMethodCollection = this.tradeService.tradeMetaDS.getPayMethods();

        this.set('payMethodCollection', payMethodCollection);
        this.set('currentPayMtd', payMethodCollection[0]);
    },

    loadCurrencyOptions: function () {
        var currOptions = [{Id: 0, DisplayName: this.get('currentAccount').curr}];

        this.set('currencyDropdownOptions', this.get('currentAccount.tradingAccId') ? currOptions : []);
        this.set('currentCurrency', this.get('currentAccount.tradingAccId') ? currOptions[0] : {});
    }.observes('currentAccount'),

    setAccountCollection: function () {
        var accountCollection = this.tradeService.accountDS.getTradingAccCollection();

        Ember.$.each(accountCollection, function (key, port) {
            sharedService.getService('trade').sendBuyingPowerRequest(port);
        });
    },

    _showMessage: function (sts, msg) {
        var app = this.get('app');

        if (sts === this.withdrawalStatus.Success) {
            this.set('message', app.lang.messages.requestSuccess);
            this.set('msgCss', 'up-fore-color');
            this.set('amount', '');
            this.setAccountCollection();
            this.set('isStaticLableEnabled', true);
            this.set('currentAccount', {});
            this.set('currentCurrency', {});
        } else if (sts === this.withdrawalStatus.Fail) {
            if (msg) {
                var rejectionRsn = utils.formatters.convertUnicodeToNativeString(msg);
                var desArray = rejectionRsn.split('|');

                this.set('desArray', desArray);
            }

            this._setRejectionReason();
        }
    },

    _setRejectionReason: function () {
        var desArray = this.get('desArray');
        var msg = '';

        if (desArray && desArray.length > 0) {
            var currLang = sharedService.userSettings.currentLanguage;
            var supportedLanguages = appConfig.customisation.supportedLanguages;
            var secondaryLang = supportedLanguages.length > 1 ? supportedLanguages[1].code : supportedLanguages[0].code;

            switch (currLang) {
                case supportedLanguages[0].code:
                    msg = desArray[0];
                    break;

                case secondaryLang:
                    msg = desArray.length > 1 ? desArray[1] : desArray[0];
                    break;
            }

            this.set('message', msg);
            this.set('msgCss', 'down-fore-color');
        }
    },

    _setDefaultBankAccount: function () {
        var bankAccounts = this.get('bankAccCollection');

        if (bankAccounts && bankAccounts.length > 0) {
            this.set('currentBankAcc', bankAccounts[0]);
        }
    },

    _checkWithdrawalRequestValidity: function () {
        return utils.validators.isAvailable(this.get('amount')) && this.get('amount') > 0 && this.get('currentAccount.tradingAccId');
    },

    submitRequest: function () {
        var that = this;

        this.tradeService.sendWithdrawalRequest({
            payMtd: this.get('currentPayMtd').Id,
            tradingAccId: this.get('currentAccount.tradingAccId'),
            curr: this.get('currentCurrency').DisplayName,
            amt: this.get('amount'),
            bId: this.get('currentBankAcc').bankId,
            toAcc: this.get('currentBankAcc').bnkAccNum,
            valDte: this.utils.formatters.convertToDisplayTimeFormat(this.get('withdrawalDate'), this.dateFormat)
        }, function (sts, msg) {
            that._showMessage(sts, msg);
        });
    },

    _submitWithdrawalRequest: function () {
        this.set('message', '');

        if (this._checkWithdrawalRequestValidity()) {
            this.showMessagePopup();
        } else {
            var app = this.get('app');
            this.set('msgCss', 'down-fore-color');

            if (this.get('amount') === 0.00) {
                this.set('message', app.lang.messages.amountGTZero);
            } else {
                this.set('message', app.lang.messages.mandatoryFields);
            }
        }
    },

    showMessagePopup: function () {
        var that = this;

        Ember.run.next(function () {
            var confirmMsg = that.app.lang.messages.confirmBankTransferRequest;
            confirmMsg = confirmMsg.replace('[Amount]', utils.formatters.formatNumber(that.get('amount'), 2));
            confirmMsg = confirmMsg.replace('[Currency]', that.get('currentCurrency').DisplayName);
            confirmMsg = confirmMsg.replace('[PortName]', that.get('currentAccount.tradingAccName'));
            confirmMsg = confirmMsg.replace('[InvAccNo]', that.get('currentAccount.cashAccount.invAccNo'));
            confirmMsg = confirmMsg.replace('[BnkAccNum]', that.get('currentBankAcc').bnkAccNum);

            that.utils.messageService.showMessage(confirmMsg,
                that.utils.Constants.MessageTypes.Question,
                false,
                that.app.lang.labels.confirm,
                [{type: that.utils.Constants.MessageBoxButtons.Yes, btnAction: that.submitRequest.bind(that)},
                    {type: that.utils.Constants.MessageBoxButtons.No}]
            );
        });
    },

    actions: {
        setPortfolio: function (option) {
            this.set('currentAccount', option);
            this.set('isStaticLableEnabled', false);
            this.set('message', '');
        },

        setBankAcc: function (option) {
            this.set('currentBankAcc', option);
        },

        setCurrency: function (option) {
            this.set('currentCurrency', option);
        },

        setPayMethod: function (option) {
            this.set('currentPayMtd', option);
        },

        onSubmit: function () {
            this._submitWithdrawalRequest();
        },

        pickDate: function () {
            var withdrawalDate = this.get('withdrawalDate');
            var that = this;
            var options = {date: withdrawalDate, mode: 'date'};

            if (window.plugins && window.plugins.datePicker) {
                window.plugins.datePicker.show(options, function (selectedDate) {
                    if (selectedDate) {
                        that.set('withdrawalDate', selectedDate);
                        that.set('displayWithdrawalDate', utils.formatters.convertToDisplayTimeFormat(selectedDate, 'DD-MM-YYYY'));
                    }
                });
            }
        }
    }
});
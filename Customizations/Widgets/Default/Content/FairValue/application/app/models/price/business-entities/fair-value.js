import Ember from 'ember';

export default Ember.Object.extend({
    sym: '',            // Symbol
    exg: '',            // Exchange
    fv: 0,             // Fair Value
    av: 0,             // Actual Value
    date: '',           // FV Data
    companyId: '',      // Company Id
    source: '',         // FV Source Name
    docId: '',          // Doc ID
    ratingId: '',       // FV Rating ID
    ratingName: '',     // FV Rating Name
    individualIdList: '', // Individual ID List
    ratingScore: '',    // FV Rating Score
    reportDate: '',     // Report Date
    adjustedFv: 0,      // Adjusted Fair Value
    lnCode: '',         // Language Code
    ticketSerial: '',   // Ticket Serial
    stock: {},

    upsidePotential: function () {
        var ltp = this.get('stock.ltp');

        return ltp !== 0 ? parseFloat((this.get('fv') - ltp) / ltp * 100) : null;
    }.property('fv', 'stock.ltp'),

    setData: function (fvMsg) {
        var that = this;

        Ember.$.each(fvMsg, function (key, value) {
            that.set(key, value);
        });
    }
});
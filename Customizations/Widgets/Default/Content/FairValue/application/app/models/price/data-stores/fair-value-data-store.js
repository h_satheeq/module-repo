import Ember from 'ember';
import FairValue from '../business-entities/fair-value';
import utils from '../../../utils/utils';

export default Ember.Object.extend({
    store: {},
    fairValueMapByExgSym: {},

    getFairValue: function (exg, sym, companyId) {
        var key = utils.keyGenerator.getKey(exg, sym);

        var currentStore = this.get('store');
        currentStore[key] = currentStore[key] || {};

        var fvObj = currentStore[key][companyId];

        if (!fvObj) {
            var stock = this.priceService.stockDS.getStock(exg, sym);

            fvObj = FairValue.create({exg: exg, sym: sym, companyId: companyId, stock: stock});

            currentStore[key][companyId] = fvObj;
            this.addToOtherCollections(exg, sym, fvObj);

            return fvObj;
        } else {
            return null;
        }
    },

    addToOtherCollections: function (exg, sym, fvObj) {
        var key = utils.keyGenerator.getKey(exg, sym);
        var currentFvMapByExgSym = this.get('fairValueMapByExgSym');

        if (currentFvMapByExgSym[key]) {
            currentFvMapByExgSym[key].pushObject(fvObj);
        } else {
            currentFvMapByExgSym[key] = Ember.A([fvObj]);
        }
    },

    getFairValueCollectionBySymbol: function (exg, sym) {
        var key = utils.keyGenerator.getKey(exg, sym);
        var fairValueMapByExgSym = this.get('fairValueMapByExgSym');

        if (!fairValueMapByExgSym[key]) {
            fairValueMapByExgSym[key] = Ember.A([]);
        }

        return fairValueMapByExgSym[key];
    }
});

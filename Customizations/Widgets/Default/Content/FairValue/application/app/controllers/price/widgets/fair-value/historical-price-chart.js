/* global STXChart */
/* global STX */
/* global _ */

import ProChart from '../../../chart/pro-chart';
import sharedService from '../../../../models/shared/shared-service';
import ChartConstants from '../../../../models/chart/chart-constants';
import utils from '../../../../utils/utils';
import LanguageDataStore from '../../../../models/shared/language/language-data-store';

export default ProChart.extend({
    baseChart: null,
    graphDataManager: null,

    chartStyle: ChartConstants.ChartStyle.Line,
    chartViewPeriod: ChartConstants.ChartViewPeriod.OneYear,
    chartCategory: ChartConstants.ChartCategory.History,

    maxArray: [],
    medianArray: [],
    minArray: [],

    app: LanguageDataStore.getLanguageObj(),

    onLoadWidget: function () {
        this._super();

        var gDM = this.get('graphDataManager');

        this.chartCategory.DefaultDataRequestDuration = 1;

        gDM.set('chartCategory', this.chartCategory);
        gDM.set('chartStyle', this.chartStyle);
        gDM.set('chartViewPeriod', this.chartViewPeriod);
    },

    initChart: function (chartContainer) {
        this._super(chartContainer);

        var baseChart = this.get('baseChart');

        baseChart.chart.xAxis.displayGridLines = false;
        baseChart.chart.yAxis.displayGridLines = false;

        baseChart.chart.panel.yAxis.drawCurrentPriceLabel = false;
        baseChart.chart.panel.yAxis.drawPriceLabels = false;
        baseChart.chart.panel.yAxis.noDraw = true;
        baseChart.controls.chartControls.style.display = 'none';

        var that = this;

        STXChart.prototype.append('draw', function () {
            that.appendSeparatorVerticalLine(baseChart);
        });
    },

    // Pro Chart Base overrides
    onDataFromMix: function (chartSymbolObj) {
        var baseChart = this.get('baseChart');

        if (baseChart) {
            baseChart.newChart(this.get('sym'), null, null, this.onFinishedLoadingNewChart(baseChart.chart.symbol));
        } else {
            this.set('reloadChartData', chartSymbolObj);
        }
    },

    // Pro Chart overrides
    onFinishedLoadingNewChart: function () {
        var that = this;

        return function (error) {
            if (!error) {
                var baseChart = that.get('baseChart');

                if (baseChart.masterData && baseChart.masterData.length > 0) {
                    baseChart.setRange({
                        dtLeft: baseChart.masterData[0].DT, // Set this to the date to appear on the left edge of chart
                        dtRight: baseChart.masterData[baseChart.masterData.length - 1].DT,  // Set this to the date to appear on the right edge of chart
                        padding: 30 // Set this to the number of pixels of padding to leave between the chart and y-axis
                    });
                }

                baseChart.draw();

                that.drawMaxMedianMinChart(); // Call addSeries after baseChart is ready
            }
        };
    },

    drawMaxMedianMinChart: function () {
        var baseChart = this.get('baseChart');

        if (baseChart) {
            var chartLineColor = '#0066CC'; // Blue
            var yAxisMaxMin = this._findChartMaxMin();
            var yMax = yAxisMaxMin[0];
            var yMin = yAxisMaxMin[1];

            baseChart.chart.yAxis.max = yMax;
            baseChart.chart.yAxis.min = yMin;

            // Define the left axis for the renderer
            var axis = new STXChart.YAxis();

            axis.position = 'left';
            axis.max = yMax;
            axis.min = yMin;
            axis.drawCurrentPriceLabel = false;
            axis.drawPriceLabels = false;

            // set the renderer
            var renderer = baseChart.setSeriesRenderer(new STX.Renderer.Lines({
                params: {
                    name: 'lines',
                    type: 'line',
                    yAxis: axis
                }
            }));

            // Create series and attach them to the chart when the data is loaded.
            baseChart.addSeries('Max', {
                data: this.loadSeries(this.get('maxTargetPrice'), true, undefined, undefined)
            });

            baseChart.addSeries('Min', {
                data: this.loadSeries(this.get('minTargetPrice'), undefined, undefined, true)
            });

            baseChart.addSeries('Median', {
                data: this.loadSeries(this.get('medianTargetPrice'), undefined, true, undefined)
            });

            renderer.removeAllSeries()
                .attachSeries('Max', {color: chartLineColor, permanent: true})
                .attachSeries('Min', {color: chartLineColor, permanent: true})
                .attachSeries('Median', {color: chartLineColor, permanent: true})
                .ready();
        }
    },

    onDisplayVolume: function () {
        // Base over ride
    },

    // Pro Chart overrides
    onGetChartInitialBulkRecords: function (params, cb) {
        var chartDataArray = this.get('graphDataManager').getDataArray();
        var chartDataArrayClone = _.clone(chartDataArray, true);

        var chartDataArrayLength = chartDataArray.length;
        var chartDataArrayLengthNew = chartDataArrayLength * 2;

        var lastDate = chartDataArrayClone[chartDataArrayClone.length - 1].DT;
        var lastDateClone = new Date(lastDate);

        var yAxisMin = this._findChartMaxMin()[1];

        for (var i = 0; i < chartDataArrayLength; i++) {
            chartDataArrayClone[i].Close = yAxisMin;
        }

        for (var k = chartDataArrayLength; k < chartDataArrayLengthNew; k++) {
            chartDataArrayClone[k] = {
                Close: yAxisMin,
                DT: lastDateClone.setDate(lastDateClone.getDate() + 1)
            };
        }

        cb({
            quotes: chartDataArrayClone,
            moreAvailable: true
        });
    },

    loadSeries: function (targetPrice, isMax, isMedian, isMin) {
        var chartDataArray = this.get('graphDataManager').getDataArray();
        var chartDataArrayClone = this._cloneChartArray(chartDataArray);

        var chartDataArrayLength = chartDataArray.length;
        var chartDataArrayLengthNew = chartDataArrayLength * 2;

        var lastDate = chartDataArrayClone[chartDataArrayClone.length - 1].DT;
        var lastDateClone = new Date(lastDate);
        var lastClose = chartDataArray[chartDataArrayLength - 1].Close;

        var count = 1;
        var yAxisMin = this._findChartMaxMin()[1];

        if (targetPrice) {
            for (var i = chartDataArrayLength; i < chartDataArrayLengthNew; i++) {
                var close = lastClose + ((targetPrice - lastClose) * count) / chartDataArrayLength;

                chartDataArrayClone[i] = {
                    Close: close,
                    DT: new Date(lastDateClone.setDate(lastDateClone.getDate() + 1))
                };

                count++;
            }
        } else {
            for (var j = chartDataArrayLength - 1; j < chartDataArrayLengthNew; j++) {
                chartDataArrayClone[j] = {
                    Close: yAxisMin,
                    DT: new Date(lastDateClone.setDate(lastDateClone.getDate() + 1))
                };

                count++;
            }
        }

        if (isMax) {
            this.set('maxArray', chartDataArrayClone);
        }

        if (isMedian) {
            this.set('medianArray', chartDataArrayClone);
        }

        if (isMin) {
            this.set('minArray', chartDataArrayClone);
        }

        var baseChart = this.get('baseChart');
        var that = this;

        STXChart.prototype.append('draw', function () {
            that.appendMaxMedianMinText(baseChart, chartDataArrayClone, isMax, isMedian, isMin);
        });

        return chartDataArrayClone;
    },

    appendSeparatorVerticalLine: function (baseChart) {
        if (baseChart.chart.dataSet.length) {
            var panel = baseChart.chart.panel;
            var context = baseChart.chart.context;

            var date = new Date();
            var x = baseChart.pixelFromDate(date, baseChart.chart);

            baseChart.startClip(panel.name); // save current context

            context.beginPath();
            context.moveTo(x, baseChart.pixelFromPrice(0, panel));
            context.lineTo(x, baseChart.pixelFromPrice(400, panel));
            context.lineWidth = 2;
            context.strokeStyle = '#4682B4'; // Blue
            context.stroke();

            baseChart.endClip(); // Restore previous context so all is back how it was.
        }
    },

    appendUpsidePotentialText: function (isMax, isMedian, isMin) {
        var that = this;
        var baseChart = this.get('baseChart');
        var chartArray;

        if (isMax) {
            chartArray = this.get('maxArray');
        } else if (isMedian) {
            chartArray = this.get('medianArray');
        } else if (isMin) {
            chartArray = this.get('minArray');
        }

        if (chartArray) {
            STXChart.prototype.append('draw', function () {
                that.appendMaxMedianMinText(baseChart, chartArray, isMax, isMedian, isMin);
            });
        }
    },

    appendMaxMedianMinText: function (baseChart, chartDataArray, isMax, isMedian, isMin) {
        if (baseChart && baseChart.chart.dataSet.length && chartDataArray.length) {
            var panel = baseChart.chart.panel;
            var price = chartDataArray[chartDataArray.length - 1].Close;
            var context = baseChart.chart.context;

            var y = baseChart.pixelFromPrice(price, panel);
            var x = baseChart.pixelFromDate(baseChart.chart.dataSet[baseChart.chart.dataSet.length - 1].Date, baseChart.chart);

            var maxTargetPrice = this.get('maxTargetPrice');
            var medianTargetPrice = this.get('medianTargetPrice');
            var minTargetPrice = this.get('minTargetPrice');

            var theme = sharedService.userSettings.currentTheme;
            var textColor = theme === 'theme1' ? '#FFFFFF' : '#000000';

            if (medianTargetPrice) {
                if (isMax && maxTargetPrice !== medianTargetPrice) {
                    context.fillStyle = textColor;
                    context.fillText(this.app.lang.labels.max + ': ' + utils.formatters.formatNumber(maxTargetPrice, 2), x - 20, y - 7);

                    var maxUpPotential = this._formatTextColor(this.get('maxUpsidePotential'), context);
                    context.fillText(maxUpPotential, x + 45, y - 7);
                } else if (isMedian) {
                    context.fillStyle = textColor;
                    context.fillText(this.app.lang.labels.median + ': ' + utils.formatters.formatNumber(medianTargetPrice, 2), x - 30, y - 7);

                    var medianUpPotential = this._formatTextColor(this.get('medianUpsidePotential'), context);
                    context.fillText(medianUpPotential, x + 45, y - 7);
                } else if (isMin && minTargetPrice !== medianTargetPrice) {
                    context.fillStyle = textColor;
                    context.fillText(this.app.lang.labels.min + ': ' + utils.formatters.formatNumber(minTargetPrice, 2), x - 20, y + 10);

                    var minUpPotential = this._formatTextColor(this.get('minUpsidePotential'), context);
                    context.fillText(minUpPotential, x + 35, y + 10);
                }
            }
        }
    },

    _formatTextColor: function (upsidePotential, context) {
        if (upsidePotential !== undefined) {
            if (upsidePotential > 0) {
                context.fillStyle = '#2ba25c'; // Green
            } else if (upsidePotential < 0) {
                context.fillStyle = '#ff0101'; // Red
            }

            return utils.formatters.formatNumberPercentage(upsidePotential, 2);
        } else {
            return '';
        }
    },

    _cloneChartArray: function (chartDataArray) {
        var chartArrayClone = [];

        for (var i = 0; i < chartDataArray.length; i++) {
            chartArrayClone[i] = chartDataArray[i];
        }

        return chartArrayClone;
    },

    _findChartMaxMin: function () {
        var ohlcSeries = sharedService.getService('price').ohlcDS.getOHLCSeries(this.get('exg'), this.get('sym'), this.chartCategory);

        if (ohlcSeries && ohlcSeries.max && ohlcSeries.min) {
            var max = ohlcSeries.max;
            var min = ohlcSeries.min;
            var maxTargetPrice = this.get('maxTargetPrice');
            var minTargetPrice = this.get('minTargetPrice');

            if (maxTargetPrice > max) {
                max = maxTargetPrice;
            }

            if (minTargetPrice < min) {
                min = minTargetPrice;
            }

            var addOrDeduction = 3;

            if (max - min > 30) {
                addOrDeduction = 10;
            }

            max = max + addOrDeduction;

            if (min - addOrDeduction > 0) {
                min = min - addOrDeduction;
            }

            return [max, min];
        }
    }
});

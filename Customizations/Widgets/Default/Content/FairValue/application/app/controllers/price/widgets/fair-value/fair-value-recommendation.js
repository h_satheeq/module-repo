import Ember from 'ember';
import BaseArrayController from '../../../base-array-controller';
import priceWidgetConfig from '../../../../config/price-widget-config';
import sharedService from '../../../../models/shared/shared-service';
import ControllerFactory from '../../../controller-factory';
import ChartConstants from '../../../../models/chart/chart-constants';
import utils from '../../../../utils/utils';
import appEvents from '../../../../app-events';

export default BaseArrayController.extend(Ember.SortableMixin, {
    fieldList: priceWidgetConfig.fairValue.fields,
    stock: undefined,
    fairValueArray: Ember.A(),
    chartController: undefined,

    maxTargetPrice: undefined,
    medianTargetPrice: undefined,
    minTargetPrice: undefined,

    maxUpsidePotential: undefined,
    medianUpsidePotential: undefined,
    minUpsidePotential: undefined,

    upPotentialColor: 'fore-color',
    consensus: undefined,
    consensusCount: 0,

    chartStyleArea: ChartConstants.ChartStyle.Area,
    chartStyleLine: ChartConstants.ChartStyle.Line,

    sortingProperties: ['fv'],
    sortedFairValueArray: Ember.computed.sort('fairValueArray', 'sortingProperties'),

    priceService: sharedService.getService('price'),

    onLoadWidget: function () {
        appEvents.subscribeSymbolChanged(this.get('wkey'), this, this.get('selectedLink'));
        this._setLanguageFields(this.get('app').lang.labels);
    },

    onPrepareData: function () {
        var exg = this.get('exg');
        var sym = this.get('sym');
        var stockObj = this.priceService.stockDS.getStock(exg, sym);

        this.set('fairValueArray', this.priceService.fairValueDS.getFairValueCollectionBySymbol(exg, sym));
        this.set('stock', stockObj);

        var title = [stockObj.sym, stockObj.lDes].join(' ~ ');
        this.set('title', title);
    },

    onAddSubscription: function () {
        var exg = this.get('exg');
        var sym = this.get('sym');
        var date = utils.formatters.generateHistoryBeginDateString(1, 0);
        var that = this;

        this.priceService.addSymbolRequest(exg, sym, this.get('inst'));

        this.priceService.sendFairValueHistoricalPriceRequest(exg, sym, date,
            function () {
                that.onFairValueDataReady();
            });
    },

    onClearData: function () {
        this.set('fairValueArray', Ember.A());
        this.set('medianTargetPrice', undefined);
        this.set('minTargetPrice', undefined);
        this.set('maxTargetPrice', undefined);

        this.set('medianUpsidePotential', undefined);
        this.set('maxUpsidePotential', undefined);
        this.set('minUpsidePotential', undefined);

        this.set('isMaxTextAppended', false);
        this.set('isMedianTextAppended', false);
        this.set('isMinTextAppended', false);
        this.set('chartController', undefined);
    },

    onRemoveSubscription: function () {
        this.priceService.removeSymbolRequest(this.get('exg'), this.get('sym'), this.get('inst'));
    },

    onUnloadWidget: function () {
        appEvents.unSubscribeSymbolChanged(this.get('wkey'), this.get('selectedLink'));
    },

    onFairValueDataReady: function () {
        this._calculateFvMedian();
        this.calculateMaxUpsidePotential();
        this.calculateMedianUpsidePotential();
        this.calculateMinUpsidePotential();

        this._loadHistoricalPriceChart();
    },

    _loadHistoricalPriceChart: function () {
        var controllerString = 'controller:price/widgets/fair-value/historical-price-chart';
        var routeString = 'price/widgets/fair-value/historical-price-chart';

        var widgetController = ControllerFactory.createController(this.container, controllerString);

        widgetController.set('maxTargetPrice', this.get('maxTargetPrice'));
        widgetController.set('medianTargetPrice', this.get('medianTargetPrice'));
        widgetController.set('minTargetPrice', this.get('minTargetPrice'));

        widgetController.set('maxUpsidePotential', this.get('maxUpsidePotential'));
        widgetController.set('medianUpsidePotential', this.get('medianUpsidePotential'));
        widgetController.set('minUpsidePotential', this.get('minUpsidePotential'));

        widgetController.set('consensusCount', this.get('consensusCount'));

        widgetController.set('sym', this.get('sym'));
        widgetController.set('exg', this.get('exg'));
        widgetController.set('inst', this.get('inst'));
        widgetController.set('wkey', this.get('wkey'));

        this.set('chartController', widgetController);

        widgetController.initializeWidget({wn: controllerString});

        var route = this.container.lookup('route:application');

        route.render(routeString, {
            into: 'price/widgets/fair-value/fair-value-recommendation',
            outlet: 'chartOutlet',
            controller: widgetController
        });
    },

    calculateUpPotentialTextColor: function () {
        Ember.run.once(this, this._calculateUpPotentialTextColor);
    }.observes('fairValueArray.@each.upsidePotential'),

    calculateMedianUpsidePotential: function () {
        var ltp = this.get('stock.ltp');
        var medianUpPer = ltp !== 0 ? (this.get('medianTargetPrice') - ltp) / ltp * 100 : undefined;

        this.set('medianUpsidePotential', medianUpPer);

        if (medianUpPer > 0) {
            this.set('upPotentialColor', 'up-fore-color');
        } else if (medianUpPer < 0) {
            this.set('upPotentialColor', 'down-fore-color');
        } else {
            this.set('upPotentialColor', 'fore-color');
        }

        var chartController = this.get('chartController');

        if (!this.get('isMedianTextAppended') && medianUpPer !== undefined && chartController) {
            chartController.set('medianUpsidePotential', medianUpPer);
            chartController.appendUpsidePotentialText(undefined, true);

            this.set('isMedianTextAppended', true);
        }
    }.observes('stock.ltp'),

    calculateMaxUpsidePotential: function () {
        var ltp = this.get('stock.ltp');
        var maxUpPer = ltp !== 0 ? (this.get('maxTargetPrice') - ltp) / ltp * 100 : undefined;

        this.set('maxUpsidePotential', maxUpPer);

        var chartController = this.get('chartController');

        if (!this.get('isMaxTextAppended') && maxUpPer !== undefined && chartController) {
            chartController.set('maxUpsidePotential', maxUpPer);
            chartController.appendUpsidePotentialText(true);

            this.set('isMaxTextAppended', true);
        }
    }.observes('stock.ltp'),

    calculateMinUpsidePotential: function () {
        var ltp = this.get('stock.ltp');
        var minUpPer = ltp !== 0 ? (this.get('minTargetPrice') - ltp) / ltp * 100 : undefined;

        this.set('minUpsidePotential', minUpPer);

        var chartController = this.get('chartController');

        if (!this.get('isMinTextAppended') && minUpPer !== undefined && chartController) {
            chartController.set('minUpsidePotential', minUpPer);
            chartController.appendUpsidePotentialText(undefined, undefined, true);

            this.set('isMinTextAppended', true);
        }
    }.observes('stock.ltp'),

    _calculateFvMedian: function () {
        var fairValueArrayCount = this.get('fairValueArray.length');

        if (fairValueArrayCount > 0) {
            var sortedFairValueArray = this.get('sortedFairValueArray');
            var median = (sortedFairValueArray[(sortedFairValueArray.length - 1) >> 1].fv + sortedFairValueArray[sortedFairValueArray.length >> 1].fv) / 2;

            this.set('medianTargetPrice', median);
            this.set('minTargetPrice', sortedFairValueArray[0].fv);
            this.set('maxTargetPrice', sortedFairValueArray[fairValueArrayCount - 1].fv);

            this._calculateConsensus(sortedFairValueArray, fairValueArrayCount);
        } else {
            this.set('medianTargetPrice', undefined);
            this.set('minTargetPrice', undefined);
            this.set('maxTargetPrice', undefined);

            this.set('consensusCount', 0);
            this.set('consensus', 0);
        }

        this._setConsensusPosition();
    },

    _calculateConsensus: function (sortedFairValueArray, fairValueArrayCount) {
        var consensus = 0;

        sortedFairValueArray.forEach(function (fvObj) {
            consensus += fvObj.ratingScore;
        });

        this.set('consensusCount', fairValueArrayCount);
        this.set('consensus', consensus / fairValueArrayCount);
    },

    _calculateUpPotentialTextColor: function () {
        var fairValueArrayCount = this.get('fairValueArray.length');

        if (fairValueArrayCount > 0) {
            var fairValueArray = this.get('fairValueArray');

            fairValueArray.forEach(function (fvObj) {
                var ltp = fvObj.get('stock.ltp');
                var upPer = ltp !== 0 ? (fvObj.fv - ltp) / ltp * 100 : 0;

                if (upPer > 0) {
                    fvObj.set('upPotentialTextColor', 'up-fore-color');
                } else if (upPer < 0) {
                    fvObj.set('upPotentialTextColor', 'down-fore-color');
                } else {
                    fvObj.set('upPotentialTextColor', 'fore-color');
                }
            });
        }
    },

    _setConsensusPosition: function () {
        var consensus = this.get('consensus');

        this.set('consensusPositionSell', false);
        this.set('consensusPosition1', false);
        this.set('consensusPosition13', false);
        this.set('consensusPosition16', false);
        this.set('consensusPosition2', false);
        this.set('consensusPosition225', false);
        this.set('consensusPosition25', false);
        this.set('consensusPosition275', false);
        this.set('consensusPosition3', false);
        this.set('consensusPosition325', false);
        this.set('consensusPosition35', false);
        this.set('consensusPosition375', false);
        this.set('consensusPosition4', false);
        this.set('consensusPosition43', false);
        this.set('consensusPosition5', false);
        this.set('consensusPositionBuy', false);

        if (consensus === 1) {
            this.set('consensusPositionSell', true);
        } else if (consensus > 1 && consensus < 1.3) {
            this.set('consensusPosition1', true);
        } else if (consensus >= 1.3 && consensus < 1.6) {
            this.set('consensusPosition13', true);
        } else if (consensus >= 1.6 && consensus < 2) {
            this.set('consensusPosition16', true);
        } else if (consensus >= 2 && consensus < 2.25) {
            this.set('consensusPosition2', true);
        } else if (consensus >= 2.25 && consensus < 2.5) {
            this.set('consensusPosition225', true);
        } else if (consensus >= 2.5 && consensus < 2.75) {
            this.set('consensusPosition25', true);
        } else if (consensus >= 2.75 && consensus <= 3) {
            this.set('consensusPosition275', true);
        } else if (consensus > 3 && consensus <= 3.25) {
            this.set('consensusPosition3', true);
        } else if (consensus >= 3.25 && consensus <= 3.5) {
            this.set('consensusPosition325', true);
        } else if (consensus >= 3.5 && consensus <= 3.75) {
            this.set('consensusPosition35', true);
        } else if (consensus >= 3.75 && consensus <= 4) {
            this.set('consensusPosition375', true);
        } else if (consensus >= 4 && consensus <= 4.3) {
            this.set('consensusPosition4', true);
        } else if (consensus >= 4.3 && consensus <= 4.6) {
            this.set('consensusPosition43', true);
        } else if (consensus >= 4.6 && consensus < 5) {
            this.set('consensusPosition5', true);
        } else if (consensus === 5) {
            this.set('consensusPositionBuy', true);
        }
    },

    _setLanguageFields: function (langObj) {
        this.fieldList.forEach(function (item) {
            Ember.set(item, 'columnName', langObj[item.field]);
        });
    },

    showReport: function (reportUrl) {
        this.utils.logger.logDebug('Report URL : ' + reportUrl); // Report is not opening in pro 11
        window.open(reportUrl, 'ReportWindow');
    },

    actions: {
        openReport: function (docId) {
            this.priceService.sendFairValueReportRequest(docId, function (reportGuId) {
                this.priceService.sendFairValueReportDownloadRequest(reportGuId);
            });
        }
    }
});

Ember.Handlebars.helper('recBackgroundColor', function (fairValueObj, isCellHighlight) {
    if (isCellHighlight) {
        switch (fairValueObj.ratingScore) {
            case 5:
                return 'green-back-color-1 appttl-fore-color';

            case 4:
                return 'green-back-color-5 appttl-fore-color';

            case 3:
                return 'yellow-back-color-2 fore-color';

            case 2:
                return 'red-back-color-2 appttl-fore-color';

            case 1:
                return 'red-back-color-5 appttl-fore-color';
        }
    }
});

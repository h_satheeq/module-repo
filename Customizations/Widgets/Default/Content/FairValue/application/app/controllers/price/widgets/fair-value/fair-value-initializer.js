import BaseModuleInitializer from '../../../../models/shared/initializers/base-module-initializer';
import sharedService from '../../../../models/shared/shared-service';
import FairValueDataStore from '../../../../models/price/data-stores/fair-value-data-store';

export default BaseModuleInitializer.extend({
    postInitialize: function () {
        var priceService = sharedService.getService('price');
        priceService.set('fairValueDS', FairValueDataStore.create({priceService: priceService}));
    }
});

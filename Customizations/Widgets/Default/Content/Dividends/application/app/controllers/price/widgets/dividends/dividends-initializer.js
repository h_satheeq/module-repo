import BaseModuleInitializer from '../../../../models/shared/initializers/base-module-initializer';
import sharedService from '../../../../models/shared/shared-service';
import CorporateActionDataStore from '../../../../models/price/data-stores/corporate-action-data-store';

export default BaseModuleInitializer.extend({
    postInitialize: function () {
        var priceService = sharedService.getService('price');
        priceService.set('corporateActionDS', CorporateActionDataStore.create({priceService: priceService}));
    }
});

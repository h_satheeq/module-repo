/* global STXChart */
/* global STX */
/* global $$$ */
/* global c3 */

import Ember from 'ember';
import utils from '../../../../utils/utils';
import sharedService from '../../../../models/shared/shared-service';
import ChartConstants from '../../../../models/chart/chart-constants';
import graphDM from '../../../../models/chart/data-stores/graph-data-manager';
import appEvents from '../../../../app-events';
import ControllerFactory from '../../../../controllers/controller-factory';
import BaseArrayController from '../../../base-array-controller';
import LanguageDataStore from '../../../../models/shared/language/language-data-store';

export default BaseArrayController.extend({
    DefaultDecimalPlaces: 2,
    MaxDecimalPlaces: 3,
    DefDecimalYieldTolerance: 0.02,

    searchKey: '',
    title: '',
    dividendsActionArry: undefined,
    nonDividendsActionArry: undefined,
    lastDivdn: {date: 'N/A', value: 'N/A'},
    nextDivdn: {date: 'N/A', value: 'N/A'},
    datesObj: Ember.A([]),
    graphDataManager: undefined,
    stock: {},
    app: LanguageDataStore.getLanguageObj(),
    selectedDuration: '3',

    DividendsChartPeriodTab: [
        {activeClass: 'active', desc: '3', displayDesc: '3Y'},
        {activeClass: '', desc: '5', displayDesc: '5Y'},
        {activeClass: '', desc: '0', displayDesc: 'All'}
    ],

    ChartDurations: {
        threeYears: '3',
        fiveYears: '5',
        All: '0'
    },

    DividendsChartPeriods: {
        OneDay: false,
        TwoDay: false,
        FiveDay: false,
        OneMonth: false,
        ThreeMonth: false,
        SixMonth: false,
        YTD: false,
        OneYear: false,
        TwoYear: false,
        ThreeYear: true,
        FiveYear: true,
        TenYear: false,
        All: true,
        Custom: false
    },

    ChartStyles: {
        stepChartColor: '#3da7e2'
    },

    divAmountChartContainer: '',
    divYieldChartContainer: '',
    divAmountChart: undefined,
    divYieldChart: undefined,
    histogramRenderer: undefined,
    lineRenderer: undefined,

    arrDisplayPeriods: function () {
        var arrTabs = Ember.A();
        var langObj = this.get('app').lang.labels;

        Ember.$.each(this.DividendsChartPeriods, function (key, value) {
            if (value) {
                var regChartPeriod = ChartConstants.ChartViewPeriod[key];
                Ember.set(regChartPeriod, 'chartperiodTitle', langObj[regChartPeriod.title]);
                arrTabs.pushObject(regChartPeriod);
            }
        });

        Ember.set(arrTabs[0], 'css', 'active');

        return arrTabs;
    }.property(),

    onLoadWidget: function () {
        this.set('selectedLink', 1);
        this.set('date', utils.formatters.convertToDisplayTimeFormat(new Date(), 'YYYYMMDD'));
        this.set('divAmountChartContainer', ['divAmountChart', this.get('wkey')].join(this.utils.Constants.StringConst.Hyphen));

        appEvents.subscribeSymbolChanged(this.get('wkey'), this, this.get('selectedLink'));
    },

    onPrepareData: function () {
        var stock = sharedService.getService('price').stockDS.getStock(this.get('exg'), this.get('sym'));

        this.set('inst', stock.inst);
        this.set('title', stock.lDes);
        this.set('stock', stock);
        this.set('lastDivdn', {date: 'N/A', value: 'N/A'});
        this.set('nextDivdn', {date: 'N/A', value: 'N/A'});

        this.prepareOhlcData();
    },

    onAddSubscription: function () {
        sharedService.getService('price').addSymbolRequest(this.get('exg'), this.get('sym'), this.get('inst'));
    },

    onRemoveSubscription: function () {
        sharedService.getService('price').removeSymbolRequest(this.get('exg'), this.get('sym'), this.get('inst'));
    },

    onAfterRender: function () {
        this._loadHistoricalPriceChart();

        if (this.get('regularChartController')) {
            this.get('regularChartController')._chartTypeSelected(ChartConstants.ChartViewPeriod.ThreeYear);
        }

        var divAmountChart = new STXChart({
            container: $$$([this.utils.Constants.StringConst.Dot, this.get('divAmountChartContainer')].join(''))});
        divAmountChart.chart.xAxis.displayGridLines = false;
        divAmountChart.chart.yAxis.displayGridLines = false;
        divAmountChart.chart.yAxis.decimalPlaces = this.DefaultDecimalPlaces;
        divAmountChart.controls.chartControls = null;
        divAmountChart.controls.home = null;
        divAmountChart.layout.crosshair = true;

        new STX.Tooltip({stx: divAmountChart, ohl: true, volume: true, series: false, studies: false}); // eslint-disable-line

        this.set('divAmountChart', divAmountChart);
    },

    prepareOhlcData: function () {
        var that = this;

        var gDM = graphDM.create({
            chartCategory: ChartConstants.ChartCategory.History,
            chartDataLevel: ChartConstants.ChartViewPeriod.All.ChartDataLevel,
            chartViewPeriod: ChartConstants.ChartViewPeriod.All,
            wKey: that.get('wkey'),

            onDataChunk: function () {
                that.onSuccessChartRequest();
            }
        });

        this.set('graphDataManager', gDM);
        gDM.addChartSymbol(that.get('exg'), that.get('sym'), true);

        this.get('graphDataManager').addChartDataSubscription(undefined, 20);
    },

    onSuccessChartRequest: function () {
        var that = this;
        var timePeriod = '';
        var duration = this.get('selectedDuration');

        this.set('chartDataArray', that.get('graphDataManager').getDataArray());

        if (duration !== this.ChartDurations.All) {
            timePeriod = utils.formatters.generateHistoryBeginDateString(duration, 0);
        }

        if (this.get('regularChartController')) {
            var regularChartController = this.get('regularChartController');

            if (duration === this.ChartDurations.threeYears && regularChartController) {
                regularChartController._chartTypeSelected(ChartConstants.ChartViewPeriod.ThreeYear);
            } else if (duration === this.ChartDurations.fiveYears) {
                regularChartController._chartTypeSelected(ChartConstants.ChartViewPeriod.FiveYear);
            } else {
                regularChartController._chartTypeSelected(ChartConstants.ChartViewPeriod.All);
            }
        }

        sharedService.getService('price').corporateActionDS.getCorporateActionCollection(this.get('exg'), this.get('sym'),
            function (allCorporateActions) {
                that.dividendsActionSuccessFn(allCorporateActions);
            }, timePeriod
        );
    },

    dividendsActionSuccessFn: function (allCorporateActions) {
        if (allCorporateActions && allCorporateActions.length > 0) {
            this.getOhlcDataSet(this.get('chartDataArray'), allCorporateActions);
            this.filterDividendsAction(this.get('allCorporateActions'));
            this._drawDivAmountChart(this.get('dividendsActionArry'));
            this._drawDivYieldsChart(this.get('dividendsActionArry'));
        }
    },

    _loadHistoricalPriceChart: function () {
        var controllerString = 'controller:chart/regular-chart';
        var routeString = 'chart/regular-chart';
        var route = this.container.lookup('route:application');
        var widgetController = ControllerFactory.createController(this.container, controllerString);

        widgetController.set('sym', this.get('sym'));
        widgetController.set('exg', this.get('exg'));
        widgetController.set('isTitleDisabled', true);
        widgetController.set('isShowTitle', false);
        widgetController.set('showVolumeChart', false);
        widgetController.set('isDisableChartControls', true);
        widgetController.set('arrDisplayPeriods', this.get('arrDisplayPeriods'));
        widgetController.initializeWidget({wn: controllerString}, {widgetArgs: {selectedLink: this.get('selectedLink')}});

        route.render(routeString, {
            into: 'price/widgets/dividends/dividends-action',
            outlet: 'chartOutlet',
            controller: widgetController
        });

        // TODO : [Chamalee] Check reason for not coming widget controller instance through lookup
        this.set('regularChartController', widgetController);
    },

    filterDividendsAction: function (corpActions) {
        var that = this;
        var dividendsActionArry = [];
        var nonDividendsActionArry = [];
        var lastDiv;
        var nextDiv;

        Ember.$.each(corpActions, function (key, val) {
            if (val.actionType === '7') {
                var mktdate = that.get('date');
                var dateDiff = mktdate - val.exdvDate;

                dividendsActionArry[dividendsActionArry.length] = val;

                if (dateDiff > 0) {
                    if (lastDiv) {
                        if (dateDiff < lastDiv) {
                            lastDiv = dateDiff;
                            that.set('lastDivdn.date', utils.formatters.formatToDate(val.exdvDate));
                            that.set('lastDivdn.value', val.dividendAmount);
                        }
                    } else {
                        lastDiv = dateDiff;
                        that.set('lastDivdn.date', utils.formatters.formatToDate(val.exdvDate));
                        that.set('lastDivdn.value', val.dividendAmount);
                    }
                } else {
                    if (nextDiv) {
                        if (dateDiff > nextDiv) {
                            nextDiv = dateDiff;
                            that.set('nextDivdn.date', utils.formatters.formatToDate(val.exdvDate));
                            that.set('nextDivdn.value', val.dividendAmount);
                        }
                    } else {
                        nextDiv = dateDiff;
                        that.set('nextDivdn.date', utils.formatters.formatToDate(val.exdvDate));
                        that.set('nextDivdn.value', val.dividendAmount);
                    }
                }
            } else {
                nonDividendsActionArry[nonDividendsActionArry.length] = val;
            }
        });

        this.set('dividendsActionArry', Ember.A(dividendsActionArry));
        this.set('nonDividendsActionArry', Ember.A(nonDividendsActionArry));
    },

    onUnloadWidget: function () {
        this.set('dividendsActionArry', undefined);
        this.set('nonDividendsActionArry', undefined);
        this.set('lastDivdn', undefined);
        this.set('nextDivdn', undefined);
        this.set('lineRenderer', undefined);
        this.set('histogramRenderer', undefined);
        this.set('divYieldChart', undefined);
        this.set('divAmountChart', undefined);
        this.set('stock', undefined);
    },

    getOhlcDataSet: function (ohlcSeries, corporateActions) {
        var allCorporateActions = corporateActions;
        var y = ohlcSeries.length ? ohlcSeries.length - 1 : -1;

        for (var x = 0; x < allCorporateActions.length; x++) {
            var divdDate = allCorporateActions[x].exDividendDateObj;

            for (; y >= 0; y--) {
                var historyDate = ohlcSeries[y].DT;
                var arry = Ember.A([]);

                if (divdDate < historyDate) {
                    if ((historyDate.getDate() === divdDate.getDate()) && (historyDate.getMonth() === divdDate.getMonth()) && (historyDate.getFullYear() === divdDate.getFullYear())) {
                        arry[0] = ohlcSeries[y];
                        arry[1] = ohlcSeries[y + 1];

                        this._setPriceDropAndDividendYield(arry, allCorporateActions[x]);
                        break;
                    }
                } else {
                    arry[0] = ohlcSeries[y];
                    arry[1] = ohlcSeries[y + 1];

                    this._setPriceDropAndDividendYield(arry, allCorporateActions[x]);
                    break;
                }
            }
        }

        this.set('allCorporateActions', allCorporateActions);
    },

    _setPriceDropAndDividendYield: function (arry, corporateAction) {
        var prcDrp = arry[0].Close - arry[1] ? arry[1].Open : arry[0].Close;
        var prcDrpCss = prcDrp === 0 ? 'fore-color' : prcDrp > 0 ? 'up-fore-color' : 'down-fore-color';

        corporateAction.setProperties({
            prcDrp: prcDrp,
            prvClose: arry[0].Close,
            nextOpen: arry[1] ? arry[1].Open : 0,
            openDate: arry[1] ? utils.formatters.convertToDisplayTimeFormat(arry[1].DT, 'YYYY-MM-DD') : '',
            prcDrpCss: prcDrpCss
        });

        if (corporateAction.actionType === '7') {
            var dividendYield = corporateAction.dividendAmount / arry[0].Close;
            dividendYield = utils.formatters.formatNumber(dividendYield, 3);
            var dividendYieldCss = dividendYield === 0 ? 'fore-color' : dividendYield > 0 ? 'up-fore-color' : 'down-fore-color';
            corporateAction.setProperties({dividendYield: dividendYield, dividendYieldCss: dividendYieldCss});
        }
    },

    onFinishedLoadingNewChart: function (stxChart) {
        return function (error) {
            if (!error) {
                if (stxChart.masterData && stxChart.masterData.length > 0) {
                    stxChart.setRange({
                        dtLeft: stxChart.masterData[0].DT, // Set this to the date to appear on the left edge of chart
                        dtRight: stxChart.masterData[stxChart.masterData.length - 1].DT,  // Set this to the date to appear on the right edge of chart
                        padding: 1 // Set this to the number of pixels of padding to leave between the chart and y-axis
                    });

                    stxChart.minimumLeftBars = stxChart.masterData.length;

                    if (stxChart.masterData.length > 2) {
                        // This is to force the chart to fit screen
                        stxChart.setCandleWidth(stxChart.layout.candleWidth * 0.9);
                    }
                }

                stxChart.chart.panel.yAxis.drawCurrentPriceLabel = false;
                stxChart.chart.panel.yAxis.drawPriceLabels = false;
                stxChart.chart.panel.yAxis.noDraw = true;

                stxChart.draw();
            }
        };
    },

    getMinMax: function (dataArray) {
        var max = 0;

        Ember.$.each(dataArray, function (key, val) {
            if (val && val.Close > max) {
                max = val.Close;
            }
        });

        return {min: 0, max: max};
    },

    _drawDivAmountChart: function (divAmountArray) {
        var that = this;
        var histogramRenderer = this.get('histogramRenderer');
        var divAmountChart = this.get('divAmountChart');

        // Clear existing series
        if (histogramRenderer && divAmountChart) {
            histogramRenderer.removeAllSeries(true);
            divAmountChart.removeSeriesRenderer(histogramRenderer);

            this.set('histogramRenderer', undefined);
        }

        // TODO: [satheeqh] Convert all divAmount to float in response handler

        if (divAmountChart && divAmountArray && divAmountArray.length > 0) {
            var seriesData = [];

            Ember.$.each(divAmountArray, function (key, val) {
                seriesData.unshiftObject({Date: val.exDividendDateObj, Close: parseFloat(val.dividendAmount)});
            });

            var axisMinMax = this.getMinMax(seriesData);
            divAmountChart.chart.yAxis.max = axisMinMax.max;
            divAmountChart.chart.yAxis.min = axisMinMax.min;

            var widthFactor = 0.7;

            if (seriesData.length < 3) {
                widthFactor = 0.2;
            }

            if (divAmountChart.chart && divAmountChart.chart.xAxis) {
                if (seriesData.length < 2) {
                    divAmountChart.chart.xAxis.formatter = function (labelDate) {
                        return that.utils.moment(labelDate).format('MM-DD-YYYY');
                    };
                } else {
                    divAmountChart.chart.xAxis.formatter = null;
                }
            }

            divAmountChart.newChart(this.get('sym'), seriesData, null, this.onFinishedLoadingNewChart(divAmountChart));

            var axis = new STXChart.YAxis();
            axis.position = 'left';
            axis.decimalPlaces = this.DefaultDecimalPlaces;
            axis.maxDecimalPlaces = this.DefaultDecimalPlaces;
            axis.drawCurrentPriceLabel = false;
            axis.drawPriceLabels = false;

            histogramRenderer = divAmountChart.setSeriesRenderer(new STX.Renderer.Histogram({
                params: {
                    yAxis: axis,
                    name: 'histogram',
                    type: 'histogram',
                    subtype: 'stacked',
                    bindToYAxis: true,
                    widthFactor: widthFactor
                }
            }));

            divAmountChart.addSeries('DividendAmount', {data: seriesData});
            histogramRenderer.removeAllSeries().attachSeries('DividendAmount', {
                color: '#007AFF',
                permanent: true
            }).ready();

            this.set('histogramRenderer', histogramRenderer);
        }
    },

    _drawDivYieldsChart: function (dividendArry) {
        var chartData = dividendArry;
        var that = this;

        if (chartData && chartData.length > 0) {
            var currency = dividendArry[0].curr;

            this.dividendsYieldsChart = c3.generate({
                data: {
                    json: chartData,

                    keys: {
                        x: 'exdvDate',
                        value: ['dividendYield', 'dummyDataPoint'] // Added 'dummyDataPoint' property to shift x-axis ticks to fit for the bar chart alignments. Ref. Issue:https://github.com/c3js/c3/issues/566
                    },

                    xFormat: '%Y%m%d',

                    types: {
                        dividendYield: 'step',
                        dummyDataPoint: 'bar'
                    },

                    colors: {
                        dividendYield: this.ChartStyles.stepChartColor
                    }
                },

                line: {
                    step: {
                        type: 'step-after'
                    }
                },

                area: {
                    zerobased: false
                },

                padding: {
                    top: 10,
                    right: 15,
                    bottom: 10,
                    left: 40
                },

                bindto: '#chartDivYld',

                axis: {
                    x: {
                        show: true,
                        type: 'timeseries',

                        tick: {
                            culling: {
                                max: 6 // The number of tick texts will be adjusted to less than this value
                            },

                            format: '%Y'
                        }
                    },

                    y: {
                        show: true,
                        min: 0,

                        padding: {
                            top: 0, bottom: 0
                        },

                        tick: {
                            count: 5,

                            format: function (d) {
                                return utils.formatters.formatNumber(d, 2);
                            }
                        }
                    }
                },

                legend: {
                    show: false
                },

                tooltip: {
                    show: true,

                    contents: function (d) {
                        var exDivDate = d[0].x.toLocaleDateString();
                        var dateParts = exDivDate.trim().split(utils.Constants.StringConst.Slash);
                        var toolTipArray = [];

                        exDivDate = dateParts[2] + '-' + that._fillDateString(dateParts[0]) + '-' + that._fillDateString(dateParts[1]);

                        toolTipArray[toolTipArray.length] = '<table style="border:1px solid black;background-color:#e8edf1"><tr><td class="font-m pad-m-lr pad-m-tb">';
                        toolTipArray[toolTipArray.length] = that.app.lang.labels.dividendYield;
                        toolTipArray[toolTipArray.length] = '</td><td class="font-m bold pad-m-lr pad-m-tb">';
                        toolTipArray[toolTipArray.length] = utils.formatters.formatNumber(d[0].value, 2);
                        toolTipArray[toolTipArray.length] = '</td></tr><tr><td class="font-m pad-m-lr">';
                        toolTipArray[toolTipArray.length] = that.app.lang.labels.exdvDate;
                        toolTipArray[toolTipArray.length] = '</td><td class="font-m bold pad-m-lr">';
                        toolTipArray[toolTipArray.length] = exDivDate;
                        toolTipArray[toolTipArray.length] = '</td></tr><tr><td class="font-m pad-m-lr pad-m-tb">';
                        toolTipArray[toolTipArray.length] = that.app.lang.labels.curr;
                        toolTipArray[toolTipArray.length] = '</td><td class="font-m bold pad-m-lr pad-m-tb">';
                        toolTipArray[toolTipArray.length] = currency;
                        toolTipArray[toolTipArray.length] = '</td></tr></table>';

                        return toolTipArray.join('');
                    }
                }
            });
        }

        if (this.dividendsYieldsChart) {
            this.dividendsYieldsChart.hide(['dummyDataPoint']);
        }
    },

    _fillDateString: function (value) {
        return (value < 10 ? '0' : '') + value;
    },

    actions: {
        onTabSelectionChanged: function (option) {
            var duration = option.desc;
            this.set('selectedDuration', duration);

            this.onSuccessChartRequest();
        }
    }
});

import Ember from 'ember';
import corporateAction from '../business-entities/corporate-action';

export default Ember.Object.extend({
    store: {},
    corporateActionMapBySymExg: {},

    getCorporateAction: function (actionId) {
        var currentStore = this.get('store');
        var corporateActionObj = currentStore[actionId];

        if (!corporateActionObj) {
            corporateActionObj = corporateAction.create({
                id: actionId
            });

            currentStore[actionId] = corporateActionObj;
        }

        return corporateActionObj;
    },

    getCorporateActionCollection: function (exchange, symbol, callbackFn, startDate) {
        this.priceService.sendCorporateActionRequest(exchange, symbol, callbackFn, startDate);
    }
});

import Ember from 'ember';
import utils from '../../../utils/utils';

export default Ember.Object.extend({
    sym: '',               // Symbol
    chg: '',               // Change
    annDate: '',           // Announcing Date
    exdvDate: '',          // Ex-Dividend Date
    exDividendDateObj: '', // Ex-Dividend Date in Milliseconds
    pmntDate: '',          // Payment Date
    curr: '',              // Currency
    dividendAmount: '',        // Dividend Amount
    actionType: '',        // Action Type
    actionNameUni: '',     // Action Name in unicode
    spltFctr: '',          // Split Factor

    actionName: function () {
        return utils.formatters.convertUnicodeToNativeString(this.get('actionNameUni'));
    }.property('actionName'),

    setData: function (corporateActionMsg) {
        var that = this;

        Ember.$.each(corporateActionMsg, function (key, value) {
            that.set(key, value);
        });
    }
});
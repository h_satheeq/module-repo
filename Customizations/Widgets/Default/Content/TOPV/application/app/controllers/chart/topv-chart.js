/* global $$$ */

import Ember from 'ember';
import chartConstants from '../../models/chart/chart-constants';
import ProChart from './pro-chart';
import appEvents from '../../app-events';
import TopvGraphDM from '../../models/chart/data-stores/topv-graph-data-manager';
import utils from '../../utils/utils';

export default ProChart.extend({
    chartStyle: chartConstants.ChartStyle.Line,
    chartViewPeriod: chartConstants.ChartViewPeriod.OneDay,
    chartCategory: chartConstants.ChartCategory.Intraday,

    onChartData: undefined,

    date: {},

    onLoadWidget: function () {
        var that = this;
        this.refineChartCategory();

        that.set('graphDataManager', TopvGraphDM.create({
            chartCategory: that.chartCategory,
            chartDataLevel: that.chartViewPeriod.ChartDataLevel,
            chartViewPeriod: that.chartViewPeriod,
            wKey: that.get('wkey'),
            date: that.get('date'),

            onData: function (ohlcPoint, exg, sym) {
                that.onDataFromRealtime(ohlcPoint, exg, sym);
            },

            onDataChunk: function (chartSymbolObj) {
                that.onDataFromMix(chartSymbolObj);

                if (that.onChartData && Ember.$.isFunction(that.onChartData)) {
                    that.onChartData(that.get('chartCategory'), chartSymbolObj);
                }
            },

            onErrorFn: function (e) {
                that.set('graphDataManager.isLoading', false);
                utils.logger.logError('Error in theoretical chart data loading : ' + e);
            }
        }));

        appEvents.subscribeSymbolChanged(this.get('wkey'), this, this.get('selectedLink'));
    },

    // Overwriting base class to prepare data after initializing chart
    onPrepareData: function () {
        if (this.get('baseChart')) {
            this.prepareChartData();
        }
    },

    onAfterRender: function () {
        // All pro chart instances shared the same dom container since both use same class to fetch container.
        // It causes second loaded chart is painted on first loaded chart.
        this.initChart($$$(['.', 'pro-chart-container', this.get('wkey')].join('')));

        // Schedule prepare chart data after initializing chart
        this.prepareChartData();
    },

    onDateChanged: function (date) {
        this.set('date', date);
        this.refineChartCategory();

        var graphDataManager = this.get('graphDataManager');

        if (graphDataManager) {
            graphDataManager.set('date', date);

            graphDataManager.refineGraphData({
                chartCategory: this.get('chartCategory'),
                chartDataLevel: this.get('chartViewPeriod').ChartDataLevel,
                chartViewPeriod: this.get('chartViewPeriod')
            });
        }
    },

    // Chart Base overrides
    onDataFromMix: function (chartSymbolObj) {
        var baseChart = this.get('baseChart');

        if (baseChart) {
            if (chartSymbolObj && chartSymbolObj.chartPointArray && chartSymbolObj.chartPointArray.length === 0) {
                baseChart.setMasterData(chartSymbolObj.chartPointArray);

                if (baseChart.chart && baseChart.chart.xaxis) {
                    baseChart.chart.xaxis = [];
                }
            }

            baseChart.newChart(this.get('sym'), null, null, this.onFinishedLoadingNewChart(baseChart.chart.symbol));
        }
    },

    refineChartCategory: function () {
        if (this.isToday(this.get('date'))) {
            this.set('chartCategory', chartConstants.ChartCategory.Intraday);
        } else {
            this.set('chartCategory', chartConstants.ChartCategory.History);
        }
    },

    isToday: function (d1) {
        var d2 = new Date();

        return d1.setHours(0, 0, 0, 0) === d2.setHours(0, 0, 0, 0);
    }
});
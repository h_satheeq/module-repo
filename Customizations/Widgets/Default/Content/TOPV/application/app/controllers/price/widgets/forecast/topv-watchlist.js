import Ember from 'ember';
import TableController from '../../../../controllers/shared/table-controller';
import appEvents from '../../../../app-events';
import priceWidgetConfig from '../../../../config/price-widget-config';
import sharedService from '../../../../models/shared/shared-service';

// Cell Views
import ClassicHeaderCell from '../../../../views/table/classic-header-cell';
import ClassicCell from '../../../../views/table/classic-cell';
import ChangeCell from '../../../../views/table/change-cell';
import TableRow from '../../../../views/table/table-row';

export default TableController.extend({
    exg: sharedService.getService('price').exchangeDS.getExchange(sharedService.getService('price').userDS.currentExchange),
    symbolCollection: [],
    columnDeclarations: [],
    fixedColumns: priceWidgetConfig.topvWatchlist.tableParams.numOfFixedColumns,
    isShowTitle: false,
    sortProperties: ['stock.tov'],
    sortAscending: false,
    oneWayContent: Ember.computed.oneWay('arrangedContent'),

    priceService: sharedService.getService('price'),

    onLoadWidget: function () {
        appEvents.subscribeLanguageChanged(this, this.get('wkey'));
        this.setCellViewsScopeToGlobal();
        this.set('columnDeclarations', priceWidgetConfig.topvWatchlist.defaultColumns);
        this.setLangLayoutSettings(sharedService.userSettings.currentLanguage);
        this.setDefaultSort();
    },

    onPrepareData: function () {
        this.loadContent();
    },

    onAddSubscription: function () {
        this.priceService.addFullMarketSymbolRequest(this.get('exg'));
    },

    onRemoveSubscription: function () {
        this.priceService.removeFullMarketSymbolRequest(this.get('exg'));
    },

    onClearData: function () {
        this.set('content', Ember.A());
        this.set('columnDeclarations', Ember.A());
        this.set('symbolCollection', Ember.A());
    },

    onUnloadWidget: function () {
        this.set('columnDeclarations', []);
        appEvents.unSubscribeLanguageChanged(this.get('wkey'));
    },

    onLanguageChanged: function () {
        this.set('columnDeclarations', []);
        this.onLoadWidget();
    },

    loadContent: function () {
        this.set('content', this.priceService.theoreticalStockDS.getSymbolCollectionByExchange(this.get('exg')));
        this.set('symbolCollection', this.priceService.stockDS.getSymbolCollectionByExchange(this.get('exg')));
    },

    onSymbolAddition: function () {
        // This is to make sure new symbol additions are included in theoretical calculations.
        Ember.run.once(this, this.updateTheoreticalDS);
    }.observes('symbolCollection.length'),

    updateTheoreticalDS: function () {
        var symbolCollection = this.get('symbolCollection');

        if (symbolCollection && symbolCollection.length > 0) {
            Ember.$.each(symbolCollection, function (key, symbol) {
                sharedService.getService('price').theoreticalStockDS.getTheoreticalStock(symbol.get('exg'), symbol.get('sym'), symbol.get('inst'));
            });
        }
    },

    setCellViewsScopeToGlobal: function () {
        Ember.ClassicHeaderCell = ClassicHeaderCell;
        Ember.ClassicCell = ClassicCell;
        Ember.ChangeCell = ChangeCell;
        Ember.TableRow = TableRow;
    },

    cellViewsForColumns: {
        button: 'Ember.ButtonCell',
        classicCell: 'Ember.ClassicCell',
        changeCell: 'Ember.ChangeCell'
    },

    setDefaultSort: function () {
        var sortColumn = this.get('sortProperties') ? this.get('sortProperties')[0] : '';
        var sortId = this.utils.validators.isAvailable(sortColumn) ? sortColumn : 'stock.tov';
        var tableColumns = this.get('columns');

        Ember.$.each(tableColumns, function (key, column) {
            if (column.get('contentPath') === sortId) {
                Ember.set(column, 'isSorted', true);
                return false;
            }
        });

        this.set('sortProperties', [sortId]);
        this.saveWidget({sortAsc: this.get('sortAscending'), sortCols: this.get('sortProperties')});
    },

    actions: {
        sort: function (column) {
            if (!column.get('isSortSupported')) {
                return;
            }

            if (this.get('sortColumn') !== column) {
                this.get('columns').setEach('isSorted', false);
                column.set('isSorted', true);
                this.set('sortColumn', column);
                this.set('sortProperties', [column.get('sortKey')]);
                this.set('isSortApplied', true);
            } else if (this.get('sortColumn') === column) {
                // Handle disabling sorts
                if (this.get('sortAscending') === true) {
                    this.set('sortColumn', undefined);
                    this.set('sortAscending', false);
                    column.set('isSorted', false);
                    this.set('isSortApplied', false);
                    this.set('sortProperties', []);
                } else {
                    this.set('sortProperties', [column.get('sortKey')]);
                    this.toggleProperty('sortAscending');
                }
            }
        },

        setLink: function (option) {
            this.set('selectedLink', option.code);
        },

        clickRow: function () {
            // Add click event handling here
        },

        doubleClickRow: function () {
            // Add click event handling here
        }
    }
});

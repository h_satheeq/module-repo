import Ember from 'ember';
import BaseController from '../../../base-array-controller';
import sharedService from '../../../../models/shared/shared-service';
import priceConstants from '../../../../models/price/price-constants';

export default BaseController.extend({
    TopStockCount: 10,

    date: '',
    exchange: undefined,
    symbolCollection: [],
    theoStockCollection: [],

    gainersSortProperties: ['pctChg:desc'],
    descSortedSymbolCollection: Ember.computed.sort('theoStockCollection', 'gainersSortProperties'),
    topGainers: [],

    losersSortProperties: ['pctChg:acs'],
    acsSortedSymbolCollection: Ember.computed.sort('theoStockCollection', 'losersSortProperties'),
    topLosers: [],

    priceService: sharedService.getService('price'),

    onLoadWidget: function () {
        this.set('date', this.utils.formatters.convertToDisplayTimeFormat(new Date(), 'DD-MMM-YYYY'));
    },

    onPrepareData: function () {
        var exg = this.get('exg');
        this.set('exchange', this.priceService.exchangeDS.getExchange(exg));
        this.set('symbolCollection', this.priceService.stockDS.getSymbolCollectionByExchange(exg));
        this.set('theoStockCollection', this.priceService.theoreticalStockDS.getSymbolCollectionByExchange(exg));
    },

    onAddSubscription: function () {
        this.priceService.addFullMarketSymbolRequest(this.get('exg'));
        this.priceService.addExchangeRequest(this.get('exg'));
    },

    onRemoveSubscription: function () {
        this.priceService.removeFullMarketSymbolRequest(this.get('exg'));
        this.priceService.removeExchangeRequest(this.get('exg'));
    },

    onClearData: function () {
        this.set('symbolCollection', []);
        this.set('theoStockCollection', []);
        this.set('topGainers', []);
        this.set('topLosers', []);
    },

    exchangeDateChanged: function () {
        var date = this.get('exchange.date');

        if (date) {
            this.set('date', this.utils.formatters.convertToDisplayTimeFormat(this.utils.formatters.convertStringToDate(date), 'DD-MMM-YYYY'));
        }
    }.observes('exchange.date'),

    onSymbolAddition: function () {
        // This is to make sure new symbol additions are included in theoretical calculations.
        Ember.run.once(this, this.updateTheoreticalDS);
    }.observes('symbolCollection.length'),

    updateTheoreticalDS: function () {
        var symbolCollection = this.get('symbolCollection');

        if (symbolCollection && symbolCollection.length > 0) {
            Ember.$.each(symbolCollection, function (key, symbol) {
                sharedService.getService('price').theoreticalStockDS.getTheoreticalStock(symbol.get('exg'), symbol.get('sym'), symbol.get('inst'));
            });
        }
    },

    onValueUpdated: function () {
        Ember.run.debounce(this, this.sortSymbols, priceConstants.TimeIntervals.ContentSortInterval);
    }.observes('theoStockCollection.@each.pctChg'),

    sortSymbols: function () {
        var topGainSymbols = this.get('descSortedSymbolCollection').slice(0, this.TopStockCount);
        var topLossSymbols = this.get('acsSortedSymbolCollection').slice(0, this.TopStockCount);

        var topPctChg = 0,
            topGainPctChg = 0,
            topLossPctChg = 0;

        if (topGainSymbols && topGainSymbols.length > 0) {
            topGainPctChg = topGainSymbols[0].get('pctChg');
        }

        if (topLossSymbols && topLossSymbols.length > 0) {
            topLossPctChg = Math.abs(topLossSymbols[0].get('pctChg'));
        }

        topPctChg = topGainPctChg > topLossPctChg ? topGainPctChg : topLossPctChg;

        this._updateGainers(topGainSymbols, topPctChg);
        this._updateLosers(topLossSymbols, topPctChg * -1);
    },

    _updateGainers: function (topSymbols, topPctChg) {
        var that = this;

        if (topSymbols && topSymbols.length > 0) {
            var count = 0;

            Ember.$.each(topSymbols, function (key, symbol) {
                var pctChg = symbol ? symbol.get('pctChg') : 0;

                if (pctChg > 0) {
                    var progressStyle = 'width: calc(' + (pctChg / topPctChg) * 100 + '% - 40px); height: 8px;';
                    var gainObj = that.topGainers[key];

                    if (gainObj) {
                        gainObj.set('sym', symbol.get('sym'));
                        gainObj.set('sDes', symbol.get('sDes'));
                        gainObj.set('pctChg', pctChg);
                        gainObj.set('progressStyle', progressStyle);
                        gainObj.set('top', symbol.get('stock').top);
                        gainObj.set('prvsClose', symbol.get('stock').prvCls);
                    } else {
                        that.topGainers.pushObject(Ember.Object.create({
                            sym: symbol.get('sym'),
                            sDes: symbol.get('sDes'),
                            pctChg: pctChg,
                            progressStyle: progressStyle,
                            top: symbol.get('stock').top,
                            prvsClose: symbol.get('stock').prvCls
                        }));
                    }

                    count++;
                }
            });

            if (count < this.topGainers.length) {
                this.set('topGainers', this.topGainers.slice(0, count));
            }
        }
    },

    _updateLosers: function (topSymbols, topPctChg) {
        var that = this;

        if (topSymbols && topSymbols.length > 0) {
            var count = 0;

            Ember.$.each(topSymbols, function (key, symbol) {
                var pctChg = symbol ? symbol.get('pctChg') : 0;

                if (pctChg < 0) {
                    var progressStyle = 'width: calc(' + ([pctChg] / topPctChg) * 100 + '% - 40px); height: 8px; float: right;';
                    var lossObj = that.topLosers[key];

                    if (lossObj) {
                        lossObj.set('sym', symbol.get('sym'));
                        lossObj.set('sDes', symbol.get('sDes'));
                        lossObj.set('pctChg', pctChg);
                        lossObj.set('progressStyle', progressStyle);
                        lossObj.set('top', symbol.get('stock').top);
                        lossObj.set('prvsClose', symbol.get('stock').prvCls);
                    } else {
                        that.topLosers.pushObject(Ember.Object.create({
                            sym: symbol.get('sym'),
                            sDes: symbol.get('sDes'),
                            pctChg: pctChg,
                            progressStyle: progressStyle,
                            top: symbol.get('stock').top,
                            prvsClose: symbol.get('stock').prvCls
                        }));
                    }

                    count++;
                }
            });

            if (count < this.topLosers.length) {
                this.set('topLosers', this.topLosers.slice(0, count));
            }
        }
    }
});
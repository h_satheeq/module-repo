import Ember from 'ember';
import BaseArrayController from '../../../base-array-controller';
import sharedService from '../../../../models/shared/shared-service';
import priceConstants from '../../../../models/price/price-constants';

export default BaseArrayController.extend({
    QuoteSettings: {
        IntZero: 0,
        EmptyString: '',

        Styles: {
            Green: 'up-fore-color',
            Red: 'down-fore-color',
            BackGreen: 'up-back-color',
            BackRed: 'down-back-color'
        }
    },

    date: '',
    exchange: '',
    symbolCollection: [],
    content: [],
    sortProperties: ['pct5dVol'],
    sortAscending: false,
    topVolAvg: [],

    priceService: sharedService.getService('price'),

    onLoadWidget: function () {
        this.set('date', this.utils.formatters.convertToDisplayTimeFormat(new Date(), 'DD-MMM-YYYY'));
    },

    onPrepareData: function () {
        var exg = this.get('exg');
        this.set('exchange', this.priceService.exchangeDS.getExchange(exg));
        this.set('symbolCollection', this.priceService.stockDS.getSymbolCollectionByExchange(exg));
        this.set('content', this.priceService.theoreticalStockDS.getSymbolCollectionByExchange(exg));
    },

    onAddSubscription: function () {
        this.priceService.addFullMarketSymbolRequest(this.get('exg'));
        this.priceService.sendVolumeWatcherRequest(this.get('exg'));
        this.priceService.addExchangeRequest(this.get('exg'));
    },

    onRemoveSubscription: function () {
        this.priceService.removeFullMarketSymbolRequest(this.get('exg'));
        this.priceService.removeExchangeRequest(this.get('exg'));
    },

    onClearData: function () {
        this.set('symbolCollection', []);
        this.set('content', []);
        this.set('topVolAvg', []);
    },

    exchangeDateChanged: function () {
        var date = this.get('exchange.date');

        if (date) {
            this.set('date', this.utils.formatters.convertToDisplayTimeFormat(this.utils.formatters.convertStringToDate(date), 'DD-MMM-YYYY'));
        }
    }.observes('exchange.date'),

    onSymbolAddition: function () {
        // This is to make sure new symbol additions are included in theoretical calculations.
        Ember.run.once(this, this.updateTheoreticalDS);
    }.observes('symbolCollection.length'),

    updateTheoreticalDS: function () {
        var symbolCollection = this.get('symbolCollection');

        if (symbolCollection && symbolCollection.length > 0) {
            Ember.$.each(symbolCollection, function (key, symbol) {
                sharedService.getService('price').theoreticalStockDS.getTheoreticalStock(symbol.get('exg'), symbol.get('sym'), symbol.get('inst'));
            });
        }
    },

    onContentSorted: function () {
        Ember.run.debounce(this, this._calculateTopVolAvg, priceConstants.TimeIntervals.ContentSortInterval);
    }.observes('arrangedContent.@each.pct5dVol'),

    _calculateTopVolAvg: function () {
        var that = this;
        var topTheoStocks = this.get('arrangedContent').slice(0, 10);

        if (topTheoStocks && topTheoStocks.length > 0 && topTheoStocks[0].get('pct5dVol') !== 0) {
            var count = 0;
            var topPctVolAvg = topTheoStocks[0].get('pct5dVol');

            Ember.$.each(topTheoStocks, function (key, symbol) {
                if (symbol && symbol.get('stock.tov') > 0) {
                    var progressStyle = 'width: ' + (symbol.get('pct5dVol') / topPctVolAvg) * 100 + '%; height: 8px;';
                    var stockObj = that.topVolAvg[key];

                    if (stockObj) {
                        stockObj.set('theoStock', symbol);
                        stockObj.set('progressStyle', progressStyle);
                        stockObj.set('progressCss', that._getProgressCss(symbol.get('pctChg')));
                        stockObj.set('changeCss', that._getChangeCss(symbol.get('pctChg')));
                    } else {
                        that.topVolAvg.pushObject(Ember.Object.create({
                            theoStock: symbol,
                            progressStyle: progressStyle,
                            progressCss: that._getProgressCss(symbol.get('pctChg')),
                            changeCss: that._getChangeCss(symbol.get('pctChg'))
                        }));
                    }

                    count++;
                }
            });

            if (count < this.topVolAvg.length) {
                this.set('topVolAvg', this.topVolAvg.slice(0, count));
            }
        }
    },

    _getChangeCss: function (pctChg) {
        var changeCss = '';

        if (pctChg > this.QuoteSettings.IntZero) {
            changeCss = this.QuoteSettings.Styles.Green;
        } else if (pctChg < this.QuoteSettings.IntZero) {
            changeCss = this.QuoteSettings.Styles.Red;
        }

        return changeCss;
    },

    _getProgressCss: function (pctChg) {
        var progressCss = '';

        if (pctChg > this.QuoteSettings.IntZero) {
            progressCss = this.QuoteSettings.Styles.BackGreen;
        } else if (pctChg < this.QuoteSettings.IntZero) {
            progressCss = this.QuoteSettings.Styles.BackRed;
        }

        return progressCss;
    }
});
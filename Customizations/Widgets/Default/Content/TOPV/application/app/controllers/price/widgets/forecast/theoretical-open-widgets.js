import Ember from 'ember';
import BaseController from '../../../base-controller';
import ControllerFactory from '../../../../controllers/controller-factory';
import priceConstants from '../../../../models/price/price-constants';
import sharedService from '../../../../models/shared/shared-service';
import userSettings from '../../../../config/user-settings';

export default BaseController.extend({
    widgetList: [
        {id: 1, wn: 'price/widgets/forecast/topv-watchlist', desc: 'fullMarket', isActive: true},
        {id: 2, wn: 'price/widgets/forecast/topv-change', desc: 'topChange', isActive: false},
        {id: 3, wn: 'price/widgets/forecast/top-unusual-volume', desc: 'topUnusualVolume', isActive: false},
        {id: 4, wn: 'price/widgets/forecast/topv-quote', desc: 'index', isActive: false, mode: 2},
        {id: 5, wn: 'price/widgets/forecast/topv-quote', desc: 'stock', isActive: false, mode: 1}
    ],

    currentWidgetId: '',
    widgetController: undefined,
    hasTimerObserver: false,
    exchange: undefined,
    marketStatDes: '',
    timeToOpen: '',
    exgOpenTime: '',
    timeDiff: '',

    onLoadWidget: function () {
        var that = this;
        var widgetList = this.widgetList;

        if (widgetList && widgetList.length > 0) {
            Ember.$.each(widgetList, function (key, value) {
                value.des = that.app.lang.labels[value.desc];
            });

            this._onClickTab(widgetList[0].id);
        }
    },

    onPrepareData: function () {
        this.set('exchange', sharedService.getService('price').exchangeDS.getExchange(this.get('exg')));
    },

    onAddSubscription: function () {
        sharedService.getService('price').addExchangeRequest(this.get('exg'));
    },

    onRemoveSubscription: function () {
        sharedService.getService('price').removeExchangeRequest(this.get('exg'));
    },

    onClearData: function () {
        this.set('widgetController', undefined);
        this.set('hasTimerObserver', false);
        this.set('exchange', undefined);
        this.set('marketStatDes', '');
        this.set('timeToOpen', '');
        this.set('exgOpenTime', '');
    },

    marketStatChanged: function () {
        var stat = this.get('exchange.stat');
        var marketStatDes = '';

        if (stat) {
            switch (stat) {
                case priceConstants.MarketStatus.PreOpen:
                    marketStatDes = this.app.lang.labels.timeToOpen;
                    this.set('hasTimerObserver', true);
                    this.addObserver('exchange.time', this.marketTimeChanged);
                    this.timeToOpenClock();
                    break;
                case priceConstants.MarketStatus.Open:
                    marketStatDes = this.app.lang.labels.marketIsOpened;

                    if (this.get('hasTimerObserver')) {
                        this.removeObserver('exchange.time', this.marketTimeChanged);
                        this.set('hasTimerObserver', false);
                    }
                    break;
                case priceConstants.MarketStatus.PreClose:
                    marketStatDes = this.app.lang.labels.marketIsPreClosed;
                    break;
                case priceConstants.MarketStatus.Close:
                    marketStatDes = this.app.lang.labels.marketIsClosed;
                    break;
                default:
                    marketStatDes = '';
                    break;
            }
        }

        this.set('marketStatDes', marketStatDes);
    }.observes('exchange.stat'),

    marketTimeChanged: function () {
        var mktOpen = this.get('exgOpenTime');
        var mktTime = this.utils.formatters.getAdjustedDateTime(this.get('exchange.time'), this.get('exchange.tzo'));

        if (mktTime && mktOpen && mktTime <= mktOpen) {
            this.set('timeDiff', this.utils.formatters.convertToUTCDate(mktOpen - mktTime));
        } else if (this.get('hasTimerObserver')) {
            this.set('timeDiff', undefined);
            this.set('timeToOpen', userSettings.displayFormat.noValue);
        }
    },

    timeToOpenClock: function () {
        this.calculateTimeToOpen();

        if (this.get('hasTimerObserver')) {
            Ember.run.later(this, this.timeToOpenClock, 1000);
        } else {
            this.set('timeToOpen', '');
        }
    },

    calculateTimeToOpen: function () {
        var diffDate = this.get('timeDiff');

        if (diffDate) {
            diffDate.setSeconds(diffDate.getSeconds() - 1);

            this.set('timeDiff', diffDate);
            this.set('timeToOpen', this.utils.formatters.convertToDisplayTimeFormat(diffDate, 'mm:ss'));
        }
    },

    exchangeTimezoneChanged: function () {
        var openTime = this.get('exchange.openTime');

        if (openTime) {
            var openDate = new Date();
            openDate.setHours(parseInt(openTime.substr(0, 2), 10), parseInt(openTime.substr(2, 3), 10), 0, 0);
            this.set('exgOpenTime', openDate);
        }
    }.observes('exchange.openTime'),

    renderWidget: function (params) {
        this.set('currentWidgetId', params.id);

        var widgetController = this.get('widgetController');
        var controllerString = 'controller:' + params.wn;
        var routeString = params.wn;
        var widgetKey = params.desc + this.get('wkey');

        if (widgetController && Ember.$.isFunction(widgetController.closeWidget)) {
            widgetController.closeWidget();
        }

        widgetController = ControllerFactory.createController(this.container, controllerString);
        widgetController.set('sym', this.get('sym'));
        widgetController.set('exg', this.get('exg'));
        widgetController.set('inst', this.get('inst'));
        widgetController.set('wkey', widgetKey);
        widgetController.set('isShowTitle', false);
        widgetController.set('hideWidgetLink', true);

        if (params.mode) {
            var mode = params.mode;

            if (mode === priceConstants.TOPVWidgetType.Index) {
                widgetController.set('sym', sharedService.userSettings.currentIndex);
            }

            widgetController.set('mode', mode);
        }

        widgetController.initializeWidget({wn: controllerString.split('/').pop()}, {widgetArgs: {selectedLink: this.get('currentLink')}});
        var route = this.container.lookup('route:application');

        route.render(routeString, {
            into: 'price/widgets/forecast/theoretical-open-widgets',
            outlet: 'widgetOutlet',
            controller: widgetController
        });

        this.set('widgetController', widgetController);
    },

    _onClickTab: function (tabId) {
        var that = this;
        var widgetList = this.widgetList;

        if (widgetList && widgetList.length > 0 && tabId !== this.get('currentWidgetId')) {
            Ember.$.each(widgetList, function (key, value) {
                if (value.id === tabId) {
                    Ember.set(value, 'isActive', true);
                    that.renderWidget(value);
                } else {
                    Ember.set(value, 'isActive', false);
                }
            });
        }
    },

    actions: {
        onClickTab: function (tabId) {
            this._onClickTab(tabId);
        }
    }
});
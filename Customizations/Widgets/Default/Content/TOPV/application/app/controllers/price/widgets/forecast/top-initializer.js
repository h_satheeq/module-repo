import BaseModuleInitializer from '../../../../models/shared/initializers/base-module-initializer';
import sharedService from '../../../../models/shared/shared-service';
import TheoreticalStockDataStore from '../../../../models/price/data-stores/theoretical-stock-data-store';

export default BaseModuleInitializer.extend({
    postInitialize: function () {
        var priceService = sharedService.getService('price');
        priceService.set('theoreticalStockDS', TheoreticalStockDataStore.create({priceService: priceService}));
    }
});

import Ember from 'ember';
import BaseController from '../../../base-controller';
import priceConstants from '../../../../models/price/price-constants';
import sharedService from '../../../../models/shared/shared-service';
import ControllerFactory from '../../../../controllers/controller-factory';
import appEvents from '../../../../app-events';
import appConfig from '../../../../config/app-config';
import chartConstants from '../../../../models/chart/chart-constants';

export default BaseController.extend({
    DefaultDateRange: 30,

    mode: 1,
    isStockBased: true,

    symbolSearchId: '',
    searchFieldId: '',

    exchange: undefined,
    stock: undefined,
    stockSummaryObj: undefined,
    indexCollectionArray: Ember.A(),
    defaultIndexForDropdown: {},
    chartController: undefined,

    isHistory: false,
    historyTop: 0,
    historyTov: 0,

    date: '',
    toEndDate: '',
    fromDate: '',
    daysOfWeekDisabled: [5, 6], // TDWL Specific days

    excludedInstruments: [],

    priceService: sharedService.getService('price'),

    onLoadWidget: function () {
        var wkey = this.get('wkey');
        this.set('symbolSearchId', ['symbolSearch', wkey].join('-'));
        this.set('searchFieldId', ['searchField', wkey].join('-'));

        var currentDate = new Date();
        currentDate.setHours(0, 0, 0, 0);
        this.set('toEndDate', currentDate);
        this.set('date', currentDate);

        // Get from date using default search days
        var fromDate = new Date();
        fromDate.setDate(fromDate.getDate() - this.DefaultDateRange);
        this.set('fromDate', fromDate);
    },

    onPrepareData: function () {
        var exg = this.get('exg');
        var stock = this.priceService.stockDS.getStock(exg, this.get('sym'), this.get('inst'));

        if (this.get('mode') === priceConstants.TOPVWidgetType.Stock) {
            this.set('excludedInstruments', [this.utils.AssetTypes.Indices]);
        } else {
            if (!appConfig.customisation.isEmbeddedMode) {
                // This is to support both embedded and non-embedded mode
                stock = this.priceService.stockDS.getStock(sharedService.userSettings.currentExchange, sharedService.userSettings.currentIndex, this.utils.AssetTypes.Indices);
            }

            this.set('isStockBased', false);

            if (this.get('indexCollectionArray').length > 0) {
                this.set('indexCollectionArray', Ember.A());
            }

            this.get('indexCollectionArray').pushObjects(this.priceService.stockDS.getIndexCollectionByExchange(sharedService.userSettings.currentExchange));
        }

        if (exg) {
            this.set('exchange', this.priceService.exchangeDS.getExchange(exg));
        }

        this.set('stock', stock);
        this.exchangeStatChanged();
    },

    onAddSubscription: function () {
        this.addStockRequest();
        this.priceService.addExchangeRequest(this.get('exg'));
    },

    onAfterRender: function () {
        var that = this;
        // Render chart
        var controllerString = 'controller:chart/topv-chart';
        var routeString = 'chart/topv-chart';
        var widgetKey = this.get('widgetId') + this.get('wkey');
        var widgetController = ControllerFactory.createController(this.container, controllerString);

        widgetController.set('sym', this.get('stock').sym);
        widgetController.set('exg', this.get('stock').exg);
        widgetController.set('inst', this.get('stock').inst);
        widgetController.set('date', this.get('date'));
        widgetController.set('wkey', widgetKey);
        widgetController.set('isShowTitle', false);
        widgetController.set('hideWidgetLink', true);
        widgetController.set('isDisableChartControls', true);

        widgetController.set('onChartData', function (chartCategory, chartSymbolObj) {
            that.onChartHistoryDataLoaded(chartCategory, chartSymbolObj);
        });

        if (this.utils.validators.isAvailable(this.get('widgetId'))) {
            // Un-subscribe previous widget
            appEvents.unSubscribeLanguageChanged(widgetKey);
            appEvents.unSubscribeThemeChanged(widgetKey);
        }

        // Subscribe new widget
        appEvents.subscribeLanguageChanged(widgetController, widgetKey);
        appEvents.subscribeThemeChanged(widgetController, widgetKey);

        widgetController.initializeWidget({wn: controllerString.split('/').pop()}, {widgetArgs: {selectedLink: this.get('currentLink')}});
        var route = this.container.lookup('route:application');

        route.render(routeString, {
            into: 'price/widgets/forecast/topv-quote',
            outlet: 'chartOutlet',
            controller: widgetController
        });

        this.set('chartController', widgetController);
    },

    onRemoveSubscription: function () {
        this.priceService.removeExchangeRequest(this.get('exg'));
        this.removeStockRequest();
    },

    onClearData: function () {
        this.set('exchange', undefined);
        this.set('stock', undefined);
        this.set('stockSummaryObj', undefined);
        this.set('indexCollectionArray', Ember.A());
        this.set('defaultIndexForDropdown', {});

        var chartController = this.get('chartController');

        if (chartController && Ember.$.isFunction(chartController.onClearData)) {
            chartController.onClearData();
        }
    },

    onUnloadWidget: function () {
        var chartController = this.get('chartController');

        if (chartController && Ember.$.isFunction(chartController.onUnloadWidget)) {
            chartController.onUnloadWidget();
        }

        this.set('chartController', undefined);
    },

    exchangeStatChanged: function () {
        var exchange = this.get('exchange');

        if (exchange) {
            var stat = exchange.get('stat');

            if (stat === priceConstants.MarketStatus.PreOpen) {
                this.set('stockSummaryObj', this.priceService.theoreticalStockDS.getTheoreticalStock(this.get('stock.exg'), this.get('stock.sym'), this.get('stock.inst')));
            } else {
                this.set('stockSummaryObj', this.get('stock'));
            }
        }
    }.observes('exchange.stat'),

    exchangeDateChanged: function () {
        var date = this.get('exchange.date');

        if (date) {
            var exchangeDate = this.utils.formatters.convertStringToDate(date);
            this.set('date', exchangeDate);
            this.set('toEndDate', exchangeDate);

            Ember.run.once(this, this._onDateChanged);
        }
    }.observes('exchange.date'),

    addStockRequest: function () {
        if (this.get('isStockBased')) {
            this.priceService.addSymbolRequest(this.get('stock.exg'), this.get('stock.sym'), this.get('stock.inst'));
        } else {
            this.priceService.addIndexRequest(this.get('stock.exg'), this.get('stock.sym'));
        }
    },

    removeStockRequest: function () {
        if (this.get('isStockBased')) {
            this.priceService.removeSymbolRequest(this.get('stock.exg'), this.get('stock.sym'), this.get('stock.inst'));
        } else {
            this.priceService.removeIndexRequest(this.get('stock.exg'), this.get('stock.sym'));
        }
    },

    onChartHistoryDataLoaded: function (chartCategory, chartSymbolObj) {
        var top = 0;
        var tov = 0;

        if (chartCategory && chartCategory.ID === chartConstants.ChartCategory.History.ID) {
            if (chartSymbolObj && chartSymbolObj.chartPointArray && chartSymbolObj.chartPointArray.length > 0) {
                var lastPoint = chartSymbolObj.chartPointArray[chartSymbolObj.chartPointArray.length - 1];

                if (lastPoint && lastPoint.Close !== undefined) {
                    top = lastPoint.Close;
                    tov = lastPoint.Volume;
                }
            }

            this.set('isHistory', true);
        } else {
            this.set('isHistory', false);
        }

        this.set('historyTop', top);
        this.set('historyTov', tov);
    },

    _onSymbolSelected: function (stock) {
        this.removeStockRequest();
        this.set('stock', this.priceService.stockDS.getStock(stock.exg, stock.sym));
        this.addStockRequest();
        this.exchangeStatChanged();

        var chartController = this.get('chartController');

        if (chartController) {
            chartController.onWidgetKeysChange({sym: stock.sym, exg: stock.exg, inst: stock.inst});
        }

        if (stock && stock.exg) {
            this.set('exchange', this.priceService.exchangeDS.getExchange(stock.exg));
        }
    },

    _onDateChanged: function () {
        var chartController = this.get('chartController');

        if (chartController && Ember.$.isFunction(chartController.onDateChanged)) {
            chartController.onDateChanged(this.get('date'));
        }
    },

    actions: {
        showSearchPopup: function () {
            var modal = this.get(this.get('symbolSearchId'));
            modal.send('showModalPopup');
        },

        closeSearchPopup: function () {
            var modal = this.get(this.get('symbolSearchId'));
            modal.send('closeModalPopup');
            this.set('searchKey', '');
        },

        onSymbolSelected: function (stock) {
            this._onSymbolSelected(stock);
        },

        onDateChanged: function (date) {
            this.set('date', date);
            this._onDateChanged();
        }
    }
});
import Ember from 'ember';
import OhlcDS from './ohlc-data-store';
import utils from '../../../utils/utils';

export default OhlcDS.extend({
    sendIntraDayOHLCDataRequest: function (gdm, chartSymbolObj) {
        this.priceService.downloadTOPVIntradayOHLCData({
            exchange: chartSymbolObj.exg,
            symbol: chartSymbolObj.sym,
            chartDataLevel: gdm.chartDataLevel,
            begin: undefined,

            reqSuccessFn: function () {
                gdm.onDataDownloadedFromMix(gdm, chartSymbolObj);
            },

            reqFailureFn: function (e) {
                if (gdm.onErrorFn && Ember.$.isFunction(gdm.onErrorFn)) {
                    gdm.onErrorFn(e);
                }
            }
        });
    },

    sendHistoryOHLCDataRequest: function (gdm, chartSymbolObj) {
        this.priceService.downloadTOPVHistoryData({
            exchange: chartSymbolObj.exg,
            symbol: chartSymbolObj.sym,
            begin: undefined,

            reqSuccessFn: function () {
                gdm.onDataDownloadedFromMix(gdm, chartSymbolObj);
            },

            reqFailureFn: function (e) {
                if (gdm.onErrorFn && Ember.$.isFunction(gdm.onErrorFn)) {
                    gdm.onErrorFn(e);
                }

                utils.logger.logDebug('Data Unavailable For History');
            }
        });
    }
});

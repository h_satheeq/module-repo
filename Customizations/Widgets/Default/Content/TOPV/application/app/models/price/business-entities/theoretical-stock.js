import Ember from 'ember';

export default Ember.Object.extend({
    stock: {},
    sym: '',
    exg: '',

    sDes: function () {
        return this.get('stock.sDes');
    }.property('stock.sDes'),

    chg: function () {
        var top = this.get('stock.top');
        var prvCls = this.get('stock.prvCls');

        if (top > 0 && prvCls > 0) {
            return top - prvCls;
        }

        return 0;
    }.property('stock.top', 'stock.prvCls'),

    pctChg: function () {
        var top = this.get('stock.top');
        var prvCls = this.get('stock.prvCls');

        if (top > 0 && prvCls > 0) {
            return ((top - prvCls) / prvCls) * 100;
        }

        return 0;
    }.property('stock.top', 'stock.prvCls'),

    pct5dVol: function () {
        var tov = this.get('stock.tov');
        var av5d = this.get('stock.av5d');

        if (tov > 0 && av5d > 0) {
            return (tov / av5d) * 100;
        }

        return 0;
    }.property('stock.tov', 'stock.av5d')
});
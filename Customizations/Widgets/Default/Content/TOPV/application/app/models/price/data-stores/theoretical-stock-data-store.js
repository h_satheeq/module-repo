import Ember from 'ember';
import TheoreticalStock from '../business-entities/theoretical-stock';
import utils from '../../../utils/utils';

export default Ember.Object.extend({
    store: {},
    theoreticalStockByExg: {},
    theoreticalIndxByExg: {},

    getTheoreticalStock: function (exg, sym, inst) {
        var key = utils.keyGenerator.getKey(exg, sym);
        var store = this.get('store');
        var theoStock = store[key];

        if (!theoStock) {
            var stock = this.priceService.stockDS.getStock(exg, sym, inst);

            theoStock = TheoreticalStock.create({exg: exg, sym: sym, stock: stock});
            store[key] = theoStock;

            this.addToOtherCollections(exg, theoStock, inst);
        }

        return theoStock;
    },

    addToOtherCollections: function (exg, theoStock, inst) {
        var store = utils.AssetTypes.isIndices(inst) ? this.get('theoreticalIndxByExg') : this.get('theoreticalStockByExg');

        if (!store[exg]) {
            store[exg] = Ember.A();
        }

        store[exg].pushObject(theoStock);
    },

    getSymbolCollectionByExchange: function (exg) {
        var theoreticalStockByExg = this.get('theoreticalStockByExg');

        if (!theoreticalStockByExg[exg]) {
            theoreticalStockByExg[exg] = Ember.A();
        }

        return theoreticalStockByExg[exg];
    }
});
import sharedService from '../../shared/shared-service';
import ChartConstants from '../chart-constants';
import utils from '../../../utils/utils';
import GraphDM from './graph-data-manager';

export default GraphDM.extend({
    date: {},
    isLoading: false,

    addSubscription: function (chartSymbolObj) {
        if (this.get('chartCategory').ID === ChartConstants.ChartCategory.Intraday.ID) {
            var ohlcSeries = sharedService.getService('price').theoreticalChartDS.getOHLCSeries(chartSymbolObj.exg, chartSymbolObj.sym, this.get('chartCategory'));

            ohlcSeries.registerForRealtimeData(this);
            sharedService.getService('price').addTOPVIntradayChartRequest(chartSymbolObj.exg, chartSymbolObj.sym);
        }

        this.downloadGraphData(chartSymbolObj);
    },

    removeChartDataSubscription: function (symbolObj) {
        var symbolArray = this.get('chartSymbolArray');

        if (symbolArray.length === 0) {
            return;
        }

        if (symbolObj) {
            this.removeSubscription(symbolObj);
        } else {
            for (var a = 0; a < symbolArray.length; a++) {
                this.removeSubscription(symbolArray[a]);
            }
        }
    },

    removeSubscription: function (chartSymbolObj) {
        if (this.get('chartCategory').ID === ChartConstants.ChartCategory.Intraday.ID) {
            sharedService.getService('price').removeTOPVIntradayChartRequest(chartSymbolObj.exg, chartSymbolObj.sym);
        }

        // remove all stores when un-subscription is invoked
        chartSymbolObj.chartPointArray.length = 0;
        sharedService.getService('price').theoreticalChartDS.removeOHLCSeries(chartSymbolObj.exg, chartSymbolObj.sym, this.get('chartCategory'));

        sharedService.getService('price').theoreticalChartDS.unSubscribeChartDataReady(utils.keyGenerator.getKey(chartSymbolObj.exg, chartSymbolObj.sym), this.get('wKey'));
    },

    refineGraphData: function (params) {
        var symbolArray = this.get('chartSymbolArray');
        var chartSymbolObj;

        this.set('chartCategory', params.chartCategory);
        this.set('chartDataLevel', params.chartDataLevel);
        this.set('chartViewPeriod', params.chartViewPeriod);

        for (var a = 0; a < symbolArray.length; a++) {
            chartSymbolObj = symbolArray[a];

            if (params.chartCategory.ID === ChartConstants.ChartCategory.Intraday.ID) {
                sharedService.getService('price').addTOPVIntradayChartRequest(chartSymbolObj.exg, chartSymbolObj.sym);
            } else if (params.chartCategory.ID === ChartConstants.ChartCategory.History.ID) {
                sharedService.getService('price').removeTOPVIntradayChartRequest(chartSymbolObj.exg, chartSymbolObj.sym);
            }

            this.downloadGraphData(chartSymbolObj);
        }
    },

    downloadGraphData: function (chartSymbolObj) {
        var ohlcSeries = sharedService.getService('price').theoreticalChartDS.getOHLCSeries(chartSymbolObj.exg, chartSymbolObj.sym, this.chartCategory);

        if (ohlcSeries.chartDataLevel < this.get('chartDataLevel')) {
            ohlcSeries.set('chartDataLevel', this.chartDataLevel);

            if (this.chartCategory.ID === ChartConstants.ChartCategory.Intraday.ID) {
                sharedService.getService('price').theoreticalChartDS.sendIntraDayOHLCDataRequest(this, chartSymbolObj);
            } else {
                sharedService.getService('price').theoreticalChartDS.sendHistoryOHLCDataRequest(this, chartSymbolObj);
            }

            if (!this.get('isLoading')) {
                this.set('isLoading', true);
            }
        } else if (ohlcSeries && ohlcSeries.ohlcDataPoints.length > 0) {
            this.onDataDownloadedFromMix(this, chartSymbolObj);
        } else if (ohlcSeries) {
            this.onDataDownloadedFromMix(this, chartSymbolObj);

            var key = utils.keyGenerator.getKey(chartSymbolObj.exg, chartSymbolObj.sym);
            sharedService.getService('price').theoreticalChartDS.subscribeChartDataReady(this, chartSymbolObj, key, this.get('wKey'));
        }
    },

    queryData: function (chartSymbolObj) {
        try {
            if (this.get('isLoading')) {
                this.set('isLoading', false);
            }

            if (chartSymbolObj) {
                var that = this;
                var ohlcSeries = sharedService.getService('price').theoreticalChartDS.getOHLCSeries(chartSymbolObj.exg, chartSymbolObj.sym, that.chartCategory);

                if (ChartConstants.ChartViewPeriod.All.ID === that.chartViewPeriod.ID) {
                    chartSymbolObj.chartPointArray = ohlcSeries.ohlcDataPoints.slice(); // It is important to get a copy as chart IQ use the same array as its master data array
                } else if (ohlcSeries.ohlcDataPoints) {
                    var fromDate = this.get('date');
                    var toTime;

                    if (this.get('chartCategory.ID') === ChartConstants.ChartCategory.History.ID) {
                        toTime = new Date(fromDate);
                        toTime = toTime.setHours(24, 0, 0, 0);
                    }

                    chartSymbolObj.chartPointArray = ohlcSeries.queryPointsForRange(fromDate.getTime(), toTime);
                }
            }
        } catch (e) {
            utils.logger.logError('Error in querying ohlc data : ' + e);
        }
    }
});

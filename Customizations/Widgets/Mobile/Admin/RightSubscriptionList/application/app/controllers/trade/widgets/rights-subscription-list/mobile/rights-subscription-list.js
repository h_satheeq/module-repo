import Ember from 'ember';
import tradeConstants from '../../../../../models/trade/trade-constants';
import RightsSubscriptionList from '../rights-subscription-list';
import utils from '../../../../../utils/utils';

export default RightsSubscriptionList.extend({
    isNativeDevice: navigator.isNativeDevice,

    onLoadWidget: function () {
        this._super();

        var today = new Date();
        this.set('displayToDate', utils.formatters.convertToDisplayTimeFormat(today, 'DD-MM-YYYY'));

        // Get from date using default search days
        var fromDate = new Date();

        fromDate.setDate(fromDate.getDate() - this.DefaultSearchDays);
        this.set('displayFromDate', utils.formatters.convertToDisplayTimeFormat(fromDate, 'DD-MM-YYYY'));
    },

    sendRequest: function () {
        this._sendSearchRequest();
    }.observes('fromDate', 'toDate'),

    actions: {
        pickDate: function (isFromDate) {
            var that = this;
            var today = new Date();
            var fromDate = this.get('fromDate');
            var toDate = this.get('toDate');
            var fromDateOptions = {date: fromDate, mode: 'date', maxDate: toDate.getTime()};
            var toDateOptions = {date: toDate, mode: 'date', maxDate: today.getTime(), minDate: fromDate.getTime()};
            var options = isFromDate ? fromDateOptions : toDateOptions;

            if (window.plugins && window.plugins.datePicker) {
                window.plugins.datePicker.show(options, function (selectedDate) {
                    if (selectedDate) {
                        that.set(isFromDate ? 'fromDate' : 'toDate', selectedDate);
                        that.set(isFromDate ? 'displayFromDate' : 'displayToDate', utils.formatters.convertToDisplayTimeFormat(selectedDate, 'DD-MM-YYYY'));
                    }
                });
            }
        }
    }
});

Ember.Handlebars.helper('getStatusColor', function (status) {
    var fillCss = 'up-back-color';
    var orderStatus = tradeConstants.SubscriptionStatus;

    if (status === orderStatus.SentToExchange) {
        fillCss = 'highlight-back-color-1';
    } else if (status === orderStatus.Rejected) {
        fillCss = 'down-back-color';
    } else if (status === orderStatus.Approved) {
        fillCss = 'up-back-color';
    }

    return fillCss;
});
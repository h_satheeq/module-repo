import Ember from 'ember';
import sharedService from '../../models/shared/shared-service';
import appEvents from '../../app-events';
import languageDataStore from '../../models/shared/language/language-data-store';

export default Ember.Component.extend({
    layoutName: 'components/mobile/mutual-fund-tab-panel',
    fundTabs: [],
    app: languageDataStore.getLanguageObj(),
    wkey: 'mutual-fund-tab-panel',

    settings: {
        tabs: [
            {id: 1, displayKey: 'fundList'},
            {id: 2, displayKey: 'transactions'},
            {id: 3, displayKey: 'fundHoldings'}
        ]
    },

    onLoad: function () {
        appEvents.subscribeLanguageChanged(this, this.get('wkey'));
    }.on('init'),

    onPrepareData: function () {
        var that = this;
        var app = this.get('app');
        var fundTabs = this.get('settings').tabs;

        Ember.$.each(fundTabs, function (key, tab) {
            Ember.set(tab, 'displayName', app.lang.labels[tab.displayKey]);
            Ember.set(tab, 'css', (tab.id === that.get('tabId')) ? 'active' : '');
        });

        this.set('fundTabs', fundTabs);
    }.on('didInsertElement'),

    languageChanged: function () {
        this.onPrepareData();
    },

    actions: {
        onMutualFundTabSelected: function (tabItem) {
            var menuContent = this.get('menuContent');

            if (menuContent) {
                sharedService.getService('sharedUI').getService('mainPanel').onRenderTabItems(menuContent.tab[tabItem.id]);
            }
        }
    }
});

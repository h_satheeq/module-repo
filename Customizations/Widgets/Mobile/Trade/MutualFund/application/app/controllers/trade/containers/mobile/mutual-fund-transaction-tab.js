import Ember from 'ember';
import BaseWidgetContainer from '../../../base-widget-container';
import mutualFundTabPanel from '../../../../components/mobile/mutual-fund-tab-panel';

export default BaseWidgetContainer.extend({
});

Ember.Handlebars.helper('mutual-fund-tab-panel', mutualFundTabPanel);

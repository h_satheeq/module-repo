import Ember from 'ember';
import sharedService from '../../../../../models/shared/shared-service';
import ControllerFactory from '../../../../../controllers/controller-factory';
import mutualFundTabPanel from '../../../../../components/mobile/mutual-fund-tab-panel';
import MutualFundList from '../../mutual-fund/mutual-fund-list';
import tradeConstants from '../../../../../models/trade/trade-constants';

export default MutualFundList.extend({
    loadMFRequestMenu: function (fund, mode) {
        var widgetController = ControllerFactory.createController(this.container, 'controller:trade.widgets.mobile.mutual-fund.mutual-fund-request', true);
        var viewName = 'trade/widgets/mobile/mutual-fund/mutual-fund-request';

        var portCollection = sharedService.getService('trade').accountDS.getTradingAccCollection();
        var currentPortfolio = portCollection.length > 0 ? portCollection[0] : {};
        var mfObj = sharedService.getService('trade').holdingDS.getHolding(currentPortfolio.tradingAccId, fund.symbol, undefined, this.utils.AssetTypes.MutualFund, undefined, true);

        widgetController.initializeWidget({wn: 'mutual-fund-request'}, {widgetArgs: {fund: fund, mfObj: mfObj, mode: mode, wkey: 'mf-' + this.get('wkey')}});

        sharedService.getService('priceUI').showChildView(viewName, widgetController, widgetController.get('title'), 'mf-request-' + this.get('wkey'));
    },

    actions: {
        onSubscription: function (fund) {
            this.loadMFRequestMenu(fund, tradeConstants.MFSubType.Subscription);
        },

        onRedemption: function (fund) {
            this.loadMFRequestMenu(fund, tradeConstants.MFSubType.Redemption);
        }
    }
});

Ember.Handlebars.helper('mutual-fund-tab-panel', mutualFundTabPanel);

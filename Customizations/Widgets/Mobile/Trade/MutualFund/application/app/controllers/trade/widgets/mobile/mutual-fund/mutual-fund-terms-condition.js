import Ember from 'ember';
import MutualFundTermsConditions from '../../mutual-fund/mutual-fund-terms-condition';
import appConfig from '../../../../../config/app-config';
import sharedService from '../../../../../models/shared/shared-service';

export default MutualFundTermsConditions.extend({
    onConfirmCancel: function () {
        sharedService.getService('priceUI').closeChildView('trade/widgets/mobile/mutual-fund/mutual-fund-terms-condition', true);
    },

    showMessagePopup: function () {
        var that = this;

        Ember.run.next(function () {
            var errorMessages = that.get('errorMessages');

            if (errorMessages && errorMessages.length > 0) {
                that.utils.messageService.showMessage(errorMessages[0],
                    that.utils.Constants.MessageTypes.Error,
                    false,
                    that.app.lang.labels.warning
                );
            }
        });
    },

    actions: {
        onConfirmBack: function () {
            var MFTransactionMenuContentId = appConfig.widgetId.MFTransactionMenuContentId;
            var MFTransactionTabContentId = appConfig.widgetId.MFTransactionTabContentId;

            sharedService.getService('priceUI').closeChildView(undefined, true);
            sharedService.getService('sharedUI').getService('mainPanel').loadWidget(MFTransactionMenuContentId, MFTransactionTabContentId);
        }
    }
});
import Ember from 'ember';
import MutualFundRequest from '../../mutual-fund/mutual-fund-request';
import sharedService from '../../../../../models/shared/shared-service';
import ControllerFactory from '../../../../../controllers/controller-factory';

export default MutualFundRequest.extend({
    executeRequest: function () {
        var widgetController = ControllerFactory.createController(this.container, 'controller:trade.widgets.mobile.mutual-fund.mutual-fund-terms-condition', true);
        var viewName = 'trade/widgets/mobile/mutual-fund/mutual-fund-terms-condition';

        var fundReqObj = this.get('fundReqObj');

        if (this.get('strategy.isSubscription')) {
            fundReqObj.set('amt', this.get('isCrossCurrEnabled') ? fundReqObj.amtCur : fundReqObj.amt);
        } else {
            fundReqObj.set('amount', this.get('isCrossCurrEnabled') ? fundReqObj.amountCur : fundReqObj.amount);
        }

        widgetController.initializeWidget({wn: 'mutual-fund-terms-condition'},
            {widgetArgs: {fundReqObj: fundReqObj,
                fundObj: {
                    portNo: this.get('currentAccount.tradingAccId'),
                    curr: this.get('currentAccount.cashAccount.curr'),
                    redTyp: this.get('redemptionType'),
                    amtUntTyp: this.get('redemptionBy')
                },
                strategy: this.get('strategy'), wkey: 'tc-' + this.get('wkey')}
            }
        );

        sharedService.getService('priceUI').showChildView(viewName, widgetController, widgetController.get('title'), 'terms-condition-' + this.get('wkey'));
    },

    showMessagePopup: function () {
        var that = this;

        Ember.run.next(function () {
            var errorMessages = that.get('errorMessages');

            if (errorMessages && errorMessages.length > 0) {
                that.utils.messageService.showMessage(errorMessages[0],
                    that.utils.Constants.MessageTypes.Error,
                    false,
                    that.app.lang.labels.warning
                );
            }
        });
    }
});
import Ember from 'ember';
import sharedService from '../../../../../models/shared/shared-service';
import ControllerFactory from '../../../../../controllers/controller-factory';
import MutualFundHoldings from '../../mutual-fund/mutual-fund-holdings';
import tradeConstants from '../../../../../models/trade/trade-constants';
import mutualFundTabPanel from '../../../../../components/mobile/mutual-fund-tab-panel';

export default MutualFundHoldings.extend({
    loadMFRequestMenu: function (fund, mode) {
        if (fund.get('isSubRedEnabled')) {
            var widgetController = ControllerFactory.createController(this.container, 'controller:trade.widgets.mobile.mutual-fund.mutual-fund-request', true);
            var viewName = 'trade/widgets/mobile/mutual-fund/mutual-fund-request';

            var mfObj = sharedService.getService('trade').mutualFundDS.getFund(fund.symbol, true);

            widgetController.initializeWidget({wn: 'mutual-fund-request'}, {
                widgetArgs: {
                    fund: fund,
                    mfObj: mfObj,
                    mode: mode,
                    wkey: 'mf-' + this.get('wkey')
                }
            });

            sharedService.getService('priceUI').showChildView(viewName, widgetController, widgetController.get('title'), 'mf-request-' + this.get('wkey'));
        }
    },

    actions: {
        onSubscription: function (fund) {
            if (fund.isSubRedEnabled) {
                this.loadMFRequestMenu(fund, tradeConstants.MFSubType.Subscription);
            }
        },

        onRedemption: function (fund) {
            if (fund.isSubRedEnabled) {
                this.loadMFRequestMenu(fund, tradeConstants.MFSubType.Redemption);
            }
        }
    }
});

Ember.Handlebars.helper('mutual-fund-tab-panel', mutualFundTabPanel);

import Ember from 'ember';
import MutualFundTransactions from '../../mutual-fund/mutual-fund-transactions';

export default MutualFundTransactions.extend({

});

Ember.Handlebars.helper('getTransactionColor', function (status) {
    var fillCss = 'up-back-color';

    switch (status) {
        case '2' :
        case '8' :
        case '9' :
            fillCss = 'highlight-back-color-1';
            break;

        case '4' :
        case '5' :
            fillCss = 'down-back-color';
            break;

        default:
            break;
    }

    return fillCss;
});

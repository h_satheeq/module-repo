export default {
    appConfig: {
        subscriptionConfig: {
            registrationPath: '/create-account?language=',
            upgradeSubscriptionPath: 'upgrade-account',
            renewSubscriptionPath: 'renew-account',
            daysBeforeExpiration: 7
        }
    },

    priceWidgetConfig: {
        chartIndicators: ['AccumulationDistribution', 'AverageTrueRange', 'BollingerBands', 'ChaikinMF', 'ChandeMomentumOscillator', 'MoneyFlowIndex', 'MovingAverage', 'PSAR', 'RelativeStrengthIndex', 'TimeSeriesForecast', 'TRIX', 'VolOsc', 'WildersSmoothing', 'WilliamsPerR']
    }
};

export default {
    appConfig: {
        customisation: {
            authenticationMode: 4,

            loginViewSettings: {
                isHelpEnable: true,
                helpURL: 'https://jksbportal-dev.azurewebsites.net/ExternalSelfServiceRequest/Create'
            }
        }
    },

    tradeSettings: {
        connectionParameters: {
            primary: {
                ip: 'www.jksbonline.com/ws',
                port: '',
                secure: true
            },

            secondary: {
                ip: 'www.jksbonline.com/ws',
                port: '',
                secure: true
            }
        },

        fieldConfigs: {
            cusName: 'cusDetails.cdsNum'
        }
    },

    priceWidgetConfig: {
        watchList: {
            classicColumnIds: ['menu', 'sym', 'trend', 'bbp', 'bbq', 'bap', 'baq', 'ltp', 'ltq', 'dltt', 'vol', 'tovr', 'chg', 'pctChg', 'open', 'high', 'low', 'prvCls', 'trades', 'ltd', 'sDes']
        }
    }
};

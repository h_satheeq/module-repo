export default {
    appConfig: {
        customisation: {
            authenticationMode: 4       
        }
    },

    tradeSettings: {
        connectionParameters: {
            primary: {
                ip: 'online.bartleetreligare.com/ws',
                port: '',
                secure: true
            },

            secondary: {
                ip: 'online.bartleetreligare.com/ws',
                port: '',
                secure: true
            }
        }
    }
};

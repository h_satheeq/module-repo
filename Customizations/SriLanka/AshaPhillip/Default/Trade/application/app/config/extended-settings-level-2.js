export default {
    appConfig: {
        customisation: {
            clientPrefix: 'asha-phillip',
            isAlertEnabled: false
        },

        googleAnalyticConfig: {
            id: 'UA-84530125-1'
        }
    },

    tradeSettings: {
        connectionParameters: {
            primary: {
                ip: 'dfn.ashaphillip.net/ws',
                port: '',
                secure: true
            },

            secondary: {
                ip: 'dfn.ashaphillip.net/ws',
                port: '',
                secure: true
            }
        }
    },

    tradeWidgetConfig: {
        orderDetails: {
            orderDetailsPopup: [
                {lanKey: 'exgOrderNo', dataField: 'ordId', formatter: 'S', style: ''},
                {lanKey: 'orderId', dataField: 'clOrdId', formatter: 'S', style: ''},
                {lanKey: 'side', dataField: 'ordSide', formatter: 'S', style: '', lanKeyAppend: 'orderSide', isCustomStyle: true},
                {lanKey: 'status', dataField: 'ordSts', formatter: 'S', style: '', lanKeyAppend: 'orderStatus', isCustomStyle: true},
                {lanKey: 'quantity', dataField: 'ordQty', formatter: 'I', style: ''},
                {lanKey: 'pendingQty', dataField: 'pendQty', formatter: 'I', style: ''},
                {lanKey: 'filledQuantity', dataField: 'cumQty', formatter: 'I', style: ''},
                {lanKey: 'orderType', dataField: 'ordTyp', formatter: 'S', style: '', lanKeyAppend: 'orderType'},
                {lanKey: 'price', dataField: 'price', formatter: 'C', style: ''},
                {lanKey: 'averagePrice', dataField: 'avgPrice', formatter: 'C', style: ''},
                {lanKey: 'date', dataField: 'adjustedCrdDte', formatter: 'S', style: ''},
                {lanKey: 'expiryDate', dataField: 'adjustedExpTime', formatter: 'S', style: ''},
                {lanKey: 'maximumPrice', dataField: 'maxPrc', formatter: 'C', style: ''},
                {lanKey: 'disclosedQty', dataField: 'disQty', formatter: 'I', style: ''},
                {lanKey: 'minFill', dataField: 'minQty', formatter: 'I', style: ''},
                {lanKey: 'symbolType', dataField: 'instruTyp', formatter: 'S', style: '', isAssetType: true},
                {lanKey: 'tif', dataField: 'tif', formatter: 'S', style: '', lanKeyAppend: 'tifType'},
                {lanKey: 'commission', dataField: 'comsn', formatter: 'C', style: ''},
                {lanKey: 'netValue', dataField: 'netOrdVal', formatter: 'I', style: ''},
                {lanKey: 'orderValue', dataField: 'ordVal', formatter: 'C', style: ''}
            ],

            tableParams: {
                MaxTableWidth: 5700,
                numOfFixedColumns: 3
            }
        }
    }
};

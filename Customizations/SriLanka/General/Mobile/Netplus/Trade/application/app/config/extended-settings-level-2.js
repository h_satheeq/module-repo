export default {
	appConfig: {
        customisation: {
            authenticationMode: 4,
            isTradingEnabled: true,
            profileServiceEnabled: false,
            smartLoginEnabled: false,
            isPasswordChangeEnable: true,
            supportedLanguages: [{code: 'EN', desc: 'English'}],
            hashType: 'MD5',
            hidePreLogin: true,
            isUpperCasePassword: true
        },

        widgetId: {
            quoteMenuId: 'fullQuote',
            watchListMenuId: 'heatMap',
            marketMenuId: 'market',
            indicesTabId: 'companyProf',
            topStocksTabId: 'topStocks',
            portfolioMenuId: 'standard',
            orderTicketMenuId: 'classic'
        }
    },

    priceSettings: {
        connectionParameters: {
            primary: {
                ip: 'lkcentralprice.directfn.com/price',
                port: '',
                secure: true
            },

            secondary: {
                ip: 'lkcentralprice.directfn.com/price',
                port: '',
                secure: true
            }
        },

        configs: {
            priceTickerConfigs: {
                tickerSymDisplayField: 'sym'
            },

            customWindowTypes: {
                SYS: {
                    include: [],
                    exclude: ['213']
                }
            }
        }
    },

    tradeSettings: {
        channelId: 22,

        connectionParameters: {
            primary: {
                ip: 'csegame.directfn.com/trade',
                port: '',
                secure: true
            },

            secondary: {
                ip: 'csegame.directfn.com/trade',
                port: '',
                secure: true
            }
        },

        fieldConfigs: {
            portfolioDes: 'extRefNo'
        }
    }
};

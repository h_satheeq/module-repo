export default {
    appConfig: {
        customisation: {
            authenticationMode: 4       
        }
    },

    tradeSettings: {
        connectionParameters: {
            primary: {
                ip: 'dfn.softlogicequity.lk/ws',
                port: '',
                secure: true
            },

            secondary: {
                ip: 'dfn.softlogicequity.lk/ws',
                port: '',
                secure: true
            }
        }
    }
};

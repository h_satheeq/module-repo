export default {
    appConfig: {
        customisation: {
            authenticationMode: 4       
        }
    },

    tradeSettings: {
        connectionParameters: {
            primary: {
                ip: 'dfn.rpsecurities.com/ws',
                port: '',
                secure: true
            },

            secondary: {
                ip: 'dfn.rpsecurities.com/ws',
                port: '',
                secure: true
            }
        }
    }
};

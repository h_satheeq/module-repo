import Ember from 'ember';

export default Ember.Object.extend({
    disclosedMargin: {default: 0},
    minMargin: {default: 0},
    amendAllowedStsByExg: {default: []},
    cancelAllowedStsByExg: {default: []},

    tickRule: {
        '*': {'*': 0.1}
    },

    amendEnableConfig: {
        isTiffDisabled: true,
        isOrderTypeDisabled: true
    },

    /* *
     * Add default values for all exchange specific variable maps
     */
    init: function () {
        var amendAllowedStsByExg = this.get('amendAllowedStsByExg');

        amendAllowedStsByExg.default = ['0', 'A', '1', '5'];
        amendAllowedStsByExg.LKCSE = ['0', 'A', '1', '5'];

        var cancelAllowedStsByExg = this.get('cancelAllowedStsByExg');

        cancelAllowedStsByExg.default = ['T', 'O', '0', 'A', '1', '5'];
        cancelAllowedStsByExg.LKCSE = ['T', 'O', '0', 'A', '1', '5'];
    }
}).create();

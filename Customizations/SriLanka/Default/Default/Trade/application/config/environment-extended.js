/* jshint node: true */
var EnvironmentCore = require('./environment-core.js');

module.exports = function (environment) {
    var ENV = new EnvironmentCore(environment);
    ENV.contentSecurityPolicy['connect-src'] = "'self' www.google-analytics.com wss://lkcentralprice.directfn.com/price ws://123.231.48.7:8800/";

    if (ENV.APP.isTestMode) {
        ENV.APP.tradeConnectionParameters = {
            primary: {
                ip: '123.231.48.7',
                port: '8800',
                secure: false
            }
        };

        ENV.APP.priceConnectionParameters = {
            primary: {
                ip: 'lkcentralprice.directfn.com/price',
                port: '',
                secure: true
            }
        };
    }

    return ENV;
};

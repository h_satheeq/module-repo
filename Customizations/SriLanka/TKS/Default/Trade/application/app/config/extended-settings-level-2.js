export default {
    appConfig: {
        customisation: {
            authenticationMode: 4       
        }
    },

    tradeSettings: {
        connectionParameters: {
            primary: {
                ip: 'dfn.tks.lk/ws',
                port: '',
                secure: false
            },

            secondary: {
                ip: 'dfn.tks.lk/ws',
                port: '',
                secure: false
            }
        }
    }
};

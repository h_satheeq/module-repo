/* jshint node: true */
var EnvironmentExtended = require('./environment-extended.js');

module.exports = function (environment) {
    var ENV = new EnvironmentExtended(environment);
    ENV.contentSecurityPolicy['connect-src'] = "'self' www.google-analytics.com wss://lkcentralprice.directfn.com/price ws://dfn.tks.lk/ws";

    return ENV;
};

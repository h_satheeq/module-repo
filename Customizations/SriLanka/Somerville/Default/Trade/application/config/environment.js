/* jshint node: true */
var EnvironmentExtended = require('./environment-extended.js');

module.exports = function (environment) {
    var ENV = new EnvironmentExtended(environment);
	
	ENV.baseURL = '/app';
    ENV.contentSecurityPolicy['connect-src'] = "'self' www.google-analytics.com wss://lkcentralprice.directfn.com/price wss://lkcentralprice.directfn.com/ws";

    if (ENV.APP.isTestMode) {
		ENV.baseURL = '/';
    }

    return ENV;
};

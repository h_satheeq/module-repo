export default {
    appConfig: {
        customisation: {
            authenticationMode: 4            
        }
    },

    tradeSettings: {
        connectionParameters: {
            primary: {
                ip: 'lkcentralprice.directfn.com/ws',
                port: '',
                secure: true
            },

            secondary: {
                ip: 'lkcentralprice.directfn.com/ws',
                port: '',
                secure: true
            }
        }
    }
};

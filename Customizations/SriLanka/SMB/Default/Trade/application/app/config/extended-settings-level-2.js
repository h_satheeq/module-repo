export default {
    appConfig: {
        customisation: {
            authenticationMode: 4,
            clientPrefix: 'smb'
        },

        googleAnalyticConfig: {
            id: 'UA-81921918-1'
        }
    },

    tradeSettings: {
        connectionParameters: {
            primary: {
                ip: 'online.smbsecurities.lk/ws',
                port: '',
                secure: true
            },

            secondary: {
                ip: 'online.smbsecurities.lk/ws',
                port: '',
                secure: true
            }
        }
    }
};

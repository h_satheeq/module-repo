/* jshint node: true */
var EnvironmentExtended = require('./environment-extended.js');

module.exports = function (environment) {
    var ENV = new EnvironmentExtended(environment);

    ENV.contentSecurityPolicy['connect-src'] = "'self' www.google-analytics.com wss://lkcentralprice.directfn.com/price wss://csegame.directfn.com/trade";
    ENV.locationType = 'none';    
	ENV.baseURL = '/mobileapp';

    if (ENV.APP.isTestMode) {       
		ENV.baseURL = '/';
        ENV.contentSecurityPolicy['connect-src'] = "'self' www.google-analytics.com wss://lkcentralprice.directfn.com/price ws://59.163.249.198:8800 ws://123.231.48.7:8800";
    }

    return ENV;
};

export default {
    appConfig: {
        customisation: {
            authenticationMode: 4,
            isTradingEnabled: true,
            profileServiceEnabled: false,
            smartLoginEnabled: false,
            isPasswordChangeEnable: true,
            supportedLanguages: [{code: 'EN', desc: 'English'}],
            hashType: 'MD5',
            hidePreLogin: true
        },

        loggerConfig: {
            serverLogLevel: 0
        }
    },

    priceSettings: {
        connectionParameters: {
            primary: {
                ip: 'lkcentralprice.directfn.com/price',
                port: '',
                secure: true
            },

            secondary: {
                ip: 'lkcentralprice.directfn.com/price',
                port: '',
                secure: true
            }
        },

        configs: {
            priceTickerConfigs: {
                tickerSymDisplayField: 'sym'
            },

            symbolSearchConfigs: {
                showSubMarket: true
            }
        }
    },

    tradeSettings: {
        channelId: 30,

        connectionParameters: {
            primary: {
                ip: '123.231.48.7',
                port: '8800',
                secure: false
            },

            secondary: {
                ip: '123.231.48.7',
                port: '8800',
                secure: false
            }
        },

        fieldConfigs: {
            portfolioDes: 'tradingAccName'
        }
    },

    userSettings: {
        customisation: {
            defaultExchange: 'TDWL',
            defaultIndex: 'TASI',
            defaultCurrency: 'SAR'
        }
    },

    priceWidgetConfig: {
        watchList: {
            classicMoreColumnIds: ['sDes', 'lDes', 'trend', 'isin', 'instDes', 'ltp', 'ltq', 'ltd', 'dltt', 'prvCls', 'chg', 'pctChg', 'vol', 'tovr', 'open', 'high', 'low', 'cls', 'bbp', 'bbq', 'bap', 'baq', 'h52', 'l52', 'trades', 'refValue', 'tbq', 'taq', 'eps', 'per', 'cit', 'cvwap', 'twap', 'vwap'],

            classicColumnIds: ['menu', 'sym', 'sDes', 'trend', 'ltp', 'ltq', 'chg', 'pctChg', 'vol', 'bbp', 'bbq', 'bap', 'baq', 'trades', 'ltd', 'dltt', 'open', 'high', 'low']
        },

        quote: {
            panelIntraday: {
                // Equity
                '1': [
                    {lanKey: 'lastTrade', dataField: 'ltp', formatter: 'C', style: 'highlight-fore-color'},
                    {lanKey: 'lastQty', dataField: 'ltq', formatter: 'L', style: 'highlight-fore-color'},
                    {lanKey: 'open', dataField: 'open', formatter: 'C'},
                    {lanKey: 'close', dataField: 'cls', formatter: 'C'},
                    {lanKey: 'preClosed', dataField: 'prvCls', formatter: 'C'},
                    {lanKey: 'vWAP', dataField: 'vwap', formatter: 'C', detailQouteTitle: 'vwapDesc'},
                    {lanKey: 'volume', dataField: 'vol', formatter: 'L', style: 'highlight-fore-color'},
                    {lanKey: 'turnover', dataField: 'tovr', formatter: 'L', style: 'highlight-fore-color'},
                    {lanKey: 'trades', dataField: 'trades', formatter: 'L', style: 'highlight-fore-color'},
                    {lanKey: 'mktCap', dataField: 'mktCap', formatter: 'DN'},
                    {lanKey: 'high', dataField: 'high', formatter: 'C'},
                    {lanKey: 'low', dataField: 'low', formatter: 'C'},
                    {lanKey: 'bid', dataField: 'bbp', formatter: 'C', style: 'up-fore-color'},
                    {lanKey: 'offer', dataField: 'bap', formatter: 'C', style: 'down-fore-color'},
                    {lanKey: 'bidQty', dataField: 'bbq', formatter: 'L', style: 'up-fore-color'},
                    {lanKey: 'offerQty', dataField: 'baq', formatter: 'L', style: 'down-fore-color'},
                    {lanKey: 'fiftyTwoWkH', dataField: 'h52', formatter: 'C'},
                    {lanKey: 'fiftyTwoWkL', dataField: 'l52', formatter: 'C'}
                ],

                // Fixed Income
                6: [
                    {lanKey: 'lastTrade', dataField: 'ltp', formatter: 'C'},
                    {lanKey: 'lastQty', dataField: 'ltq', formatter: 'L'},
                    {lanKey: 'open', dataField: 'open', formatter: 'C'},
                    {lanKey: 'close', dataField: 'cls', formatter: 'C'},
                    {lanKey: 'preClosed', dataField: 'prvCls', formatter: 'C'},
                    {lanKey: 'volume', dataField: 'vol', formatter: 'L'},
                    {lanKey: 'turnover', dataField: 'tovr', formatter: 'L'},
                    {lanKey: 'trades', dataField: 'trades', formatter: 'L'},
                    {lanKey: 'bid', dataField: 'bbp', formatter: 'C', style: 'up-fore-color'},
                    {lanKey: 'offer', dataField: 'bap', formatter: 'C', style: 'down-fore-color'},
                    {lanKey: 'bidQty', dataField: 'bbq', formatter: 'L', style: 'up-fore-color'},
                    {lanKey: 'offerQty', dataField: 'baq', formatter: 'L', style: 'down-fore-color'},
                    {lanKey: 'couponRate', dataField: 'cor', formatter: 'C'},
                    {lanKey: 'couponFreq', dataField: 'cof', formatter: 'C'},
                    {lanKey: 'previousCouponDate', dataField: 'pcd', formatter: 'D'},
                    {lanKey: 'faceValue', dataField: 'fVal', formatter: 'C'},
                    {lanKey: 'maturityDate', dataField: 'matD', formatter: 'D'},
                    {lanKey: 'bondType', dataField: 'boT', formatter: 'S'},
                    {lanKey: 'outstandingAmount', dataField: 'outA', formatter: 'DN'},
                    {lanKey: 'settlementDate', dataField: 'setD', formatter: 'D'},
                    {lanKey: 'dayCountMethod', dataField: 'dcm', formatter: 'S'}
                ],

                // Mutual Fund
                5: [
                    {lanKey: 'lastTrade', dataField: 'ltp', formatter: 'C'},
                    {lanKey: 'lastQty', dataField: 'ltq', formatter: 'L'},
                    {lanKey: 'open', dataField: 'open', formatter: 'C'},
                    {lanKey: 'close', dataField: 'cls', formatter: 'C'},
                    {lanKey: 'preClosed', dataField: 'prvCls', formatter: 'C'},
                    {lanKey: 'vWAP', dataField: 'vwap', formatter: 'C'},
                    {lanKey: 'volume', dataField: 'vol', formatter: 'L'},
                    {lanKey: 'turnover', dataField: 'tovr', formatter: 'L'},
                    {lanKey: 'trades', dataField: 'trades', formatter: 'L'},
                    {lanKey: 'mktCap', dataField: 'mktCap', formatter: 'DN'},
                    {lanKey: 'bid', dataField: 'bbp', formatter: 'C', style: 'up-fore-color'},
                    {lanKey: 'offer', dataField: 'bap', formatter: 'C', style: 'down-fore-color'},
                    {lanKey: 'bidQty', dataField: 'bbq', formatter: 'L', style: 'up-fore-color'},
                    {lanKey: 'offerQty', dataField: 'baq', formatter: 'L', style: 'down-fore-color'},
                    {lanKey: 'high', dataField: 'high', formatter: 'C'},
                    {lanKey: 'low', dataField: 'low', formatter: 'C'},
                    {lanKey: 'fiftyTwoWkH', dataField: 'h52', formatter: 'C'},
                    {lanKey: 'fiftyTwoWkL', dataField: 'l52', formatter: 'C'}
                ]
            },

            panelFundamental: {
                // Equity
                '1': [],

                // Fixed Income
                6: [
                    {lanKey: 'couponRate', dataField: 'cor', formatter: 'C'},
                    {lanKey: 'couponFreq', dataField: 'cof', formatter: 'C'},
                    {lanKey: 'previousCouponDate', dataField: 'pcd', formatter: 'D'},
                    {lanKey: 'faceValue', dataField: 'fVal', formatter: 'C'},
                    {lanKey: 'maturityDate', dataField: 'matD', formatter: 'D'},
                    {lanKey: 'bondType', dataField: 'boT', formatter: 'S'},
                    {lanKey: 'outstandingAmount', dataField: 'outA', formatter: 'C'},
                    {lanKey: 'settlementDate', dataField: 'setD', formatter: 'D'},
                    {lanKey: 'dayCountMethod', dataField: 'dcm', formatter: 'S'}
                ],

                // Mutual Fund
                5: []
            }
        },

        gms: [
            {sym: 'EURUSD', exg: 'GLOBAL', inst: '0', sDes: 'EURUSD', icon: 'comm-icon icon-euro'},
            {sym: 'USDJPY', exg: 'GLOBAL', inst: '0', sDes: 'USDJPY', icon: 'comm-icon icon-usd'},
            {sym: 'GBPUSD', exg: 'GLOBAL', inst: '0', sDes: 'GBPUSD', icon: 'comm-icon icon-usd'},
            {sym: 'USDAUD', exg: 'GLOBAL', inst: '0', sDes: 'USDAUD', icon: 'comm-icon icon-usd'},
            {sym: 'USDLKR', exg: 'GLOBAL', inst: '0', sDes: 'USDLKR', icon: 'comm-icon icon-euro'}
        ],

        WidgetList: {
            trade: [
                {code: 'trade.widgets.order-list', des: 'Order List', desc: 'orderList', icon: 'glyphicon glyphicon-list-alt'},
                {code: 'trade.widgets.order-search', des: 'Order Search', desc: 'orderSearch', icon: 'glyphicon glyphicon-search'},
                {code: 'trade.widgets.order-ticket.order-ticket-landscape', des: 'Order Ticket (L)', desc: 'orderTicket', icon: 'glyphicon glyphicon-send'},
                {code: 'trade.widgets.portfolio', des: 'Portfolio', desc: 'portfolio', icon: 'glyphicon glyphicon-duplicate'},
                {code: 'trade.widgets.saved-orders', des: 'Saved Orders', desc: 'savedOrders', icon: 'glyphicon glyphicon-saved'}
            ]
        }
    },

    tradeWidgetConfig: {
        portfolio: {
            defaultColumnIds: ['buyMore', 'liquidate', 'sym', 'sDes', 'qty', 'pendSell', 'pendBuy', 'avaiQty', 'mktPrice', 'avgCst', 'costVal', 'mktVal', 'gainLoss', 'gainLossPer', 'portPer', 'curr', 'exg']
        }
    }
};

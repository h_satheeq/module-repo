export default {
    appVersion: '0.0.0',
        helpGuidePath: '',

        customisation: {
        appName: 'NetPlus New',
            clientPrefix: 'default',
            authenticationMode: 1,
            isStateSavingEnabled: true,
            productType: 56,

            supportedLanguages: [
            {code: 'EN', desc: 'English'},
            {code: 'AR', desc: 'العربية'}
        ],

            applicationIdleCheckConfig: {
                defaultIdleTime: 14, // 14 minute
                isEnabledInactiveLogout: true
        },

        supportedThemes: [
            {code: 'theme1', desc: 'Dark', category: 'Dark', langKey: 'dark'},
            {code: 'theme2', desc: 'Light', category: 'Light', langKey: 'light'}
        ],

            profileServiceEnabled: false,
            smartLoginEnabled: true,
            isPasswordChangeEnable: false,
            isEmbeddedMode: false,
            isVirtualKeyboardEnable: false,
            hashType: '',
            hidePreLogin: false,
            isDiscloseQtyEnabled: false,
            isUpperCasePassword: true,
            isMultipleMenuExpand: false,
            isCustomWorkSpaceEnabled: false,
            isAlertEnabled: true,
            showFailedLoginAttempt: false,
            isCacheClearEnabled: true,

            defaultSubMarket: {
            TDWL: '1'
        },

        loginViewSettings: {
            showTermsAndConditions: false
        }
    },

    loginConfig: {
        isRememberPassword: false
    },

    loggerConfig: {
        serverLogLevel: 3, // Server log level is info
            consoleLogLevel: 3, // Console log level is info
            logBufferSize: 50, // This number of latest logs will be maintained. Will be used only in sendLogToServer mode
            logUpdateTimeout: 300000, // Logs will be uploaded to server every 5min
            maxLogBufferSize: 10000 // Log buffer will be cleared when it reach maxLogBufferSize
    },

    searchConfig: {
        minCharLenForSymbol: 2, // Minimum input characters for performing symbol search
            minCharLenForContent: 3 // Minimum input characters for performing news & announcement search
    },

    googleAnalyticConfig: {
        id: 'UA-70494667-1'
    },

    subscriptionConfig: {
        registrationPath: '',
            upgradeSubscriptionPath: '',
            renewSubscriptionPath: '',
            daysBeforeExpiration: 7
    },

    chartConfig: {
        chartIndicators: ['MovingAverage', 'BollingerBands', 'MACD', 'Momentum']
    }
};
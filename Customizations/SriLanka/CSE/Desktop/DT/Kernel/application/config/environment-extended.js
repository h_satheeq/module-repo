/* jshint node: true */
var EnvironmentCore = require('./environment-core.js');

module.exports = function (environment) {
    var ENV = new EnvironmentCore(environment);
    /*ENV.contentSecurityPolicy['connect-src'] = "'self' www.google-analytics.com wss://lkcentralprice.directfn.com/price ws://123.231.48.7:8800/";

    if (ENV.APP.isTestMode) {
        ENV.APP.tradeConnectionParameters = {
            primary: {
                ip: '123.231.48.7',
                port: '8800',
                secure: false
            }
        };

        ENV.APP.priceConnectionParameters = {
            primary: {
                ip: 'lkcentralprice.directfn.com/price',
                port: '',
                secure: true
            }
        };
    }

    return ENV;*/

    ENV.contentSecurityPolicy['connect-src'] = "'self' www.google-analytics.com wss://data-sa9.mubasher.net/html5ws wss://icap-wstrs-qa.directfn.net/trs 192.168.13.62:8090/trs";

    if (ENV.APP.isTestMode) {
        ENV.APP.tradeConnectionParameters = {
            primary: {
                //ip: 'icap-wstrs-qa.directfn.net/trs',
                ip: '192.168.13.62:8090/trs',
                port: '',
                secure: false
            }
        };

        ENV.APP.priceConnectionParameters = {
            primary: {
                ip: 'data-sa9.mubasher.net/html5ws',
                port: '',
                secure: true
            }
        };

        ENV.contentSecurityPolicy['connect-src'] = "'self' www.google-analytics.com wss://data-sa9.mubasher.net/html5ws wss://icap-wstrs-qa.directfn.net/trs 192.168.13.62:8090/trs";
    }
    return ENV;
};

class ConnectionSettings {
    constructor() {
        this.ip = 'icap-wstrs-qa.directfn.net/trs';
        this.port = '';
        this.secure = true;
        this.enablePulse = true;
    }
}
module.exports = new ConnectionSettings();

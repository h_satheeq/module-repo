export default {
    appConfig: {
        helpGuidePath: 'http://csegame.directfn.com/HelpGuide',

        customisation: {
            appName: 'Pro Lite',
            clientPrefix: 'cse'
        },

        googleAnalyticConfig: {
            id: 'UA-84571533-1'
        }
    },

    priceSettings: {
        configs: {
            customWindowTypes: {
                SYS: {
                    include: [],
                    exclude: ['213']
                }
            }
        }
    },

    tradeSettings: {
        connectionParameters: {
            primary: {
                ip: 'csegame.directfn.com/trade',
                port: '',
                secure: true
            },

            secondary: {
                ip: 'csegame.directfn.com/trade',
                port: '',
                secure: true
            }
        }
    }
};

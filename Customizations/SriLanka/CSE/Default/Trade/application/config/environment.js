/* jshint node: true */
var EnvironmentExtended = require('./environment-extended.js');

module.exports = function (environment) {
    var ENV = new EnvironmentExtended(environment);
	
	ENV.baseURL = '/app';
    ENV.contentSecurityPolicy['connect-src'] = "'self' www.google-analytics.com wss://lkcentralprice.directfn.com/price wss://csegame.directfn.com/ws";

    if (ENV.APP.isTestMode) {		
		ENV.baseURL = '/';
		ENV.contentSecurityPolicy['connect-src'] = "'self' www.google-analytics.com wss://lkcentralprice.directfn.com/price ws://123.231.48.7:8800/";
    }

    return ENV;
};
export default {
    EN: {
        labels: {
            depthByPrice: 'Order Book',
            mktStatus_1: 'Open Auction',
            mktStatus_2: 'Regular Trading',
            mktStatus_3: 'Market Close',
            mktStatus_4: 'Market Close',
            mktStatus_26: 'Closed',
            commission: 'Transaction Cost',
            liquidate: 'Sell',
            orderTicket: 'Order Blotter',
            news: 'Announ...',
            newsAnn: 'Announcements',
            newsAnnouncement: 'Announcements'
        }
    }
};

export default {
    appConfig: {
        customisation: {
            supportedLanguages: [
                {code: 'EN', desc: 'English'},
                {code: 'FR', desc: 'français'}
            ]
        }
    }
};

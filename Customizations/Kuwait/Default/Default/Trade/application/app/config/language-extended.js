export default {
    EN: {
        labels: {
            /*eslint-disable */
            orderStatus_0: 'Queued'
            /*eslint-enable */
        }
    },

    AR: {
        labels: {
            /*eslint-disable */
            orderStatus_0: '\u0641\u064A \u0627\u0644\u0627\u0646\u062A\u0638\u0627\u0631'
            /*eslint-enable */
        }
    }
};

export default {
    appConfig: {
        customisation: {
            clientPrefix: 'mKuwait',
			authenticationMode: 4,
            isTradingEnabled: true,
            hashType: 'MD5',
            isUpperCasePassword: true,
            profileServiceEnabled: false,

            supportedContacts: [
                {key: 'phn', value: '920005710', type: 'T'},
                {key: 'phn', value: '+966112140549000', type: 'T'},
                {key: 'phn', value: '+966112140549', type: 'T'},
                {key: 'email', value: 'support-ksa@directfn.com', type: 'E'},
                {key: 'url', value: 'www.directfn.sa', type: 'U'}
            ],

            displayProperties: {
                dispProp: ['tradingAccName', 'cashAccount.invAccNo'],
                dispProp1: ['cid', 'sDes'],
                dispProp2: 'cid',
                dispProp3: 'lDes'
            }
        },

        loggerConfig: {
            serverLogLevel: 0,
            consoleLogLevel: 3
        }
    },

    priceSettings: {
        connectionParameters: {
            primary: {
                ip: 'ir.directfn.com/ws',
                port: '',
                secure: true // Is connection secure - true or false
            },

            secondary: {
                ip: 'ir.directfn.com/ws',
                port: '',
                secure: true // Is connection secure - true or false
            }
        },

		configs: {
            priceTickerConfigs: {
                tickerSymDisplayField: 'dispProp1'
            }
        }
    },

    tradeSettings: {
		channelId: 30,

        // Colombo TRS - Local
        connectionParameters: {
            primary: {
                ip: 'kse-wstrs-qa.directfn.net/trs',
                port: '',
                secure: false
            },

            secondary: {
                ip: 'kse-wstrs-qa.directfn.net/trs',
                port: '',
                secure: false
            }
        }
    },

    tradeWidgetConfig: {
        orderDetails: {
            orderDetailsPopup: [
                {lanKey: 'exgOrderNo', dataField: 'ordId', formatter: 'S', style: '', cellStyle: ''},
                {lanKey: 'orderId', dataField: 'clOrdId', formatter: 'S', style: '', cellStyle: ''},
                {lanKey: 'side', dataField: 'ordSide', formatter: 'S', style: '', lanKeyAppend: 'orderSide', isCustomStyle: true, cellStyle: ''},
                {lanKey: 'status', dataField: 'ordSts', formatter: 'S', style: '', lanKeyAppend: 'orderStatus', isCustomStyle: true, cellStyle: ''},
                {lanKey: 'quantity', dataField: 'ordQty', formatter: 'I', style: '', cellStyle: ''},
                {lanKey: 'pendingQty', dataField: 'pendQty', formatter: 'I', style: '', cellStyle: ''},
                {lanKey: 'filledQuantity', dataField: 'cumQty', formatter: 'I', style: '', cellStyle: ''},
                {lanKey: 'orderType', dataField: 'ordTyp', formatter: 'S', style: '', lanKeyAppend: 'orderType', cellStyle: ''},
                {lanKey: 'price', dataField: 'price', formatter: 'C', style: '', cellStyle: ''},
                {lanKey: 'averagePrice', dataField: 'avgPrice', formatter: 'C', style: '', cellStyle: ''},
                {lanKey: 'stopPriceType', dataField: 'stPrcTyp', formatter: 'S', style: '', cellStyle: '', allowedOrdTypes: ['3', '4']},
                {lanKey: 'stopPrice', dataField: 'stPrc', formatter: 'C', style: '', cellStyle: '', allowedOrdTypes: ['3', '4']},
                {lanKey: 'maximumPrice', dataField: 'maxPrc', formatter: 'C', style: '', cellStyle: ''},
                {lanKey: 'date', dataField: 'adjustedCrdDte', formatter: 'S', style: '', cellStyle: ''},
                {lanKey: 'expiryDate', dataField: 'adjustedExpTime', formatter: 'S', style: '', cellStyle: ''},
                {lanKey: 'tif', dataField: 'tif', formatter: 'S', style: '', lanKeyAppend: 'tifType', cellStyle: ''},
                {lanKey: 'disclosedQty', dataField: 'disQty', formatter: 'I', style: '', cellStyle: ''},
                {lanKey: 'minFill', dataField: 'minQty', formatter: 'C', style: '', cellStyle: ''},
                {lanKey: 'orderValue', dataField: 'ordVal', formatter: 'C', style: '', cellStyle: ''},
                {lanKey: 'commission', dataField: 'comsn', formatter: 'C', style: '', cellStyle: ''},
                {lanKey: 'netValue', dataField: 'netOrdVal', formatter: 'C', style: '', cellStyle: ''},
                {lanKey: 'cumOrdVal', dataField: 'cumOrdVal', formatter: 'C', style: '', cellStyle: ''},
                {lanKey: 'symbolType', dataField: 'instruTyp', formatter: 'S', style: '', isAssetType: true, cellStyle: ''},
                {lanKey: 'settlementDate', dataField: 'adjustedSttlmntDte', formatter: 'S', style: '', cellStyle: ''}
            ]
        }
    },

    userSettings: {
        customisation: {
            defaultExchange: 'KSE',
            defaultIndex: '11',
            defaultCurrency: 'KWD',
			defaultLanguage: 'AR'
        },

        orderConfig: {
            defaultOrderQty: 0
        }
    }
};

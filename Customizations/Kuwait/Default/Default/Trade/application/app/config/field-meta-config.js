export default {
    portfolio: {
        decimalPlaces: 4,

        mktPrice: {
            multiFactor: 0.001,
            decimalPlaces: 4
        }
    },

    accountSummary: {
        decimalPlaces: 4
    },

    orderDetails: {
        decimalPlaces: 4
    },

    orderTicket: {
        orderValue: {
            multiFactor: 0.001,
            decimalPlaces: 4
        }
    },

    multiFactors: {
        KSE: {
            multiFactor: 0.001,
            decimalPlaces: 4
        },

        DFM: {
            decimalPlaces: 3
        }
    }
};
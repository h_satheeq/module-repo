export default {
    EN: {
        labels: {
            /*eslint-disable */
            orderStatus_0: 'Queued',
            /*eslint-enable */
            sharqAddress: 'Al-Sharq Financial Brokerage Co<br>P.O.Box : 187 Al-Souk<br>Al-Dakheley 15252, Kuwait'
        }
    },

    AR: {
        labels: {
            /*eslint-disable */
            orderStatus_0: '\u0641\u064A \u0627\u0644\u0627\u0646\u062A\u0638\u0627\u0631',
            /*eslint-enable */
            sharqAddress: '\u0635\u0646\u062f\u0648\u0642 \u0628\u0631\u064a\u062f\u003a<br>\u0031\u0038\u0037\u0627\u0644\u0633\u0648\u0642 \u0627\u0644\u062f\u0627\u062e\u0644\u064a<br>\u0627\u0644\u0631\u0645\u0632 \u0627\u0644\u0628\u0631\u064a\u062f\u064a \u0031\u0035\u0032\u0035\u0032'
        }
    }
};

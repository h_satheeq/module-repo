/* jshint node: true */
var EnvironmentExtended = require('./environment-extended.js');

module.exports = function (environment) {
    var ENV = new EnvironmentExtended(environment);
    ENV.baseURL = '/';
	ENV.locationType = 'none';

    ENV.contentSecurityPolicy['connect-src'] = "'self' www.google-analytics.com wss://data-sa9.mubasher.net/html5ws ws://193.22.172.215/wsqs ws://193.22.172.215/wstrs wss://m-mefbc-trade-universal.directfn.com/wsqs wss://m-mefbc-trade-universal.directfn.com/wstrs";

    if (ENV.APP.isTestMode) {
        ENV.APP.tradeConnectionParameters = {
            primary: {
                ip: 'kse-wstrs-qa.directfn.net/trs',
                port: '',
                secure: false
            }
        };

        ENV.APP.priceConnectionParameters = {
            primary: {
                ip: 'ir.directfn.com/ws',
                port: '',
                secure: true
            }
        };

        ENV.contentSecurityPolicy['connect-src'] = "'self' www.google-analytics.com wss://ir.directfn.com/ws wss://data-sa9.mubasher.net/html5ws ws://kse-wstrs-qa.directfn.net/trs";
    }

    return ENV;
};

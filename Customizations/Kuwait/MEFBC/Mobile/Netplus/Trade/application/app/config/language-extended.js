export default {
    EN: {
        labels: {
            /*eslint-disable */
            orderStatus_0: 'Queued',
            /*eslint-enable */
            mefbcAddress: 'AL-AWSAT<br>Middle East Financial Brokerage Co. k.s.c.c<br>Boursa Kuwait Building, 2nd floor, office 2/121<br>Mubarak Al Kabeer St, Sharq, Kuwait City<br>P O BOX 819 – Safat 1309 Kuwait'
        }
    },

    AR: {
        labels: {
            /*eslint-disable */
            orderStatus_0: '\u0641\u064A \u0627\u0644\u0627\u0646\u062A\u0638\u0627\u0631',
            /*eslint-enable */
            mefbcAddress: '\u0627\u0644\u0623\u0648\u0633\u0637<br>\u0634\u0631\u0643\u0629 \u0627\u0644\u0634\u0631\u0642 \u0627\u0644\u0623\u0648\u0633\u0637 \u0644\u0644\u0648\u0633\u0627\u0637\u0629 \u0627\u0644\u0645\u0627\u0644\u064a\u0629 \u0634\u002e\u0645\u002e\u0643\u002e\u0645<br>' +
                            '\u0645\u0628\u0646\u0649 \u0628\u0648\u0631\u0635\u0629 \u0627\u0644\u0643\u0648\u064a\u062a\u060c \u0627\u0644\u062f\u0648\u0631 \u0627\u0644\u062b\u0627\u0646\u064a \u0645\u0643\u062a\u0628 \u0032\u002f\u0031\u0032\u0031<br>' +
                            '\u0634\u0627\u0631\u0639 \u0645\u0628\u0627\u0631\u0643 \u0627\u0644\u0643\u0628\u064a\u0631\u060c \u0634\u0631\u0642\u060c \u0645\u062f\u064a\u0646\u0629 \u0627\u0644\u0643\u0648\u064a\u062a<br>' +
                            '\u0635\u002e\u0628 \u0038\u0031\u0039 \u0627\u0644\u0635\u0641\u0627\u0629 \u0031\u0033\u0030\u0039 \u0627\u0644\u0643\u0648\u064a\u062a'
        }
    }
};

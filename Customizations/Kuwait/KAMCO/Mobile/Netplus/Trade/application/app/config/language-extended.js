export default {
    EN: {
        labels: {
            /*eslint-disable */
            orderStatus_0: 'Queued',
            /*eslint-enable */
            kamcoAddress: 'KAMCO Investment Company K.S.C.(Public)<br>Al-Shaheed Tower,<br>Khalid Bin Waleed Street Sharq,<br>Kuwait'
        }
    },

    AR: {
        labels: {
            /*eslint-disable */
            orderStatus_0: '\u0641\u064A \u0627\u0644\u0627\u0646\u062A\u0638\u0627\u0631',
            /*eslint-enable */
            kamcoAddress: '\u0634\u0631\u0643\u0629 \u0643\u0627\u0645\u0643\u0648 \u0644\u0644\u0625\u0633\u062a\u062b\u0645\u0627\u0631 \u0634\u002e\u0645\u002e\u0643\u002e \u0028\u0639\u0627\u0645\u0629\u0029<br>' +
            '\u0628\u0631\u062c \u0627\u0644\u0634\u0647\u064a\u062f\u002c<br>\u0634\u0631\u0642 \u002d \u0634\u0627\u0631\u0639 \u062e\u0627\u0644\u062f \u0628\u0646 \u0627\u0644\u0648\u0644\u064a\u062f<br>\u0627\u0644\u0643\u0648\u064a\u062a'
        }
    }
};

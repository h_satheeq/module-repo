export default {
    appConfig: {
        customisation: {
            authenticationMode: 4,
            isTradingEnabled: true,
            hashType: 'MD5',
            isUpperCasePassword: true,
            isPasswordChangeEnable: true,
            smartLoginEnabled: false,
            showFailedLoginAttempt: true,
            isEnabledPwRules: true,

            passwordRules: {
                maxLength: 18,
                minLength: 7,
                checkLength: true,
                checkContainNumber: true,
                checkStartWithLetter: false,
                checkContainSmallLetter: false,
                checkContainCapitalLetter: false,
                checkIdenticalCharacter: false,
                checkConsecutiveCharacter: false,
                checkSpecialCharacter: true,
                checkUsernameMatch: true
            },

            applicationIdleCheckConfig: {
                defaultIdleTime: 14, // 14 minute
                isEnabledInactiveLogout: true
            }
        },

        widgetId: {
            quoteMenuId: 'fullQuote',
            watchListMenuId: 'heatMap',
            marketMenuId: 'market',
            indicesTabId: 'companyProf',
            topStocksTabId: 'topStocks',
            portfolioMenuId: 'standard',
            orderTicketMenuId: 'classic'
        }
    },

    priceSettings: {
        configs: {
            priceTickerConfigs: {
                tickerSymDisplayField: 'sym'
            }
        }
    },

    tradeSettings: {
	channelId: 30,

        // Colombo TRS - Local
        connectionParameters: {
            primary: {
                ip: 'brokerage.nbkcapital.com/wstrs',
                port: '',
                secure: true
            },

            secondary: {
                ip: 'brokerage.nbkcapital.com/wstrs',
                port: '',
                secure: true
            }
        }
    },

    userSettings: {
        customisation: {
            defaultExchange: 'KSE',
            defaultIndex: '11',
            defaultCurrency: 'KWD',
			defaultLanguage: 'AR'
        },

        orderConfig: {
            defaultOrderQty: 0
        }
    },

    tradeWidgetConfig: {
        accountSummary: {
            accountSummaryFields: [
                {lanKey: 'unsettledSales', dataField: 'penSet', formatter: 'C', style: '', isValueBaseCss: true},
                {lanKey: 'cashBalance', dataField: 'balance', formatter: 'C', style: '', isValueBaseCss: true},
                {lanKey: 'blockedAndOutstanding', dataField: 'blkAmt', formatter: 'C', style: '', isValueBaseCss: true},
                {lanKey: 'portfolioValue', dataField: 'valuation', formatter: 'C', style: '', isValueBaseCss: true},
                {lanKey: 'odLimit', dataField: 'odLmt', formatter: 'C', style: '', isValueBaseCss: true},
                {lanKey: 'buyingPower', dataField: 'buyPwr', formatter: 'C', style: '', isValueBaseCss: true},
                {lanKey: 'payableAmount', dataField: 'payAmt', formatter: 'C', style: ''},
                {lanKey: 'totalPortfolio', dataField: 'totVal', formatter: 'C', style: ''}
            ],

            decimalPlace: 4
        }
    }
};

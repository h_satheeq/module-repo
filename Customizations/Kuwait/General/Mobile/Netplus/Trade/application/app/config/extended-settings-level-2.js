export default {
    appConfig: {
        customisation: {
            authenticationMode: 4,
            isTradingEnabled: true,
            hashType: 'MD5',
            isUpperCasePassword: true,
            isPasswordChangeEnable: true
        },

        widgetId: {
            quoteMenuId: 'fullQuote',
            watchListMenuId: 'heatMap',
            marketMenuId: 'market',
            indicesTabId: 'companyProf',
            topStocksTabId: 'topStocks',
            portfolioMenuId: 'standard',
            orderTicketMenuId: 'classic'
        }
    },

    priceSettings: {
        configs: {
            priceTickerConfigs: {
                tickerSymDisplayField: 'sym'
            }
        }
    },

    tradeSettings: {
	channelId: 30,

        // Colombo TRS - Local
        connectionParameters: {
            primary: {
                ip: 'kse-wstrs-qa.directfn.net/trs',
                port: '',
                secure: false
            },

            secondary: {
                ip: 'kse-wstrs-qa.directfn.net/trs',
                port: '',
                secure: false
            }
        }
    },

    userSettings: {
        customisation: {
            defaultExchange: 'KSE',
            defaultIndex: '11',
            defaultCurrency: 'KWD',
			defaultLanguage: 'AR'
        },

        orderConfig: {
            defaultOrderQty: 0
        }
    },

    tradeWidgetConfig: {
        accountSummary: {
            accountSummaryFields: [
                {lanKey: 'unsettledSales', dataField: 'penSet', formatter: 'C', style: '', isValueBaseCss: true},
                {lanKey: 'cashBalance', dataField: 'balance', formatter: 'C', style: '', isValueBaseCss: true},
                {lanKey: 'blockedAndOutstanding', dataField: 'blkAmt', formatter: 'C', style: '', isValueBaseCss: true},
                {lanKey: 'portfolioValue', dataField: 'valuation', formatter: 'C', style: '', isValueBaseCss: true},
                {lanKey: 'odLimit', dataField: 'odLmt', formatter: 'C', style: '', isValueBaseCss: true},
                {lanKey: 'buyingPower', dataField: 'buyPwr', formatter: 'C', style: '', isValueBaseCss: true},
                {lanKey: 'payableAmount', dataField: 'payAmt', formatter: 'C', style: ''},
                {lanKey: 'totalPortfolio', dataField: 'totVal', formatter: 'C', style: ''}
            ],

            decimalPlace: 4
        }
    }
};

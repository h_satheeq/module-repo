export default {
    appConfig: {
        customisation: {
            authenticationMode: 4,
            isTradingEnabled: true,
            hashType: 'MD5',
            isUpperCasePassword: true,
            isPasswordChangeEnable: true,
            isAboutUsEnabled: true
        },

        widgetId: {
            quoteMenuId: 'fullQuote',
            watchListMenuId: 'heatMap',
            marketMenuId: 'market',
            indicesTabId: 'companyProf',
            topStocksTabId: 'topStocks',
            portfolioMenuId: 'standard',
            orderTicketMenuId: 'classic'
        }
    },

    priceSettings: {
        connectionParameters: {
            primary: {
                ip: 'ir.directfn.com/ws',
                port: '',
                secure: true // Is connection secure - true or false
            },

            secondary: {
                ip: 'ir.directfn.com/ws',
                port: '',
                secure: true // Is connection secure - true or false
            }
        }
    },

    tradeSettings: {
        channelId: 30,

        // Colombo TRS - Local
        connectionParameters: {
            primary: {
                ip: 'kse-wstrs-qa.directfn.net/trs',
                port: '',
                secure: false
            },

            secondary: {
                ip: 'kse-wstrs-qa.directfn.net/trs',
                port: '',
                secure: false
            }
        }
    },

    userSettings: {
        customisation: {
            defaultExchange: 'KSE',
            defaultIndex: '11',
            defaultCurrency: 'KWD',
			defaultLanguage: 'AR'
        }
    },

    priceWidgetConfig: {
        watchList: {
            defaultColumnMapping: {
                sym: {id: 'dataObj.dispProp1', sortKeyword: 'cid', type: 'dualText', isndicatorAvailable: true},
                ltp: {noOfSecValueDecimalPlaces: -1}
            },

            classicColumnMapping: {
                sym: {id: 'dataObj.dispProp1', sortKeyword: 'cid'}
            },

            quoteColumns: [{id: 'dispProp1', width: 65, headerName: 'symbol', secondId: 'sDes', headerStyle: 'text-left-header', sortKeyword: 'cid', type: 'dualText'},
                {id: 'trend', width: 22, name: 'trend', headerName: '', thirdId: 'trend', sortKeyword: 'chg', type: 'upDown'},
                {id: 'ltp', width: 48, headerName: 'last', sortKeyword: 'ltp', dataType: 'float', firstValueStyle: 'highlight-fore-color bold'},
                {id: 'chg', width: 54, headerName: 'change', headerSecondName: 'perChange', secondId: 'pctChg', sortKeyword: 'chg', positiveNegativeChange: true, type: 'dualChange', dataType: 'float'}],

            classicColumnIds: ['menu', 'sym', 'sDes', 'trend', 'ltp', 'ltq', 'chg', 'pctChg', 'vol', 'bbp', 'bbq', 'bap', 'baq', 'trades', 'ltd', 'dltt', 'intsV', 'open', 'high', 'low'],

            moreColumnIds: ['trend', 'ltp', 'chg', 'bbp', 'bap', 'l52', 'vol', 'tovr', 'trades', 'prvCls', 'cit', 'dltt', 'high', 'cvwap', 'max', 'twap', 'vwap'],

            classicMoreColumnIds: ['sDes', 'lDes', 'trend', 'isin', 'instDes', 'ltp', 'ltq', 'ltd', 'dltt', 'prvCls', 'chg', 'pctChg', 'vol', 'tovr', 'open', 'high', 'low', 'cls', 'bbp', 'bbq', 'bap', 'baq', 'h52', 'l52', 'trades', 'refValue', 'max', 'min', 'tbq', 'taq', 'eps', 'per', 'intsV', 'cit', 'cvwap', 'twap', 'vwap'],

            classicAssetTypes: {
                0: ['menu', 'sym', 'sDes', 'trend', 'ltp', 'ltq', 'chg', 'pctChg', 'bbp', 'bbq', 'bap', 'baq', 'vol', 'tovr', 'trades', 'h52'],    // Equity
                75: ['sym', 'sDes', 'trend', 'ltp', 'ltq', 'chg', 'pctChg', 'bbp', 'bbq', 'bap', 'baq', 'vol', 'tovr', 'trades', 'prvCls', 'open', 'high', 'low'],    // Bonds (Fixed Income)
                68: ['sym', 'sDes', 'trend', 'ltp', 'ltq', 'chg', 'pctChg', 'bbp', 'bbq', 'bap', 'baq', 'vol', 'tovr', 'trades', 'high', 'low'],  // Options/Future
                86: ['sym', 'sDes', 'trend', 'ltp', 'ltq', 'chg', 'pctChg', 'bbp', 'bbq', 'bap', 'baq', 'vol', 'tovr', 'trades', 'high', 'low'],  // ETF (Mutual Funds)
                11: ['sym', 'sDes', 'trend', 'ltp', 'ltq', 'chg', 'pctChg', 'bbp', 'bbq', 'bap', 'baq', 'vol', 'tovr', 'trades', 'high', 'low'],  // Currency
                7: ['sym', 'sDes', 'trend', 'ltp', 'chg', 'pctChg', 'high', 'low', 'vol', 'tovr', 'trades', 'h52', 'l52', 'prvCls', 'open']  // Index
            },

            assetTypes: {
                0: ['menu', 'sym', 'trend', 'ltp', 'chg', 'bbp', 'bap', 'l52', 'vol', 'tovr', 'trades', 'prvCls', 'cit', 'dltt', 'high'],    // Equity
                75: ['sym', 'trend', 'ltp', 'chg', 'bbp', 'bap', 'vol', 'tovr', 'trades', 'prvCls', 'high'],    // Bonds (Fixed Income)
                68: ['sym', 'trend', 'ltp', 'chg', 'bbp', 'bap', 'vol', 'tovr', 'trades', 'high'],  // Options/Future
                86: ['sym', 'trend', 'ltp', 'chg', 'bbp', 'bap', 'vol', 'tovr', 'trades', 'high'],  // ETF (Mutual Funds)
                11: ['sym', 'trend', 'ltp', 'chg', 'bbp', 'bap', 'vol', 'tovr', 'trades', 'high'],  // Currency
                7: ['sym', 'trend', 'ltp', 'chg', 'high', 'vol', 'tovr', 'trades', 'l52', 'prvCls']  // Index
            }
        }
    },

    tradeWidgetConfig: {
        portfolio: {
            defaultColumnMapping: {     // Column Object parameters : id, width, headerName, sortKeyword, multiValueIds, cellStyle, sortDisable, firstValueStyle, isBold, dataType, backgroundColour,
                sym: {id: 'symbolInfo.dispProp1', sortKeyword: 'cid'},
                sDes: {id: 'sDes'},
                mktPrice: {isFieldConfigAvailable: true},
                avgCst: {isFieldConfigAvailable: true},
                costVal: {isFieldConfigAvailable: true},
                mktVal: {isFieldConfigAvailable: true},
                gainLoss: {isFieldConfigAvailable: true},
                gainLossPer: {noOfDecimalPlaces: 2},
                portPer: {noOfDecimalPlaces: 2}
            },

            defaultColumnIds: ['buyMore', 'liquidate', 'sym', 'sDes', 'exg', 'qty', 'pendSell', 'pendBuy', 'avaiQty', 'mktPrice', 'avgCst', 'costVal', 'mktVal', 'gainLoss', 'gainLossPer', 'portPer', 'curr'],

            moreColumnIds: ['sDes', 'exg', 'qty', 'pendSell', 'pendBuy', 'avaiQty', 'mktPrice', 'avgCst', 'costVal', 'mktVal', 'gainLoss', 'gainLossPer', 'portPer', 'curr', 'secAccNum'],

            footerParams: {
                // Format Accepting:- data type = string [eg: 'sDes'].
                // Not Accepting:- data type = int, float (other than string) [eg: 'qty'] & Properties like [eg: 'symbolInfo.sDes'].
                totalValueProp: 'sDes'
            }
        },

        orderList: {
            defaultColumnMapping: {     // Column Object parameters : id, width, headerName, sortKeyword, multiValueIds, cellStyle, sortDisable, firstValueStyle, isBold, dataType, backgroundColour,
                sym: {id: 'symbolInfo.dispProp1', sortKeyword: 'cid'},
                price: {noOfDecimalPlaces: 'symbolInfo.deci'},
                avgPrice: {noOfDecimalPlaces: 'symbolInfo.deci'},
                ordVal: {noOfDecimalPlaces: 'symbolInfo.deci'},
                netOrdVal: {noOfDecimalPlaces: 'symbolInfo.deci'}
            }
        },

        orderSearch: {
            defaultColumnMapping: {     // Column Object parameters : id, width, headerName, sortKeyword, multiValueIds, cellStyle, sortDisable, firstValueStyle, isBold, dataType, backgroundColour,
                sym: {id: 'symbolInfo.dispProp1', sortKeyword: 'cid'},
                price: {noOfDecimalPlaces: 'symbolInfo.deci'},
                ordVal: {noOfDecimalPlaces: 'symbolInfo.deci'}
            }
        },

        cashStatement: {
            defaultColumnMapping: {     // Column Object parameters : id, width, headerCellView, headerName, sortKeyword,  cellStyle, sortDisable, dataType, type, headerStyle, cellStyle
                particulars: {id: 'des', firstValueStyle: 'colour-normal font-l'},
                amount: {width: 120}
            }
        }
    }
};

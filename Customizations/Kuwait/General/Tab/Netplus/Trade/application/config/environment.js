/* jshint node: true */
var EnvironmentExtended = require('./environment-extended.js');

module.exports = function (environment) {
    var ENV = new EnvironmentExtended(environment);
    ENV.baseURL = '/';

    ENV.APP.tradeConnectionParameters = {
        primary: {
            ip: 'kse-wstrs-qa.directfn.net/trs',
            port: '',
            secure: false
        }
    };

    ENV.APP.priceConnectionParameters = {
        primary: {
            ip: 'ir.directfn.com/ws',
            port: '',
            secure: true
        }
    };

    ENV.contentSecurityPolicy['connect-src'] = "'self' www.google-analytics.com wss://ir.directfn.com/ws wss://data-sa9.mubasher.net/html5ws ws://kse-wstrs-qa.directfn.net/trs";

    return ENV;
};

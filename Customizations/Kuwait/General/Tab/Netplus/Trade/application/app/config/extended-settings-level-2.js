export default {
    appConfig: {
        customisation: {
            isTablet: true,
            isHorizontalTickerAnimation: true,
            isPasswordChangeEnable: true,
            isAboutUsEnabled: true,
            hidePreLogin: true
        }
    },

    priceWidgetConfig: {
        watchList: {
            defaultColumnMapping: {
                sym: {id: 'dataObj.dispProp1', sortKeyword: 'cid'},
                ltp: {noOfSecValueDecimalPlaces: -1}
            },

            classicColumnMapping: {
                sym: {id: 'dataObj.dispProp1', sortKeyword: 'cid', type: 'classicCell'}
            },

            quoteColumns: [{id: 'dispProp1', width: 65, headerName: 'symbol', secondId: 'sDes', headerStyle: 'text-left-header', sortKeyword: 'cid', type: 'dualText'},
                {id: 'trend', width: 22, name: 'trend', headerName: '', thirdId: 'trend', sortKeyword: 'chg', type: 'upDown'},
                {id: 'ltp', width: 48, headerName: 'last', sortKeyword: 'ltp', dataType: 'float', firstValueStyle: 'highlight-fore-color bold'},
                {id: 'chg', width: 54, headerName: 'change', headerSecondName: 'perChange', secondId: 'pctChg', sortKeyword: 'chg', positiveNegativeChange: true, type: 'dualChange', dataType: 'float'}],

            defaultColumnIds: ['sym', 'trend', 'ltp', 'chg', 'bbp', 'bap', 'l52', 'vol', 'tovr', 'trades', 'prvCls', 'cit', 'dltt', 'high'],

            classicColumnIds: ['sym', 'sDes', 'trend', 'ltp', 'ltq', 'chg', 'pctChg', 'vol', 'bbp', 'bbq', 'bap', 'baq', 'trades', 'ltd', 'dltt', 'open', 'high', 'low'],

            moreColumnIds: ['trend', 'ltp', 'chg', 'bbp', 'bap', 'l52', 'vol', 'tovr', 'trades', 'prvCls', 'cit', 'dltt', 'high', 'cvwap', 'max', 'twap', 'vwap'],

            classicMoreColumnIds: ['sDes', 'lDes', 'trend', 'isin', 'instDes', 'ltp', 'ltq', 'ltd', 'dltt', 'prvCls', 'chg', 'pctChg', 'vol', 'tovr', 'open', 'high', 'low', 'cls', 'bbp', 'bbq', 'bap', 'baq', 'h52', 'l52', 'trades', 'refValue', 'max', 'min', 'tbq', 'taq', 'eps', 'per', 'intsV', 'cit', 'cvwap', 'twap', 'vwap'],

            customDefaultColumnIds: ['sym', 'exg', 'trend', 'ltp', 'chg', 'bbp', 'bap', 'l52', 'vol', 'tovr', 'trades', 'prvCls', 'cit', 'dltt', 'high'],

            customClassicColumnIds: ['sym', 'sDes', 'exg', 'trend', 'ltp', 'ltq', 'chg', 'pctChg', 'bbp', 'bbq', 'bap', 'baq', 'vol', 'tovr', 'trades', 'cit', 'h52', 'l52', 'prvCls', 'open', 'ltd', 'dltt', 'high', 'low'],

            nonRemovableColumnIds: ['sym'],

            classicAssetTypes: {
                0: ['sym', 'sDes', 'trend', 'ltp', 'ltq', 'chg', 'pctChg', 'bbp', 'bbq', 'bap', 'baq', 'vol', 'tovr', 'trades', 'h52'],    // Equity
                75: ['sym', 'sDes', 'trend', 'ltp', 'ltq', 'chg', 'pctChg', 'bbp', 'bbq', 'bap', 'baq', 'vol', 'tovr', 'trades', 'prvCls', 'open', 'high', 'low'],    // Bonds (Fixed Income)
                68: ['sym', 'sDes', 'trend', 'ltp', 'ltq', 'chg', 'pctChg', 'bbp', 'bbq', 'bap', 'baq', 'vol', 'tovr', 'trades', 'high', 'low'],  // Options/Future
                86: ['sym', 'sDes', 'trend', 'ltp', 'ltq', 'chg', 'pctChg', 'bbp', 'bbq', 'bap', 'baq', 'vol', 'tovr', 'trades', 'high', 'low'],  // ETF (Mutual Funds)
                11: ['sym', 'sDes', 'trend', 'ltp', 'ltq', 'chg', 'pctChg', 'bbp', 'bbq', 'bap', 'baq', 'vol', 'tovr', 'trades', 'high', 'low'],  // Currency
                7: ['sym', 'sDes', 'trend', 'ltp', 'chg', 'pctChg', 'high', 'low', 'vol', 'tovr', 'trades', 'h52', 'l52', 'prvCls', 'open']  // Index
            },

            assetTypes: {
                0: ['sym', 'trend', 'ltp', 'chg', 'bbp', 'bap', 'l52', 'vol', 'tovr', 'trades', 'prvCls', 'cit', 'dltt', 'high'],    // Equity
                75: ['sym', 'trend', 'ltp', 'chg', 'bbp', 'bap', 'vol', 'tovr', 'trades', 'prvCls', 'high'],    // Bonds (Fixed Income)
                68: ['sym', 'trend', 'ltp', 'chg', 'bbp', 'bap', 'vol', 'tovr', 'trades', 'high'],  // Options/Future
                86: ['sym', 'trend', 'ltp', 'chg', 'bbp', 'bap', 'vol', 'tovr', 'trades', 'high'],  // ETF (Mutual Funds)
                11: ['sym', 'trend', 'ltp', 'chg', 'bbp', 'bap', 'vol', 'tovr', 'trades', 'high'],  // Currency
                7: ['sym', 'trend', 'ltp', 'chg', 'high', 'vol', 'tovr', 'trades', 'l52', 'prvCls']  // Index
            },

            tableParams: {
                MinHeaderHeight: {standard: 36, classic: 30},
                RowHeight: {standard: 42, classic: 32},
                numOfFixedColumns: 1
            }
        },

        timeAndSales: {
            defaultColumnMapping: {     // Column Object parameters : id, width, headerName, sortKeyword, multiValueIds, cellStyle, sortDisable, firstValueStyle, isBold, dataType, backgroundColour,
                dDt: {width: 61},
                trp: {width: 50},
                splits: {width: 33},
                tick: {width: 25}
            },

            defaultColumnIds: ['dDt', 'trp', 'trq', 'pctChg', 'splits', 'tick', 'trdType']
        },

        topStocks: {
            // TopGainersByChange
            0: {
                fields: [
                    {filed: 'description', objName: 'dDesc', fontColor: 'symbol-fore-color', textAlign: 'h-left', bold: 'bold', padding: 'pad-widget-left'},
                    {filed: 'last', objName: 'ltp', fontColor: 'fore-color', textAlign: 'h-right', formatter: 'formatNumberWithDeci', padding: 'pad-m-l', col: 'layout-col-24'},
                    {filed: 'change', objName: 'chg', fontColor: 'redOrGreen', textAlign: 'h-right ltr', formatter: 'formatNumberWithDeci', bold: 'bold', padding: 'pad-m-l'},
                    {filed: 'perChange', objName: 'pctChg', fontColor: 'redOrGreen', textAlign: 'h-right ltr', formatter: 'formatNumberPercentage', padding: 'pad-m-l'},
                    {filed: 'volume', objName: 'vol', fontColor: 'fore-color', textAlign: 'h-right', formatter: 'formatNumber', padding: 'pad-m-l'},
                    {filed: 'trades', objName: 'trades', fontColor: 'fore-color', textAlign: 'h-right', formatter: 'formatNumber', padding: 'pad-m-l'},
                    {filed: 'turnover', objName: 'tovr', fontColor: 'fore-color', textAlign: 'h-right', formatter: 'formatNumber', padding: 'pad-m-l pad-widget-right'}
                ],
                title: 'topGainers',
                icon: 'glyphicon-triangle-top up-fore-color',
                showTopStockTabs: true
            },
            // TopGainersByPercentageChange
            1: {
                fields: [
                    {filed: 'description', objName: 'dDesc', fontColor: 'symbol-fore-color', textAlign: 'h-left', bold: 'bold', padding: 'pad-widget-left'},
                    {filed: 'last', objName: 'ltp', fontColor: 'fore-color', textAlign: 'h-right', formatter: 'formatNumberWithDeci', col: 'layout-col-24', padding: 'pad-m-l'},
                    {filed: 'change', objName: 'chg', fontColor: 'redOrGreen', textAlign: 'h-right ltr', formatter: 'formatNumberWithDeci', padding: 'pad-m-l'},
                    {filed: 'perChange', objName: 'pctChg', fontColor: 'redOrGreen', textAlign: 'h-right ltr', formatter: 'formatNumberPercentage', bold: 'bold', padding: 'pad-m-l'},
                    {filed: 'volume', objName: 'vol', fontColor: 'fore-color', textAlign: 'h-right', formatter: 'formatNumber', padding: 'pad-m-l'},
                    {filed: 'trades', objName: 'trades', fontColor: 'fore-color', textAlign: 'h-right', formatter: 'formatNumber', padding: 'pad-m-l'},
                    {filed: 'turnover', objName: 'tovr', fontColor: 'fore-color', textAlign: 'h-right', formatter: 'formatNumber', padding: 'pad-m-l pad-widget-right'}
                ],
                title: 'topGainers',
                icon: 'glyphicon-triangle-top up-fore-color',
                showTopStockTabs: true
            },
            // TopLosersByChange
            2: {
                fields: [
                    {filed: 'description', objName: 'dDesc', fontColor: 'symbol-fore-color', textAlign: 'h-left', bold: 'bold', padding: 'pad-widget-left'},
                    {filed: 'last', objName: 'ltp', fontColor: 'fore-color', textAlign: 'h-right', formatter: 'formatNumberWithDeci', padding: 'pad-m-l', col: 'layout-col-24'},
                    {filed: 'change', objName: 'chg', fontColor: 'redOrGreen', textAlign: 'h-right ltr', formatter: 'formatNumberWithDeci', bold: 'bold', padding: 'pad-m-l'},
                    {filed: 'perChange', objName: 'pctChg', fontColor: 'redOrGreen', textAlign: 'h-right ltr', formatter: 'formatNumberPercentage', padding: 'pad-m-l'},
                    {filed: 'volume', objName: 'vol', fontColor: 'fore-color', textAlign: 'h-right', formatter: 'formatNumber', padding: 'pad-m-l'},
                    {filed: 'trades', objName: 'trades', fontColor: 'fore-color', textAlign: 'h-right', formatter: 'formatNumber', padding: 'pad-m-l'},
                    {filed: 'turnover', objName: 'tovr', fontColor: 'fore-color', textAlign: 'h-right', formatter: 'formatNumber', padding: 'pad-m-l pad-widget-right'}
                ],
                title: 'topLosers',
                icon: 'glyphicon-triangle-bottom down-fore-color',
                showTopStockTabs: true
            },
            // TopLosersByPercentageChange
            3: {
                fields: [
                    {filed: 'description', objName: 'dDesc', fontColor: 'symbol-fore-color', textAlign: 'h-left', bold: 'bold', padding: 'pad-widget-left'},
                    {filed: 'last', objName: 'ltp', fontColor: 'fore-color', textAlign: 'h-right', formatter: 'formatNumberWithDeci', padding: 'pad-m-l', col: 'layout-col-24'},
                    {filed: 'change', objName: 'chg', fontColor: 'redOrGreen', textAlign: 'h-right ltr', formatter: 'formatNumberWithDeci', padding: 'pad-m-l'},
                    {filed: 'perChange', objName: 'pctChg', fontColor: 'redOrGreen', textAlign: 'h-right ltr', formatter: 'formatNumberPercentage', bold: 'bold', padding: 'pad-m-l'},
                    {filed: 'volume', objName: 'vol', fontColor: 'fore-color', textAlign: 'h-right', formatter: 'formatNumber', padding: 'pad-m-l'},
                    {filed: 'trades', objName: 'trades', fontColor: 'fore-color', textAlign: 'h-right', formatter: 'formatNumber', padding: 'pad-m-l'},
                    {filed: 'turnover', objName: 'tovr', fontColor: 'fore-color', textAlign: 'h-right', formatter: 'formatNumber', padding: 'pad-m-l pad-widget-right'}
                ],
                title: 'topLosers',
                icon: 'glyphicon-triangle-bottom down-fore-color',
                showTopStockTabs: true
            },
            // MostActiveByVolume
            4: {
                fields: [
                    {filed: 'description', objName: 'dDesc', fontColor: 'symbol-fore-color', textAlign: 'h-left', bold: 'bold', padding: 'pad-widget-left'},
                    {filed: 'last', objName: 'ltp', fontColor: 'fore-color', textAlign: 'h-right', formatter: 'formatNumberWithDeci', padding: 'pad-m-l', col: 'layout-col-24'},
                    {filed: 'change', objName: 'chg', fontColor: 'redOrGreen', textAlign: 'h-right ltr', formatter: 'formatNumberWithDeci', padding: 'pad-m-l'},
                    {filed: 'perChange', objName: 'pctChg', fontColor: 'redOrGreen', textAlign: 'h-right ltr', formatter: 'formatNumberPercentage', padding: 'pad-m-l'},
                    {filed: 'volume', objName: 'vol', fontColor: 'highlight-fore-color', textAlign: 'h-right', formatter: 'formatNumber', bold: 'bold', padding: 'pad-m-l pad-widget-right'}

                ],
                title: 'MActiveByVol',
                showTopStockTabs: false
            },
            // MostActiveByTrades
            5: {
                fields: [
                    {filed: 'description', objName: 'dDesc', fontColor: 'symbol-fore-color', textAlign: 'h-left', bold: 'bold', padding: 'pad-widget-left'},
                    {filed: 'last', objName: 'ltp', fontColor: 'fore-color', textAlign: 'h-right', formatter: 'formatNumberWithDeci', padding: 'pad-m-l', col: 'layout-col-24'},
                    {filed: 'change', objName: 'chg', fontColor: 'redOrGreen', textAlign: 'h-right ltr', formatter: 'formatNumberWithDeci', padding: 'pad-m-l'},
                    {filed: 'perChange', objName: 'pctChg', fontColor: 'redOrGreen', textAlign: 'h-right ltr', formatter: 'formatNumberPercentage', padding: 'pad-m-l'},
                    {filed: 'trades', objName: 'trades', fontColor: 'highlight-fore-color', textAlign: 'h-right', formatter: 'formatNumber', bold: 'bold', padding: 'pad-m-l pad-widget-right'}
                ],
                title: 'MActiveByTrades',
                showTopStockTabs: false
            },
            // MostActiveByTurnover
            6: {
                fields: [
                    {filed: 'description', objName: 'dDesc', fontColor: 'symbol-fore-color', textAlign: 'h-left', bold: 'bold', padding: 'pad-widget-left'},
                    {filed: 'last', objName: 'ltp', fontColor: 'fore-color', textAlign: 'h-right', formatter: 'formatNumberWithDeci', padding: 'pad-m-l', col: 'layout-col-24'},
                    {filed: 'change', objName: 'chg', fontColor: 'redOrGreen', textAlign: 'h-right ltr', formatter: 'formatNumberWithDeci', padding: 'pad-m-l'},
                    {filed: 'perChange', objName: 'pctChg', fontColor: 'redOrGreen', textAlign: 'h-right ltr', formatter: 'formatNumberPercentage', padding: 'pad-m-l'},
                    {filed: 'turnover', objName: 'tovr', fontColor: 'highlight-fore-color', textAlign: 'h-right', formatter: 'formatNumber', bold: 'bold', padding: 'pad-m-l pad-widget-right'}
                ],
                title: 'MActiveByTurnover',
                showTopStockTabs: false
            }
        }
    },

    tradeWidgetConfig: {
        portfolio: {
            defaultColumnMapping: {     // Column Object parameters : id, width, headerName, sortKeyword, multiValueIds, cellStyle, sortDisable, firstValueStyle, isBold, dataType, backgroundColour,
                sym: {id: 'symbolInfo.dispProp1', sortKeyword: 'cid'},
                sDes: {id: 'sDes'},
                mktPrice: {isFieldConfigAvailable: true},
                avgCst: {isFieldConfigAvailable: true},
                costVal: {isFieldConfigAvailable: true},
                mktVal: {isFieldConfigAvailable: true},
                gainLoss: {isFieldConfigAvailable: true},
                gainLossPer: {noOfDecimalPlaces: 2},
                portPer: {noOfDecimalPlaces: 2}
            },

            defaultColumnIds: ['buyMore', 'liquidate', 'sym', 'sDes', 'exg', 'qty', 'pendSell', 'pendBuy', 'avaiQty', 'mktPrice', 'avgCst', 'costVal', 'mktVal', 'gainLoss', 'gainLossPer', 'portPer', 'curr'],

            moreColumnIds: ['sDes', 'exg', 'qty', 'pendSell', 'pendBuy', 'avaiQty', 'mktPrice', 'avgCst', 'costVal', 'mktVal', 'gainLoss', 'gainLossPer', 'portPer', 'curr'],

            footerParams: {
                // Format Accepting:- data type = string [eg: 'sDes'].
                // Not Accepting:- data type = int, float (other than string) [eg: 'qty'] & Properties like [eg: 'symbolInfo.sDes'].
                totalValueProp: 'sDes'
            }
        },

        orderList: {
            defaultColumnMapping: {     // Column Object parameters : id, width, headerName, sortKeyword, multiValueIds, cellStyle, sortDisable, firstValueStyle, isBold, dataType, backgroundColour,
                orderDetailPopup: {id: 'orderDetailPopup', width: 25, headerCellView: 'Ember.ClassicHeaderCell', name: 'orderDetails', headerName: '', headerStyle: 'text-center', iconClass: 'glyphicon icon-info-circle', isColumnSortDisabled: true, type: 'button', title: 'orderDetails', buttonFunction: 'getOrderDetailsPopup'},
                sym: {id: 'symbolInfo.dispProp1', sortKeyword: 'cid'},
                clOrdId: {width: 110},
                ordSts: {width: 110},
                price: {noOfDecimalPlaces: 'symbolInfo.deci'},
                avgPrice: {noOfDecimalPlaces: 'symbolInfo.deci'},
                ordVal: {noOfDecimalPlaces: 'symbolInfo.deci'},
                netOrdVal: {noOfDecimalPlaces: 'symbolInfo.deci'}
            },

            defaultColumnIds: ['isAmendEnabled', 'isCancelEnabled', 'orderDetailPopup', 'sym', 'clOrdId', 'ordSts', 'ordSide', 'ordQty', 'price', 'cumQty', 'pendQty', 'avgPrice', 'ordVal', 'netOrdVal', 'ordTyp', 'adjustedCrdDte', 'adjustedExpTime'],

            nonRemovableColumnIds: ['isAmendEnabled', 'isCancelEnabled', 'orderDetailPopup', 'sym']
        },

        orderSearch: {
            defaultColumnMapping: {     // Column Object parameters : id, width, headerName, sortKeyword, multiValueIds, cellStyle, sortDisable, firstValueStyle, isBold, dataType, backgroundColour,
                sym: {id: 'symbolInfo.dispProp1', sortKeyword: 'cid'},
                ordSts: {width: 110},
                price: {noOfDecimalPlaces: 'symbolInfo.deci'},
                ordVal: {noOfDecimalPlaces: 'symbolInfo.deci'}
            }
        },

        accountSummary: {
            decimalPlace: 4
        }
    }
};

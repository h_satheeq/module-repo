/* jshint node: true */
var EnvironmentCore = require('./environment-core.js');

module.exports = function (environment) {
    var ENV = new EnvironmentCore(environment);

    ENV.baseURL = '/tradeApp';
    ENV.contentSecurityPolicy['connect-src'] = "'self' www.google-analytics.com wss://itrade.lbsbd.com/price ws://itrade.lbsbd.com/";

    if (ENV.APP.isTestMode) {
        ENV.APP.tradeConnectionParameters = {
            primary: {
                ip: 'itrade.lbsbd.com/wst',
                port: '',
                secure: true
            }
        };

        ENV.APP.priceConnectionParameters = {
            primary: {
                ip: 'itrade.lbsbd.com/price',
                port: '',
                secure: true
            }
        };
    }

    return ENV;
};

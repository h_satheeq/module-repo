export default {
    appConfig: {
        customisation: {
            appName: 'LankaBangla',
            countryAddress: 'lbslAddress',
            isAboutUsEnabled: true,

            supportedContacts: [
                {key: 'email', value: 'info@lbsbd.com', type: 'E'},
                {key: 'url', value: 'www.lbsbd.com', type: 'U'},
                {key: 'phn', value: '+880 2956 39 01', type: 'T'},
                {key: 'phn', value: '+880 2956 39 03-5', type: 'T'},
                {key: 'fax', value: '+880 2956 39 02', type: 'T'}
            ],

            loginViewSettings: {
                showTermsAndConditions: true,
                termsAndConditionURL: 'https://itrade.lbsbd.com/t&c/'
            }
        }
    },

    tradeSettings: {
        connectionParameters: {
            primary: {
                ip: 'itrade.lbsbd.com/wst',
                port: '',
                secure: true // Is connection secure - true or false
            },

            secondary: {
                ip: 'itrade.lbsbd.com/wst',
                port: '',
                secure: true // Is connection secure - true or false
            }
        }
    },

	priceSettings: {
        connectionParameters: {
            primary: {
                ip: 'itrade.lbsbd.com/price',
                port: '',
                secure: true
            },

            secondary: {
                ip: 'itrade.lbsbd.com/price',
                port: '',
                secure: true
            }
        }
    },

	priceWidgetConfig: {
        gms: [
            {sym: 'EURUSD', exg: 'GLOBAL', inst: '0', sDes: 'EURUSD', icon: 'comm-icon icon-usd'},
            {sym: 'USDJPY', exg: 'GLOBAL', inst: '0', sDes: 'USDJPY', icon: 'comm-icon icon-usd'},
            {sym: 'GBPUSD', exg: 'GLOBAL', inst: '0', sDes: 'GBPUSD', icon: 'comm-icon icon-usd'},
            {sym: 'USDLKR', exg: 'GLOBAL', inst: '0', sDes: 'USDLKR', icon: 'comm-icon icon-usd'},
            {sym: 'USDAUD', exg: 'GLOBAL', inst: '0', sDes: 'USDAUD', icon: 'comm-icon icon-usd'}
        ]
    }
};
export default {
    appConfig: {
        widgetId: {
            quoteMenuId: 'fullQuote',
            watchListMenuId: 'heatMap',
            marketMenuId: 'market',
            indicesTabId: 'companyProf',
            topStocksTabId: 'topStocks',
            portfolioMenuId: 'standard',
            orderTicketMenuId: 'classic'
        }
    },

    tradeSettings: {
        channelId: 22,

        connectionParameters: {
            primary: {
                ip: 'itrade.lbsbd.com/wst',
                port: '',
                secure: true // Is connection secure - true or false
            },

            secondary: {
                ip: 'itrade.lbsbd.com/wst',
                port: '',
                secure: true // Is connection secure - true or false
            }
        }
    },

    tradeWidgetConfig: {
        cashStatement: {
            defaultColumnMapping: {     // Column Object parameters : id, width, headerCellView, headerName, sortKeyword,  cellStyle, sortDisable, dataType, type, headerStyle, cellStyle
                date: {width: 80},
                des: {width: 185},
                amount: {width: 90}
            }
        }
    }
};
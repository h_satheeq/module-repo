/* jshint node: true */
var EnvironmentExtended = require('./environment-extended.js');

module.exports = function (environment) {
    var ENV = new EnvironmentExtended(environment);

    ENV.baseURL = '/tradeExpressMobileApp';
    ENV.contentSecurityPolicy['connect-src'] = "'self' www.google-analytics.com wss://itrade.lbsbd.com/price wss://itrade.lbsbd.com";
	ENV.locationType = 'none';
	
	ENV.APP.tradeConnectionParameters = {
        primary: {
            ip: 'itrade.lbsbd.com/wst',
            port: '',
            secure: true
        }
    };
	
    return ENV;
};
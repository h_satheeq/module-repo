export default {
    appConfig: {
        customisation: {
            profileServiceEnabled: false,
            smartLoginEnabled: false,
            supportedLanguages: [{code: 'EN', desc: 'English'}],
            hidePreLogin: true
        },

        loggerConfig: {
            serverLogLevel: 0
        }
    },

    priceSettings: {
        connectionParameters: {
            primary: {
                ip: 'lkcentralprice.directfn.com/price',
                port: '',
                secure: true
            },

            secondary: {
                ip: 'lkcentralprice.directfn.com/price',
                port: '',
                secure: true
            }
        }
    },

    userSettings: {
        customisation: {
            defaultExchange: 'XCHG',
            defaultIndex: 'CSCX',
            defaultCurrency: 'BDT'
        }
    }
};

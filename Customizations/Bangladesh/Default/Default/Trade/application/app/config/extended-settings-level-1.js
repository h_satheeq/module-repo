export default {
    appConfig: {
        customisation: {
            authenticationMode: 4,
            isTradingEnabled: true,
            profileServiceEnabled: false,
            smartLoginEnabled: false,
            isPasswordChangeEnable: true,
            supportedLanguages: [{code: 'EN', desc: 'English'}],
            hashType: 'MD5',
            hidePreLogin: true,
            isAlertEnabled: false
        },

        loggerConfig: {
            serverLogLevel: 0
        }
    },

    priceSettings: {
        connectionParameters: {
            primary: {
                ip: 'itrade.lbsbd.com/price',
                port: '',
                secure: true
            },

            secondary: {
                ip: 'itrade.lbsbd.com/price',
                port: '',
                secure: true
            }
        },

        configs: {
            customWindowTypes: {
                XCHG: {
                    include: [],
                    exclude: ['20']
                },

                GLOBAL: {
                    include: [],
                    exclude: ['20']
                }
            }
        }
    },

    tradeSettings: {
        channelId: 22,

        connectionParameters: {
            primary: {
                ip: '192.168.13.244',
                port: '8800',
                secure: false
            },

            secondary: {
                ip: '192.168.13.244',
                port: '8800',
                secure: false
            }
        },

        orderConfig: {
            isValidateHoldings: true
        }
    },

    userSettings: {
        customisation: {
            defaultExchange: 'XCHG',
            defaultIndex: 'CSCX',
            defaultCurrency: 'BDT'
        }
    },

    tradeWidgetConfig: {
        portfolio: {
            defaultColumnIds: ['liquidate', 'sym', 'qty', 'pendSell', 'pendBuy', 'avaiQty', 'penHolding', 'mktPrice', 'avgCst', 'costVal', 'mktVal', 'gainLoss', 'gainLossPer', 'portPer', 'curr', 'exg'],

            nonRemovableColumnIds: ['liquidate', 'sym']
        },

        orderDetails: {
            orderDetailsPopup: [
                {lanKey: 'exgOrderNo', dataField: 'ordId', formatter: 'S', style: ''},
                {lanKey: 'orderId', dataField: 'clOrdId', formatter: 'S', style: ''},
                {lanKey: 'side', dataField: 'ordSide', formatter: 'S', style: '', lanKeyAppend: 'orderSide', isCustomStyle: true},
                {lanKey: 'status', dataField: 'ordSts', formatter: 'S', style: '', lanKeyAppend: 'orderStatus', isCustomStyle: true},
                {lanKey: 'quantity', dataField: 'ordQty', formatter: 'I', style: ''},
                {lanKey: 'pendingQty', dataField: 'pendQty', formatter: 'I', style: ''},
                {lanKey: 'filledQuantity', dataField: 'cumQty', formatter: 'I', style: ''},
                {lanKey: 'orderType', dataField: 'ordTyp', formatter: 'S', style: '', lanKeyAppend: 'orderType'},
                {lanKey: 'price', dataField: 'price', formatter: 'C', style: ''},
                {lanKey: 'averagePrice', dataField: 'avgPrice', formatter: 'C', style: ''},
                {lanKey: 'date', dataField: 'adjustedCrdDte', formatter: 'S', style: ''},
                {lanKey: 'expiryDate', dataField: 'adjustedExpTime', formatter: 'S', style: ''},
                {lanKey: 'maximumPrice', dataField: 'maxPrc', formatter: 'C', style: ''},
                {lanKey: 'disclosedQty', dataField: 'disQty', formatter: 'I', style: ''},
                {lanKey: 'symbolType', dataField: 'instruTyp', formatter: 'S', style: '', isAssetType: true},
                {lanKey: 'tif', dataField: 'tif', formatter: 'S', style: '', lanKeyAppend: 'tifType'},
                {lanKey: 'commission', dataField: 'comsn', formatter: 'C', style: ''},
                {lanKey: 'netValue', dataField: 'netOrdVal', formatter: 'I', style: ''},
                {lanKey: 'orderValue', dataField: 'ordVal', formatter: 'C', style: ''}
            ]
        }
    },

    priceWidgetConfig: {
        watchList: {
            moreColumnIds: ['trend', 'ltp', 'chg', 'bbp', 'bap', 'l52', 'vol', 'tovr', 'trades', 'prvCls', 'cit', 'dltt', 'high', 'cvwap', 'max', 'twap', 'vwap'],

            classicMoreColumnIds: ['sDes', 'lDes', 'trend', 'isin', 'instDes', 'ltp', 'ltq', 'ltd', 'dltt', 'prvCls', 'chg', 'pctChg', 'vol', 'tovr', 'open', 'high', 'low', 'cls', 'bbp', 'bbq', 'bap', 'baq', 'h52', 'l52', 'trades', 'refValue', 'max', 'min', 'tbq', 'taq', 'eps', 'per', 'intsV', 'cit', 'cvwap', 'twap', 'vwap']
        },

        quote: {
            panelIntraday: {
                // Equity
                '1': [
                    {lanKey: 'lastTrade', dataField: 'ltp', formatter: 'C', style: 'highlight-fore-color'},
                    {lanKey: 'lastQty', dataField: 'ltq', formatter: 'L', style: 'highlight-fore-color'},
                    {lanKey: 'open', dataField: 'open', formatter: 'C'},
                    {lanKey: 'close', dataField: 'cls', formatter: 'C'},
                    {lanKey: 'preClosed', dataField: 'prvCls', formatter: 'C'},
                    {lanKey: 'vWAP', dataField: 'vwap', formatter: 'C', detailQouteTitle: 'vwapDesc'},
                    {lanKey: 'volume', dataField: 'vol', formatter: 'L', style: 'highlight-fore-color'},
                    {lanKey: 'turnover', dataField: 'tovr', formatter: 'L', style: 'highlight-fore-color'},
                    {lanKey: 'trades', dataField: 'trades', formatter: 'L', style: 'highlight-fore-color'},
                    {lanKey: 'mktCap', dataField: 'mktCap', formatter: 'DN'},
                    {lanKey: 'high', dataField: 'high', formatter: 'C'},
                    {lanKey: 'low', dataField: 'low', formatter: 'C'},
                    {lanKey: 'bid', dataField: 'bbp', formatter: 'C', style: 'up-fore-color'},
                    {lanKey: 'offer', dataField: 'bap', formatter: 'C', style: 'down-fore-color'},
                    {lanKey: 'bidQty', dataField: 'bbq', formatter: 'L', style: 'up-fore-color'},
                    {lanKey: 'offerQty', dataField: 'baq', formatter: 'L', style: 'down-fore-color'},
                    {lanKey: 'fiftyTwoWkH', dataField: 'h52', formatter: 'C'},
                    {lanKey: 'fiftyTwoWkL', dataField: 'l52', formatter: 'C'}
                ]
            }
        }
    }
};

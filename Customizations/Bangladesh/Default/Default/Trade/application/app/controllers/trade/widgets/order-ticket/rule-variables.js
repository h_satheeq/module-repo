import Ember from 'ember';

export default Ember.Object.extend({
    disclosedMargin: {default: 0},
    minMargin: {default: 0},
    amendAllowedStsByExg: {default: []},
    cancelAllowedStsByExg: {default: []},
    defaultExgConfig: {},
    tickRule: {
        '*': {10: 0.1, 25: 0.1, 50: 0.1, 100: 0.1, '*': 0.1}
    },

    /* *
     * Add default values for all exchange specific variable maps
     */
    init: function () {
        var amendAllowedStsByExg = this.get('amendAllowedStsByExg');

        amendAllowedStsByExg.default = ['0', 'A', '1', '5'];
        amendAllowedStsByExg.XCHG = ['0', 'A', '1', '5'];

        var cancelAllowedStsByExg = this.get('cancelAllowedStsByExg');

        cancelAllowedStsByExg.default = ['T', 'O', '0', 'A', '1', '5'];
        cancelAllowedStsByExg.XCHG = ['T', 'O', '0', 'A', '1', '5'];

        var defaultExgConfig = this.get('defaultExgConfig');
        defaultExgConfig.defaultConfig = {
            exg: '',
            cnfInfoLst: [
                {
                    fldType: '3',
                    valueSt: '1,2'
                },
                {
                    fldType: '2',
                    valueSt: '1,2'
                },
                {
                    fldType: '1',
                    valueSt: '0,1,3,4,6'
                }
            ]
        };
    }
}).create();

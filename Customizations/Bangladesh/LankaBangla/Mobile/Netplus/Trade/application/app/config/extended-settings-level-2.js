export default {
    appConfig: {
        customisation: {
            authenticationMode: 4,
            isTradingEnabled: true,
            profileServiceEnabled: false,
            smartLoginEnabled: false,
            isPasswordChangeEnable: true,
            supportedLanguages: [
                {code: 'EN', desc: 'English'}
            ],
            hashType: 'MD5',
            hidePreLogin: false
        },

        loggerConfig: {
            serverLogLevel: 0
        },

        widgetId: {
            quoteMenuId: 'fullQuote',
            watchListMenuId: 'heatMap',
            marketMenuId: 'market',
            indicesTabId: 'companyProf',
            topStocksTabId: 'topStocks',
            portfolioMenuId: 'standard',
            orderTicketMenuId: 'classic'
        }
    },

    priceSettings: {
        connectionParameters: {
            primary: {
                ip: 'itrade.lbsbd.com/price',
                port: '',
                secure: true
            },

            secondary: {
                ip: 'itrade.lbsbd.com/price',
                port: '',
                secure: true
            }
        }
    },

    tradeSettings: {
        channelId: 22,

        connectionParameters: {
            primary: {
                ip: 'itrade.lbsbd.com/ws',
                port: '',
                secure: true // Is connection secure - true or false
            },

            secondary: {
                ip: 'itrade.lbsbd.com/ws',
                port: '',
                secure: true // Is connection secure - true or false
            }
        }
    }
};
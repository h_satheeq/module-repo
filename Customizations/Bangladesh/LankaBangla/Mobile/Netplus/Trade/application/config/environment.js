/* jshint node: true */
var EnvironmentExtended = require('./environment-extended.js');

module.exports = function (environment) {
    var ENV = new EnvironmentExtended(environment);

    ENV.baseURL = '/mobileApp';
    ENV.contentSecurityPolicy['connect-src'] = "'self' www.google-analytics.com wss://itrade.lbsbd.com/price wss://itrade.lbsbd.com";

    if (ENV.APP.isTestMode) {
        ENV.contentSecurityPolicy['connect-src'] = "'self' www.google-analytics.com wss://itrade.lbsbd.com/price wss://itrade.lbsbd.com";
    }

    return ENV;
};
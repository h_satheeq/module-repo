export default {
    appConfig: {
        customisation: {
            allowedSubMarkets: {
                XCHG: ['N']
            },

            loginViewSettings: {
                isPoweredByEnabled: true
            }
        }
    },

    priceSettings: {
        connectionParameters: {
            primary: {
                ip: 'itrade.lbsbd.com/price',
                port: '',
                secure: true
            },

            secondary: {
                ip: 'itrade.lbsbd.com/price',
                port: '',
                secure: true
            }
        }
    },

    tradeSettings: {
        channelId: 22,

        connectionParameters: {
            primary: {
                ip: 'itrade.lbsbd.com/ws',
                port: '',
                secure: true // Is connection secure - true or false
            },

            secondary: {
                ip: 'itrade.lbsbd.com/ws',
                port: '',
                secure: true // Is connection secure - true or false
            }
        }
    },

    priceWidgetConfig: {
        gms: [
            {sym: 'EURUSD', exg: 'GLOBAL', inst: '0', sDes: 'EURSAR', icon: 'comm-icon icon-euro'},
            {sym: 'USDJPY', exg: 'GLOBAL', inst: '0', sDes: 'EURSAR', icon: 'comm-icon icon-euro'},
            {sym: 'GBPUSD', exg: 'GLOBAL', inst: '0', sDes: 'EURSAR', icon: 'comm-icon icon-euro'},
            {sym: 'USDAUD', exg: 'GLOBAL', inst: '0', sDes: 'EURSAR', icon: 'comm-icon icon-euro'},
            {sym: 'EURGBP', exg: 'GLOBAL', inst: '0', sDes: 'EURSAR', icon: 'comm-icon icon-euro'}
        ]
    }
};
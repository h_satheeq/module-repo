export default {
    EN: {
        labels: {
            /*eslint-disable */
            mktStatus_2: 'Trading',
            mktStatus_4: 'Close',
            mktStatus_21: 'OP Publication',
            mktStatus_26: 'SOD',
            mktStatus_27: 'CP Publication',
            mktStatus_28: 'CPT',
            /*eslint-enable */
            pendHolding: 'Pending Holding'
        }
    }
};

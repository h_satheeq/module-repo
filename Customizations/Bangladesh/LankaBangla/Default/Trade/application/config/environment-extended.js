/* jshint node: true */
var EnvironmentCore = require('./environment-core.js');

module.exports = function (environment) {
    var ENV = new EnvironmentCore(environment);

    if (ENV.APP.isTestMode) {
	ENV.contentSecurityPolicy['connect-src'] = "'self' www.google-analytics.com wss://itrade.lbsbd.com/price wss://itrade.lbsbd.com";
	
        ENV.APP.tradeConnectionParameters = {
            primary: {
                ip: 'itrade.lbsbd.com/ws',
                port: '',
                secure: true
            }
        };

        ENV.APP.priceConnectionParameters = {
            primary: {
                ip: 'itrade.lbsbd.com/price',
                port: '',
                secure: true
            }
        };
    }

    return ENV;
};

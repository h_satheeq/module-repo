/* jshint node: true */
var EnvironmentExtended = require('./environment-extended.js');

module.exports = function (environment) {
    var ENV = new EnvironmentExtended(environment);
	
    ENV.contentSecurityPolicy['connect-src'] = "'self' www.google-analytics.com wss://itrade.lbsbd.com/price ws://192.168.13.244:8800/";

    if (ENV.APP.isTestMode) {		
		ENV.contentSecurityPolicy['connect-src'] = "'self' www.google-analytics.com wss://itrade.lbsbd.com/price ws://192.168.13.244:8800/";
    }

    return ENV;
};
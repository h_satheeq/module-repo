/* jshint node: true */
var EnvironmentCore = require('./environment-core.js');

module.exports = function (environment) {
    var ENV = new EnvironmentCore(environment);

    ENV.baseURL = '/app';
    ENV.contentSecurityPolicy['connect-src'] = "'self' www.google-analytics.com wss://itrade.lbsbd.com/price ws://itrade.lbsbd.com/";

    if (ENV.APP.isTestMode) {
        ENV.APP.tradeConnectionParameters = {
            primary: {
                ip: '192.168.14.144',
                port: '8800',
                secure: false
            }
        };

        ENV.APP.priceConnectionParameters = {
            primary: {
                ip: 'itrade.lbsbd.com/price',
                port: '',
                secure: true
            }
        };

        ENV.baseURL = '/app';
        ENV.contentSecurityPolicy['connect-src'] = "'self' www.google-analytics.com wss://itrade.lbsbd.com/price ws://192.168.14.144:8800/";
    }

    return ENV;
};

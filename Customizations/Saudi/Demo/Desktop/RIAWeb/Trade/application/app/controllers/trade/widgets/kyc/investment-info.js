import BaseController from '../../../base-controller';
import utils from '../../../../utils/utils';
import sharedService from '../../../../models/shared/shared-service';
import kycConfig from '../../../../config/kyc-config';
import Ember from 'ember';

export default BaseController.extend({
    tradeService: sharedService.getService('trade'),
    isRequiredFieldsNotFilled: false,

    dataObj: {},
    kycUpdate: Ember.Object.create({}),
    message: '',

    kycUpdateStatus: {
        Success: 1,
        SystemError: -1,
        PartiallyUpdated: -2,
        RequiredDataMissing: -3
    },

    setKYCUpdateStatus: function () {
        var sts = this.get('kycUpdate').sts;
        var msg;

        if (sts !== this.kycUpdateStatus.Success) {
            msg = this.get('kycUpdate').rsn;
        }

        if (sts) {
            this._showMessage(sts, msg);
            this.set('isMsgEnable', true);
        }
    }.observes('kycUpdate.sts', 'kycUpdate.rsn'),

    onPrepareData: function () {
        this.set('capitalAmountCollection', this.tradeService.kycMasterDS.getMasterDataTypes(kycConfig.KYCMasterDataTypes.CapitalAmount, sharedService.userSettings.currentLanguage));
        this.set('investmentObjectivesCollection', this.tradeService.kycMasterDS.getMasterDataTypes(kycConfig.KYCMasterDataTypes.InvestmentObjectives, sharedService.userSettings.currentLanguage));
        this.set('investmentPeriodCollection', this.tradeService.kycMasterDS.getMasterDataTypes(kycConfig.KYCMasterDataTypes.InvestmentPeriod, sharedService.userSettings.currentLanguage));
        this.set('yesNoCollection', this.tradeService.kycMasterDS.getMasterDataTypes(kycConfig.KYCMasterDataTypes.YesNo, sharedService.userSettings.currentLanguage));
        this.set('investmentExperienceCollection', this.tradeService.kycMasterDS.getMasterDataTypes(kycConfig.KYCMasterDataTypes.InvestmentExperience, sharedService.userSettings.currentLanguage));
        this.set('riskToleranceCollection', this.tradeService.kycMasterDS.getMasterDataTypes(kycConfig.KYCMasterDataTypes.RiskTolerance, sharedService.userSettings.currentLanguage));

        var businessObj = this.get('businessObj');

        this.set('dataObj.capitalAmt', businessObj.get('capitalAmt'));
        this.set('dataObj.invPeriod', businessObj.get('invPeriod'));
        this.set('dataObj.invExp', businessObj.get('invExp'));
        this.set('dataObj.riskTol', businessObj.get('riskTol'));
        this.set('dataObj.invAccOtherCom', businessObj.get('invAccOtherCom'));
        this.set('dataObj.invObjec', businessObj.get('invObjec'));
        this.set('dataObj.isTradedLocal', businessObj.get('isTradedLocal'));
        this.set('dataObj.isTradedInternational', businessObj.get('isTradedInternational'));
        this.set('dataObj.isTradedBondsSukuk', businessObj.get('isTradedBondsSukuk'));
    },

    _showMessage: function (sts, msg) {
        var app = this.get('app');
        var message = sts === this.kycUpdateStatus.Success ? app.lang.messages.success : (msg ? msg : app.lang.labels.reqFailed);
        var messageCss = sts === this.kycUpdateStatus.Success ? 'up-fore-color' : 'down-fore-color';

        this.set('message', message);
        this.set('msgCss', messageCss);
    },

    actions: {
        onSaveInvestmentsInfo: function () {
            var isRequiredFieldsAvailable = true;
            var dataObject = this.get('dataObj');

            Ember.$.map(dataObject, function (item) {
                if (!utils.validators.isAvailable(item)) {
                    isRequiredFieldsAvailable = false;

                    return true;
                }
            });

            if (isRequiredFieldsAvailable) {
                this.set('isRequiredFieldsNotFilled', false);
                this.onSave(this.get('dataObj'));
            } else {
                this.set('isRequiredFieldsNotFilled', true);
            }
        }
    }
});
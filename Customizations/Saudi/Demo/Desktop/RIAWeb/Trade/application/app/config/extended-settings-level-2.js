export default {
    appConfig: {
        customisation: {
            authenticationMode: 4,
            isTradingEnabled: true,
            hashType: 'MD5',
            isUpperCasePassword: true
        },

        kycUpdateURL: 'http://192.168.13.60/uas/kyc/home.do',

        tadawulityURLs: {
            tadawulityLogin: 'https://brokerreg.tadawul.com.sa/ir/validateInvestor',
            tadawulityInfo: 'http://tadawulaty.tadawul.com.sa/tadawulaty/index.htm'
        },

        onlineAccountOpenURLs: {
            existingCustomer: 'http://192.168.13.60/uas/accountCreation/home.do',
            newCustomer: 'https://eforms.saib.com.sa/_layouts/SAIBeForms/ProductSelection.aspx'
        },

        mutualFund: {
            /*eslint-disable */
            agreementPDF_EN: 'http://www.alistithmarcapital.com/pdfs/120-SAIB-Saudi-Companies-Fund.pdf',
            agreementPDF_AR: 'http://www.alistithmarcapital.com/pdfs/120-SAIB-Saudi-Companies-Fund.pdf'
            /*eslint-enable */
        }
    },

    tradeSettings: {
        channelId: 30,

        connectionParameters: {
            primary: {
                ip: 'icap-wstrs-qa.directfn.net/trs',
                port: '',
                secure: true
            },

            secondary: {
                ip: 'icap-wstrs-qa.directfn.net/trs',
                port: '',
                secure: true
            }
        }
    }
};

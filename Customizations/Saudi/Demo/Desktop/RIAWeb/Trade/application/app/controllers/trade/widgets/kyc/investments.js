import utils from '../../../../utils/utils';
import BaseController from '../../../base-controller';
import Ember from 'ember';

export default BaseController.extend({
    headerAvailable: false,
    isRequiredFieldsNotFilled: false,

    dataObj: {},
    kycUpdate: Ember.Object.create({}),
    message: '',

    kycUpdateStatus: {
        Success: 1,
        SystemError: -1,
        PartiallyUpdated: -2,
        RequiredDataMissing: -3
    },

    setKYCUpdateStatus: function () {
        var sts = this.get('kycUpdate').sts;
        var msg;

        if (sts !== this.kycUpdateStatus.Success) {
            msg = this.get('kycUpdate').rsn;
        }

        if (sts) {
            this._showMessage(sts, msg);
            this.set('isMsgEnable', true);
        }
    }.observes('kycUpdate.sts', 'kycUpdate.rsn'),

    totVal: function () {
        var total = 0;
        var dataObject = this.get('dataObj');

        Ember.$.map(dataObject, function (item) {
            total += parseInt(item ? item : 0, 10);
        });

        return total;
    }.property('dataObj.sharesHighRisk', 'dataObj.sharesMedRisk', 'dataObj.sharesLowRisk', 'dataObj.debtInstrmntsHighRisk', 'dataObj.debtInstrmntsMedRisk', 'dataObj.debtInstrmntsLowRisk', 'dataObj.invstmntFundHighRisk', 'dataObj.invstmntFundMedRisk', 'dataObj.invstmntFundLowRisk', 'dataObj.tradeFinanceLowRisk', 'dataObj.tradeFinanceHighRisk', 'dataObj.tradeFinanceMedRisk', 'dataObj.commdtsHighRisk', 'dataObj.commdtsLowRisk', 'dataObj.commdtsMedRisk', 'dataObj.optionsHighRisk', 'dataObj.optionsMedRisk', 'dataObj.optionsLowRisk'),

    onPrepareData: function () {
        var businessObj = this.get('businessObj');

        this.set('dataObj.sharesHighRisk', businessObj.get('sharesHighRisk'));
        this.set('dataObj.sharesMedRisk', businessObj.get('sharesMedRisk'));
        this.set('dataObj.debtInstrmntsHighRisk', businessObj.get('debtInstrmntsHighRisk'));
        this.set('dataObj.debtInstrmntsMedRisk', businessObj.get('debtInstrmntsMedRisk'));
        this.set('dataObj.debtInstrmntsLowRisk', businessObj.get('debtInstrmntsLowRisk'));
        this.set('dataObj.invstmntFundHighRisk', businessObj.get('invstmntFundHighRisk'));
        this.set('dataObj.invstmntFundMedRisk', businessObj.get('invstmntFundMedRisk'));
        this.set('dataObj.invstmntFundLowRisk', businessObj.get('invstmntFundLowRisk'));
        this.set('dataObj.tradeFinanceLowRisk', businessObj.get('tradeFinanceLowRisk'));
        this.set('dataObj.commdtsHighRisk', businessObj.get('commdtsHighRisk'));
        this.set('dataObj.optionsHighRisk', businessObj.get('optionsHighRisk'));
        this.set('dataObj.sharesLowRisk', businessObj.get('sharesLowRisk'));
        this.set('dataObj.tradeFinanceHighRisk', businessObj.get('tradeFinanceHighRisk'));
        this.set('dataObj.tradeFinanceMedRisk', businessObj.get('tradeFinanceMedRisk'));
        this.set('dataObj.commdtsMedRisk', businessObj.get('commdtsMedRisk'));
        this.set('dataObj.commdtsLowRisk', businessObj.get('commdtsLowRisk'));
        this.set('dataObj.optionsMedRisk', businessObj.get('optionsMedRisk'));
        this.set('dataObj.optionsLowRisk', businessObj.get('optionsLowRisk'));
    },

    _showMessage: function (sts, msg) {
        var app = this.get('app');
        var message = sts === this.kycUpdateStatus.Success ? app.lang.messages.success : (msg ? msg : app.lang.labels.reqFailed);
        var messageCss = sts === this.kycUpdateStatus.Success ? 'up-fore-color' : 'down-fore-color';

        this.set('message', message);
        this.set('msgCss', messageCss);
    },

    actions: {
        onSaveInvestments: function () {
            var isRequiredFieldsAvailable = true;
            var dataObject = this.get('dataObj');

            Ember.$.map(dataObject, function (item) {
                if (!utils.validators.isAvailable(item)) {
                    isRequiredFieldsAvailable = false;
                    return true;
                }
            });

            if (isRequiredFieldsAvailable) {
                this.set('isRequiredFieldsNotFilled', false);
                this.onSave(this.get('dataObj'));
            } else {
                this.set('isRequiredFieldsNotFilled', true);
            }
        }
    }
});
export default {
    appConfig: {
        customisation: {
            authenticationMode: 4,
            isTradingEnabled: true,
            hashType: 'MD5',
            isUpperCasePassword: true,
            isDiscloseQtyEnabled: true,
            smartLoginEnabled: false,
            showFailedLoginAttempt: true,
            isPasswordChangeEnable: true,

            loginViewSettings: {
                isSignUpEnabled: false,
                isForgotPasswordEnabled: true,
                isPoweredByEnabled: true,
                isRegisterEnabled: true
            },

            passwordRules: {
                maxLength: 18,
                minLength: 8,
                checkLength: true,
                checkContainNumber: true,
                checkStartWithLetter: true,
                checkContainSmallLetter: true,
                checkContainCapitalLetter: true,
                checkIdenticalCharacter: true,
                checkConsecutiveCharacter: true,
                checkSpecialCharacter: true,
                checkUsernameMatch: true
            },

            usernameRules: {
                maxLength: 12,
                minLength: 6,
                checkLength: true,
                checkEngAlphaNumeric: true,
                checkStartWithLetter: true,
                checkIdenticalCharacter: true
            },

            displayProperties: {
                dispProp: ['tradingAccName', 'cashAccount.invAccNo']
            }
        },

        widgetId: {
            quoteMenuId: 'fullQuote',
            watchListMenuId: 'heatMap',
            marketMenuId: 'market',
            indicesTabId: 'companyProf',
            topStocksTabId: 'topStocks',
            portfolioMenuId: 'standard',
            orderTicketMenuId: 'classic'
        },

        chartConfig: {
            chartIndicators: ['AccumulationDistribution', 'AverageTrueRange', 'BollingerBands', 'ChaikinMF', 'ChandeMomentumOscillator', 'MoneyFlowIndex', 'MovingAverage', 'PSAR', 'RelativeStrengthIndex', 'TimeSeriesForecast', 'TRIX', 'VolOsc', 'WildersSmoothing', 'WilliamsPerR']
        },

        googleAnalyticConfig: {
            id: ''
        }
    },

    priceSettings: {
        urlTypes: {
            prodSub: 'https://data-sa.mubasher.net/uatria/riaauth',
            astMgmtEN: 'https://icap.com.sa/en/asset-management',
            astMgmtAR: 'https://icap.com.sa/ar/assetmgmt',
            accountTermsCondition: ''
        },

        connectionParameters: {
            primary: {
                ip: 'data-sa9.mubasher.net/html5-Retail',
                port: '',
                secure: true
            },

            secondary: {
                ip: 'data-sa9.mubasher.net/html5-Retail',
                port: '',
                secure: true
            }
        }
    },

    tradeSettings: {
        channelId: 30,

        connectionParameters: {
            primary: {
                ip: 'icap-wstrs-qa.directfn.net/trs',
                port: '',
                secure: true
            },

            secondary: {
                ip: 'icap-wstrs-qa.directfn.net/trs',
                port: '',
                secure: true
            }
        }
    },

    userSettings: {
        orderConfig: {
            defaultOrderQty: 0
        },

        displayFormat: {
            dateFormat: 'DD/MM/YYYY',
            dateTimeFormat: 'DD/MM/YYYY HH:mm:ss',
            dateTimeMinuteFormat: 'DD/MM/YYYY HH:mm'
        }
    }
};

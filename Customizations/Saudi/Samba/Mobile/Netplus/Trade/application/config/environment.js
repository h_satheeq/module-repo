/* jshint node: true */
var EnvironmentExtended = require('./environment-extended.js');

module.exports = function (environment) {
    var ENV = new EnvironmentExtended(environment);
    ENV.baseURL = '/';
	ENV.locationType = 'none';

    ENV.contentSecurityPolicy['connect-src'] = "'self' www.google-analytics.com wss://data-sa9.mubasher.net/html5-Retail wss://online5.icap.com.sa/trs https://data-sa9.mubasher.net/mix2/ClientServiceProvider";

    if (ENV.APP.isTestMode) {
        ENV.APP.tradeConnectionParameters = {
            primary: {
                ip: 'icap-wstrs-qa.directfn.net/trs',
                port: '',
                secure: true
            }
        };

        ENV.APP.priceConnectionParameters = {
            primary: {
                ip: 'data-sa9.mubasher.net/html5-Retail',
                port: '',
                secure: true
            }
        };

        ENV.contentSecurityPolicy['connect-src'] = "'self' www.google-analytics.com wss://data-sa9.mubasher.net/html5-Retail wss://icap-wstrs-qa.directfn.net/trs https://data-sa9.mubasher.net/mix2/ClientServiceProvider";

        ENV.gReCaptcha = {
            // Domains included: localhost, saudi-dev-riaweb.universal.directfn.net, saudi-qa-riaweb.universal.directfn.net
            siteKey: '6LfVBw4UAAAAAM4SQ1bESrYwhO7fIojAw7utbYUl'
        };
    }

    return ENV;
};

export default {
    appConfig: {
        customisation: {
            appName: 'Samba Capital',
            clientPrefix: 'samba',
            hashType: 'MD5',
            isUpperCasePassword: true,

            supportedThemes: [
                {code: 'theme1', desc: 'Dark', category: 'Dark'},
                {code: 'theme2', desc: 'Light', category: 'Light'},
                {code: 'theme3', desc: 'Pink', category: 'Pink'}
            ]
        }
    },

    tradeSettings: {
        connectionParameters: {
            primary: {
                ip: 'icap-wstrs-qa.directfn.net/trs',
                port: '',
                secure: true
            },
            secondary: {
                ip: 'icap-wstrs-qa.directfn.net/trs',
                port: '',
                secure: true
            }
        }
    }
};

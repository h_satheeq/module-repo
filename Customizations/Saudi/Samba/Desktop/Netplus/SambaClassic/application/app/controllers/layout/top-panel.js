/* global c3 */
/* global Mousetrap */

import Ember from 'ember';
import sharedService from '../../models/shared/shared-service';
import PriceConstants from '../../models/price/price-constants';
import ChartConstants from '../../models/chart/chart-constants';
import BaseController from '../base-controller';
import priceTicker from './price-ticker';
import graphDM from '../../models/chart/chart-data-stores/graph-data-manager';
import LanguageDataStore from '../../models/shared/language/language-data-store';
import ThemeDataStore from '../../models/shared/shared-data-stores/theme-data-store';
import d3 from 'd3';

/**
 * Controller class to handle all top panel
 * This widget contains
 *      1. Main index details
 *      2. Exchange snapshot
 *      3. Full market price ticker
 *      4. Index chart
 *
 * Initially this controller will send subscription requests to get above data from price service
 *
 */
export default BaseController.extend({
    // Ember objects references
    exchange: undefined, // User Default exchange reference
    index: undefined,
    fullMarket: undefined, // ticker setting : to keep full market tickers
    lastDrawnIndex: 0, // For main index chart
    marketTime: '', // formatted market time
    mktTime: '',
    isEnabledTimer: false, // Status of timer for market time
    exgSymCollection: undefined, // Exchange Symbol Collection

    // Updating below properties according to some values of 'exchange' or 'index'
    marketStatusCSS: '', // Market status colour. ex : Green Market Open , red Market close
    indexCss: '',     // Index colour
    indexChangeSign: '', // Index sign + or -
    previousMainIndexSide: '=',
    indexArrowCss: 'glyphicon padding-15',
    ytdCss: 'green',

    // Chart data
    intradayMainIndexChart: null, // Intra-day main index chart object
    symbolsTradedChart: null,// symbols traded chart object
    cashMapChart: null, // cash map chart object
    graphDataManager: null,

    symbolsTradedChartData: {columns: [['symbolsTraded', 0], ['symbolsTradedRem', 0]]}, // symbols traded chart data
    cashMapChartData: {columns: [['cashIn', 0], ['cashOut', 0]], lastUpdatedChartValue: 0, lastUpdatedCashInValue: 0}, // cash map chart data

    // Ticker related settings
    previousRunTime: undefined,  // last run time
    tickerIndex: 0,      // global variable to keep last used index of ticker list
    noOfItemsToAddToQ: 20, // number of symbols to add to Q in one loop
    numberOfIteration: 0,  // number of iterations
    isInitialTickerItemsAdded: false,

    settings: [
        {
            code: 'lang:' + LanguageDataStore.getChangeLanguageObj().lang.code,
            des: LanguageDataStore.getChangeLanguageObj().lang.desc
        },
        {
            code: 'theme:' + ThemeDataStore.getChangeThemeObj().theme.code,
            des: ThemeDataStore.getChangeThemeObj().theme.desc
        }
    ],

    topPanelSettings: {
        intZero: 0,
        emptyString: '',
        styles: {
            green: 'green',
            darkGreen: 'green-dark',
            red: 'red',
            darkRed: 'red-dark',
            white: 'colour-2',
            upArrow: 'glyphicon-triangle-top glyphicon ',
            downArrow: 'glyphicon-triangle-bottom glyphicon ',
            chartDefaultColor: '#262932',
            cashInColor: '#2ecc71',
            cashOutColor: '#ff6b6b'
        },
        signs: {plus: '+', minus: '-', equal: '='},
        chartsSettings: {
            symbolsTradedChart: {
                width: 85,
                height: 85
            },
            cashMapChart: {
                width: 85,
                height: 85
            },
            indexChart: {}
        },
        timeInterval: {
            OneSecondInMillis: 1000,
            OneMinuteInMillis: 60000
        },
        cashMapDrawingThreshold: 2
    },

    // Market Open, Close remaining time bar and countdown data
    openExgTime: '0000',
    closeExgTime: '0000',

    openTimeIndex: 0,
    closeTimeIndex: 0,
    currentTimeIndex: 0,
    criticalTimeIndex: 0,

    criticalTimeMin: 25,
    isCriticalTime: false,
    isMarketOpen: false,

    openTimeHour: 0,
    openTimeMins: 0,

    bar: '',
    progressStyle: '',
    showTime: false,
    showTimeText: '',

    /**
     * init function.
     * This will automatically be called by Ember context
     * No need to invoke explicitly
     */
    init: function () {
        this._super();

        var that = this;
        var waitingTimeUntilDomReady = this.topPanelSettings.timeInterval.OneSecondInMillis * 5;

        this.priceService = sharedService.getService('price');

        // Step 1 sending request to price services,
        // below requests are sending blindly to price service. Price service will handle multiple subscriptions
        this.addExchangeRequest();
        this.addMainIndexRequest();

        // Step 2 get and set data store date objects to local variable to access easily
        // objects will automatically be updated, Since those are Ember objects
        // setting 'exchange' object
        this.setExchangeObject();

        // Main index 'index' object
        this.setMainIndexObject();

        this.set('exgSymCollection', this.priceService.stockDS.getSymbolCollectionByExchange(this.priceService.userDS.currentExchange));

        // get and set full market symbols
        this.setFullMarketSymbols();

        // Create the graph data manager
        var gDM = graphDM.create({
            chartCategory: ChartConstants.ChartCategory.Intraday,
            chartDataLevel: ChartConstants.ChartDataLevel.IntradayCurrentDay,
            chartViewPeriod: ChartConstants.ChartViewPeriod.OneDay,
            onData: function (ohlcPoint, exg, sym) {
                that.amendMainIndexChart(ohlcPoint, exg, sym);
            },
            onDataChunk: function (chartSymbolObj) {
                that.updateMainIndexChart(chartSymbolObj);
            },
            onErrorFn: function () {
                // TODO: [Amila] implement this
            }
        });
        that.set('graphDataManager', gDM);
        gDM.addChartSymbol(that.priceService.userDS.currentExchange, that.priceService.userDS.currentIndex, true);

        // todo [Lasantha] check how to apply on-ready function on Emberjs
        // This time out is to handle on - ready and architecturally this is wrong no not guaranteed
        setTimeout(function () {
            priceTicker.init();
            that.initPriceTickerData();
        }, waitingTimeUntilDomReady);

        // Initiate the data download via the GDM
        that.get('graphDataManager').addChartDataSubscription();

        // Open, Close difference calculate
        this.setInitialTimeIndices();

        // Keyboard search
        Mousetrap.bind('ctrl+f', function () {
            Ember.$('.mousetrap').focus();
            return false;
        });

    },

    /**
     *  Adding Exchange snapshot request to price service
     *  This will use user's current exchange
     */
    addExchangeRequest: function () {
        this.priceService.addExchangeRequest(this.priceService.userDS.currentExchange);
    },

    /**
     * Adding main index request to price service
     * This will use user's current exchange and index
     */
    addMainIndexRequest: function () {
        this.priceService.addIndexRequest(this.priceService.userDS.currentExchange, this.priceService.userDS.currentIndex);
    },

    /**
     * Set exchange object to local variable
     */
    setExchangeObject: function () {
        this.set('exchange', this.priceService.exchangeDS.getExchange(this.priceService.userDS.currentExchange));
    },

    /**
     * Set main index to local variable
     */
    setMainIndexObject: function () {
        this.set('index', this.priceService.stockDS.getStock(this.priceService.userDS.currentExchange, this.priceService.userDS.currentIndex, PriceConstants.InstrumentTypes.Indices));
    },

    /**
     * Setting full market tickers to local variable
     * This is used by price ticker
     */
    // TODO: [Amila] review below code with Lasantha
    setFullMarketSymbols: function () {
        var store = this.priceService.stockDS.get('stockMapByExg');
        var exchange = this.get('exchange').exg;

        if (!store[exchange]) {
            store[exchange] = Ember.A();
            this.priceService.stockDS.set('stockMapByExg', store); // TODO [Lasantha/Arosha] need to check with Arosha to remove this line
        }

        var fullMarket = store[exchange];
        this.set('fullMarket', fullMarket);
    },

    /**
     * Returns no of symbols of selected exchange
     */
    exgSymbolCount: function () {
        return this.get('exgSymCollection').length;
    }.property('exgSymCollection.@each'),

    symbolsTradedPer: function () {
        var symbolTraded = this.get('exchange.symt');
        var noOfSymbols = this.get('exgSymbolCount');
        var symTradedPercentage = noOfSymbols === 0 ? 0 : ((symbolTraded / noOfSymbols) * 100);
        return symTradedPercentage;
    }.property('exchange.symt', 'exgSymbolCount'),

    /**
     * Update Market time of top panel
     * Observing exchange status and date and change ui accordingly
     * Observes : exchange.status and exchange.date
     */
    updateMarketTime: function () {
        if (this.exchange.get('stat') === 3) {
            var dateTimeStr = this.utils.formatters.formatToDate(this.exchange.date, this.exchange.get('tzo'));
            this.set('isEnabledTimer', false);
            this.set('marketTime', dateTimeStr);
        } else {
            if (!this.get('isEnabledTimer')) {
                this.set('isEnabledTimer', true);
                this.updateClock();
            }
        }
    }.observes('exchange.date', 'exchange.stat'),

    setMarketTime: function () {
        var marketTime = this.utils.formatters.getAdjustedDateTime(this.exchange.time, this.exchange.get('tzo'));
        this.set('mktTime', marketTime);
    }.observes('exchange.time'),

    updateClock: function () {
        var that = this;
        var time = this.get('mktTime');
        var dateTimeStr;
        var sleepTime = this.topPanelSettings.timeInterval.OneSecondInMillis;

        if (this.utils.validators.isAvailable(time)) {
            time.setSeconds(time.getSeconds() + 1);
            this.set('mktTime', time);
            dateTimeStr = this.utils.formatters.convertToDisplayTimeFormat(time);
            this.set('marketTime', dateTimeStr);
        }

        Ember.run.later(function () {
            if (that.get('isEnabledTimer')) {
                that.updateClock();
            }
        }, sleepTime);
    },

    updateMarketTimeTest: function () {
        var dateTimeStr;

        dateTimeStr = this.utils.formatters.formatToTime(this.exchange.openTime, -(this.exchange.get('tzo')));
        this.set('openTime', dateTimeStr);
    },

    /**
     * Change market status colour according to market status
     * Observes : exchange.stat
     */
    updateMarketStatusColor: function () {
        var marketStatusCSS = '';
        var stat = this.exchange.get('stat');

        if (stat === PriceConstants.MarketStatus.Close || stat === PriceConstants.MarketStatus.PreClose) {
            marketStatusCSS = this.topPanelSettings.styles.red;
        } else {
            marketStatusCSS = this.topPanelSettings.styles.green;
        }

        this.set('marketStatusCSS', marketStatusCSS);
    }.observes('exchange.stat'),

    /**
     * Change YTD percentage change css according to ytd value
     * Observes : exchange.pctYtd
     */
    updateYTDColor: function () {
        var ytdCss = this.topPanelSettings.emptyString;
        var pctYtd = this.index.get('pctYtd');

        if (pctYtd >= this.topPanelSettings.intZero) {
            ytdCss = this.topPanelSettings.styles.green;
        } else if (pctYtd < this.topPanelSettings.intZero) {
            ytdCss = this.topPanelSettings.styles.red;
        } else {
            ytdCss = this.topPanelSettings.styles.white;
        }

        this.set('ytdCss', ytdCss);
    }.observes('index.pctYtd'),

    /**
     * This is to updated index relate CSS ( colours and sign +/- )
     * Observes : index.pctChg
     * Change: indexArrowCss , indexChangeSign, indexCss, indexValCss
     */
    updateIndexRelatedCss: function () {
        var indexArrowCss = '';
        var indexCss = '';
        var indexValCss = '';
        var indexChangeSign = '';
        var pctChg = this.index.get('pctChg');

        if (pctChg > this.topPanelSettings.intZero) {
            indexCss = this.topPanelSettings.styles.darkGreen;
            indexValCss = this.topPanelSettings.styles.green;
            indexArrowCss = this.topPanelSettings.styles.upArrow;
            indexChangeSign = this.topPanelSettings.signs.plus;
        } else if (pctChg < this.topPanelSettings.intZero) {
            indexCss = this.topPanelSettings.styles.darkRed;
            indexValCss = this.topPanelSettings.styles.red;
            indexChangeSign = '';
            indexArrowCss = this.topPanelSettings.styles.downArrow;
        } else {
            indexCss = this.topPanelSettings.styles.white;
            indexValCss = this.topPanelSettings.styles.white;
            indexChangeSign = '';
            indexArrowCss = this.topPanelSettings.emptyString;
        }

        this.set('indexArrowCss', indexArrowCss);
        this.set('indexChangeSign', indexChangeSign);
        this.set('indexCss', indexCss);
        this.set('indexValCss', indexValCss);
    }.observes('index.pctChg'),

    /**
     * Update main index chart
     * draw chart if chart is null
     */
    updateMainIndexChart: function () {
        var that = this;
        var chartDataArray = that.get('graphDataManager').getDataArray();
        this.set('lastDrawnIndex', chartDataArray.length - 1);

        if (chartDataArray.length > 1) {
            if (that.intradayMainIndexChart === null) {
                that.drawMainIndexChart();
            } else {
                that.intradayMainIndexChart.load({
                    json: chartDataArray,
                    keys: {
                        x: 'DT',
                        value: ['Close']
                    }
                });
            }
        }
    },

    amendMainIndexChart: function (ohlcPoint) {
        var that = this;

        if (that.intradayMainIndexChart === null) {
            that.drawMainIndexChart();
        } else {
            if (ohlcPoint) {
                that.intradayMainIndexChart.flow({
                    json: [ohlcPoint],
                    keys: {
                        x: 'DT',
                        value: ['Close']
                    },
                    length: 0
                });
            }
        }
    },

    /**
     * Draw main index chart
     */
    drawMainIndexChart: function () {
        var that = this;
        var chartData = that.get('graphDataManager').getDataArray();

        this.intradayMainIndexChart = c3.generate({
            data: {
                json: chartData,
                keys: {
                    x: 'DT',
                    value: ['Close']
                },
                types: {
                    Close: 'area'
                },
                colors: {
                    Close: that.getChartColor()
                }
            },
            area: {
                zerobased: false
            },
            point: {
                show: false
            },
            size: {
                width: 165,
                height: 74
            },
            padding: {
                top: 7,
                right: 15,
                bottom: 1,
                left: 40
            },
            axis: {
                x: {
                    type: 'timeseries',
                    tick: {
                        // format: '%m %d',
                        format: '%H:%M',
                        culling: {
                            max: 5
                        },
                        count: 5
                    }
                },
                y: {
                    tick: {
                        format: function (val) {
                            return that.utils.formatters.formatNumber(val, 0);
                        },
                        culling: {
                            max: 3
                        },
                        count: 3
                    }
                }
            },
            legend: {
                show: false
            },
            tooltip: {
                show: false
            }
        });
    },

    /**
     * Initializing price ticker data
     * Run every 1 minute if market is not open
     * Run every 1 seconds if Market open but no data to show in ticker
     * Run every 30 seconds if required elements are added to Q
     */
    initPriceTickerData: function () {
        // logger.logInfo('Initializing price ticker data');
        var marketTime = this.get('exchange').time;
        var marketStatus = this.get('exchange').stat;
        // Initial sleep time
        var sleepTime = this.topPanelSettings.timeInterval.OneSecondInMillis;
        var that = this;

        // logger.logInfo('Market Status is ' + marketStatus);
        if (marketStatus !== PriceConstants.MarketStatus.Open) {
            // Run this every 1 min if market is not open
            this.addElementsToQ();
            // set last run time after running above function
            this.set('previousRunTime', marketTime);

            if (this.isInitialTickerItemsAdded) {
                sleepTime = this.topPanelSettings.timeInterval.OneMinuteInMillis;
            }
            else {
                sleepTime = this.topPanelSettings.timeInterval.OneSecondInMillis;
            }
        } else {
            this.addElementsToQ();
            // set last run time after running above function
            this.set('previousRunTime', marketTime);
            // run this every 30 seconds
            if (priceTicker.getQueueSize() > 0) {
                sleepTime = this.topPanelSettings.timeInterval.OneSecondInMillis * 30;
            }
        }

        setTimeout(function () {
            that.initPriceTickerData();
        }, sleepTime);
    },

    /**
     *
     * @param initEffectiveElementsCount
     */
    addElementsToQ: function (initEffectiveElementsCount) {
        var previousRunTime = this.get('previousRunTime');

        // logger.logInfo('Starting add data to Ticker Q  Previous Run time ' + previousRunTime);

        var maxNumberOfIterationPerInvoke = 2;
        var effectiveElementsCount = initEffectiveElementsCount || 0;
        var numberOfIterationPerInvoke = this.get('numberOfIteration');

        var fullMarket = this.get('fullMarket');
        var tickerIndex = this.tickerIndex;

        var noOfTickersInList = fullMarket.length;
        var marketStatus = this.get('exchange').stat;

        if (marketStatus !== PriceConstants.MarketStatus.Open) {
            this.noOfItemsToAddToQ = 8;
        }
        else {
            this.noOfItemsToAddToQ = 20;
        }

        // add 20 elements in each loop
        // or until complete the full list
        while (noOfTickersInList !== tickerIndex && effectiveElementsCount < this.noOfItemsToAddToQ) {
            // logger.logInfo('Getting data form Full market ticker list Index : ' + tickerIndex);
            var stock = fullMarket[tickerIndex];

            if (stock.inst !== 0) {
                // logger.logInfo('This is not an equity continue to next element stock.inst : ' + stock.inst);
            } else {
                // Last traded time of this ticker
                var ltt = stock.ltt;
                var isValidStock = false;

                if (previousRunTime === undefined) {
                    // This is first time. No need to check last traded time condition
                    // Just add to Q if there are trades
                    isValidStock = stock.trades > 0;
                } else {
                    isValidStock = (stock.trades > 0 && previousRunTime < ltt);
                }

                if (isValidStock) {
                    if (marketStatus !== PriceConstants.MarketStatus.Open) {
                        this.isInitialTickerItemsAdded = true;
                        priceTicker.addItemToQueueWhenMarketClosed(stock);
                    }
                    else {
                        priceTicker.addItemToQueue(stock);
                    }

                    effectiveElementsCount++;
                    // logger.logInfo('This stock is added to ticker = ' + ltt + ', Previous Run Time '  + previousRunTime + ' And Number of Trades = ' + stock.trades);

                } else {
                    // Ignored Ticker Index
                    // logger.logInfo('This stock is ignored Last Traded Time = ' + ltt + ', Previous Run Time '  + previousRunTime + ' And Number of Trades = ' + stock.trades);
                }

            }

            tickerIndex++;
        }

        numberOfIterationPerInvoke++;

        // full list is completed
        if (noOfTickersInList === tickerIndex) {
            // reset tickerIndex
            tickerIndex = 0;
        }

        this.set('tickerIndex', tickerIndex);

        // Iterate through the list from the beginning if there is no 20 elements to add
        // Iterate only twice even though there are no enough elements
        if (effectiveElementsCount < this.noOfItemsToAddToQ && numberOfIterationPerInvoke < maxNumberOfIterationPerInvoke) {
            this.set('numberOfIteration', numberOfIterationPerInvoke);
            this.set('numberOfIteration', numberOfIterationPerInvoke);
            this.set('tickerIndex', 0);
            this.addElementsToQ(effectiveElementsCount);
        } else {
            // Required elements added to Q
            // or maxNumberOfIteration reached
        }
    },

    /**
     * This is  to get top panel chart colour based on change val
     * @param pctChgVal  percentage change
     * @returns {string} colour
     */
    getChartColor: function (pctChgVal) {
        var pctChg = pctChgVal || this.index.get('pctChg');
        var lineColor = '#559fd6';

        if (pctChg > this.topPanelSettings.intZero) {
            lineColor = '#19703e';
        } else if (pctChg < this.topPanelSettings.intZero) {
            lineColor = '#842424';
        } else {
            lineColor = '#559fd6';
        }

        return lineColor;
    },

    /**
     * this is to update main index colour real time
     * change chart colour if only
     * Observe index.pctChg
     */
    updateMainIndexChartColors: function () {
        var pctChg = this.index.get('pctChg');
        var previousMainIndexSide = this.get('previousMainIndexSide');
        var currentMainIndexSide = '';
        var that = this;

        if (pctChg > this.topPanelSettings.intZero) {
            currentMainIndexSide = this.topPanelSettings.signs.plus;
        } else if (pctChg < this.topPanelSettings.intZero) {
            currentMainIndexSide = this.topPanelSettings.signs.minus;
        } else {
            currentMainIndexSide = this.topPanelSettings.signs.equal;
        }

        if (previousMainIndexSide !== currentMainIndexSide && this.intradayMainIndexChart && this.intradayMainIndexChart.data) {
            this.set('previousMainIndexSide', currentMainIndexSide);

            this.intradayMainIndexChart.data.colors({
                Close: that.getChartColor()
            });
        }
    }.observes('index.pctChg'),

    drawSymbolsTradedChart: function () {
        var that = this;

        this.symbolsTradedChart = c3.generate({
            data: {
                columns: that.symbolsTradedChartData.columns,
                type: 'donut',
                order: null
            },
            color: {
                pattern: ['#2ecc71', '#262932']
            },
            donut: {
                title: '',
                width: 5,
                label: {
                    show: false
                }
            },
            bindto: '#symbols_traded_chart',
            size: {
                width: 85,
                height: 85
            },
            legend: {
                hide: true
            },
            tooltip: {
                show: false
            }
        });
    },

    drawCashMapChart: function () {
        var that = this;

        this.cashMapChart = c3.generate({
            data: {
                columns: that.cashMapChartData.columns,
                type: 'donut',
                order: null
            },
            color: {
                pattern: [that.topPanelSettings.styles.cashInColor, that.topPanelSettings.styles.cashOutColor]
            },
            donut: {
                title: '',
                width: 5,
                label: {
                    show: false
                }
            },
            bindto: '#cash_map_chart',
            size: {
                width: 85,
                height: 85
            },
            legend: {
                hide: true
            },
            tooltip: {
                show: false
            }
        });
    },

    getCashInPer: function () {
        return this.exchange.get('cashInPer');
    },

    updateCashMapChart: function (cashInPer, chartDataColumns, isCashMapUpdated) {
        var chartDefaultColor = this.topPanelSettings.styles.chartDefaultColor;
        var cashInColor = this.topPanelSettings.styles.cashInColor;
        var cashOutColor = this.topPanelSettings.styles.cashOutColor;
        var titleValue = cashInPer + '%';

        if (this.cashMapChart === null) {
            this.drawCashMapChart();
        } else if (isCashMapUpdated) {
            this.cashMapChart.load({
                columns: chartDataColumns
            });
        }

        if (!cashInPer) {
            titleValue = '0.00%';

            if (this.cashMapChart.data.colors() &&
                this.cashMapChart.data.colors().cashIn === cashInColor) {
                this.cashMapChart.data.colors({cashIn: chartDefaultColor, cashOut: chartDefaultColor});
            }
        } else if (cashInPer && this.cashMapChart.data.colors() &&
            this.cashMapChart.data.colors().cashIn === this.topPanelSettings.styles.chartDefaultColor) {
            this.cashMapChart.data.colors({cashIn: cashInColor, cashOut: cashOutColor});
        }

        Ember.$('.c3-chart-arcs-title', Ember.$(this.cashMapChart.element)).text(titleValue);
    },

    bindCashMapChartData: function () {
        var cashInPer = Math.abs(this.getCashInPer());

        if (isNaN(cashInPer)) {
            cashInPer = 0;
        } else {
            cashInPer = Math.round(cashInPer * 1e2) / 1e2;    // TODO [arosha] move this to Formatters in case of high usages
        }

        var isCashMapUpdated = false;
        var lastUpdatedChartValue = this.get('cashMapChartData').lastUpdatedChartValue;
        var lastUpdatedCashInValue = this.get('cashMapChartData').lastUpdatedCashInValue;

        if (cashInPer === 0 || cashInPer !== lastUpdatedCashInValue) {
            lastUpdatedCashInValue = cashInPer;

            if (cashInPer === 0 || Math.abs(cashInPer - lastUpdatedChartValue) > this.topPanelSettings.cashMapDrawingThreshold) {
                isCashMapUpdated = true;
                lastUpdatedChartValue = cashInPer;
            }

            var chartDataColumns = [['cashIn', cashInPer], ['cashOut', (100 - cashInPer)]];
            this.set('cashMapChartData', {columns: chartDataColumns, lastUpdatedChartValue: lastUpdatedChartValue, lastUpdatedCashInValue: lastUpdatedCashInValue});

            this.updateCashMapChart(cashInPer, chartDataColumns, isCashMapUpdated);
        }
    }.observes('exchange.cashInPer'),

    updateSymbolsTradedChart: function () {
        var content = this.get('symbolsTradedChartData');
        var symbolsTradedPer = this.get('symbolsTradedPer');
        var symbolsTradedTitle = this.get('exchange.symt');
        var noOfSymbolsTitle = this.get('exgSymbolCount');
        var symbolsTradedValue = this.utils.formatters.formatNumber(symbolsTradedTitle);
        var noOfSymbolsValue = this.utils.formatters.formatNumber(noOfSymbolsTitle);

        if (this.symbolsTradedChart === null) {
            this.drawSymbolsTradedChart();
        } else {
            this.symbolsTradedChart.load({
                columns: content.columns
            });
        }

        if (symbolsTradedPer === this.topPanelSettings.intZero || symbolsTradedPer === this.topPanelSettings.emptyString || isNaN(symbolsTradedPer)) {
            symbolsTradedValue = '0';
        }

        var label = d3.select('#symbols_traded_chart text.c3-chart-arcs-title');

        Ember.$('.c3-chart-arcs-title', Ember.$(this.symbolsTradedChart.element)).empty();
        label.insert('tspan').text(symbolsTradedValue).attr('dy', -2).attr('x', 0);
        label.insert('tspan').text(noOfSymbolsValue).attr('dy', 11).attr('x', 0).attr('class', 'symbols-title');

        var cashInPerColour = symbolsTradedPer > 0 ? '#2ecc71' : '#ff6b6b';

        this.symbolsTradedChart.data.colors({
            symbolsTraded: cashInPerColour,
            symbolsTradedRem: '#262932'
        });

        var currentCount = content.columns[0][1];

        if (currentCount === 0) {
            var that = this;

            setTimeout(function () {
                that.bindSymbolsTradedChartData();
            }, 1000);
        }

    }.observes('symbolsTradedChartData.@each.value', 'exchange.symt', 'exgSymbolCount'),

    bindSymbolsTradedChartData: function () {
        var symbolsTradedPer = this.get('symbolsTradedPer');
        var symbolsTradedPerAbs = Math.abs(symbolsTradedPer);

        if (isNaN(symbolsTradedPerAbs)) {
            symbolsTradedPerAbs = 0;
        }

        if (symbolsTradedPer > this.topPanelSettings.intZero) {
            this.set('symbolsTradedChartData', {columns: [['symbolsTraded', symbolsTradedPerAbs], ['symbolsTradedRem', (100 - symbolsTradedPerAbs)]]});
        } else {
            this.set('symbolsTradedChartData', {columns: [['symbolsTradedRem', (100 - symbolsTradedPerAbs)], ['symbolsTraded', symbolsTradedPerAbs]]});
        }
    }.observes('symbolsTradedPer'),

    /**
     * Calculate up,down,unchanged progress bar percentage
     * Observes : exchange.ups,exchange.dwns,exchange.nChg
     */
    upsDownsNoChgValueArray: function () {
        var exg = this.get('exchange');
        var ups = exg.ups;
        var down = exg.dwns;
        var unchanged = exg.nChg;
        var maxValue = Math.max(ups, down, unchanged);

        var upsDownsNoChgValueArray = [{value: ups, barClass: 'progress-bar progress-up', barWidth: ''},
            {value: down, barClass: 'progress-bar progress-down', barWidth: ''},
            {value: unchanged, barClass: 'progress-bar progress-unchanged', barWidth: ''}];

        Ember.$.each(upsDownsNoChgValueArray, function (index, item) {
            var percentage = 0;

            if (isNaN(item.value) || item.value === 0 || maxValue === 0) {
                percentage = 0;
            }
            else {
                percentage = Math.round((item.value / maxValue) * 90) + 10;
            }

            item.barWidth = 'width:' + percentage + '%;';
        });

        return upsDownsNoChgValueArray;
    }.property('exchange.ups', 'exchange.dwns', 'exchange.nChg'),

    /** Start of progress bar - time
     * Calculate market open, close bar percentage
     */
    calculateIndex: function (hour, min) {  // Args are int
        var openHour = this.get('openTimeHour');
        var openMin = this.get('openTimeMin');
        var hourIndex = hour - openHour;
        var minIndex = (min - openMin) >= 0 ? (min - openMin) : (60 - (min - openMin));
        if (hourIndex >= 0) {
            return hourIndex * 3600 + minIndex * 60;
        } else {
            return 0;
        }
    },

    setInitialTimeIndices: function () {
        var openTime = this.get('exchange').openTime;
        var closeTime = this.get('exchange').closeTime;

        this.set('openExgTime', openTime);
        this.set('closeExgTime', closeTime);

        if (openTime && closeTime) {
            this.set('openTimeHour', parseInt(openTime.substr(0, 2), 10));
            this.set('openTimeMin', parseInt(openTime.substr(2, 2), 10));

            this.set('closeTimeIndex', this.calculateIndex(parseInt(closeTime.substr(0, 2), 10), parseInt(closeTime.substr(2, 2), 10)));
            var criticalMin = this.get('criticalTimeMin');
            this.set('criticalTimeIndex', this.get('closeTimeIndex') - criticalMin * 60);
            this.UpdateProgressTime();
        }
    },

    UpdateProgressTime: (function () {
        var currentTime = this.get('exchange').time;

        if (currentTime) {
            var currentIndex = this.calculateIndex(parseInt(currentTime.substr(0, 2), 10) + this.get('exchange').tzo, parseInt(currentTime.substr(2, 2), 10)) + parseInt(currentTime.substr(4, 2), 10);
            this.set('currentTimeIndex', currentIndex);

            if (this.get('exchange').stat === 2 && currentIndex >= 0) {   // TODO [arosha] replace 2 with const
                this.set('isMarketOpen', true);
            } else if (this.get('exchange').stat !== 2 || currentIndex >= this.get('closeTimeIndex')) {
                this.set('isMarketOpen', false);
            }

            if (currentIndex >= this.get('criticalTimeIndex') && this.get('closeTimeIndex') >= currentIndex) {
                this.set('isCriticalTime', true);
            } else {
                this.set('isCriticalTime', false);
            }

            if (this.get('isMarketOpen')) {
                this.showProgressTimeBar();

                if (this.get('isCriticalTime')) {
                    var timeStamp = (this.get('closeTimeIndex') - currentIndex);
                    var timeStampInt = parseInt(timeStamp / 60, 10);

                    if (!isNaN(timeStampInt)) {
                        this.set('showTime', true);

                        if (timeStampInt >= 2) {
                            this.set('showTimeText', timeStampInt + ' mins left');
                        } else if (timeStampInt === 1 && timeStamp >= 60) {
                            this.set('showTimeText', timeStampInt + ' min left');
                        } else {
                            this.set('showTimeText', timeStamp + ' sec left');
                        }
                    }
                } else {
                    this.set('showTime', false);
                }
            } else {
                this.set('showTime', false);
            }
        }
    }).observes('exchange.time'),

    showProgressTimeBar: function () {
        var bar = '';
        var style = '';
        var timeDiffInSecs = this.get('currentTimeIndex');
        var fullTimeDiffInSecs = this.get('closeTimeIndex');

        if (fullTimeDiffInSecs) {
            var differenceInPercentage = (timeDiffInSecs / fullTimeDiffInSecs) * 100;
            differenceInPercentage = differenceInPercentage > 0 && differenceInPercentage <= 100 ? differenceInPercentage : 0;
            bar = 'width:' + differenceInPercentage + '%;';

            if (this.get('isCriticalTime')) {
                style = 'progress-bar mkt-close-indicator';
            } else {
                style = 'progress-bar';
            }

            this.set('bar', bar);
            this.set('progressStyle', style);
        }
    },

    popUpWidget: function (id) {
        var sym = this.priceService.userDS.currentIndex;
        var exg = this.priceService.userDS.currentExchange;
        var symbolPopupView = this.container.lookupFactory('view:symbol-popup-view').create();

        symbolPopupView.show(id, sym, exg, 7);
    },

    // TODO: [Champaka] Need to move this action into separated controller
    actions: {
        logout: function () {
            this.utils.applicationSessionHandler.logout();
        },

        toggleFullScreen: function () {
            var isInFullScreen = (document.fullScreenElement && document.fullScreenElement !== null) ||
                (document.mozFullScreen || document.webkitIsFullScreen);

            if (isInFullScreen) {
                this.utils.fullScreenHelper.cancelFullScreen(document);
            } else {
                this.utils.fullScreenHelper.requestFullScreen(document.documentElement);
            }
        },

        changeSettings: function (selectedSetting) {
            var codeKeys = selectedSetting.code.split(this.utils.Constants.StringConst.Colon);

            if (codeKeys.length > 1) {
                if (codeKeys[0] === 'theme') {
                    ThemeDataStore.changeTheme(codeKeys[1]);
                } else if (codeKeys[0] === 'lang') {
                    LanguageDataStore.changeLanguage(codeKeys[1]);
                }

                var changeToLang = LanguageDataStore.getChangeLanguageObj();
                var changeToTheme = ThemeDataStore.getChangeThemeObj();

                Ember.set(this, 'settings', [
                    {code: 'lang:' + changeToLang.lang.code, des: changeToLang.lang.desc},
                    {code: 'theme:' + changeToTheme.theme.code, des: changeToTheme.theme.desc}
                ]);
            }
        },

        popUpChartOptionWidget: function () {
            // chart id = 4
            this.popUpWidget(4);
        }
    }
});

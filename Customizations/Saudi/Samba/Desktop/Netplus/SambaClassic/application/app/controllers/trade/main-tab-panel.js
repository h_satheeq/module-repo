import BaseController from '../base-controller';
import LanguageDataStore from '../../models/shared/language/language-data-store';
import sharedService from '../../models/shared/shared-service';

export default BaseController.extend({
    app: LanguageDataStore.getLanguageObj(),
    exchange: sharedService.getService('price').exchangeDS.getExchange(sharedService.getService('price').userDS.currentExchange),

    onLoadWidget: function () {
        this.loadingControllers('controller:trade/portfolio-valuation', 'trade.portfolio-valuation',
            'trade.main-tab-panel', 'w1');
        //this.loadingControllers('controller:price/watch-list', 'price.watch-list',
        //    'trade.main-tab-panel', 'MarketWatch');
    },

    onUnloadWidget: function () {
        this.set('columnDeclarations', []);
    },

    loadingControllers: function (controllerString, controllerTemplate, layout, outlet) {
        var route = this.container.lookup('route:application');
        var keys = controllerTemplate.split('.');
        var widgetController = this.container.lookupFactory(controllerString).create();
        widgetController.set('exg', this.get('exchange'));
        widgetController.set('wkey', keys[keys.length - 1]);
        widgetController.set('isClassicView', true);
        widgetController.initializeWidget({wn: controllerString.split('/').pop()});

        route.render(controllerTemplate, {
            into: layout,
            outlet: outlet,
            controller: widgetController
        });
    },

    contentLoader: function () {
        // var exchange = this.get('exchange').exg;
        var data = this.getData();
        // var Orders = tradeService.orderDS.getOrderCollectionByExchange(exchange);

        this.set('content', data);
        this.set('masterContent', data);
    },

    actions: {
        tabAction: function (id) {
            var controllerStr, controllerTemplate;

            switch (id) {
                case 1 :
                    controllerStr = 'controller:trade/portfolio-valuation';
                    controllerTemplate = 'trade.portfolio-valuation';
                    break;
                case 2 :
                    controllerStr = 'controller:price/watch-list';
                    controllerTemplate = 'price.watch-list';
                    break;
                default :
                    controllerStr = 'controller:shared/empty';
                    controllerTemplate = 'shared.empty';
                    break;
            }

            this.loadingControllers(controllerStr, controllerTemplate,
                'trade.main-tab-panel', 'w1');
        }
    }
});

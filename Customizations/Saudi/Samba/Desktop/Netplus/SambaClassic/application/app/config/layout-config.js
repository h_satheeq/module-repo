export default (function () {
    var layout = {
        mainContent: [
            {
                id: 6,
                title: 'sambaClassic',
                widgetTitle: 'Samba Classic',
                icon: 'glyphicon icon-file-text',
                layoutTemplate: {wn: 'custom-workspace.base-layout'},
                def: true,
                rightPanel: -1,
                custom: true,
                tab: [
                    {
                        id: 1,
                        title: 'samba',
                        def: true,
                        preset: true,
                        outlet: 'trade.samba-demo',
                        w: [{id: 1, wn: 'trade.summary-panel'},
                            {id: 2, wn: 'trade.main-tab-panel'},
                            {id: 3, wn: 'trade.buy-sell-panel'}]
                    },
                    {
                        id: 2,
                        title: 'standard',
                        def: false,
                        css: 'active',
                        outlet: 'custom-workspace.standard',
                        preset: true,
                        w: [{id: '1', wn: 'price.watch-list'},
                            {id: '2', wn: 'price.exchange-announcement'},
                            {id: '3', wn: 'price.exchange-announcement'}]
                    },
                    {
                        id: 3,
                        title: 'classic',
                        def: false,
                        outlet: 'custom-workspace.classic',
                        preset: true,
                        w: [{id: '1', wn: 'price.watch-list'}]
                    },
                    {
                        id: 4,
                        title: 'investor',
                        def: false,
                        outlet: 'custom-workspace.investor',
                        preset: true,
                        w: [{id: '1', wn: 'price.watch-list'},
                            {id: '2', wn: 'price.top-stocks'},
                            {id: '3', wn: 'chart.pro-chart'}]
                    },
                    {
						id: 5,
						title: 'custom',
						def: false,
						outlet: 'custom-workspace.layout-selection',
						w: []
                    }
                ]
            },
            //{
            //    id: 6,
            //    title: 'sambaClassic',
            //    widgetTitle: 'Samba Classic',
            //    icon: 'glyphicon icon-chart-bar',
            //    layoutTemplate: {wn: 'exchange-container-controller'},
            //    def: true,
            //    rightPanel: -1,
            //    tab: [
            //        {
            //            id: 1,
            //            def: true,
            //            outlet: 'trade.samba-demo',
            //            w: [{id: 1, wn: 'trade.summary-panel'},
            //                {id: 2, wn: 'trade.main-tab-panel'},
            //                {id: 3, wn: 'trade.buy-sell-panel'}]
            //        }]
            //},
            {
                id: 1,
                title: 'market',
                widgetTitle: 'Market',
                icon: 'glyphicon icon-chart-bar',
                layoutTemplate: {wn: 'exchange-container-controller'},
                def: false,
                rightPanel: 1,
                tab: [
                    {
                        id: 1,
                        def: true,
                        outlet: 'price.market-overview-tab',
                        w: [{id: 1, wn: 'price.sector-overview'},
                            {id: 2, wn: 'price.top-stocks'},
                            {id: 3, wn: 'price.commodities-overview'},
                            {id: 4, wn: 'price.watch-list'}]
                    }]
            },
            {
                id: 8,
                title: 'trade',
                widgetTitle: 'Trade',
                icon: 'glyphicon icon-time-and-sales',
                layoutTemplate: {wn: 'exchange-container-controller'},
                def: false,
                rightPanel: -1,
                tab: [
                    {
                        id: 1,
                        def: true,
                        outlet: 'trade.samba-demo-trade',
                        w: [{id: 1, wn: 'trade.trade-panel'}]
                    }]
            },
            {
                id: 2,
                title: 'quote',
                widgetTitle: 'Quote',
                icon: 'glyphicon icon-list-bullet-1',
                layoutTemplate: {wn: 'symbol-container-controller'},
                def: false,
                rightPanel: 4,
                tab: [
                    {
                        id: 1,
                        title: 'fullQuote',
                        def: true,
                        css: 'active',
                        outlet: 'price.quote-tab',
                        w: [{id: 1, wn: 'price.quote-summery'},
                            {id: 2, wn: 'chart.regular-chart'},
                            {id: 3, wn: 'price.quote-intraday-performance'},
                            {id: 4, wn: 'price.quote-time-and-sales'},
                            {id: 5, wn: 'price.quote-market-depth'},
                            {id: 6, wn: 'price.quote-market-depth'},
                            {id: 7, wn: 'price.symbol-announcement'}]
                    },
                    {
                        id: 4,
                        title: 'companyProf',
                        def: false,
                        outlet: 'price.company-tab',
                        w: [{id: 1, wn: 'price.quote-summery'},
                            {id: 2, wn: 'price.company-profile.company-basic-info'},
                            {id: 3, wn: 'price.company-profile.company-management-info'},
                            {id: 4, wn: 'price.company-profile.company-owners-info'},
                            {id: 5, wn: 'price.company-profile.company-subsidiaries-info'}]
                    }]
            },
            {
                id: 3,
                title: 'topStocks',
                widgetTitle: 'Top Stocks',
                icon: 'glyphicon icon-like-filled',
                layoutTemplate: {wn: 'exchange-container-controller'},
                def: false,
                rightPanel: -1,
                tab: [
                    {
                        id: 1,
                        def: true,
                        outlet: 'price.top-stocks-tab',
                        w: [{id: 1, wn: 'price.top-stock'},
                            {id: 2, wn: 'price.top-stock'},
                            {id: 3, wn: 'price.top-stock'},
                            {id: 4, wn: 'price.top-stock'},
                            {id: 5, wn: 'price.top-stock'}
                        ]
                    }]
            },
            {
                id: 7,
                title: 'heatMap',
                widgetTitle: 'Performance Heat Map',
                icon: 'glyphicon icon-thumbnails',
                layoutTemplate: {wn: 'exchange-container-controller'},
                def: false,
                rightPanel: -1,
                tab: [
                    {
                        id: 1,
                        def: true,
                        outlet: 'price.heatmap-tab',
                        w: [{id: 1, wn: 'price.heatmap'}]
                    }]
            },
            {
                id: 4,
                title: 'news',
                widgetTitle: 'News & Announcements',
                icon: 'glyphicon icon-mic',
                layoutTemplate: {wn: 'exchange-container-controller'},
                def: false,
                rightPanel: -1,
                tab: [
                    {
                        id: 1,
                        def: true,
                        outlet: 'price.news-tab',
                        w: [{id: 1, wn: 'price.announcement-list'}]
                    }]
            },
            {
                id: 5,
                title: 'chart',
                widgetTitle: 'Pro Chart',
                icon: 'glyphicon icon-pro-chart',
                layoutTemplate: {wn: 'symbol-container-controller'},
                def: false,
                rightPanel: -1,
                tab: [
                    {
                        id: 1,
                        def: true,
                        outlet: 'chart.pro-chart-tab',
                        w: [{id: 1, wn: 'chart.pro-chart'}]
                    }]
            }
        ],

        rightPanel: [
            {
                id: 1,
                icon: 'fa fa-bullhorn',
                def: true,
                layoutTemplate: 'price.announcement'
            },
            //{
            //    id: 2,
            //    icon: 'fa fa-bell',
            //    def: false,
            //    layoutTemplate: 'price.alert-price'
            //},
            //{
            //    id: 3,
            //    icon: 'fa fa-comment',
            //    def: false,
            //    layoutTemplate: 'price.chat'
            //},
            {
                id: 4,
                icon: 'fa fa-eye',
                def: false,
                layoutTemplate: 'price.quote-watch-list'
            }
        ]
    };

    var args = {
        2: {
            tab: {
                1: {
                    w: {
                        5: {
                            mode: 1         // DepthByPrice: 1, DepthByOrder: 2
                        },
                        6: {
                            mode: 2         // DepthByPrice: 1, DepthByOrder: 2
                        }
                    }
                }
            }
        },
        8: {
            tab: {
                1: {
                    w: {
                        1: {
                            src: 'assets/img/mt-trade.PNG'
                        },
                        2: {
                            src: 'assets/img/mt-trade.PNG'
                        },
                        3: {
                            src: 'assets/img/mt-trade.PNG'
                        },
                        4: {
                            src: 'assets/img/mt-trade.PNG'
                        }
                    }
                }
            }
        },
        3: {
            tab: {
                1: {
                    w: {
                        1: {
                            mode: 1         // TopGainersByChange, TopGainersByPercentageChange
                        },
                        2: {
                            mode: 3         //  TopLosersByChange, TopLosersByPercentageChange
                        },
                        3: {
                            mode: 4         //  MostActiveByVolume
                        },
                        4: {
                            mode: 5         // MostActiveByTrades
                        },
                        5: {
                            mode: 6         // MostActiveByValue
                        }
                    }
                }
            }
        },
        6: {
            tab: {
                1: {
                    w: {
                        1: {
                            isClassicView: true
                        }
                    }
                },
                2: {
                    w: {
                        1: {
                            isClassicView: true
                        },
                        2: {
                            type: 11
                        },
                        3: {
                            type: 77
                        }
                    }
                },
                3: {
                    w: {
                        1: {
                            isClassicView: true
                        }
                    }
                },
                4: {
                    w: {
                        1: {
                            isClassicView: true
                        }
                    }
                }
            }
        }
    };

    return {
        layout: layout,
        args: args
    };
});

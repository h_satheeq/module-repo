import Ember from 'ember';
import BaseController from '../base-controller';
import sharedService from '../../models/shared/shared-service';
import PriceConstants from '../../models/price/price-constants';

export default BaseController.extend({
    IntZero: 0,
    Styles: {
        green: 'green',
        darkGreen: 'green-dark',
        red: 'red',
        darkRed: 'red-dark',
        white: 'colour-2',
        upArrow: 'glyphicon-triangle-top glyphicon ',
        downArrow: 'glyphicon-triangle-bottom glyphicon '
    },

    index: undefined,
    indexValCss: '',
    indexArrowCss: 'glyphicon padding-15',

    priceService: sharedService.getService('price'),

    onPrepareData: function () {
        this.priceService.addIndexRequest(this.priceService.userDS.currentExchange, this.priceService.userDS.currentIndex);
    },

    onClearData: function () {
        this.priceService.removeIndexRequest(this.priceService.userDS.currentExchange, this.priceService.userDS.currentIndex);
    },

    onLoadWidget: function () {
        this.set('index', this.priceService.stockDS.getStock(this.priceService.userDS.currentExchange, this.priceService.userDS.currentIndex, PriceConstants.InstrumentTypes.Indices));
        Ember.Logger.debug(this.get('index'));
    },

    updateIndexRelatedCss: function () {
        var indexArrowCss = '';
        var indexValCss = '';
        var pctChg = this.index.get('pctChg');

        if (pctChg > this.IntZero) {
            indexValCss = this.Styles.green;
            indexArrowCss = this.Styles.upArrow;
        } else if (pctChg < this.IntZero) {
            indexValCss = this.Styles.red;
            indexArrowCss = this.Styles.downArrow;
        } else {
            indexValCss = this.Styles.white;
            indexArrowCss = '';
        }

        this.set('indexArrowCss', indexArrowCss);
        this.set('indexValCss', indexValCss);
    }.observes('index.pctChg')
});

export default {
    // UAT - Saudi
    /**connectionParameters: {
        primary: {
            ip: '78.93.230.73',
            port: '9018',
            secure: false // Is connection secure - true or false
        },
        Secondary: {
            ip: '78.93.230.73',
            port: '9018',
            secure: false // Is connection secure - true or false
        }
    },*/

    // UAT - LDC
    connectionParameters: {
        primary: {
            ip: 'data-sa9.mubasher.net',
            port: '9018',
            secure: false // Is connection secure - true or false
        },
        Secondary: {
            ip: 'data-sa9.mubasher.net',
            port: '9018',
            secure: false // Is connection secure - true or false
        }
    },

    // Local
    /**connectionParameters: {
        primary: {
            ip: '192.168.13.84',
            port: '8090',
            secure: false // Is connection secure - true or false
        },
        Secondary: {
            ip: '192.168.13.84',
            port: '8090',
            secure: false // Is connection secure - true or false
        }
    },*/

    customisation: {
        defaultExchange: 'TDWL',
        defaultIndex: 'TASI',
        defaultTheme: 'theme2',
        defaultLanguage: 'EN',
        clientPrefix: 'default',
        authenticationMode: 1,
        productType: 56,

        supportedLanguages: [
            {code: 'EN', desc: 'English'},
            {code: 'AR', desc: 'العربية'}
        ],
        supportedThemes: [
            {code: 'theme1', desc: 'Dark', category: 'Dark'},
            {code: 'theme2', desc: 'Light', category: 'Light'}
        ],
        gms: {
            'PXAUUSDOZ.SP': {sym: 'PXAUUSDOZ.SP', exg: 'GLOBAL', inst: '0', sDes: 'Gold', icon: 'comm-icon icon-gold'},
            'PXAGUSDOZ.SP': {sym: 'PXAGUSDOZ.SP', exg: 'GLOBAL', inst: '0', sDes: 'Silver', icon: 'comm-icon icon-silver'},
            'PBROUSDBR.SP': {sym: 'PBROUSDBR.SP', exg: 'GLOBAL', inst: '0', sDes: 'Brent Crude', icon: 'comm-icon icon-brent-crude'},
            'PWTIUSDBR.SP': {sym: 'PWTIUSDBR.SP', exg: 'GLOBAL', inst: '0', sDes: 'WTI Crude', icon: 'comm-icon icon-wti-crude'},
            'EURSAR': {sym: 'EURSAR', exg: 'GLOBAL', inst: '0', sDes: 'EURSAR', icon: 'comm-icon icon-euro'}
        }
    },

    displayFormat: {
        dateFormat: 'YYYY-MM-DD',
        timeFormat: 'HH:mm:ss',
        dateTimeFormat: 'YYYY-MM-DD HH:mm:ss',
        noValue: '--',
        decimalPlaces: 2
    },

    loggerConfig: {
        loggerConfig: 0, // appLogger.logLevels.logError,
        logTarget: 0, // appLogger.logTargets.displayLogInConsole,
        logBufferSize: 100, // This number of latest logs will be maintained. Will be used only in sendLogToServer mode
        logUploadingTimer: 3600000 // Logs will be uploaded to server hourly
    },

    searchConfig: {
        minCharLenForSymbol: 2, // Minimum input characters for performing symbol search
        minCharLenForContent: 3 // Minimum input characters for performing news & announcement search
    },

    googleAnalyticConfig: {
        id: 'UA-70494667-1'    
	}
};

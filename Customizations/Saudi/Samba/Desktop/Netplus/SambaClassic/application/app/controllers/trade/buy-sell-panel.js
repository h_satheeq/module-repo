import Ember from 'ember';
import BaseController from '../base-controller';
import GlobalSearch from '../../components/global-search';

export default BaseController.extend({
    company: '',
    panelClass: 'buy',

    onAfterRender: function () {
        var that = this;
        Ember.$('input[type="radio"][name="buy-sell-radio"]').bind('change', function () {
            that._buySellRadioChecked(this.value);
        });
    },

    _buySellRadioChecked: function (value) {
        // Change css
        if(value === 'sell') {
            this.set('panelClass', 'sell');
        } else {
            this.set('panelClass', 'buy');
        }
    },

    actions: {
        showSearchPopup: function () {
            var modal = this.get('orderSymbolSearch');
            modal.send('showModalPopup');
        },

        closeSearchPopup: function () {
            var modal = this.get('orderSymbolSearch');
            modal.send('closeModalPopup');
        },

        onSymbolSelected: function (item) {
            this.set('company', item.sDes);
            this.set('searchKey', item.sym);
        }
    }
});

Ember.Handlebars.helper('global-search', GlobalSearch);
import Ember from 'ember';
import TableController from '../../controllers/shared/table-controller';
import sharedService from '../../models/shared/shared-service';

// Cell Views
import ClassicHeaderCell from '../../views/table/classic-header-cell';
import HeaderCell from '../../views/table/dual-cells/header-cell';
import Cell from '../../views/table/cell';
import ButtonCell from '../../views/table/button-cell';
import ButtonMenuCell from '../../views/table/button-menu-cell';
import ClassicCell from '../../views/table/classic-cell';
import ClassicProgressCell from '../../views/table/classic-progress-cell';
import ClassicHyperlinkCell from '../../views/table/classic-hyperlink-cell';
import ChangeCell from '../../views/table/change-cell';
import UpDownCell from '../../views/table/up-down-cell';
import DotCell from '../../views/table/dual-cells/dot-cell';
import DualArrowCell from '../../views/table/dual-cells/dual-arrow-cell';
import DualChangeCell from '../../views/table/dual-cells/dual-change-cell';
import DualTextCell from '../../views/table/dual-cells/dual-text-cell';
import DualCell from '../../views/table/dual-cells/dual-cell';
import ProgressCell from '../../views/table/dual-cells/progress-cell';
import TableRow from '../../views/table/table-row';

export default TableController.extend({
    exchange: sharedService.getService('price').exchangeDS.getExchange(sharedService.getService('price').userDS.currentExchange),
    columnDeclarations: [],
    footerArray: Ember.A(),

    onLoadWidget: function () {
        this.setCellViewsScopeToGlobal();
        this.set('columnDeclarations', this.getColumnConfig());

        // TODO [AROSHA] Move this Scroll enable method to global.
        Ember.run.later(function () {
            Ember.$('.nano').nanoScroller();
        }, 200);
    },

    onPrepareData: function () {
        this.contentLoader();
    },

    onClearData: function () {
        this.set('content', Ember.A());
        this.set('masterContent', Ember.A());
    },

    onUnloadWidget: function () {
        this.set('columnDeclarations', []);
    },

    onLanguageChanged: function () {
        this.set('columnDeclarations', []);
        this.onLoadWidget();
    },

    contentLoader: function () {
        // var exchange = this.get('exchange').exg;
        var data = this.getData();
        // var Orders = tradeService.orderDS.getOrderCollectionByExchange(exchange);

        this.set('footerArray', Ember.A([Ember.Object.create({sym: 'Total', comp: '', qty: '', ltp: '', bs: '', cp: '', cv: 2334812.59, mv: 1179292.50, ugl: -158419.81, uglPct: -6.79})]));
        this.set('content', data);
        this.set('masterContent', data);
    },

    setCellViewsScopeToGlobal: function () {
        Ember.HeaderCell = HeaderCell;
        Ember.ClassicHeaderCell = ClassicHeaderCell;
        Ember.Cell = Cell;
        Ember.ClassicCell = ClassicCell;
        Ember.ClassicProgressCell = ClassicProgressCell;
        Ember.ChangeCell = ChangeCell;
        Ember.UpDownCell = UpDownCell;
        Ember.ButtonCell = ButtonCell;
        Ember.ButtonMenuCell = ButtonMenuCell;
        Ember.DotCell = DotCell;
        Ember.DualArrowCell = DualArrowCell;
        Ember.DualChangeCell = DualChangeCell;
        Ember.DualTextCell = DualTextCell;
        Ember.DualCell = DualCell;
        Ember.ProgressCell = ProgressCell;
        Ember.ClassicHyperlinkCell = ClassicHyperlinkCell;
        Ember.TableRow = TableRow;      //  TODO [AROSHA] MOVE THIS TO GLOBAL
    },

    cellViewsForColumns: {
        button: 'Ember.ButtonCell',
        buttonMenu: 'Ember.ButtonMenuCell',
        classicProgressCell: 'Ember.ClassicProgressCell',
        classicCell: 'Ember.ClassicCell',
        changeCell: 'Ember.ChangeCell',
        upDown: 'Ember.UpDownCell',
        dual: 'Ember.DualCell',
        dualText: 'Ember.DualTextCell',
        dualChange: 'Ember.DualChangeCell',
        progress: 'Ember.ProgressCell',
        dot: 'Ember.DotCell',
        dualArrow: 'Ember.DualArrowCell',
        hyperlink: 'Ember.ClassicHyperlinkCell'
    },

    getColumnConfig: function () {
        return [     // Column Object parameters : id, width, headerName, sortKeyword, multiValueIds, cellStyle, sortDisable, firstValueStyle, isBold, dataType, backgroundColour,
            {id: 'sym', width: 60, headerName: 'Symbol', headerStyle: 'text-left-header', type: 'classicCell', cellStyle: 'text-left-header'},
            {id: 'comp', width: 50, headerName: 'Company Name', headerStyle: 'text-left-header', type: 'classicCell', cellStyle: 'text-left-header'},
            {id: 'qty', width: 60, headerName: 'Quantity', headerStyle: 'text-center', type: 'classicCell', dataType: 'int', firstValueStyle: 'colour-normal'},
            {id: 'bs', width: 60, headerName: 'Blocked Security', headerStyle: 'text-center', type: 'classicCell', firstValueStyle: 'colour-normal'},
            {id: 'ltp', width: 60, headerName: 'Last Trade Price', headerStyle: 'text-center', type: 'classicCell', dataType: 'float', firstValueStyle: 'colour-normal'},
            {id: 'cp', width: 80, headerName: 'Cost Price', headerStyle: 'text-center', type: 'classicCell', dataType: 'float', firstValueStyle: 'colour-normal'},
            {id: 'cv', width: 80, headerName: 'Cost Value', headerStyle: 'text-center', type: 'classicCell', dataType: 'float', firstValueStyle: 'colour-normal'},
            {id: 'mv', width: 85, headerName: 'Market Value', headerStyle: 'text-center', type: 'classicCell', dataType: 'float', firstValueStyle: 'colour-normal'},
            {id: 'ugl', width: 90, headerName: 'Unrealized Gain/Loss', headerStyle: 'text-center', dataType: 'float', positiveNegativeChange: true, type: 'changeCell', firstValueStyle: 'colour-normal'},
            {id: 'uglPct', width: 80, headerName: 'Unrealized Gain/Loss%', headerStyle: 'text-center', dataType: 'float', positiveNegativeChange: true, type: 'changeCell', firstValueStyle: 'colour-normal'},
            {id: 'buy', width: 35, name: 'Amend', headerName: 'Buy', headerStyle: 'text-center', isColumnSortDisabled: true, type: 'hyperlink', firstValueStyle: 'colour-2'},
            {id: 'sell', width: 35, name: 'Cancel', headerName: 'Sell', headerStyle: 'text-center', isColumnSortDisabled: true, type: 'hyperlink', firstValueStyle: 'colour-2'}
        ];
    },

    getData: function () {
        return [
            {sym: '1010', comp: 'RIBL', qty: 12394, ltp: 84.00, bs: '0', cp: '97.25', cv: 1205348.42, mv: 1041096.00, ugl: -164252.42, uglPct: -13.63, buy: 'Buy', sell: 'Sell'},
            {sym: '1020', comp: 'BJAZ', qty: 1830, ltp: 38.00, bs: '0', cp: '36.04', cv: 65963.05, mv: 69540.00, ugl: 3576.95, uglPct: 5.42, buy: 'Buy', sell: 'Sell'},
            {sym: '1040', comp: 'SHB', qty: 10000, ltp: 0.00, bs: '0', cp: '50.17', cv: 501749.98, mv: 0.00, ugl: -501749.98, uglPct: -100.00, buy: 'Buy', sell: 'Sell'},
            {sym: '1050', comp: 'BSFR', qty: 2, ltp: 0.00, bs: '0', cp: '89.23', cv: 178.47, mv: 0.00, ugl: -178.47, uglPct: -100.00, buy: 'Buy', sell: 'Sell'},
            {sym: '1090', comp: 'SAMBA', qty: 20120, ltp: 0.00, bs: '0', cp: '24.61', cv: 495171.83, mv: 0.00, ugl: -495171.83, uglPct: -100.00, buy: 'Buy', sell: 'Sell'},
            {sym: '2010', comp: 'SABICA', qty: 803, ltp: 85.50, bs: '0', cp: '82.89', cv: 66400.84, mv: 68656.50, ugl: -2255.66, uglPct: 3.40, buy: 'Buy', sell: 'Sell'},
            {sym: '--', comp: '--', qty: '--', ltp: '--', bs: '--', cp: '--', cv: '--', mv: '--', ugl: '--', uglPct: '--'},
            {sym: '--', comp: '--', qty: '--', ltp: '--', bs: '--', cp: '--', cv: '--', mv: '--', ugl: '--', uglPct: '--'},
            {sym: '--', comp: '--', qty: '--', ltp: '--', bs: '--', cp: '--', cv: '--', mv: '--', ugl: '--', uglPct: '--'},
            {sym: '--', comp: '--', qty: '--', ltp: '--', bs: '--', cp: '--', cv: '--', mv: '--', ugl: '--', uglPct: '--'},
            {sym: '--', comp: '--', qty: '--', ltp: '--', bs: '--', cp: '--', cv: '--', mv: '--', ugl: '--', uglPct: '--'},
            {sym: '--', comp: '--', qty: '--', ltp: '--', bs: '--', cp: '--', cv: '--', mv: '--', ugl: '--', uglPct: '--'}
        ];
    },

    actions: {
        clickRow: function (closeMenuAction) {      // Flexibility is given if portfolio needs right click popup
            var modal = this.get('rightClickPopupMenu');

            if (closeMenuAction) {
                modal.send('closeModalPopup');
            }
        }
    }
});

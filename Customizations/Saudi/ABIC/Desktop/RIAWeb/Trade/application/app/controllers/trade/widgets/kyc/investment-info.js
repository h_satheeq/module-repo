import BaseController from '../../../base-controller';
import utils from '../../../../utils/utils';
import sharedService from '../../../../models/shared/shared-service';
import kycConfig from '../../../../config/kyc-config';
import Ember from 'ember';

export default BaseController.extend({
    tradeService: sharedService.getService('trade'),
    dataObj: {},

    onPrepareData: function () {
        this.set('capitalAmountCollection', this.tradeService.kycMasterDS.getMasterDataTypes(kycConfig.KYCMasterDataTypes.CapitalAmount, sharedService.userSettings.currentLanguage));
        this.set('investmentObjectivesCollection', this.tradeService.kycMasterDS.getMasterDataTypes(kycConfig.KYCMasterDataTypes.InvestmentObjectives, sharedService.userSettings.currentLanguage));
        this.set('investmentPeriodCollection', this.tradeService.kycMasterDS.getMasterDataTypes(kycConfig.KYCMasterDataTypes.InvestmentPeriod, sharedService.userSettings.currentLanguage));
        this.set('yesNoCollection', this.tradeService.kycMasterDS.getMasterDataTypes(kycConfig.KYCMasterDataTypes.YesNo, sharedService.userSettings.currentLanguage));
        this.set('investmentExperienceCollection', this.tradeService.kycMasterDS.getMasterDataTypes(kycConfig.KYCMasterDataTypes.InvestmentExperience, sharedService.userSettings.currentLanguage));
        this.set('riskToleranceCollection', this.tradeService.kycMasterDS.getMasterDataTypes(kycConfig.KYCMasterDataTypes.RiskTolerance, sharedService.userSettings.currentLanguage));

        var businessObj = this.get('businessObj');

        if (businessObj) {
            this.set('dataObj.capitalAmt', businessObj.get('capitalAmt'));
            this.set('dataObj.invPeriod', businessObj.get('invPeriod'));
            this.set('dataObj.invExp', businessObj.get('invExp'));
            this.set('dataObj.riskTol', businessObj.get('riskTol'));
            this.set('dataObj.invAccOtherCom', businessObj.get('invAccOtherCom'));
            this.set('dataObj.invObjec', businessObj.get('invObjec'));
            this.set('dataObj.isTradedLocal', businessObj.get('isTradedLocal'));
            this.set('dataObj.isTradedInternational', businessObj.get('isTradedInternational'));
            this.set('dataObj.isTradedBondsSukuk', businessObj.get('isTradedBondsSukuk'));
        }
    },

    getDataObject: function () {
        return this.get('dataObj');
    },

    onValidate: function () {
        var errorCollection = [];
        var isRequiredFieldsAvailable = true;

        var dataObject = this.get('dataObj');
        var app = this.get('app');

        Ember.$.map(dataObject, function (item) {
            if (!utils.validators.isAvailable(item)) {
                isRequiredFieldsAvailable = false;

                return true;
            }
        });

        if (!isRequiredFieldsAvailable) {
            errorCollection.pushObject(app.lang.messages.mandatoryFields);
        }

        return errorCollection;
    }
});
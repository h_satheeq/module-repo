export default (function () {
    var layout = {
        menuPanel: {
            template: 'layout.horizontal-navigation'
        },

        titleBar: {
            template: 'layout.title-bar'
        },

        topPanel: {
            template: 'top-panel-container-controller',
            content: [
                {
                    id: 9,
                    title: 'priceTopPanel',
                    def: true,
                    tab: [
                        {
                            id: 1,
                            title: 'priceTopPanel',
                            def: true,
                            outlet: 'price.containers.top-panel-tab',
                            w: [{id: 1, wn: 'price.top-panel.top-panel'}]
                        }]
                }
            ]
        },

        mainPanel: {
            template: 'main-panel-container-controller',
            content: [
                {
                    id: 8,
                    title: 'trading',
                    widgetTitle: 'trading',
                    icon: 'glyphicon icon-trade',
                    def: true,
                    isShowTitle: false,
                    rightPanel: -1,
                    tab: [
                        {
                            id: 1,
                            title: 'oneStop',
                            def: true,
                            outlet: 'trade.containers.trade-order-tab',
                            w: [
                                {
                                    id: 1,
                                    iw: [{id: 1, wn: 'trade.widgets.order-ticket.order-ticket-landscape', desc: 'orderTicket', def: true}]
                                },
                                {
                                    id: 2,
                                    iw: [{id: 1, wn: 'price.widgets.watch-list.watch-list-container', desc: 'watchList', def: true}]
                                },
                                {
                                    iw: [
                                        {id: 1, wn: 'trade.widgets.order-list', desc: 'orderList', def: true, isShow: true},
                                        {id: 2, wn: 'trade.widgets.order-search', desc: 'orderSearch', def: false, isShow: true},
                                        {id: 3, wn: 'trade.widgets.saved-orders', desc: 'savedOrders', def: false, isShow: true},
                                        {id: 4, wn: 'trade.widgets.rapid-order', desc: 'rapidOrder', def: false, isShow: true},
                                        {id: 5, wn: 'trade.widgets.panic-withdrawal', desc: 'panicWithdrawal', def: false, isShow: true}
                                    ],
                                    id: 3
                                },
                                {
                                    id: 4,
                                    iw: [
                                        {id: 1, wn: 'trade.widgets.portfolio', desc: 'portfolio', def: true, isShow: true},
                                        {id: 2, wn: 'trade.widgets.rights-subscription-list.rights-subscription-list', desc: 'rightsSubscriptionList', def: false, isShow: true},
                                        {id: 3, wn: 'trade.widgets.account-summary', desc: 'accountSummary', def: false, isShow: true}
                                    ]
                                }
                            ]
                        },
                        {
                            id: 3,
                            title: 'transfers',
                            def: false,
                            outlet: 'trade.containers.transfer-tab',
                            w: [
                                {
                                    id: 1,
                                    iw: [
                                        {id: 1, wn: 'trade.widgets.cash-transfer', desc: 'bankTransfer', def: true, isShow: true},
                                        {id: 2, wn: 'trade.widgets.transaction-history', desc: 'cashTransferHistory', def: false, isShow: true},
                                        {id: 3, wn: 'trade.widgets.cash-transfer-portfolios', desc: 'transferBetweenPortfolios', def: false, isShow: true}
                                    ]
                                },
                                {
                                    id: 2,
                                    iw: [
                                        {id: 1, wn: 'trade.widgets.product-subscription.product-subscription', desc: 'productSubscription', def: true, isShow: true}
                                    ]
                                },
                                {
                                    id: 3,
                                    iw: [
                                        {id: 1, wn: 'trade.widgets.account-statement.account-statement', desc: 'accStatement', def: true, isShow: true},
                                        {id: 2, wn: 'trade.widgets.consolidated-reports', desc: 'consolidatedReports', def: false, isShow: true}
                                    ]
                                },
                                {
                                    id: 4,
                                    iw: [
                                        {id: 1, wn: 'trade.widgets.tadawulity', desc: 'tdwlCart', def: true, isShow: true}
                                    ]
                                }
                            ]
                        },
                        {
                            id: 4,
                            cache: false,
                            title: 'mutualFund',
                            def: true,
                            outlet: 'trade.containers.mutual-fund-order-tab',
                            w: [
                                {
                                    id: 1,
                                    iw: [{id: 1, wn: 'trade.widgets.mutual-fund.mutual-fund-list', desc: 'mutualFund', def: true}]
                                },
                                {
                                    id: 2,
                                    iw: [{id: 1, wn: 'trade.widgets.mutual-fund.mutual-fund-holdings', desc: 'mutualFund', def: true}]
                                },
                                {
                                    id: 3,
                                    iw: [{id: 1, wn: 'trade.widgets.mutual-fund.mutual-fund-transactions', desc: 'mutualFund', def: true}]
                                }
                            ]
                        },
                        {
                            id: 2,
                            cache: false,
                            title: 'optionChain',
                            def: false,
                            outlet: 'price.containers.option-chain-tab',
                            w: [
                                {
                                    id: 1,
                                    iw: [{id: 1, wn: 'price.widgets.option-chain', desc: 'optionChain', def: true}]
                                },
                                {
                                    id: 2,
                                    iw: [{id: 1, wn: 'price.widgets.quote-intraday-performance', desc: 'detailQuote', def: true}]
                                },
                                {
                                    id: 3,
                                    iw: [{id: 1, wn: 'chart.regular-chart', desc: 'chart', def: true}]
                                },
                                {
                                    id: 4,
                                    iw: [{id: 1, wn: 'trade.widgets.order-ticket.order-ticket-portrait', desc: 'orderTicket', def: true}]
                                },
                                {
                                    id: 5,
                                    iw: [{id: 1, wn: 'trade.widgets.option-trading.option-order-list', desc: 'optionOrderList', def: true}]
                                }
                            ]
                        }
                    ]
                },
                {
                    id: 1,
                    title: 'market',
                    icon: 'glyphicon icon-analytics-chart-graph',
                    def: false,
                    rightPanel: -1,
                    tab: [
                        {
                            id: 1,
                            cache: false,
                            title: 'market',
                            def: true,
                            outlet: 'price.containers.market-overview-tab',
                            w: [
                                {
                                    id: 1,
                                    iw: [{id: 1, wn: 'price.widgets.sector-overview', desc: 'sectorOverview', def: true}]
                                },
                                {
                                    id: 2,
                                    iw: [{id: 1, wn: 'price.widgets.top-stocks', desc: 'topStocks', def: true}]
                                },
                                {
                                    id: 3,
                                    iw: [{id: 1, wn: 'price.widgets.commodities-overview', desc: 'gms', def: true}]
                                },
                                {
                                    id: 4,
                                    iw: [{id: 1, wn: 'price.widgets.watch-list.watch-list-container', desc: 'watchList', def: true}]
                                }
                            ]
                        },
                        {
                            id: 2,
                            title: 'topStocks',
                            def: false,
                            outlet: 'price.containers.top-stocks-tab',
                            w: [
                                {
                                    id: 1,
                                    iw: [{id: 1, wn: 'price.widgets.top-stock', desc: 'topGainers', def: true}]
                                },
                                {
                                    id: 2,
                                    iw: [{id: 1, wn: 'price.widgets.top-stock', desc: 'topLosers', def: true}]
                                },
                                {
                                    id: 3,
                                    iw: [{id: 1, wn: 'price.widgets.top-stock', desc: 'mostActive', def: true}]
                                },
                                {
                                    id: 4,
                                    iw: [{id: 1, wn: 'price.widgets.top-stock', desc: 'mostActive', def: true}]
                                },
                                {
                                    id: 5,
                                    iw: [{id: 1, wn: 'price.widgets.top-stock', desc: 'mostActive', def: true}]
                                }
                            ]
                        },
                        {
                            id: 3,
                            title: 'heatMap',
                            def: false,
                            outlet: 'price.containers.heatmap-tab',
                            w: [
                                {
                                    id: 1,
                                    iw: [{id: 1, wn: 'price.widgets.heatmap', desc: 'heatMap', def: true}]
                                }
                            ]
                        },
                        {
                            id: 5,
                            title: 'news',
                            def: false,
                            outlet: 'price.containers.news-tab',
                            w: [
                                {
                                    id: 1,
                                    iw: [{id: 1, wn: 'price.widgets.announcement.announcement-list', desc: 'newsAnnouncement', def: true}]
                                }
                            ]
                        }]
                },
                {
                    id: 2,
                    title: 'quote',
                    icon: 'glyphicon icon-list-ul',
                    def: false,
                    rightPanel: 4,
                    tab: [
                        {
                            id: 1,
                            cache: false,
                            title: 'fullQuote',
                            def: true,
                            outlet: 'price.containers.quote-tab',
                            w: [
                                {
                                    id: 1,
                                    iw: [{id: 1, wn: 'price.widgets.quote-summery', desc: 'quoteSummary', def: true}]
                                },
                                {
                                    id: 2,
                                    iw: [{id: 1, wn: 'chart.regular-chart', desc: 'chart', def: true}]
                                },
                                {
                                    id: 3,
                                    iw: [{id: 1, wn: 'price.widgets.quote-intraday-performance', desc: 'detailQuote', def: true}]
                                },
                                {
                                    id: 4,
                                    iw: [{id: 1, wn: 'price.widgets.time-and-sales.quote-time-and-sales', desc: 'timeAndSales', def: true}]
                                },
                                {
                                    id: 5,
                                    iw: [{id: 1, wn: 'price.widgets.quote-market-depth', desc: 'depthByPrice', def: true}]
                                },
                                {
                                    id: 6,
                                    iw: [{id: 1, wn: 'price.widgets.quote-market-depth', desc: 'depthByOrder', def: true}]
                                },
                                {
                                    id: 7,
                                    iw: [{id: 1, wn: 'price.widgets.announcement.symbol-announcement', desc: 'newsAnn', def: true}]
                                }
                            ]
                        },
                        {
                            id: 4,
                            cache: false,
                            title: 'companyProf',
                            def: false,
                            outlet: 'price.containers.company-tab',
                            w: [
                                {
                                    id: 1,
                                    iw: [{id: 1, wn: 'price.widgets.quote-summery', desc: 'quoteSummary', def: true}]
                                },
                                {
                                    id: 2,
                                    iw: [{id: 1, wn: 'price.widgets.company-profile.company-basic-info', desc: 'companyInfor', def: true}]
                                },
                                {
                                    id: 3,
                                    iw: [{id: 1, wn: 'price.widgets.company-profile.company-management-info', desc: 'mngtNBrdMbrs', def: true}]
                                },
                                {
                                    id: 4,
                                    iw: [{id: 1, wn: 'price.widgets.company-profile.company-owners-info', desc: 'owners', def: true}]
                                },
                                {
                                    id: 5,
                                    iw: [{id: 1, wn: 'price.widgets.company-profile.company-subsidiaries-info', desc: 'subsidiaries', def: true}]
                                }
                            ]
                        },
                        {
                            id: 5,
                            title: 'chart',
                            def: false,
                            outlet: 'chart.pro-chart-tab',
                            w: [
                                {
                                    id: 1,
                                    iw: [{id: 1, wn: 'chart.pro-chart', desc: 'proChartTitle', def: true}]
                                }
                            ]
                        }]
                },
                {
                    id: 6,
                    title: 'netplus',
                    icon: 'glyphicon icon-star',
                    def: false,
                    rightPanel: -1,
                    tab: [
                        {
                            id: 1,
                            cache: false,
                            title: 'investor',
                            def: true,
                            outlet: 'custom-workspace.investor',
                            w: [
                                {
                                    id: 1,
                                    iw: [{id: 1, wn: 'price.widgets.watch-list.watch-list-container', desc: 'watchList', def: true}]
                                },
                                {
                                    id: 2,
                                    iw: [{id: 1, wn: 'price.widgets.top-stocks', desc: 'topStocks', def: true}]
                                },
                                {
                                    id: 3,
                                    iw: [{id: 1, wn: 'chart.pro-chart', desc: 'proChartTitle', def: true}]
                                }
                            ]
                        },
                        {
                            id: 2,
                            cache: false,
                            title: 'standard',
                            def: false,
                            outlet: 'custom-workspace.standard',
                            w: [
                                {
                                    id: 1,
                                    iw: [{id: 1, wn: 'price.widgets.watch-list.watch-list-container', desc: 'watchList', def: true}]
                                },
                                {
                                    id: 2,
                                    iw: [{id: 1, wn: 'price.widgets.announcement.exchange-announcement', desc: 'newsAnn', def: true}]
                                },
                                {
                                    id: 3,
                                    iw: [{id: 1, wn: 'price.widgets.announcement.exchange-announcement', desc: 'newsAnn', def: true}]
                                }
                            ]
                        },
                        {
                            id: 3,
                            cache: false,
                            title: 'classic',
                            def: false,
                            outlet: 'custom-workspace.classic',
                            w: [
                                {
                                    id: 1,
                                    iw: [{id: 1, wn: 'price.widgets.watch-list.watch-list-container', desc: 'watchList', def: true}]
                                }
                            ]
                        }]
                },
                {
                    id: 10,
                    title: 'kyc',
                    icon: 'glyphicon icon-person',
                    def: false,
                    isShowTitle: true,
                    rightPanel: -1,
                    tab: [
                        {
                            id: 1,
                            title: 'ria',
                            def: true,
                            outlet: 'trade.containers.ria-web-tab',
                            w: [
                                {
                                    id: 1,
                                    iw: [{id: 1, wn: 'trade.widgets.kyc.kyc-update', desc: 'kycUpdate', def: true}]
                                }
                            ]
                        }]
                }
            ]
        },

        rightPanel: {
            template: 'layout.right-panel-container',
            content: [
                {
                    id: 1,
                    icon: 'fa fa-bullhorn',
                    def: true,
                    wn: 'price.widgets.announcement.announcement',
                    rightPanelTitleKey: 'newsAnn'
                },
                // {
                //    id: 2,
                //    icon: 'fa fa-bell',
                //    def: false,
                //    wn: 'price.widgets.alert-price'
                // },
                // {
                //    id: 3,
                //    icon: 'fa fa-comment',
                //    def: false,
                //    wn: 'price.widgets.chat'
                // },
                {
                    id: 4,
                    icon: 'fa fa-eye',
                    def: false,
                    wn: 'price.widgets.watch-list.quote-watch-list',
                    rightPanelTitleKey: 'watchList'
                }
            ]
        }
    };

    var args = {
        mainPanel: {
            2: {
                tab: {
                    1: {
                        w: {
                            1: {
                                1: {selectedLink: 1}
                            },
                            2: {
                                1: {selectedLink: 1}
                            },
                            3: {
                                1: {selectedLink: 1}
                            },
                            4: {
                                1: {selectedLink: 1}
                            },
                            5: {
                                1: {
                                    mode: 1,
                                    selectedLink: 1
                                } // DepthByPrice: 1, DepthByOrder: 2
                            },
                            6: {
                                1: {
                                    mode: 2,
                                    selectedLink: 1
                                } // DepthByPrice: 1, DepthByOrder: 2
                            },
                            7: {
                                1: {selectedLink: 1}
                            }
                        }
                    },
                    4: {
                        w: {
                            1: {
                                1: {selectedLink: 1}
                            },
                            2: {
                                1: {selectedLink: 1}
                            },
                            3: {
                                1: {selectedLink: 1}
                            },
                            4: {
                                1: {selectedLink: 1}
                            },
                            5: {
                                1: {selectedLink: 1}
                            }
                        }
                    },
                    5: {
                        w: {
                            1: {
                                1: {selectedLink: 1}
                            }
                        }
                    }
                }
            },
            1: {
                tab: {
                    2: {
                        w: {
                            1: {
                                1: {mode: 1, selectedLink: 1} // TopGainersByChange, TopGainersByPercentageChange
                            },
                            2: {
                                1: {mode: 3, selectedLink: 1} //  TopLosersByChange, TopLosersByPercentageChange
                            },
                            3: {
                                1: {mode: 4, selectedLink: 1} //  MostActiveByVolume
                            },
                            4: {
                                1: {mode: 5, selectedLink: 1} // MostActiveByTrades
                            },
                            5: {
                                1: {mode: 6, selectedLink: 1} // MostActiveByValue
                            }
                        }
                    }
                }
            },
            8: {
                tab: {
                    1: {
                        w: {
                            1: {
                                1: {selectedLink: 1}
                            },
                            2: {
                                1: {
                                    isClassicView: true,
                                    isThinWL: true,
                                    selectedLink: 1
                                }
                            },
                            4: {
                                1: {selectedLink: 1}
                            },
                            5: {
                                1: {
                                    dockedWidgetId: 4,
                                    selectedLink: 1
                                }
                            }
                        }
                    },
                    2: {
                        w: {
                            1: {
                                1: {selectedLink: 1}
                            },
                            2: {
                                1: {selectedLink: 1}
                            },
                            3: {
                                1: {selectedLink: 1}
                            },
                            4: {
                                1: {selectedLink: 1, isOptionMode: true}
                            }
                        }
                    }
                }
            },
            6: {
                tab: {
                    1: {
                        w: {
                            1: {
                                1: {isClassicView: true}
                            },
                            2: {
                                1: {type: 11}
                            },
                            3: {
                                1: {type: 77}
                            }
                        }
                    },
                    2: {
                        w: {
                            1: {
                                1: {isClassicView: true}
                            }
                        }
                    },
                    3: {
                        w: {
                            1: {
                                1: {isClassicView: true}
                            }
                        }
                    }
                }
            },
            10: {
                tab: {
                    1: {
                        w: {
                            1: {
                                1: {isKYC: true}
                            }
                        }
                    }
                }
            }
        },

        rightPanel: {
            4: {selectedLink: 1}
        }
    };

    return {
        layout: layout,
        args: args
    };
});
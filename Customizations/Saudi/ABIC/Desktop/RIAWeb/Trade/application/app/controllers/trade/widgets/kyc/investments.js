import utils from '../../../../utils/utils';
import BaseController from '../../../base-controller';
import Ember from 'ember';

export default BaseController.extend({
    dataObj: {},

    totVal: function () {
        var total = 0;
        var dataObject = this.get('dataObj');

        Ember.$.map(dataObject, function (item) {
            total += parseInt(item ? item : 0, 10);
        });

        return total;
    }.property('dataObj.sharesHighRisk', 'dataObj.sharesMedRisk', 'dataObj.sharesLowRisk', 'dataObj.debtInstrmntsHighRisk', 'dataObj.debtInstrmntsMedRisk', 'dataObj.debtInstrmntsLowRisk', 'dataObj.invstmntFundHighRisk', 'dataObj.invstmntFundMedRisk', 'dataObj.invstmntFundLowRisk', 'dataObj.tradeFinanceLowRisk', 'dataObj.tradeFinanceHighRisk', 'dataObj.tradeFinanceMedRisk', 'dataObj.commdtsHighRisk', 'dataObj.commdtsLowRisk', 'dataObj.commdtsMedRisk', 'dataObj.optionsHighRisk', 'dataObj.optionsMedRisk', 'dataObj.optionsLowRisk'),

    onPrepareData: function () {
        var businessObj = this.get('businessObj');

        if (businessObj) {
            this.set('dataObj.sharesHighRisk', businessObj.get('sharesHighRisk'));
            this.set('dataObj.sharesMedRisk', businessObj.get('sharesMedRisk'));
            this.set('dataObj.debtInstrmntsHighRisk', businessObj.get('debtInstrmntsHighRisk'));
            this.set('dataObj.debtInstrmntsMedRisk', businessObj.get('debtInstrmntsMedRisk'));
            this.set('dataObj.debtInstrmntsLowRisk', businessObj.get('debtInstrmntsLowRisk'));
            this.set('dataObj.invstmntFundHighRisk', businessObj.get('invstmntFundHighRisk'));
            this.set('dataObj.invstmntFundMedRisk', businessObj.get('invstmntFundMedRisk'));
            this.set('dataObj.invstmntFundLowRisk', businessObj.get('invstmntFundLowRisk'));
            this.set('dataObj.tradeFinanceLowRisk', businessObj.get('tradeFinanceLowRisk'));
            this.set('dataObj.commdtsHighRisk', businessObj.get('commdtsHighRisk'));
            this.set('dataObj.optionsHighRisk', businessObj.get('optionsHighRisk'));
            this.set('dataObj.sharesLowRisk', businessObj.get('sharesLowRisk'));
            this.set('dataObj.tradeFinanceHighRisk', businessObj.get('tradeFinanceHighRisk'));
            this.set('dataObj.tradeFinanceMedRisk', businessObj.get('tradeFinanceMedRisk'));
            this.set('dataObj.commdtsMedRisk', businessObj.get('commdtsMedRisk'));
            this.set('dataObj.commdtsLowRisk', businessObj.get('commdtsLowRisk'));
            this.set('dataObj.optionsMedRisk', businessObj.get('optionsMedRisk'));
            this.set('dataObj.optionsLowRisk', businessObj.get('optionsLowRisk'));
        }
    },

    getDataObject: function () {
        return this.get('dataObj');
    },

    onValidate: function () {
        var isRequiredFieldsAvailable = true;
        var errorCollection = [];

        var dataObject = this.get('dataObj');
        var app = this.get('app');

        Ember.$.map(dataObject, function (item) {
            if (!utils.validators.isAvailable(item)) {
                isRequiredFieldsAvailable = false;
                return true;
            }
        });

        if (this.get('totVal') !== 100) {
            errorCollection.pushObject(app.lang.messages.totalHundredMessage);
        }

        if (!isRequiredFieldsAvailable) {
            errorCollection.pushObject(app.lang.messages.mandatoryFields);
        }

        return errorCollection;
    }
});
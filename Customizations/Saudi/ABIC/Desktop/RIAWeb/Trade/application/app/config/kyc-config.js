export default {
    kycInfo: {
        tabArray: [
            // Field Types: textField: 0, dropDownField: 1, dateField: 2, textAreaField: 3, check-box: 4
            // Data Types: string: 0, int: 1, email: 2, double: 3
            {ID: 0, langKey: 'generalInformation', DisplayName: 'General Information', key: 'generalInfo', isDynamic: true, dataCollTab: [
                {grpId: 0, langKey: 'personalInfo', grpDes: 'Personal Info', dataCollGroup: [
                    {langKey: 'firstName', fieldType: 0, prop: 'firstName', dataType: 0, isDisabled: true, isRequired: true},
                    {langKey: 'secondName', fieldType: 0, prop: 'secondName', dataType: 0, isDisabled: true, isRequired: true},
                    {langKey: 'thirdName', fieldType: 0, prop: 'thirdName', dataType: 0, isDisabled: false, isRequired: true},
                    {langKey: 'familyName', fieldType: 0, prop: 'familyName', dataType: 0, isDisabled: true, isRequired: true},
                    {langKey: 'gender', fieldType: 1, typId: '38', prop: 'gender', dataType: 0, isDisabled: false, isRequired: true},
                    {langKey: 'nationality', fieldType: 1, typId: '2', prop: 'nationality', isRequired: true, dataType: 0, isDisabled: true},
                    // {langKey: 'dobHijri', fieldType: 2, prop: 'dobHijri', isRequired: false, dataType: 0, isDisabled: false},
                    {langKey: 'dob', fieldType: 2, prop: 'dob', isRequired: false, isDisabled: false, endDate: 0},
                    {langKey: 'pobCountry', fieldType: 1, typId: '2', prop: 'cob', isRequired: true, dataType: 0, isDisabled: false}
                ]},
                {grpId: 1, langKey: 'idInfo', grpDes: 'Identity Info', dataCollGroup: [
                    {langKey: 'idType', fieldType: 1, typId: '33', dataType: 0, isDisabled: true, prop: 'identityType', isRequired: true},
                    {langKey: 'idNo', fieldType: 0, dataType: 0, isDisabled: true, prop: 'identityNumber', isRequired: true},
                    {langKey: 'issueDate', fieldType: 2, dataType: 0, isDisabled: true, prop: 'issueDate', isRequired: true, endDate: 0},
                    // {langKey: 'issueDateHijri', fieldType: 2, dataType: 0, isDisabled: true, prop: 'issueDateHijri', isRequired: true},
                    {langKey: 'issuePlace', fieldType: 1, typId: '34', dataType: 0, isDisabled: true, prop: 'issuePlace', isRequired: false},
                    // {langKey: 'idExpDateHijri', fieldType: 2, dataType: 0, isDisabled: true, prop: 'idExpDateHijri', isRequired: true},
                    {langKey: 'idExpDate', fieldType: 2, dataType: 0, isDisabled: true, prop: 'idExpDate', isRequired: true},
                    {langKey: 'isBeneficiaryOwner', fieldType: 1, typId: '52', prop: 'isBeneficiaryOwner', isRequired: false, dataType: 0, isDisabled: false},
                    {langKey: 'beneficiaryOwnerName', fieldType: 0, dataType: 0, isDisabled: false, prop: 'beneficiaryOwnerName', isRequired: true, depends: {field: 'isBeneficiaryOwner', enabled: [0]}},
                    {langKey: 'relationship', fieldType: 0, dataType: 0, isDisabled: false, prop: 'relationship', depends: {field: 'isBeneficiaryOwner', enabled: [0]}}
                ]},
                {grpId: 2, langKey: '', grpDes: '', dataCollGroup: [
                    {langKey: 'anotherNationality', fieldType: 1, typId: '52', prop: 'anotherNationality', isRequired: true, dataType: 0, isDisabled: false}
                ]},
                {grpId: 3, langKey: '', grpDes: '', dataCollGroup: [
                    {langKey: 'ifYesSpecify', fieldType: 1, typId: '2', prop: 'otherNationalityId', isRequired: false, dataType: 0, isDisabled: false, depends: {field: 'anotherNationality', enabled: [1]}}
                ]}
            ]},
            {ID: 1, langKey: 'contactInformation', DisplayName: 'Contact Information', key: 'contactInformation', isDynamic: true, dataCollTab: [
                {grpId: 0, langKey: 'contactInformation', grpDes: 'Contact Info', isSubHeadingEnable: false, dataCollGroup: [
                    {id: '0', displayDes: 'P.O Box', langKey: 'poBox', fieldType: 0, prop: 'pobox', dataType: 0, isDisabled: false, isRequired: true},
                    {id: '1', displayDes: 'Zip', langKey: 'zip', fieldType: 0, prop: 'zip', isRequired: true, dataType: 0, isDisabled: false},
                    {id: '4', displayDes: 'Country', langKey: 'country', fieldType: 1, typId: '2', prop: 'country', dataType: 0, isDisabled: false, isRequired: true},
                    {id: '5', displayDes: 'City', langKey: 'city', fieldType: 1, typId: '34', prop: 'city', dataType: 0, isDisabled: false, isRequired: true, depends: {field: 'country'}},
                    {id: '8', displayDes: 'Home Tel', langKey: 'homePhoneNo', fieldType: 0, prop: 'homeTel', isRequired: true, dataType: 0, isDisabled: false},
                    {id: '9', displayDes: 'Mobile No', langKey: 'mobileNo', fieldType: 0, prop: 'mobileNo', isRequired: true, dataType: 0, isDisabled: true},
                    {id: '11', displayDes: 'Email Address', langKey: 'emailAddress', fieldType: 0, prop: 'email', isRequired: false, dataType: 2, isDisabled: false},
                    {id: '3', displayDes: 'Wasel', langKey: 'wasel', fieldType: 0, prop: 'wasel', dataType: 0, isDisabled: false, isRequired: false},
                    {id: '2', displayDes: 'House No', langKey: 'houseNo', fieldType: 0, prop: 'houseNo', dataType: 1, isDisabled: false, isRequired: false},
                    {id: '3', displayDes: 'Area', langKey: 'chartArea', fieldType: 0, prop: 'area', dataType: 0, isDisabled: false, isRequired: true},
                    {id: '7', displayDes: 'Street Name', langKey: 'streetName', fieldType: 0, prop: 'streetName', isRequired: true, dataType: 0, isDisabled: false},
                    {id: '2', displayDes: 'Zip Code No', langKey: 'zipCodeNo', fieldType: 0, prop: 'zipCodeNo', dataType: 0, isDisabled: false, isRequired: false},
                    {id: '2', displayDes: 'Additional No', langKey: 'additionalNo', fieldType: 0, prop: 'additionalNo', dataType: 1, isDisabled: false, isRequired: false},
                    {id: '2', displayDes: 'Units No', langKey: 'unitsNo', fieldType: 0, prop: 'unitsNo', dataType: 1, isDisabled: false, isRequired: false},
                    {id: '10', displayDes: 'Major Landmark', langKey: 'majorLandmark', fieldType: 0, prop: 'majorLandmark', isRequired: false, dataType: 0, isDisabled: false}
                ]}
            ]},
            {ID: 2, langKey: 'socialStatus', DisplayName: 'Social Status', key: 'bankInfo', isDynamic: true, dataCollTab: [
                {grpId: 0, langKey: 'bankInfo', grpDes: '', isSubHeadingEnable: false, dataCollGroup: [
                    {langKey: 'maritalSts', fieldType: 1, typId: '19', prop: 'maritalStatus', isRequired: true, dataType: 0, isDisabled: false},
                    {langKey: 'noOfDependencies', fieldType: 0, prop: 'nod', isRequired: true, dataType: 1, isDisabled: false},
                    {langKey: 'residenceType', fieldType: 1, typId: '62', prop: 'residenceType', isRequired: true, dataType: 0, isDisabled: false}
                ]}
            ]},
            {ID: 3, langKey: 'educationLevel', DisplayName: 'Educational Level', key: 'empInfo', isDynamic: true, dataCollTab: [
                {grpId: 0, langKey: 'contactInformation', grpDes: 'Contact Info', dataCollGroup: [
                    {langKey: 'educationLevel', fieldType: 1, typId: '54', prop: 'eduLevel', dataType: 0, isDisabled: false, isRequired: true},
                    {langKey: '', fieldType: 0, dataType: 0, isDisabled: false, prop: 'eduLevelOther', depends: {field: 'eduLevel', enabled: ['4']}}
                ]}
            ]},
            {ID: 4, langKey: 'employmentInformation', DisplayName: 'Employment Information', key: 'attorneyInfo', isDynamic: true, dataCollTab: [
                {grpId: 0, langKey: '', grpDes: '', isSubHeadingEnable: false, dataCollGroup: [
                    {langKey: 'empStatus', fieldType: 1, typId: '53', prop: 'empStatus', dataType: 0, isDisabled: false, isRequired: true},
                    {langKey: '', fieldType: 0, dataType: 0, isDisabled: false, prop: 'empStatusOther', depends: {field: 'empStatus', enabled: ['2']}},
                    // {langKey: 'dateOfEmpHijri', fieldType: 2, prop: 'empDate', dataType: 0, isDisabled: false, isRequired: true},
                    {langKey: 'dateOfEmp', fieldType: 2, prop: 'empDate', dataType: 0, isDisabled: false, isRequired: true},
                    {langKey: 'compName', fieldType: 0, prop: 'nameEMp', dataType: 0, isDisabled: false, isRequired: true},
                    {langKey: 'poBox', fieldType: 0, prop: 'poboxEmp', dataType: 0, isDisabled: false, isRequired: true},
                    {langKey: 'country', fieldType: 1, typId: '2', prop: 'countryEmp', dataType: 0, isDisabled: false, isRequired: true},
                    {langKey: 'city', fieldType: 1, typId: '34', prop: 'cityEmp', dataType: 0, isDisabled: false, isRequired: true, depends: {field: 'countryEmp'}},
                    {langKey: 'phoneNo', fieldType: 0, prop: 'phoneNoEmp', dataType: 0, isDisabled: false, isRequired: true},
                    {langKey: 'zipCodeNo', fieldType: 0, prop: 'zipCodeNoEmp', dataType: 0, isDisabled: false, isRequired: true},
                    {langKey: 'jobTitle', fieldType: 1, typId: '31', prop: 'profession', isRequired: true, dataType: 0, isDisabled: false},
                    {langKey: 'isClientDirectorCom', fieldType: 1, typId: '52', prop: 'dirOrOff', isRequired: true, dataType: 0, isDisabled: false},
                    {langKey: '', fieldType: 0, dataType: 0, isDisabled: false, prop: 'dirComName', depends: {field: 'dirOrOff', enabled: [1]}},
                    {langKey: 'politicalPerson', fieldType: 1, typId: '52', prop: 'pep', isRequired: true, dataType: 0, isDisabled: false}
                ]}
            ]},
            {ID: 5, langKey: 'financialInformation', DisplayName: 'Financial Information', key: 'financial', isDynamic: true, dataCollTab: [
                {grpId: 0, langKey: 'financialInfo', grpDes: 'Financial Info', dataCollGroup: [
                    {langKey: 'appAnnualIncome', fieldType: 1, typId: '50', prop: 'annualIncome', dataType: 0, isDisabled: false, isRequired: true},
                    {langKey: 'appNetworth', fieldType: 1, typId: '51', prop: 'netWorth', dataType: 0, isDisabled: false, isRequired: true}
                ]},
                {grpId: 1, langKey: 'financialInfo', grpDes: 'Financial Info', dataCollGroup: [
                    {langKey: 'otherSourcesIncomeWealth', fieldType: 1, typId: '52', prop: 'isOtherIncome', dataType: 0, isDisabled: false, isRequired: true},
                    {langKey: 'ifYesSpecify', fieldType: 4, typId: '56', prop: 'otherIncome', dataType: 0, isDisabled: false, isRequired: false, itemList: '', depends: {field: 'isOtherIncome', enabled: [1]}},
                    {langKey: 'specifyTotAmt', fieldType: 0, prop: 'sourceWealthAmt', isRequired: true, dataType: 0, isDisabled: false, depends: {field: 'isOtherIncome', enabled: [1]}}
                ]},
                {grpId: 2, langKey: 'bankInfo', grpDes: 'Bank Information', isSubHeadingEnable: false, dataCollGroup: [
                    {langKey: 'bnkName', fieldType: 1, typId: '32', prop: 'bnkName', dataType: 0, isDisabled: true, isRequired: true},
                    {langKey: 'branchName', fieldType: 0, prop: 'bnkBranch', isRequired: true, dataType: 0, isDisabled: true},
                    {langKey: 'accNo', fieldType: 0, prop: 'bnkAcc', dataType: 0, isDisabled: true, isRequired: true},
                    {langKey: 'businessVolume', fieldType: 0, prop: 'businessVol', dataType: 0, isDisabled: false, isRequired: true},
                    {langKey: 'accName', fieldType: 0, prop: 'accName', dataType: 0, isDisabled: true, isRequired: true},
                    {langKey: 'ibanNo', fieldType: 0, prop: 'ibanNo', dataType: 0, isDisabled: true, isRequired: true},
                    {langKey: 'accType', fieldType: 1, prop: 'accType', dataType: 0, typId: '55', isDisabled: true, isRequired: true},
                    {langKey: 'curr', fieldType: 0, prop: 'curr', dataType: 0, isDisabled: true, isRequired: true}
                ]}
            ]},
            {ID: 6, langKey: 'investmentInformation', DisplayName: 'Investment Information', isDynamic: true, outlet: 'investment-info', key: 'investmentInfo', dataCollTab: [
                {grpId: 0, langKey: '', grpDes: '', dataCollGroup: [
                    {langKey: 'investmentPercentage', fieldType: 1, typId: '57', prop: 'capitalAmt', dataType: 0, isDisabled: false, isRequired: true},
                    {langKey: 'investmentPeriod', fieldType: 1, typId: '58', dataType: 0, isDisabled: false, prop: 'invPeriod', isRequired: true},
                    {langKey: 'investmentExperience', fieldType: 1, typId: '59', prop: 'invExp', dataType: 0, isDisabled: false, isRequired: true},
                    {langKey: 'riskTolerance', fieldType: 1, typId: '60', prop: 'riskTol', dataType: 0, isDisabled: false, isRequired: true},
                    {langKey: 'generalInvestmentObjectives', fieldType: 1, typId: '61', prop: 'invObjec', dataType: 0, isDisabled: false, isRequired: true}
                ]},
                {grpId: 1, langKey: '', grpDes: '', dataCollGroup: [
                    {langKey: 'isTradedInLocalShares', fieldType: 1, typId: '52', prop: 'isTradedLocal', dataType: 1, isDisabled: false, isRequired: true},
                    {langKey: 'forYears', fieldType: 0, dataType: 1, isDisabled: false, prop: 'TradedLocal', isRequired: true, depends: {field: 'isTradedLocal', enabled: [1]}},
                    {langKey: 'isTradedInInternationalShares', fieldType: 1, typId: '52', dataType: 1, isDisabled: false, prop: 'isTradedInternational', isRequired: true},
                    {langKey: 'forYears', fieldType: 0, dataType: 1, isDisabled: false, prop: 'TradedInternational', isRequired: true, depends: {field: 'isTradedInternational', enabled: [1]}},
                    {langKey: 'isTradedInBondsSukuk', fieldType: 1, typId: '52', prop: 'isTradedBonds', dataType: 1, isDisabled: false, isRequired: true},
                    {langKey: 'forYears', fieldType: 0, dataType: 1, isDisabled: false, prop: 'TradedBonds', isRequired: true, depends: {field: 'isTradedBonds', enabled: [1]}},
                    {langKey: 'hasOtherCompanyInvestmentAcc', fieldType: 1, typId: '52', prop: 'invAccOtherCom', dataType: 1, isDisabled: false, isRequired: true},
                    {langKey: 'forYears', fieldType: 0, dataType: 1, isDisabled: false, prop: 'invAccOtherComSince', isRequired: true, depends: {field: 'invAccOtherCom', enabled: [1]}}
                ]}
            ]},
            {ID: 7, langKey: 'clientIdealInvestment', DisplayName: 'Client\'s Ideal Investments', isDynamic: false, outlet: 'investments', key: 'investments', dataCollTab: [{grpId: 0, langKey: 'gender', grpDes: 'Preferences', dataCollGroup: [
            ]}]}
        ]
    },

    kycMasterData: {
        Gender: [
            {code: 'Male', langKey: 'male'},
            {code: 'Female', langKey: 'female'}
        ],

        Income: [
            {code: 0, des: '< 200,000', langKey: 'lessThanTwoLacks'},
            {code: 1, des: '200,001 - 500,000', langKey: 'betweenTwoAndFiveLacks'},
            {code: 2, des: '500,001 - 1,000,000', langKey: 'betweenFiveAndTenLacks'},
            {code: 3, des: '> 1,000,000', langKey: 'overTenLacks'}
        ],

        Networth: [
            {code: 0, des: '< 500,000', langKey: 'lessThanFiveLacks'},
            {code: 1, des: '500,001 - 1,000,000', langKey: 'betweenFiveAndTenLacks'},
            {code: 2, des: '1,000,001 - 5,000,000', langKey: 'betweenTenAndFiftyLacks'},
            {code: 3, des: '> 5,000,000', langKey: 'overFiftyLacks'}
        ],

        YesNo: [
            {code: 1, langKey: 'yes'},
            {code: 0, langKey: 'no'}
        ],

        Residence: [
            {code: '0', langKey: 'owned'},
            {code: '1', langKey: 'family'},
            {code: '2', langKey: 'workResidence'},
            {code: '3', langKey: 'rent'}
        ],

        EmployeeStatus: [
            {code: '0', langKey: 'employee'},
            {code: '1', langKey: 'employer'},
            {code: '2', langKey: 'other'}
        ],

        EducationLevel: [
            {code: '0', langKey: 'secSchool'},
            {code: '1', langKey: 'diploma'},
            {code: '2', langKey: 'uni'},
            {code: '3', langKey: 'postGraduate'},
            {code: '4', langKey: 'other'}
        ],

        AccountType: [
            {code: 26, langKey: 'check'},
            {code: 0, langKey: 'current'},
            {code: 3, langKey: 'joint'},
            {code: 1, langKey: 'saving'}
        ],

        IncomeSources: [
            {code: 0, langKey: 'additionalSalary'},
            {code: 1, langKey: 'business'},
            {code: 2, langKey: 'realEstate'},
            {code: 3, langKey: 'pension'},
            {code: 4, langKey: 'inheritance'}
        ],

        CapitalAmount: [
            {code: 1, langKey: 'lessThanTwentyFivePer'},
            {code: 2, langKey: 'betweenTwentyFiveAndFiftyPer'},
            {code: 3, langKey: 'betweenFiftyAndSeventyFivePer'},
            {code: 4, langKey: 'overSeventyFivePer'}
        ],

        InvestmentPeriod: [
            {code: 1, langKey: 'lessThanOneYear'},
            {code: 2, langKey: 'oneTwoYears'},
            {code: 3, langKey: 'twoFiveYears'},
            {code: 4, langKey: 'moreThanFiveYears'}
        ],

        InvestmentExperience: [
            {code: '0', langKey: 'limited'},
            {code: '1', langKey: 'good'},
            {code: '2', langKey: 'extensive'}
        ],

        RiskTolerance: [
            {code: '0', langKey: 'high'},
            {code: '1', langKey: 'medium'},
            {code: '2', langKey: 'low'}
        ],

        InvestmentObjectives: [
            {code: '0', langKey: 'protectCapital'},
            {code: '1', langKey: 'income'},
            {code: '2', langKey: 'balanced'},
            {code: '3', langKey: 'growth'}
        ],

        // TODO: [Chamalee] Remove hardcoded arrays used for dependent dropdown functional testing after OMS Integration

        CountryTest: [
            {code: '0', des: 'SL'},
            {code: '1', des: 'SA'},
            {code: '2', des: 'UK'},
            {code: '3', des: 'USA'}
        ],

        CityTest: [
            {code: '0', des: 'Kugala', country: '0'},
            {code: '1', des: 'Colombo', country: '0'},
            {code: '2', des: 'Riyadh', country: '1'},
            {code: '3', des: 'Mecca', country: '1'},
            {code: '4', des: 'London', country: '2'},
            {code: '5', des: 'Bristol', country: '2'},
            {code: '6', des: 'Sanfrancisco', country: '3'},
            {code: '7', des: 'New york', country: '3'}
        ]
    },

    KYCMasterDataTypes: {
        Title: '18',
        MaritalStatus: '19',
        City: '34',
        Country: '2',
        Profession: '31',
        Bank: '32',
        IdentityType: '33',
        Gender: '38',
        Income: '50',
        Networth: '51',
        YesNo: '52',
        Residence: '62',
        EmployeeStatus: '53',
        EducationLevel: '54',
        AccountType: '55',
        IncomeSources: '56',
        CapitalAmount: '57',
        InvestmentPeriod: '58',
        InvestmentExperience: '59',
        RiskTolerance: '60',
        InvestmentObjectives: '61',
        CountryTest: '70',
        CityTest: '71'
    }
};

export default {
    appConfig: {
        customisation: {
            authenticationMode: 4,
            isTradingEnabled: true,
            hashType: 'MD5',
            isUpperCasePassword: true,
            isDiscloseQtyEnabled: true,
            smartLoginEnabled: false,

            loginViewSettings: {
                isSignUpEnabled: false,
                isForgotPasswordEnabled: false,
                isPoweredByEnabled: true,
                isRegisterEnabled: false
            },

            passwordRules: {
                maxLength: 18,
                minLength: 8,
                checkLength: true,
                checkContainNumber: true,
                checkStartWithLetter: true,
                checkContainSmallLetter: true,
                checkContainCapitalLetter: true,
                checkIdenticalCharacter: true,
                checkConsecutiveCharacter: true,
                checkSpecialCharacter: true,
                checkUsernameMatch: true
            },

            usernameRules: {
                maxLength: 12,
                minLength: 6,
                checkLength: true,
                checkEngAlphaNumeric: true,
                checkStartWithLetter: true,
                checkIdenticalCharacter: true
            }
        },

        widgetId: {
            quoteMenuId: 'fullQuote',
            watchListMenuId: 'heatMap',
            marketMenuId: 'market',
            indicesTabId: 'companyProf',
            topStocksTabId: 'topStocks',
            portfolioMenuId: 'standard',
            orderTicketMenuId: 'classic'
        },

        mutualFund: {
            /*eslint-disable */
            agreementPDF_EN: 'http://www.alistithmarcapital.com/pdfs/120-SAIB-Saudi-Companies-Fund.pdf',
            agreementPDF_AR: 'http://www.alistithmarcapital.com/pdfs/120-SAIB-Saudi-Companies-Fund.pdf'
            /*eslint-enable */
        },

        chartConfig: {
            chartIndicators: ['AccumulationDistribution', 'AverageTrueRange', 'BollingerBands', 'ChaikinMF', 'ChandeMomentumOscillator', 'MoneyFlowIndex', 'MovingAverage', 'PSAR', 'RelativeStrengthIndex', 'TimeSeriesForecast', 'TRIX', 'VolOsc', 'WildersSmoothing', 'WilliamsPerR']
        }
    },

    tradeSettings: {
        channelId: 30,

        connectionParameters: {
            primary: {
                ip: '192.168.60.12',
                port: '8090/trs',
                secure: false
            },

            secondary: {
                ip: '192.168.60.12',
                port: '8090/trs',
                secure: false
            }
        }
    }
};

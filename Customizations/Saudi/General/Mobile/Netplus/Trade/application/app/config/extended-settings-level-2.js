export default {
    appConfig: {
        customisation: {
            authenticationMode: 4,
            isTradingEnabled: true,
            hashType: 'SHA-1'
        },

        widgetId: {
            quoteMenuId: 'fullQuote',
            watchListMenuId: 'heatMap',
            marketMenuId: 'market',
            indicesTabId: 'companyProf',
            topStocksTabId: 'topStocks',
            portfolioMenuId: 'standard',
            orderTicketMenuId: 'classic'
        }
    },

    tradeSettings: {
        channelId: 30,

        connectionParameters: {
            primary: {
                ip: 'wstrs-qa.directfn.net/trs',
                port: '',
                secure: true
            },

            secondary: {
                ip: 'wstrs-qa.directfn.net/trs',
                port: '',
                secure: true
            }
        }
    },

    userSettings: {
        orderConfig: {
            defaultOrderQty: 0
        }
    }
};

export default {
    appConfig: {
        customisation: {
            appName: 'NetPlus New',
            clientPrefix: 'aljazira',
            authenticationMode: 2,
            isAlertEnabled: false
        },

        googleAnalyticConfig: {
            id: 'UA-72340908-1'
        }
    },

    priceSettings: {
        connectionParameters: {
            primary: {
                ip: 'data-sa.mubasher.net/html5ws',
                port: '',
                secure: true
            },

            secondary: {
                ip: 'data-sa.mubasher.net/html5ws',
                port: '',
                secure: true
            }
        }
    }
};

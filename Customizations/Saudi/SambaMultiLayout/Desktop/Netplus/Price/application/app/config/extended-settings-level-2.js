export default {
    appConfig: {
        customisation: {
            clientPrefix: 'icap',
            isUpperCasePassword: true
        }
    }
};
import Ember from 'ember';
import BaseWidgetContainer from '../base-widget-container';
import LanguageDataStore from '../../models/shared/language/language-data-store';
import ThemeDataStore from '../../models/shared/data-stores/theme-data-store';
import MessageViewer from '../../components/message-viewer';
import SettingsDropdown from '../../components/settings-dropdown';
import messageService from '../../utils/message-service';
import utils from '../../utils/utils';
import appConfig from '../../config/app-config';
import sharedService from '../../models/shared/shared-service';
import PriceConstants from '../../models/price/price-constants';
import responsiveHandler from '../../helpers/responsive-handler';

export default BaseWidgetContainer.extend({
    // Subscription key
    containerKey: 'titleBar',
    layoutName: 'layout/title-bar-samba',
    userSettings: null,
    appGlobal: undefined,
    msgArray: Ember.A(),
    gaKey: 'title-bar',
    toLang: LanguageDataStore.getChangeLanguageObj(),
    appVersion: appConfig.appVersion,

    settings: [],
    supportedLanguages: LanguageDataStore.getUserLanguages(),
    supportedThemes: ThemeDataStore.getUserThemes(),
    isLanguageOptionAvailable: false,

    // This is used by global search component
    searchKey: '',

    tradeService: {},
    isTradingEnabled: false,
    priceService: {},
    app: LanguageDataStore.getLanguageObj(),

    defaultComp: undefined,
    currentComp: undefined,
    currentOutlet: undefined,
    cssStyleOne: '',
    cssStyleTwo: '',
    cssStyleDefault: '',
    newMessage: Ember.A(),
    messageArray: {},
    isNewMessageAvailable: true,
    messageOuterContainerCss: '',

    // Tablet configs
    isTablet: appConfig.customisation.isTablet,
    isSearchEnable: true,
    isChangeWidth: false,
    showPriceConnection: true,
    isLastLoginTimeEnable: true,

    titleConstants: {
        outletOne: 'titleBarOutletOne',
        outletTwo: 'titleBarOutletTwo',
        outletDefault: 'titleBarOutletDefault',

        cssConstants: {
            lowerDivCss: 'div-down',
            upperDivCss: 'div-up',
            fadeOut: 'hide',
            fadeIn: 'show'
        }
    },

    topPanelSettings: {
        intZero: 0,
        emptyString: '',

        styles: {
            upColor: 'up-fore-color',
            downColor: 'down-fore-color',
            foreColor: 'fore-color'
        },

        timeInterval: {
            OneSecondInMillis: 1000,
            OneMinuteInMillis: 60000
        }
    },

    isShowUpgrade: function () {
        return utils.validators.isAvailable(appConfig.subscriptionConfig.upgradeSubscriptionPath) && this.priceService.userDS.isDelayedExchangesAvailable();
    }.property('priceService.userDS.delayedExchg.length'),

    isShowRenewal: function () {
        return utils.validators.isAvailable(appConfig.subscriptionConfig.renewSubscriptionPath) && this.priceService.userDS.isRealtimeExchangesAvailable() && this.priceService.userDS.willExpireRecently();
    }.property('priceService.userDS.expiryDate', 'priceService.userDS.delayedExchg.length', 'priceService.userDS.userExchg.length'),

    init: function () {
        this._super();
        this.priceService = sharedService.getService('price');

        var isTradingEnabled = appConfig.customisation.isTradingEnabled;
        this.set('isTradingEnabled', isTradingEnabled);
        this.set('showPriceConnection', !isTradingEnabled || this.get('isTablet'));

        if (isTradingEnabled) {
            var cusName = sharedService.getService('trade').fieldConfigs.cusName;

            // TODO: [satheeq] Verify and remove trade service access from title bar.
            this.set('tradeService', sharedService.getService('trade'));

            if (cusName) {
                this.addObserver('tradeService.userDS.' + cusName, this.setUsername);
            } else {
                this.addObserver('tradeService.userDS.cusNme', this.setUsername);
            }
        }

        this.set('userSettings', sharedService.userSettings);
        this.set('appGlobal', Ember.appGlobal);

        messageService.set('titleBar', this);      // Title bar shows messages

        this.initializeSettings();
        this.setTabletConfigs();

        // TODO [AROSHA] Move this Scroll enable method to global.
        Ember.run.later(function () {
            Ember.$('.nano').nanoScroller();
        }, 1000);
    },

    _updateMarketStatusColor: function () {
        var marketStatusCSS;
        var stat = this.get('exchange.stat');
        var marketStatusElement = Ember.$('#marketSummaryExchangeStatusContainer');

        if (stat === PriceConstants.MarketStatus.Close || stat === PriceConstants.MarketStatus.PreClose) {
            marketStatusCSS = this.topPanelSettings.styles.downColor;
            marketStatusElement.addClass('ms-top-bar-exchange-status-alert');
        } else {
            marketStatusCSS = this.topPanelSettings.styles.upColor;
            marketStatusElement.addClass('ms-top-bar-exchange-status-alert');
        }

        setTimeout(function () {
            marketStatusElement.removeClass('ms-top-bar-exchange-status-alert');
        }, 1000);

        this.set('marketStatusCSS', marketStatusCSS);
    }.observes('exchange.stat'),

    onAfterRender: function () {
        this.initializeResponsive();
        this.set('exchange', this.priceService.exchangeDS.getExchange(sharedService.userSettings.currentExchange));
    },

    fullScreenToggleTitle: function () {
        return this.set('fullScreenToggleTitle', this.get('app').lang.labels.fullScreen);
    }.property(),

    initializeResponsive: function () {
        this.set('responsive', responsiveHandler.create({controller: this, widgetId: 'appTitle', callback: this.onResponsive}));

        this.responsive.addList('title-middle', [
            {id: 'title-welcome', width: 490},
            {id: 'title-search', width: 490}
        ]);

        this.responsive.initialize();
    },

    onResponsive: function () {
        // Will callback here when responsive event changed
        // ToDo Create base component class and add this into it
    },

    setTabletConfigs: function () {
        this.set('showWidgetButtons', !appConfig.customisation.isTablet);
    },

    setDefaultComponent: function (templateName, comp) {
        var defaultComp = {templateName: templateName, comp: comp};
        this.set('defaultComp', defaultComp);
        var currentComp = this.get('currentComp');

        if (!currentComp) {
            this.renderNotificationTemplate(defaultComp.templateName, defaultComp.comp, true);
        }
    },

    renderNotificationTemplate: function (templateName, comp, isDefault, timeout, containerCss, isQueueDisabled) {
        var that = this;
        var currentOutlet;

        if (!isDefault) {
            var currentComp = this.get('currentComp');
            var newMessage = this.get('newMessage');

            if (!isQueueDisabled && currentComp && currentComp.templateName) {
                newMessage.pushObject({templateName: templateName, comp: comp});
                this.set('isNewMessageAvailable', true);

            } else {
                this.set('currentComp', {templateName: templateName, comp: comp});
                this.set('newMessage', []);
            }
        }

        // This should move to single message viewer but should get full background
        if (comp && comp.type === utils.Constants.MessageTypes.Error) {
            if (appConfig.customisation.isMobile) {
                comp.set('backgroundCss', 'down-back-color');
            } else {
                this.set('messageOuterContainerCss', 'back-colour-red title-panel-def');
            }
        } else if (comp && comp.type === utils.Constants.MessageTypes.Info && appConfig.customisation.isMobile) {
            comp.set('backgroundCss', 'highlight-back-color-1');
        } else {
            this.set('messageOuterContainerCss', containerCss ? containerCss : '');
        }

        if (isDefault) {
            currentOutlet = this.get('titleConstants.outletDefault');
        } else {
            this.setCurrentOutlet();
            currentOutlet = this.get('currentOutlet');
            this.set('messageOuterContainerCss', this.get('messageOuterContainerCss') + ' ticker-panel-message');

            Ember.run.later(function () {
                var queuedMsgArray = that.get('newMessage');
                var queuedMsg = queuedMsgArray[queuedMsgArray.length - 1];

                if (that.get('isNewMessageAvailable') && queuedMsg && queuedMsg.templateName) {
                    that.set('currentComp', {});
                    that.renderNotificationTemplate(queuedMsg.templateName, queuedMsg.comp);
                }
            }, 5000);
        }

        var route = this.container.lookup('route:application');
        var containerPath = appConfig.customisation.isMobile ? 'layout/mobile/ticker-panel' : this.get('layoutName');

        route.render(templateName, {
            into: containerPath,
            outlet: currentOutlet,
            controller: comp
        });

        // TODO: [satheeqh] Refactor other hideNotification usages with timeout
        if (timeout && timeout > 0) {
            Ember.run.later(that, function () {
                that.hideNotificationTemplate(templateName);
            }, timeout);
        }
    },

    hideNotificationTemplate: function (templateName) {
        var that = this;
        that.set('messageOuterContainerCss', '');
        var currentComp = this.get('currentComp');

        if (currentComp && templateName === currentComp.templateName) {
            var defaultComp = this.get('defaultComp');
            var currentOutlet = this.get('currentOutlet');
            var animationTimeout = 400;

            if (defaultComp) {
                this.set('cssStyleOne', this.get('titleConstants.cssConstants.upperDivCss'));
                this.set('cssStyleTwo', this.get('titleConstants.cssConstants.upperDivCss'));
            } else {
                var route = this.container.lookup('route:application');
                var containerPath = appConfig.customisation.isMobile ? 'layout/mobile/ticker-panel' : 'layout/title-bar-samba';

                route.disconnectOutlet({
                    parentView: containerPath,
                    outlet: currentOutlet
                });

                that.set('currentOutlet', undefined);
            }

            this.set('currentComp', undefined);

            // Hide closing component after Animation
            Ember.run.later(function () {
                if (defaultComp) {
                    that.set('cssStyleDefault', that.get('titleConstants.cssConstants.fadeIn'));
                }
            }, animationTimeout);
        }
    },

    setCurrentOutlet: function () {
        var outletOne = this.get('titleConstants.outletOne');
        var outletTwo = this.get('titleConstants.outletTwo');
        var currentOutlet = this.get('currentOutlet');

        if (currentOutlet) {
            switch (currentOutlet) {
                case outletOne:
                    this.set('currentOutlet', outletTwo);
                    this.set('cssStyleOne', this.get('titleConstants.cssConstants.upperDivCss'));
                    this.set('cssStyleTwo', this.get('titleConstants.cssConstants.lowerDivCss'));
                    break;

                case outletTwo:
                    this.set('currentOutlet', outletOne);
                    this.set('cssStyleOne', this.get('titleConstants.cssConstants.lowerDivCss'));
                    this.set('cssStyleTwo', this.get('titleConstants.cssConstants.upperDivCss'));
                    break;
            }

        } else {
            this.set('currentOutlet', this.get('titleConstants.outletTwo'));
            this.set('cssStyleOne', this.get('titleConstants.cssConstants.upperDivCss'));
            this.set('cssStyleTwo', this.get('titleConstants.cssConstants.lowerDivCss'));
        }

        this.set('cssStyleDefault', this.get('titleConstants.cssConstants.fadeOut'));
    },

    initializeSettings: function () {
        var that = this;
        var isActive;
        var app = this.get('app');

        Ember.appGlobal.events.currentLayout = 'samba';
        Ember.appGlobal.events.isIcap = false;
        Ember.set(this.container.lookup('route:application').get('controller'), 'isSamba', true);

        Ember.$('html').removeClass('sfc');
        Ember.$('body').removeClass('sfc');

        Ember.$('html').addClass('samba');
        Ember.$('body').addClass('samba');

        this.get('settings').clear();
        this.get('settings').pushObject({code: 'layout:samba', des: 'Samba Layout', active: Ember.appGlobal.events.currentLayout === 'samba'});
        this.get('settings').pushObject({code: 'layout:icap', des: 'ICAP Layout', active: Ember.appGlobal.events.currentLayout === 'icap'});
        this.get('settings').pushObject({code: 'layout:sfc', des: 'SFC Layout', active: Ember.appGlobal.events.currentLayout === 'sfc'});

        if (appConfig.customisation.supportedLanguages.length > 1) {
            Ember.$.each(this.get('supportedLanguages'), function (index, item) {
                isActive = (item.code === sharedService.userSettings.currentLanguage);
                that.get('settings').pushObject({code: 'lang:' + item.code, des: item.desc, active: isActive});
            });

            // This object seperates theme and language lists.
            that.get('settings').pushObject({code: '', des: '', active: false});
            this.set('isLanguageOptionAvailable', true);
        }

        Ember.$.each(this.get('supportedThemes'), function (index, item) {
            isActive = (item.code === sharedService.userSettings.currentTheme);
            that.get('settings').pushObject({code: 'theme:' + item.code, des: app.lang.labels[item.langKey], active: isActive});
        });

        if (appConfig.customisation.isPasswordChangeEnable) {
            that.get('settings').pushObject({code: 'password:', des: app.lang.labels.changePassword, active: false});
        }

        if (utils.validators.isAvailable(appConfig.helpGuidePath)) {
            that.get('settings').pushObject({code: 'help:', des: app.lang.labels.help, active: false});
        }

        if (utils.validators.isAvailable(appConfig.subscriptionConfig.renewSubscriptionPath) && that.priceService.userDS.isRealtimeExchangesAvailable()) {
            that.get('settings').pushObject({code: 'renew:', des: app.lang.labels.renewSubscription, active: false});
        }

        if (appConfig.customisation.isAboutUsEnabled) {
            that.get('settings').pushObject({code: 'about:', des: app.lang.labels.aboutUs, active: false});
        }

        if (appConfig.customisation.isCacheClearEnabled) {
            that.get('settings').pushObject({code: 'clearCache:', des: app.lang.labels.clearCache, active: false});
        }
    },

    showMessage: function (messageTag, messageType) {
        var langMessage = '';
        var msgArray = this.get('msgArray');

        if (messageTag) {
            langMessage = LanguageDataStore.getLanguageObj().lang.messages[messageTag];
        }

        if (msgArray.length > 0) {      // Only one msg is shown for now. modify this when extended service.
            msgArray.removeAt(0);
        }

        var messageClass = messageType ? messageType === this.utils.Constants.MessageTypes.Error ? 'text-danger' : 'tittle-bar-price-connected' : '';

        msgArray.pushObject({type: messageType, messageTag: messageTag, content: langMessage, messageClass: messageClass});
    },

    onLanguageChanged: function () {
        var messageArray = this.get('msgArray');

        Ember.$.each(messageArray, function (index, item) {
            Ember.set(item, 'content', LanguageDataStore.getLanguageObj().lang.messages[item.messageTag]);
        });

        this.set('settings', []);

        this._updateSettings();
        this.initializeSettings();
    }.observes('userSettings.currentLanguage'),

    connectionStatusStyle: function () {
        return this.get('msgArray').length > 0 &&
        this.get('msgArray')[0].type === this.utils.Constants.MessageTypes.Error ? 'connection-status-red' : 'connection-status-green';
    }.property('msgArray.@each'),

    setUsername: function () {
        var username;
        var isTradingEnabled = this.get('isTradingEnabled');

        if (isTradingEnabled) {
            var lstLgnTme = this.tradeService.userDS.get('lstLgnTme');

            if (lstLgnTme) {
                this.set('isLastLoginTimeEnable', true);
                this.set('shortLastLoginTime', utils.formatters.formatToDayMonthTime(lstLgnTme));
                this.set('lastLoginTime', utils.formatters.formatToDateTime(lstLgnTme));
            } else {
                this.set('isLastLoginTimeEnable', false);
            }
        }

        username = this._setUserName();

        this.set('username', username);
    }.observes('appGlobal.session.id'),

    _setUserName: function () {
        if (this.get('isTradingEnabled')) {
            var cusName = this.tradeService.fieldConfigs.cusName;
            var tradeUser = this.tradeService.userDS;

            if (cusName && tradeUser.get(cusName)) {
                return tradeUser.get(cusName);
            } else if (tradeUser.get('cusNme')) {
                return tradeUser.get('cusNme');
            } else {
                return Ember.appGlobal.session.id;
            }
        } else {
            return Ember.appGlobal.session.id;
        }
    },

    _updateSettings: function () {
        var tempArray = [];

        Ember.$.each(this.get('settings'), function (index, item) {
            if (item.code) {
                tempArray = item.code.split(utils.Constants.StringConst.Colon);
                Ember.set(item, 'active', (tempArray.length > 1) && (tempArray[1] === sharedService.userSettings.currentTheme) ||
                    (tempArray[1] === sharedService.userSettings.currentLanguage) ||
                    (tempArray[1] === Ember.appGlobal.events.currentLayout));
            }
        });
    },

    _showPasswordPopup: function () {
        var changePasswordPopup = sharedService.getService('sharedUI').getService('changePasswordPopup');
        changePasswordPopup.send('showModalPopup', true);
    },

    _showAboutUsPopup: function () {
        var aboutUsPopup = sharedService.getService('sharedUI').getService('aboutUsPopup');
        aboutUsPopup.send('showModalPopup', true);
    },

    _showHelpGuide: function () {
        window.open(appConfig.helpGuidePath, '_blank');
    },

    _showRenewalPage: function () {
        window.open(utils.requestHelper.generateQueryString(appConfig.subscriptionConfig.renewSubscriptionPath, {
            user: sharedService.getService('price').userDS.username,
            language: sharedService.userSettings.get('currentLanguage')
        }), '_blank');
    },

    _clearCache: function () {
        var that = this;

        utils.messageService.showMessage(this.get('app').lang.messages.cacheClearMassage, utils.Constants.MessageTypes.Question, false, this.get('app').lang.labels.clearCache, [
            {
                type: 'yes', btnAction: function () {
                    that.priceService.priceMeta.clearSavedData();
                    that.priceService.priceExchangeMeta.clearSavedData();
                    that.priceService.priceSymbolMeta.clearSavedData();

                    that.utils.applicationSessionHandler.logout();
                }
            },
            {
                type: 'no', btnAction: undefined
            }], null
        );
    },

    _changeLayout: function (code) {
        Ember.appGlobal.events.currentLayout = code;
        this._updateSettings();

        sharedService.getService('sharedUI').loadLayout(code);
    },

    _changeSettings: function (selectedSetting) {
        var codeKeys = selectedSetting.code.split(this.utils.Constants.StringConst.Colon);

        if (codeKeys.length > 1) {
            switch (codeKeys[0]) {
                case 'theme':
                    ThemeDataStore.changeTheme(codeKeys[1]);
                    this._updateSettings();

                    this.utils.analyticsService.trackEvent(this.get('gaKey'), this.utils.Constants.GAActions.settingsChanged, ['theme:', codeKeys[1]].join(''));
                    break;

                case 'lang':
                    LanguageDataStore.changeLanguage(codeKeys[1]);
                    this._updateSettings();

                    this.utils.analyticsService.trackEvent(this.get('gaKey'), this.utils.Constants.GAActions.settingsChanged, ['language:', codeKeys[1]].join(''));
                    break;

                case 'password':
                    this._showPasswordPopup();
                    break;

                case 'about':
                    this._showAboutUsPopup();
                    break;

                case 'help':
                    this._showHelpGuide();
                    break;

                case 'renew':
                    this._showRenewalPage();
                    break;

                case 'clearCache':
                    this._clearCache();
                    break;

                case 'layout':
                    this._changeLayout(codeKeys[1]);
                    break;

                default:
                    break;
            }
        }
    },

    showSearchPopup: function () {
        if (this.get('searchKey') !== '') {
            var modal = this.get('topBarSymbolSearch');
            modal.send('showModalPopup');
        }
    },

    searchKeyDidChange: function () {
        var searchKey = this.get('searchKey');

        if (searchKey && searchKey.length >= appConfig.searchConfig.minCharLenForSymbol) {
            Ember.run.debounce(this, this.showSearchPopup, 300);
        }
    }.observes('searchKey'),

    actions: {
        showSearchBox: function () {
            var that = this;
            var currentState = this.get('isChangeWidth');

            this.set('searchKey', '');

            Ember.run.later(function () {
                that.set('isChangeWidth', !currentState);
            }, 10);
        },

        showSearchPopup: function () {
            this.showSearchPopup();
        },

        closeSearchPopup: function () {
            var modal = this.get('topBarSymbolSearch');

            modal.send('closeModalPopup');
            this.set('searchKey', '');
        },

        logout: function () {
            this.utils.analyticsService.trackEvent(this.get('gaKey'), this.utils.Constants.GAActions.logout);
            this.utils.applicationSessionHandler.logout();
        },

        toggleFullScreen: function () {
            // Checking whether the application is in fullscreen mode.
            // fullScreenElement is an Element that is in fullscreen if there is an element in fullscreen mode
            // It is undefined if there is no element in fullscreen mode. (standard)
            // mozFullScreen is either true or false depending whether the browser is in fullscreen mode and undefined
            // in browsers other than Firefox.
            // webkitIsFullScreen has the same behaviour as mozFullScreen for Chrome
            // msFullscreenElement is undefined in browsers other than IE
            // msFullscreenElement is null in IE when not in fullscreen mode
            // msFullscreenElement is an element (which is in fullscreen) when there is an element in fullscreen.
            var isInFullScreen = document.fullScreenElement || document.mozFullScreen || document.webkitIsFullScreen ||
                document.msFullscreenElement;

            if (isInFullScreen) {
                this.set('fullScreenToggleTitle', this.get('app').lang.labels.fullScreen);

                this.utils.fullScreenHelper.cancelFullScreen(document);
                this.utils.analyticsService.trackEvent(this.get('gaKey'), this.utils.Constants.GAActions.restore);
            } else {
                // document.body should be passed if the browser is IE
                // In IE 11 and above document.msFullscreenElement is null if not in fullscreen
                // otherwise undefined (in other browsers)
                this.set('fullScreenToggleTitle', this.get('app').lang.labels.exitFullScreen);

                if (document.msFullscreenElement === null) {
                    this.utils.fullScreenHelper.requestFullScreen(document.body);
                } else {
                    this.utils.fullScreenHelper.requestFullScreen(document.documentElement);
                }

                this.utils.analyticsService.trackEvent(this.get('gaKey'), this.utils.Constants.GAActions.maximize);
            }
        },

        changeSettings: function (selectedSetting) {
            this._changeSettings(selectedSetting);
        },

        changeLanguage: function (toLang) {
            LanguageDataStore.changeLanguage(toLang);
        },

        onWidgetClick: function () {
            Ember.appGlobal.activeWidget = 'titleBar';
        }
    }
});

Ember.Handlebars.helper('message-viewer', MessageViewer);
Ember.Handlebars.helper('settings-dropdown', SettingsDropdown);

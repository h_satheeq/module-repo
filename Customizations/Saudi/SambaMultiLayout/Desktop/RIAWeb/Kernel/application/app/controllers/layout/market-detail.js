import Ember from 'ember';
import sharedService from '../../models/shared/shared-service';
import appEvents from '../../app-events';
import PriceConstants from '../../models/price/price-constants';
import utils from '../../utils/utils';
import BaseArrayController from '../base-array-controller';

export default BaseArrayController.extend({
    priceService: sharedService.getService('price'),
    intZero: 0,

    topPanelSettings: {
        intZero: 0,
        emptyString: '',

        styles: {
            upColor: 'up-fore-color',
            downColor: 'down-fore-color',
            foreColor: 'fore-color'
        },

        timeInterval: {
            OneSecondInMillis: 1000,
            OneMinuteInMillis: 60000
        }
    },

    styles: {
        upColor: 'up-fore-color',
        downColor: 'down-fore-color',
        foreColor: 'fore-color',
        upArrow: 'glyphicon-triangle-top glyphicon ',
        downArrow: 'glyphicon-triangle-bottom glyphicon font-x-l ',
        backgroundUp: 'ms-top-bar-index-change-green',
        backgroundDown: 'ms-top-bar-index-change-red',
        backgroundZero: 'ms-top-bar-index-change-none',
        percentageUp: 'ms-top-bar-index-percentage-green',
        percentageDown: 'ms-top-bar-index-percentage-red',
        percentageZero: 'ms-top-bar-index-percentage-none'
    },

    onLoadWidget: function () {
        appEvents.subscribeSymbolChanged(this.get('wkey'), this, this.get('selectedLink'));
    },

    onPrepareData: function () {
        this.set('exchange', this.priceService.exchangeDS.getExchange(this.get('exg')));
        this.set('tasi', this.priceService.stockDS.getStock(this.get('exg'), 'TASI', 7));
        this.set('nomu', this.priceService.stockDS.getStock(this.get('exg'), 'NOMU', 7));

        this._updateProgressTime();
    },

    onAddSubscription: function () {
        this.priceService.addSymbolRequest(this.get('exg'), 'TASI', 7);
        this.priceService.addSymbolRequest(this.get('exg'), 'NOMU', 7);
    },

    _updateMarketTime: function () {
        if (this.get('exchange.stat') === PriceConstants.MarketStatus.Close || this.get('exchange.stat') === PriceConstants.MarketStatus.PreClose) {
            var dateTimeStr = this.utils.formatters.formatToDate(this.exchange.date, this.get('exchange.exg'));
            this.set('isEnabledTimer', false);
            this.set('marketTime', dateTimeStr);
        } else {
            if (!this.get('isEnabledTimer')) {
                this.set('isEnabledTimer', true);
                this._updateClock();
            }
        }
    }.observes('exchange.date', 'exchange.stat'),

    _updateIndexRelatedCss: function () {
        var indexArrowCss, indexCss, indexValCss, indexChangeSign, indexBackgroundCss, indexPercentageCss;
        var pctChg = this.get('tasi.pctChg');

        if (pctChg > this.intZero) {
            indexCss = this.styles.upColor;
            indexValCss = this.styles.upColor;
            indexArrowCss = this.styles.upArrow;
            indexBackgroundCss = this.styles.backgroundUp;
            indexPercentageCss = this.styles.percentageUp;
        } else if (pctChg < this.intZero) {
            indexCss = this.styles.downColor;
            indexValCss = this.styles.downColor;
            indexArrowCss = this.styles.downArrow;
            indexBackgroundCss = this.styles.backgroundDown;
            indexPercentageCss = this.styles.percentageDown;
        } else {
            indexCss = this.styles.foreColor;
            indexValCss = this.styles.foreColor;
            indexArrowCss = this.emptyString;
            indexBackgroundCss = this.styles.backgroundZero;
            indexPercentageCss = this.styles.percentageZero;
        }

        this.set('indexArrowCss', indexArrowCss);
        this.set('indexCss', indexCss);
        this.set('indexValCss', indexValCss);
        this.set('indexBackgroundCss', indexBackgroundCss);
        this.set('indexPercentageCss', indexPercentageCss);
    }.observes('tasi.pctChg'),

    _updatenomuIndexRelatedCss: function () {
        var indexArrowCss, indexCss, indexValCss, indexChangeSign, indexBackgroundCss, indexPercentageCss;
        var pctChg = this.get('nomu.pctChg');

        if (pctChg > this.intZero) {
            indexCss = this.styles.upColor;
            indexValCss = this.styles.upColor;
            indexArrowCss = this.styles.upArrow;
            indexBackgroundCss = this.styles.backgroundUp;
            indexPercentageCss = this.styles.percentageUp;
        } else if (pctChg < this.intZero) {
            indexCss = this.styles.downColor;
            indexValCss = this.styles.downColor;
            indexArrowCss = this.styles.downArrow;
            indexBackgroundCss = this.styles.backgroundDown;
            indexPercentageCss = this.styles.percentageDown;
        } else {
            indexCss = this.styles.foreColor;
            indexValCss = this.styles.foreColor;
            indexArrowCss = this.emptyString;
            indexBackgroundCss = this.styles.backgroundZero;
            indexPercentageCss = this.styles.percentageZero;
        }

        this.set('indexArrowCssNomu', indexArrowCss);
        this.set('indexCssNomu', indexCss);
        this.set('indexValCssNomu', indexValCss);
        this.set('indexBackgroundCssNomu', indexBackgroundCss);
        this.set('indexPercentageCssNomu', indexPercentageCss);
    }.observes('nomu.pctChg'),

    _updateClock: function () {
        var that = this;
        var sleepTime = this.topPanelSettings.timeInterval.OneSecondInMillis;

        Ember.run.once(this, this._updateUITime);

        setTimeout(function () {
            if (that.get('isEnabledTimer')) {
                that._updateClock();
            }
        }, sleepTime);
    },

    _updateUITime: function () {
        var time = this.get('mktTime');
        var dateTimeStr;

        if (this.utils.validators.isAvailable(time)) {
            time.setSeconds(time.getSeconds() + 1);
            this.set('mktTime', time);
            dateTimeStr = this.utils.formatters.convertToDisplayTimeFormat(time);
            this.set('marketTime', dateTimeStr);
        }
    },

    _setMarketTime: function () {
        var marketTime = this.utils.formatters.getAdjustedDateTime(this.exchange.time, this.get('exchange.tzo'));
        this.set('mktTime', marketTime);
    }.observes('exchange.time'),

    _calculateIndex: function (hour, min) {  // Args are int
        var openHour = this.get('openTimeHour'); // Market open hours
        var openMin = this.get('openTimeMin'); // Market open minutes
        var hourIndex = hour - openHour;
        var minIndex = min - openMin;

        if (hourIndex >= 0) {
            return hourIndex * 3600 + minIndex * 60;
        } else {
            return 0;
        }
    },

    _setInitialTimeIndices: function () {
        var exg = this.get('exchange');

        if (exg) {
            var openTime = exg.openTime;
            var closeTime = exg.closeTime;

            this.set('openExgTime', openTime);
            this.set('closeExgTime', closeTime);

            if (openTime && closeTime) {
                this.set('openTimeHour', parseInt(openTime.substr(0, 2), 10));
                this.set('openTimeMin', parseInt(openTime.substr(2, 2), 10));
                this.set('closeTimeIndex', this._calculateIndex(parseInt(closeTime.substr(0, 2), 10), parseInt(closeTime.substr(2, 2), 10)));

                var criticalMin = this.get('criticalTimeMin');
                this.set('criticalTimeIndex', this.get('closeTimeIndex') - criticalMin * 60);
            }
        }
    }.observes('exchange.tzo'),

    _updateProgressTime: (function () {
        var exg = this.get('exchange');

        if (exg && exg.time) {
            var currentTime = exg.time;
            var currentIndex = this._calculateIndex(parseInt(currentTime.substr(0, 2), 10) + exg.tzo, parseInt(currentTime.substr(2, 2), 10)) + parseInt(currentTime.substr(4, 2), 10);

            this.set('currentTimeIndex', currentIndex);

            if (exg.stat === PriceConstants.MarketStatus.Open && currentIndex >= 0) {
                this.set('isMarketOpen', true);
            } else if (exg.stat !== PriceConstants.MarketStatus.Open || currentIndex >= this.get('closeTimeIndex')) {
                this.set('isMarketOpen', false);
            }

            if (currentIndex >= this.get('criticalTimeIndex') && this.get('closeTimeIndex') >= currentIndex) {
                this.set('isCriticalTime', true);
            } else {
                this.set('isCriticalTime', false);
            }

            if (this.get('isMarketOpen')) {

                if (this.get('isCriticalTime')) {
                    var timeStamp = (this.get('closeTimeIndex') - currentIndex);
                    var timeStampInt = parseInt(timeStamp / 60, 10);
                    var space = utils.Constants.StringConst.Space;

                    if (!isNaN(timeStampInt)) {
                        this.set('showTime', true);

                        if (timeStampInt >= 2) {
                            this.set('showTimeText', [timeStampInt, this.get('app').lang.labels.minsLeft].join(space));
                        } else if (timeStampInt === 1 && timeStamp >= 60) {
                            this.set('showTimeText', [timeStampInt, this.get('app').lang.labels.minLeft].join(space));
                        } else {
                            this.set('showTimeText', [timeStampInt, this.get('app').lang.labels.secLeft].join(space));
                        }
                    }
                } else {
                    this.set('showTime', false);
                }
            } else {
                this.set('showTime', false);
            }
        }
    }).observes('exchange.time')
});


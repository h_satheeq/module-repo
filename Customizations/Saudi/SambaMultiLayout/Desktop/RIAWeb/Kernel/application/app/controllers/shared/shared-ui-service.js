import Ember from 'ember';
import appConfig from '../../config/app-config';
import ControllerFactory from '../controller-factory';
import appEvents from '../../app-events';
import webConnection from '../../models/shared/communication-adapters/web-http-connection';
import layoutHandler from '../layout/layout-handler';
import sharedService from '../../models/shared/shared-service';
import utils from '../../utils/utils';

export default Ember.Object.extend({
    serviceMap: {},
    subscriptionKey: 'sharedUI',
    container: null,
    layoutMap: {},
    loginViewController: {},

    init: function () {
        this._super();
        appEvents.subscribeDomReady(this.subscriptionKey, this);
    },

    onDomReady: function () {
        Ember.run.next(this, function () { // It is required in mobile to execute this in next loop
            webConnection.setRequestPermission(true);
            webConnection.sendPendingRequest();
        });
    },

    registerService: function (type, service) {
        this.serviceMap[type] = service;
    },

    getService: function (type) {
        return this.serviceMap[type];
    },

    invokeChangePassword: function (uiContainer) {
        if (uiContainer) {
            var changePasswordPopup = this.getService('changePasswordPopup');
            changePasswordPopup.send('showModalPopup', false);
        }
    },

    invokeRightClick: function (stock, wkey, event, menuComponent) {
        menuComponent.initialize(wkey, stock);

        if (event && event.button === 2 && !appConfig.customisation.isMobile) {
            var viewName = 'components/symbol-click-menu-popup';
            var modal = this.getService('modalPopupId');

            menuComponent.showPopup(menuComponent, viewName, modal); // Todo [Anushka] view popup to right click
        }
    },

    renderFullScreenWidget: function (controllerString, routeString, popUpName, callbackFn) {
        var widgetController = this.getService(popUpName);

        if (!widgetController) {
            widgetController = ControllerFactory.createController(this.container, controllerString);
            this.registerService(popUpName, widgetController);
        }

        widgetController.set('loginViewCallbackFn', callbackFn);

        var modal = this.getService('modalPopupId');

        if (modal) {
            modal.send('enableOverlay');
            modal.send('showModalPopup');
            modal.set('modalPopupStyle', 'full-height full-width');
        }

        var route = this.container.lookup('route:application');

        route.render(routeString, {
            into: 'application',
            outlet: 'modalPopupContent',
            controller: widgetController
        });

        widgetController.initializeWidget({wn: controllerString});
    },

    onLayoutReady: function (appLayout) {
        this.container = appLayout.container;
    },

    loadLayout: function (layoutId) {
        var that = this;
        var route = this.container.lookup('route:application');

        // To clear last loaded menu/tabs/innerWidgets
        Ember.appGlobal.queryParams.appParams[utils.Constants.EmbeddedModeParams.Page] = undefined;
        sharedService.userState.lastMenu = undefined;
        sharedService.userState.lastInnerWidget = undefined;

        appEvents.onAppClose();

        Ember.$.each(route.connections, function (key, conn) {
            route.disconnectOutlet({
                parentView: conn.outlet,
                outlet: conn.into
            });
        });

        Ember.set(route.get('controller'), 'isAppRenderingEnabled', false);
        Ember.appGlobal.tableCache = [];

        Ember.run.later(this, function () {
            Ember.set(route.get('controller'), 'isAppRenderingEnabled', true);

            var layoutConfig = layoutHandler.getLayoutConfig('u1', layoutId);
            that.registerService('appLayoutConfig', layoutConfig);

            layoutHandler.renderTemplate(layoutConfig, route);
        }, 100);
    },

    setLayoutMap: function (layoutMap) {
        this.layoutMap = layoutMap;
    },

    getTitleBar: function () {
        return this.getService('titleBar');
    },

    navigateMenu: function (menuTitle, tabTitle, isAddedToLastMenuStack) {
        var menuContent = this.layoutMap[menuTitle];

        if (menuContent) {
            var tabId = tabTitle ? menuContent.tabMap[tabTitle].id : undefined;
            this.getService('mainPanel').onRenderMenuItems(menuContent, undefined, tabId, !isAddedToLastMenuStack); // Enable Navigate Back option when navigating with this function
        }
    },

    refreshPanelWidgets: function (args) {
        var that = this;
        var panelContainers = ['mainPanel', 'rightPanel', 'topPanel'];

        Ember.$.each(panelContainers, function (id, container) {
            var widgetContainer = that.getService(container);

            if (widgetContainer && Ember.$.isFunction(widgetContainer.refreshContainerWidgets)) {
                widgetContainer.refreshContainerWidgets(args);
            }
        });
    },

    showSessionTimeout: function (reason, isHideControls) {
        // Assuming there is no login back after session timeout and application should refresh after this state.
        Ember.$('div#ember-app-root').css('display', 'none');

        this.loginViewController.showAuthFailMessage(reason, isHideControls);
    }
});

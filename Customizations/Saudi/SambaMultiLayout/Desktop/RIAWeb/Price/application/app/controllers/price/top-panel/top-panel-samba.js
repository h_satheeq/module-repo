/* global Mousetrap */

import Ember from 'ember';
import sharedService from '../../../models/shared/shared-service';
import PriceConstants from '../../../models/price/price-constants';
import BaseController from '../../base-controller';
import mainIndexChart from './main-index-chart';
import priceTicker from './price-ticker';
import cashMaps from './cash-maps';
import responsiveHandler from '../../../helpers/responsive-handler';
import appEvents from '../../../app-events';
import appConfig from '../../../config/app-config';
import utils from '../../../utils/utils';

/* *
 * Controller class to handle all top panel
 * This widget contains exchange snapshot
 */
export default BaseController.extend({
    // Ember objects references
    exchange: undefined, // User Default exchange reference
    marketTime: '', // Formatted market time
    mktTime: '',
    isEnabledTimer: false, // Status of timer for market time
    exgSymCollection: undefined, // Exchange Symbol Collection
    prevUpdatedBar: 0,
    exchanges: [],
    selectedExg: {},
    disableMoreMarkets: true,
    subscriptionKey: 'topPanel',
    priceService: sharedService.getService('price'),

    // Updating below properties according to some values of 'exchange' or 'index'
    marketStatusCSS: '', // Market status colour. ex : Green Market Open , red Market close
    ytdCss: 'up-fore-color',
    isRender: false,

    topPanelSettings: {
        intZero: 0,
        emptyString: '',

        styles: {
            upColor: 'up-fore-color',
            downColor: 'down-fore-color',
            foreColor: 'fore-color'
        },

        timeInterval: {
            OneSecondInMillis: 1000,
            OneMinuteInMillis: 60000
        }
    },

    // Market Open, Close remaining time bar and countdown data
    openExgTime: '0000',
    closeExgTime: '0000',

    openTimeIndex: 0,
    closeTimeIndex: 0,
    currentTimeIndex: 0,
    criticalTimeIndex: 0,

    criticalTimeMin: 25,
    isCriticalTime: false,
    isMarketOpen: false,

    openTimeHour: 0,
    openTimeMins: 0,

    bar: '',
    progressStyle: '',
    showTime: false,
    intZero: 0,
    emptyString: '',
    signs: {plus: '+', minus: '-', equal: '='},
    showTimeText: '',

    styles: {
        upColor: 'up-fore-color',
        downColor: 'down-fore-color',
        foreColor: 'fore-color',
        upArrow: 'glyphicon-triangle-top glyphicon ',
        downArrow: 'glyphicon-triangle-bottom glyphicon font-x-l ',
        backgroundUp: 'ms-top-bar-index-change-green',
        backgroundDown: 'ms-top-bar-index-change-red',
        backgroundZero: 'ms-top-bar-index-change-none',
        percentageUp: 'ms-top-bar-index-percentage-green',
        percentageDown: 'ms-top-bar-index-percentage-red',
        percentageZero: 'ms-top-bar-index-percentage-none'
    },

    /* *
     * Calculate up,down,unchanged progress bar percentage
     * Observes : exchange.ups,exchange.dwns,exchange.nChg
     */
    upsDownsNoChgValueArray: function () {
        var exg = this.get('exchange');

        if (exg) {
            var ups = exg.ups;
            var down = exg.dwns;
            var unchanged = exg.nChg;
            var maxValue = Math.max(ups, down, unchanged);

            var upsDownsNoChgValueArray = [
                {value: ups, barClass: 'progress-bar up-back-color', barWidth: ''},
                {value: down, barClass: 'progress-bar down-back-color', barWidth: ''},
                {value: unchanged, barClass: 'progress-bar no-changed-back-color', barWidth: ''}
            ];

            Ember.$.each(upsDownsNoChgValueArray, function (index, item) {
                var percentage;

                if (isNaN(item.value) || item.value === 0 || maxValue === 0) {
                    percentage = 0;
                } else {
                    percentage = Math.round((item.value / maxValue) * 90) + 10;
                }

                item.barWidth = 'width:' + percentage + '%;';
            });

            return upsDownsNoChgValueArray;
        }
    }.property('exchange.ups', 'exchange.dwns', 'exchange.nChg'),

    isTablet: function () {
        return appConfig.customisation.isTablet;
    }.property(),

    onLoadWidget: function () {
        this.set('exg', this.get('exg') ? this.get('exg') : sharedService.userSettings.currentExchange);
        this.set('idx', this.get('idx') ? this.get('idx') : sharedService.userSettings.currentIndex);
    },

    initializeResponsive: function () {
        this.set('responsive', responsiveHandler.create({controller: this, widgetId: 'topPanel-' + this.get('wkey'), callback: this.onResponsive}));

        this.responsive.addList('topPanel-left', [
            {id: 'topPanel-cashmaps', width: 5, responsiveMarginRatio: 4},
            {id: 'topPanel-ytd', width: 20, responsiveMarginRatio: 4},
            {id: 'topPanel-chartIcon', width: 20, responsiveMarginRatio: 4},
            {id: 'topPanel-volume', width: 20, responsiveMarginRatio: 4},
            {id: 'topPanel-turnover', width: 20, responsiveMarginRatio: 4},
            {id: 'topPanel-symbol', width: 20, responsiveMarginRatio: 4}
        ]);

        this.responsive.initialize();
    },

    onResponsive: function (responsiveArgs) {
        var controller = responsiveArgs.controller;

        if (responsiveArgs.responsiveLevel === 0) {
            Ember.run.later(function () {
                Ember.set(controller, 'cashMapStyle', 'position: relative; top: 0; left: 0');
            }, 1);
        } else {
            Ember.set(controller, 'cashMapStyle', 'position: absolute; top: 25000px; left: 25000px');
        }
    },

    onPrepareData: function () {
        this.set('exchange', this.priceService.exchangeDS.getExchange(this.get('exg')));
        this.set('exgSymCollection', this.priceService.stockDS.getSymbolCollectionByExchange(this.get('exg')));
        this.set('idx', this.utils.validators.isAvailable(this.get('idx')) ? this.get('idx') : this.get('exchange.mainIdx'));
        this.set('index', this.priceService.stockDS.getStock(this.get('exg'), this.get('idx'), this.utils.AssetTypes.Indices));
        this.set('currentIndex', this.priceService.stockDS.getStock(this.get('exg'), sharedService.userSettings.currentIndex, utils.AssetTypes.Indices));

        // Open, Close difference calculate
        this._setInitialTimeIndices();

        // Keyboard search
        Mousetrap.bind('ctrl+f', function () {
            Ember.$('#appGlobalSymbolSearch').focus();
            return false;
        }, 'global');

        this._callComponentsMethod('onPrepareData');
        this.priceService.subscribeAuthSuccess(this, this.get('subscriptionKey'));
    },

    _updateIndexRelatedCss: function () {
        var indexArrowCss, indexCss, indexValCss, indexChangeSign, indexBackgroundCss, indexPercentageCss;
        var pctChg = this.get('currentIndex.pctChg');

        if (pctChg > this.intZero) {
            indexCss = this.styles.upColor;
            indexValCss = this.styles.upColor;
            indexArrowCss = this.styles.upArrow;
            indexChangeSign = this.signs.plus;
            indexBackgroundCss = this.styles.backgroundUp;
            indexPercentageCss = this.styles.percentageUp;
        } else if (pctChg < this.intZero) {
            indexCss = this.styles.downColor;
            indexValCss = this.styles.downColor;
            indexChangeSign = '';
            indexArrowCss = this.styles.downArrow;
            indexBackgroundCss = this.styles.backgroundDown;
            indexPercentageCss = this.styles.percentageDown;
        } else {
            indexCss = this.styles.foreColor;
            indexValCss = this.styles.foreColor;
            indexChangeSign = '';
            indexArrowCss = this.emptyString;
            indexBackgroundCss = this.styles.backgroundZero;
            indexPercentageCss = this.styles.percentageZero;
        }

        this.set('indexArrowCss', indexArrowCss);
        this.set('indexChangeSign', indexChangeSign);
        this.set('indexCss', indexCss);
        this.set('indexValCss', indexValCss);
        this.set('indexBackgroundCss', indexBackgroundCss);
        this.set('indexPercentageCss', indexPercentageCss);
    }.observes('currentIndex.pctChg'),

    onAddSubscription: function () {
        this.priceService.addExchangeRequest(this.get('exg'));
        this.priceService.addIndexRequest(this.get('exg'), this.get('idx'));

        this._callComponentsMethod('onAddSubscription');
    },

    onAfterRender: function () {
        Ember.run.later(this, this.renderComponents, 1000);
    },

    renderComponents: function () {
        this.set('isRender', true);
    },

    onClearData: function () {
        this.set('exchange', {});
        this.set('index', {});
        this.set('idx', '');

        this._callComponentsMethod('onClearData');
    },

    onRemoveSubscription: function () {
        this.priceService.removeExchangeRequest(this.get('exg'));
        this.priceService.removeIndexRequest(this.get('exg'), this.get('idx'));

        this._callComponentsMethod('onRemoveSubscription');
    },

    onLanguageChanged: function () {
        this._callComponentsMethod('onLanguageChanged');
        Ember.run.later(this, this._setExchangeDesc, 100);
    },

    onThemeChanged: function () {
        this._callComponentsMethod('onThemeChanged');
    },

    onVisibilityChanged: function () {
        this._callComponentsMethod('onVisibilityChanged');
    },

    onAuthSuccess: function () {
        var that = this;
        var exchangeCodes = this.priceService.userDS.get('userExchg');

        this.set('userExgArray', Ember.A());

        Ember.$.each(exchangeCodes, function (key, item) {
            var exgObj = that.priceService.exchangeDS.getExchange(item);
            that.get('userExgArray').pushObject(exgObj);
        });
    },

    _callComponentsMethod: function (method) {
        var mainIndexChartComponent = Ember.View.views['price-top-panel-main-index-chart'];
        var priceTickerComponent = Ember.View.views['price-top-panel-price-ticker'];
        var cashMapsComponent = Ember.View.views['price-top-panel-cash-maps'];

        try {
            if (mainIndexChartComponent) {
                mainIndexChartComponent.send(method, this.get('exg'));
            }
        } catch (e) {
            this.utils.logger.logError(e);
        }

        try {
            if (priceTickerComponent) {
                priceTickerComponent.send(method, this.get('exg'));
            }
        } catch (e) {
            this.utils.logger.logError(e);
        }

        try {
            if (cashMapsComponent) {
                cashMapsComponent.send(method);
            }
        } catch (e) {
            this.utils.logger.logError(e);
        }
    },

    /* *
     * Returns no of symbols of selected exchange
     */
    _exgSymbolCount: function () {
        return this.get('exgSymCollection').length;
    }.property('exgSymCollection.@each'),

    _symbolsTradedPer: function () {
        var symbolTraded = this.get('exchange.symt');
        var noOfSymbols = this.get('_exgSymbolCount');
        return noOfSymbols ? ((symbolTraded / noOfSymbols) * 100) : 0;
    }.property('exchange.symt', '_exgSymbolCount'),

    /* *
     * Update Market time of top panel
     * Observing exchange status and date and change ui accordingly
     * Observes : exchange.status and exchange.date
     */
    _updateMarketTime: function () {
        if (this.get('exchange.stat') === PriceConstants.MarketStatus.Close || this.get('exchange.stat') === PriceConstants.MarketStatus.PreClose) {
            var dateTimeStr = this.utils.formatters.formatToDate(this.exchange.date, this.get('exchange.exg'));
            this.set('isEnabledTimer', false);
            this.set('marketTime', dateTimeStr);
        } else {
            if (!this.get('isEnabledTimer')) {
                this.set('isEnabledTimer', true);
                this._updateClock();
            }
        }
    }.observes('exchange.date', 'exchange.stat'),

    _setMarketTime: function () {
        var marketTime = this.utils.formatters.getAdjustedDateTime(this.exchange.time, this.get('exchange.tzo'));
        this.set('mktTime', marketTime);
    }.observes('exchange.time'),

    _updateClock: function () {
        var that = this;
        var sleepTime = this.topPanelSettings.timeInterval.OneSecondInMillis;

        Ember.run.once(this, this._updateUITime);

        setTimeout(function () {
            if (that.get('isEnabledTimer')) {
                that._updateClock();
            }
        }, sleepTime);
    },

    _updateUITime: function () {
        var time = this.get('mktTime');
        var dateTimeStr;

        if (this.utils.validators.isAvailable(time)) {
            time.setSeconds(time.getSeconds() + 1);
            this.set('mktTime', time);
            dateTimeStr = this.utils.formatters.convertToDisplayTimeFormat(time);
            this.set('marketTime', dateTimeStr);
        }
    },

    /* *
     * Change market status colour according to market status
     * Observes : exchange.stat
     */
    _updateMarketStatusColor: function () {
        var marketStatusCSS;
        var stat = this.get('exchange.stat');
        var marketStatusElement = Ember.$('#marketSummaryExchangeStatusContainer');

        if (stat === PriceConstants.MarketStatus.Close || stat === PriceConstants.MarketStatus.PreClose) {
            marketStatusCSS = this.topPanelSettings.styles.downColor;
            marketStatusElement.addClass('ms-top-bar-exchange-status-alert');
        } else {
            marketStatusCSS = this.topPanelSettings.styles.upColor;
            marketStatusElement.addClass('ms-top-bar-exchange-status-alert');
        }

        setTimeout(function () {
            marketStatusElement.removeClass('ms-top-bar-exchange-status-alert');
        }, 1000);

        this.set('marketStatusCSS', marketStatusCSS);
    }.observes('exchange.stat'),

    /* *
     * Change YTD percentage change css according to ytd value
     * Observes : exchange.pctYtd
     */
    _updateYTDColor: function () {
        var ytdCss;
        var pctYtd = this.get('index.pctYtd');

        if (pctYtd >= this.topPanelSettings.intZero) {
            ytdCss = this.topPanelSettings.styles.upColor;
        } else if (pctYtd < this.topPanelSettings.intZero) {
            ytdCss = this.topPanelSettings.styles.downColor;
        } else {
            ytdCss = this.topPanelSettings.styles.foreColor;
        }

        this.set('ytdCss', ytdCss);
    }.observes('index.pctYtd'),

    /* * Start of progress bar - time
     * Calculate market open, close bar percentage
     * @param hour market time hours (GMT hours + time zone offset) int
     * @param min market time minutes (GMT minutes) int
     */
    _calculateIndex: function (hour, min) {  // Args are int
        var openHour = this.get('openTimeHour'); // Market open hours
        var openMin = this.get('openTimeMin'); // Market open minutes
        var hourIndex = hour - openHour;
        var minIndex = min - openMin;

        if (hourIndex >= 0) {
            return hourIndex * 3600 + minIndex * 60;
        } else {
            return 0;
        }
    },

    _setInitialTimeIndices: function () {
        var exg = this.get('exchange');

        if (exg) {
            var openTime = exg.openTime;
            var closeTime = exg.closeTime;

            this.set('openExgTime', openTime);
            this.set('closeExgTime', closeTime);

            if (openTime && closeTime) {
                this.set('openTimeHour', parseInt(openTime.substr(0, 2), 10));
                this.set('openTimeMin', parseInt(openTime.substr(2, 2), 10));
                this.set('closeTimeIndex', this._calculateIndex(parseInt(closeTime.substr(0, 2), 10), parseInt(closeTime.substr(2, 2), 10)));

                var criticalMin = this.get('criticalTimeMin');
                this.set('criticalTimeIndex', this.get('closeTimeIndex') - criticalMin * 60);
            }
        }
    }.observes('exchange.tzo'),

    _updateProgressTime: (function () {
        var exg = this.get('exchange');

        if (exg && exg.time) {
            var currentTime = exg.time;
            var currentIndex = this._calculateIndex(parseInt(currentTime.substr(0, 2), 10) + exg.tzo, parseInt(currentTime.substr(2, 2), 10)) + parseInt(currentTime.substr(4, 2), 10);

            this.set('currentTimeIndex', currentIndex);

            if (exg.stat === PriceConstants.MarketStatus.Open && currentIndex >= 0) {
                this.set('isMarketOpen', true);
            } else if (exg.stat !== PriceConstants.MarketStatus.Open || currentIndex >= this.get('closeTimeIndex')) {
                this.set('isMarketOpen', false);
            }

            if (currentIndex >= this.get('criticalTimeIndex') && this.get('closeTimeIndex') >= currentIndex) {
                this.set('isCriticalTime', true);
            } else {
                this.set('isCriticalTime', false);
            }

            if (this.get('isMarketOpen')) {
                this._showProgressTimeBar();

                if (this.get('isCriticalTime')) {
                    var timeStamp = (this.get('closeTimeIndex') - currentIndex);
                    var timeStampInt = parseInt(timeStamp / 60, 10);
                    var space = utils.Constants.StringConst.Space;

                    if (!isNaN(timeStampInt)) {
                        this.set('showTime', true);

                        if (timeStampInt >= 2) {
                            this.set('showTimeText', [timeStampInt, this.get('app').lang.labels.minsLeft].join(space));
                        } else if (timeStampInt === 1 && timeStamp >= 60) {
                            this.set('showTimeText', [timeStampInt, this.get('app').lang.labels.minLeft].join(space));
                        } else {
                            this.set('showTimeText', [timeStampInt, this.get('app').lang.labels.secLeft].join(space));
                        }
                    }
                } else {
                    this.set('showTime', false);
                }
            } else {
                this.set('showTime', false);
            }
        }
    }).observes('exchange.time'),

    _showProgressTimeBar: function () {
        var bar;
        var style;
        var timeDiffInSecs = this.get('currentTimeIndex');
        var fullTimeDiffInSecs = this.get('closeTimeIndex');
        var prevUpdatedBar = this.get('prevUpdatedBar');

        if (fullTimeDiffInSecs) {
            var differenceInPercentage = (timeDiffInSecs / fullTimeDiffInSecs) * 100;
            differenceInPercentage = differenceInPercentage > 0 && differenceInPercentage <= 100 ? differenceInPercentage : 0;

            if (differenceInPercentage === 0 || Math.abs(differenceInPercentage - prevUpdatedBar) > 1) {
                this.set('prevUpdatedBar', differenceInPercentage);
                bar = 'width:' + differenceInPercentage + '%;';

                if (this.get('isCriticalTime')) {
                    style = 'progress-bar mkt-close-indicator';
                } else {
                    style = 'progress-bar';
                }

                this.set('bar', bar);
                this.set('progressStyle', style);
            }
        }
    },

    _setExchangeDesc: function () {
        var that = this;
        var exchangeArray = [];
        var userExgs = this.get('userExgArray');

        if (userExgs) {
            Ember.$.each(userExgs, function (index, item) {
                exchangeArray[index] = {code: item.exg, desc: item.des, de: that.get('isTablet') ? item.des : item.de};

                if (item.exg === sharedService.userSettings.currentExchange) {
                    that.set('selectedExg', exchangeArray[index]);
                }
            });
        }

        this.set('exchanges', exchangeArray);
        this.set('disableMoreMarkets', exchangeArray.length <= 1);
    },

    _observeUserExchanges: function () {
        Ember.run.once(this, this._setExchangeDesc);
    }.observes('userExgArray.@each', 'userExgArray.@each.de'),

    actions: {
        setExchange: function (exchg) {
            var that = this;
            this.priceService.exchangeDS.getExchangeMetadata(exchg.code);

            Ember.$.each(this.get('exchanges'), function (key, val) {
                if (val.code === exchg.code) {
                    that.set('selectedExg', val);
                }
            });

            this.refreshWidget({exg: exchg.code});
            appEvents.onExchangeChanged(-1, exchg.code);
        }
    }
});

Ember.Handlebars.helper('main-index-chart', mainIndexChart);
Ember.Handlebars.helper('price-ticker', priceTicker);
Ember.Handlebars.helper('cash-maps', cashMaps);
export default {
    appConfig: {
        customisation: {
            clientPrefix: 'alawwal',

            supportedThemes: [
                {code: 'theme1', desc: 'Classic Dark', category: 'Dark', langKey: 'dark'},
                {code: 'theme2', desc: 'Alawwal Green', category: 'Green'},
                {code: 'theme3', desc: 'Alawwal Pink', category: 'Pink'},
                {code: 'theme4', desc: 'Alawwal Dark', category: 'Dark'}
            ],

            hidePreLogin: true
        },

        responseConfig: {
            150: {
                UE: 'TDWL,0,1|ADSM,0,1|DFM,0,1|DSM,0,1|OPRA,0,0|NSDQ,0,0|NYSE,0,0|AMEX,0,0'
            }
        }
    },

    tradeSettings: {
        channelId: 30,

        connectionParameters: {
            primary: {
                ip: 'wstrs-qa.directfn.net/trs',
                port: '',
                secure: true
            },

            secondary: {
                ip: 'wstrs-qa.directfn.net/trs',
                port: '',
                secure: true
            }
        }
    }
};

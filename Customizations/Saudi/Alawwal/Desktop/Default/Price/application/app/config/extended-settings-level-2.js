export default {
    appConfig: {
        customisation: {
            clientPrefix: 'alawwal',

            supportedLanguages: [
                {code: 'EN', desc: 'English'},
                {code: 'AR', desc: 'العربية'}
            ],

            supportedThemes: [
                {code: 'theme1', desc: 'Classic Dark', category: 'Dark', langKey: 'dark'},
                {code: 'theme2', desc: 'Alawwal Green', category: 'Green'},
                {code: 'theme3', desc: 'Alawwal Pink', category: 'Pink'},
                {code: 'theme4', desc: 'Alawwal Dark', category: 'Dark'}
            ],

            hidePreLogin: true
        }
    }
};

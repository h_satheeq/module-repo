/* jshint node: true */
var EnvironmentExtended = require('./environment-extended.js');

module.exports = function (environment) {
    var ENV = new EnvironmentExtended(environment);  
	ENV.contentSecurityPolicy['connect-src'] = "'self' www.google-analytics.com wss://data-sa9.mubasher.net/html5ws wss://wstrs-qa.directfn.net/trs";
	
	if (ENV.APP.isTestMode) {
        ENV.APP.tradeConnectionParameters = {
            primary: {
                ip: 'wstrs-qa.directfn.net/trs',
                port: '',
                secure: true
            }
        };
    }
	
    return ENV;
};

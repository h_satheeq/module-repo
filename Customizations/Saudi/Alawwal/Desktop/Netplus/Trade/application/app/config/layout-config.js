export default (function () {
    var layout = {
        menuPanel: {
            template: 'layout.left-navigation'
        },

        titleBar: {
            template: 'layout.title-bar'
        },

        topPanel: {
            template: 'top-panel-container-controller',
            content: [
                {
                    id: 9,
                    title: 'priceTopPanel',
                    def: true,
                    tab: [
                        {
                            id: 1,
                            title: 'priceTopPanel',
                            def: true,
                            outlet: 'price.containers.top-panel-tab',
                            w: [{id: 1, wn: 'price.top-panel.top-panel'}]
                        }]
                }
            ]
        },

        mainPanel: {
            template: 'main-panel-container-controller',
            content: [
                {
                    id: 1,
                    title: 'market',
                    icon: 'glyphicon icon-analytics-chart-graph',
                    def: true,
                    rightPanel: 1,
                    tab: [
                        {
                            id: 1,
                            cache: false,
                            title: 'market',
                            def: true,
                            outlet: 'price.containers.market-overview-tab',
                            w: [
                                {
                                    id: 1,
                                    iw: [{id: 1, wn: 'price.widgets.sector-overview', desc: 'sectorOverview', def: true}]
                                },
                                {
                                    id: 2,
                                    iw: [{id: 1, wn: 'price.widgets.top-stocks', desc: 'topStocks', def: true}]
                                },
                                {
                                    id: 3,
                                    iw: [{id: 1, wn: 'price.widgets.commodities-overview', desc: 'gms', def: true}]
                                },
                                {
                                    id: 4,
                                    iw: [{id: 1, wn: 'price.widgets.watch-list.watch-list', desc: 'watchList', def: true}]
                                }
                            ]
                        }]
                },
                {
                    id: 8,
                    title: 'trading',
                    widgetTitle: 'trading',
                    icon: 'glyphicon icon-trade',
                    def: false,
                    isShowTitle: false,
                    rightPanel: -1,
                    tab: [
                        {
                            id: 1,
                            title: 'trading',
                            def: true,
                            outlet: 'trade.containers.trade-order-tab',
                            w: [
                                {
                                    id: 1,
                                    iw: [{id: 1, wn: 'trade.widgets.order-ticket.order-ticket-landscape', desc: 'orderTicket', def: true}]
                                },
                                {
                                    id: 2,
                                    iw: [{id: 1, wn: 'price.widgets.watch-list.watch-list', desc: 'watchList', def: true}]
                                },
                                {
                                    id: 3,
                                    iw: [
                                        {id: 1, wn: 'trade.widgets.order-list', desc: 'orderList', def: true, isShow: true},
                                        {id: 2, wn: 'trade.widgets.portfolio', desc: 'portfolio', def: false, isShow: true},
                                        {id: 3, wn: 'trade.widgets.account-summary', desc: 'accountSummary', def: false, isShow: true},
                                        {id: 4, wn: 'trade.widgets.order-search', desc: 'orderSearch', def: false, isShow: true},
                                        {id: 5, wn: 'trade.widgets.cash-statements', desc: 'cashStatements', def: false, isShow: true}
                                    ]
                                },
                                {
                                    id: 4,
                                    iw: [{id: 1, wn: 'view-container-controller', def: true}]
                                },
                                {
                                    id: 5,
                                    iw: [{id: 1, wn: 'chart.regular-chart', desc: 'chart', def: true}]
                                }
                            ]
                        }]
                },
                {
                    id: 2,
                    title: 'quote',
                    icon: 'glyphicon icon-list-ul',
                    def: false,
                    rightPanel: 4,
                    tab: [
                        {
                            id: 1,
                            cache: false,
                            title: 'fullQuote',
                            def: true,
                            outlet: 'price.containers.quote-tab',
                            w: [
                                {
                                    id: 1,
                                    iw: [{id: 1, wn: 'price.widgets.quote-summery', desc: 'quoteSummary', def: true}]
                                },
                                {
                                    id: 2,
                                    iw: [{id: 1, wn: 'chart.regular-chart', desc: 'chart', def: true}]
                                },
                                {
                                    id: 3,
                                    iw: [{id: 1, wn: 'price.widgets.quote-intraday-performance', desc: 'detailQuote', def: true}]
                                },
                                {
                                    id: 4,
                                    iw: [{id: 1, wn: 'price.widgets.time-and-sales.quote-time-and-sales', desc: 'timeAndSales', def: true}]
                                },
                                {
                                    id: 5,
                                    iw: [{id: 1, wn: 'price.widgets.quote-market-depth', desc: 'depthByPrice', def: true}]
                                },
								{
                                    id: 6,
                                    iw: [{id: 1, wn: 'price.widgets.quote-market-depth', desc: 'depthByOrder', def: true}]
                                },
                                {
                                    id: 7,
                                    iw: [{id: 1, wn: 'price.widgets.announcement.symbol-announcement', desc: 'newsAnn', def: true}]
                                }
                            ]
                        },
                        {
                            id: 4,
                            cache: false,
                            title: 'companyProf',
                            def: false,
                            outlet: 'price.containers.company-tab',
                            w: [
                                {
                                    id: 1,
                                    iw: [{id: 1, wn: 'price.widgets.quote-summery', desc: 'quoteSummary', def: true}]
                                },
                                {
                                    id: 2,
                                    iw: [{id: 1, wn: 'price.widgets.company-profile.company-basic-info', desc: 'companyInfor', def: true}]
                                },
                                {
                                    id: 3,
                                    iw: [{id: 1, wn: 'price.widgets.company-profile.company-management-info', desc: 'mngtNBrdMbrs', def: true}]
                                },
                                {
                                    id: 4,
                                    iw: [{id: 1, wn: 'price.widgets.company-profile.company-owners-info', desc: 'owners', def: true}]
                                },
                                {
                                    id: 5,
                                    iw: [{id: 1, wn: 'price.widgets.company-profile.company-subsidiaries-info', desc: 'subsidiaries', def: true}]
                                }
                            ]
                        },
                        {
                            id: 2,
                            cache: false,
                            title: 'optionChain',
                            def: false,
                            outlet: 'price.containers.option-chain-tab',
                            w: [
                                {
                                    id: 1,
                                    iw: [{id: 1, wn: 'price.widgets.option-chain', desc: 'optionChain', def: true}]
                                }
                            ]
                        }]
                },
                {
                    id: 3,
                    title: 'topStocks',
                    icon: 'glyphicon icon-thumbs-o-up',
                    def: false,
                    rightPanel: -1,
                    tab: [
                        {
                            id: 1,
                            title: 'topStocks',
                            def: true,
                            outlet: 'price.containers.top-stocks-tab',
                            w: [
                                {
                                    id: 1,
                                    iw: [{id: 1, wn: 'price.widgets.top-stock', desc: 'topGainers', def: true}]
                                },
                                {
                                    id: 2,
                                    iw: [{id: 1, wn: 'price.widgets.top-stock', desc: 'topLosers', def: true}]
                                },
                                {
                                    id: 3,
                                    iw: [{id: 1, wn: 'price.widgets.top-stock', desc: 'mostActive', def: true}]
                                },
                                {
                                    id: 4,
                                    iw: [{id: 1, wn: 'price.widgets.top-stock', desc: 'mostActive', def: true}]
                                },
                                {
                                    id: 5,
                                    iw: [{id: 1, wn: 'price.widgets.top-stock', desc: 'mostActive', def: true}]
                                }
                            ]
                        }]
                },
                {
                    id: 7,
                    title: 'heatMap',
                    icon: 'glyphicon icon-sitemap',
                    widgetTitle: 'perHeatMap',
                    def: false,
                    isShowTitle: true,
                    rightPanel: -1,
                    tab: [
                        {
                            id: 1,
                            title: 'heatMap',
                            def: true,
                            outlet: 'price.containers.heatmap-tab',
                            w: [
                                {
                                    id: 1,
                                    iw: [{id: 1, wn: 'price.widgets.heatmap', desc: 'heatMap', def: true}]
                                }
                            ]
                        }]
                },
                {
                    id: 4,
                    title: 'news',
                    icon: 'glyphicon icon-info-circle',
                    widgetTitle: 'newsAnn',
                    def: false,
                    isShowTitle: true,
                    rightPanel: -1,
                    tab: [
                        {
                            id: 1,
                            title: 'news',
                            def: true,
                            outlet: 'price.containers.news-tab',
                            w: [
                                {
                                    id: 1,
                                    iw: [{id: 1, wn: 'price.widgets.announcement.announcement-list', desc: 'newsAnnouncement', def: true}]
                                }
                            ]
                        }]
                },
                {
                    id: 5,
                    title: 'chart',
                    icon: 'glyphicon icon-graph',
                    widgetTitle: 'proChartTitle',
                    def: false,
                    isShowTitle: true,
                    rightPanel: -1,
                    tab: [
                        {
                            id: 1,
                            title: 'chart',
                            def: true,
                            outlet: 'chart.pro-chart-tab',
                            w: [
                                {
                                    id: 1,
                                    iw: [{id: 1, wn: 'chart.pro-chart', desc: 'proChartTitle', def: true}]
                                }
                            ]
                        }]
                }
            ]
        },

        rightPanel: {
            template: 'layout.right-panel-container',
            content: [
                {
                    id: 1,
                    icon: 'fa fa-bullhorn',
                    def: true,
                    wn: 'price.widgets.announcement.announcement',
                    rightPanelTitleKey: 'newsAnn'
                },
                // {
                //    id: 2,
                //    icon: 'fa fa-bell',
                //    def: false,
                //    wn: 'price.widgets.alert-price'
                // },
                // {
                //    id: 3,
                //    icon: 'fa fa-comment',
                //    def: false,
                //    wn: 'price.widgets.chat'
                // },
                {
                    id: 4,
                    icon: 'fa fa-eye',
                    def: false,
                    wn: 'price.widgets.watch-list.quote-watch-list',
                    rightPanelTitleKey: 'watchList'
                }
            ]
        }
    };

    var args = {
        mainPanel: {
            2: {
                tab: {
                    1: {
                        w: {
                            1: {
                                1: {selectedLink: 1}
                            },
                            2: {
                                1: {selectedLink: 1}
                            },
                            3: {
                                1: {selectedLink: 1}
                            },
                            4: {
                                1: {selectedLink: 1}
                            },
                            5: {
                                1: {
                                    mode: 1,
                                    selectedLink: 1
                                } // DepthByPrice: 1, DepthByOrder: 2
                            },
							6: {
                                1: {
                                    mode: 2,
                                    selectedLink: 1
                                } // DepthByPrice: 1, DepthByOrder: 2
                            },
                            7: {
                                1: {selectedLink: 1}
                            }
                        }
                    },
                    4: {
                        w: {
                            1: {
                                1: {selectedLink: 1}
                            },
                            2: {
                                1: {selectedLink: 1}
                            },
                            3: {
                                1: {selectedLink: 1}
                            },
                            4: {
                                1: {selectedLink: 1}
                            },
                            5: {
                                1: {selectedLink: 1}
                            }
                        }
                    }
                }
            },
            3: {
                tab: {
                    1: {
                        w: {
                            1: {
                                1: {mode: 1, selectedLink: 1} // TopGainersByChange, TopGainersByPercentageChange
                            },
                            2: {
                                1: {mode: 3, selectedLink: 1} //  TopLosersByChange, TopLosersByPercentageChange
                            },
                            3: {
                                1: {mode: 4, selectedLink: 1} //  MostActiveByVolume
                            },
                            4: {
                                1: {mode: 5, selectedLink: 1} // MostActiveByTrades
                            },
                            5: {
                                1: {mode: 6, selectedLink: 1} // MostActiveByValue
                            }
                        }
                    }
                }
            },
            8: {
                tab: {
                    1: {
                        w: {
                            1: {
                                1: {selectedLink: 1}
                            },
                            2: {
                                1: {
                                    isClassicView: true,
                                    isThinWL: true,
                                    selectedLink: 1
                                }
                            },
                            4: {
                                1: {selectedLink: 1}
                            },
                            5: {
                                1: {
                                    dockedWidgetId: 4,
                                    selectedLink: 1
                                }
                            }
                        }
                    }
                }
            },
            6: {
                tab: {
                    1: {
                        w: {
                            1: {
                                1: {isClassicView: true}
                            },
                            2: {
                                1: {type: 11}
                            },
                            3: {
                                1: {type: 77}
                            }
                        }
                    },
                    2: {
                        w: {
                            1: {
                                1: {isClassicView: true}
                            }
                        }
                    },
                    3: {
                        w: {
                            1: {
                                1: {isClassicView: true}
                            }
                        }
                    }
                }
            }
        },

        rightPanel: {
            4: {selectedLink: 1}
        }
    };

    return {
        layout: layout,
        args: args
    };
});

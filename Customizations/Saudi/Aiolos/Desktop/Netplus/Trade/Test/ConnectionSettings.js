class ConnectionSettings {
    constructor() {
        this.ip = '192.168.17.44:9080/streaming-api';
        this.port = '';
        this.secure = false;
        this.enablePulse = true;
    }
}
module.exports = new ConnectionSettings();

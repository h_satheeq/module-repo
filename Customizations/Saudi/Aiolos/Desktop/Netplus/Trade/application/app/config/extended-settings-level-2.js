export default {
    appConfig: {
        customisation: {
            appName: 'NetPlus New',
            hashType: 'SHA-256',

            displayProperties: {
                dispProp: ['tradingAccName', 'cashAccount.invAccNo']
            }
        },

        loggerConfig: {
            serverLogLevel: 0 // Server log level disabled
        }
    },

    tradeSettings: {
        channelId: 1,

        connectionParameters: {
            primary: {
                ip: '192.168.14.214:9080/streaming-api',
                port: '',
                secure: false
            },

            secondary: {
                ip: '192.168.14.214:9080/streaming-api',
                port: '',
                secure: false
            }
        }
    },

    tradeWidgetConfig: {
        transactionHistory: {
            defaultColumnMapping: {     // Column Object parameters : id, width, headerName, sortKeyword, multiValueIds, cellStyle, sortDisable, firstValueStyle, isBold, dataType, backgroundColour,
                typ: {id: 'trnTyp', width: 90, sortKeyword: 'trnTyp', type: 'transTypCell', firstValueStyle: 'colour-normal', cellStyle: 'text-left-header'},
                amount: {id: 'amt', width: 90, type: 'classicCell', sortKeyword: 'amount', firstValueStyle: 'fore-color', dataType: 'float'},
                sts: {width: 95, type: 'transStatusCell', firstValueStyle: 'colour-normal', headerStyle: 'text-left-header', cellStyle: 'text-left-header'},
                valDte: {width: 90, dataType: 'date', headerStyle: 'text-left-header', cellStyle: 'text-left-header'},
                secAccNum: {width: 100}
            },

            defaultColumnIds: ['typ', 'curr', 'amount', 'invAccNum', 'sts', 'valDte', 'chTranId', 'payMtd', 'bnkAccNum', 'secAccNum'],

            tableParams: {
                MaxTableWidth: 5700,
                numOfFixedColumns: 2
            }
        }
    }
};
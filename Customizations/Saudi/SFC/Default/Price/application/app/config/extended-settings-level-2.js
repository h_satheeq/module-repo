export default {
    appConfig: {
        customisation: {
            clientPrefix: 'sfc',
            authenticationMode: 2
        },

        googleAnalyticConfig: {
            id: ''
        }
    },

    priceSettings: {
        connectionParameters: {
            primary: {
                ip: 'data-sa9.mubasher.net/html5-Retail',
                port: '',
                secure: true
            },

            secondary: {
                ip: 'data-sa9.mubasher.net/html5-Retail',
                port: '',
                secure: true
            }
        }
    }
};

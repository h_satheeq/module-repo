import Ember from 'ember';
import BaseController from '../../base-controller';

export default BaseController.extend({
    onAfterRender: function () {
        Ember.$('.nano').nanoScroller({scroll: 'top'});
    }
});
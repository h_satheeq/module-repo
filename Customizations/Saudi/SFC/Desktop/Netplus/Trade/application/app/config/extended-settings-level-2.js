export default {
    appConfig: {
        customisation: {
            authenticationMode: 4,
            isTradingEnabled: true,
            hashType: 'MD5',
            isUpperCasePassword: true,
            isPasswordChangeEnable: true,
            isAboutUsEnabled: true
        }
    }
};

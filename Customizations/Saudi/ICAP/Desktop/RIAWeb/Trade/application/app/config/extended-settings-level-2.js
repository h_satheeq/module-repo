export default {
    appConfig: {
        customisation: {
            appName: 'ICAP Trade',
            authenticationMode: 4,
            isTradingEnabled: true,
            hashType: 'MD5',
            isUpperCasePassword: true,
            isStateSavingEnabled: false,
            isAboutUsEnabled: true,
            isCustomWorkSpaceEnabled: true,
            showFailedLoginAttempt: true,

            loginViewSettings: {
                isForgotPasswordEnabled: true,
                isRegisterEnabled: true,
                isOnlineAccEnabled: true
            },

            supportedContacts: [
                {key: 'email', value: 'info@alistithmarcapital.com', type: 'E'},
                {key: 'url', value: 'WWW.ICAP.COM.SA', type: 'U'}
            ],

            displayProperties: {
                dispProp: ['tradingAccName', 'cashAccount.invAccNo'],
                dispPropPrefix: ['', 'invAccNo']
            },

            logo: 'assets/img/logo.png',
            logoBase64: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAJoAAAAzCAYAAABv7BExAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAA2lpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNS1jMDE0IDc5LjE1MTQ4MSwgMjAxMy8wMy8xMy0xMjowOToxNSAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wTU09Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9tbS8iIHhtbG5zOnN0UmVmPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvc1R5cGUvUmVzb3VyY2VSZWYjIiB4bWxuczp4bXA9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8iIHhtcE1NOk9yaWdpbmFsRG9jdW1lbnRJRD0ieG1wLmRpZDo3NTg1QkJDMkM4MEQxMUU2OTY4NUNERTkyODVEQjBFNyIgeG1wTU06RG9jdW1lbnRJRD0ieG1wLmRpZDoyMjAwQ0VDQjJBOUIxMUU3QTEzOEM3NUI3RTkyQkI3RCIgeG1wTU06SW5zdGFuY2VJRD0ieG1wLmlpZDoyMjAwQ0VDQTJBOUIxMUU3QTEzOEM3NUI3RTkyQkI3RCIgeG1wOkNyZWF0b3JUb29sPSJBZG9iZSBQaG90b3Nob3AgQ0MgKFdpbmRvd3MpIj4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6NzAwY2ZlYTEtNThiYy1hOTRkLWIwNjctNTc5YWU2YTZlY2I5IiBzdFJlZjpkb2N1bWVudElEPSJ4bXAuZGlkOjc1ODVCQkMyQzgwRDExRTY5Njg1Q0RFOTI4NURCMEU3Ii8+IDwvcmRmOkRlc2NyaXB0aW9uPiA8L3JkZjpSREY+IDwveDp4bXBtZXRhPiA8P3hwYWNrZXQgZW5kPSJyIj8+3+VfSwAAFVZJREFUeNrsXQmUFNW5vlW99yzMwgzbsA6bgGjCIosoKoIGH6hHAZ9riHsAZYkEXkQNKiAqg6gIGsCIgYdEEiURn7gLPskTBUSRHUYYZh9m670q94evnEtR1V099HD00fec/zTTVXWX/373+5d7q5FUVWXJkixNXaT8S+cmtfBzmzRFZXKYE4SUkOpac5nGZQOXjVyURPb1+00zTnzKyWk7p0sal/lcJnNZzuWKpsJEEmjnbsngspjLTQKzvcLl8qbARRJo52ZJ4bKIy83sVAPcjssKLkMTjY0k0M69ks3lBS7/aXK9DZeliQZbEmjnVknlUsDl9hhzn8/lVS5DuNiSQEuWeEoWl+dgLq2UPPhsCWE2fQVSlEptTaQAiSUqUDcfo/QTmexoY23KPjbjsoDLr+NspzN8tkFnCjbZgFr7c3EZoLt7E62yHmi3KQr5G10NxnO2C7XfDeNsz6WVARC6N9FizuQyj8ttUe7xcVGjMNtSmFE5UUCjgT4JG+4E+jtymcPl4gQrwMvlbi5TmsiEE4j/yGVEHBN4IZdhXOwJ7Afp8T+gVypXcpkLp1wDwl1c7uUSsVSj9c2cVLR1T5R7ShCBbopS83lc/sSld2OZVz/BAS4tuSwEE5CSfsflFnRIX+zCZ7xguQwAzuVyvAmANpHLePQrYNGsPcZlLfokFoewEOMdZz4mqROXGuiU2GUqrl/DTiZN3ZZsL+0KRCzvCtwcJbqkUoExTwfQP48Ctnz0OT1RPlodPrNxvS2oNaxjvgvAfL0wmMFxtt0cn3VNaDYZWEKyALJRXMq5vMdOJjE1sHUAYMlfuR4MGc+q9mJyqvF3SNe/DHzWJJjNaPFfEcUtqZAk9odQxLbYH7Izf8ixU1Gl+wA2szISRJSQqJOUSPtdQcF+651VD5dxiGLuxoq8Lc62/RadYDd8nHgpuz5OPRSgrS1g2l/i2lVcngf4yBT/Ps6+hE1MYp1w3VIwYJHNPACZC/8+vR5JrQyFbY9W1blf6tKiVL246wF2cfd9zO0Ib6/1uyZwwH0eZS5SBb/SHg/qrZgUI5C8BXP6BZfdkHgjsFhM0IfLJVy+4/K2wAaJqF8sEfhQpVyKuczishPXPuEyE0xXBGZKxMZzfAuH8BWbzXK43MmlkMtK6G2k2Bb/R1m1zz3ngrZFL9psEfWZcetZt24HuEEPs3krrmNbC1t9ta+4+cTKOu8iu6wM1NV/GEJs/xCXr7m8YcU1sRsQs2pxlX4u0OzmBJu95mDKh7ByngXQmrK8LPxbXNHfQhgUGw9LJuwkhKSqTIrOZs3AthRc1XKp4rKGy3C4OVQqy2tTHuuQU/H8wlvXsa4cYGpFM+YrzTpxcfp1fC3l1rKxMyd8+UFp9m/TPP7FDlvkImExPoO5fxruhA/fr46FG9kAaJE4deBGyK6vt6WBf0As1SJGfeSz/IHLEwgS5iBxGGJnp+SgD5JBFJtrsQ7yW9slrEex2Swbi3KiEG0+j9TE03wgRbKsVhdXpc/vkFu+ZOX9K1nXtseYr7i5JxC2t+GmtCOXDv46Twu1NMXZv1MhG3fplq8iivRgMGz/mvtyYczB37k8CpBpZpoCxxtiMbT9jNzNk07ujcgLPS6AbDCiqWexurQw/mb4fkujmBOqbxKXvVzuQNjNziLIyEy+BvZShYmbCTP6VoyFez6Xp7jM5nLwLLBZOphsqm6y2yNtQQy34Eh5hrNTi7KFa6a9EuqYU9bMV5nRl4PrEiyKdLBvWajOu2PqNR98yDrVbM174fj/zn3zqifSvL7BXmfwOVWVHuH33G+gs1egq7Vn4qNFU+rFYJ4VwvcUpT6C1f9fQjtjsBrmxIgUxxLFQ3FnE2Q0SQ/Cx1muW3DUp8kAmlFxYHWHUcel+GxqNiPmnQYmM4Jhnk1WZx2tTL+lc27ZjtemLFM65h3rFSjJvAss1Fr/AI83mK/es8e9n706fdz6ZZU+99uvbhy01sect3ucwfs52MzAvgg6+2uigUbK7QlTWK7LJJOitwoRlRtRXDZ8BxYlMdgHjvgxrDZtC0nCs4dZgk+BCimbq9HXsA5ow9GPet39dE9HMDU53uuRL/PrXJBGb7OdYDM6TSsbTi6BbIZZbk+W1NrSmpR3erQuPvra5OVKfrujgwLFzcnUEZuZtympXYI+9+PBOm/PuRPXTm2ZdbzoidUjt/Hhvssj0xEmj5Gr9CL68kYigaYKKRBxoEH4VqruXn+U3J2YyaazUv3BknZdknQPO3lY7x9C2/FGd06TZ2X0O2TQxxphPDbk0rpwWQZwUtLzYfgw1bp2tWR2WqPYTDGEqMZk06IkkGuqfO6CC9sdffLF36zx57cp7h4qzXpGIZBZaJqa9djDN4UPZtQ9eOt701SZfT1v1a/utUlqgcMeGW3yWC58Nlqo6xoDNNVgckJCWCsmcwPop6oDXyhKBKuVvfDf2iAPpAr3EAgvgz9IgPvGoC7yA2nPcLvAKFrbHWEuyHf5ksub7NQdiaDAZH6DsYq5r8fAvi9joumZMrSpTxDT96u47DIYb2w2U07zzQhkv4PzbzZ/tZzNFhwrbj736bHr/V3yD7sCxdnTFCYNsJIj0VZJSJGZGrbfyQ5mfDh5zHtr7CH54CsbBz/oC9nJgF5r8ijNzwvQ2fp4gKaiXXG7SWUN2zLi6pV1dWohvpM17DeqUYKPnfBt9KmBMHygN5AXam8AtBQ47LRVMkFYGH74InPhJ2olH8HKcQPmk9FfLXktC/c4hESoCoWqwjVJV98hdnIHRdOZ6wxy/g6Y6SlR5q5eVeWFx/32ecP7fOPLb1nGIsfT+3FkXCax+N54O2HvZYWFAo67pYPZH08c9+6RlR9fdLDa73zAaVNIJ6OigK1AzK/aLZgcJxRLk3c5FKUpX8vP3ABfzQk2yoRp0WiU/J6heM6uA/kgTPiPCwn3STomD8L/UU363RK7E4cBLm0h0D7t9YiC/8nlb/BrpgKwVajTBp/QC8YqwxjDYFLqD+0JHoUi3Rif5kcSAHrAxxSPW+Vh8dign18IemXCAnQbms1TSzcEK2b7ovW84QKVqXOKyrPq5495hw24YBfzlWVexf2uxqdbJDYwIrFeakl60fihXyhL3h94uNbvnCxJJ+bFjNnyEaRMjJbekIR/+2AOR0CMtlIugmjFB59kgu7eIwLThPFse0R0VstREx9LBTuFMck+gOhy4Z4PYO6uhSm9RVdHEA7/OIO664QVHITcJehgMIRM5w8CI7dhDRvoWqkWzHAIzwQsmM3ezPy4VvEJ/1VSn6n1u+pG9dvBuuUVsXBNiouDrMeZ+OO8C86IIp8X8bk/umf0e4G1X/Rm1T7Xfu6vTYYerhUWjliuNjOdGhOoyH+FkCP5VGAxVWCfiM60VAvs4AOzaQxYh7SFlmX/FwBmjyMik9DGDpNrHrTvhJk9iDH0RFt0XKYvO3m26gh8iQohggwLepGERRcWxqWNW2NWRdCBDdd8iJqpHOByn9CGgvo0PXwGIG6xMP5MkwllMFEv8e7VcqCxmwdsZX177mW+8oxmHGgpCcj95HAQ21ltSiCi/Bh/HIQOrzLpV4oItC5gghJMCK2YIlRCSnsfYqUMwGchnOV3WMOmfHfk2Irx9z5IIooH7ESO8hKMh5hvm+DbBDHh49Au5fRWnoX8XMlJAJiWnaxhXzVWqcLCMfLz6HTJeE6EBRxYPiF9EWrEbo+RuQgzCgFS65hNVsSk8L1RTHmNCLR2QGQaVnsYPlN1nH3pwE6e4qDyHswKHR3eiJzPZOSjKPP/fwmeTA8mdAoy93UGUeOTYOZ2yHl9xn5+ZTv6fqGJA06+pyPdE5j/5019fBd0OMK6ti2q9lWnlkrSmf30hSyxwzZXMPjS34az8toUytF1AE6ui/LYhh+TjlkdhlVjotqCBejoz+pGrAItUtyGZG0RMvuVQsLyAL6rSfAERNDmv5j5+bYQmGwrAoafbNH8M8mY0UjPV5j4XMR0/Vz2iPrNDy2/+PpQm0iPVqVqXnZVJ1WxXaLLFFhnM0WqdbrCT9vaVhXet/AWVlHnzrPb1IUxQEYWcdLE3wwp1xitHMDSjuEEG6mfCp2J+Ejn+61rwrkJsv9PhWKAiGEqlsb5OkhhKjM+op7KMTqtZUYNe39nl6cOFGcH+vb+fkOoNOsOXvH58XZF4f6YyxV4V25btXfRqmFqIGzLc9iUBVGiTS0wmcqEo2PEaAm15cmSEE5jlPOSVMMwyY+EM4GsvwnYXPzRvh5XyLavNPuLvu2OFbVuVdIq4nMNps0syWroxX0yhy3is3sDU2avHrFz/l+vyuO9WmC3KTfE8EkppXFiz5Mz2o/mLll+ejhjil1mKneMTHKs5I7Q4YQFZole/mRaujsw9csDbafdXHCH9G1hq0Wu3Io3CTwmG+Onl7DM7F3L581+c8T7f1w1KsfpCD/nsoejgawcOcPT9jpFRovlf6UIoT2Z3Nw4WTAV4bnRq102XJdZHFs0CSxZSPhWnYm/jDri0YmmVwcz2KIjoJ1wbhVTZtuKOi4yJA1VcjXz+n+562iu49Nt52249PzdW1q1Ks0K+zwXSDHMpV1Sa13nlTxR8Jdh8wveGpZLx4TS3IFYTEZ50zXilxqj6YFGk00Z9F26/BRlpGcBseRI0y7AQ0hjlFjIfVGhEx23wmHX7yVSFDUNyt57lkF2PfJqzZGe6YCgIV43ggDzBPpfajB+o0IR8HSkZXaYKlAxrcWHCN4rpJb0xZ2ZWt93z9Hc8Ic7uv/zyvN3b2qRW1ao+N2Z6sldCz3IFPLJ7K7QIw+/fs3SpW8PpfzY4jSPf0yUoVBw91su/62/YAQ0qoUy239CiqBCuJ9SH7chatwBp5T28H4wAI1WvMillYHBKLrdA0cxgtXfGuBtjQmnJOpXZxFkDwBk/0C0TPkgeu/y40YEGGEsPFqIAbBULyFvaFQIYKNgIT409dekqGDzA2wSwCbrn5cl1dUstb7P4fIs18YdXTcM77Z/S06b4k+UgPsTVZG38lu28Qn6lD/4d4c7sMje7vjy2X8etXn+umHNnY7Q4lR38Aba9YwSBN4HJlOtAI0GS/uOw7BSPtFR/GDWcGoiDfdXQcGaY0rMp+0o/Bq5tO1Y4anoSAWARYci+6Ed+r6HkAaRUTexRB7ycP0A3gj6KLGGc3BO9K8zGDYEM03t0J5bJ9wrmieK3OgN7peR96vG4qnBAgrhHm2fswyfnTD+fujbIXyfjn5XQjfjsTi/w/jtADUx5jGMg9IRvQHqj6LZgyhAo1IPS5HKTt0KRBDLwSarnjS3v09RVXpo445um4f3/r68ebvib2317i1hRd7E7/nEk+r7VG5f8/28Zb+qen7DJZleV/A5rys0lltwsxewfTCXq5nJGUF9MCDBdMjI9F5uEM2IL670gCPaFixAq7IrgHEFlNgCmWNNPZSofRwr3QmF57CGfUM3WO5KRC3ae6VTMRhq82GY3/5IAHdG3dMAgMvQjpaIpiTtnfhefx5sEPrypTCuEBK5xBJ0YOBRAGcWAMuwgBaCPZ6Eq6FirM+zhvcncgFEO/RCR6AHCm6CqFclevDHHXibFOt8By1Meh2wwLAO9QQzpuem180orGr2+3uXjnOMnDmZ7S7OCnlyKmq8bUpqn1p3ZWTsjHvYsneHtHDaIi+muoI3cjaUVfP27ooGMrHYBaANQXb5KyhriG6V6c9Y2cEwMu7thEnV9v6I/fbjkwptCzVDcpie/xYrIoTvbDDL5OPcDbO7GdePw/4PBoMuh1I74f4dMFv9Ya5SkDAm07QKTnOFQSY9ZOCERwST9DGe6w7g7gd70NhXAKi0ENbDRLrQdhBM3gX+rg1+nw9s3EsAmaUolAJFC7Ei6XU22ptoFvhkeX3TvzvSMhII2Z+d9ProSPO0Oj6LKtt+qA2rrPVmu52hBW4eXarmG/Gkm0mYE0vBmyxEfQOhxJ7wxcbq/UQdVVcAfD5MvBfobi2wg581nEqognnSNpeDrGEfVMb9FQBIHVhPAaMUQ6rw9zH0QdvILQVwgmhDQtsVAEehgc91BAA38z2OoP12mMCQ4PgWwrxuBvM60Z9KAagBYfxB7Lp0xHc1FgKF0yJQC6ym+UzExIuj5IMzPI7QzMwU36Q9xTm2z3Z3ZJ/tymf+kD0zxR141iYr0UBWD5/MMshEoP0CWVzaZP4fdvK9vZGs4ZUx7USEaGq1E7DaCYY7wRzTBb+pmWCCZTCAIoT14qkCJ8TOTj0J4BT+drCGo9FOALUFzH0lWMODa/o+68sG3HO7Th+DBBNJx1w2oV2vsChlwZnfJ4DQI7CUWzDXucgv7QPwMwW92pjFH6FRONAU62CjVxYXRQtEVJXNctgiD7gdYeZ2hFK5mSTTP46Zb1VpIPtLvMGSDAWOwSqrRCcPo9JbhAgymzX8TkQ6lJWDybwavtXnrOHkq8Yyo4VcVQrqqIPTPgC+j4T6UlF3lgDCLPiPrTGRaag3A3Ic398Ic9sfvl4WJN+EOWpg9oZicZBJu0kAUTlM3HUY/yDUUw8Hvg/80iUATBb60xp/l2ChjkaA5IA+tRd6umPRZQggtmRGLZYK+JZLYkS9szD2GRir2TGkkMBkcW/50f8z4IXjWw5W037gpTsYYzsmshfAswcA6wITcgROeWuYmEJMfjqUWgo/qjMAswsmMA8BwTeg4J543ocJOoQ+9cTASuA7Hod57IDvDqCvuVCuF5GeA8DT7jGieRvG0RkA0o5H+VDPYCFPGETf70LA8ySUvwd1Z6G9QvQ9BXrVxt8JoD8E3eyGGe0l1G3pJWl6xzOOXxTKQF/vjQLTzdBtG5PrdUJ06Y8HYNr/M0BAE48dRwSmU9mpZ/clwVeTdb6b9k6BqptQLV0hPiNGr/qDlPoo18oaVgTQKMKz4itusX7qQfstuJDOF7UJetHqehTgGCsEPsxAJ/rxa8FX2MR9sfpzFI35Dy3SkSUYb3Jd28I3q3E8gip/vEymAU0DR8Rk8iIGE88MwlnVZDWKylOigCRaBGb1IFUkStuxStBCnSr8riI4+Cns1LeolBjjZyas2hTvqOpLNdwEhTX8ZhwzWFBG0eXExjCZWXojWawD8lWs/J/b0aQq1nB0/g4rsQd2TlaeKciSQIu/KEJK5udYKCiZAmYdHyUiDyJP9loiQMZY8pjQuVgqBRCZuQsTwNwJW1RJRjs3iw9go3KrQDhhmNfXE8VkSaAlSzWYi/zN2xC00N8rmLUfl04CLVni8tkmI2OwDUwWaIqGpOT/QJwsZ6P8W4ABACAfvkBfZMmqAAAAAElFTkSuQmCC'
        },

        tadawulityURLs: {
            tadawulityLogin: 'https://brokerreg.tadawul.com.sa/ir/validateInvestor',
            tadawulityInfo: 'http://tadawulaty.tadawul.com.sa/tadawulaty/index.htm'
        },

        onlineAccountOpenURLs: {
            newCustomer: 'https://eforms.saib.com.sa/_layouts/SAIBeForms/ProductSelection.aspx'
        },

        loggerConfig: {
            serverLogLevel: 0 // Server log level is off
        },

        chartConfig: {
            chartIndicators: ['MovingAverage', 'TimeSeriesForecast', 'WildersSmoothing', 'BollingerBands', 'AccumulationDistribution', 'AverageTrueRange', 'ChandeMomentumOscillator', 'CommodityChannelIndex', 'DirectionalMovementPlusDI', 'DirectionalMovementMinusDI', 'DirectionalMovementADX', 'DirectionalMovementADXR', 'DirectionalMovementDX', 'MACD', 'Momentum', 'MoneyFlowIndex', 'RelativeStrengthIndex', 'StochasticOscillator', 'WilliamsPerR', 'ChaikinMF', 'PSAR', 'TRIX', 'VolOsc']
        },

        googleAnalyticConfig: {
            id: ''
        }
    },

    priceSettings: {
        urlTypes: {
            price: 'https://data-sa9.mubasher.net/mix2/ClientServiceProvider',
            content: 'https://data-sa9.mubasher.net/mix2/ClientServiceProvider',
            chart: 'https://data-sa9.mubasher.net/mix2/ClientServiceProvider',
            prodSub: 'https://data-sa.mubasher.net/uatria/riaauth',
            astMgmtEN: 'https://icap.com.sa/en/asset-management',
            astMgmtAR: 'https://icap.com.sa/ar/assetmgmt',
            accountTermsCondition: ''
        },

        connectionParameters: {
            primary: {
                ip: 'data-sa9.mubasher.net/html5-Retail',
                port: '',
                secure: true
            },

            secondary: {
                ip: 'data-sa9.mubasher.net/html5-Retail',
                port: '',
                secure: true
            }
        }
    },

    tradeSettings: {
        channelId: 30,

        connectionParameters: {
            primary: {
                ip: 'online5.icap.com.sa/trs',
                port: '',
                secure: true
            },

            secondary: {
                ip: 'online5.icap.com.sa/trs',
                port: '',
                secure: true
            }
        }
    },

    userSettings: {
        orderConfig: {
            defaultOrderQty: 0
        },

        displayFormat: {
            dateFormat: 'DD/MM/YYYY',
            dateTimeFormat: 'DD/MM/YYYY HH:mm:ss',
            dateTimeMinuteFormat: 'DD/MM/YYYY HH:mm',
            dayMonthTimeFormat: 'DD/MM HH:mm:ss'
        }
    },

    priceWidgetConfig: {
        watchList: {
            classicColumnIds: ['menu', 'sym', 'sDes', 'trend', 'ltp', 'ltq', 'bbq', 'bbp', 'bap', 'baq', 'vol', 'tovr', 'trades', 'chg', 'pctChg', 'ltd', 'dltt', 'top', 'tov', 'open', 'high', 'low'],
            customClassicColumnIds: ['menu', 'sym', 'sDes', 'exg', 'trend', 'ltp', 'ltq', 'bbq', 'bbp', 'bap', 'baq', 'vol', 'tovr', 'trades', 'chg', 'pctChg', 'ltd', 'dltt', 'top', 'tov', 'open', 'high', 'low']
        },

        WidgetList: {
            trade: [
                {code: 'trade.widgets.account-summary', des: 'Account Summary', desc: 'accountSummary', icon: 'glyphicon glyphicon-tags'},
                {code: 'trade.widgets.order-list', des: 'Order List', desc: 'orderList', icon: 'glyphicon glyphicon-list-alt'},
                {code: 'trade.widgets.order-search', des: 'Order Search', desc: 'orderSearch', icon: 'glyphicon glyphicon-search'},
                {code: 'trade.widgets.order-ticket.order-ticket-landscape', des: 'Order Ticket (L)', desc: 'orderTicket', icon: 'glyphicon glyphicon-send'},
                {code: 'trade.widgets.portfolio', des: 'Portfolio', desc: 'portfolio', icon: 'glyphicon glyphicon-duplicate'},
                {code: 'trade.widgets.rights-subscription-list.rights-subscription-list', des: 'Rights Subscription List', desc: 'rightsSubscriptionList', icon: 'glyphicon glyphicon-list-alt'},
                {code: 'trade.widgets.saved-orders', des: 'Saved Orders', desc: 'savedOrders', icon: 'glyphicon glyphicon-saved'},
                {code: 'trade.widgets.tadawulity', des: 'Tadawulity', desc: 'tdwlCart', icon: 'glyphicon glyphicon-certificate'}
                // {code: 'trade.widgets.rapid-order', des: 'Rapid Order', desc: 'rapidOrder', icon: 'glyphicon icon-sitemap'},
                // {code: 'trade.widgets.panic-withdrawal', des: 'Panic Withdrawal', desc: 'panicWithdrawal', icon: 'glyphicon icon-sitemap'},
                // {code: 'trade.widgets.consolidated-reports', des: 'Consolidated Reports', desc: 'consolidatedReports', icon: 'glyphicon icon-sitemap'},
            ],

            transfer: [
                {code: 'trade.widgets.account-statement.account-statement', des: 'Account Statement', desc: 'accStatement', icon: 'glyphicon glyphicon-file'},
                {code: 'trade.widgets.cash-transfer', des: 'Bank Transfer', desc: 'bankTransfer', icon: 'glyphicon glyphicon-transfer'},
                {code: 'trade.widgets.transaction-history', des: 'Cash Transfer History', desc: 'cashTransferHistory', icon: 'glyphicon glyphicon-book'},
                {code: 'trade.widgets.product-subscription.product-subscription', des: 'Product Subscription', desc: 'productSubscription', icon: 'glyphicon glyphicon-hand-up'},
                {code: 'trade.widgets.cash-transfer-portfolios', des: 'Transfer between Investment Accounts', desc: 'transferBetweenPortfolios', icon: 'glyphicon glyphicon-transfer'}
            ],

            mutualFund: [
                {code: 'trade.widgets.mutual-fund.mutual-fund-holdings', des: 'Mutual Fund Holdings', desc: 'fundHoldings', icon: 'glyphicon glyphicon-tag'},
                {code: 'trade.widgets.mutual-fund.mutual-fund-list', des: 'Mutual Fund List', desc: 'fundList', icon: 'glyphicon glyphicon-list-alt'},
                {code: 'trade.widgets.mutual-fund.mutual-fund-transactions', des: 'Mutual Fund Transactions', desc: 'transactions', icon: 'glyphicon glyphicon-transfer'}
            ],

            optionChain: [
                {code: 'price.widgets.option-chain', des: 'Option Chain', desc: 'optionChain', icon: 'glyphicon glyphicon-link'},
                {code: 'trade.widgets.option-trading.option-order-list', des: 'Option Order List', desc: 'optionOrderList', icon: 'glyphicon glyphicon-list-alt'}
                // {code: 'trade.widgets.order-ticket.order-ticket-portrait', des: 'Order Ticket (P)', desc: 'orderTicket', icon: 'glyphicon glyphicon-send'}
            ],

            userProfile: [
                // {code: 'trade.widgets.kyc.kyc-update', des: 'KYC Update', desc: 'kycUpdate', icon: 'glyphicon glyphicon-edit'}
            ]
        }
    }
};

/* jshint node: true */
var EnvironmentExtended = require('./environment-extended.js');

module.exports = function (environment) {
    var ENV = new EnvironmentExtended(environment);		
    ENV.contentSecurityPolicy['connect-src'] = "'self' www.google-analytics.com wss://data-sa.mubasher.net/html5ws wss://data-sa.mubasher.net/html5-ws-UAT";    

    return ENV;
};
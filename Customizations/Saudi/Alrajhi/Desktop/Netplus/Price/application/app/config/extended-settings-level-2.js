export default {
    appConfig: {
        customisation: {
            appName: 'NetPlus New',
            clientPrefix: 'alrajhisso',
            isPoweredByEnabled: true,
            authenticationMode: 2
        },

        googleAnalyticConfig: {
            id: ''
        }
    },

    priceSettings: {
        connectionParameters: {
            primary: {
                ip: 'data-sa9.mubasher.net/html5-Retail',
                port: '',
                secure: true
            },

            secondary: {
                ip: 'data-sa9.mubasher.net/html5-Retail',
                port: '',
                secure: true
            }
        }
    }
};

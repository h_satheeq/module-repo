export default (function () {
    var layout = {
        menuPanel: {
            template: 'layout.horizontal-navigation'
        },

        titleBar: {
            template: 'layout.title-bar'
        },

        topPanel: {
            template: 'top-panel-container-controller',
            content: [
                {
                    id: 9,
                    title: 'priceTopPanel',
                    def: true,
                    tab: [
                        {
                            id: 1,
                            title: 'priceTopPanel',
                            def: true,
                            outlet: 'price.containers.top-panel-tab',
                            w: [{id: 1, wn: 'price.top-panel.top-panel'}]
                        }]
                }
            ]
        },

        mainPanel: {
            template: 'main-panel-container-controller',
            content: [
                {
                    id: 8,
                    title: 'trading',
                    widgetTitle: 'trading',
                    icon: 'glyphicon icon-trade',
                    def: true,
                    isShowTitle: false,
                    rightPanel: -1,
                    tab: [
                        {
                            id: 1,
                            title: 'oneStop',
                            def: true,
                            outlet: 'trade.containers.trade-order-tab',
                            w: [
                                {
                                    id: 1,
                                    iw: [
                                        {id: 1, wn: 'trade.widgets.portfolio', desc: 'portfolioVal', def: false, isShow: true},
                                        {id: 2, wn: 'price.widgets.watch-list.watch-list', desc: 'marketWatch', def: true, isShow: true},
                                        {id: 3, wn: 'trade.widgets.order-search', desc: 'orderBook', def: false, isShow: true},
                                        {id: 4, wn: 'trade.widgets.account-summary', desc: 'sambaMub', def: false, isShow: true},
                                        {id: 5, wn: 'trade.widgets.cash-statements', desc: 'regTDWL', def: false, isShow: true},
                                        {id: 6, wn: 'trade.widgets.order-list', desc: 'pharos', def: true, isShow: true}
                                    ]
                                },
                                {
                                    id: 2,
                                    iw: [{id: 1, wn: 'trade.widgets.order-ticket.order-ticket-landscape', desc: 'orderTicket', def: true}]
                                }
                            ]
                        }
                    ]
                }
            ]
        },

        rightPanel: {
            template: 'layout.right-panel-container',
            content: [
                {
                    id: 1,
                    icon: 'fa fa-bullhorn',
                    def: true,
                    wn: 'price.widgets.announcement.announcement',
                    rightPanelTitleKey: 'newsAnn'
                },
                // {
                //    id: 2,
                //    icon: 'fa fa-bell',
                //    def: false,
                //    wn: 'price.widgets.alert-price'
                // },
                // {
                //    id: 3,
                //    icon: 'fa fa-comment',
                //    def: false,
                //    wn: 'price.widgets.chat'
                // },
                {
                    id: 4,
                    icon: 'fa fa-eye',
                    def: false,
                    wn: 'price.widgets.watch-list.quote-watch-list',
                    rightPanelTitleKey: 'watchList'
                }
            ]
        }
    };

    var args = {
        mainPanel: {
            2: {
                tab: {
                    1: {
                        w: {
                            1: {
                                1: {selectedLink: 1}
                            },
                            2: {
                                1: {selectedLink: 1}
                            },
                            3: {
                                1: {selectedLink: 1}
                            },
                            4: {
                                1: {selectedLink: 1}
                            },
                            5: {
                                1: {
                                    mode: 1,
                                    selectedLink: 1
                                } // DepthByPrice: 1, DepthByOrder: 2
                            },
							6: {
                                1: {
                                    mode: 2,
                                    selectedLink: 1
                                } // DepthByPrice: 1, DepthByOrder: 2
                            },
                            7: {
                                1: {selectedLink: 1}
                            }
                        }
                    },
                    4: {
                        w: {
                            1: {
                                1: {selectedLink: 1}
                            },
                            2: {
                                1: {selectedLink: 1}
                            },
                            3: {
                                1: {selectedLink: 1}
                            },
                            4: {
                                1: {selectedLink: 1}
                            },
                            5: {
                                1: {selectedLink: 1}
                            }
                        }
                    }
                }
            },
            3: {
                tab: {
                    1: {
                        w: {
                            1: {
                                1: {mode: 1, selectedLink: 1} // TopGainersByChange, TopGainersByPercentageChange
                            },
                            2: {
                                1: {mode: 3, selectedLink: 1} //  TopLosersByChange, TopLosersByPercentageChange
                            },
                            3: {
                                1: {mode: 4, selectedLink: 1} //  MostActiveByVolume
                            },
                            4: {
                                1: {mode: 5, selectedLink: 1} // MostActiveByTrades
                            },
                            5: {
                                1: {mode: 6, selectedLink: 1} // MostActiveByValue
                            }
                        }
                    }
                }
            },
            8: {
                tab: {
                    1: {
                        w: {
                            1: {
                                1: {selectedLink: 1}
                            },
                            2: {
                                1: {
                                    isClassicView: true,
                                    isThinWL: true,
                                    selectedLink: 1
                                }
                            },
                            4: {
                                1: {selectedLink: 1}
                            },
                            5: {
                                1: {
                                    dockedWidgetId: 4,
                                    selectedLink: 1
                                }
                            }
                        }
                    },
                    2: {
                        w: {
                            3: {
                                1: {isVerticalView: true}
                            }
                        }
                    },
                    4: {
                        w: {
                            1: {
                                1: {selectedLink: 1}
                            },
                            2: {
                                1: {selectedLink: 1}
                            },
                            3: {
                                1: {selectedLink: 1}
                            },
                            4: {
                                1: {selectedLink: 1, isOptionMode: true}
                            }
                        }
                    }
                }
            },
            6: {
                tab: {
                    1: {
                        w: {
                            1: {
                                1: {isClassicView: true}
                            },
                            2: {
                                1: {type: 11}
                            },
                            3: {
                                1: {type: 77}
                            }
                        }
                    },
                    2: {
                        w: {
                            1: {
                                1: {isClassicView: true}
                            }
                        }
                    },
                    3: {
                        w: {
                            1: {
                                1: {isClassicView: true}
                            }
                        }
                    }
                }
            }
        },

        rightPanel: {
            4: {selectedLink: 1}
        }
    };

    return {
        layout: layout,
        args: args
    };
});

export default {
    appConfig: {
        customisation: {
            clientPrefix: 'retail',
            hidePreLogin: true
        },

        subscriptionConfig: {
            registrationPath: '/create-account?language=',
            upgradeSubscriptionPath: 'upgrade-account',
            renewSubscriptionPath: 'renew-account',
            daysBeforeExpiration: 7
        }
    },

    priceSettings: {
        connectionParameters: {
            primary: {
                ip: 'data-sa9.mubasher.net/html5-Retail',
                port: '',
                secure: true
            },

            secondary: {
                ip: 'data-sa9.mubasher.net/html5-Retail',
                port: '',
                secure: true
            }
        }
    }
};

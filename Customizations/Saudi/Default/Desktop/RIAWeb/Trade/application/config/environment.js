/* jshint node: true */
var EnvironmentCore = require('./environment-core.js');

module.exports = function (environment) {
    var ENV = new EnvironmentCore(environment);
    ENV.contentSecurityPolicy['connect-src'] = "'self' www.google-analytics.com wss://lkcentralprice.directfn.com/price ws://59.163.249.198:8800/";
    ENV.contentSecurityPolicy['frame-src'] = "'self' http://dfnnet.directfn.com/ESTORERIA/Agreement/en/EULA.htm";

    if (environment === 'development' || 'automation') {
        ENV.APP.tradeConnectionParameters = {
            primary: {
                ip: '192.168.13.29',
                port: '8090/trs',
                secure: false
            }
        };

        ENV.APP.priceConnectionParameters = {
            primary: {
                ip: 'data-sa9.mubasher.net/html5ws',
                port: '',
                secure: true
            }
        };

        /* ENV.gReCaptcha = {
            // Domains included: localhost, saudi-dev-riaweb.universal.directfn.net, saudi-qa-riaweb.universal.directfn.net
            siteKey: '6LfVBw4UAAAAAM4SQ1bESrYwhO7fIojAw7utbYUl'
        }; */
    }

    return ENV;
};
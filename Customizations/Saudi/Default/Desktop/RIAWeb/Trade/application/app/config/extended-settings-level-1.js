export default {
    appConfig: {
        customisation: {
            hashType: 'SHA-1',
            tilaAgreementEnabled: true,
            isVirtualKeyboardEnable: true,
            isSavedOrdersEnabled: true,
            isPasswordChangeEnable: true,
            smartLoginEnabled: false,
            isKYCUpdateEnable: true,

            loginViewSettings: {
                isForgotPasswordEnabled: true,
                isRegisterEnabled: true
            },

            passwordRules: {
                maxLength: 18,
                minLength: 8,
                checkLength: true,
                checkContainNumber: true,
                checkStartWithLetter: true,
                checkContainSmallLetter: true,
                checkContainCapitalLetter: true,
                checkIdenticalCharacter: true,
                checkConsecutiveCharacter: true,
                checkSpecialCharacter: true,
                checkUsernameMatch: true
            },

            usernameRules: {
                maxLength: 12,
                minLength: 6,
                checkLength: true,
                checkEngAlphaNumeric: true,
                checkStartWithLetter: true,
                checkIdenticalCharacter: true
            }
        },

        chartConfig: {
            chartIndicators: ['MovingAverage', 'TimeSeriesForecast', 'WildersSmoothing', 'BollingerBands', 'AccumulationDistribution', 'AverageTrueRange', 'ChandeMomentumOscillator', 'CommodityChannelIndex', 'DirectionalMovementPlusDI', 'DirectionalMovementMinusDI', 'DirectionalMovementADX', 'DirectionalMovementADXR', 'DirectionalMovementDX', 'MACD', 'Momentum', 'MoneyFlowIndex', 'RelativeStrengthIndex', 'StochasticOscillator', 'WilliamsPerR']
        }
    },

    userSettings: {
        customisation: {
            defaultCountryID: '2',
            defaultanotherNationalityID: '226'
        }
    },

    tradeWidgetConfig: {
        cashStatement: {
            defaultColumnMapping: {     // Column Object parameters : id, width, headerCellView, headerName, sortKeyword,  cellStyle, sortDisable, dataType, type, headerStyle, cellStyle
                date: {width: 90, sortKeyword: '', headerName: 'date', firstValueStyle: 'colour-normal font-l', dataType: 'date'},
                trnsType: {width: 170, headerName: 'transactionType'},
                amount: {width: 85, headerName: 'amount', headerStyle: '', cellStyle: 'text-right-header'},
                balance: {width: 150, sortKeyword: '', headerName: 'accBal', headerStyle: '', cellStyle: 'text-right-header'}
            },

            defaultColumnIds: ['date', 'trnsType', 'sDes', 'amount', 'commission', 'vat', 'balance'],

            tableParams: {
                MaxTableWidth: 5700
            }
        },

        transactionHistory: {
            defaultColumnMapping: {     // Column Object parameters : id, width, headerName, sortKeyword, multiValueIds, cellStyle, sortDisable, firstValueStyle, isBold, dataType, backgroundColour,
                typ: {id: 'trnTyp', width: 90, sortKeyword: 'bankAcnt', type: 'transTypCell', firstValueStyle: 'colour-normal', cellStyle: 'text-left-header'},
                amount: {id: 'amt', width: 90, type: 'classicCell', sortKeyword: 'amount', firstValueStyle: 'fore-color', dataType: 'float'},
                sts: {width: 95, type: 'transStatusCell', firstValueStyle: 'colour-normal', headerStyle: 'text-left-header', cellStyle: 'text-left-header'},
                valDte: {width: 90, dataType: 'date', headerStyle: 'text-left-header', cellStyle: 'text-left-header'},
                secAccNum: {width: 100}
            },

            defaultColumnIds: ['typ', 'curr', 'amount', 'invAccNum', 'sts', 'valDte', 'chTranId', 'payMtd', 'bnkAccNum', 'secAccNum'],

            tableParams: {
                MaxTableWidth: 5700,
                numOfFixedColumns: 2
            }
        },

        portfolio: {
            defaultColumnMapping: {     // Column Object parameters : id, width, headerName, sortKeyword, multiValueIds, cellStyle, sortDisable, firstValueStyle, isBold, dataType, backgroundColour,
                secAccNum: {id: 'portfolioInfo.tradingAccName'},
                mktPrice: {noOfDecimalPlaces: 'symbolInfo.deci'},
                avgCst: {isFieldConfigAvailable: true},
                costVal: {isFieldConfigAvailable: true},
                mktVal: {isFieldConfigAvailable: true},
                gainLoss: {width: 110, isFieldConfigAvailable: true},
                gainLossPer: {width: 110, noOfDecimalPlaces: 2},
                portPer: {noOfDecimalPlaces: 2},
                pendSubQty: {width: 135}
            },

            defaultColumnIds: ['buyMore', 'liquidate', 'isRightSymbol', 'sym', 'sDes', 'secAccNum', 'qty', 'pendSell', 'pendBuy', 'avaiQty', 'mktPrice', 'avgCst', 'costVal', 'mktVal', 'gainLoss', 'gainLossPer', 'portPer', 'curr', 'pldQty', 'payQty', 'recQty', 'pendSubQty', 'subQty', 'wAvgPrice', 'subPrice', 'exg'],

            moreColumnIds: ['sDes', 'secAccNum', 'qty', 'pendSell', 'pendBuy', 'avaiQty', 'mktPrice', 'avgCst', 'costVal', 'mktVal', 'gainLoss', 'gainLossPer', 'portPer', 'curr', 'pldQty', 'payQty', 'recQty', 'pendSubQty', 'subQty', 'wAvgPrice', 'subPrice', 'exg'],

            nonRemovableColumnIds: ['buyMore', 'liquidate', 'isRightSymbol', 'sym'],

            tableParams: {
                MaxTableWidth: 5700,
                numOfFixedColumns: 4
            },

            footerParams: {
                // Format Accepting:- data type = string [eg: 'sDes'].
                // Not Accepting:- data type = int, float (other than string) [eg: 'qty'] & Properties like [eg: 'symbolInfo.sDes'].
                totalValueProp: 'dSym'
            }
        },

        orderList: {
            defaultColumnIds: ['isAmendEnabled', 'isCancelEnabled', 'sym', 'sDes', 'instDes', 'clOrdId', 'ordSts', 'ordSide', 'ordQty', 'price', 'cumQty', 'pendQty', 'avgPrice', 'ordVal', 'commission', 'vat', 'netOrdVal', 'ordTyp', 'adjustedCrdDte', 'adjustedExpTime'],

            moreColumnIds: ['sDes', 'instDes', 'clOrdId', 'ordSts', 'ordSide', 'ordQty', 'price', 'cumQty', 'pendQty', 'avgPrice', 'ordVal', 'commission', 'vat', 'netOrdVal', 'ordTyp', 'tif', 'adjustedCrdDte', 'adjustedExpTime', 'exg']
        },

        orderDetails: {
            orderDetailsPopup: [
                {lanKey: 'exgOrderNo', dataField: 'ordId', formatter: 'S', style: '', cellStyle: ''},
                {lanKey: 'orderId', dataField: 'clOrdId', formatter: 'S', style: '', cellStyle: ''},
                {lanKey: 'side', dataField: 'ordSide', formatter: 'S', style: '', lanKeyAppend: 'orderSide', isCustomStyle: true, cellStyle: ''},
                {lanKey: 'status', dataField: 'ordSts', formatter: 'S', style: '', lanKeyAppend: 'orderStatus', isCustomStyle: true, cellStyle: ''},
                {lanKey: 'quantity', dataField: 'ordQty', formatter: 'I', style: '', cellStyle: ''},
                {lanKey: 'pendingQty', dataField: 'pendQty', formatter: 'I', style: '', cellStyle: ''},
                {lanKey: 'filledQuantity', dataField: 'cumQty', formatter: 'I', style: '', cellStyle: ''},
                {lanKey: 'orderType', dataField: 'ordTyp', formatter: 'S', style: '', lanKeyAppend: 'orderType', cellStyle: ''},
                {lanKey: 'price', dataField: 'price', formatter: 'C', style: '', cellStyle: ''},
                {lanKey: 'averagePrice', dataField: 'avgPrice', formatter: 'C', style: '', cellStyle: ''},
                {lanKey: 'stopPriceType', dataField: 'stPrcTyp', formatter: 'S', style: '', cellStyle: '', allowedOrdTypes: ['3', '4']},
                {lanKey: 'stopPrice', dataField: 'stPrc', formatter: 'C', style: '', cellStyle: '', allowedOrdTypes: ['3', '4']},
                {lanKey: 'maximumPrice', dataField: 'maxPrc', formatter: 'C', style: '', cellStyle: ''},
                {lanKey: 'date', dataField: 'adjustedCrdDte', formatter: 'S', style: '', cellStyle: ''},
                {lanKey: 'expiryDate', dataField: 'adjustedExpTime', formatter: 'S', style: '', cellStyle: ''},
                {lanKey: 'tif', dataField: 'tif', formatter: 'S', style: '', lanKeyAppend: 'tifType', cellStyle: ''},
                {lanKey: 'disclosedQty', dataField: 'disQty', formatter: 'I', style: '', cellStyle: ''},
                {lanKey: 'minFill', dataField: 'minQty', formatter: 'I', style: '', cellStyle: ''},
                {lanKey: 'orderValue', dataField: 'ordVal', formatter: 'C', style: '', cellStyle: ''},
                {lanKey: 'commission', dataField: 'comsn', formatter: 'C', style: '', cellStyle: ''},
                {lanKey: 'vat', dataField: 'vatAmount', formatter: 'C', style: '', cellStyle: ''},
                {lanKey: 'netValue', dataField: 'netOrdVal', formatter: 'C', style: '', cellStyle: ''},
                {lanKey: 'cumOrdVal', dataField: 'cumOrdVal', formatter: 'C', style: '', cellStyle: ''},
                {lanKey: 'symbolType', dataField: 'instruTyp', formatter: 'S', style: '', isAssetType: true, cellStyle: ''},
                {lanKey: 'settlementDate', dataField: 'adjustedSttlmntDte', formatter: 'S', style: '', cellStyle: ''}
            ]
        },

        orderSearch: {
            defaultColumnIds: ['sym', 'sDes', 'clOrdId', 'ordSts', 'ordSide', 'ordQty', 'price', 'cumQty', 'ordVal', 'commission', 'vat', 'netOrdVal', 'ordTyp', 'adjustedCrdDte'],

            moreColumnIds: ['sDes', 'clOrdId', 'ordSts', 'ordSide', 'ordQty', 'price', 'cumQty', 'pendQty', 'avgPrice', 'ordVal', 'commission', 'vat', 'netOrdVal', 'ordTyp', 'tif', 'adjustedCrdDte', 'adjustedExpTime', 'exg']
        },

        accountSummary: {
            accountSummaryFields: [     // Column Object parameters : id, width, headerCellView, headerName, sortKeyword,  cellStyle, sortDisable, dataType, type, headerStyle, cellStyle
                {lanKey: 'cashBalance', dataField: 'balance', formatter: 'C', style: ''},
                {lanKey: 'availCashBalance', dataField: 'cashAmt', formatter: 'C', style: ''},
                {lanKey: 'blockedAndOutstanding', dataField: 'blkAmt', formatter: 'C', style: ''},
                {lanKey: 'cashAvailableForWidth', dataField: 'cashForWith', formatter: 'C', style: ''},
                {lanKey: 'unsettledSales', dataField: 'penSet', formatter: 'C', style: ''},
                {lanKey: 'payableAmount', dataField: 'payAmt', formatter: 'C', style: ''},
                {lanKey: 'odLimit', dataField: 'odLmt', formatter: 'C', style: ''},
                {lanKey: 'portfolioValue', dataField: 'valuation', formatter: 'C', style: ''},
                {lanKey: 'totalPortfolio', dataField: 'totVal', formatter: 'C', style: ''},
                {lanKey: 't1BuyingPower', dataField: 't1BuyPowr', formatter: 'C', style: ''},
                {lanKey: 't2BuyingPower', dataField: 't2BuyPowr', formatter: 'C', style: ''},
                {lanKey: 't3BuyingPower', dataField: 't3BuyPowr', formatter: 'C', style: ''}
            ]
        }
    },

    priceSettings: {
        connectionParameters: {
            primary: {
                ip: 'data-sa9.mubasher.net/html5ws',
                port: '',
                secure: true
            },

            secondary: {
                ip: 'data-sa9.mubasher.net/html5ws',
                port: '',
                secure: true
            }
        },

        configs: {
            customWindowTypes: {
                SYS: {
                    include: ['213'],
                    exclude: []
                }
            }
        }
    },

    tradeSettings: {
        channelId: 30,

        connectionParameters: {
            primary: {
                ip: '59.163.249.198',
                port: '8800',
                secure: false // Is connection secure - true or false
            },

            secondary: {
                ip: '59.163.249.198',
                port: '8800',
                secure: false // Is connection secure - true or false
            }
        }
    }
};
export default (function () {
    var layout = {
        mainPanel: {
            template: 'main-panel-container-controller',
            content: [
                {
                    id: 8,
                    title: 'fairValue',
                    icon: 'glyphicon icon-analytics-chart-graph',
                    def: false,
                    rightPanel: -1,
                    tab: [
                        {
                            id: 1,
                            cache: false,
                            title: 'fairValue',
                            def: true,
                            outlet: 'price.containers.forecast-tab',
                            w: [
                                {
                                    id: 1,
                                    iw: [
                                        {id: 1, wn: 'price.widgets.fair-value.fair-value-recommendation', desc: 'fairValue', def: true, isShow: true}
                                    ]
                                }
                            ]
                        }]
                },
                {
                    id: 9,
                    title: 'theoretical',
                    icon: 'glyphicon icon-analytics-chart-graph',
                    def: false,
                    rightPanel: -1,
                    tab: [
                        {
                            id: 1,
                            cache: false,
                            title: 'theoretical',
                            def: true,
                            outlet: 'price.containers.topv-tab',
                            w: [
                                {
                                    id: 1,
                                    iw: [
                                        {id: 1, wn: 'price.widgets.forecast.theoretical-open-widgets', desc: 'stock', def: true, isShow: true}
                                    ]
                                }
                            ]
                        }]
                },
                {
                    id: 10,
                    title: 'dividends',
                    icon: 'glyphicon icon-analytics-chart-graph',
                    def: false,
                    rightPanel: -1,
                    tab: [
                        {
                            id: 1,
                            cache: false,
                            title: 'dividends',
                            def: true,
                            outlet: 'price.containers.dividends-action-tab',
                            w: [
                                {
                                    id: 1,
                                    iw: [
                                        {id: 1, wn: 'price.widgets.dividends-action', desc: 'dividend', def: true, isShow: true}
                                    ]
                                }
                            ]
                        }]
                }
            ]
        },

        rightPanel: {
            template: 'layout.right-panel-container',
            content: [
                {
                    id: 1,
                    icon: 'fa fa-bullhorn',
                    def: true,
                    wn: 'price.widgets.announcement.announcement',
                    rightPanelTitleKey: 'newsAnn'
                },
                {
                    id: 4,
                    icon: 'fa fa-eye',
                    def: false,
                    wn: 'price.widgets.watch-list.quote-watch-list',
                    rightPanelTitleKey: 'watchList'
                }
            ]
        }
    };

    var args = {
        mainPanel: {
            2: {
                tab: {
                    1: {
                        w: {
                            1: {
                                1: {selectedLink: 1}
                            },
                            2: {
                                1: {selectedLink: 1}
                            },
                            3: {
                                1: {selectedLink: 1}
                            },
                            4: {
                                1: {selectedLink: 1}
                            },
                            5: {
                                1: {mode: 1,
                                    selectedLink: 1} // DepthByPrice: 1, DepthByOrder: 2
                            },
                            6: {
                                1: {mode: 2,
                                    selectedLink: 1} // DepthByPrice: 1, DepthByOrder: 2
                            },
                            7: {
                                1: {selectedLink: 1}
                            }
                        }
                    },
                    4: {
                        w: {
                            1: {
                                1: {selectedLink: 1}
                            },
                            2: {
                                1: {selectedLink: 1}
                            },
                            3: {
                                1: {selectedLink: 1}
                            },
                            4: {
                                1: {selectedLink: 1}
                            },
                            5: {
                                1: {selectedLink: 1}
                            }
                        }
                    }
                }
            },
            9: {
                tab: {
                    1: {
                        w: {
                            1: {
                                1: {mode: 1},
                                2: {mode: 2}
                            }
                        }
                    }
                }
            },
            3: {
                tab: {
                    1: {
                        w: {
                            1: {
                                1: {mode: 1, selectedLink: 1} // TopGainersByChange, TopGainersByPercentageChange
                            },
                            2: {
                                1: {mode: 3, selectedLink: 1} //  TopLosersByChange, TopLosersByPercentageChange
                            },
                            3: {
                                1: {mode: 4, selectedLink: 1} //  MostActiveByVolume
                            },
                            4: {
                                1: {mode: 5, selectedLink: 1} // MostActiveByTrades
                            },
                            5: {
                                1: {mode: 6, selectedLink: 1} // MostActiveByValue
                            }
                        }
                    }
                }
            },
            6: {
                tab: {
                    1: {
                        w: {
                            1: {
                                1: {isClassicView: true}
                            },
                            2: {
                                1: {type: 11}
                            },
                            3: {
                                1: {type: 77}
                            }
                        }
                    },
                    2: {
                        w: {
                            1: {
                                1: {isClassicView: true}
                            }
                        }
                    },
                    3: {
                        w: {
                            1: {
                                1: {isClassicView: true}
                            }
                        }
                    }
                }
            }
        },

        rightPanel: {
            4: {selectedLink: 1}
        }
    };

    return {
        layout: layout,
        args: args
    };
});
export default {
    appConfig: {
        customisation: {
            authenticationMode: 2,
            profileServiceEnabled: false,
            isEmbeddedMode: true,
            supportedThemes: [
                {code: 'theme1', desc: 'Dark', category: 'Dark'},
                {code: 'theme2', desc: 'Light', category: 'Light'},
                {code: 'theme3', desc: 'Grey', category: 'Grey'},
                {code: 'theme4', desc: 'Crystal', category: 'Crystal'}
            ]
        },

        loggerConfig: {
            serverLogLevel: 0, // Server log is off
            consoleLogLevel: 5 // Console log level is debug
        }
    },

    priceSettings: {
        connectionParameters: {
            primary: {
                ip: '127.0.0.1',
                port: '8887/myapp',
                secure: false
            },

            secondary: {
                ip: '127.0.0.1',
                port: '8887/myapp',
                secure: false
            }
        }
    }
};

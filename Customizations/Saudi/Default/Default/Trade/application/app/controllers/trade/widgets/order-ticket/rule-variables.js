import Ember from 'ember';

export default Ember.Object.extend({
    disclosedMargin: {default: 0},
    disclosedQtyMargin: {default: 0},
    minMargin: {default: 0},
    amendAllowedStsByExg: {default: []},
    cancelAllowedStsByExg: {default: []},
    tickRule: {
        '*': {'*': 0.01},
        'TDWL': {
            '*': {10: 0.01, 25: 0.02, 50: 0.05, 100: 0.1, '*': 0.2}
        }
    },

    /* *
     * Add default values for all exchange specific variable maps
     */
    init: function () {
        var amendAllowedStsByExg = this.get('amendAllowedStsByExg');

        amendAllowedStsByExg.default = ['0', 'A', '1', '5'];
        amendAllowedStsByExg.TDWL = ['0', 'A', '1', '5'];

        var cancelAllowedStsByExg = this.get('cancelAllowedStsByExg');

        cancelAllowedStsByExg.default = ['T', 'O', '0', 'A', '1', '5'];
        cancelAllowedStsByExg.TDWL = ['T', 'O', '0', 'A', '1', '5'];

        this.disclosedQtyMargin.TDWL = 50000;
    }
}).create();

export default {
    appConfig: {
        customisation: {
            authenticationMode: 4,
            profileServiceEnabled: false,
            smartLoginEnabled: false,
            isPasswordChangeEnable: true,
            hashType: 'SHA-1'
        }
    },

    priceSettings: {
        connectionParameters: {
            primary: {
                ip: 'data-sa9.mubasher.net/html5ws',
                port: '',
                secure: true
            },

            secondary: {
                ip: 'data-sa9.mubasher.net/html5ws',
                port: '',
                secure: true
            }
        },

        configs: {
            customWindowTypes: {
                SYS: {
                    include: ['213'],
                    exclude: []
                }
            }
        }
    },

    tradeSettings: {
        channelId: 30,

        connectionParameters: {
            primary: {
                ip: 'wstrs-qa.directfn.net/trs',
                port: '',
                secure: true
            },

            secondary: {
                ip: 'wstrs-qa.directfn.net/trs',
                port: '',
                secure: true
            }
        }
    },

    tradeWidgetConfig: {
        portfolio: {
            defaultColumnIds: ['buyMore', 'liquidate', 'sym', 'sDes', 'secAccNum', 'qty', 'pendSell', 'pendBuy', 'avaiQty', 'mktPrice', 'avgCst', 'costVal', 'mktVal', 'gainLoss', 'gainLossPer', 'portPer', 'curr', 'pldQty', 'payQty', 'recQty', 'pendSubQty', 'subQty', 'wAvgPrice', 'subPrice', 'exg'],

            moreColumnIds: ['sDes', 'secAccNum', 'qty', 'pendSell', 'pendBuy', 'avaiQty', 'mktPrice', 'avgCst', 'costVal', 'mktVal', 'gainLoss', 'gainLossPer', 'portPer', 'curr', 'pldQty', 'payQty', 'recQty', 'pendSubQty', 'subQty', 'wAvgPrice', 'subPrice', 'exg']
        },

        orderDetails: {
            orderDetailsPopup: [
                {lanKey: 'exgOrderNo', dataField: 'ordId', formatter: 'S', style: '', cellStyle: ''},
                {lanKey: 'orderId', dataField: 'clOrdId', formatter: 'S', style: '', cellStyle: ''},
                {lanKey: 'side', dataField: 'ordSide', formatter: 'S', style: '', lanKeyAppend: 'orderSide', isCustomStyle: true, cellStyle: ''},
                {lanKey: 'status', dataField: 'ordSts', formatter: 'S', style: '', lanKeyAppend: 'orderStatus', isCustomStyle: true, cellStyle: ''},
                {lanKey: 'quantity', dataField: 'ordQty', formatter: 'I', style: '', cellStyle: ''},
                {lanKey: 'pendingQty', dataField: 'pendQty', formatter: 'I', style: '', cellStyle: ''},
                {lanKey: 'filledQuantity', dataField: 'cumQty', formatter: 'I', style: '', cellStyle: ''},
                {lanKey: 'orderType', dataField: 'ordTyp', formatter: 'S', style: '', lanKeyAppend: 'orderType', cellStyle: ''},
                {lanKey: 'price', dataField: 'price', formatter: 'C', style: '', cellStyle: ''},
                {lanKey: 'averagePrice', dataField: 'avgPrice', formatter: 'C', style: '', cellStyle: ''},
                {lanKey: 'stopPriceType', dataField: 'stPrcTyp', formatter: 'S', style: '', cellStyle: '', allowedOrdTypes: ['3', '4']},
                {lanKey: 'stopPrice', dataField: 'stPrc', formatter: 'C', style: '', cellStyle: '', allowedOrdTypes: ['3', '4']},
                {lanKey: 'maximumPrice', dataField: 'maxPrc', formatter: 'C', style: '', cellStyle: ''},
                {lanKey: 'date', dataField: 'adjustedCrdDte', formatter: 'S', style: '', cellStyle: ''},
                {lanKey: 'expiryDate', dataField: 'adjustedExpTime', formatter: 'S', style: '', cellStyle: ''},
                {lanKey: 'tif', dataField: 'tif', formatter: 'S', style: '', lanKeyAppend: 'tifType', cellStyle: ''},
                {lanKey: 'disclosedQty', dataField: 'disQty', formatter: 'I', style: '', cellStyle: ''},
                {lanKey: 'minFill', dataField: 'minQty', formatter: 'C', style: '', cellStyle: ''},
                {lanKey: 'orderValue', dataField: 'ordVal', formatter: 'C', style: '', cellStyle: ''},
                {lanKey: 'commission', dataField: 'comsn', formatter: 'C', style: '', cellStyle: ''},
                {lanKey: 'netValue', dataField: 'netOrdVal', formatter: 'C', style: '', cellStyle: ''},
                {lanKey: 'cumOrdVal', dataField: 'cumOrdVal', formatter: 'C', style: '', cellStyle: ''},
                {lanKey: 'symbolType', dataField: 'instruTyp', formatter: 'S', style: '', isAssetType: true, cellStyle: ''},
                {lanKey: 'settlementDate', dataField: 'adjustedSttlmntDte', formatter: 'S', style: '', cellStyle: ''}
            ]
        }
    }
};

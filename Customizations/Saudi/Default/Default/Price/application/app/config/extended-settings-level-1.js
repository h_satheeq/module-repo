export default {
    priceSettings: {
        connectionParameters: {
            primary: {
                ip: 'data-sa9.mubasher.net/html5ws',
                port: '',
                secure: true
            },

            secondary: {
                ip: 'data-sa9.mubasher.net/html5ws',
                port: '',
                secure: true
            }
        },

        configs: {
            customWindowTypes: {
                SYS: {
                    include: ['213'],
                    exclude: []
                }
            }
        }
    }
};

import utils from '../../../../utils/utils';
import BaseController from '../../../base-controller';
import Ember from 'ember';

export default BaseController.extend({
    dataObj: {},

    totVal: function () {
        var total = 0;
        var dataObject = this.get('dataObj');

        Ember.$.map(dataObject, function (item) {
            total += parseInt(item ? item : 0, 10);
        });

        return total;
    }.property('dataObj.sharesHighRisk', 'dataObj.sharesMedRisk', 'dataObj.sharesLowRisk', 'dataObj.debtInstrmntsHighRisk', 'dataObj.debtInstrmntsMedRisk', 'dataObj.debtInstrmntsLowRisk', 'dataObj.invstmntFundHighRisk', 'dataObj.invstmntFundMedRisk', 'dataObj.invstmntFundLowRisk', 'dataObj.tradeFinanceLowRisk', 'dataObj.tradeFinanceHighRisk', 'dataObj.tradeFinanceMedRisk', 'dataObj.commdtsHighRisk', 'dataObj.commdtsLowRisk', 'dataObj.commdtsMedRisk', 'dataObj.optionsHighRisk', 'dataObj.optionsMedRisk', 'dataObj.optionsLowRisk'),

    onPrepareData: function () {
        var businessObj = this.get('businessObj');

        this.set('dataObj.sharesHighRisk', businessObj ? businessObj.get('sharesHighRisk') : 0);
        this.set('dataObj.sharesMedRisk', businessObj ? businessObj.get('sharesMedRisk') : 0);
        this.set('dataObj.debtInstrmntsHighRisk', businessObj ? businessObj.get('debtInstrmntsHighRisk') : 0);
        this.set('dataObj.debtInstrmntsMedRisk', businessObj ? businessObj.get('debtInstrmntsMedRisk') : 0);
        this.set('dataObj.debtInstrmntsLowRisk', businessObj ? businessObj.get('debtInstrmntsLowRisk') : 0);
        this.set('dataObj.invstmntFundHighRisk', businessObj ? businessObj.get('invstmntFundHighRisk') : 0);
        this.set('dataObj.invstmntFundMedRisk', businessObj ? businessObj.get('invstmntFundMedRisk') : 0);
        this.set('dataObj.invstmntFundLowRisk', businessObj ? businessObj.get('invstmntFundLowRisk') : 0);
        this.set('dataObj.tradeFinanceLowRisk', businessObj ? businessObj.get('tradeFinanceLowRisk') : 0);
        this.set('dataObj.commdtsHighRisk', businessObj ? businessObj.get('commdtsHighRisk') : 0);
        this.set('dataObj.optionsHighRisk', businessObj ? businessObj.get('optionsHighRisk') : 0);
        this.set('dataObj.sharesLowRisk', businessObj ? businessObj.get('sharesLowRisk') : 0);
        this.set('dataObj.tradeFinanceHighRisk', businessObj ? businessObj.get('tradeFinanceHighRisk') : 0);
        this.set('dataObj.tradeFinanceMedRisk', businessObj ? businessObj.get('tradeFinanceMedRisk') : 0);
        this.set('dataObj.commdtsMedRisk', businessObj ? businessObj.get('commdtsMedRisk') : 0);
        this.set('dataObj.commdtsLowRisk', businessObj ? businessObj.get('commdtsLowRisk') : 0);
        this.set('dataObj.optionsMedRisk', businessObj ? businessObj.get('optionsMedRisk') : 0);
        this.set('dataObj.optionsLowRisk', businessObj ? businessObj.get('optionsLowRisk') : 0);
    },

    getDataObject: function () {
        return this.get('dataObj');
    },

    onValidate: function () {
        var isRequiredFieldsAvailable = true;
        var errorCollection = [];

        var dataObject = this.get('dataObj');
        var app = this.get('app');

        if (!utils.validators.isAvailable(dataObject.sharesHighRisk) || !utils.validators.isAvailable(dataObject.sharesMedRisk) || !utils.validators.isAvailable(dataObject.sharesLowRisk)) {
            isRequiredFieldsAvailable = false;
            this.set('sharesRiskColor', 'down-fore-color');
        } else {
            this.set('sharesRiskColor', '');
        }

        if (!utils.validators.isAvailable(dataObject.debtInstrmntsHighRisk) || !utils.validators.isAvailable(dataObject.debtInstrmntsMedRisk) || !utils.validators.isAvailable(dataObject.debtInstrmntsLowRisk)) {
            isRequiredFieldsAvailable = false;
            this.set('debtInstRiskColor', 'down-fore-color');
        } else {
            this.set('debtInstRiskColor', '');
        }

        if (!utils.validators.isAvailable(dataObject.invstmntFundHighRisk) || !utils.validators.isAvailable(dataObject.invstmntFundMedRisk) || !utils.validators.isAvailable(dataObject.invstmntFundLowRisk)) {
            isRequiredFieldsAvailable = false;
            this.set('investFundRiskColor', 'down-fore-color');
        } else {
            this.set('investFundRiskColor', '');
        }

        if (!utils.validators.isAvailable(dataObject.tradeFinanceHighRisk) || !utils.validators.isAvailable(dataObject.tradeFinanceMedRisk) || !utils.validators.isAvailable(dataObject.tradeFinanceLowRisk)) {
            isRequiredFieldsAvailable = false;
            this.set('tradeFinanceRiskColor', 'down-fore-color');
        } else {
            this.set('tradeFinanceRiskColor', '');
        }

        if (!utils.validators.isAvailable(dataObject.commdtsHighRisk) || !utils.validators.isAvailable(dataObject.commdtsMedRisk) || !utils.validators.isAvailable(dataObject.commdtsLowRisk)) {
            isRequiredFieldsAvailable = false;
            this.set('commdtsRiskColor', 'down-fore-color');
        } else {
            this.set('commdtsRiskColor', '');
        }

        if (!utils.validators.isAvailable(dataObject.optionsHighRisk) || !utils.validators.isAvailable(dataObject.optionsMedRisk) || !utils.validators.isAvailable(dataObject.optionsLowRisk)) {
            isRequiredFieldsAvailable = false;
            this.set('optionsRiskColor', 'down-fore-color');
        } else {
            this.set('optionsRiskColor', '');
        }

        if (this.get('totVal') !== 100) {
            errorCollection.pushObject(app.lang.messages.totalHundredMessage);
        }

        if (!isRequiredFieldsAvailable) {
            errorCollection.pushObject(app.lang.messages.mandatoryFields);
        }

        return errorCollection;
    }
});
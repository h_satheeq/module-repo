/* jshint node: true */
var EnvironmentExtended = require('./environment-extended.js');

module.exports = function (environment) {
    var ENV = new EnvironmentExtended(environment);
    ENV.baseURL = '/';
	ENV.locationType = 'none';

    ENV.contentSecurityPolicy['connect-src'] = "'self' www.google-analytics.com wss://data-sa9.mubasher.net/html5ws wss://online5.icap.com.sa/trs";

    if (ENV.APP.isTestMode) {
        ENV.APP.tradeConnectionParameters = {
            primary: {
                ip: 'icap-wstrs-qa.directfn.net/trs',
                port: '',
                secure: true
            }
        };

        ENV.APP.priceConnectionParameters = {
            primary: {
                ip: 'data-sa9.mubasher.net/html5ws',
                port: '',
                secure: true
            }
        };

        ENV.contentSecurityPolicy['connect-src'] = "'self' www.google-analytics.com wss://data-sa9.mubasher.net/html5ws wss://icap-wstrs-qa.directfn.net/trs www.gstatic.com/recaptcha";
        ENV.contentSecurityPolicy['script-src'] = "'self' 'unsafe-inline' 'unsafe-eval' www.google-analytics.com https://www.google.com/recaptcha/api.js https://www.gstatic.com/recaptcha/api2/";
        ENV.contentSecurityPolicy['frame-src'] = "'self' http://dfnnet.directfn.com/ESTORERIA/Agreement/en/EULA.htm https://www.google.com/recaptcha/api2/";

        ENV.gReCaptcha = {
            siteKey: '6LdPCDUUAAAAAFOw-Se3J7Oqpau6Z7iO4QH9H8Ju'
        };
    }

    return ENV;
};

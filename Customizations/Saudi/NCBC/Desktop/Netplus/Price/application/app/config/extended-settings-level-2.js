export default {
	appConfig: {
		customisation: {
			clientPrefix: 'ncbc',

			loginViewSettings: {
				isPoweredByEnabled: true
			}
		}
	}
};

/* jshint node: true */
var EnvironmentCore = require('./environment-core.js');

module.exports = function (environment) {
    var ENV = new EnvironmentCore(environment);

    if (environment === 'development' || 'automation') {
        ENV.APP.tradeConnectionParameters = {
            primary: {
                ip: '172.22.140.169',
                port: '9994',
                secure: false
            }
        };

        ENV.APP.priceConnectionParameters = {
            primary: {
                ip: 'data-sa9.mubasher.net/html5ws',
                port: '',
                secure: true
            }
        };

        ENV.contentSecurityPolicy['connect-src'] = "'self' www.google-analytics.com wss://data-sa9.mubasher.net/html5ws ws://172.22.140.169:9994";
    }

    return ENV;
};

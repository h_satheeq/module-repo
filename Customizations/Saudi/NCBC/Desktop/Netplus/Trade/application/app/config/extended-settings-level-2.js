export default {
	appConfig: {
		customisation: {
			clientPrefix: 'ncbc'
		},

		loginViewSettings: {
            isForgotPasswordEnabled: true,
            isRegisterEnabled: true,
            isOnlineAccEnabled: true
        }
	},
	
	tradeSettings: {
        channelId: -1,

        connectionParameters: {
            primary: {
                ip: '172.22.140.169',
                port: '9994',
                secure: false
            },

            secondary: {
                ip: '172.22.140.169',
                port: '9994',
                secure: false
            }
        }
    }
};

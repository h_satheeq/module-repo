export default {
	appConfig: {
		customisation: {
			clientPrefix: 'riyad',

			loginViewSettings: {
				isPoweredByEnabled: true
			}
		}
	}
};

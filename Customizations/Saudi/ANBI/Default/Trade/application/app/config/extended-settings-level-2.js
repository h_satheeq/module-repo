export default {
	tradeSettings: {
        channelId: 30,

        connectionParameters: {
            primary: {
                ip: '192.168.17.33:8090/trs',
                port: '',
                secure: false
            },

            secondary: {
                ip: '192.168.17.33:8090/trs',
                port: '',
                secure: false
            }
        }
    }
};

export default {
    appConfig: {
        customisation: {
            authenticationMode: 4,
            isTradingEnabled: true,
            hashType: 'MD5',
            isUpperCasePassword: true,
            isDiscloseQtyEnabled: true,
            smartLoginEnabled: false,

            loginViewSettings: {
                isSignUpEnabled: false,
                isForgotPasswordEnabled: true,
                isPoweredByEnabled: true,
                isRegisterEnabled: true
            },

            passwordRules: {
                maxLength: 18,
                minLength: 8,
                checkLength: true,
                checkContainNumber: true,
                checkStartWithLetter: true,
                checkContainSmallLetter: true,
                checkContainCapitalLetter: true,
                checkIdenticalCharacter: true,
                checkConsecutiveCharacter: true,
                checkSpecialCharacter: true,
                checkUsernameMatch: true
            },

            usernameRules: {
                maxLength: 12,
                minLength: 6,
                checkLength: true,
                checkEngAlphaNumeric: true,
                checkStartWithLetter: true,
                checkIdenticalCharacter: true
            }
        },

        widgetId: {
            quoteMenuId: 'fullQuote',
            watchListMenuId: 'heatMap',
            marketMenuId: 'market',
            indicesTabId: 'companyProf',
            topStocksTabId: 'topStocks',
            portfolioMenuId: 'standard',
            orderTicketMenuId: 'classic'
        },

        mutualFund: {
            /*eslint-disable */
            agreementPDF_EN: 'http://www.alistithmarcapital.com/pdfs/120-SAIB-Saudi-Companies-Fund.pdf',
            agreementPDF_AR: 'http://www.alistithmarcapital.com/pdfs/120-SAIB-Saudi-Companies-Fund.pdf'
            /*eslint-enable */
        }
    },

    tradeSettings: {
        channelId: 30,

        connectionParameters: {
            primary: {
                ip: '192.168.17.33',
                port: '8090/trs',
                secure: false
            },

            secondary: {
                ip: '192.168.17.33',
                port: '8090/trs',
                secure: false
            }
        }
    },

    priceWidgetConfig: {
        optionChain: {
            defaultColumns: [     // Column Object parameters : id, width, headerName, sortKeyword, multiValueIds, cellStyle, sortDisable, firstValueStyle, isBold, dataType, backgroundColour,
                {id: 'cStock.bbp', width: 40, headerName: 'callBestBid', headerStyle: 'text-left-header', type: 'classicCell', firstValueStyle: 'up-fore-color bold', backgroundStyle: 'watchlist-cell-back-green', blinkUpStyle: 'blink-classic-up', blinkDownStyle: 'blink-classic-down', isBlink: true, dataType: 'float'},
                {id: 'cStock.bap', width: 40, headerName: 'callBestAsk', headerStyle: 'text-left-header', type: 'classicCell', firstValueStyle: 'down-fore-color bold', backgroundStyle: 'watchlist-cell-back-red', sortKeyword: 'bap', blinkUpStyle: 'blink-classic-up', blinkDownStyle: 'blink-classic-down', isBlink: true, dataType: 'float'},
                {id: 'strkPrc', width: 60, headerName: 'strikePrice', headerStyle: 'text-left-header', dataType: 'float', firstValueStyle: 'highlight-fore-color bold h-middle', backgroundStyle: 'watchlist-cell-back-lastqty', blinkUpStyle: 'blink-classic-up', blinkDownStyle: 'blink-classic-down', isBlink: true, type: 'classicCell'},
                {id: 'pStock.bbp', width: 40, headerName: 'putBestBid', headerStyle: 'text-left-header', type: 'classicCell', firstValueStyle: 'up-fore-color bold', backgroundStyle: 'watchlist-cell-back-green', blinkUpStyle: 'blink-classic-up', blinkDownStyle: 'blink-classic-down', isBlink: true, dataType: 'float'},
                {id: 'pStock.bap', width: 40, headerName: 'putBestAsk', headerStyle: 'text-left-header', type: 'classicCell', firstValueStyle: 'down-fore-color bold', backgroundStyle: 'watchlist-cell-back-red', sortKeyword: 'bap', blinkUpStyle: 'blink-classic-up', blinkDownStyle: 'blink-classic-down', isBlink: true, dataType: 'float'}
            ]
        },

        chartIndicators: ['AccumulationDistribution', 'AverageTrueRange', 'BollingerBands', 'ChaikinMF', 'ChandeMomentumOscillator', 'MoneyFlowIndex', 'MovingAverage', 'PSAR', 'RelativeStrengthIndex', 'TimeSeriesForecast', 'TRIX', 'VolOsc', 'WildersSmoothing', 'WilliamsPerR']
    }
};

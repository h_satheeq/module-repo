/* jshint node: true */
var EnvironmentExtended = require('./environment-extended.js');

module.exports = function (environment) {
    var ENV = new EnvironmentExtended(environment);
    ENV.baseURL = '/';
	ENV.locationType = 'none';

    ENV.contentSecurityPolicy['connect-src'] = "'self' www.google-analytics.com wss://data-sa9.mubasher.net/html5ws ws://192.168.17.33:8090/trs";

    if (ENV.APP.isTestMode || environment === 'development' || 'automation') {
        ENV.APP.tradeConnectionParameters = {
            primary: {
                ip: '192.168.17.33:8090/trs',
                port: '',
                secure: false
            }
        };

        ENV.APP.priceConnectionParameters = {
            primary: {
                ip: 'data-sa9.mubasher.net/html5ws',
                port: '',
                secure: true
            }
        };

        ENV.contentSecurityPolicy['connect-src'] = "'self' www.google-analytics.com wss://data-sa9.mubasher.net/html5ws wss://192.168.17.33:8090/trs";

        ENV.gReCaptcha = {
            // Domains included: localhost, saudi-dev-riaweb.universal.directfn.net, saudi-qa-riaweb.universal.directfn.net
            siteKey: '6LfVBw4UAAAAAM4SQ1bESrYwhO7fIojAw7utbYUl'
        };
    }

    return ENV;
};

export default {
    appConfig: {
        customisation: {
            clientPrefix: 'mIcap',
            profileServiceEnabled: false,
            isMobile: true,

            supportedContacts: [
                {key: 'phn', value: '+9661 254-7666', type: 'T'},
                {key: 'phn', value: '+9661 489-2653', type: 'T'},
                {key: 'phn', value: '(800) 124-8282', type: 'T'},
                {key: 'email', value: 'info@alistithmarcapital.com', type: 'E'}
            ]
        },

        loggerConfig: {
            serverLogLevel: 0,
            consoleLogLevel: 3
        },

        widgetId: {
            quoteMenuId: 'fullQuote',
            watchListMenuId: 'heatMap',
            marketMenuId: 'market',
            indicesTabId: 'companyProf',
            topStocksTabId: 'topStocks'
        }
    },

    // TODO [Arosha] Remove this after done this from responsive class
    priceWidgetConfig: {
        watchList: {
            quoteColumns: [
                {id: 'contextMenu', width: 15, name: 'contextMenu', headerName: '', headerStyle: 'text-center', iconClass: 'glyphicon glyphicon-chevron-right', isColumnSortDisabled: true, type: 'contextMenuMobile', buttonFunction: 'showOrderTicket'},
                {id: 'dSym', width: 80, headerCellView: 'Ember.MoreHeaderCell', headerName: 'symbol', headerSecondName: '', headerThirdName: 'description', secondId: 'lDes', thirdId: 'open', headerStyle: 'text-left-header', sortKeyword: 'sDes', type: 'expandedSymbolMobile', expandedWidthRatio: '0.5', defaultWidthRatio: 16 / 37, isndicatorAvailable: true},
                {id: 'ltp', width: 45, headerCellView: 'Ember.ExpandedHeaderCell', headerName: 'last', headerSecondName: '', headerThirdName: 'volume', secondId: 'vol', thirdId: 'low', sortKeyword: 'ltp', dataType: 'float', firstValueStyle: 'highlight-fore-color bold', type: 'expandedLtpMobile', noOfSecValueDecimalPlaces: 0, expandedWidthRatio: '0.25', blinkUpStyle: 'up-back-color btn-txt-color', blinkDownStyle: 'down-back-color btn-txt-color', isBlink: true, defaultWidthRatio: 9 / 37},
                {id: 'pctChg', width: 60, headerCellView: 'Ember.ExpandedHeaderCell', headerName: 'perChange', headerSecondName: '', headerThirdName: 'change', headerStyle: 'pad-m-r', secondId: 'chg', thirdId: 'prvCls', sortKeyword: 'chg', positiveNegativeChange: true, type: 'expandedChgMobile', dataType: 'float', expandedWidthRatio: '0.25', defaultWidthRatio: 12 / 37}]
        },

        indices: {
            columns: [
                {id: 'dSym', width: 80, headerCellView: 'Ember.MoreHeaderCell', headerName: 'index', headerSecondName: '', headerThirdName: 'description', secondId: 'lDes', thirdId: 'open', headerStyle: 'text-left-header', sortKeyword: 'sDes', type: 'expandedSymbolMobile', expandedWidthRatio: '0.5', defaultWidthRatio: 16 / 37, isndicatorAvailable: true},
                {id: 'ltp', width: 45, headerCellView: 'Ember.ExpandedHeaderCell', headerName: 'last', headerSecondName: '', headerThirdName: 'volume', secondId: 'vol', thirdId: 'low', sortKeyword: 'ltp', dataType: 'float', firstValueStyle: 'highlight-fore-color bold', type: 'expandedLtpMobile', noOfSecValueDecimalPlaces: 0, expandedWidthRatio: '0.25', defaultWidthRatio: 9 / 37},
                {id: 'pctChg', width: 60, headerCellView: 'Ember.ExpandedHeaderCell', headerName: 'perChange', headerSecondName: '', headerThirdName: 'change', headerStyle: 'pad-m-r', secondId: 'chg', thirdId: 'prvCls', sortKeyword: 'chg', positiveNegativeChange: true, type: 'expandedChgMobile', dataType: 'float', expandedWidthRatio: '0.25', defaultWidthRatio: 12 / 37}]
        },

        topStocks: {
            // TopGainersByPercentageChange
            1: {
                fields: [
                    {filed: 'description', objName: 'dDesc', fontColor: 'fore-color', textAlign: 'h-left', col: 'layout-col-24', padding: 'pad-m-l'},
                    // {filed: 'symbol', objName: 'dSym', fontColor: 'symbol-fore-color', textAlign: 'h-left ', bold: 'bold', padding: 'pad-widget-left'},
                    {filed: 'last', objName: 'ltp', fontColor: 'highlight-fore-color', textAlign: 'h-right', formatter: 'formatNumberWithDeci', padding: 'pad-m-l pad-widget-right'},
                    {filed: 'change', objName: 'chg', fontColor: 'redOrGreen', textAlign: 'h-right', formatter: 'formatNumberWithDeci', padding: 'pad-m-l pad-widget-right'},
                    {filed: 'perChange', objName: 'pctChg', fontColor: 'redOrGreen', textAlign: 'h-right', formatter: 'formatNumberPercentage', bold: 'bold', padding: 'pad-m-l pad-widget-right'}
                ],
                title: 'topGainers',
                icon: 'glyphicon-triangle-top up-fore-color',
                showTopStockTabs: true
            },
            // TopLosersByPercentageChange
            3: {
                fields: [
                    {filed: 'description', objName: 'dDesc', fontColor: 'fore-color', textAlign: 'h-left', col: 'layout-col-24', padding: 'pad-m-l'},
                    // {filed: 'symbol', objName: 'dSym', fontColor: 'symbol-fore-color', textAlign: 'h-left', bold: 'bold', padding: 'pad-widget-left'},
                    {filed: 'last', objName: 'ltp', fontColor: 'highlight-fore-color', textAlign: 'h-right', formatter: 'formatNumberWithDeci', padding: 'pad-m-l pad-widget-right'},
                    {filed: 'change', objName: 'chg', fontColor: 'redOrGreen', textAlign: 'h-right', formatter: 'formatNumberWithDeci', padding: 'pad-m-l pad-widget-right'},
                    {filed: 'perChange', objName: 'pctChg', fontColor: 'redOrGreen', textAlign: 'h-right', formatter: 'formatNumberPercentage', bold: 'bold', padding: 'pad-m-l pad-widget-right'}
                ],
                title: 'topLosers',
                icon: 'glyphicon-triangle-bottom  down-fore-color',
                showTopStockTabs: true
            },
            // MostActiveByVolume
            4: {
                fields: [
                    {filed: 'description', objName: 'dDesc', fontColor: 'fore-color', textAlign: 'h-left', col: 'layout-col-24', padding: 'pad-m-l'},
                    // {filed: 'symbol', objName: 'dSym', fontColor: 'highlight-fore-color', textAlign: 'h-left', bold: 'bold', padding: 'pad-widget-left'},
                    {filed: 'last', objName: 'ltp', fontColor: 'fore-color', textAlign: 'h-right', formatter: 'formatNumberWithDeci', padding: 'pad-m-l pad-widget-right'},
                    // {filed: 'change', objName: 'chg', fontColor: 'redOrGreen', textAlign: 'h-right', formatter: 'formatNumberWithDeci', padding: 'pad-m-l'},
                    {filed: 'perChange', objName: 'pctChg', fontColor: 'redOrGreen', textAlign: 'h-right', formatter: 'formatNumberPercentage', padding: 'pad-m-l pad-widget-right'},
                    {filed: 'volume', objName: 'vol', fontColor: 'highlight-fore-color', textAlign: 'h-right', formatter: 'formatNumber', bold: 'bold', padding: 'pad-m-l pad-widget-right'}

                ],
                title: 'MActiveByVol',
                icon: 'glyphicon-signal  fade-fore-color',
                showTopStockTabs: false
            },
            // MostActiveByTrades
            5: {
                fields: [
                    {filed: 'description', objName: 'dDesc', fontColor: 'fore-color', textAlign: 'h-left', col: 'layout-col-24', padding: 'pad-m-l'},
                    // {filed: 'symbol', objName: 'dSym', fontColor: 'symbol-fore-color', textAlign: 'h-left', bold: 'bold', padding: 'pad-widget-left'},
                    {filed: 'last', objName: 'ltp', fontColor: 'highlight-fore-color', textAlign: 'h-right', formatter: 'formatNumberWithDeci', padding: 'pad-m-l pad-widget-right'},
                    {filed: 'perChange', objName: 'pctChg', fontColor: 'redOrGreen', textAlign: 'h-right', formatter: 'formatNumberPercentage', padding: 'pad-m-l pad-widget-right'},
                    {filed: 'trades', objName: 'trades', fontColor: 'highlight-fore-color', textAlign: 'h-right', formatter: 'formatNumber', bold: 'bold', padding: 'pad-m-l pad-widget-right'}
                ],
                title: 'MActiveByTrades',
                icon: 'glyphicon glyphicon-transfer fore-color',
                showTopStockTabs: false
            },
            // MostActiveByTurnover
            6: {
                fields: [
                    {filed: 'description', objName: 'dDesc', fontColor: 'fore-color', textAlign: 'h-left', col: 'layout-col-24', padding: 'pad-m-l'},
                    // {filed: 'symbol', objName: 'dSym', fontColor: 'highlight-fore-color', textAlign: 'h-left', bold: 'bold', padding: 'pad-widget-left'},
                    {filed: 'last', objName: 'ltp', fontColor: 'fore-color', textAlign: 'h-right', formatter: 'formatNumberWithDeci', padding: 'pad-m-l pad-widget-right'},
                    // {filed: 'change', objName: 'chg', fontColor: 'redOrGreen', textAlign: 'h-right', formatter: 'formatNumberWithDeci', padding: 'pad-m-l'},
                    {filed: 'perChange', objName: 'pctChg', fontColor: 'redOrGreen', textAlign: 'h-right', formatter: 'formatNumberPercentage', padding: 'pad-m-l pad-widget-right'},
                    {filed: 'turnover', objName: 'tovr', fontColor: 'highlight-fore-color', textAlign: 'h-right', formatter: 'formatNumber', bold: 'bold', padding: 'pad-m-l pad-widget-right'}
                ],
                title: 'MActiveByTurnover',
                icon: 'glyphicon-usd fade-fore-color',
                showTopStockTabs: false
            }
        },

        timeAndSales: {
            defaultColumnIds: ['dDt', 'trp', 'trq', 'nChg', 'splits', 'tick', 'trdType']
        }
    }
};

export default {
    appConfig: {
        customisation: {
            authenticationMode: 4,
            hashType: 'MD5',
            isUpperCasePassword: true,
            isCustomWorkSpaceEnabled: true
        }
    },

    tradeSettings: {
        channelId: 30,

        connectionParameters: {
            primary: {
                ip: '192.168.17.33:8090/trs',
                port: '',
                secure: false
            },

            secondary: {
                ip: '192.168.17.33:8090/trs',
                port: '',
                secure: false
            }
        }
    },

    priceWidgetConfig: {
        WidgetList: {
            trade: [
                {code: 'trade.widgets.account-summary', des: 'Account Summary', desc: 'accountSummary', icon: 'glyphicon glyphicon-tags'},
                {code: 'trade.widgets.order-list', des: 'Order List', desc: 'orderList', icon: 'glyphicon glyphicon-list-alt'},
                {code: 'trade.widgets.order-search', des: 'Order Search', desc: 'orderSearch', icon: 'glyphicon glyphicon-search'},
                {code: 'trade.widgets.order-ticket.order-ticket-landscape', des: 'Order Ticket (L)', desc: 'orderTicket', icon: 'glyphicon glyphicon-send'},
                {code: 'trade.widgets.portfolio', des: 'Portfolio', desc: 'portfolio', icon: 'glyphicon glyphicon-duplicate'},
                {code: 'trade.widgets.rights-subscription-list.rights-subscription-list', des: 'Rights Subscription List', desc: 'rightsSubscriptionList', icon: 'glyphicon glyphicon-list-alt'},
                {code: 'trade.widgets.saved-orders', des: 'Saved Orders', desc: 'savedOrders', icon: 'glyphicon glyphicon-saved'},
                {code: 'trade.widgets.tadawulity', des: 'Tadawulity', desc: 'tdwlCart', icon: 'glyphicon glyphicon-certificate'}
                // {code: 'trade.widgets.rapid-order', des: 'Rapid Order', desc: 'rapidOrder', icon: 'glyphicon icon-sitemap'},
                // {code: 'trade.widgets.panic-withdrawal', des: 'Panic Withdrawal', desc: 'panicWithdrawal', icon: 'glyphicon icon-sitemap'},
                // {code: 'trade.widgets.consolidated-reports', des: 'Consolidated Reports', desc: 'consolidatedReports', icon: 'glyphicon icon-sitemap'},
            ],

            transfer: [
                {code: 'trade.widgets.account-statement.account-statement', des: 'Account Statement', desc: 'accStatement', icon: 'glyphicon glyphicon-file'},
                {code: 'trade.widgets.cash-transfer', des: 'Bank Transfer', desc: 'bankTransfer', icon: 'glyphicon glyphicon-transfer'},
                {code: 'trade.widgets.transaction-history', des: 'Cash Transfer History', desc: 'cashTransferHistory', icon: 'glyphicon glyphicon-book'},
                {code: 'trade.widgets.product-subscription.product-subscription', des: 'Product Subscription', desc: 'productSubscription', icon: 'glyphicon glyphicon-hand-up'},
                {code: 'trade.widgets.cash-transfer-portfolios', des: 'Transfer between Investment Accounts', desc: 'transferBetweenPortfolios', icon: 'glyphicon glyphicon-transfer'}
            ],

            mutualFund: [
                {code: 'trade.widgets.mutual-fund.mutual-fund-holdings', des: 'Mutual Fund Holdings', desc: 'fundHoldings', icon: 'glyphicon glyphicon-tag'},
                {code: 'trade.widgets.mutual-fund.mutual-fund-list', des: 'Mutual Fund List', desc: 'fundList', icon: 'glyphicon glyphicon-list-alt'},
                {code: 'trade.widgets.mutual-fund.mutual-fund-transactions', des: 'Mutual Fund Transactions', desc: 'transactions', icon: 'glyphicon glyphicon-transfer'}
            ],

            optionChain: [
                {code: 'price.widgets.option-chain', des: 'Option Chain', desc: 'optionChain', icon: 'glyphicon glyphicon-link'},
                {code: 'trade.widgets.option-trading.option-order-list', des: 'Option Order List', desc: 'optionOrderList', icon: 'glyphicon glyphicon-list-alt'}
                // {code: 'trade.widgets.order-ticket.order-ticket-portrait', des: 'Order Ticket (P)', desc: 'orderTicket', icon: 'glyphicon glyphicon-send'}
            ],

            userProfile: [
                // {code: 'trade.widgets.kyc.kyc-update', des: 'KYC Update', desc: 'kycUpdate', icon: 'glyphicon glyphicon-edit'}
            ]
        }
    }
};

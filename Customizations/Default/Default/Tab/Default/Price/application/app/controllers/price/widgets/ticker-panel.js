import Ember from 'ember';
import sharedService from '../../../models/shared/shared-service';
import BaseController from '../../base-controller';
import priceTicker from '../../price/top-panel/price-ticker';
import appConfig from '../../../config/app-config';

export default BaseController.extend({
    subscriptionKey: 'tickerPanel',
    priceService: sharedService.getService('price'),

    isTablet: function () {
        return appConfig.customisation.isTablet;
    }.property(),

    onLoadWidget: function () {
        this.set('exchange', this.priceService.exchangeDS.getExchange(this.get('exg')));
    },

    onPrepareData: function () {
        this._callComponentsMethod('onPrepareData');
    },

    onAddSubscription: function () {
        this._callComponentsMethod('onAddSubscription');
    },

    onClearData: function () {
        this.set('exchange', {});
        this._callComponentsMethod('onClearData');
    },

    onRemoveSubscription: function () {
        this._callComponentsMethod('onRemoveSubscription');
    },

    onLanguageChanged: function () {
        this._callComponentsMethod('onLanguageChanged');
    },

    onThemeChanged: function () {
        this._callComponentsMethod('onThemeChanged');
    },

    onVisibilityChanged: function () {
        this._callComponentsMethod('onVisibilityChanged');
    },

    _callComponentsMethod: function (method) {
        var priceTickerComponent = Ember.View.views['price-top-panel-price-ticker'];

        try {
            if (priceTickerComponent) {
                priceTickerComponent.send(method, this.get('exg'));
            }
        } catch (e) {
            this.utils.logger.logError(e);
        }
    }
});

Ember.Handlebars.helper('price-ticker', priceTicker);

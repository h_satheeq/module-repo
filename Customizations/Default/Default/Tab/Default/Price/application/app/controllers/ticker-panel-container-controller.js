import WidgetContainerController from './widget-container-controller';

export default WidgetContainerController.extend({
    // Subscription key
    containerKey: 'tickerPanel',

    getLastActiveMenu: function () {
        return undefined;
    },

    getLastActiveTab: function () {
        return undefined;
    }
});

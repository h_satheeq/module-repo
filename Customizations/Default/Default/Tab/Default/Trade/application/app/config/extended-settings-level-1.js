export default {
    appConfig: {
        customisation: {
            authenticationMode: 4,
            profileServiceEnabled: false,
            smartLoginEnabled: false,
            isPasswordChangeEnable: true,
            hashType: 'SHA-1',
			isTablet: true
        }
    },

    priceSettings: {
        connectionParameters: {
            primary: {
                ip: 'data-sa9.mubasher.net/html5ws',
                port: '',
                secure: true
            },

            secondary: {
                ip: 'data-sa9.mubasher.net/html5ws',
                port: '',
                secure: true
            }
        },

        configs: {
            customWindowTypes: {
                SYS: {
                    include: ['213'],
                    exclude: []
                }
            }
        }
    },

    priceWidgetConfig: {
        watchList: {
            defaultColumnIds: ['sym', 'trend', 'ltp', 'chg', 'bbp', 'bap', 'l52', 'vol', 'tovr', 'trades', 'prvCls', 'cit', 'dltt', 'high'],

            classicColumnIds: ['sym', 'sDes', 'trend', 'ltp', 'ltq', 'chg', 'pctChg', 'vol', 'bbp', 'bbq', 'bap', 'baq', 'trades', 'ltd', 'dltt', 'intsV', 'top', 'tov', 'open', 'high', 'low'],

            moreColumnIds: ['trend', 'ltp', 'chg', 'bbp', 'bap', 'l52', 'vol', 'tovr', 'trades', 'prvCls', 'cit', 'dltt', 'high', 'cvwap', 'max', 'top', 'tcp', 'twap', 'vwap'],

            classicMoreColumnIds: ['sDes', 'lDes', 'trend', 'isin', 'instDes', 'ltp', 'ltq', 'ltd', 'dltt', 'prvCls', 'chg', 'pctChg', 'vol', 'tovr', 'open', 'high', 'low', 'cls', 'bbp', 'bbq', 'bap', 'baq', 'h52', 'l52', 'trades', 'refValue', 'max', 'min', 'tbq', 'taq', 'eps', 'per', 'intsV', 'top', 'tov', 'cit', 'cvwap', 'twap', 'tcp', 'tcv', 'vwap'],

            customDefaultColumnIds: ['sym', 'exg', 'trend', 'ltp', 'chg', 'bbp', 'bap', 'l52', 'vol', 'tovr', 'trades', 'prvCls', 'cit', 'dltt', 'high'],

            customClassicColumnIds: ['sym', 'sDes', 'exg', 'trend', 'ltp', 'ltq', 'chg', 'pctChg', 'bbp', 'bbq', 'bap', 'baq', 'vol', 'tovr', 'trades', 'cit', 'h52', 'l52', 'prvCls', 'open', 'ltd', 'dltt', 'high', 'low'],

            nonRemovableColumnIds: ['sym'],

            classicAssetTypes: {
                0: ['sym', 'sDes', 'trend', 'ltp', 'ltq', 'chg', 'pctChg', 'bbp', 'bbq', 'bap', 'baq', 'vol', 'tovr', 'trades', 'h52'],    // Equity
                75: ['sym', 'sDes', 'trend', 'ltp', 'ltq', 'chg', 'pctChg', 'bbp', 'bbq', 'bap', 'baq', 'vol', 'tovr', 'trades', 'prvCls', 'open', 'high', 'low'],    // Bonds (Fixed Income)
                68: ['sym', 'sDes', 'trend', 'ltp', 'ltq', 'chg', 'pctChg', 'bbp', 'bbq', 'bap', 'baq', 'vol', 'tovr', 'trades', 'high', 'low'],  // Options/Future
                86: ['sym', 'sDes', 'trend', 'ltp', 'ltq', 'chg', 'pctChg', 'bbp', 'bbq', 'bap', 'baq', 'vol', 'tovr', 'trades', 'high', 'low'],  // ETF (Mutual Funds)
                11: ['sym', 'sDes', 'trend', 'ltp', 'ltq', 'chg', 'pctChg', 'bbp', 'bbq', 'bap', 'baq', 'vol', 'tovr', 'trades', 'high', 'low'],  // Currency
                7: ['sym', 'sDes', 'trend', 'ltp', 'chg', 'pctChg', 'high', 'low', 'vol', 'tovr', 'trades', 'h52', 'l52', 'prvCls', 'open']  // Index
            },

            assetTypes: {
                0: ['sym', 'trend', 'ltp', 'chg', 'bbp', 'bap', 'l52', 'vol', 'tovr', 'trades', 'prvCls', 'cit', 'dltt', 'high'],    // Equity
                75: ['sym', 'trend', 'ltp', 'chg', 'bbp', 'bap', 'vol', 'tovr', 'trades', 'prvCls', 'high'],    // Bonds (Fixed Income)
                68: ['sym', 'trend', 'ltp', 'chg', 'bbp', 'bap', 'vol', 'tovr', 'trades', 'high'],  // Options/Future
                86: ['sym', 'trend', 'ltp', 'chg', 'bbp', 'bap', 'vol', 'tovr', 'trades', 'high'],  // ETF (Mutual Funds)
                11: ['sym', 'trend', 'ltp', 'chg', 'bbp', 'bap', 'vol', 'tovr', 'trades', 'high'],  // Currency
                7: ['sym', 'trend', 'ltp', 'chg', 'high', 'vol', 'tovr', 'trades', 'l52', 'prvCls']  // Index
            }
        }
    },

    tradeSettings: {
        channelId: 30,

        connectionParameters: {
            primary: {
                ip: 'icap-wstrs-qa.directfn.net/trs',
                port: '',
                secure: true
            },

            secondary: {
                ip: 'icap-wstrs-qa.directfn.net/trs',
                port: '',
                secure: true
            }
        }
    },

    tradeWidgetConfig: {
        orderDetails: {
            orderDetailsPopup: [
                {lanKey: 'exgOrderNo', dataField: 'ordId', formatter: 'S', style: '', cellStyle: ''},
                {lanKey: 'orderId', dataField: 'clOrdId', formatter: 'S', style: '', cellStyle: ''},
                {lanKey: 'side', dataField: 'ordSide', formatter: 'S', style: '', lanKeyAppend: 'orderSide', isCustomStyle: true, cellStyle: ''},
                {lanKey: 'status', dataField: 'ordSts', formatter: 'S', style: '', lanKeyAppend: 'orderStatus', isCustomStyle: true, cellStyle: ''},
                {lanKey: 'quantity', dataField: 'ordQty', formatter: 'I', style: '', cellStyle: ''},
                {lanKey: 'pendingQty', dataField: 'pendQty', formatter: 'I', style: '', cellStyle: ''},
                {lanKey: 'filledQuantity', dataField: 'cumQty', formatter: 'I', style: '', cellStyle: ''},
                {lanKey: 'orderType', dataField: 'ordTyp', formatter: 'S', style: '', lanKeyAppend: 'orderType', cellStyle: ''},
                {lanKey: 'price', dataField: 'price', formatter: 'C', style: '', cellStyle: ''},
                {lanKey: 'averagePrice', dataField: 'avgPrice', formatter: 'C', style: '', cellStyle: ''},
                {lanKey: 'stopPriceType', dataField: 'stPrcTyp', formatter: 'S', style: '', cellStyle: '', allowedOrdTypes: ['3', '4']},
                {lanKey: 'stopPrice', dataField: 'stPrc', formatter: 'C', style: '', cellStyle: '', allowedOrdTypes: ['3', '4']},
                {lanKey: 'maximumPrice', dataField: 'maxPrc', formatter: 'C', style: '', cellStyle: ''},
                {lanKey: 'date', dataField: 'adjustedCrdDte', formatter: 'S', style: '', cellStyle: ''},
                {lanKey: 'expiryDate', dataField: 'adjustedExpTime', formatter: 'S', style: '', cellStyle: ''},
                {lanKey: 'tif', dataField: 'tif', formatter: 'S', style: '', lanKeyAppend: 'tifType', cellStyle: ''},
                {lanKey: 'disclosedQty', dataField: 'disQty', formatter: 'I', style: '', cellStyle: ''},
                {lanKey: 'minFill', dataField: 'minQty', formatter: 'C', style: '', cellStyle: ''},
                {lanKey: 'orderValue', dataField: 'ordVal', formatter: 'C', style: '', cellStyle: ''},
                {lanKey: 'commission', dataField: 'comsn', formatter: 'C', style: '', cellStyle: ''},
                {lanKey: 'netValue', dataField: 'netOrdVal', formatter: 'C', style: '', cellStyle: ''},
                {lanKey: 'cumOrdVal', dataField: 'cumOrdVal', formatter: 'C', style: '', cellStyle: ''},
                {lanKey: 'symbolType', dataField: 'instruTyp', formatter: 'S', style: '', isAssetType: true, cellStyle: ''},
                {lanKey: 'settlementDate', dataField: 'adjustedSttlmntDte', formatter: 'S', style: '', cellStyle: ''}
            ]
        }
    }
};

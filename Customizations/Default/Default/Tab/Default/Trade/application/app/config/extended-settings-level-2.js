export default {
    appConfig: {
        customisation: {
            isTradingEnabled: true,
            hashType: 'MD5',
            isUpperCasePassword: true
        }
    }
};

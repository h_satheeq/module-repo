import BaseModuleInitializer from '../../../models/shared/initializers/base-module-initializer';

export default BaseModuleInitializer.extend({
    postInitialize: function () {
        // Initialization logic for multi screens should goes here
    }
});
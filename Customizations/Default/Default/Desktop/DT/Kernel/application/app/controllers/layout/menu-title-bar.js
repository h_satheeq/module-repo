import Ember from 'ember';
import TitleBar from './title-bar';
import utils from '../../utils/utils';
import sharedService from '../../models/shared/shared-service';

export default TitleBar.extend({
    gaKey: 'menu-title-bar',
    layoutName: 'layout/menu-title-bar',

    initialize: function () {
        var symbolIndexArr = Ember.A(); // TODO: [DineshS] This hardcoded array need to be removed and need to allow user to pick the symbols or index
        symbolIndexArr.push(sharedService.getService('price').stockDS.getStock('TDWL', 'TASI'));
        symbolIndexArr.push(sharedService.getService('price').stockDS.getStock('TDWL', 'NOMU'));
        symbolIndexArr.push(sharedService.getService('price').stockDS.getStock('BSE', 'BSEX'));
        symbolIndexArr.push(sharedService.getService('price').stockDS.getStock('MSM', 'MSM30'));
        symbolIndexArr.push(sharedService.getService('price').stockDS.getStock('DFM', 'DFMGI'));
        this.set('symbolIndexList', symbolIndexArr);

    }.on('init'),

    _openNativeWindow: function () {
        try {
            fin.desktop.main(function () {
                if (Ember.appGlobal.multiScreen.isParentWindow) {
                    var lastIdNo = 0;

                    if (sharedService.windowState.windowIds && sharedService.windowState.windowIds.length !== 0) {
                        var arrLength = sharedService.windowState.windowIds.length;
                        var lastIdNoStr = sharedService.windowState.windowIds[arrLength - 1];

                        lastIdNo = parseInt(lastIdNoStr, 10);
                    }

                    var windId = ++lastIdNo;

                    utils.nativeHelper.createWindow(windId).then(function (window) {
                        utils.nativeHelper.registerWindow(window, windId, true);
                    });
                }
            });
        } catch (e) {
            utils.logger.logError('Error: OpenFin environment not available or error while opening native window: ', e);
        }
    },

    isParentWindow: function () {
        return Ember.appGlobal.multiScreen.isParentWindow;
    }.property(),

    actions: {
        openNativeWindow: function () {
            this._openNativeWindow();
        }
    }
});
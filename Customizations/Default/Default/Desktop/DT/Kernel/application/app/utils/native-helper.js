import Ember from 'ember';
import sharedService from '../models/shared/shared-service';
import utils from '../utils/utils';

export default (function () {

    var createWindow = function (windowId) {
        return new Promise(function (resolve/* ,  reject*/) {
            var url = [window.location.protocol, '//', window.location.host, '/', '?', utils.Constants.EmbeddedModeParams.ChildWindowId,
                '=', windowId, '&loading=true', '&protocol=', window.location.protocol, '&host=', window.location.host].join('');

            try {
                var _win = new fin.desktop.Window({
                    name: windowId.toString(),
                    autoShow: true,
                    url: url,
                    contextMenu: true,
                    accelerator: {devtools: true, reload: true},
                    waitForPageLoad: true
                }, function () {
                    resolve(_win);
                });

            } catch (e) {
                utils.logger.logError('Error: OpenFin environment not available or error while creating native window: ', e);
            }
        });
    };

    var initializeWindows = function () {
        sharedService.windowState.load();

        if (Ember.appGlobal.multiScreen.isParentWindow && sharedService.windowState.windowIds && sharedService.windowState.windowIds.length !== 0) {
            var that = this;
            try {
                fin.desktop.main(function () {
                    Ember.$.each(sharedService.windowState.windowIds, function (index, winId) {
                        that.createWindow(winId).then(function (window) {
                            that.registerWindow(window, winId, false);
                        });
                    });
                });

            } catch (e) {
                utils.logger.logError('Error: OpenFin environment not available or error while opening native window: ', e);
            }
        }
    };

    var registerWindow = function (window, windId, isFirstTime) {
        Ember.appGlobal.multiScreen.childWindowArray.push(window);

        if (isFirstTime) {
            sharedService.windowState.windowIds.push(windId);
            sharedService.windowState.save();
        }

        window.addEventListener('closed', function (event) {

            if (!Ember.appGlobal.multiScreen.isParentLogoutFired) {
                var windowId = event.name;

                sharedService.windowState.windowIds.splice(sharedService.windowState.windowIds.indexOf(parseInt(windowId, 10)), 1);
                sharedService.windowState.save();
                sharedService.userState.remove(windowId);
            }
        });
    };

    var closeWindows = function () {
        Ember.$.each(Ember.appGlobal.multiScreen.childWindowArray, function (index, window) {
            window.close();
        });
    };

    return {
        createWindow: createWindow,
        initializeWindows: initializeWindows,
        closeWindows: closeWindows,
        registerWindow: registerWindow
    };
})();

import Ember from 'ember';
import utils from '../utils/utils';

export default (function () {
    var dbName = 'HTML5DTDatabase';
    var dbConnection;

    var _isDbAvailable = function () {
        return (typeof (window.indexedDB) !== 'undefined');
    };
    var initDbConnection = function () {

        if(!_isDbAvailable) {
            return;
        }
        try {
            var request = window.indexedDB.open(dbName, 3);

            request.onerror = function (event) {
                utils.logger.logError('Error connecting to Db : '.concat(dbName), event);
            };
            request.onsuccess = function (event) {
                dbConnection = event.target.result;
                utils.logger.logInfo('Successfully connected to Db : '.concat(dbName));
                _populateDummyData(); // TODO : remove this after database creation and data population part completed
            };

            request.onupgradeneeded = function (event) {
                _createTables(event.target.result);
                _createIndex(event);
            };
        }catch(err) {
            utils.logger.logError('Error connecting to Db : '.concat(dbName), err);
        }
    };

    var _createTables = function (dbCon) {
        if(!dbCon.objectStoreNames.contains('customers')) {
            dbCon.createObjectStore('customers', {
                keyPath: 'customerID'
            });
        }

        if(!dbCon.objectStoreNames.contains('portfolios')) {
            dbCon.createObjectStore('portfolios', {
                keyPath: 'securityAccNo'
            });
        }
    };

    var _createIndex = function (dbUpgradeEvent) {
        var objStore = dbUpgradeEvent.currentTarget.transaction.objectStore('portfolios');
        objStore.createIndex('custIDIdx', 'customerID', {unique: false});
    };

    var _populateDummyData = function () {
        var dbCon = _getDbConnection();
        var customerData = [
            {customerID: '933913981', firstName: 'holly', lastName: 'homes', email: 'holly@home.org', loginAlias: 'hlyhms'},
            {customerID: '933445566', firstName: 'ronda', lastName: 'rousy', email: 'ronda@rousy.org', loginAlias: 'rndrsy'},
            {customerID: '33557799', firstName: 'ronda1', lastName: 'rousy1', email: 'ronda@rousy1.org', loginAlias: 'rndrsy1'},
            {customerID: '88552255', firstName: 'ronda2', lastName: 'rousy2', email: 'ronda@rousy2.org', loginAlias: 'rndrsy2'}
        ];

        try {
            var customerObjectStore = dbCon.transaction('customers', 'readwrite').objectStore('customers');
            for (var i in customerData) {
                customerObjectStore.add(customerData[i]);
            }
        } catch(e) {
            utils.logger.logError('Error adding customer data to Db : '.concat(dbName), e);
        }

        var portfolioData = [
            {securityAccNo: 'S000001664', tradingAccName: '0300052719', customerID: '933913981', cashAccountId: 'C000001636'},
            {securityAccNo: 'S000022542', tradingAccName: '0300401643', customerID: '933913981', cashAccountId: 'C000001636'},
            {securityAccNo: 'S000022561', tradingAccName: '0300559382', customerID: '933445566', cashAccountId: 'C000022257'}
        ];

        try {
            var portfolioObjectStore = dbCon.transaction('portfolios', 'readwrite').objectStore('portfolios');
            for (var j in portfolioData) {
                portfolioObjectStore.add(portfolioData[j]);
            }
        } catch(e) {
            utils.logger.logError('Error adding portfolio data to Db : '.concat(dbName), e);
        }

    };

    initDbConnection();

    var _getDbConnection = function () {
        return dbConnection;
    };

    var getObjectsByParams = function (tableName, filerKey, filterVal, onSuccess) {
        var dbCon = _getDbConnection();
        var objectArr = Ember.A();

        try {
            var transaction = dbCon.transaction(tableName, 'readonly');
            var objectStore = transaction.objectStore(tableName);

            objectStore.openCursor().onsuccess = function (event) { //  TODO : DineshS find a mechanism to query table by index params
                var cursor = event.target.result;

                if (cursor) {
                    if(cursor.value[filerKey].toLowerCase().indexOf(filterVal.toLowerCase()) !== -1) {
                        objectArr.push(cursor.value);
                    }
                    cursor.continue();
                }else {
                    utils.logger.logDebug('Returning empty result set for query with table name : '.concat(tableName).concat(', params : ').concat(filterVal));
                }
                onSuccess(objectArr);
            };
        }catch (e) {
            utils.logger.logError('Error querying with table name : '.concat(tableName).concat(', params : ').concat(filterVal), e);
        }
    };

    var getObjectsByKey = function (tableName, searchValue, onSuccess) {
        try {
            var dbCon = _getDbConnection();
            var transaction = dbCon.transaction(tableName, 'readonly');
            var store = transaction.objectStore(tableName);
            var request = store.openCursor(window.IDBKeyRange.bound(searchValue, searchValue + '\uffff'));
            var objectArr = Ember.A();

            request.onsuccess = function (event) {
                var cursor = event.target.result;

                if (cursor) {
                    objectArr.push(cursor.value);
                    cursor.continue();
                }
            };

            transaction.oncomplete = function (/* event*/) {
                onSuccess(objectArr);
            };

            transaction.onerror = function (/* event*/) {
                utils.logger.logError('Transaction Error querying with table name : '.concat(tableName).concat(', params : ').concat(searchValue));
            };
        } catch (e) {
            utils.logger.logError('Error querying with table name : '.concat(tableName).concat(', params : ').concat(searchValue), e);
        }
    };

    var getObjectsByIndex = function (tableName, searchIndex, searchValue, onSuccess) {
        try {
            var dbCon = _getDbConnection();
            var transaction = dbCon.transaction(tableName, 'readonly');
            var store = transaction.objectStore(tableName);
            var index = store.index(searchIndex);
            var request = index.openCursor(window.IDBKeyRange.only(searchValue));
            var objectArr = Ember.A();

            request.onsuccess = function (event) {
                var cursor = event.target.result;
                if (cursor) {
                    objectArr.push(cursor.value);
                    cursor.continue();
                }
            };

            transaction.oncomplete = function (/* event*/) {
                onSuccess(objectArr);
            };

            transaction.onerror = function (/* event*/) {
                utils.logger.logError('Transaction Error querying with table name : '.concat(tableName).concat(', params : ').concat(searchValue));
            };
        } catch (e) {
            utils.logger.logError('Error querying with table name : '.concat(tableName).concat(', params : ').concat(searchValue), e);
        }
    };

    return {
        getObjectsByKey: getObjectsByKey,
        getObjectsByIndex: getObjectsByIndex,
        getObjectsByParams: getObjectsByParams
    };
})();
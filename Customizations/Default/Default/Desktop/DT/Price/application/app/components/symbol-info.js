import Ember from 'ember';
import sharedService from '../models/shared/shared-service';

export default Ember.Component.extend({
    priceService: sharedService.getService('price'),

    didInsertElement: function () {
        var symbol = this.get('symbol');

        this.priceService.addSymbolRequest(symbol.exg, symbol.sym, symbol.inst); // TODO: [DineshS] remove symbol or index subscription when removing it from widget
    },

    changeBackgroundColor: function () {
        var symbol = this.get('symbol');
        var backgroundColorCss = 'menu-up-back-color';

        if (symbol.chg < 0) {
            backgroundColorCss = 'menu-down-back-color';
        }

        this.set('backgroundColorCss', backgroundColorCss);
    }.observes('symbol.ltp')
});
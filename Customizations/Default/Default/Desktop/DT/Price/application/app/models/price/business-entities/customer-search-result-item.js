import Ember from 'ember';

export default Ember.Object.extend({
    cusId: '',               // Customer ID
    fName: '',              // First Name
    lstName: '',              // Last Name
    lgAlias: '',             // Login Alias
    dCustomer: '',             // Display Customer
    nin: '',                // NIN
    type: -1,              // Search result type
    isDisLDes: false,      // Display long description
    priority: 0,           // For sorting the final array

    setData: function (message) {
        var that = this;

        Ember.$.each(message, function (key, value) {
            that.set(key, value);
        });
    }
});

import Ember from 'ember';
import BaseComponent from '../../../../../components/base-component';
import utils from '../../../../../utils/utils';
import appConfig from '../../../../../config/app-config';
import tradeConstants from '../../../../../models/trade/trade-constants';
import sharedService from '../../../../../models/shared/shared-service';

export default BaseComponent.extend({
    layoutName: 'trade/widgets/order-ticket/components/order-main-details',

    indexArrowCSS: '',  // TODO: [satheeq] Change price index indicator css based on change/change%
    symbolSearchId: '',
    customerSearchId: '',

    dropDownBuySell: [],
    selectedSide: '',
    enableSearch: false,
    searchContainerCss: 'hide-container',
    searchIconCss: 'v-top',

    accountDropdownOptions: Ember.A(),

    quoteSettings: {
        intZero: 0,
        emptyString: '',
        styles: {
            green: 'up-fore-color',
            darkGreen: 'up-fore-color',
            red: 'down-fore-color',
            darkRed: 'down-fore-color',
            white: 'white',
            upArrow: 'glyphicon-triangle-top glyphicon ',
            downArrow: 'glyphicon-triangle-bottom glyphicon '
        }
    },

    changeSign: '',
    perChgCss: '',
    changeCss: '',
    excludedInstruments: [utils.AssetTypes.Indices], // Used to exclude in symbol search result
    portfolioDes: sharedService.getService('trade').fieldConfigs.portfolioDes,
    tradeService: sharedService.getService('trade'),

    isShowHolding: true,
    isShowGainLoss: true,

    isTablet: function () {
        return appConfig.customisation.isTablet;
    }.property(),

    onInit: function () {
        this.set('symbolSearchId', ['symbolSearch', this.get('wkey')].join('-'));
        this.set('customerSearchId', ['customerSearch', this.get('wkey')].join('-'));
        this.onSymbolUpdated();
        this.onPortfolioUpdated();
    }.on('init'),

    onReady: function () {
        this.updatePercentageChangeCss();
        // Commented line to fix over calling loadPortfolioOptions()
        // this.loadPortfolioOptions();

        if (appConfig.customisation.isMobile) {
            this.checkScreenResolution();
        }
    }.on('didInsertElement'),

    /* *
     * Update dropdown options based on exchange while portfolio selected.
     */
    onPortfolioUpdated: function () {
        var invoker = this.get('invoker');

        if (invoker.currentPortfolio && invoker.currentPortfolio.tradingAccId) {
            var exg = invoker && invoker.symbolInfo && invoker.symbolInfo.exg ? invoker.symbolInfo.exg : this.tradeService.userDS.get('userExchg')[0];
            this.updateDropDowns(exg);
        }
    }.observes('invoker.currentPortfolio'),

    updateDropDowns: function (exg) {
        var invoker = this.get('invoker');
        var sides = [];
        var orderSides = this.tradeService.tradeMetaDS.getOrderSideCollectionByExchange(exg);
        this._updateDescription(orderSides, this.tradeService.tradeMetaDS.metaMapping.orderSide);
        sides.pushObjects(orderSides);

        var defaultSide = this.get('app').lang.labels[[this.tradeService.tradeMetaDS.metaMapping.orderSide, invoker && invoker.orderParams.ordSide ? invoker.orderParams.ordSide : '1'].join('_')];

        if (!utils.validators.isAvailable(defaultSide) && orderSides.length > 0) {
            defaultSide = orderSides[0].des;
        }

        this.set('dropDownBuySell', sides);
        this.set('selectedSide', defaultSide);

        if (this.get('isTablet')) {
            this._setTabletView(invoker && invoker.orderParams.ordSide ? invoker.orderParams.ordSide : '1');
        }
    },

    updateSide: function () {
        var invoker = this.get('invoker');
        var orderSide = this.get('selectedSide');
        orderSide = invoker && invoker.orderParams && invoker.orderParams.ordSide ? this.get('app').lang.labels[[this.tradeService.tradeMetaDS.metaMapping.orderSide, invoker.orderParams.ordSide].join('_')] : orderSide;

        this.set('selectedSide', orderSide);
    }.observes('invoker.orderParams.ordSide'),

    _updateDescription: function (options, type) {
        var that = this;

        if (options) {
            Ember.$.each(options, function (index, option) {
                Ember.set(option, 'des', that.app.lang.labels[[type, option.code].join('_')]);
            });
        }
    },

    onSymbolUpdated: function () {
        var invoker = this.get('invoker');

        if (invoker && invoker.symbolInfo) {
            var symbol = this.get('invoker').symbolInfo;

            if (symbol.sym && symbol.sDes) {
                this.set('searchKey', symbol.dSym);
            } else {
                this.set('searchKey', '');
            }
        }

        this.loadPortfolioOptions();
    }.observes('invoker.symbolInfo'),

    loadPortfolioOptions: function () {
        var invoker = this.get('invoker');

        if (invoker && invoker.symbolInfo) {
            var tradingAccByExg = this.tradeService.accountDS.getTradingAccCollByExchange(invoker.get('symbolInfo.exg'));
            var tradingAccColl = this.tradeService.accountDS.getTradingAccCollection();
            var exgPortfolioAcc = tradingAccByExg && tradingAccByExg.length > 0 ? tradingAccByExg[0].tradingAccId :
                tradingAccColl && tradingAccColl.length > 0 ? tradingAccColl[0].tradingAccId : '';
            var currentHolding = invoker.currentHolding;
            var currentAccId = currentHolding && currentHolding.symbolInfo && (currentHolding.symbolInfo.sym === invoker.symbolInfo.sym) && currentHolding.tradingAccId ? currentHolding.tradingAccId : exgPortfolioAcc;
            var currAccount = this.tradeService.accountDS.getTradingAccount(currentAccId);

            invoker.set('currentPortfolio', currAccount);
            this.set('accountDropdownOptions', tradingAccColl);
            this.tradeService.accountDS.subscribeTradingAccountInfo(currAccount);
        }
    },

    loadPortfoliosOfCustomer: function (customer) {
        var that = this;

        var onDataReceiveSuccess = function (portCollection) {
            var invoker = that.get('invoker');
            that.set('accountDropdownOptions', portCollection);

            if (portCollection && portCollection.length > 0) {
                if (invoker) {
                    invoker.set('currentPortfolio', portCollection[0]);
                    that.tradeService.accountDS.subscribeTradingAccountInfo(invoker.get('currentPortfolio'));
                }
            }
        };
        this.tradeService.customerSearchDS.filterCustomerPortfolios(customer.customerID, onDataReceiveSuccess);

    },

    updatePercentageChangeCss: function () {
        var invoker = this.get('invoker');

        if (invoker && invoker.symbolInfo) {
            var pctChg = invoker.get('symbolInfo').pctChg;
            var changeSign = '';
            var perChgCss = '';
            var changeCss = '';

            if (pctChg > this.quoteSettings.intZero) {
                changeSign = this.quoteSettings.styles.upArrow;
                perChgCss = this.quoteSettings.styles.green;
                changeCss = this.quoteSettings.styles.darkGreen;
            } else if (pctChg < this.quoteSettings.intZero) {
                changeSign = this.quoteSettings.styles.downArrow;
                perChgCss = this.quoteSettings.styles.red;
                changeCss = this.quoteSettings.styles.darkRed;
            } else {
                changeSign = this.quoteSettings.emptyString;
                perChgCss = this.quoteSettings.styles.white;
            }

            this.set('changeSign', changeSign);
            this.set('perChgCss', perChgCss);
            this.set('changeCss', changeCss);
        }

    }.observes('invoker.symbolInfo.pctChg'),

    /* *
     * Reset order ticket if there is no valid search key added.
     * @private
     */
    _validateSearchKey: function () {
        var searchKey = this.get('searchKey');
        var invoker = this.get('invoker');

        if (invoker && invoker.symbolInfo) {
            var symbol = this.get('invoker').symbolInfo;

            if (!utils.validators.isAvailable(searchKey) || symbol.dSym) {
                if (symbol && utils.validators.isAvailable(searchKey)) {
                    this.set('searchKey', symbol.dSym);
                } else {
                    this.sendAction(this.get('onSymbolChanged'));
                }
            } else {
                this.set('searchKey', '');
            }
        }
    },

    searchToggle: function () {
        if (this.get('enableSearch')) {
            this.set('searchContainerCss', 'full-width pad-widget-right');
            this.set('searchIconCss', '');
        } else {
            this.set('searchContainerCss', 'hide-container');
            this.set('searchIconCss', 'v-top');
        }
    }.observes('enableSearch'),

    checkScreenResolution: function () {
        var lowResolutionWidth = 340;

        if (window.screen.width <= lowResolutionWidth) {
            this.set('containerPadding', 'low-res-padding-right');
        }
    },

    _setTabletView: function (code) {
        if (this.get('isTablet')) {
            if (code === tradeConstants.OrderSide.Buy) {
                this.set('isShowHolding', false);
                this.set('isShowGainLoss', true);
            } else if (code === tradeConstants.OrderSide.Sell) {
                this.set('isShowHolding', true);
                this.set('isShowGainLoss', false);
            }
        }
    },

    actions: {
        onSideChanged: function (side) {
            this.get('invoker').orderParams.set('ordSide', side.code);
            this.set('selectedSide', side.des);

            if (this.get('isTablet')) {
                this._setTabletView(side.code);
            }
        },

        showSearchPopup: function () {
            var modal = this.get(this.get('symbolSearchId'));
            modal.send('showModalPopup');
        },

        closeSearchPopup: function () {
            var modal = this.get(this.get('symbolSearchId'));
            modal.send('closeModalPopup');
        },

        onSymbolSelected: function (symbol) {
            this.set('searchKey', symbol.dSym);
            this.set('enableSearch', false);

            // Here an action call used to overcome boiler plate code on symbol subscription and stockDS call
            this.sendAction(this.get('onSymbolChanged'), symbol);
            this.loadPortfolioOptions();
        },

        onSelectPortfolio: function (portfolio) {
            var invoker = this.get('invoker');
            var oldPortObj = invoker.get('currentPortfolio');

            if (oldPortObj) {
                this.tradeService.accountDS.unSubscribeTradingAccountInfo(oldPortObj);
            }

            invoker.set('currentPortfolio', portfolio);
            this.tradeService.accountDS.subscribeTradingAccountInfo(portfolio);
        },
        showCustSearchPopup: function () {
            var modal = this.get(this.get('customerSearchId'));
            modal.send('showModalPopup');
        },
        closeCustSearchPopup: function () {
            var modal = this.get(this.get('customerSearchId'));
            modal.send('closeModalPopup');
        },

        onCustomerSelected: function (customer) {
            this.set('cusSearchKey', customer.customerID);
            this.set('enableSearch', false);
            this.loadPortfoliosOfCustomer(customer);
        },

        onTradeMetaReady: function () {
            this.loadPortfolioOptions();
            this.onPortfolioUpdated();
        },

        onLanguageChanged: function () {
            this.onPortfolioUpdated();
        },

        validateSearchKey: function () {
            this._validateSearchKey();
        },

        onReset: function () {
            // Reset dropdown
        },

        setPriceValue: function (priceVal) {
            this.set('displayPrice', priceVal);
        },

        toggleDisplay: function () {
            this.toggleProperty('enableSearch');
        },

        // This method is used to show Symbol Search directly using 'toggleDisplay(isCloseSearch)', passing 'isCloseSearch' as false in Mobile
        showOrderTicketSearch: function () {
            sharedService.getService('sharedUI').getService('titleBar').toggleDisplay();
        }
    }
});
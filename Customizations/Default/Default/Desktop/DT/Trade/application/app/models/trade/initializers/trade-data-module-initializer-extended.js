import TradeDataModuleInitializer from './trade-data-module-initializer';
import TradeServiceExtended from '../../../models/trade/trade-service-extended';

export default TradeDataModuleInitializer.extend({
    createService: function () {
        return TradeServiceExtended.create();
    }
});
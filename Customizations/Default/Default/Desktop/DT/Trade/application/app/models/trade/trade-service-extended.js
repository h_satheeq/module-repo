import TradeService from './trade-service';
import DataStoreFactory from './data-stores/data-store-factory';

export default TradeService.extend({
    customerSearchDS: undefined,

    createDataStores: function () {
        this._super();
        this.set('customerSearchDS', DataStoreFactory.createCustomerSearchDataStore(this));
    }

});
import Ember from 'ember';
import sharedService from '../models/shared/shared-service';
import PriceConstants from '../models/price/price-constants';

export default Ember.Component.extend(Ember.SortableMixin, {
    tradeService: sharedService.getService('trade'),
    content: Ember.A(),

    customerSearchCellCss: function () {
        return this.get('isMobile') ? 'layout-container full-width' : 'layout-col';
    }.property(),

    customerSearchContainerCss: function () {
        return this.get('isMobile') ? 'layout-col-24' : 'layout-col';
    }.property(),

    getSearchResult: function () {
        this.getCustomerSearchResult();
    },
    getCustomerSearchResult: function () {
        var searchKey = this.get('searchKey');

        if(searchKey) {
            var customerList = this.tradeService.customerSearchDS.filterCustomerSearchResults(searchKey);
            this.set('content', customerList);
        }
    },

    showSearchResultContainer: function () {
        Ember.appGlobal.activeWidget = 'customer-search' + this.get('wkey');
        return this.get('content').length > 0 && this.get('searchKey');
    }.property('content.@each'),

    searchKeyDidChange: function () {
        if (this.get('searchKey')) {
            Ember.set(this, 'searchKey', this.get('searchKey').toUpperCase());
        }
        // Every time a key is pressed, this event fires, and that event will start the filter in given time interval
        Ember.run.debounce(this, this.getSearchResult, PriceConstants.TimeIntervals.SearchAutoCompleteInterval);
    }.observes('searchKey'),

    _onItemSelected: function (item) {
       this.sendAction('clickAction', item);
        this.sendAction('closePopup');
    },

    actions: {
        searchClick: function () {
            this.getSearchResult();
        },
        onItemSelected: function (item, link) {
            this._onItemSelected(item, link);
        }
    }
});
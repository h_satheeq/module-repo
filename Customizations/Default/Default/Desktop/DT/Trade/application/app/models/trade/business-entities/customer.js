import Ember from 'ember';

export default Ember.Object.extend({
    customerID: '',
    firstName: '',
    lastName: '',
    email: '',
    loginAlias: '',

    setData: function (portMessage) {
        var that = this;

        Ember.$.each(portMessage, function (key, value) {
            that.set(key, value);
        });
    }
});

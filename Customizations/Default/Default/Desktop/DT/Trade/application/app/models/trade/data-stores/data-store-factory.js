import customerSearchDS from './customer-search-data-store';

export default (function () {
    var createCustomerSearchDataStore = function (tradeService) {
        var customerSearchDataStore = customerSearchDS.create({tradeService: tradeService});
        return customerSearchDataStore;
    };

    return {
        createCustomerSearchDataStore: createCustomerSearchDataStore
    };

})();
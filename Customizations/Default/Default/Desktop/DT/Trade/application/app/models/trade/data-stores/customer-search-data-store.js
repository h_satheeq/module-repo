import Ember from 'ember';
import utils from '../../../utils/utils';
import Customer from '../business-entities/customer';

export default Ember.Object.extend({

    filterCustomerSearchResults: function (searchKey) {
        var customerSearchResults = Ember.A();

        var onSuccessDbDataRetrieval = function (customerList) {
            var customerSearchData = Ember.A();
            Ember.$.each(customerList, function (key, customerObj) {
                var customer = Customer.create({customerID: customerObj.customerID});
                customer.setData(customerObj);
                customerSearchData.pushObject(customer);
            });

            if (customerSearchData.length > 0) {
                customerSearchResults.pushObject(Ember.Object.create({
                    type: '',
                    rank: 1,
                    name: '',
                    colorCss: '',
                    contents: customerSearchData
                }));
            } else {
                customerSearchResults.pushObject(Ember.Object.create({
                    isDataUnavailable: true
                }));
            }
        };
        utils.indexedDBConnector.getObjectsByKey('customers', searchKey, onSuccessDbDataRetrieval);

        return customerSearchResults;
    },

    filterCustomerPortfolios: function (customerID, onDataReceiveSuccess) {
        var customerPortfolios = Ember.A();
        var that = this;

        var onSuccess = function (portfolioList) {
            Ember.$.each(portfolioList, function (key, portfolioObj) {
                var tradingAccount = that.tradeService.accountDS.getTradingAccount(portfolioObj.securityAccNo, portfolioObj.cashAccountId, ['TDWL']);

                if (tradingAccount) {
                    tradingAccount.setData({
                        tradingAccId: portfolioObj.securityAccNo,
                        cashAccountId: portfolioObj.cashAccountId,
                        portNme: portfolioObj.tradingAccName
                    });
                }
                customerPortfolios.pushObject(tradingAccount);
            });
            onDataReceiveSuccess(customerPortfolios);
        };
        utils.indexedDBConnector.getObjectsByIndex('portfolios', 'custIDIdx', customerID, onSuccess);

        return customerPortfolios;
    }
});
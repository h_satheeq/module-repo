/* jshint node: true */
var EnvironmentCore = require('./environment-core.js');

module.exports = function (environment) {
    var ENV = new EnvironmentCore(environment);

    if (ENV.APP.isTestMode) {
        ENV.APP.priceConnectionParameters = {
            primary: {
                ip: 'data-sa9.mubasher.net/html5ws',
                port: '',
                secure: true // Is connection secure - true or false
            }
        };

        ENV.contentSecurityPolicy['connect-src'] = "'self' www.google-analytics.com wss://data-sa9.mubasher.net/html5ws wss://data-sa.mubasher.net/html5-ws-UAT wss://data-sa9.mubasher.net/html5-Retail";
    }

    return ENV;
};

// Data stores
import announcementDS from './announcements-data-store';
import marketDepthDS from './depth-data-store';
import exchangeDS from './exchange-data-store';
import ohlcDS from '../../chart/data-stores/ohlc-data-store';
import stockDS from './stock-data-store';
import gmsDS from './gms-data-store';
import topStockDS from './top-stocks-data-store';
import companyProfileDS from './company-profile-data-store';
import timeAndSalesDS from './time-and-sales-data-store';
import searchDS from './search-data-store';
import sectorDS from './sector-data-store';
import subMarketDS from './sub-market-data-store';
import watchListDS from './watch-list-data-store';
import fairValueDS from './fair-value-data-store';
import corporateActionDS from './corporate-action-data-store';
import theoreticalChartDS from '../../chart/data-stores/topv-data-store';
import theoreticalStockDS from './theoretical-stock-data-store';

export default (function () {
    var createAnnouncementDataStore = function (priceService) {
        return announcementDS.create({priceService: priceService});
    };

    var createMarketDepthDataStore = function (priceService) {
        return marketDepthDS(priceService);
    };

    var createExchangeDataStore = function (priceService) {
        return exchangeDS.create({priceService: priceService});
    };

    var createOHLCDataStore = function (priceService) {
        var ohlcDataStore = ohlcDS.create({priceService: priceService});
        ohlcDataStore.initialize();

        return ohlcDataStore;
    };

    var createStockDataStore = function (priceService) {
        var stockDataStore = stockDS.create({priceService: priceService});
        stockDataStore.initialize();

        return stockDataStore;
    };

    var createGMSDataStore = function (priceService) {
        return gmsDS.create({priceService: priceService});
    };

    var createTopStockDataStore = function (priceService) {
        return topStockDS.create({priceService: priceService});
    };

    var createCompanyProfileDataStore = function (priceService) {
        return companyProfileDS(priceService);
    };

    var createTimeAndSalesDataStore = function (priceService) {
        return timeAndSalesDS.create({priceService: priceService});
    };

    var createSearchDataStore = function (priceService) {
        return searchDS(priceService);
    };

    var createSectorDataStore = function (priceService) {
        return sectorDS.create({priceService: priceService});
    };

    var createSubMarketDataStore = function (priceService) {
        return subMarketDS.create({priceService: priceService});
    };

    var createWatchListDataStore = function (priceService) {
        return watchListDS.create({priceService: priceService});
    };

    var createFairValueDataStore = function (priceService) {
        return fairValueDS.create({priceService: priceService});
    };

    var createCorporateActionDataStore = function (priceService) {
        return corporateActionDS.create({priceService: priceService});
    };

    var createTheoreticalChartDataStore = function (priceService) {
        var theoreticalDS = theoreticalChartDS.create({priceService: priceService});
        theoreticalDS.initialize();

        return theoreticalDS;
    };

    var createTheoreticalStockDataStore = function (priceService) {
        return theoreticalStockDS.create({priceService: priceService});
    };

    return {
        createAnnouncementDataStore: createAnnouncementDataStore,
        createMarketDepthDataStore: createMarketDepthDataStore,
        createExchangeDataStore: createExchangeDataStore,
        createOHLCDataStore: createOHLCDataStore,
        createStockDataStore: createStockDataStore,
        createGMSDataStore: createGMSDataStore,
        createTopStockDataStore: createTopStockDataStore,
        createCompanyProfileDataStore: createCompanyProfileDataStore,
        createTimeAndSalesDataStore: createTimeAndSalesDataStore,
        createSearchDataStore: createSearchDataStore,
        createSectorDataStore: createSectorDataStore,
        createSubMarketDataStore: createSubMarketDataStore,
        createWatchListDataStore: createWatchListDataStore,
        createFairValueDataStore: createFairValueDataStore,
        createCorporateActionDataStore: createCorporateActionDataStore,
        createTheoreticalChartDataStore: createTheoreticalChartDataStore,
        createTheoreticalStockDataStore: createTheoreticalStockDataStore
    };
})();

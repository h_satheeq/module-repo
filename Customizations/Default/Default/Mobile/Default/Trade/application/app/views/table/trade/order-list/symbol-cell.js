import Ember from 'ember';
import userSettings from '../../../../config/user-settings';
import languageDataStore from '../../../../models/shared/language/language-data-store';
import utils from '../../../../utils/utils';
import tradeConstants from '../../../../models/trade/trade-constants';
import ExpandedCell from '../../dual-cells/expanded-cell';

export default ExpandedCell.extend({
    templateName: 'table/views/trade/order-list/symbol-cell',
    app: languageDataStore.getLanguageObj(),
    orderType: undefined,

    formattedFirstValue: function () {
        var isDefaultValueCheck = this.get('column') ? this.get('column').isDefaultValueCheck : false;
        var firstValue = this.get('cellContent') ? this.get('cellContent').firstValue : undefined;

        if (isDefaultValueCheck) {
            var defaultValue = this.get('column') ? this.get('column').defaultValue : undefined;

            if (firstValue === defaultValue) {
                return userSettings.displayFormat.noValue;
            }
        }

        return this.addFormat(firstValue);
    }.property('cellContent'),

    thirdValue: function () {
        return this.get('cellContent') ? this.get('cellContent').thirdValue : undefined;
    }.property('cellContent'),

    formattedThirdValue: function () {
        var labels = this.get('app').lang.labels;
        var currentValue = this.get('cellContent') ? this.get('cellContent').thirdValue : '';
        var labelKey = this.get('column') && this.get('column').name && utils.validators.isAvailable(currentValue) ? [this.get('column').name, currentValue].join('_') : undefined;

        var rowData = this.get('row') ? this.get('row').getProperties('ordTyp') : undefined;

        if (rowData && rowData.ordTyp) {
            var value = rowData.ordTyp;
            this.set('orderType', tradeConstants.OrderTypes[value].DisplayName);
        }

        return labelKey && labels[labelKey] ? labels[labelKey] : currentValue;
    }.property('cellContent', 'app.lang'),

    styleThirdValue: function () {
        var currentValue = this.get('cellContent') ? this.get('cellContent').thirdValue : '';
        var styleKey = this.get('column') && this.get('column').name && utils.validators.isAvailable(currentValue) ? [this.get('column').name, currentValue].join('_') : '';

        return [this.get('column.thirdValueStyle'), styleKey].join(' ');
    }.property('cellContent'),

    formattedFourthValue: Ember.computed(function () {
        var value = this.get('row') ? this.get('row.ordQty') : undefined;
        return this._getFormattedValue(value, 0);
    }).property('row.ordQty'),

    formattedFifthValue: Ember.computed(function () {
        if (this.get('row.ordTyp') === '1') {
            return this.get('app').lang.labels.mktPrice;
        }

        var value = this.get('row') ? this.get('row.price') : undefined;
        return this._getFormattedValue(value);
    }).property('row.price'),

    formattedSixthValue: Ember.computed(function () {
        var value = this.get('row') ? this.get('row.netOrdVal') : undefined;
        return this._getFormattedValue(value);
    }).property('row.netOrdVal'),

    _getFormattedValue: function (value, decimals) {
        var decimalPlaces = decimals || decimals === 0 ? decimals : this.get('column.noOfDecimalPlaces');
        return value === -1 ? userSettings.displayFormat.noValue : utils.formatters.formatNumber(value, decimalPlaces || decimalPlaces === 0 ? decimalPlaces : userSettings.displayFormat.decimalPlaces);
    }
});

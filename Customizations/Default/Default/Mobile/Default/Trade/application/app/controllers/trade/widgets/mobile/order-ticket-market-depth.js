import Ember from 'ember';
import QuoteMarketDepth from '../../../price/widgets/quote-market-depth';

export default QuoteMarketDepth.extend({
    marketDepthCss: 'marketDepthContainerCollapsed',
    marketDepthIconCss: 'pad-l-t',
    adjustedBidRecordList: Ember.A(),
    adjustedOfferRecordList: Ember.A(),
    isBottomRowHidden: false,

    onAfterRender: function () {
        this.initializeButtonAnimation();
        this.checkScreenResolution();
    },

    showMarketDepth: function () {
        var marketDepthCss;
        var marketDepthIconCss;

        if (this.get('parentContainer').isMarketDepthEnable) {
            marketDepthCss = 'marketDepthContainerOpen shadow';
            marketDepthIconCss = '';
        } else {
            marketDepthCss = 'marketDepthContainerCollapsed';
            marketDepthIconCss = 'pad-l-t';
        }

        this.set('marketDepthCss', marketDepthCss);
        this.set('marketDepthIconCss', marketDepthIconCss);
    }.observes('parentContainer.isMarketDepthEnable'),

    setDisplayPrice: function () {
        var invoker = this.get('invoker');

        if (invoker.orderParams && !invoker.orderParams.get('displayPrice') && (invoker.get('symbolInfo') && invoker.get('symbolInfo').bap)) {
            this.setPriceValue(invoker.get('symbolInfo').bap);
        }
    }.observes('invoker.symbolInfo.bap'),

    sliceArray: function () {
        var bidRecordList = this.get('bidRecordList');
        var offerRecordList = this.get('offerRecordList');
        var depthSymbol = this.get('symbolObj');
        var adjustedBidRecordList;
        var adjustedOfferRecordList;

        this.set('adjustedBidRecordList', Ember.A());
        this.set('adjustedOfferRecordList', Ember.A());

        if (bidRecordList && bidRecordList.length > 0) {
            if (bidRecordList.length >= 4) {
                adjustedBidRecordList = bidRecordList.slice(0, 4);
            } else {
                adjustedBidRecordList = bidRecordList;
            }
        } else {
            adjustedBidRecordList = [{prc: depthSymbol.get('bbp'), qty: depthSymbol.get('bbq')}];
        }

        if (offerRecordList && offerRecordList.length > 0) {
            if (bidRecordList.length >= 4) {
                adjustedOfferRecordList = offerRecordList.slice(0, 4);
            } else {
                adjustedOfferRecordList = offerRecordList;
            }
        } else {
            adjustedOfferRecordList = [{prc: depthSymbol.get('bap'), qty: depthSymbol.get('baq')}];
        }

        this.get('adjustedBidRecordList').pushObjects(adjustedBidRecordList);
        this.get('adjustedOfferRecordList').pushObjects(adjustedOfferRecordList);
    }.observes('symbolObj.bbq', 'offerRecordList.[]'),

    setPriceValue: function (priceVal) {
        var invoker = this.get('invoker');

        if (invoker && invoker.orderStrategy && !invoker.orderStrategy.isPriceDisabled) {
            invoker.orderParams.set('displayPrice', priceVal);
        }
    },

    checkScreenResolution: function () {
        var lowResolutionWidth = 340;

        if (window.screen.width <= lowResolutionWidth) {
            this.set('containerPadding', 'low-res-padding-right');
            this.set('isBottomRowHidden', true);
        }
    },

    actions: {
        setPriceValue: function (priceVal) {
            this.setPriceValue(priceVal);
        },

        showMarketDepth: function () {
            this.toggleProperty('parentContainer.isMarketDepthEnable');
        }
    }
});
import sharedService from '../../../../../models/shared/shared-service';
import ControllerFactory from '../../../../../controllers/controller-factory';
import BasePopup from '../../../../../components/base-popup';

export default BasePopup.extend({
    layoutName: 'trade/widgets/mobile/components/general-popup-mobile',

    showPopup: function (controller, viewName, modal, container) {
        var accController = ControllerFactory.createController(this.container, 'controller:trade/widgets/mobile/components/general-popup-mobile');
        var popViewName = 'trade/widgets/mobile/components/general-popup-mobile';
        this._super(accController, popViewName, modal, container);

        if (controller) {
            var route = this.container.lookup('route:application');

            route.render(viewName, {
                into: 'trade/widgets/mobile/components/general-popup-mobile',
                outlet: 'generalPopupOutlet',
                controller: controller
            });
        }
    },

    actions: {
        closePopup: function () {
            var modal = sharedService.getService('sharedUI').getService('modalPopupId');
            modal.send('closeModalPopup');
        }
    }
});
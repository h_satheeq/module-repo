import Ember from 'ember';
import HeaderCell from '../../../../views/table/dual-cells/header-cell';
import GainLossCell from '../../../../views/table/trade/portfolio/gain-loss-cell';
import OrderValueCell from '../../../../views/table/trade/portfolio/order-value-cell';
import QuantityCell from '../../../../views/table/trade/portfolio/quantity-cell';
import QuantityHeaderCell from '../../../../views/table/trade/portfolio/quantity-header-cell';
import ContextMenuMobile from '../../../../views/table/mobile/context-menu-cell';
import MoreHeaderCell from '../../../../views/table/more-header-cell';
import TableRow from '../../../../views/table/table-row';
import ClassicHeaderCell from '../../../../views/table/classic-header-cell';
import portfolio from '../portfolio';
import sharedService from '../../../../models/shared/shared-service';
import layoutConfig from '../../../../config/layout-config';
import ControllerFactory from '../../../../controllers/controller-factory';
import utils from '../../../../utils/utils';
import responsiveHandler from '../../../../helpers/responsive-handler';

export default portfolio.extend({
    gainLossValue: '',
    gainLossCss: '',
    gainLossPercentage: '',
    mktVal: '',
    isEnableWatchlist: false,
    isOddEvenRowStyleDisabled: true,
    isExpandedView: false,
    widgetName: '',
    headerHeight: 40,
    watchListController: '',
    contextPath: 'trade/widgets/mobile/components/portfolio-context-panel',
    subscriptionSymNames: [],
    menuArray: [],
    defaultMenu: {},

    oneWayContent: Ember.computed.oneWay('arrangedContent'),
    isRenderingEnabled: false,

    onLoadWidget: function () {
        this._super();
        this.set('widgetName', this.app.lang.labels.watchList);
        this.set('appLayout', layoutConfig);
        this._setMenuArray();
    },

    initializeResponsive: function () {
        var that = this;

        Ember.run.next(this, function () {
            that.set('responsive', responsiveHandler.create({
                controller: this,
                widgetId: 'hnav-panel-mobile',
                callback: this.onResponsive
            }));

            that.responsive.addList('portfolio-free', [{id: 'portfolio-value', width: 5}]);
            that.responsive.initialize();
        });
    },

    headerNames: function () {
        var app = this.app.lang.labels;
        return {
            firstValue: app.gain + ' / ' + app.loss,
            secondValue: app.gainLossPerShort,
            thirdValue: app.marketValue
        };
    }.property('app.lang.labels'),

    gainLossLimits: {
        upperLimit: 9999999.99,
        lowerLimit: 99999.99
    },

    styleSettings: {
        expandedModeHeight: 79,
        collapsedModeHeight: 57,
        expandedHeaderHeight: 50,
        collapsedHeaderHeight: 40,
        contextMenuRatio: 37 / 40,
        symbolColumnWidth: 150
    },

    switchRowMode: function () {
        var height = this.get('isExpandedView') ? this.get('styleSettings.expandedModeHeight') : this.get('styleSettings.collapsedModeHeight');
        var headerHeight = this.get('isExpandedView') ? this.get('styleSettings.expandedHeaderHeight') : this.get('styleSettings.collapsedHeaderHeight');

        this.set('rowHeight', height);
        this.set('headerHeight', headerHeight);
    }.observes('isExpandedView'),

    switchPortfolioWatchlist: function () {
        if (this.get('isEnableWatchlist')) {
            this.renderPortfolioWatchlist();
        }
    }.observes('isEnableWatchlist'),

    onUnloadWidget: function () {
        this._super();

        sharedService.getService('price').removeSymbolListRequest(this.get('exg'), this.get('subscriptionSymNames'));
        this.set('subscriptionSymNames', []);
    },

    setFooterArray: function () {       // TODO [AROSHA] Remove below calculation and get vales from PortfolioDS after testing PortfolioDS GL implementation data
        this._super();
        var totalInfo = this.get('footerArray')[0];

        if (totalInfo) {
            var gainLoss = totalInfo.gainLoss;

            if (gainLoss > this.gainLossLimits.upperLimit) { // Maximum number which app can support as per available space
                gainLoss = utils.formatters.divideNumber(gainLoss, 1);
            } else if (this.gainLossLimits.upperLimit >= gainLoss && gainLoss > this.gainLossLimits.lowerLimit) {
                var intValue = gainLoss.toString().split('.');
                gainLoss = utils.formatters.formatNumber(intValue[0], 0);
            } else {
                gainLoss = utils.formatters.formatNumber(gainLoss, 2);
            }

            this.set('gainLossValue', gainLoss);
            this.set('gainLossPercentage', totalInfo.gainLossPer);
            this.set('gainLossCss', totalInfo.gainLoss !== 0 ? totalInfo.gainLoss > 0 ? 'up-back-color' : 'down-back-color' : 'appttl-mobile');
            this.set('gainLossPerCss', totalInfo.gainLossPer !== 0 ? totalInfo.gainLossPer > 0 ? 'up-fore-color' : 'down-fore-color' : 'fade-fore-color');
            this.set('mktVal', totalInfo.mktVal);
        }
    },

    cellViewsForColumns: {
        gainLossCell: 'Ember.GainLossCell',
        orderValueCell: 'Ember.OrderValueCell',
        quantityCell: 'Ember.QuantityCell',
        contextMenuMobile: 'Ember.ContextMenuMobile'
    },

    setCellViewsScopeToGlobal: function () {
        Ember.HeaderCell = HeaderCell;
        Ember.GainLossCell = GainLossCell;
        Ember.OrderValueCell = OrderValueCell;
        Ember.QuantityCell = QuantityCell;
        Ember.QuantityHeaderCell = QuantityHeaderCell;
        Ember.ContextMenuMobile = ContextMenuMobile;
        Ember.MoreHeaderCell = MoreHeaderCell;
        Ember.TableRow = TableRow;
        Ember.ClassicHeaderCell = ClassicHeaderCell;
    },

    renderPortfolioWatchlist: function () {
        var route = this.container.lookup('route:application');
        var viewName = 'price/widgets/mobile/watch-list/watch-list';
        var controller = this.get('watchListController');

        if (!controller) {
            controller = ControllerFactory.createController(this.container, 'controller:price/widgets/mobile/watch-list/watch-list');
        }

        controller.initializeWidget({wn: 'watch-list'}, {widgetArgs: {isAllowPrepareData: false}});
        controller.set('sortCols', this.get('sortPropertiesWl'));
        controller.set('sortAsc', this.get('sortAscWl'));
        controller.set('rowHeight', 57);
        controller.set('watchListType', sharedService.getService('price').watchListDS.watchListTypes.portfolio);
        controller.set('currentPortfolio', this.get('currentPortfolio'));
        controller.set('isHideLink', true);
        controller.set('selectedLink', '1');
        controller.set('isTableControlPanelDisabled', true);
        controller.set('isCustomWLMode', true);
        controller.set('isDeleteButtonDisabled', true);
        controller.set('wkey', this.get('wkey'));

        this.set('watchListController', controller);
        this.sendSymbolRequestForSubcriptionSymbols(this.get('arrangedContent'));

        route.render(viewName, {
            into: 'trade/widgets/mobile/portfolio',
            outlet: 'portfolioWatchlist',
            controller: controller
        });
    },

    sendSymbolRequestForSubcriptionSymbols: function (holdingsArray) {
        var subscriptionSymNames = this.get('subscriptionSymNames');

        Ember.$.each(holdingsArray, function (key, holding) {
            var priceService = sharedService.getService('price');
            var symbol = holding.symbol;
            var exchange = holding.exg;

            if (subscriptionSymNames.indexOf(symbol) < 0) {
                priceService.addSymbolRequest(exchange, symbol);
                subscriptionSymNames[subscriptionSymNames.length] = symbol;
            }
        });

        this.set('subscriptionSymNames', subscriptionSymNames);
    },

    onAfterRender: function () {
        var that = this;
        this.initializeButtonAnimation();

        Ember.run.later(function () {
            that.set('isRenderingEnabled', true);
        }, 1);
    },

    onLanguageChanged: function () {
        this._super();
        this.refreshTableComponent();
        this._setMenuArray();
    },

    updateContent: function () {
        this._super();
        this.sendSymbolRequestForSubcriptionSymbols(this.get('arrangedContent'));

        var controller = this.get('watchListController');

        if (controller) {
            controller.refreshWidget({currentPortfolio: this.get('currentPortfolio')});
        }
    },

    _setMenuArray: function () {
        var accountSummaryItem = {id: 1, name: this.get('app.lang.labels.accountSummary')};

        this.set('defaultMenu', accountSummaryItem);
        this.set('menuArray', [accountSummaryItem]);
    },

    renderSubscribeWindow: function (selectedRow) {
        var rowData = selectedRow.getProperties('isRoot', 'exg', 'symbol', 'avaiQty', 'instruTyp', 'secAccNum', 'sDes', 'subPrice');

        var widgetController = ControllerFactory.createController(this.container, 'controller:trade/widgets/rights-subscription');
        var viewName = 'trade/widgets/rights-subscription';

        widgetController.set('currentPortfolio', this.get('currentPortfolio'));
        widgetController.set('rowData', rowData);
        widgetController.set('title', this.get('app').lang.labels[widgetController.get('titleKey')]);

        widgetController.initializeWidget({wn: 'rights-subscription'});
        sharedService.getService('priceUI').showChildView(viewName, widgetController, this.get('app.lang.labels.rightsSubscription'), 'portfolio-' + this.get('wkey'));
    },

    _collapsePortfolioRow: function (target, isContextPanelEnable) {
        var targetClicked = target;

        if (targetClicked) {
            if (!isContextPanelEnable) {
                this.set('isContextPanel', false);
            }

            if (targetClicked.style) {
                targetClicked.style.removeProperty('width');
            }

            var arrowIconArray = targetClicked.getElementsByClassName('icon-angle-left');

            if (arrowIconArray && arrowIconArray.length > 0) {
                arrowIconArray[0].className = 'glyphicon icon-angle-right font-5x-l';
            }
        }
    },

    actions: {
        itemClicked: function () {
            var widgetController = ControllerFactory.createController(this.container, 'controller:trade/widgets/mobile/account-summary');
            var viewName = 'trade/widgets/mobile/account-summary';

            widgetController.initializeWidget({wn: 'account-summary'});

            sharedService.getService('priceUI').showChildView(viewName, widgetController, this.get('app.lang.labels.accountSummary'), 'portfolio-' + this.get('wkey'));

            if (this.get('currentPortfolio.tradingAccId') !== this.get('app').lang.labels.all) {
                widgetController.setPortfolio(this.get('currentPortfolio'));
            }
        },

        expandColumnAction: function () {
            this.toggleProperty('isExpandedView');
        },

        showOrderTicket: function (isBid) {
            var tradeService = sharedService.getService('trade');
            var isTradeEnabledExchange = tradeService && tradeService.userDS.isTradeEnabledExchange(this.get('exg'));

            if (isTradeEnabledExchange) {
                sharedService.getService('tradeUI').showOrderTicket(this.container, isBid, this.get('symInfo'));
            }
        },

        renderSubscribeWindow: function (selectedRow) {
            this.renderSubscribeWindow(selectedRow);
        },

        clickRow: function (selectedRow) {
            var target = event.target ? event.target : event.srcElement;

            if (target) {
                var cellId = target.attributes && target.attributes.getNamedItem('cell-id') && target.attributes.getNamedItem('cell-id').value ?
                    target.attributes.getNamedItem('cell-id').value : '';
                var targetArray = Ember.$(target).parents('[cell-id=contextMenu]');
                var targetButtonArray = Ember.$(target).parents('[cell-id=menuPanel]');

                if (cellId === 'menuPanel' || (targetButtonArray && targetButtonArray.length > 0)) {
                    var symRowData = selectedRow.getProperties('symbolInfo');

                    if (symRowData && symRowData.symbolInfo) {
                        this.set('symInfo', symRowData.symbolInfo);
                    }

                    return;
                }

                if (cellId !== 'contextMenu' && targetArray.length <= 0) {   // In case target is the top most element (closest() is not working for IE)
                    var rowData = selectedRow.getProperties('secAccNum', 'qty', 'avgCst', 'costVal', 'symbol', 'symbolInfo');
                    var widgetController = ControllerFactory.createController(this.container, 'controller:trade/widgets/mobile/portfolio-details');
                    var viewName = 'trade/widgets/mobile/portfolio-details';
                    var symbolInfo = rowData.symbolInfo;
                    var currentPortfolio = rowData.secAccNum;

                    if (symbolInfo && rowData.symbol && currentPortfolio) {
                        widgetController.set('currentHolding', sharedService.getService('trade').holdingDS.getHolding(currentPortfolio, rowData.symbol, symbolInfo.get('exg')));
                    }

                    widgetController.set('holdingArray', this.get('arrangedContent'));
                    widgetController.set('rowData', rowData);
                    widgetController.set('parentController', this);
                    widgetController.set('selectedRow', selectedRow);
                    widgetController.initializeWidget({wn: 'portfolio-details'});

                    sharedService.getService('priceUI').showChildView(viewName, widgetController, this.get('app.lang.labels.portfolioDetails'), 'portfolio-' + this.get('wkey'));
                } else {
                    var width;

                    if (targetArray.length > 0) {
                        target = targetArray[0];
                    }

                    if (target.style.width === 100 + '%') {
                        this._collapsePortfolioRow(target);
                    } else {
                        width = 100 + '%';
                        target.getElementsByClassName('icon-angle-right')[0].className = 'glyphicon icon-angle-left font-5x-l';
                        this.set('isContextPanel', true);

                        if (this.get('previousRow') !== target) {
                            this._collapsePortfolioRow(this.get('previousRow'), true);
                        }
                    }

                    this.set('previousRow', target);
                    target.style.width = width;
                }
            }
        }
    }
});
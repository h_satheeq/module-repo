import HeaderCell from '../../dual-cells/expanded-header-cell';
import ExpandedColumnMixin from '../../table-mixins/expanded-column-mixin';

export default HeaderCell.extend(ExpandedColumnMixin, {
    templateName: 'table/views/trade/portfolio/quantity-header-cell'
});
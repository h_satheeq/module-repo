import Ember from 'ember';
import OrderSearch from '../order-search';
import HeaderCell from '../../../../views/table/dual-cells/header-cell';
import TableRow from '../../../../views/table/table-row';
import SymbolCell from '../../../../views/table/trade/order-list/symbol-cell';
import QuantityCell from '../../../../views/table/trade/order-list/quantity-cell';
import StatusCell from '../../../../views/table/trade/order-list/status-cell';
import ClassicHeaderCell from '../../../../views/table/classic-header-cell';
import MoreHeaderCell from '../../../../views/table/more-header-cell';
import ExpandedHeaderCell from '../../../../views/table/dual-cells/expanded-header-cell';
import ContextMenuMobile from '../../../../views/table/mobile/context-menu-cell';
import ControllerFactory from '../../../../controllers/controller-factory';
import layoutConfig from '../../../../config/layout-config';
import sharedService from '../../../../models/shared/shared-service';
import utils from '../../../../utils/utils';
import AppEvents from '../../../../app-events';
import appConfig from '../../../../config/app-config';
import userSettings from '../../../../config/user-settings';

export default OrderSearch.extend({
    isNativeDevice: navigator.isNativeDevice,

    rowHeight: 80,
    isOddEvenRowStyleDisabled: true,
    rowData: undefined,
    params: undefined,
    disableExpand: true,
    isExpandedView: true,
    showAdvancedFilterPopup: false,
    clickedOrder: undefined,
    title: 'Recent Orders',
    contextPath: 'trade/widgets/mobile/components/order-list-context-panel',

    oneWayContent: Ember.computed.oneWay('arrangedContent'),
    isRenderingEnabled: false,
    containerHeight: '',
    dateDisplayFormat: userSettings.displayFormat.dateFormat,

    styleSettings: {
        contextMenuRatio: 37 / 40,
        symbolColumnWidth: 150
    },

    onLoadWidget: function () {
        this._super();
        this.set('appLayout', layoutConfig);

        if (this.get('isChildView')) {
            this.set('containerHeight', 'order-list-popup-container-height');
        } else {
            this.set('containerHeight', 'order-list-container-height');
        }

        this.set('displayToDate', utils.formatters.convertToDisplayTimeFormat(this.get('toDate'), this.dateDisplayFormat));
        this.set('displayFromDate', utils.formatters.convertToDisplayTimeFormat(this.get('fromDate'), this.dateDisplayFormat));
    },

    onAfterRender: function () {
        var that = this;

        Ember.run.later(function () {
            that.set('isRenderingEnabled', true);
        }, 1);
    },

    onLanguageChanged: function () {
        this._super();
        this.refreshTableComponent();
    },

    onClearData: function () {
        this._super();

        this.set('showAdvancedFilterPopup', false);
    },

    headerNames: function () {
        var app = this.app.lang.labels;
        return {firstValue: app.quantity, secondValue: app.price, thirdValue: app.netValue};
    }.property('app.lang.labels'),

    setCellViewsScopeToGlobal: function () {
        Ember.SymbolCell = SymbolCell;
        Ember.QuantitiesCell = QuantityCell;
        Ember.StatusCell = StatusCell;
        Ember.HeaderCell = HeaderCell;
        Ember.TableRow = TableRow;
        Ember.ClassicHeaderCell = ClassicHeaderCell;
        Ember.MoreHeaderCell = MoreHeaderCell;
        Ember.ExpandedHeaderCell = ExpandedHeaderCell;
        Ember.ContextMenuMobile = ContextMenuMobile;
    },

    cellViewsForColumns: {
        symbolCell: 'Ember.SymbolCell',
        quantityCell: 'Ember.QuantitiesCell',
        statusCell: 'Ember.StatusCell',
        contextMenuMobile: 'Ember.ContextMenuMobile'
    },

    onSearchFiledChanged: function () {
        Ember.run.debounce(this, this.sendSearchRequest, 250);
    }.observes('symbol', 'orderId'),

    actions: {
        clickRow: function (selectedRow) {
            var target = event.target ? event.target : event.srcElement;
            var rowData = selectedRow.getProperties('isRoot', 'symbol', 'ordSts', 'exg', 'minQty', 'disQty', 'pendQty', 'symbolInfo', 'clOrdId', 'ordSide', 'ordTyp', 'crdDte', 'lstUptdTme', 'expTime', 'ordQty', 'price', 'comsn', 'netOrdVal', 'avgPrice', 'tif', 'fillQty', 'filledQty', 'ordVal', 'txt', 'isAmendEnabled', 'mubOrdNum', 'isCancelEnabled');
            var clickedOrder = rowData.mubOrdNum ? sharedService.getService('trade').orderDS.getOrder(rowData.mubOrdNum) : undefined;

            if (target) {
                var cellId = target.attributes && target.attributes.getNamedItem('cell-id') && target.attributes.getNamedItem('cell-id').value ?
                    target.attributes.getNamedItem('cell-id').value : '';
                var targetArray = Ember.$(target).parents('[cell-id=contextMenu]');
                var targetButtonArray = Ember.$(target).parents('[cell-id=menuPanel]');

                if (cellId === 'menuPanel' || (targetButtonArray && targetButtonArray.length > 0)) {
                    return;
                }

                if (cellId !== 'contextMenu' && targetArray.length <= 0) {   // In case target is the top most element (closest() is not working for IE)
                    var widgetController = ControllerFactory.createController(this.container, 'controller:trade/widgets/mobile/order-details');
                    var viewName = 'trade/widgets/mobile/order-details';

                    widgetController.set('orderArray', this.get('arrangedContent'));
                    widgetController.set('rowData', rowData ? rowData : clickedOrder);
                    widgetController.initializeWidget({wn: 'order-details'});

                    sharedService.getService('priceUI').showChildView(viewName, widgetController, this.get('app.lang.labels.orderDetails'), 'orderSearch-' + this.get('wkey'));
                } else {
                    var width;

                    if (targetArray.length > 0) {
                        target = targetArray[0];
                    }

                    if (target.style.width === 100 + '%') {
                        target.style.removeProperty('width');
                        this.set('isContextPanel', false);
                    } else {
                        width = 100 + '%';
                        this.set('isContextPanel', true);

                        if (this.get('previousRow')) {
                            this.get('previousRow').style.removeProperty('width');
                        }
                    }

                    this.set('previousRow', target);
                    target.style.width = width;
                }
            }

            this.set('rowData', rowData);
        },

        showOrderTicket: function (isCancel) {
            var rowData = this.get('rowData');

            if (rowData.isAmendEnabled || rowData.isCancelEnabled) {
                sharedService.getService('tradeUI').loadAmendCancel(this.container, isCancel, rowData, this.get('invoker'));
                sharedService.getService('priceUI').closeChildView('trade/widgets/mobile/order-search');
            }
        },

        loadQuoteSummary: function (stock) {
            var that = this;
            var rowData = stock.getProperties('exg', 'sym', 'inst');
            var quoteMenuId = appConfig.widgetId ? appConfig.widgetId.quoteMenuId : '';

            if (rowData && quoteMenuId) {
                sharedService.getService('sharedUI').navigateMenu(quoteMenuId);

                Ember.run.next(function () {
                    AppEvents.onSymbolChanged(rowData.sym, rowData.exg, rowData.inst, that.get('selectedLink'));
                });
            }
        },

        onSearchOrder: function () {
            this._super();
            this.set('showAdvancedFilterPopup', false);
        },

        onAdvancedFilter: function () {
            this.set('showAdvancedFilterPopup', true);
        },

        onAdvancedSearchCancel: function () {
            this.set('showAdvancedFilterPopup', false);
        },

        pickDate: function (isFromDate) {
            var that = this;
            var today = new Date();
            var toDate = this.get('toDate');
            var fromDate = this.get('fromDate');
            var fromDateOptions = {date: fromDate, mode: 'date', maxDate: toDate.getTime()};
            var toDateOptions = {date: toDate, mode: 'date', maxDate: today.getTime(), minDate: fromDate.getTime()};
            var options = isFromDate ? fromDateOptions : toDateOptions;

            if (window.plugins && window.plugins.datePicker) {
                window.plugins.datePicker.show(options, function (selectedDate) {
                    if (selectedDate) {
                        that.set(isFromDate ? 'fromDate' : 'toDate', selectedDate);
                        that.set(isFromDate ? 'displayFromDate' : 'displayToDate', utils.formatters.convertToDisplayTimeFormat(selectedDate, 'DD-MM-YYYY'));
                    }
                });
            }
        }
    }
});

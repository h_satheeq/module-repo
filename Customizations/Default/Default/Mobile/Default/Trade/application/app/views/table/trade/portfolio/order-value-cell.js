import Ember from 'ember';
import ExpandedCell from '../../dual-cells/expanded-cell';
import utils from '../../../../utils/utils';
import userSettings from '../../../../config/user-settings';
import fieldMetaConfig from '../../../../config/field-meta-config';

export default ExpandedCell.extend({
    templateName: 'table/views/trade/portfolio/order-value-cell',

    formattedThirdValue: Ember.computed(function () {
        var thirdValue = this.get('row') ? this.get('row.mktPrice') : undefined;
        var decimalPlaces = (fieldMetaConfig.portfolio && fieldMetaConfig.portfolio.decimalPlaces) ? fieldMetaConfig.portfolio.decimalPlaces : this.get('column.noOfDecimalPlaces');

        return utils.formatters.formatNumber(thirdValue, decimalPlaces || decimalPlaces === 0 ? decimalPlaces : userSettings.displayFormat.decimalPlaces);
    }).property('row.mktPrice'),

    formattedFourthValue: Ember.computed(function () {
        var value = this.get('row') ? this.get('row.gainLoss') : undefined;
        return this._getFormattedValue(value);
    }).property('row.gainLoss'),

    formattedFifthValue: Ember.computed(function () {
        var value = this.get('row') ? this.get('row.gainLossPer') : undefined;
        var decimalPlaces = this.get('column.noOfDecimalPlaces');

        return utils.formatters.formatNumberPercentage(value, decimalPlaces || decimalPlaces === 0 ? decimalPlaces : userSettings.displayFormat.decimalPlaces);
    }).property('row.gainLossPer'),

    formattedSixthValue: Ember.computed(function () {
        var value = this.get('row') ? this.get('row.mktVal') : undefined;
        var decimalPlaces = (fieldMetaConfig.portfolio && fieldMetaConfig.portfolio.decimalPlaces) ? fieldMetaConfig.portfolio.decimalPlaces : this.get('column.noOfDecimalPlaces');

        return utils.formatters.formatNumber(value, decimalPlaces || decimalPlaces === 0 ? decimalPlaces : userSettings.displayFormat.decimalPlaces);
    }).property('row.mktVal'),

    gainLossCss: function () {
        return this.get('row.gainLoss') >= 0 ? 'up-fore-color' : 'down-fore-color';
    }.property('row.gainLoss'),

    gainLossPerCss: function () {
        return this.get('row.gainLossPer') >= 0 ? 'up-fore-color' : 'down-fore-color';
    }.property('row.gainLossPer'),

    _getFormattedValue: function (value) {
        var decimalPlaces = this.get('column.noOfDecimalPlaces');
        return utils.formatters.formatNumber(value, decimalPlaces || decimalPlaces === 0 ? decimalPlaces : userSettings.displayFormat.decimalPlaces);
    },

    mktPriceCss: function () {
        var row = this.get('row');
        var css = 'up-fore-color glyphicon-triangle-top';

        if (row) {
            var symbolInfo = row.get('symbolInfo') ? row.get('symbolInfo') : undefined;

            if (symbolInfo) {
                css = symbolInfo.chg >= 0 ? 'up-fore-color glyphicon-triangle-top' : 'down-fore-color glyphicon-triangle-bottom';
            }
        }

        return css;
    }.property('row')
});
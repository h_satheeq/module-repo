import Ember from 'ember';
import CashStatements from '../cash-statements';

// Cell Views
import DualCell from '../../../../views/table/dual-cells/dual-cell';
import ClassicHeaderCell from '../../../../views/table/classic-header-cell';
import HeaderCell from '../../../../views/table/dual-cells/header-cell';
import ClassicCell from '../../../../views/table/classic-cell';
import TableRow from '../../../../views/table/table-row';
import utils from '../../../../utils/utils';

export default CashStatements.extend({
    isNativeDevice: navigator.isNativeDevice,

    onLoadWidget: function () {
        this._super();

        var today = new Date();
        this.set('displayToDate', utils.formatters.convertToDisplayTimeFormat(today, 'DD-MM-YYYY'));

        var fromDate = new Date();
        fromDate.setDate(fromDate.getDate() - this.DefaultCashStatementPeriod);
        this.set('displayFromDate', utils.formatters.convertToDisplayTimeFormat(fromDate, 'DD-MM-YYYY'));
    },

    setCellViewsScopeToGlobal: function () {
        Ember.HeaderCell = HeaderCell;
        Ember.ClassicHeaderCell = ClassicHeaderCell;
        Ember.ClassicCell = ClassicCell;
        Ember.DualCell = DualCell;
        Ember.TableRow = TableRow;
    },

    cellViewsForColumns: {
        classicCell: 'Ember.ClassicCell',
        dualCell: 'Ember.DualCell'
    },

    setData: function () {
        if (this.get('toDate') && this.get('fromDate') && this.get('currentPortfolio.tradingAccId')) {
            this.sendRequest();
        }
    }.observes('toDate', 'fromDate', 'currentPortfolio'),

    onLanguageChanged: function () {
        this._super();

        this.set('columnDeclarations', []);
        this.onLoadWidget();
        this.refreshTableComponent();
    },

    actions: {
        pickDate: function (isFromDate) {
            var that = this;
            var today = new Date();
            var toDate = this.get('toDate');
            var fromDate = this.get('fromDate');
            var fromDateOptions = {date: fromDate, mode: 'date', maxDate: toDate.getTime()};
            var toDateOptions = {date: toDate, mode: 'date', maxDate: today.getTime(), minDate: fromDate.getTime()};
            var options = isFromDate ? fromDateOptions : toDateOptions;

            if (window.plugins && window.plugins.datePicker) {
                window.plugins.datePicker.show(options, function (selectedDate) {
                    if (selectedDate) {
                        that.set(isFromDate ? 'fromDate' : 'toDate', selectedDate);
                        that.set(isFromDate ? 'displayFromDate' : 'displayToDate', utils.formatters.convertToDisplayTimeFormat(selectedDate, 'DD-MM-YYYY'));
                    }
                });
            }
        }
    }
});
import Ember from 'ember';
import ExpandedCell from '../../dual-cells/expanded-cell';
import utils from '../../../../utils/utils';
import userSettings from '../../../../config/user-settings';
import fieldMetaConfig from '../../../../config/field-meta-config';

export default ExpandedCell.extend({
    templateName: 'table/views/trade/portfolio/quantity-cell',

    formattedSecondValue: Ember.computed(function () {
        var secondValue = this.get('cellContent') ? this.get('cellContent').secondValue : undefined;
        var decimalPlaces = (fieldMetaConfig.portfolio && fieldMetaConfig.portfolio.decimalPlaces) ? fieldMetaConfig.portfolio.decimalPlaces : this.get('column.noOfDecimalPlaces');

        return utils.formatters.formatNumber(secondValue, decimalPlaces || decimalPlaces === 0 ? decimalPlaces : userSettings.displayFormat.decimalPlaces);
    }).property('cellContent'),

    formattedThirdValue: Ember.computed(function () {
        var thirdValue = this.get('cellContent') ? this.get('cellContent').thirdValue : undefined;
        var decimalPlaces = (fieldMetaConfig.portfolio && fieldMetaConfig.portfolio.decimalPlaces) ? fieldMetaConfig.portfolio.decimalPlaces : this.get('column.noOfDecimalPlaces');

        return utils.formatters.formatNumber(thirdValue, decimalPlaces || decimalPlaces === 0 ? decimalPlaces : userSettings.displayFormat.decimalPlaces);
    }).property('cellContent')
});
import Ember from 'ember';
import HeaderCell from '../../../../views/table/dual-cells/header-cell';
import TableRow from '../../../../views/table/table-row';
import SymbolCell from '../../../../views/table/trade/order-list/symbol-cell';
import QuantityCell from '../../../../views/table/trade/order-list/quantity-cell';
import StatusCell from '../../../../views/table/trade/order-list/status-cell';
import ClassicHeaderCell from '../../../../views/table/classic-header-cell';
import MoreHeaderCell from '../../../../views/table/more-header-cell';
import ExpandedHeaderCell from '../../../../views/table/dual-cells/expanded-header-cell';
import ContextMenuMobile from '../../../../views/table/mobile/context-menu-cell';
import responsiveHandler from '../../../../helpers/responsive-handler';
import orderList from '../order-list';
import ControllerFactory from '../../../../controllers/controller-factory';
import layoutConfig from '../../../../config/layout-config';
import tradeConstants from '../../../../models/trade/trade-constants';
import sharedService from '../../../../models/shared/shared-service';
import utils from '../../../../utils/utils';
import AppEvents from '../../../../app-events';
import appConfig from '../../../../config/app-config';

export default orderList.extend({
    rowHeight: 80,
    isOddEvenRowStyleDisabled: true,
    rowData: undefined,
    params: undefined,
    disableExpand: true,
    isExpandedView: true,
    clickedOrder: undefined,
    title: 'Recent Orders',  // TODO [Arosha] Load all Child View Titles from Language file
    contextPath: 'trade/widgets/mobile/components/order-list-context-panel',

    oneWayContent: Ember.computed.oneWay('arrangedContent'),
    isRenderingEnabled: false,
    containerHeight: '',

    styleSettings: {
        contextMenuRatio: 37 / 40,
        symbolColumnWidth: 150
    },

    onLoadWidget: function () {
        this._super();
        this.set('appLayout', layoutConfig);

        if (this.get('isChildView')) {
            this.set('containerHeight', 'order-list-popup-container-height');
        } else {
            this.set('containerHeight', 'order-list-container-height');
        }
    },

    onAfterRender: function () {
        var that = this;

        Ember.run.later(function () {
            that.set('isRenderingEnabled', true);
        }, 1);
    },

    onLanguageChanged: function () {
        this._super();
        this.refreshTableComponent();
    },

    headerNames: function () {
        var app = this.app.lang.labels;
        return {firstValue: app.quantity, secondValue: app.price, thirdValue: app.netValue};
    }.property('app.lang.labels'),

    setCellViewsScopeToGlobal: function () {
        Ember.SymbolCell = SymbolCell;
        Ember.QuantitiesCell = QuantityCell;
        Ember.StatusCell = StatusCell;
        Ember.HeaderCell = HeaderCell;
        Ember.TableRow = TableRow;
        Ember.ClassicHeaderCell = ClassicHeaderCell;
        Ember.MoreHeaderCell = MoreHeaderCell;
        Ember.ExpandedHeaderCell = ExpandedHeaderCell;
        Ember.ContextMenuMobile = ContextMenuMobile;
    },

    cellViewsForColumns: {
        symbolCell: 'Ember.SymbolCell',
        quantityCell: 'Ember.QuantitiesCell',
        statusCell: 'Ember.StatusCell',
        contextMenuMobile: 'Ember.ContextMenuMobile'
    },

    filterOrders: function () {
        var statusFilter = this.get('isFilterStatus');

        if (statusFilter) {
            var filteredOrders = this.get('masterContent').filter((function () {    //eslint-disable-line
                return function (order) {
                    var lastUpdatedDate = order.lstUptdTme ? order.lstUptdTme.split(utils.Constants.StringConst.Space)[0] : '';
                    var todayDate = new Date().toJSON().split(utils.Constants.StringConst.time)[0];

                    return !lastUpdatedDate || lastUpdatedDate === todayDate || (order.ordSts !== tradeConstants.OrderStatus.Filled && order.ordSts !== tradeConstants.OrderStatus.Canceled);
                };
            })(this));
            this.set('filteredContent', filteredOrders);

            this.set('content', filteredOrders);
        }

        this._super();
    },

    onResponsive: function (responsiveArgs) {
        var controller = responsiveArgs.controller;

        if (responsiveArgs.responsiveLevel >= 1) {
            controller.set('lblClass', 'wdgttl-drp-dwn-btn-width font-x-l');
        } else {
            controller.set('lblClass', 'font-x-l');
        }
    },

    onResize: function () {
        var that = this;

        if (that.responsive) {
            var resHandler = that.responsive;

            // Call onResize when values are changed
            Ember.run.debounce(resHandler, 'onResize', 1);
        }
    }.observes('currentStatus', 'currentAccount'),

    initializeResponsive: function () {
        var that = this;

        Ember.run.next(function () {
            that.set('responsive', responsiveHandler.create({
                controller: that,
                widgetId: 'orderList-' + that.get('wkey'),
                callback: that.onResponsive
            }));

            that.responsive.addList('status-free', [
                {id: 'statusDropdown', width: 5}
            ]);

            that.responsive.initialize();
        });
    }.on('didInsertElement'),

    actions: {
        clickRow: function (selectedRow) {
            var target = event.target ? event.target : event.srcElement;
            var rowData = selectedRow.getProperties('isRoot', 'symbol', 'ordSts', 'exg', 'minQty', 'disQty', 'pendQty', 'symbolInfo', 'clOrdId', 'ordSide', 'ordTyp', 'crdDte', 'lstUptdTme', 'expTime', 'ordQty', 'price', 'comsn', 'netOrdVal', 'avgPrice', 'tif', 'fillQty', 'filledQty', 'ordVal', 'txt', 'isAmendEnabled', 'mubOrdNum', 'isCancelEnabled');
            var clickedOrder = rowData.mubOrdNum ? sharedService.getService('trade').orderDS.getOrder(rowData.mubOrdNum) : undefined;

            if (target) {
                var cellId = target.attributes && target.attributes.getNamedItem('cell-id') && target.attributes.getNamedItem('cell-id').value ?
                    target.attributes.getNamedItem('cell-id').value : '';
                var targetArray = Ember.$(target).parents('[cell-id=contextMenu]');
                var targetButtonArray = Ember.$(target).parents('[cell-id=menuPanel]');

                if (cellId === 'menuPanel' || (targetButtonArray && targetButtonArray.length > 0)) {
                    return;
                }

                if (cellId !== 'contextMenu' && targetArray.length <= 0) {   // In case target is the top most element (closest() is not working for IE)
                    var widgetController = ControllerFactory.createController(this.container, 'controller:trade/widgets/mobile/order-details');
                    var viewName = 'trade/widgets/mobile/order-details';

                    widgetController.set('orderArray', this.get('arrangedContent'));
                    widgetController.set('rowData', clickedOrder ? clickedOrder : rowData);
                    widgetController.initializeWidget({wn: 'order-details'});

                    sharedService.getService('priceUI').showChildView(viewName, widgetController, this.get('app.lang.labels.orderDetails'), 'orderList-' + this.get('wkey'));
                } else {
                    var width;

                    if (targetArray.length > 0) {
                        target = targetArray[0];
                    }

                    if (target.style.width === 100 + '%') {
                        target.style.removeProperty('width');
                        this.set('isContextPanel', false);
                    } else {
                        width = 100 + '%';
                        this.set('isContextPanel', true);

                        if (this.get('previousRow')) {
                            this.get('previousRow').style.removeProperty('width');
                        }
                    }

                    this.set('previousRow', target);
                    target.style.width = width;
                }
            }

            this.set('rowData', rowData);
        },

        showOrderTicket: function (isCancel) {
            var rowData = this.get('rowData');

            if (rowData.isAmendEnabled || rowData.isCancelEnabled) {
                sharedService.getService('tradeUI').loadAmendCancel(this.container, isCancel, rowData, this.get('invoker'));
                sharedService.getService('priceUI').closeChildView('trade/widgets/mobile/order-list');
            }
        },

        loadQuoteSummary: function (stock) {
            var that = this;
            var rowData = stock.getProperties('exg', 'sym', 'inst');
            var quoteMenuId = appConfig.widgetId ? appConfig.widgetId.quoteMenuId : '';

            if (rowData && quoteMenuId) {
                sharedService.getService('sharedUI').navigateMenu(quoteMenuId);

                Ember.run.next(function () {
                    AppEvents.onSymbolChanged(rowData.sym, rowData.exg, rowData.inst, that.get('selectedLink'));
                });
            }
        }
    }
});

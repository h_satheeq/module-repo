import BaseComponent from './base-component';
import LanguageDataStore from '../models/shared/language/language-data-store';

export default BaseComponent.extend({
    app: LanguageDataStore.getLanguageObj(),

    actions: {
        addPrice: function () {
            var currentValue = this.get('value') ? this.get('value') : 0;
            var newValue = (parseFloat(currentValue) + parseFloat(this.get('step'))).toFixed(2);

            if (newValue <= this.get('max')) {
                this.set('value', newValue);
            }
        },

        redusePrice: function () {
            var currentValue = this.get('value') ? this.get('value') : 0;
            var newValue = (parseFloat(currentValue) - parseFloat(this.get('step'))).toFixed(2);

            if (newValue >= this.get('min')) {
                this.set('value', newValue);
            }
        }
    }
});
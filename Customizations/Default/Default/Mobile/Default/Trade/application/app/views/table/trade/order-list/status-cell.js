import Ember from 'ember';
import utils from '../../../../utils/utils';
import ExpandedCell from '../../dual-cells/expanded-cell';
import languageDataStore from '../../../../models/shared/language/language-data-store';
import tradeConstants from '../../../../models/trade/trade-constants';

export default ExpandedCell.extend({
    templateName: 'table/views/trade/order-list/status-cell',
    app: languageDataStore.getLanguageObj(),
    progressPercentage: '100%',
    isEnablePer: false,
    filledColor: undefined,

    didInsertElement: function () {
        this.setOrderStsCss();
    }.on('didInsertElement'),

    setOrderStsCss: function () {
        var currentValue = this.get('cellContent') ? this.get('cellContent').firstValue : '';
        var fillColor = 'up-back-color';

        var orderStsObj = tradeConstants.OrderStatus;
        var isEnablePerVal = false;

        this.set('progressPercentage', '100%');

        if (currentValue === orderStsObj.PartiallyFilled) {
            isEnablePerVal = true;
            this.set('progressPercentage', '0%');
            this.calculateFilledPercentage();
        } else if (currentValue === orderStsObj.Rejected) {
            fillColor = 'down-back-color ellipsis';
        } else if (currentValue === orderStsObj.Filled) {
            fillColor = 'up-back-color ellipsis';
        } else if (currentValue === orderStsObj.SendToBrokerNew) {
            fillColor = 'pending-back-color ellipsis';
        } else if (currentValue === orderStsObj.New) {
            fillColor = 'pending-back-color ellipsis';
        } else {
            fillColor = 'ellipsis';
        }

        this.set('isEnablePer', isEnablePerVal);
        this.set('filledColor', fillColor);
    },

    formattedFirstValue: function () {
        var labels = this.get('app').lang.labels;
        var currentValue = this.get('cellContent') ? this.get('cellContent').firstValue : '';
        this.setOrderStsCss();

        return currentValue && labels['orderStatus_' + currentValue] ? labels['orderStatus_' + currentValue] : currentValue;
    }.property('cellContent.firstValue'),

    calculateFilledPercentage: function () {
        var pendingQty = this.get('row') ? this.get('row.pendQty') : undefined;
        var ordQty = this.get('row') ? this.get('row.ordQty') : undefined;

        var percentage = ((ordQty - pendingQty) * 100) / (ordQty);
        percentage = percentage === 0 && this.get('cellContent') && this.get('cellContent').firstValue !== tradeConstants.OrderStatus.PartiallyFilled ? 100 : percentage;

        var formattedPer = utils.formatters.formatNumberPercentage(percentage, 0);

        this.set('progressPercentage', formattedPer);
    },

    formattedSecondValue: function () {
        var labels = this.get('app').lang.labels;
        var currentValue = this.get('cellContent') ? this.get('cellContent').secondValue : '';
        var labelKey = this.get('column') && this.get('column').name && utils.validators.isAvailable(currentValue) ? [this.get('column').name, currentValue].join('_') : undefined;

        return labelKey && labels[labelKey] ? labels[labelKey] : currentValue;
    }.property('cellContent'),

    formattedThirdValue: Ember.computed(function () {
        var pendingQty = this.get('row') ? this.get('row.pendQty') : undefined;
        var ordQty = this.get('row') ? this.get('row.ordQty') : undefined;

        this.calculateFilledPercentage();

        return this.addFormat(ordQty - pendingQty);
    }).property('row.pendQty')
});

import Ember from 'ember';
import TradeUIService from './trade-ui-service';
import sharedService from '../../models/shared/shared-service';
import ControllerFactory from '../../controllers/controller-factory';
import tradeConstants from '../../models/trade/trade-constants';
import OrderParams from '../../models/trade/business-entities/order-params';
import appConfig from '../../config/app-config';

export default TradeUIService.extend({
    subscriptionKey: 'tradeUI',

    onLayoutReady: function (appLayout) {
        this.initializeComponents(appLayout);
        this._loadGainLossComponent();
    },

    _loadGainLossComponent: function () {
        var layoutNameAppTitle = 'trade/widgets/mobile/components/title-gain-loss';
        var gainLossComponent = sharedService.getService('sharedUI').getService('title-gain-loss');

        sharedService.getService('sharedUI').getService('titleBar').renderAppTitleTemplate(layoutNameAppTitle, gainLossComponent);
    },

    showOrderTicket: function (container, isBid, symInfo) {
        var widgetController = ControllerFactory.createController(container, 'controller:trade.widgets.order-ticket.order-ticket-mobile', true);
        var invoker = widgetController.get('invoker');
        var newOrder = OrderParams.create();

        if (invoker && Ember.$.isFunction(invoker.set)) {
            newOrder.set('ordSide', isBid ? tradeConstants.OrderSide.Sell : tradeConstants.OrderSide.Buy);
            newOrder.set('action', tradeConstants.OrderAction.New);
            invoker.set('orderParams', newOrder);

            if (symInfo) {
                widgetController.prepare(symInfo.sym, symInfo.exg, symInfo.inst);

                if (invoker.get('symbolInfo') && invoker.get('symbolInfo').sym === symInfo.sym) {
                    invoker.updateHolding();
                }
            }
        }

        this._loadOrderTicketWidget();
    },

    loadAmendCancel: function (container, isCancel, rowData) {
        var clickedOrder = rowData.mubOrdNum ? sharedService.getService('trade').orderDS.getOrder(rowData.mubOrdNum) : undefined;

        var params = {
            widgetArgs: {
                sym: rowData.symbolInfo.sym,
                exg: rowData.symbolInfo.exg,
                inst: rowData.symbolInfo.inst,
                side: rowData.ordSide,
                qty: rowData.qty
            }
        };

        clickedOrder.set('displayPrice', clickedOrder.price);
        params.widgetArgs.order = clickedOrder;

        if (rowData.isAmendEnabled || rowData.isCancelEnabled) {
            if (clickedOrder && params.widgetArgs.sym && params.widgetArgs.exg) {
                if (isCancel && clickedOrder.get('isCancelEnabled')) {
                    clickedOrder.set('action', tradeConstants.OrderAction.Cancel);
                }

                if (!isCancel && clickedOrder && clickedOrder.get('isAmendEnabled')) {
                    clickedOrder.set('action', tradeConstants.OrderAction.Amend);
                }
            }

            this._loadOrderTicketWidget();

            var widgetController = ControllerFactory.createController(this.container, 'controller:trade.widgets.order-ticket.order-ticket-mobile', true);
            widgetController.initializeWidget({wn: 'controller:trade/widgets/order-ticket/order-ticket-mobile'.split('/').pop()}, params);
        }
    },

    _loadOrderTicketWidget: function () {
        var orderTicketMenuId = appConfig.widgetId ? appConfig.widgetId.orderTicketMenuId : '';

        if (orderTicketMenuId) {
            sharedService.getService('sharedUI').navigateMenu(orderTicketMenuId);
        }
    }
});

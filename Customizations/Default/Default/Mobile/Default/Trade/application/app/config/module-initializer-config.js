import PriceDataModuleInitializer from '../models/price/initializers/price-data-module-initializer';
import SharedUIModuleInitializerMobile from '../controllers/shared/initializers/mobile/shared-ui-module-initializer-mobile';
import PriceUIModuleInitializerMobile from '../controllers/price/initializers/mobile/price-ui-module-initializer-mobile';
import sharedDataModuleInitializer from '../models/shared/initializers/shared-data-module-initializer';
import TradeDataModuleInitializer from '../models/trade/initializers/trade-data-module-initializer';
import TradeUIModuleInitializerMobile from '../controllers/trade/initializers/mobile/trade-ui-module-initializer-mobile';
// Please don't delete below place holders - Used by build scripts.
//{{IMPORT}}

export default {
    modules: [
        sharedDataModuleInitializer,
        SharedUIModuleInitializerMobile.create(),
        PriceDataModuleInitializer.create(),
        PriceUIModuleInitializerMobile.create(),
        TradeDataModuleInitializer.create(),
        TradeUIModuleInitializerMobile.create()//{{CREATE}}
    ]
};

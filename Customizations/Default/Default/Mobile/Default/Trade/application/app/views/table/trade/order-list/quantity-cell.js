import userSettings from '../../../../config/user-settings';
import ExpandedCell from '../../dual-cells/expanded-cell';
import utils from '../../../../utils/utils';

export default ExpandedCell.extend({
    templateName: 'table/views/trade/order-list/quantity-cell',

    formattedFirstValue: function () {
        var isDefaultValueCheck = this.get('column') ? this.get('column').isDefaultValueCheck : false;
        var firstValue = this.get('cellContent') ? this.get('cellContent').firstValue : undefined;

        if (isDefaultValueCheck) {
            var defaultValue = this.get('column') ? this.get('column').defaultValue : undefined;

            if (firstValue === defaultValue) {
                return userSettings.displayFormat.noValue;
            }
        }

        return this.addFormat(firstValue);
    }.property('cellContent'),

    formattedSecondValue: function () {
        var isDefaultValueCheck = this.get('column') ? this.get('column').isDefaultValueCheck : false;
        var secondValue = this.get('cellContent') ? this.get('cellContent').secondValue : undefined;
        var decimalPlaces = this.get('column.noOfDecimalPlaces');

        if (isDefaultValueCheck) {
            var defaultValue = this.get('column') ? this.get('column').defaultValue : undefined;

            if (secondValue === defaultValue) {
                return userSettings.displayFormat.noValue;
            }
        }

        return utils.formatters.formatNumber(secondValue, decimalPlaces || decimalPlaces === 0 ? decimalPlaces : userSettings.displayFormat.decimalPlaces);
    }.property('cellContent'),

    formattedThirdValue: function () {
        var isDefaultValueCheck = this.get('column') ? this.get('column').isDefaultValueCheck : false;
        var thirdValue = this.get('cellContent') ? this.get('cellContent').thirdValue : undefined;
        var decimalPlaces = this.get('column.noOfDecimalPlaces');

        if (isDefaultValueCheck) {
            var defaultValue = this.get('column') ? this.get('column').defaultValue : undefined;

            if (thirdValue === defaultValue) {
                return userSettings.displayFormat.noValue;
            }
        }

        return utils.formatters.formatNumber(thirdValue, decimalPlaces || decimalPlaces === 0 ? decimalPlaces : userSettings.displayFormat.decimalPlaces);
    }.property('cellContent')
});

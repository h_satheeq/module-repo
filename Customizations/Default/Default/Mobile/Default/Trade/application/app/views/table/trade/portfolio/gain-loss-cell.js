import Ember from 'ember';
import ExpandedCell from '../../dual-cells/expanded-cell';
import utils from '../../../../utils/utils';
import userSettings from '../../../../config/user-settings';

export default ExpandedCell.extend({
    templateName: 'table/views/trade/portfolio/gain-loss-cell',

    formattedFirstValue: Ember.computed(function () {
        var firstValue = this.get('cellContent') ? this.get('cellContent').firstValue : undefined;
        return this._getFormattedValue(firstValue);
    }).property('cellContent'),

    formattedSecondValue: Ember.computed(function () {
        var secondValue = this.get('cellContent') ? this.get('cellContent').secondValue : undefined;
        return this._getFormattedValue(secondValue);
    }).property('cellContent'),

    formattedThirdValue: Ember.computed(function () {
        var thirdValue = this.get('cellContent') ? this.get('cellContent').thirdValue : undefined;
        return this._getFormattedValue(thirdValue);
    }).property('cellContent'),

    gainLossCss: function () {
        return this.get('firstValue') >= 0 ? 'up-fore-color' : 'down-fore-color';
    }.property('firstValue'),

    gainLossPerCss: function () {
        return this.get('secondValue') >= 0 ? 'up-fore-color' : 'down-fore-color';
    }.property('secondValue'),

    _getFormattedValue: function (value) {
        var decimalPlaces = this.get('column.noOfDecimalPlaces');
        return utils.formatters.formatNumber(value, decimalPlaces || decimalPlaces === 0 ? decimalPlaces : userSettings.displayFormat.decimalPlaces);
    }
});
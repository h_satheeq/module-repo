import TradeUIModuleInitializer from '../../../../controllers/trade/initializers/trade-ui-module-initializer';
import TradeUIServiceMobile from '../../../../controllers/trade/trade-ui-service-mobile';
import brokerageInitializer from '../brokerage-initializer';
import appConfig from '../../../../config/app-config';

export default TradeUIModuleInitializer.extend({
    postInitialize: function () {
        if (appConfig.customisation.isBrokerageSelectionEnabled) {
            brokerageInitializer.prepareBrokerageView();
            brokerageInitializer.showBrokerageView();
        }
    },

    createService: function () {
        return TradeUIServiceMobile.create();
    }
});

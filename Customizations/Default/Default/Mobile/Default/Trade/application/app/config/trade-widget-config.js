export default {
    portfolio: {
        defaultColumnMapping: {
            contextMenu: {id: 'contextMenu', width: 15, name: 'contextMenu', headerName: '', headerStyle: 'text-center', iconClass: 'glyphicon glyphicon-chevron-right', isColumnSortDisabled: true, type: 'contextMenuMobile', buttonFunction: 'showOrderTicket'},
            symInfo: {id: 'symbolInfo.dispProp1', secondId: 'lDes', thirdId: 'mktPrice', width: 125, headerCellView: 'Ember.MoreHeaderCell', headerName: 'symbol', headerSecondName: 'description', headerThirdName: 'price', headerStyle: 'text-left-header', secondHeaderStyle: 'h-left', cellStyle: 'text-left-header', sortKeyword: 'symbol', type: 'orderValueCell', firstValueStyle: 'font-x-l symbol-fore-color bold'},
            // gainLoss: {id: 'gainLoss', secondId: 'gainLossPer', thirdId: 'mktVal', width: 45, headerName: 'Gain/Loss', headerSecondName: 'G/L Per', headerThirdName: 'portfolioValue', sortKeyword: 'pendBuy', dataType: 'int', type: 'gainLossCell', firstValueStyle: 'font-x-l colour-normal', headerCellView: 'Ember.ExpandedHeaderCell'},
            qty: {id: 'qty', secondId: 'costVal', thirdId: 'avgCst', width: 60, headerName: 'quantity', headerSecondName: 'costValue', headerThirdName: 'avgCost', sortKeyword: 'qty', dataType: 'int', type: 'quantityCell', firstValueStyle: 'font-x-l colour-normal bold', headerCellView: 'Ember.QuantityHeaderCell', secondHeaderStyle: 'rtl', headerStyle: 'pad-m-r'}
        },

        defaultColumnIds: ['contextMenu', 'symInfo', 'qty'],

        moreColumnIds: ['contextMenu', 'symInfo', 'qty'],

        nonRemovableColumnIds: ['contextMenu', 'symInfo', 'qty'],

        tableParams: {
            MaxTableWidth: 5700,
            numOfFixedColumns: 0
        }
    },

    orderList: {
        defaultColumnMapping: {
            contextMenu: {id: 'contextMenu', width: 15, name: 'contextMenu', headerName: '', headerStyle: 'text-center', iconClass: 'glyphicon glyphicon-chevron-right', isColumnSortDisabled: true, type: 'contextMenuMobile', buttonFunction: 'showOrderTicket'},
            symInfo: {id: 'symbolInfo.dispProp1', width: 125, headerCellView: 'Ember.MoreHeaderCell', secondId: 'symbolSDes', thirdId: 'ordSide', name: 'orderSide', mappingName: 'ordTyp', sortKeyword: 'symbol', headerName: 'symbol', headerSecondName: 'description', headerThirdName: 'side', type: 'symbolCell', firstValueStyle: 'symbol-fore-color bold', secondValueStyle: 'pad-m-t', headerStyle: 'text-left-header', cellStyle: 'text-left-header'},
            // ordQty: {id: 'ordQty', width: 45, headerCellView: 'Ember.ExpandedHeaderCell', secondId: 'price', thirdId: 'netOrdVal', sortKeyword: 'ordQty', headerName: 'quantity', headerSecondName: 'price', headerThirdName: 'netValue', type: 'quantityCell', dataType: 'int', firstValueStyle: 'fore-color', secondValueStyle: 'fade-fore-color bold pad-m-t', headerStyle: 'text-right-header', cellStyle: 'text-right-header', noOfSecValueDecimalPlaces: 2, isDefaultValueCheck: true, defaultValue: -1},
            ordSts: {id: 'ordSts', width: 60, headerCellView: 'Ember.ExpandedHeaderCell', secondId: 'tif', name: 'tifType', thirdId: 'fillQty', sortKeyword: 'ordSts', headerName: 'status', headerSecondName: 'tif', type: 'statusCell', firstValueStyle: 'up-back-color', secondValueStyle: 'fade-for-color pad-m-t ', thirdValueStyle: 'green-dark', headerStyle: 'text-right-header pad-m-r', cellStyle: 'text-left-header', title: 'txt'}
        },

        defaultColumnIds: ['contextMenu', 'symInfo', 'ordSts'],

        moreColumnIds: ['contextMenu', 'symInfo', 'ordSts'],

        nonRemovableColumnIds: ['contextMenu', 'symInfo', 'ordSts'],

        tableParams: {
            MaxTableWidth: 5700,
            numOfFixedColumns: 0
        }
    },

    transactionHistory: {
        defaultColumnMapping: {     // Column Object parameters : id, width, headerName, sortKeyword, multiValueIds, cellStyle, sortDisable, firstValueStyle, isBold, dataType, backgroundColour,
            typ: {id: 'typ', width: 25, headerCellView: 'Ember.ClassicHeaderCell', name: 'type', headerName: 'type', headerStyle: 'text-center', iconClass: 'glyphicon glyphicon-edit', isColumnSortDisabled: true, type: 'button', title: 'amend'},
            mtd: {id: 'mtd', width: 30, headerCellView: 'Ember.ClassicHeaderCell', headerName: 'method', headerStyle: 'text-center', iconClass: 'glyphicon glyphicon-remove', isColumnSortDisabled: true, type: 'button', title: 'cancel'},
            symbol: {id: 'symbol', width: 80, headerCellView: 'Ember.ClassicHeaderCell', sortKeyword: 'symbol', headerName: 'symbol', type: 'classicCell', firstValueStyle: 'colour-normal bold', headerStyle: 'text-left-header', cellStyle: 'text-left-header'},
            amount: {id: 'Amount', width: 60, headerCellView: 'Ember.ClassicHeaderCell', sortKeyword: 'amnt', headerName: 'amount', type: 'classicCell', firstValueStyle: 'colour-normal bold', headerStyle: 'text-left-header', cellStyle: 'text-left-header'},
            secAccNum: {id: 'secAccNum', width: 60, headerCellView: 'Ember.ClassicHeaderCell', sortKeyword: 'secAccNum', headerName: 'portfolioId', type: 'classicCell', firstValueStyle: 'colour-normal bold', headerStyle: 'text-left-header', cellStyle: 'text-left-header'},
            sts: {id: 'sts', width: 50, headerCellView: 'Ember.ClassicHeaderCell', sortKeyword: 'sts', headerName: 'status', type: 'classicStatusCell', firstValueStyle: 'colour-normal bold', headerStyle: 'text-left-header', cellStyle: 'text-left-header'},
            refId: {id: 'refId', width: 35, headerCellView: 'Ember.ClassicHeaderCell', sortKeyword: 'refId', headerName: 'refId', type: 'classicMappingCell', firstValueStyle: 'colour-normal bold', headerStyle: 'text-left-header', cellStyle: 'text-left-header'},
            bankAcnt: {id: 'bankAcnt', width: 60, headerCellView: 'Ember.ClassicHeaderCell', sortKeyword: 'bankAcnt', headerName: 'bankAccount', type: 'classicCell', dataType: 'int', firstValueStyle: 'colour-normal bold'},
            valDte: {id: 'valDte', width: 50, headerCellView: 'Ember.ClassicHeaderCell', sortKeyword: 'valDte', headerName: 'date', type: 'classicCell', dataType: 'float', firstValueStyle: 'colour-normal bold'}
        },

        defaultColumnIds: ['typ', 'mtd', 'symbol', 'amount', 'secAccNum', 'sts', 'refId', 'bankAcnt', 'valDte'],

        tableParams: {
            MaxTableWidth: 5700,
            numOfFixedColumns: 0
        }
    },

    orderSearch: {
        defaultColumnMapping: {     // Column Object parameters : id, width, headerName, sortKeyword, multiValueIds, cellStyle, sortDisable, firstValueStyle, isBold, dataType, backgroundColour,
            sym: {id: 'symbol', width: 105, headerCellView: 'Ember.ClassicHeaderCell', sortKeyword: 'symbol', headerName: 'symbol', type: 'classicCell', firstValueStyle: 'symbol-fore-color bold', headerStyle: 'text-left-header', cellStyle: 'text-left-header'},
            clOrdId: {id: 'clOrdId', width: 80, headerCellView: 'Ember.ClassicHeaderCell', sortKeyword: 'clOrdId', headerName: 'orderId', type: 'classicCell', firstValueStyle: 'colour-normal bold', headerStyle: 'text-left-header', cellStyle: 'text-left-header'},
            ordSts: {id: 'ordSts', width: 80, headerCellView: 'Ember.ClassicHeaderCell', sortKeyword: 'ordSts', headerName: 'status', type: 'classicStatusCell', firstValueStyle: 'colour-normal bold', headerStyle: 'text-left-header', cellStyle: 'text-left-header', title: 'txt'},
            ordSide: {id: 'ordSide', width: 45, headerCellView: 'Ember.ClassicHeaderCell', name: 'orderSide', sortKeyword: 'ordSide', headerName: 'side', type: 'classicMappingCell', headerStyle: 'text-left-header', cellStyle: 'text-left-header'},
            ordQty: {id: 'ordQty', width: 80, headerCellView: 'Ember.ClassicHeaderCell', sortKeyword: 'ordQty', headerName: 'quantity', type: 'classicCell', dataType: 'int', firstValueStyle: 'colour-normal bold'},
            price: {id: 'price', width: 60, headerCellView: 'Ember.ClassicHeaderCell', sortKeyword: 'price', headerName: 'price', type: 'orderPriceCell', secondId: 'ordTyp', dataType: 'float', firstValueStyle: 'colour-normal bold'},
            cumQty: {id: 'cumQty', width: 60, headerCellView: 'Ember.ClassicHeaderCell', sortKeyword: 'fillQty', headerName: 'filledQty', type: 'classicCell', dataType: 'int', firstValueStyle: 'colour-normal bold', isBlink: true, blinkUpStyle: 'blink-font-style-change', blinkDownStyle: 'blink-font-style-change', isDefaultValueCheck: true, defaultValue: -1}
        },

        defaultColumnIds: ['sym', 'clOrdId', 'ordSts', 'ordSide', 'ordQty', 'price', 'cumQty'],

        moreColumnIds: ['clOrdId', 'ordSts', 'ordSide', 'ordQty', 'price', 'cumQty'],

        nonRemovableColumnIds: ['sym'],

        tableParams: {
            MaxTableWidth: 5700,
            numOfFixedColumns: 1
        }
    },

    cashStatement: {
        defaultColumnMapping: {     // Column Object parameters : id, width, headerCellView, headerName, sortKeyword,  cellStyle, sortDisable, dataType, type, headerStyle, cellStyle
            date: {id: 'date', secondId: 'settleDate', width: 85, headerCellView: 'Ember.HeaderCell', sortKeyword: 'date', headerName: 'date', headerSecondName: 'settlementDate', type: 'dualCell', dataType: 'date', headerStyle: 'text-left-header', firstValueStyle: 'text-left-header font-x-l bold', secondValueStyle: 'text-left-header font-x-l pad-m-t fade-fore-color'},
            des: {id: 'des', width: 150, headerCellView: 'Ember.ClassicHeaderCell', sortKeyword: 'description', headerName: 'description', type: 'classicCell', headerStyle: 'text-center', cellStyle: 'text-left-header v-top font-l', firstValueStyle: 'white-space-normal line-height-l'},
            amount: {id: 'amount', secondId: 'balance', width: 87, headerCellView: 'Ember.HeaderCell', sortKeyword: 'amount', headerName: 'amount', headerSecondName: 'accBal', type: 'dualCell', dataType: 'float', positiveNegativeChange: true, headerStyle: 'text-right-header', firstValueStyle: 'font-x-l h-right ellipsis', secondValueStyle: 'font-x-l fade-fore-color pad-m-t h-right ellipsis'}
            // {id: 'date', width: 80, headerCellView: 'Ember.ClassicHeaderCell', sortKeyword: 'Date', headerName: 'Date', type: 'classicCell', dataType: 'dateTime', headerStyle: 'text-left-header', cellStyle: 'text-left-header'},
            // {id: 'trnsType', width: 95, headerCellView: 'Ember.ClassicHeaderCell', sortKeyword: 'trnsType', headerName: 'Transaction Type', type: 'classicCell', headerStyle: 'text-left-header', cellStyle: 'text-left-header'},
            // {id: 'trnsRef', width: 110, headerCellView: 'Ember.ClassicHeaderCell', sortKeyword: 'tnsRef', headerName: 'Transaction Ref.', type: 'classicCell', headerStyle: 'text-left-header', cellStyle: 'text-left-header'}
            // {id: 'amount', width: 100, headerCellView: 'Ember.ClassicHeaderCell', sortKeyword: 'amount', headerName: 'Amount', type: 'dualChange', dataType: 'float', positiveNegativeChange: true, headerStyle: 'text-center', cellStyle: 'text-left-header'},
            // {id: 'balance', width: 100, headerCellView: 'Ember.ClassicHeaderCell', sortKeyword: 'balance', headerName: 'Balance', type: 'classicCell', dataType: 'float', headerStyle: 'text-center', cellStyle: 'text-center'},
            // {id: 'settleDate', width: 80, headerCellView: 'Ember.ClassicHeaderCell', sortKeyword: 'settleDate', headerName: 'Settle Date', type: 'classicCell', dataType: 'dateTime', headerStyle: 'text-center', cellStyle: 'text-center'}
        },

        defaultColumnIds: ['date', 'des', 'amount'],

        tableParams: {
            MaxTableWidth: 5700
        }
    },

    rightsSubscriptionList: {
        defaultColumnMapping: {     // Column Object parameters : id, width, headerCellView, headerName, sortKeyword,  cellStyle, sortDisable, dataType, type, headerStyle, cellStyle
            sym: {id: 'sym', width: 60, headerCellView: 'Ember.ClassicHeaderCell', sortKeyword: '', headerName: 'symbol', type: 'classicCell', headerStyle: 'text-left-header', cellStyle: 'text-left-header', firstValueStyle: 'symbol-fore-color bold'},
            dSym: {id: 'dSym', width: 80, headerCellView: 'Ember.ClassicHeaderCell', sortKeyword: '', headerName: 'sDescription', type: 'classicCell', firstValueStyle: 'symbol-fore-color bold', headerStyle: 'text-left-header', cellStyle: 'text-left-header'},
            statusLabel: {id: 'statusLabel', width: 110, headerCellView: 'Ember.ClassicHeaderCell', sortKeyword: '', headerName: 'status', headerStyle: 'text-left-header', cellStyle: 'text-left-header', type: 'classicCell', firstValueStyle: 'bold fore-color'},
            qty: {id: 'qty', width: 60, headerCellView: 'Ember.ClassicHeaderCell', sortKeyword: '', headerName: 'quantity', type: 'classicCell', dataType: 'int', firstValueStyle: 'colour-normal bold'},
            avgPrice: {id: 'avgPrice', width: 50, headerCellView: 'Ember.ClassicHeaderCell', sortKeyword: '', headerName: 'avgPrice', type: 'classicCell', dataType: 'float', firstValueStyle: 'colour-normal bold', headerStyle: 'text-left-header', cellStyle: 'text-left-header'},
            netValue: {id: 'netValue', width: 80, headerCellView: 'Ember.ClassicHeaderCell', sortKeyword: '', headerName: 'netValue', type: 'classicCell', dataType: 'float', firstValueStyle: 'colour-normal', headerStyle: 'text-left-header', cellStyle: 'text-left-header'},
            subNo: {id: 'subNo', width: 110, headerCellView: 'Ember.ClassicHeaderCell', sortKeyword: '', headerName: 'refId', type: 'classicCell', firstValueStyle: 'colour-normal', headerStyle: 'text-left-header', cellStyle: 'text-left-header'},
            subDate: {id: 'subDate', width: 80, headerCellView: 'Ember.ClassicHeaderCell', sortKeyword: '', headerName: 'date', type: 'classicCell', dataType: 'date', firstValueStyle: 'colour-normal', headerStyle: 'text-left-header', cellStyle: 'text-left-header'}
        },

        defaultColumnIds: ['sym', 'dSym', 'statusLabel', 'qty', 'avgPrice', 'netValue', 'subNo', 'subDate'],

        tableParams: {
            MaxTableWidth: 5700
        }
    },

    accountSummary: {
        accountSummaryFields: [
            {lanKey: 'unsettledSales', dataField: 'penSet', formatter: 'C', style: '', isValueBaseCss: true},
            {lanKey: 'cashBalance', dataField: 'balance', formatter: 'C', style: '', isValueBaseCss: true},
            {lanKey: 'blockedAndOutstanding', dataField: 'blkAmt', formatter: 'C', style: '', isValueBaseCss: true},
            {lanKey: 'portfolioValue', dataField: 'valuation', formatter: 'C', style: '', isValueBaseCss: true},
            {lanKey: 'odLimit', dataField: 'odLmt', formatter: 'C', style: '', isValueBaseCss: true},
            {lanKey: 'buyingPower', dataField: 'buyPwr', formatter: 'C', style: '', isValueBaseCss: true},
            {lanKey: 'payableAmount', dataField: 'payAmt', formatter: 'C', style: ''},
            {lanKey: 'totalPortfolio', dataField: 'totVal', formatter: 'C', style: ''}
        ]
    },

    mutualFundTransaction: {
        defaultColumnMapping: {
            fndId: {id: 'fndId', width: 50, headerCellView: 'Ember.ClassicHeaderCell', sortKeyword: 'code', headerName: 'code', type: 'classicCell', firstValueStyle: 'symbol-fore-color bold', headerStyle: 'text-left-header', cellStyle: 'text-left-header'},
            fndName: {id: 'fndName', width: 120, headerCellView: 'Ember.ClassicHeaderCell', sortKeyword: 'fndNme', headerName: 'name', type: 'classicCell', firstValueStyle: 'symbol-fore-color bold', headerStyle: 'text-left-header', cellStyle: 'text-left-header'},
            transType: {id: 'transType', width: 90, headerCellView: 'Ember.ClassicHeaderCell', name: 'mfTransType', sortKeyword: 'transType', headerName: 'type', type: 'classicMappingCell', firstValueStyle: 'symbol-fore-color bold', headerStyle: 'text-left-header', cellStyle: 'text-left-header'},
            transNo: {id: 'transNo', width: 85, headerCellView: 'Ember.ClassicHeaderCell', sortKeyword: 'transNo', headerName: 'transNo', type: 'classicCell', firstValueStyle: 'colour-normal', headerStyle: 'text-left-header', cellStyle: 'text-left-header'},
            traDte: {id: 'traDte', width: 130, headerCellView: 'Ember.ClassicHeaderCell', sortKeyword: 'traDte', headerName: 'transactionDate', type: 'classicCell', dataType: 'dateTime', firstValueStyle: 'colour-normal', headerStyle: 'text-left-header', cellStyle: 'text-left-header'},
            amt: {id: 'amt', width: 80, headerCellView: 'Ember.ClassicHeaderCell', sortKeyword: 'amt', headerName: 'orderAmount', type: 'classicCell', dataType: 'float', firstValueStyle: 'colour-normal'}
        },

        defaultColumnIds: ['fndId', 'fndName', 'transType', 'transNo', 'traDte', 'amt'],

        tableParams: {
            MaxTableWidth: 5700,
            numOfFixedColumns: 0
        }
    },

    mutualFundSubRedem: {
        defaultColumnMapping: {
            fndId: {id: 'fndId', width: 50, headerCellView: 'Ember.ClassicHeaderCell', sortKeyword: 'fndId', headerName: 'code', type: 'classicCell', firstValueStyle: 'symbol-fore-color bold', headerStyle: 'text-left-header', cellStyle: 'text-left-header'},
            fundName: {id: 'fundName', width: 120, headerCellView: 'Ember.ClassicHeaderCell', sortKeyword: 'fundName', headerName: 'name', type: 'classicCell', firstValueStyle: 'symbol-fore-color bold', headerStyle: 'text-left-header', cellStyle: 'text-left-header'},
            transType: {id: 'transType', width: 70, headerCellView: 'Ember.ClassicHeaderCell', name: 'mfTransType', sortKeyword: 'transType', headerName: 'type', type: 'classicMappingCell', firstValueStyle: 'symbol-fore-color bold', headerStyle: 'text-left-header', cellStyle: 'text-left-header'},
            transNo: {id: 'transNo', width: 85, headerCellView: 'Ember.ClassicHeaderCell', sortKeyword: 'transNo', headerName: 'transNo', type: 'classicCell', firstValueStyle: 'colour-normal', headerStyle: 'text-left-header', cellStyle: 'text-left-header'},
            traDte: {id: 'traDte', width: 130, headerCellView: 'Ember.ClassicHeaderCell', sortKeyword: 'traDte', headerName: 'transactionDate', type: 'classicCell', dataType: 'dateTime', firstValueStyle: 'colour-normal', headerStyle: 'text-left-header', cellStyle: 'text-left-header'},
            curr: {id: 'curr', width: 60, headerCellView: 'Ember.ClassicHeaderCell', sortKeyword: 'curr', headerName: 'curr', type: 'classicCell', firstValueStyle: 'colour-normal', headerStyle: 'text-left-header', cellStyle: 'text-left-header'},
            netAmount: {id: 'netAmount', width: 80, headerCellView: 'Ember.ClassicHeaderCell', sortKeyword: 'netAmount', headerName: 'netAmount', type: 'classicCell', dataType: 'float', firstValueStyle: 'colour-normal'},
            fee: {id: 'fee', width: 60, headerCellView: 'Ember.ClassicHeaderCell', sortKeyword: 'fee', headerName: 'orderFee', type: 'classicCell', dataType: 'float', firstValueStyle: 'colour-normal'},
            units: {id: 'units', width: 60, headerCellView: 'Ember.ClassicHeaderCell', sortKeyword: 'units', headerName: 'orderUnits', type: 'classicCell', dataType: 'int', firstValueStyle: 'colour-normal'},
            transStatus: {id: 'transStatus', width: 70, headerCellView: 'Ember.ClassicHeaderCell', sortKeyword: 'transStatus', headerName: 'status', type: 'classicCell', firstValueStyle: 'bold fore-color', headerStyle: 'text-left-header', cellStyle: 'text-left-header'}
        },

        defaultColumnIds: ['fndId', 'fundName', 'transType', 'transNo', 'traDte', 'curr', 'netAmount', 'fee', 'units', 'transStatus'],

        tableParams: {
            MaxTableWidth: 5700,
            numOfFixedColumns: 3
        }
    },

    mutualFundList: {
        defaultColumnMapping: {
            sym: {id: 'symbol', width: 50, headerCellView: 'Ember.ClassicHeaderCell', sortKeyword: 'symbol', headerName: 'code', type: 'classicCell', firstValueStyle: 'symbol-fore-color bold', headerStyle: 'text-left-header', cellStyle: 'text-left-header'},
            fndNme: {id: 'fndNme', width: 120, headerCellView: 'Ember.ClassicHeaderCell', sortKeyword: 'fndNme', headerName: 'name', type: 'classicCell', firstValueStyle: 'symbol-fore-color bold', headerStyle: 'text-left-header', cellStyle: 'text-left-header'},
            subscription: {id: 'subscription', width: 25, headerCellView: 'Ember.ClassicHeaderCell', name: 'subscription', headerName: '', headerStyle: 'text-center', iconClass: 'glyphicon glyphicon-plus font-x-s top-zero', isColumnSortDisabled: true, type: 'button', title: 'subscription', buttonFunction: 'onSubscription', buttonStyle: 'btn btn-buy-small btn-animation mf-btn-icon'},
            redemption: {id: 'redemption', width: 25, headerCellView: 'Ember.ClassicHeaderCell', name: 'redemption', headerName: '', headerStyle: 'text-center', iconClass: 'glyphicon glyphicon-minus font-x-s top-zero', isColumnSortDisabled: true, type: 'button', title: 'redemption', buttonFunction: 'onRedemption', buttonStyle: 'btn btn-sell-small btn-animation mf-btn-icon'},
            untPri: {id: 'untPri', width: 100, headerCellView: 'Ember.ClassicHeaderCell', sortKeyword: 'untPri', headerName: 'unitPrice', type: 'classicCell', dataType: 'float', firstValueStyle: 'colour-normal'}
        },

        defaultColumnIds: ['sym', 'fndNme', 'subscription', 'redemption', 'untPri'],

        tableParams: {
            MaxTableWidth: 5700,
            numOfFixedColumns: 0
        }
    },

    mutualFundHolding: {
        defaultColumnMapping: {
            sym: {id: 'symbol', width: 50, headerCellView: 'Ember.ClassicHeaderCell', sortKeyword: 'symbol', headerName: 'code', type: 'classicCell', firstValueStyle: 'symbol-fore-color bold', headerStyle: 'text-left-header', cellStyle: 'text-left-header'},
            fndNme: {id: 'fndNme', width: 120, headerCellView: 'Ember.ClassicHeaderCell', sortKeyword: 'fndNme', headerName: 'name', type: 'classicCell', firstValueStyle: 'symbol-fore-color bold', headerStyle: 'text-left-header', cellStyle: 'text-left-header'},
            subscription: {id: 'subscription', width: 25, headerCellView: 'Ember.ClassicHeaderCell', name: 'subscription', headerName: '', headerStyle: 'text-center', iconClass: 'glyphicon glyphicon-plus font-x-s top-zero', isColumnSortDisabled: true, type: 'button', title: 'subscription', buttonFunction: 'onSubscription', buttonStyle: 'btn btn-buy-small btn-animation mf-btn-icon'},
            redemption: {id: 'redemption', width: 25, headerCellView: 'Ember.ClassicHeaderCell', name: 'redemption', headerName: '', headerStyle: 'text-center', iconClass: 'glyphicon glyphicon-minus font-x-s top-zero', isColumnSortDisabled: true, type: 'button', title: 'redemption', buttonFunction: 'onRedemption', buttonStyle: 'btn btn-sell-small btn-animation mf-btn-icon'},
            qty: {id: 'qty', width: 60, headerCellView: 'Ember.ClassicHeaderCell', sortKeyword: 'qty', headerName: 'quantity', type: 'classicCell', dataType: 'int', firstValueStyle: 'colour-normal'}
        },

        defaultColumnIds: ['sym', 'fndNme', 'subscription', 'redemption', 'qty'],

        tableParams: {
            MaxTableWidth: 5700,
            numOfFixedColumns: 0
        }
    }
};
/* global c3 */
import Ember from 'ember';
import utils from '../../../../utils/utils';
import responsiveHandler from '../../../../helpers/responsive-handler';
import BaseController from '../../../base-controller';
import layoutConfig from '../../../../config/layout-config';
import sharedService from '../../../../models/shared/shared-service';
import fieldMetaConfig from '../../../../config/field-meta-config';

export default BaseController.extend({
    title: 'Portfolio Details',
    portfolioPer: '',
    todayGainLossLbl: '',
    decimalPlaces: '',
    isDisabledSubscription: false,

    onLoadWidget: function () {
        this._super();

        var decimalPlaces = this.get('currentHolding.symbolInfo.deci');
        var portfolioMetaConfig = fieldMetaConfig.portfolio;

        if (portfolioMetaConfig && portfolioMetaConfig.decimalPlaces) {
            decimalPlaces = portfolioMetaConfig.decimalPlaces;
        }

        this.set('decimalPlaces', decimalPlaces);
        sharedService.getService('trade').accountDS.subscribeHoldingCalculation();
    },

    onPrepareData: function () {
        this.set('layoutConfig', layoutConfig);
        this.updatePortfolioChart();
        var lowResolutionWidth = 340;

        if (window.screen.width <= lowResolutionWidth) {
            this.set('isDisabledSubscription', true);
        }
    },

    initializeResponsive: function () {
        var lowResolutionWidth = 340;

        this.set('responsive', responsiveHandler.create({controller: this, widgetId: 'hnav-panel-mobile', callback: this.onResponsive}));
        this.responsive.addList('hnav-panel-mobile', [{id: 'hnav-panel-mobile', width: lowResolutionWidth}]);
        this.responsive.initialize();
    },

    gainLossCss: function () {
        var gainLoss = this.get('currentHolding') ? this.get('currentHolding').gainLoss : 0.00;
        return gainLoss > 0 ? 'up-fore-color' : (gainLoss === 0.00 ? 'fade-fore-color' : 'down-fore-color');
    }.property('currentHolding'),

    gainLossLbl: function () {
        var gainLoss = this.get('currentHolding') ? this.get('currentHolding').gainLoss : '';
        return gainLoss >= 0 ? this.get('app').lang.labels.unrealizedGain : this.get('app').lang.labels.unrealizedLoss;
    }.property('currentHolding'),

    todayGain: function () {
        var symbolInfo = this.get('currentHolding').symbolInfo;
        return symbolInfo.chg * this.get('currentHolding').qty;
    }.property('currentHolding'),

    todayGainPer: function () {
        var symbolInfo = this.get('currentHolding').symbolInfo;
        var todayGainPer = symbolInfo.pctChg;

        this.set('todayGainPerCss', todayGainPer > 0 ? 'up-fore-color' : (todayGainPer === 0.00 ? 'fade-fore-color' : 'down-fore-color'));
        this.set('todayGainLossLbl', todayGainPer >= 0 ? this.get('app').lang.labels.todayGain : this.get('app').lang.labels.todayLoss);
        return todayGainPer;
    }.property('currentHolding'),

    showOrderTicket: function (side) {
        var currentHolding = this.get('currentHolding');

        if (currentHolding) {
            var symInfo = {sym: currentHolding.symbol, exg: currentHolding.exg, inst: currentHolding.instruTyp};
            sharedService.getService('tradeUI').showOrderTicket(this.container, side, symInfo);
        }

        sharedService.getService('priceUI').closeChildView('trade/widgets/mobile/portfolio-details');
    },

    onAfterRender: function () {
        var that = this;

        this.initializeButtonAnimation();

        /* setTimeout(function () {
         that._drawAccountsChart();
         }, 10);*/

        Ember.run.later(function () {
            that._drawAccountsChart();
        }, 1000);
    },

    _drawAccountsChart: function () {
        var portCahrt = c3.generate({
            data: {
                columns: [],
                type: 'pie',
                order: null
            },

            pie: {
                title: 'Portfolio Breakdown',
                width: 100,
                label: {
                    show: false
                }
            },

            color: {
                pattern: ['#7092bf', '#e3e3e3']
            },

            legend: {
                show: false
            },

            tooltip: {
                show: false
            },

            bindto: '#c3-portfolio-chart',

            padding: {
                right: 100
            }
        });

        this.set('portfolioChart', portCahrt);
        this.updatePortfolioChart();
    },

    updatePortfolioChart: function () {
        var chart = this.get('portfolioChart');
        var mktValue = this.get('currentHolding').mktVal;
        var totalMktValue = sharedService.getService('trade').accountDS.getTotalMktValue();

        if (!chart || !chart.load || !mktValue) {
            return;
        }

        var portfolioChartData = [['1', mktValue], ['2', totalMktValue - mktValue]];
        var portfolioPer = mktValue * 100 / totalMktValue;

        this.set('portfolioPer', utils.formatters.formatNumberPercentage(portfolioPer, 1));

        chart.load({
            columns: portfolioChartData
        });
    },

    navigateHoldings: function (isNextHolding) {
        var currentHolding = this.get('currentHolding');
        var holdingArray = this.get('holdingArray');
        var currentSymbol = currentHolding.symbol;
        var currentHoldingIndex = 0;

        Ember.$.each(holdingArray, function (key, holding) {
            if (holding.symbol === currentSymbol) {
                currentHoldingIndex = key;

                return false;
            }
        });

        var newHoldingIndex = isNextHolding ? currentHoldingIndex + 1 : currentHoldingIndex - 1;

        if (newHoldingIndex < holdingArray.length && newHoldingIndex > -1) {
            var newHolding = holdingArray[newHoldingIndex];

            if (newHolding && newHolding.symbolInfo && newHolding.symbol && newHolding.secAccNum) {
                this.set('currentHolding', sharedService.getService('trade').holdingDS.getHolding(newHolding.secAccNum, newHolding.symbol, newHolding.exg));
            }
        }

        var that = this;

        Ember.run.later(function () {
            that._drawAccountsChart();
        }, 100);
    },

    isTradingDisabledExg: function () {
        var tradeService = sharedService.getService('trade');
        return tradeService && !tradeService.userDS.isTradeEnabledExchange(this.get('currentHolding.exg'));
    }.property('currentHolding.exg'),

    actions: {
        onBuyMore: function () {
            this.showOrderTicket(false);
        },

        navigateHoldings: function (isNextHolding) {
            this.navigateHoldings(isNextHolding);
        },

        onLiquidate: function () {
            this.showOrderTicket(true);
        },

        renderSubscribeWindow: function () {
            var parentController = this.get('parentController');

            if (parentController) {
                parentController.renderSubscribeWindow(this.get('selectedRow'));
            }
        }
    }
});
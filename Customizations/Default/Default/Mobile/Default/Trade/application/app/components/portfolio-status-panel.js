import Ember from 'ember';
import BaseComponent from './base-component';
import userSettings from '../config/user-settings';
import sharedService from '../models/shared/shared-service';

export default BaseComponent.extend({
    layoutName: 'components/portfolio-status-panel',

    currentPortfolioNo: 0,
    currentPortfolio: {},
    portfolioOptions: [],
    isToggleEnable: false,
    currency: '',
    portfolioDropdownOptions: Ember.A(),
    portfolioDes: sharedService.getService('trade').fieldConfigs.portfolioDes,

    onPrepareData: function () {
        this.setPortfolioCurrency();
    }.on('didInsertElement'),

    nextPortRightAction: function () {
        var that = this;

        if (this.get('currentPortfolioNo') < this.get('portfolioOptions').length - 1) {
            this.set('currentPortfolioNo', that.get('currentPortfolioNo') + 1);
        } else {
            this.set('currentPortfolioNo', 0);
        }

        this.sendAction('selectAction', this.get('portfolioOptions')[this.get('currentPortfolioNo')]);
    },

    nextPortLeftAction: function () {
        var that = this;

        if (this.get('currentPortfolioNo') === 0) {
            this.set('currentPortfolioNo', this.get('portfolioOptions').length - 1);
        } else {
            this.set('currentPortfolioNo', that.get('currentPortfolioNo') - 1);
        }

        this.sendAction('selectAction', this.get('portfolioOptions')[this.get('currentPortfolioNo')]);
    },

    setPortfolioCurrency: function () {
        this.set('currency', this.get('currentPortfolio.cashAccount.curr') ? this.get('currentPortfolio.cashAccount.curr') : userSettings.customisation.defaultCurrency);
    }.observes('currentPortfolio', 'currentPortfolio.cashAccount.curr'),

    actions: {
        nextPortRight: function () {
            this.nextPortRightAction();
        },

        nextPortLeft: function () {
            this.nextPortLeftAction();
        },

        onSelectPortfolio: function (item) {
            this.sendAction('selectAction', item);
        }
    }
});
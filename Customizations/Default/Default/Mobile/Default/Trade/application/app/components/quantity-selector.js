import Ember from 'ember';
import BaseComponent from './base-component';
import utils from '../utils/utils';
import languageDataStore from '../models/shared/language/language-data-store';

export default BaseComponent.extend({
    values: [],
    quantity: '',
    app: languageDataStore.getLanguageObj(),

    setValues: function () {
        var that = this;
        this.set('values', [{desc: '1K', value: 1000, pad: 'pad-s-lr'}, {desc: '100', value: 100, pad: 'pad-s-lr'}, {desc: '10', value: 10, pad: 'pad-s-lr'}, {desc: '1', value: 1, pad: 'pad-s-lr'}]);

        Ember.run.later(function () {
            var button = document.querySelectorAll('.btn-animation');

            for (var i = 0; i < button.length; i++) {
                button[i].onmousedown = that._clickAnimation;
            }
        }, 1);
    }.on('didInsertElement'),

    _clickAnimation: function (e) {
        var clickedElement = Ember.$(e.target);
        var buttonElement, topValue, leftValue;

        // Get button element
        if (clickedElement[0].localName === 'button') {
            buttonElement = clickedElement;
            topValue = buttonElement[0].clientHeight / 2;
            leftValue = buttonElement[0].clientWidth / 2;

        } else if (clickedElement.parent().closest('button') && clickedElement.parent().closest('button')[0] && clickedElement.parent().closest('button')[0].clientHeight) {
            buttonElement = clickedElement.parent().closest('button');
            topValue = buttonElement[0].clientHeight / 2;
            leftValue = buttonElement[0].clientWidth / 2;
        } else {
            leftValue = e.offsetX ? e.offsetX : e.layerX;
            topValue = e.offsetY ? e.offsetY : e.layerY;
        }

        var effect = document.createElement('div');

        effect.className = 'effect';
        effect.style.top = topValue + 'px';
        effect.style.left = leftValue + 'px';

        e.srcElement.appendChild(effect);

        setTimeout(function () {
            e.srcElement.removeChild(effect);
        }, 1100);
    },

    checkValue: function (quantity) {
        var orderQuantity = quantity;

        if (orderQuantity && orderQuantity < 0) {
            utils.messageService.showMessage(this.get('app').lang.messages.quantityGTZero, utils.Constants.MessageTypes.Warning, false);
            orderQuantity = this.get('quantity');
        }

        return orderQuantity;
    },

    actions: {
        addQuantity: function (value) {
            var currentQty = this.get('quantity') ? this.get('quantity') : 0;
            this.set('quantity', parseInt(currentQty, 10) + parseInt(value, 10));
        },

        resetQuantity: function () {
            this.set('quantity', 0);
        },

        reduseQuantity: function (value) {
            var currentQty = this.get('quantity') ? this.get('quantity') : 0;
            var newQuantity = parseInt(currentQty, 10) - parseInt(value, 10);
            var checkedValue = this.checkValue(newQuantity);

            this.set('quantity', checkedValue);
        }
    }
});
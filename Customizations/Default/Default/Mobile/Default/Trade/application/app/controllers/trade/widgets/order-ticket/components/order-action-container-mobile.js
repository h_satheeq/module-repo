import Ember from 'ember';
import OrderActionContainer from './order-action-container';
import utils from '../../../../../utils/utils';

export default OrderActionContainer.extend({
    layoutName: 'trade/widgets/order-ticket/components/order-action-container',

    sliderPlusButton: {isSliderActionCompleted: false, css: 'btn-default', view: '+'},
    sliderMinusButton: {isSliderActionCompleted: false, css: 'btn-default', view: '-'},
    selectorButton: {isSelectorActionCompleted: false, css: 'btn-default', view: '/'},

    mktPlaceholder: '',
    isNativeDevice: navigator.isNativeDevice,
    decimalPlaces: '',

    onReady: function () {
        this._super();
        this.checkScreenResolution();
        this.onDecimalPlacesChange();
    }.on('didInsertElement'),

    onInvokerSideChange: function () {
        var invoker = this.get('invoker');

        if (invoker && invoker.orderParams) {
            this.set('isBuy', invoker.orderParams.ordSide === '1');
        }
    }.observes('invoker.orderParams.ordSide'),

    onInvokerTypeChange: function () {
        var that = this;
        var invoker = this.get('invoker');
        var orderType = invoker.orderParams.ordTyp;
        var orderOptions = this.get('orderTypeOptions');

        if (invoker && invoker.orderParams) {
            this.set('isLimitOrder', orderType === '2');

            Ember.$.each(orderOptions, function (key, value) {
                if (value.code === orderType) {
                    that.set('selectedOrderType', value);
                }
            });
        }
    }.observes('invoker.orderParams.ordTyp'),

    onTifTypeChange: function () {
        this.loadTifDropdown();
    }.observes('invoker.orderParams.tif'),

    isPanelAvailable: function () {
        return this.get('sliderEnabled') || this.get('quantitySelectorEnabled');
    }.property('sliderEnabled', 'quantitySelectorEnabled'),

    orderSideToggleId: function () {
        return 'orderSideToggleSwitch' + this.get('wkey');
    }.property(),

    orderTypeToggleId: function () {
        return 'orderTypeToggleSwitch' + this.get('wkey');
    }.property(),

    onDecimalPlacesChange: function () {
        var noOfDecimalPlaces = this.get('invoker.symbolInfo.deci');
        var stepSize = this.get('invoker.orderParams.tickSize');

        if (noOfDecimalPlaces && noOfDecimalPlaces < 0) {
            noOfDecimalPlaces = utils.formatters.getDecimalPlaces(stepSize);
        }

        this.set('decimalPlaces', noOfDecimalPlaces);
    }.observes('invoker.symbolInfo', 'invoker.orderParams.tickSize'),

    checkScreenResolution: function () {
        var lowResolutionWidth = 340;

        if (window.screen.width <= lowResolutionWidth) {
            this.set('layoutCell', 'iphone-layout');
        }
    },

    setMarketPlaceholder: function () {
        var invoker = this.get('invoker');
        this.set('mktPlaceholder', invoker && invoker.orderStrategy && invoker.orderStrategy.isMarketOrder ?
        this.get('app').lang.labels.marketPrice + ' ' : '');
    }.observes('invoker.orderStrategy.isMarketOrder'),

    onLanguageChanged: function () {
        this._super();
        this.setMarketPlaceholder();
    },

    selectTifType: function () {
        var nativePicker = window.plugins && window.plugins.listpicker ? window.plugins.listpicker : '';
        var options = this.get('tifTypeOptions');
        var value = this.get('selectedTifType');
        var title = this.get('app').lang.labels.selectOption;

        this.clickEventHandler = this.onTifTypeChanged.bind(this);

        if (nativePicker && this.get('isNativeDevice')) {
            var config = {title: title, items: options, selectedValue: value.value};

            // Show the picker
            nativePicker.showPicker(config, this.clickEventHandler);
        } else {
            this._super();
        }
    },

    actions: {
        setStatus: function (stat) {
            var componentStatus = this.get('componentStatus');
            var stepSize = this.get('invoker.orderParams.tickSize');
            var noOfDecimalPlaces = this.get('decimalPlaces');

            switch (stat) {
                case componentStatus.sliderPlus:
                    this.set('selectorButton', {isSelectorActionCompleted: false, css: 'btn-default', view: '/'});

                    if (!this.get('sliderPlusButton').isSliderActionCompleted) {
                        this.set('sliderPlusButton', {isSliderActionCompleted: true, css: 'btn-default', view: '+'});
                    } else {
                        this.set('sliderPlusButton', {isSliderActionCompleted: false, css: 'btn-default', view: '+'});
                    }

                    var currentPrice = this.get('invoker.orderParams.displayPrice');
                    currentPrice = currentPrice && !isNaN(currentPrice) ? currentPrice : 0;

                    var newPrice = (parseFloat(currentPrice) + (stepSize ? stepSize : 0)).toFixed(noOfDecimalPlaces);
                    this.set('invoker.orderParams.displayPrice', newPrice);

                    break;

                case componentStatus.sliderMinus:
                    var orderParams = this.get('invoker.orderParams');

                    if (orderParams) {
                        stepSize = orderParams.getTickSize(true);
                    }

                    var currentDisplayPrice = this.get('invoker.orderParams.displayPrice') ? this.get('invoker.orderParams.displayPrice') : 0;
                    var newDisplayPrice = (parseFloat(currentDisplayPrice) - stepSize).toFixed(noOfDecimalPlaces);

                    this.set('selectorButton', {isSelectorActionCompleted: false, css: 'btn-default', view: '/'});

                    if (newDisplayPrice > 0) {
                        this.set('invoker.orderParams.displayPrice', newDisplayPrice);
                    }

                    break;

                case componentStatus.quantitySelector:
                    this.set('sliderPlusButton', {isSliderActionCompleted: false, css: 'btn-default', view: '+'});

                    if (!this.get('selectorButton').isSelectorActionCompleted) {
                        this.set('selectorButton', {isSelectorActionCompleted: true, css: 'btn-default', view: '/'});
                    } else {
                        this.set('selectorButton', {isSelectorActionCompleted: false, css: 'btn-default', view: '/'});
                    }

                    break;
            }

            this.sendAction('statAction', stat);
        },

        selectDate: function () {
            var expDateTime = this.get('expDateTime');
            var that = this;
            var minDate = new Date();

            if (!Ember.isIos) {
                minDate = minDate.getTime();
            }

            var options = {date: expDateTime, mode: 'date', minDate: minDate};

            if (window.plugins && window.plugins.datePicker) {
                window.plugins.datePicker.show(options, function (selectedDate) {
                    that.set('expDateTime', selectedDate);
                });
            }
        }
    }
});
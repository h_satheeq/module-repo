import Ember from 'ember';
import utils from '../../../../../utils/utils';
import appEvents from '../../../../../app-events';
import sharedService from '../../../../../models/shared/shared-service';
import layoutConfig from '../../../../../config/layout-config';
import BaseComponent from '../../../../../components/base-component';
import userSettings from '../../../../../config/user-settings';
import LanguageDataStore from '../../../../../models/shared/language/language-data-store';

export default BaseComponent.extend({
    layoutName: 'trade/widgets/mobile/components/title-gain-loss',
    gainLoss: '',
    gainLossPercentage: '',
    gainLossCss: '',
    gainLossLabel: '',
    appLayout: {},
    currency: userSettings.customisation.defaultCurrency,
    priceService: sharedService.getService('price'),
    tradeService: sharedService.getService('trade'),
    subscriptionSymNames: [],
    holdings: [],
    wkey: 'title-gain-loss-mobile',

    gainLossLimits: {
        upperLimit: 999999999.99,
        lowerLimit: 9999999.99
    },

    onPrepareData: function () {
        this.set('appLayout', LanguageDataStore.getLanguageObj());
        this.tradeService.accountDS.subscribeHoldingCalculation();
        this.checkContentUpdate();
        appEvents.subscribeLanguageChanged(this, this.get('wkey'));
        this.tradeService.subscribeTradeMetaReady(this, this.get('wkey'));
    }.on('init'),

    onTradeMetaReady: function () {
        this.tradeService.accountDS.subscribeHoldingCalculation();
    },

    checkContentUpdate: function () {
        Ember.run.once(this, this.updateContent);
    }.observes('tradeService.accountDS.totalGainLoss'),

    updateContent: function () {
        var gainLoss = this.tradeService.accountDS.getTotalGainLoss();
        this.checkHoldingsUpdate();

        if (gainLoss > this.gainLossLimits.upperLimit) { // Maximum number which app can support as per available space
            gainLoss = utils.formatters.divideNumber(gainLoss, 1);
        } else if (this.gainLossLimits.upperLimit >= gainLoss && gainLoss > this.gainLossLimits.lowerLimit) {
            var intValue = gainLoss.toString().split('.');
            gainLoss = utils.formatters.formatNumber(intValue[0], 0);
        } else {
            gainLoss = utils.formatters.formatNumber(gainLoss, 2);
        }

        this.set('gainLoss', gainLoss);
        this.set('gainLossPercentage', this.tradeService.accountDS.getTotalGainLossPer());
        this.set('gainLossCss', this.get('gainLossPercentage') >= 0 ? 'up-back-color' : 'down-back-color');
        this.set('gainLossLabel', this.get('gainLossPercentage') >= 0 ? this.get('appLayout').lang.labels.unGain : this.get('appLayout').lang.labels.unLoss);
    },

    checkHoldingsUpdate: function () {
        Ember.run.once(this, this.updateHoldings);
    }.observes('holdings.[]'),

    updateHoldings: function () {
        var that = this;
        var holdingsArray = this.get('tradeService').holdingDS.getHoldingCollection();
        var subscriptionSymNames = this.get('subscriptionSymNames');

        if (this.get('holdings').length === 0) {
            this.set('holdings', holdingsArray);
        }

        Ember.$.each(holdingsArray, function (key, holding) {
            var symbol = holding.symbol;
            var exchange = holding.exg;

            if (subscriptionSymNames.indexOf(symbol) < 0) {
                that.get('priceService').addSymbolRequest(exchange, symbol);
                subscriptionSymNames[subscriptionSymNames.length] = symbol;
            }
        });

        this.set('subscriptionSymNames', subscriptionSymNames);
    },

    languageChanged: function () {
        this.onPrepareData();
    },

    actions: {
        onGLClick: function () {
            var portfolioMenuId = 4;

            if (this.get('activeMainId') !== portfolioMenuId) {
                var portfolio = layoutConfig.layout.mainPanel.content[portfolioMenuId - 1];
                sharedService.getService('sharedUI').getService('mainPanel').onRenderMenuItems(portfolio);
            }
        }
    }
});

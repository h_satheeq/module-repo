import Ember from 'ember';
import OrderTicketPortrait from './order-ticket-portrait';
import ActionsComp from './components/order-action-container-mobile';
import sharedService from '../../../../models/shared/shared-service';
import tradeConstants from '../../../../models/trade/trade-constants';
import ControllerFactory from '../../../../controllers/controller-factory';
import appEvents from '../../../../app-events';
import utils from '../../../../utils/utils';

export default OrderTicketPortrait.extend({
    sliderEnabled: false,
    enableSearch: false,
    quantitySelectorEnabled: false,
    currentStatus: '',
    popupPanelCss: 'order-ticket-slider',
    isMarketDepthEnable: false,

    componentStatus: {
        sliderPlus: 1,
        sliderMinus: 2,
        quantitySelector: 3
    },

    onLoadWidget: function () {
        this._super();
        var priceUIService = sharedService.getService('priceUI');

        if (priceUIService) {
            priceUIService.subscribeBidOfferChanged(this, this.get('selectedLink'));
        }

        appEvents.subscribeSymbolChanged(this.get('wkey'), this, this.get('selectedLink'));
        this.checkScreenResolution();
    },

    onAfterRender: function () {
        this._super();
        var widgetId = '#' + 'orderTicket-' + this.get('wkey');
        this.initializeEventListner(widgetId, 'popupEnable');
        this.renderMarketDepth();
        this.initializeButtonAnimation();
    },

    renderMarketDepth: function () {
        var invoker = this.get('invoker');
        var route = this.container.lookup('route:application');
        var viewName = 'trade/widgets/mobile/order-ticket-market-depth';
        var controller = ControllerFactory.createController(this.container, 'controller:trade/widgets/mobile/order-ticket-market-depth');

        controller.set('sym', invoker.get('symbolInfo').get('sym'));
        controller.set('exg', invoker.get('symbolInfo').get('exg'));

        controller.initializeWidget({wn: 'order-ticket-market-depth'}, {widgetArgs: {isAllowPrepareData: false}});

        controller.set('symbol', invoker.symbolInfo);
        controller.set('invoker', invoker);
        controller.set('parentContainer', this);
        controller.set('selectedLink', '1');

        this.set('marketDepthController', controller);

        route.render(viewName, {
            into: 'trade/widgets/order-ticket/order-ticket-mobile',
            outlet: 'marketDepth',
            controller: controller
        });
    },

    prepareMarketDepth: function () {
        var invoker = this.get('invoker');
        var marketDepth = this.get('marketDepthController');

        if (invoker && invoker.get('symbolInfo').sym && marketDepth) {
            var sym = invoker.get('symbolInfo').sym;
            var exg = invoker.get('symbolInfo').exg;
            var inst = invoker.get('symbolInfo').inst;

            marketDepth.onWidgetKeysChange({sym, exg, inst});
            marketDepth.set('symbol', invoker.symbolInfo);
            marketDepth.set('invoker', invoker);
            marketDepth.set('parentContainer', this);
        }
    }.observes('invoker.symbolInfo'),

    setCompStatus: function (stat) {
        var componentStatus = this.get('componentStatus');

        switch (stat) {
            case componentStatus.sliderPlus:
            case componentStatus.sliderMinus:
                this.set('popupPanelCss', '');
                this.set('sliderEnabled', true);
                this.set('quantitySelectorEnabled', false);
                break;

            case componentStatus.quantitySelector:
                this.set('popupPanelCss', '');
                this.set('sliderEnabled', false);
                this.set('quantitySelectorEnabled', true);
                break;

            default:
        }

        this.set('currentStatus', stat);
    },

    focusField: function () {
        // Do Nothing
        // Overwriting this method to prevent focusssing any fields during order ticket loading
    },

    showMessagePopup: function () {
        var that = this;

        Ember.run.next(function () {
            var invoker = that.get('invoker');

            if (invoker && invoker.errorMessages && invoker.errorMessages.length > 0) {
                utils.messageService.showMessage(invoker.errorMessages[0],
                    utils.Constants.MessageTypes.Error,
                    false,
                    that.get('app').lang.labels[invoker.message]
                );
            }
        });
    },

    showOrderConfirmation: function () {
        var widgetController = ControllerFactory.createController(this.container, 'controller:trade/widgets/order-ticket/order-confirmation');
        var viewName = 'trade/widgets/order-ticket/order-confirmation';

        widgetController.set('invoker', this.get('invoker'));
        widgetController.initializeWidget({wn: 'order-confirmation'});

        sharedService.getService('priceUI').showChildView(viewName, widgetController, this.get('app.lang.labels.confirmOrder'), 'orderTicket-' + this.get('wkey'));
    },

    popupEnable: function (event) {
        var element = this.getParentElement(event, '#orderTicketSliderMobile');
        var marketDepth = this.getParentElement(event, '#orderTicketMarketDepth');
        var searchPanel = this.getParentElement(event, '#orderTicketSearch');

        if ((this.get('popupPanelCss') === '') && element && (element.length < 1)) {
            this.set('popupPanelCss', 'order-ticket-slider');
            this.set('currentStatus', '');
        }

        if ((this.get('isMarketDepthEnable')) && marketDepth && (marketDepth.length < 1)) {
            this.set('isMarketDepthEnable', false);
        }

        if ((this.get('enableSearch')) && searchPanel && (searchPanel.length < 1)) {
            this.set('enableSearch', false);
        }
    },

    checkScreenResolution: function () {
        var lowResolutionWidth = 340;

        if (window.screen.width <= lowResolutionWidth) {
            this.set('containerPadding', 'low-res-padding-right');
        }
    },

    resetOrderTicket: function () {
        var orderParams = this.get('invoker').get('orderParams');

        orderParams.set('action', tradeConstants.OrderAction.New);
        orderParams.set('ordSide', tradeConstants.OrderSide.Buy);
    },

    actions: {
        setStat: function (stat) {
            var status = stat;

            if (this.get('currentStatus') === status) {
                status = '';
            }

            this.setCompStatus(status);
        },

        showRecentOrders: function () {
            var widgetController = ControllerFactory.createController(this.container, 'controller:trade/widgets/mobile/order-list');
            var viewName = 'trade/widgets/mobile/order-list';

            widgetController.set('disableInnerWidgets', true);
            widgetController.set('isFilterStatus', true);
            widgetController.set('isChildView', true);
            widgetController.set('wkey', 'recentOrderPopup');

            widgetController.initializeWidget({wn: 'order-list'});
            widgetController.filterOrders();

            sharedService.getService('priceUI').showChildView(viewName, widgetController, this.get('app.lang.labels.recentOrders'), 'orderTicket-' + this.get('wkey'));
        }
    }
});

Ember.Handlebars.helper('order-action-container-mobile', ActionsComp);

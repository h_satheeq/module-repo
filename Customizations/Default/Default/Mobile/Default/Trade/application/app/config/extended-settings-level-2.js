export default {
    appConfig: {
        customisation: {
            authenticationMode: 4,
            isTradingEnabled: true,
            hashType: 'MD5',

            loginViewSettings: {
                isSignUpEnabled: false,
                isForgotPasswordEnabled: true,
                isPoweredByEnabled: true,
                isRegisterEnabled: false
            },

            supportedLanguages: [
                {code: 'EN', desc: 'English'}
            ]
        },

        widgetId: {
            quoteMenuId: 'fullQuote',
            watchListMenuId: 'heatMap',
            marketMenuId: 'market',
            indicesTabId: 'companyProf',
            topStocksTabId: 'topStocks',
            portfolioMenuId: 'standard',
            orderTicketMenuId: 'classic'
        }
    },

    priceSettings: {
        // Colombo Central Price
        // connectionParameters: {
        //    primary: {
        //        ip: '123.231.71.182',
        //        port: '9066',
        //        secure: false
        //    },
        //    secondary: {
        //        ip: '123.231.71.182',
        //        port: '9066',
        //        secure: false
        //    }
        // },

        connectionParameters: {
            primary: {
                ip: 'lkcentralprice.directfn.com/price',
                port: '',
                secure: true
            },
            secondary: {
                ip: 'lkcentralprice.directfn.com/price',
                port: '',
                secure: true
            }
        },

        configs: {
            priceTickerConfigs: {
                tickerSymDisplayField: 'sym'
            }
        }
    },

    tradeSettings: {
	channelId: 22,

        // Colombo TRS - Local
        connectionParameters: {
            primary: {
                ip: '123.231.48.7',
                port: '8800',
                secure: false
            },

            secondary: {
                ip: '123.231.48.7',
                port: '8800',
                secure: false
            }
        }
        // connectionParameters: {
        //    primary: {
        //        ip: 'csegame.directfn.com/trade',
        //        port: '',
        //        secure: true
        //    },
        //
        //    secondary: {
        //        ip: 'csegame.directfn.com/trade',
        //        port: '',
        //        secure: true
        //    }
        // }
    },

    userSettings: {
        customisation: {
            defaultExchange: 'LKCSE',
            defaultIndex: 'ASI',
            defaultCurrency: 'LKR'
        }
    }
};

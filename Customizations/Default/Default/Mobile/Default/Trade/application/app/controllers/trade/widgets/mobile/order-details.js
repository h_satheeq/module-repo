import Ember from 'ember';
import BaseController from '../../../base-controller';
import tradeConstants from '../../../../models/trade/trade-constants';
import utils from '../../../../utils/utils';
import responsiveHandler from '../../../../helpers/responsive-handler';
import sharedService from '../../../../models/shared/shared-service';
import userSettings from '../../../../config/user-settings';
import languageDataStore from '../../../../models/shared/language/language-data-store';

export default BaseController.extend({
    title: 'Order Details',
    filledColor: undefined,
    progressPercentage: '100%',
    isOrdRejection: false,
    tradeService: sharedService.getService('trade'),

    isVATEnabled: function () {
        return this.tradeService.userDS.isVatApplicble === tradeConstants.VATStatus.Enabled;
    }.property('tradeService.userDS.isVatApplicble'),

    onLoadWidget: function () {
        this.setOrderStsCss();
    },

    initializeResponsive: function () {
        var lowResolutionWidth = 340;

        this.set('responsive', responsiveHandler.create({controller: this, widgetId: 'hnav-panel-mobile', callback: this.onResponsive}));
        this.responsive.addList('hnav-panel-mobile', [{id: 'hnav-panel-mobile', width: lowResolutionWidth}]);
        this.responsive.initialize();
    },

    ordSideCss: function () {
        return this.get('rowData').ordSide === '1' ? 'up-fore-color' : 'down-fore-color';
    }.property('rowData.ordSide'),

    calculateFilledPercentage: function () {
        var pendingQty = this.get('rowData') ? this.get('rowData.pendQty') : undefined;
        var ordQty = this.get('rowData') ? this.get('rowData.ordQty') : undefined;

        if (pendingQty && ordQty) {
            var filledQty = ordQty - pendingQty;
            var percentage = (filledQty * 100) / (ordQty);
            var formattedPer = utils.formatters.formatNumberPercentage(percentage, 0);

            this.set('rowData.fillQty', filledQty);
            this.set('progressPercentage', formattedPer);
        }
    }.observes('rowData.pendQty'),

    priceValue: function () {
        var rowData = this.get('rowData');
        var priceValueCss = '';
        var formattedPriceValue = this.get('app').lang.labels.mktPrice;

        if (rowData.ordTyp !== '1') {
            formattedPriceValue = this.get('utils').formatters.formatNumber(rowData.price, rowData.symbolInfo.deci);
            priceValueCss = 'bold';
        }

        this.set('priceValueCss', priceValueCss);

        return formattedPriceValue;
    }.property('rowData.ordTyp'),

    errorMessageText: function () {
        var message = this.get('rowData.txt');

        if (message && message.includes('|')) {
            var langDesMap = languageDataStore.generateLangMessage(message);

            return langDesMap[sharedService.userSettings.currentLanguage];
        } else {
            return message;
        }
    }.property('rowData.txt'),

    setOrderStsCss: function () {
        var currentValue = this.get('rowData') ? this.get('rowData').ordSts : '';
        var fillCss = 'up-back-color';
        var orderStatus = tradeConstants.OrderStatus;

        this.set('progressPercentage', '100%');

        if (currentValue === orderStatus.PartiallyFilled) {
            this.set('progressPercentage', '0%');
            this.set('isEnablePer', true);
            this.calculateFilledPercentage();
        } else if (currentValue === orderStatus.Rejected) {
            fillCss = 'down-back-color ellipsis';
            this.set('isOrdRejection', true);
        } else if (currentValue === orderStatus.SendToBrokerNew) {
            fillCss = 'pending-back-color ellipsis';
        } else if (currentValue === orderStatus.New) {
            fillCss = 'pending-back-color ellipsis';
        } else if (currentValue === orderStatus.Filled) {
            this.set('rowData.fillQty', this.get('rowData.ordQty'));
        } else {
            fillCss = 'ellipsis';
        }

        this.set('filledColor', fillCss);
    }.observes('rowData.ordSts'),

    showOrderTicket: function (isCancel) {
        var rowData = this.get('rowData');

        if (rowData.isAmendEnabled || rowData.isCancelEnabled) {
            sharedService.getService('tradeUI').loadAmendCancel(this.container, isCancel, rowData);
            sharedService.getService('priceUI').closeChildView('trade/widgets/mobile/order-details');
        }
    },

    navigateOrders: function (isNextOrder) {
        var currentOrder = this.get('rowData');
        var orderArray = this.get('orderArray');
        var currentOrderId = currentOrder.clOrdId;
        var currentOrderIndex = 0;

        Ember.$.each(orderArray, function (key, order) {
            if (order.clOrdId === currentOrderId) {
                currentOrderIndex = key;

                return false;
            }
        });

        var newOrderIndex = isNextOrder ? currentOrderIndex + 1 : currentOrderIndex - 1;

        if (newOrderIndex < orderArray.length && newOrderIndex > -1) {
            var newOrder = orderArray[newOrderIndex];

            if (newOrder.mubOrdNum) {
                this.set('rowData', sharedService.getService('trade').orderDS.getOrder(newOrder.mubOrdNum));
            }
        }
    },

    orderId: function () {
        var orderId = this.get('rowData.clOrdId');
        return orderId === -1 ? userSettings.displayFormat.noValue : orderId;
    }.property('rowData.clOrdId'),

    orderValue: function () {
        return this._getFormattedValue(this.get('rowData.ordVal'));
    }.property('rowData.ordVal'),

    commission: function () {
        return this._getFormattedValue(this.get('rowData.comsn'));
    }.property('rowData.comsn'),

    vat: function () {
        return this._getFormattedValue(this.get('rowData.vatAmount'));
    }.property('rowData.vatAmount'),

    netValue: function () {
        return this._getFormattedValue(this.get('rowData.netOrdVal'));
    }.property('rowData.netOrdVal'),

    averagePrice: function () {
        return this._getFormattedValue(this.get('rowData.avgPrice'));
    }.property('rowData.avgPrice'),

    _getFormattedValue: function (rowValue) {
        var decimalPlaces = this.get('rowData.symbolInfo.deci');
        return rowValue === -1 ? userSettings.displayFormat.noValue : this.get('utils').formatters.formatNumber(rowValue, decimalPlaces);
    },

    actions: {
        amendOrder: function () {
            this.showOrderTicket(false);
        },

        navigateOrders: function (isNextOrder) {
            this.navigateOrders(isNextOrder);
        },

        cancelOrder: function () {
            this.showOrderTicket(true);
        }
    }
});
import Ember from 'ember';
import BaseController from '../../../base-controller';
import sharedService from '../../../../models/shared/shared-service';
import ControllerFactory from '../../../../controllers/controller-factory';
import tradeConstants from '../../../../models/trade/trade-constants';
import fieldMetaConfig from '../../../../config/field-meta-config';

export default BaseController.extend({
    invoker: {},
    exchange: {},
    title: 'Order Confirmation',
    isOrderConfirmationDisabled: false,
    valueDecimals: undefined,
    tradeService: sharedService.getService('trade'),

    isVATEnabled: function () {
        return this.tradeService.userDS.isVatApplicble === tradeConstants.VATStatus.Enabled;
    }.property('tradeService.userDS.isVatApplicble'),

    initialize: function () {
        this.setValueDecimals();
    }.on('init'),

    expose: function () {
        this._super();
        this.onExchangeChange();
    }.on('didInsertElement'),

    ordSideCss: function () {
        return this.get('invoker').get('orderParams').ordSide === '1' ? 'up-fore-color' : 'down-fore-color';
    }.property('invoker'),

    isCancelOrder: function () {
        return this.get('invoker').get('orderParams').action === tradeConstants.OrderAction.Cancel;
    }.property('invoker'),

    priceValue: function () {
        var invoker = this.get('invoker');
        var priceValueCss = '';
        var formattedPriceValue = this.get('app').lang.labels.mktPrice;

        if (this.get('invoker').get('orderParams').ordTyp !== '1') {
            formattedPriceValue = this.get('utils').formatters.formatNumber(invoker.orderParams.price, invoker.symbolInfo.deci);
            priceValueCss = 'bold';
        }

        this.set('priceValueCss', priceValueCss);

        return formattedPriceValue;
    }.property('invoker.orderParams.ordTyp'),

    onExchangeChange: function () {
        var invoker = this.get('invoker');
        var orderParams = invoker && invoker.orderParams ? invoker.get('orderParams') : undefined;

        if (orderParams && orderParams.exg) {
            var exg = orderParams.get('exg');
            this.set('exchange', sharedService.getService('price').exchangeDS.getExchange(exg));
        }
    }.observes('invoker.orderParams.exg'),

    isOfflineOrder: function () {
        var exchange = this.get('exchange');
        var invoker = this.get('invoker');

        if (exchange && exchange.stat) {
            var stat = exchange.get('stat');

            return sharedService.getService('price').MarketStatus.isOfflineStatus(stat) && invoker &&
                invoker.orderStrategy && !invoker.orderStrategy.isOfflineOrdWarningDisabled;
        }

        return false;
    }.property('exchange.stat'),

    setValueDecimals: function () {
        var orderTicketMeta = fieldMetaConfig.orderTicket;

        if (orderTicketMeta && orderTicketMeta.orderValue && orderTicketMeta.orderValue.decimalPlaces) {
            this.set('valueDecimals', orderTicketMeta.orderValue.decimalPlaces);
        }
    },

    resetOrderTicket: function () {
        var invoker = this.get('invoker');

        if (invoker && invoker.orderStrategy) {
            invoker.get('orderStrategy').resetOrder();
        }

        // Notify all components to reset
        this.notifyComponents('onReset');
    },

    notifyComponents: function (method) {
        var componentList = this.get('componentList');

        if (componentList) {
            Ember.$.each(componentList, function (key, componentId) {
                var component = Ember.View.views[componentId];

                if (component) {
                    component.send(method);
                }
            });
        }
    },

    actions: {
        confirmOrder: function () {
            var that = this;

            if (!this.get('isOrderConfirmationDisabled')) {
                // Avoid Confirm button being clicked more than once per order
                that.set('isOrderConfirmationDisabled', true);

                var success = this.get('invoker').confirmOrder();

                if (success) {
                    this.get('invoker').get('orderParams').set('action', tradeConstants.OrderAction.New);

                    Ember.run.later(function () {
                        that.resetOrderTicket();
                    }, 300);
                }

                var widgetController = ControllerFactory.createController(this.container, 'controller:trade/widgets/mobile/order-list');
                var viewName = 'trade/widgets/mobile/order-list';

                widgetController.set('disableInnerWidgets', true);
                widgetController.set('isFilterStatus', true);
                widgetController.set('isChildView', true);
                widgetController.set('wkey', 'recentOrderPopup');

                widgetController.initializeWidget({wn: 'order-list'});
                widgetController.filterOrders();

                sharedService.getService('priceUI').showChildView(viewName, widgetController, this.get('app.lang.labels.recentOrders'), 'orderTicket-' + this.get('wkey'));
            }
        },

        closePopup: function () {
            sharedService.getService('priceUI').closeChildView('trade/widgets/order-ticket/order-confirmation');
        }
    }
});
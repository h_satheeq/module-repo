/* jshint node: true */
var EnvironmentExtended = require('./environment-extended.js');

module.exports = function (environment) {
    var ENV = new EnvironmentExtended(environment);

    ENV.baseURL = '/';
    ENV.contentSecurityPolicy['connect-src'] = "'self' ws://123.231.71.182:9066 wss://data-sa9.mubasher.net/html5ws www.google-analytics.com ws://123.231.48.7:8800 ws://31.222.133.25:9018 wss://csegame.directfn.com/ws wss://csegame.directfn.com/trade wss://lkcentralprice.directfn.com/price ws://lkcentralprice.directfn.com/price";
    ENV.locationType = 'none';

    if (ENV.APP.isTestMode) {
        ENV.APP.tradeConnectionParameters = {
            primary: {
                ip: '123.231.48.7',
                port: '8800',
                secure: false // Is connection secure - true or false     
            }
        };

        ENV.APP.priceConnectionParameters = {
            primary: {
                ip: 'lkcentralprice.directfn.com/price',
                port: '',
                secure: true // Is connection secure - true or false
            }
        };

        ENV.baseURL = '/';
        ENV.contentSecurityPolicy['connect-src'] = "'self' ws://123.231.71.182:9066 wss://data-sa9.mubasher.net/html5ws www.google-analytics.com ws://123.231.48.7:8800 ws://31.222.133.25:9018 wss://csegame.directfn.com/ws wss://csegame.directfn.com/trade wss://lkcentralprice.directfn.com/price ws://lkcentralprice.directfn.com/price";
    }

    return ENV;
};

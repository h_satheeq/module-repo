import Ember from 'ember';
import SharedUIModuleInitializer from '../../../../controllers/shared/initializers/shared-ui-module-initializer';
import SharedUIServiceMobile from '../../shared-ui-service-mobile';
import themeInitializer from '../../../../controllers/shared/initializers/theme-initializer';
import sharedDataModuleInitializer from '../../../../models/shared/initializers/shared-data-module-initializer';
import languageInitializer from '../../../../controllers/shared/initializers/language-initializer';

export default SharedUIModuleInitializer.extend({
    preInitialize: function () {
        var that = this;

        // This is added to show login view in preInitialize in mobile
        Ember.$('div.login.pos-abs').removeClass('display-none');

        themeInitializer.prepareThemeView(function () {
            that._showLoginView();
        });

        this._super();
    },

    // Below Function overridden only because to avoid calling 'themeInitializer.prepareThemeView' twice
    postInitialize: function () {
        this._super();
        this.loginViewController.initialize(sharedDataModuleInitializer.authType, sharedDataModuleInitializer.authController);
        languageInitializer.prepareLanguageView();
        this._showLandingPage();
    },

    createService: function () {
        return SharedUIServiceMobile.create();
    }
});
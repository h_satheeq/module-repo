import sharedService from '../../models/shared/shared-service';
import ControllerFactory from '../../controllers/controller-factory';
import languageDataStore from '../../models/shared/language/language-data-store';
import SharedUIService from './shared-ui-service';

export default SharedUIService.extend({
    isChildViewEnabled: false,

    getTitleBar: function () {
        return this.getService('tickerPanel');
    },

    setChildViewEnable: function (isEnabled) {
        this.set('isChildViewEnabled', isEnabled);
    },

    invokeChangePassword: function (uiContainer) {
        if (uiContainer) {
            var widgetController = ControllerFactory.createController(this.container, 'component:password-change');
            var viewName = 'components/password-change';
            var app = languageDataStore.getLanguageObj();

            widgetController.set('isBackDisabled', true);
            widgetController.send('showModalPopup', false);

            sharedService.getService('priceUI').showChildView(viewName, widgetController, app.lang.labels.changePassword, 'change-password-mobile');
        }
    }
});
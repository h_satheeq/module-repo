import Ember from 'ember';
import ExpandedCell from '../../dual-cells/expanded-cell';
import userSettings from '../../../../config/user-settings';
import utils from '../../../../utils/utils';
import symbolIndicatorHelper from '../../symbol-indicator-helper';
import languageDataStore from '../../../../models/shared/language/language-data-store';
import priceWidgetConfig from '../../../../config/price-widget-config';

export default ExpandedCell.extend({
    templateName: 'table/views/mobile/watchlist/expanded-symbol-cell',
    app: languageDataStore.getLanguageObj(),

    formattedThirdValue: Ember.computed(function () {
        return utils.formatters.formatNumber(this.get('cellContent') ? this.get('cellContent').thirdValue : undefined, this.get('row') && !isNaN(this.get('row.deci')) ? this.get('row.deci') : userSettings.displayFormat.decimalPlaces);
    }).property('cellContent'),

    formattedFourthValue: Ember.computed(function () {
        return utils.formatters.formatNumber(this.get('row') ? this.get('row.high') : undefined, this.get('row') && !isNaN(this.get('row.deci')) ? this.get('row.deci') : userSettings.displayFormat.decimalPlaces);
    }).property('row.high'),

    formattedFifthValue: Ember.computed(function () {
        var displayValue = this.get('formattedFirstValue');
        var shortDescription = this.get('row.sDes');
        var companyCode = this.get('row.cid') ? this.get('row.cid') + ' - ' : '';

        if (companyCode && shortDescription) {
            displayValue = companyCode + shortDescription;
        }

        return displayValue;
    }).property('row.sDes'),

    styleFourthValue: Ember.computed(function () {
        return this.get('column.fourthValueStyle') ? this.get('column.fourthValueStyle') : '';
    }).property('formattedFourthValue'),

    // indicator properties
    isndicatorAvailable: function () {
        return this.get('column.isndicatorAvailable');
    }.property('cellContent'),

    dcfsValue: function () {
        return this.get('row') ? this.get('row.dcfs') : undefined;
    }.property('row.dcfs'),

    is52WeekHigh: function () {
        return this.get('row') ? this.get('row.is52WeekHigh') : false;
    }.property('row.is52WeekHigh'),

    is52WeekLow: function () {
        return this.get('row') ? this.get('row.is52WeekLow') : false;
    }.property('row.is52WeekLow'),

    isTodayHigh: function () {
        return this.get('row') ? this.get('row.isTodayHigh') : false;
    }.property('row.isTodayHigh'),

    isTodayLow: function () {
        return this.get('row') ? this.get('row.isTodayLow') : false;
    }.property('row.isTodayLow'),

    dcfsStyle: (function () {
        var dcfsObj = symbolIndicatorHelper.formatDcfsValueStyle(this.get('dcfsValue'));
        this.set('dcfsToolTip', this.get('app').lang.labels[dcfsObj.dcfsToolTip]);

        return dcfsObj.dcfsClass;
    }).property('dcfsValue'),

    isAnnAvailable: function () {
        var rowProperties = this.getProperties('row');
        var row = rowProperties ? rowProperties.row : '';
        var isAnnAvailable = false;

        if (row) {
            isAnnAvailable = row.get('ann');
            this.set('annToolTip', this.get('app').lang.labels.annAvailable);
        }

        return isAnnAvailable;
    }.property('cellContent'),

    isSymSuspended: function () {
        return this.get('row.isSymSuspended');
    }.property('row.isSymSuspended'),

    setDisplaySymbol: function () {
        this.set('isDisplayConfigTwo', priceWidgetConfig.watchList.symbolCellLayout === 2);
    }.on('init')
});
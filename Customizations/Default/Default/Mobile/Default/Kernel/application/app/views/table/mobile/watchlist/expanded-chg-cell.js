import Ember from 'ember';
import DualChangeCell from '../../dual-cells/dual-change-cell';
import ExpandedColumnMixin from '../../table-mixins/expanded-column-mixin';
import userSettings from '../../../../config/user-settings';

export default DualChangeCell.extend(ExpandedColumnMixin, {
    templateName: 'table/views/mobile/watchlist/expanded-chg-cell',
    isColumnResizeEnabled: false,

    isIndexView: function () {
        return this.get('controller').isIndexView;
    }.property('controller.isIndexView'),

    styleFirstValue: Ember.computed(function () {
        var upStyle = 'up-back-color watch-list-border-radius watch-list-value-width btn-txt-color';
        var downStyle = 'down-back-color watch-list-border-radius watch-list-value-width btn-txt-color';
        var zeroStyle = 'no-changed-back-color watch-list-border-radius watch-list-value-width fore-color';

        if (this.get('isIndexView')) {
            upStyle = 'up-fore-color bold';
            downStyle = 'down-fore-color bold';
            zeroStyle = 'fade-fore-color bold';
        }

        return this.getPositiveNegativeStyle(this.get('firstValue'), upStyle, downStyle, zeroStyle);
    }).property('firstValue'),

    formattedThirdValue: Ember.computed(function () {
        return this.addFormat(this.get('cellContent') ? this.get('cellContent').thirdValue : undefined, false, this.get('row') && !isNaN(this.get('row.deci')) ? this.get('row.deci') : userSettings.displayFormat.decimalPlaces);
    }).property('cellContent')
});
import Ember from 'ember';
import ExpandedCell from '../../dual-cells/expanded-cell';
import userSettings from '../../../../config/user-settings';
import utils from '../../../../utils/utils';

export default ExpandedCell.extend({
    templateName: 'table/views/mobile/watchlist/expanded-ltp-cell',

    formattedThirdValue: Ember.computed(function () {
        return utils.formatters.formatNumber(this.get('row') ? this.get('row.low') : undefined, this.get('row') && !isNaN(this.get('row.deci')) ? this.get('row.deci') : userSettings.displayFormat.decimalPlaces);
    }).property('cellContent')
});
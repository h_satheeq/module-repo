import Ember from 'ember';
import layoutConfig from '../config/layout-config';
import appEvents from '../app-events';
import utils from '../utils/utils';
import sharedService from '../models/shared/shared-service';

export default Ember.Route.extend({
    appLayout: {},
    clickEventHandler: null,
    disableTopPanel: false,
    mainPanelHeight: 'widget-container-height',
    layoutMap: {},

    renderTemplate: function (controller, model) {
        this._super(controller, model); // Render the base template
        this.set('appLayout', layoutConfig);
        sharedService.getService('sharedUI').registerService('appLayoutConfig', layoutConfig);

        this.get('controller').mainOutletStyle = 'col-xs-10 full-height';
        this.get('controller').rightPanelStyle = '';
        Ember.addObserver(sharedService.getService('sharedUI'), 'isChildViewEnabled', this.get('controller'), this.setChildViewEnabled);

        this.addDomReadyListener();
        this.addVisibilityChangeListener();
        this.addAppCloseListener();
        this.addDocumentClickListener();
        this.addOrientationChangeListener();

        var that = this;
        var layoutContent = this.get('appLayout').layout;

        Ember.$.each(layoutContent, function (prop, content) {
            that.render(content.template, {
                into: 'application',
                outlet: prop + 'Outlet'
            });

            var containerController = that.controllerFor(content.template);
            containerController.initializeContainer(that, that.get('appLayout'));

            sharedService.getService('sharedUI').registerService(prop, containerController);
        });

        Ember.$.each(layoutContent, function (prop) {
            sharedService.getService('sharedUI').getService(prop).initializeUI();
        });

        this._initializeSharedComponents();
        this._initializeLayoutConfigMap();
        appEvents.onLayoutReady(this);
    },

    setChildViewEnabled: function () {
        this.set('isChildViewEnabled', sharedService.getService('sharedUI').get('isChildViewEnabled'));

        Ember.run.next(this, function () {
            if (this.mainPanelHeight === 'widget-container-height-order-ticket' && !this.get('isChildViewEnabled')) {
                Ember.$('#notificationPanel').addClass('display-none');
            }
        });
    },

    addVisibilityChangeListener: function () {
        if (document.addEventListener) {
            document.addEventListener('visibilitychange', function () {
                appEvents.onVisibilityChanged(document.hidden);
            });
        }
    },

    addAppCloseListener: function () {
        window.onbeforeunload = function () {
            utils.webStorage.addString(utils.webStorage.getKey(utils.Constants.CacheKeys.LoggedIn), utils.Constants.No, utils.Constants.StorageType.Session);
            appEvents.onAppClose();
        };
    },

    addOrientationChangeListener: function () {
        if (window.addEventListener && window.DeviceOrientationEvent) {
            window.addEventListener('orientationchange', function () {
                appEvents.onOrientationChanged();
            });

            appEvents.onOrientationChanged(); // To set current orientation on first time load
        }
    },

    _initializeSharedComponents: function () {
        var messageViewer = this.container.lookupFactory('component:single-message-viewer').create();
        sharedService.getService('sharedUI').registerService('single-message-viewer', messageViewer);
    },

    addDocumentClickListener: function () {
        this.clickEventHandler = this.onDocumentClick.bind(this);
        document.addEventListener('mousedown', this.clickEventHandler, true);
    },

    setMainPanelHeight: function () {
        var disableTopPanel = this.get('disableTopPanel');
        var notificationPanel = Ember.$('#notificationPanel');

        if (disableTopPanel) {
            Ember.set(this.get('controller'), 'mainPanelHeight', 'widget-container-height-order-ticket');
            notificationPanel.addClass('display-none');
        } else {
            // This is to remove animation of already shown notifications
            var notificationContainers = notificationPanel.find('.div-up');
            notificationContainers.addClass('div-up-top');
            notificationContainers.removeClass('div-up');

            Ember.set(this.get('controller'), 'mainPanelHeight', 'widget-container-height');
            notificationPanel.removeClass('display-none');
        }
    }.observes('disableTopPanel'),

    onDocumentClick: function (e) {
        Ember.appGlobal.events.mousedown = e;
    },

    _initializeLayoutConfigMap: function () {
        var layoutMap = {};
        var menuArray = layoutConfig.layout.mainPanel.content;

        // Set Menu Content array to map
        Ember.$.each(menuArray, function (key, menu) {
            layoutMap[menu.title] = menu;

            // Set Tab Content array to map
            Ember.$.each(menu.tab, function (tabKey, tabItem) {
                var tabItemMap = layoutMap[menu.title].tabMap;

                if (tabItemMap) {
                    tabItemMap[tabItem.title] = tabItem;
                } else {
                    layoutMap[menu.title].tabMap = {};
                }
            });
        });

        this.set('layoutMap', layoutMap);
        sharedService.getService('sharedUI').setLayoutMap(layoutMap);
    },

    addDomReadyListener: function () {
        window.addEventListener('load', function () {
            appEvents.onDomReady();

            window.removeEventListener('load', function () {
            }); // Remove listener, no longer needed
        });
    },

    actions: {
        renderMenuItems: function (menuContent) {
            sharedService.getService('sharedUI').getService('mainPanel').onRenderMenuItems(menuContent);
        },

        renderDirectTabItems: function (menuContent, tabContent) {
            sharedService.getService('sharedUI').getService('mainPanel').onRenderMenuItems(menuContent, undefined, tabContent.id);
        },

        renderTabItems: function (tabContent) {
            sharedService.getService('sharedUI').getService('mainPanel').onRenderTabItems(tabContent);
        },

        renderRightPanelItems: function (rightPanelContent) {
            sharedService.getService('sharedUI').getService('rightPanel').renderRightPanelView(rightPanelContent, {isUserClick: true});
            utils.analyticsService.trackEvent('right-panel', utils.Constants.GAActions.viewChanged, ['tab:', rightPanelContent.layoutTemplate].join(''));
        },

        escapeMainNavigation: function () {
            sharedService.getService('sharedUI').getService('titleBar').hideMainNavigation();
        }
    }
});

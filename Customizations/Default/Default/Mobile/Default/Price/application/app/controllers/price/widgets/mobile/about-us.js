import Ember from 'ember';
import BaseController from '../../../base-controller';
import appConfig from '../../../../config/app-config';
import sharedService from '../../../../models/shared/shared-service';

export default BaseController.extend({
    title: '',
    address: '',
    longVersion: appConfig.longVersion,

    onLoadWidget: function () {
        this.set('title', this.get('app').lang.labels.aboutUs);
    },

    onPrepareData: function () {
        var that = this;
        var itemArray = [];
        var configArray = appConfig.customisation.supportedContacts;

        if (configArray && configArray.length > 0) {
            Ember.$.each(configArray, function (key, item) {
                var contact = {};
                var lanKey = item.key;

                if (lanKey) {
                    contact.value = item.value;
                    contact.lanKey = that.get('app').lang.labels[lanKey] + ' : ';

                    if (item.type === 'T') {
                        contact.isTel = true;
                    } else if (item.type === 'E') {
                        contact.isEmail = true;
                    }else if (item.type === 'U') {
                        contact.isUrl = true;
                    }

                    itemArray[itemArray.length] = contact;
                }
            });
        }

        var addressKey = appConfig.customisation.countryAddress;
        addressKey = addressKey ? addressKey : 'saudiArabiaAddress';

        this.set('address', this.get('app').lang.labels[addressKey]);
        this.set('supportedContacts', itemArray);
        this.set('imgSrc', appConfig.customisation.imgSrc);
    },

    isArabic: function () {
        return sharedService.userSettings.currentLanguage === 'AR';
    }.property('sharedService.userSettings.currentLanguage')
});
import BaseModuleInitializer from '../../../models/shared/initializers/base-module-initializer';
import PriceUIService from '../../../controllers/price/price-ui-service';
import appEvents from '../../../app-events';
import languageDataStore from '../../../models/shared/language/language-data-store';
import sharedService from '../../../models/shared/shared-service';

export default BaseModuleInitializer.extend({
    preInitialize: function () {
        var service = this.createService();

        sharedService.registerService(service.subscriptionKey, service);

        service.initialize(languageDataStore.getLanguageObj());
        appEvents.subscribeLayoutReady(service.subscriptionKey, service);
    },

    createService: function () {
         return PriceUIService.create();
    }
});

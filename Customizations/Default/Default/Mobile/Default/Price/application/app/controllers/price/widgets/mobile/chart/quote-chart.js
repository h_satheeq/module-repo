import Ember from 'ember';
import RegularChart from '../../../../chart/regular-chart';
import ChartConstants from '../../../../../models/chart/chart-constants';
import ChartStudies from '../../../../../models/chart/chart-studies';
import appEvents from '../../../../../app-events';
import ChartStatusPanel from '../quote-summary/components/chart-status-panel';
import appConfig from '../../../../../config/app-config';

/* global STX */
/* global $$$ */

export default RegularChart.extend({
    DefaultChartStyle: ChartConstants.ChartStyle.Area,
    DefaultChartPeriod: ChartConstants.ChartViewPeriod.OneDay,
    VChartType: 'vchart',

    chartStudies: Ember.A(),
    chartContainer: '',
    pageContainer: '',
    chartHeightStyle: 'mobile-portrait-chart-height',

    isLandscapeMode: false,

    arrDisplayPeriods: function () {
        var arrTabs = Ember.A();

        Ember.$.each(ChartConstants.MobileQuoteChartPeriodTab, function (key, value) {
            if (value) {
                arrTabs.pushObject(ChartConstants.ChartViewPeriod[key]);
            }
        });

        Ember.set(arrTabs[0], 'css', 'active');

        return arrTabs;
    }.property(),

    chartStyles: function () {
        var arrStyles = Ember.A();
        var that = this;
        var chartStyle = null;
        // Display name is changed when changing a language
        var selectedStyle = that.get('chartStyle');

        Ember.set(selectedStyle, 'DisplayName', that.get('app').lang.labels[selectedStyle.LanguageTag]);

        Ember.$.each(ChartConstants.ChartStyle, function (key) {
            chartStyle = ChartConstants.ChartStyle[key];
            Ember.set(chartStyle, 'DisplayName', that.get('app').lang.labels[chartStyle.LanguageTag]);
            arrStyles.pushObject(chartStyle);
        });

        return arrStyles;
    }.property('app.lang'),

    indicators: function () {
        var that = this;
        var indicators = Ember.A();
        var indicatorObj;
        var configIndicators = appConfig.chartConfig.chartIndicators;

        if (configIndicators && configIndicators.length > 0) {
            Ember.$.each(configIndicators, function (key, value) {
                indicatorObj = ChartStudies.Indicators[value];

                if (indicatorObj) {
                    Ember.set(indicatorObj, 'DisplayName', that.get('app').lang.labels[indicatorObj.LanguageTag]);
                    indicators.pushObject(indicatorObj);
                }
            });
        }

        return indicators;
    }.property('app.lang'),

    onLoadWidget: function () {
        var wkey = this.get('wkey');
        this.set('chartContainer', ['chart-container', wkey].join('-'));
        this.set('pageContainer', ['mobileChartContainer', wkey].join('-'));

        this._super();
        appEvents.subscribeOrientationChanged(wkey, this);
    },

    onAfterRender: function () {
        var that = this;

        Ember.run.next(this, function () {
            that.initChart($$$(['.', that.get('chartContainer')].join('')));

            var baseChart = that.get('baseChart');

            if (baseChart && baseChart.controls) {
                baseChart.controls.chartControls = null;
                baseChart.allowZoom = false;
                baseChart.allowScroll = false;
            }

            that.onOrientationChanged(Ember.appGlobal.orientation.isLandscape);
        });
    },

    // Chart Base overrides
    onFinishedLoadingNewChart: function () {
        var that = this;

        return function (error) {
            if (!error) {
                var baseChart = that.get('baseChart');

                if (baseChart.masterData && baseChart.masterData.length > 0) {
                    baseChart.setRange({
                        dtLeft: baseChart.masterData[0].DT, // Set this to the date to appear on the left edge of chart
                        dtRight: baseChart.masterData[baseChart.masterData.length - 1].DT,  // Set this to the date to appear on the right edge of chart
                        padding: 30 // Set this to the number of pixels of padding to leave between the chart and y-axis
                    });
                }

                baseChart.draw();
            }
        };
    },

    onOrientationChanged: function (isLandscape) {
        if (isLandscape === this.get('isLandscapeMode')) {
            this._updateChartConfigs(isLandscape);

            return;
        }

        this.set('isLandscapeMode', isLandscape);
        this.set('chartHeightStyle', isLandscape ? 'mobile-landscape-chart-height' : 'mobile-portrait-chart-height');
        this.toggleFullScreen(isLandscape, this.get('pageContainer'), this.get('wkey'));

        if (!isLandscape) {
            this._setChartStyle(this.DefaultChartStyle);
            this._clearChartStudies();
            this._chartTypeSelected(this.DefaultChartPeriod);
        }

        this._updateChartConfigs(isLandscape);
    },

    onUnloadWidget: function () {
        this._super();
        appEvents.unSubscribeOrientationChanged(this.get('wkey'));
    },

    onCheckVolumeChartEnabler: function () {
        return false;
    },

    toggleFullScreen: function (isLandscapeMode, widgetContainerId, widgetId, fullViewHeight, regularViewHeight) {
        var fullScreenContainer = document.getElementById('fullScreenContainer');
        var widgetContainer = document.getElementById(widgetContainerId);
        var widget = document.getElementById(widgetId);
        var body = document.body;

        if (widgetContainer) {
            if (isLandscapeMode) {
                this.set('previousParent', widgetContainer.parentElement);
                this.set('previousWatchListStyleAttribute', widgetContainer.getAttribute('style'));
                this.set('previousFullScreenContainerStyleAttribute', fullScreenContainer.getAttribute('style'));
                fullScreenContainer.appendChild(widgetContainer);
                widgetContainer.setAttribute('style', 'position: absolute; left: 0; top: 0; bottom: 0; right: 0;');
                fullScreenContainer.setAttribute('style', 'z-index:300; position: absolute; left: 0; top: 0; bottom: 0; right: 0;');

                var html = document.documentElement;
                var height = Math.max(body.scrollHeight, body.offsetHeight, html.clientHeight, html.scrollHeight, html.offsetHeight);

                if (widget) {
                    widget.setAttribute('style', 'height:' + fullViewHeight ? fullViewHeight : height + 'px;');
                }
            } else if (!isLandscapeMode && this.get('previousParent')) {
                this.get('previousParent').appendChild(widgetContainer);
                widgetContainer.setAttribute('style', this.get('previousWatchListStyleAttribute'));
                fullScreenContainer.setAttribute('style', this.get('previousFullScreenContainerStyleAttribute'));
                this.set('previousParent', null);

                if (regularViewHeight && widget) {
                    widget.setAttribute('style', 'height:' + regularViewHeight + 'px;');
                }
            }
        }
    },

    _updateChartConfigs: function (isEnabled) {
        var baseChart = this.get('baseChart');

        if (baseChart) {
            baseChart.allowZoom = isEnabled;
            baseChart.allowScroll = isEnabled;

            if (baseChart.chart) {
                baseChart.chart.xAxis.displayGridLines = isEnabled;
                baseChart.chart.yAxis.displayGridLines = isEnabled;
            }

            if (isEnabled) {
                var vSD = STX.Studies.quickAddStudy(baseChart, this.VChartType, {});
                this.chartStudies.pushObject(vSD);
            }
        }
    },

    _setChartStyle: function (style) {
        var baseChart = this.get('baseChart');

        this.set('chartStyle', style);

        if (baseChart) {
            baseChart.setChartType(style.ChartIQChartType);
        }
    },

    _clearChartStudies: function (isReset) {
        var that = this;
        var chartStudies = this.get('chartStudies');
        var baseChart = this.get('baseChart');

        if (baseChart && chartStudies && chartStudies.length > 0) {
            var allowedStudies = [];
            var len = chartStudies.length;

            for (var i = 0; i < len; ++i) {
                var study = chartStudies.popObject();

                if (!isReset || (study && study.name !== that.VChartType)) {
                    STX.Studies.removeStudy(baseChart, study);
                } else if (study) {
                    allowedStudies.pushObject(study);
                }
            }

            this.set('chartStudies', allowedStudies);
        }
    },

    actions: {
        setChartStyle: function (option) {
            this._setChartStyle(option);
        },

        onCreateStudy: function (option) {
            var indicatorId = option.ChartIQID;
            var baseChart = this.get('baseChart');

            if (!baseChart || !baseChart.chart.dataSet || !indicatorId) {
                return;
            }

            var libraryEntry = STX.Studies.studyLibrary[indicatorId];

            // Keep track on created studies in order to remove later
            this.chartStudies.pushObject(STX.Studies.addStudy(baseChart, indicatorId, libraryEntry.inputs, libraryEntry.outputs));
        },

        onOrientationChanged: function (isLandscape) {
            this.onOrientationChanged(isLandscape);
        },

        onResetChart: function () {
            this._clearChartStudies(true);
        }
    }
});

Ember.Handlebars.helper('chart-status-panel', ChartStatusPanel);
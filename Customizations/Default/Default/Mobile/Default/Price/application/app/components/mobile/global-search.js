import Ember from 'ember';
import sharedService from '../../models/shared/shared-service';
import searchResultItem from '../../models/price/business-entities/search-result-item';
import priceWidgetConfig from '../../config/price-widget-config';
import PriceConstants from '../../models/price/price-constants';
import responsiveHandler from '../../helpers/responsive-handler';
import GlobalSearch from '../global-search';
import languageDataStore from '../../models/shared/language/language-data-store';
import utils from '../../utils/utils';

export default GlobalSearch.extend({
    layoutName: 'components/mobile/global-search',
    priceService: sharedService.getService('price'),
    app: languageDataStore.getLanguageObj(),

    isMultipleExgAvailable: false,
    isEnableOptionSearch: false,
    isCall: true,

    showContent: function () {
        var that = this;

        Ember.run.later(function () {
            that.checkResponsive(that);
        }, 100);
    }.observes('content', 'content.[]'),

    searchKeyDidChange: function () {
        Ember.set(this, 'searchKey', this.get('searchKey').toUpperCase());

        if (this.get('isEnableOptionSearch')) {
            // Every time a key is pressed, this event fires, and that event will start the filter in given time interval
            Ember.run.debounce(this, this.getOptionSymbolSearchResult, PriceConstants.TimeIntervals.SearchAutoCompleteInterval);
        } else {
            Ember.run.debounce(this, this.getSearchResult, PriceConstants.TimeIntervals.SearchAutoCompleteInterval);
        }
    }.observes('searchKey'),

    isSymbolSelect: function () {
        this.set('isDisableBtn', !this.get('selectedSymbol'));
    }.observes('selectedSymbol'),

    getOptionSymbolSearchResult: function () {
        if (this.get('isEnableOptionSearch')) {
            // Focus search area
            Ember.$('#titlePanelSearchArea').find('#titlePanelSeach')[0].focus();

            var searchKey = this.get('searchKey');
            var exclInst = this.get('excludedInstruments.length') > 0 ? this.get('excludedInstruments') : [utils.AssetTypes.Option];

            // Clear data
            this.set('optionSymbolContent', []);
            this.set('optionDates', []);
            this.set('optionPrices', []);
            this.set('selectedSymbol', '');
            this.set('isShowNotAvailable', false);

            if (searchKey) {
                this.priceService.searchDS.filterSymbolSearchResults(searchKey, sharedService.userSettings.currentLanguage, exclInst, {isOptionMode: false});
                this.setAddToWatchListIconCss();
            }
        }
    }.observes('isEnableOptionSearch'),

    changeSymbol: function () {
        var selectedSymbol = this.get('selectedSymbol');

        if (selectedSymbol) {
            var status = this.get('isCall') ? 'call' : 'put';
            this.set('selectedSymbolDes', this._getSymbolDes(selectedSymbol, status));
        }
    }.observes('isCall'),

    getOptionSymbol: function () {
        if (this.get('isEnableOptionSearch') && this.get('content')) {
            var content = this.get('content');
            var contentFirstElement = content[0];

            if (contentFirstElement && contentFirstElement.contents && contentFirstElement.contents[0]) {
                // Content available
                Ember.run.debounce(this, this.sortOptionSymbol, content, 600);
            }
        }
    }.observes('content', 'content.[]'),

    sortOptionSymbol: function (content) {
        var optionSymbolContent = [];

        if (content && content.length > 0) {
            Ember.$.each(content, function (key, group) {
                if (group.contents && group.contents.length > 0) {
                    Ember.$.each(group.contents, function (id, symbol) {
                        optionSymbolContent[optionSymbolContent.length] = Ember.Object.create({sym: symbol.sym, exg: symbol.exg, inst: symbol.inst, lDes: symbol.lDes});
                    });
                }
            });
        }

        this.set('optionSymbolContent', optionSymbolContent);

        if (optionSymbolContent[0]) {
            this._onFirstSymbolSelect(optionSymbolContent[0], '0');
        }
    },

    checkResponsive: function (controller) {
        if (controller.get('content')) {
            var content = controller.get('content');
            var contentFirstElement = content[0];

            if (contentFirstElement && contentFirstElement.contents && contentFirstElement.contents[0]) {
                // Content available
                controller.setResponsive(controller, content);
            }
        }
    },

    setResponsive: function (controller, content) {
        controller.set('responsive', responsiveHandler.create({controller: controller, widgetId: 'globalSearch', callback: controller.onResponsive}));

        Ember.$.each(content, function (key, group) {
            Ember.$.each(group.contents, function (id, symbol) {
                var responsiveKey = [key, id].join('');

                Ember.set(symbol, 'responsiveID', responsiveKey);
                controller.responsive.addList(responsiveKey, [{id: 'row-' + responsiveKey, width: 5}]);
            });
        });

        controller.responsive.initialize();
    },

    onResponsive: function () {
        // Called from responsive-handler when responsive level changed.
    },

    setModalPopup: function () {
        var that = this;
        var firstElementIndex = 0;
        var additionalHeightForDotPanel = 0;
        var maxOffScreenLength = '25000px';
        var searchMenuHeight = 62;
        var searchSubmenuHeight = 29;
        var optionSubmenuHeight = 35;

        var searchPanel = Ember.$('#nav-mid');
        var popup = Ember.$('#modalContainer');
        var fullHeight = Ember.$('#mainPanelOutletContainer');
        var notificationPanel = Ember.$('#notificationPanel');
        var popupFirstElement = popup[firstElementIndex];

        // Generate popup off the screen
        if (popup && popup.length > 0) {
            popupFirstElement.style.left = maxOffScreenLength;
            popupFirstElement.style.top = maxOffScreenLength;
        }

        // Change popup styles dynamically after showing modal popup
        Ember.run.later(function () {
            if (notificationPanel && notificationPanel.length > 0) {
                additionalHeightForDotPanel = notificationPanel[firstElementIndex].offsetHeight + 1;
            }

            if (searchPanel && popup && popup.length > 0 && searchPanel.length > 0) {
                var PopUpHeight = fullHeight[firstElementIndex].offsetHeight + additionalHeightForDotPanel;
                var searchPopUpHeight = PopUpHeight + 'px';
                var searchPopUpHeightMenu = (PopUpHeight - searchSubmenuHeight) + 'px';

                // set height for option symbol column height
                var searchPopUpHeightOption = (PopUpHeight - searchMenuHeight) + 'px';
                var dateColumnHeight = (PopUpHeight - searchMenuHeight - optionSubmenuHeight) + 'px';

                that.set('searchPopUpHeight', searchPopUpHeightMenu);
                that.set('searchPopUpHeightOption', searchPopUpHeightOption);
                that.set('dateColumnHeight', dateColumnHeight);
                popup[firstElementIndex].style.width = searchPanel[firstElementIndex].offsetWidth + 'px';

                Ember.$('div.search-pop-up').css('height', searchPopUpHeight);

                popup[firstElementIndex].style.height = searchPopUpHeight;
                popup.addClass('container-back-color');
                popup.addClass('border-bottom hide-scroll left-zero');
            }

            that.getSymbolResultContent(that);
            that.setMultipleExgAvailability(that);

            popup[firstElementIndex].style.left = 0;
            popup[firstElementIndex].style.top = 0;
        }, 50);
    }.on('didInsertElement'),

    getSymbolResultContent: function (controller) {
        var symbol;
        var groupingObj;
        var myFavoriteSymbols;
        var isAvailableInRecentArray = false;
        var config = priceWidgetConfig.globalSearch.groups;
        var recentSearched = controller.priceService.searchDS.getRecentSearchedItems();
        var searchResultContent = recentSearched.slice();
        var customWl = controller.priceService.watchListDS.customWatchListArray;
        var languageObj = languageDataStore.getLanguageObj();

        if (customWl.length > 0) {
            Ember.$.each(customWl, function (customWlKey, watchList) {
                myFavoriteSymbols = watchList.stkArray;

                Ember.$.each(myFavoriteSymbols, function (wlKey, WLSymbol) {
                    isAvailableInRecentArray = false;

                    Ember.$.each(searchResultContent, function (key, recentSymbol) {
                        if (recentSymbol.sym === WLSymbol.sym) {
                            isAvailableInRecentArray = true;
                        }
                    });

                    if (!isAvailableInRecentArray) {
                        var stockFromStore = controller.priceService.stockDS.getStock(WLSymbol.exg, WLSymbol.sym);
                        var exchange = controller.priceService.exchangeDS.getExchange(stockFromStore.exg);

                        symbol = searchResultItem.create();
                        groupingObj = config[stockFromStore.ast] ? config[stockFromStore.ast] : config.other;

                        symbol.setData({
                            sym: stockFromStore.sym,
                            exg: stockFromStore.exg,
                            dSym: stockFromStore.dSym,
                            inst: stockFromStore.inst,
                            lDes: stockFromStore.lDes,
                            sDes: stockFromStore.sDes,
                            ast: stockFromStore.ast,
                            subMkt: stockFromStore.subMkt,
                            dispProp1: stockFromStore.get('dispProp1'),
                            groupingObj: groupingObj
                        });

                        symbol.set('de', exchange.de);
                        symbol.set('isAddedToCustomWatchList', controller.priceService.watchListDS.isSymbolAvailableInCustomWL(symbol));

                        searchResultContent[searchResultContent.length] = symbol;
                    }
                });
            });
        }

        Ember.$.each(searchResultContent, function (key, searchResult) {
            var stockFromStore = controller.priceService.stockDS.getStock(searchResult.exg, searchResult.sym);
            searchResult.set('lDes', stockFromStore.get('lDes') ? stockFromStore.get('lDes') : searchResult.get('lDes'));
        });

        controller.get('content').clear();

        controller.get('content').pushObject(Ember.Object.create({
            name: languageObj.lang.labels.recentSymbols,
            contents: searchResultContent
        }));
    },

    setMultipleExgAvailability: function (controller) {
        var userExg = sharedService.getService('price').userDS.get('userExchg');

        if (userExg && userExg.length > 1) {
            controller.set('isMultipleExgAvailable', true);
        }
    },

    _onItemSelected: function (item) {
        sharedService.userState.globalArgs.exg = item.exg;

        // Check whether notification enabled for particular search
        var stopNotify = this.get('stopGlobalNotification');

        if (!stopNotify) {
            var currentExchange = sharedService.userSettings.currentExchange;

            // Since exchange is changed globally when setting globalArgs.exg (for Mobile), exchange meta should be retrieved
            // If meta data already available, only meta version will be checked, this is further restricted by checking current exg
            if (item.exg !== currentExchange) {
                this.priceService.exchangeDS.getExchangeMetadata(item.exg, true);
            }
        }

        // this._super(item, link); // Enable this after fixing issue in Exg
    },

    // Commented to fixed popup height issue

    // setPopUpHeight: function () {
    //    this.resizeEventHandler = this.setModalPopup.bind(this);
    //    window.addEventListener('resize', this.resizeEventHandler);
    // } .on('didInsertElement'),

    _getSymbolDes: function (symObj, status) {
        // Generate symbol description
        var optPrd = utils.moment(utils.formatters.convertStringToDate(symObj.optPrd)).format('MMM YYYY');

        return [symObj.baseSym, optPrd, symObj.strkPrc, status].join(' ');
    },

    _setSymbol: function (symObj, status) {
        this.set('selectedSymbol', symObj);
        this.set('selectedSymbolDes', this._getSymbolDes(symObj, status));
    },

    _sendOptionDataRequest: function (exg, sym, index) {
        var that = this;

        this.priceService.sendOptionChainRequest({sym: sym, exg: exg, inst: 0, optPeriod: '', optListType: 0, nearMon: 0, optType: 2},
            function () {
                that._setOptionPeriod(exg, sym, index);
            });

        this.set('isDateLoading', true);
    },

    _setOptionPeriod: function (exg, sym, index) {
        var that = this;
        var dataID = index ? index : '0';
        var optPeriods = this.priceService.optionPeriodDS.getOptionPeriodList(exg, sym);

        if (optPeriods.length) {
            this.set('isShowNotAvailable', false);

            Ember.$.each(optPeriods, function (id, period) {
                // Check period.optPrd is date string or not (Date string has 8 characters 'YYYYMMDD')
                period.isMonthOption = period.optPrd && period.optPrd.length < 8;
            });

            this.set('optionDates', optPeriods);

            // Set default view
            var stockArr = this.priceService.optionStockDS.getOptionStockList(exg, sym, optPeriods[0].optPrd);

            if (stockArr.length) {
                var fistPriceRow = stockArr[0];
                var status = this.get('isCall') ? 'call' : 'put';

                this.set('optionPrices', stockArr);
                this._setSymbol(fistPriceRow, status);

                Ember.run.later(function () {
                    that._addRowColor('symbol-' + dataID, that.previousSymbolID, 'active-symbol-row');
                    that._addRowColor('date-0', that.previousDateID, 'active-date-row');
                    that._addRowColor('price-0', that.previousPriceID, 'active-price-row');

                    that.previousSymbolID = 'symbol-' + dataID;
                    that.previousDateID = 'date-0';
                    that.previousPriceID = 'price-0';
                }, 10);
            }
        } else {
            this.set('isShowNotAvailable', true);
        }

        this.set('isDateLoading', false);
    },

    _setOptionPrice: function (exg, sym, optPrd, index) {
        var that = this;
        var stockArr = this.priceService.optionStockDS.getOptionStockList(exg, sym, optPrd);
        var selectDateID = 'date-' + index;

        this.set('isPriceLoading', false);
        this.set('optionPrices', stockArr);
        this._addRowColor(selectDateID, this.previousDateID, 'active-date-row');
        this.set('previousDateID', selectDateID);

        // Set default view
        if (stockArr.length) {
            var fistPriceRow = stockArr[0];
            var status = this.get('isCall') ? 'call' : 'put';

            this._setSymbol(fistPriceRow, status);

            Ember.run.later(function () {
                that._addRowColor('price-0', undefined, 'active-price-row');
                that.previousPriceID = 'price-0';
            }, 10);
        }
    },

    _addRowColor: function (selectSymbolID, previousSymbolID, className) {
        if (previousSymbolID) {
            Ember.$('[data-id=' + previousSymbolID + ']').removeClass(className);
        }

        Ember.$('[data-id=' + selectSymbolID + ']').addClass(className);
    },

    _onFirstSymbolSelect: function (row, index) {
        this.set('optionDates', []);
        this.set('optionPrices', []);
        this.set('selectedSymbol', '');
        this.set('isShowNotAvailable', false);

        var selectSymbolID = 'symbol-' + index;
        this._addRowColor(selectSymbolID, this.previousSymbolID, 'active-symbol-row');

        this._sendOptionDataRequest(row.exg, row.sym, index);
        this.set('previousSymbolID', selectSymbolID);
    },

    isOptionSymbolSearchEnabled: function () {
        return this.priceService.userDS.get('isOptionSymbolSearchEnabled');
    }.property(),

    actions: {
        onOptionSymbolSelect: function (row, index) {
            this._onFirstSymbolSelect(row, index);
        },

        onOptionDateSelect: function (row, index) {
            var that = this;

            this.set('optionPrices', []);
            this.set('selectedSymbol', '');

            var stockArr = this.priceService.optionStockDS.getOptionStockList(row.exg, row.sym, row.optPrd);

            if (stockArr.length > 0) {
                this._setOptionPrice(row.exg, row.sym, row.optPrd, index);
            } else {
                var optType = row.optPrd.length === 8 ? 1 : 0;
                this.set('isPriceLoading', true);

                this.priceService.sendOptionChainRequest({sym: row.sym, exg: row.exg, inst: 0, optPeriod: row.optPrd, optListType: 1, nearMon: 0, optType: optType},
                    function () {
                        that._setOptionPrice(row.exg, row.sym, row.optPrd, index);
                    });
            }
        },

        onOptionPriceSelect: function (row, index) {
            var selectPriceID = 'price-' + index;
            var status = this.get('isCall') ? 'call' : 'put';

            this._setSymbol(row, status);

            // Add color to selected row
            this._addRowColor(selectPriceID, this.previousPriceID, 'active-price-row');
            this.set('previousPriceID', selectPriceID);
        },

        onSelect: function () {
            var symbol = this.get('selectedSymbol');

            if (symbol) {
                if (this.get('isCall')) {
                    this.onItemSelected(symbol.cStock, this.get('defaultLink'));
                } else {
                    this.onItemSelected(symbol.pStock, this.get('defaultLink'));
                }
            }
        }
    }
});


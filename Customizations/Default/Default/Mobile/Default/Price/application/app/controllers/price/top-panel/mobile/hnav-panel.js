/* global Hammer */
import Ember from 'ember';
import BaseWidgetContainer from '../../../base-widget-container';
import sharedService from '../../../../models/shared/shared-service';

export default BaseWidgetContainer.extend({
    containerKey: 'hnavPanel',
    mainOutlet: 'price.widgets.mobile.market-overview-tab',

    options: {
        dragLockToAxis: true,
        dragBlockHorizontal: true
    },

    onLoadContainer: function () {
        this._super();
        this._setMenuTitle();
    },

    languageChanged: function () {
        this._setMenuTitle();
    },

    onAfterRender: function () {
        var that = this;
        var initHorizontalPosition = 0;
        var initVerticalPosition = 0;

        var hammerObj = new Hammer(document.getElementById('mainPanelOutletContainer'), this.options);
        hammerObj.get('swipe').set({velocity: 0.1});

        // Prevent default horizontal behavior and let Hammer handles it
        Ember.$('#mainPanelOutletContainer').bind('touchstart', function (e) {
            initVerticalPosition = e.originalEvent.touches[0].clientY;
            initHorizontalPosition = e.originalEvent.touches[0].clientX;
        });

        Ember.$('#mainPanelOutletContainer').bind('touchmove', function (e) {
            var currentY = e.originalEvent.touches[0].clientY;
            var currentX = e.originalEvent.touches[0].clientX;

            if (Math.abs(initVerticalPosition - currentY) < Math.abs(initHorizontalPosition - currentX)) {
                e.preventDefault();
            }
        });

        hammerObj.on('swipeleft swiperight', function (ev) {
            var activeTab = sharedService.getService('sharedUI').getService('mainPanel').get('activeTab');
            var menuContent = sharedService.getService('sharedUI').getService('mainPanel').get('menuContent');
            var swipeDirection = 'swipeleft';

            if (sharedService.userSettings.currentLanguage === 'AR') { // For Arabic Language
                swipeDirection = 'swiperight';
            }

            if (!Ember.isQuoteStatusSwipeTriggered) {
                if (ev.type === swipeDirection) {
                    var swipeNextTab = that.getSwipedTabItem(menuContent.tab, activeTab, true);

                    if (swipeNextTab) {
                        sharedService.getService('sharedUI').getService('mainPanel').onRenderTabItems(swipeNextTab);
                    }
                } else {
                    var swipePreviousTab = that.getSwipedTabItem(menuContent.tab, activeTab);

                    if (swipePreviousTab) {
                        sharedService.getService('sharedUI').getService('mainPanel').onRenderTabItems(swipePreviousTab);
                    }
                }
            } else {
                Ember.isQuoteStatusSwipeTriggered = false;
            }
        });
    },

    _setMenuTitle: function () {
        var that = this;
        var menuArray = this.get('appLayout').layout.mainPanel.content;

        Ember.$.each(menuArray, function (key, menuObj) {
            try {
                Ember.set(menuObj, 'displayMainTitle', that.get('app').lang.labels[menuObj.titleKey] ? that.get('app').lang.labels[menuObj.titleKey] : that.get('app').lang.labels[menuObj.title]);
            } catch (e) {
                menuObj.displayMainTitle = that.get('app').lang.labels[menuObj.titleKey] ? that.get('app').lang.labels[menuObj.titleKey] : that.get('app').lang.labels[menuObj.title];
            }
        });
    },

    setActiveMenu: function (currentMenu) {
        var that = this;
        var menuArray = this.get('appLayout').layout.mainPanel.content;

        // At first time, object behaves as a normal javascript object
        // After that object is an ember object
        // Still we cannot call .set() directly on the object
        // Need to call Ember.set() instead
        Ember.$.each(menuArray, function (key, menuObj) {
            if (menuObj.id === currentMenu.id) {
                var subMenuCss = sharedService.userSettings.currentLanguage === 'AR' ? 'dot-panel-container' : 'display-inline-block';

                try {
                    Ember.set(menuObj, 'mainMenuCss', 'appmnu-sidebar-active');
                    Ember.set(menuObj, 'subMenuCss', subMenuCss);
                } catch (e) {
                    menuObj.mainMenuCss = 'appmnu-sidebar-active';
                    menuObj.subMenuCss = subMenuCss;
                }

                if (menuObj.tab.length < 2) {
                    that.set('dotPanelCss', 'display-none');
                } else {
                    that.set('dotPanelCss', '');
                }
            } else {
                try {
                    Ember.set(menuObj, 'mainMenuCss', '');
                    Ember.set(menuObj, 'subMenuCss', 'display-none');
                } catch (e) {
                    menuObj.mainMenuCss = '';
                    menuObj.subMenuCss = 'display-none';
                }
            }
        });
    },

    getSwipedTabItem: function (menuTabArray, activeTab, isNext) {
        var nextMenu = null;
        var tabArray = menuTabArray;
        var tabArrayLength = tabArray.length;
        var initialMenuKey = isNext ? 0 : tabArrayLength - 1;
        var endMenuKey = isNext ? tabArrayLength - 1 : 0;

        Ember.$.each(tabArray, function (key, tabObj) {
            if (tabObj.title === activeTab.title) {
                if (key === endMenuKey) {
                    nextMenu = tabArray[initialMenuKey];
                } else {
                    nextMenu = tabArray[isNext ? key + 1 : key - 1];
                }

                return false;
            }
        });

        return nextMenu;
    }
});
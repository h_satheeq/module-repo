import Ember from 'ember';
import PriceUIService from '../../controllers/price/price-ui-service';
import sharedService from '../../models/shared/shared-service';
import utils from '../../utils/utils';
import appEvents from '../../app-events';
import priceConstants from '../../models/price/price-constants';

export default PriceUIService.extend({
    subscriptionKey: 'priceUI',

    lastMenuStack: Ember.A(),
    isDeviceAwake: false,

    onLayoutReady: function (appLayout) {
        this.initializeComponents(appLayout);
        this._loadIndexChangeComponent();

        // Load Child View
        this.loadView(appLayout, 'childView', 'component:child-view', 'components/child-view', 'childViewOutlet');
        this.setTitleBarDefaultComponent();
        appEvents.subscribeVisibilityChanged(this, this.get('subscriptionKey'));
    },

    _loadIndexChangeComponent: function () {
        var layoutNameAppTitle = 'price/widgets/mobile/components/title-index-panel';
        var indexChangeComponent = sharedService.getService('sharedUI').getService('title-index-panel');

        sharedService.getService('sharedUI').getService('titleBar').renderAppTitleTemplate(layoutNameAppTitle, indexChangeComponent);
    },

    setTitleBarDefaultComponent: function () {
        var titleBar = sharedService.getService('sharedUI').getService('tickerPanel');
        var defaultComponentName = 'price/top-panel/mobile/announcement-panel';
        var defaultComponent = this.container.lookupFactory('controller:price/top-panel/mobile/announcement-panel').create();

        defaultComponent.initializeWidget({wn: defaultComponentName.split('/').pop()}, {widgetArgs: {selectedLink: 1}});
        titleBar.setDefaultComponent(defaultComponentName, defaultComponent);
    },

    pushLastMenuStack: function (menuContent, tabContent) {
        var currentStack = this.get('lastMenuStack');
        var newMenu = {menuContent: menuContent, tabContent: tabContent};

        if (currentStack.indexOf(newMenu) < 0) {
            currentStack.pushObject(newMenu);
        }
    },

    popLastMenuStack: function () {
        var currentStack = this.get('lastMenuStack');
        var lastMenu;

        if (currentStack.length > 0) {
            lastMenu = currentStack[currentStack.length - 1];
            currentStack.pop();
        }

        return lastMenu;
    },

    resetLastMenuStack: function () {
        this.set('lastMenuStack', Ember.A());
    },

    loadView: function (appLayout, name, controllerPath, viewPath, outletName) {
        this.container = appLayout.container;

        var route = this.container.lookup('route:application');
        var controller = this.container.lookupFactory(controllerPath).create();

        route.render(viewPath, {
            into: 'application',
            outlet: outletName,
            controller: controller
        });

        sharedService.getService('sharedUI').registerService(name, controller);
    },

    showChildView: function (viewName, widgetController, widgetName, parentView) {
        sharedService.getService('sharedUI').getService('childView').showChildView(viewName, widgetController, widgetName, parentView);
    },

    closeChildView: function (viewName, close) {
        sharedService.getService('sharedUI').getService('childView').closeChildView(viewName, close);
    },

    onVisibilityChanged: function (isHidden) {
        var that = this;

        if (!isHidden) {
            that.set('isDeviceAwake', true);

            Ember.run.later(function () {
                that.set('isDeviceAwake', false);
            }, priceConstants.TimeIntervals.DeviceWakeTimeout);
        }
    },

    shareScreenshot: function (message) {
        var that = this;

        if (navigator && navigator.screenshot) {
            var parentNode;
            var imgElem;

            if (!Ember.isIos) {
                var canvasElems = document.getElementsByTagName('canvas');
                var canvas = canvasElems[0];
                imgElem = document.createElement('img');

                if (canvas && imgElem) {
                    imgElem.width = canvas.clientWidth;
                    imgElem.height = canvas.clientHeight;
                    imgElem.src = canvas.toDataURL('image/png');

                    parentNode = canvas.parentNode.parentNode; // Parent node to insert image

                    if (parentNode) {
                        parentNode.prepend(imgElem);

                        for (var i = 0; i < canvasElems.length; i++) {
                            if (canvasElems[i]) {
                                canvasElems[i].hidden = true;
                            }
                        }
                    }
                }
            }

            Ember.run.later(this, function () {
                navigator.screenshot.save(function (error, res) {
                    if (!error && res.filePath && window.plugins && window.plugins.socialsharing) {
                        var langLabels = that.get('app').lang.labels;
                        var StringConst = utils.Constants.StringConst;

                        var shareOptions = {
                            message: [message, StringConst.Space, StringConst.NumberSign, langLabels.companyName,
                                StringConst.Space, StringConst.NumberSign, langLabels.productTitle, StringConst.Space, StringConst.Hyphen, StringConst.Space, langLabels.screenShare].join(StringConst.Empty),
                            subject: [langLabels.companyName, langLabels.productTitle, StringConst.Hyphen, langLabels.screenShare].join(StringConst.Space),
                            files: ['file:///' + res.filePath]
                        };

                        window.plugins.socialsharing.shareWithOptions(shareOptions);

                        if (canvasElems && parentNode && imgElem) {
                            for (i = 0; i < canvasElems.length; i++) {
                                if (canvasElems[i]) {
                                    canvasElems[i].hidden = false;
                                }
                            }

                            parentNode.removeChild(imgElem);
                        }
                    }
                });
            }, 100);
        }
    },

    notifyPriceConnectionStatus: function () {
        var that = this;

        if (that.get('isDeviceAwake')) {
            Ember.run.later(function () {
                that._showHideConnectionStatus();
            }, priceConstants.TimeIntervals.ShowNotificationTimeout);
        } else {
            that._showHideConnectionStatus();
        }
    }.observes('priceService', 'priceService.connectionStatus'),

    _showHideConnectionStatus: function () {
        var that = this;
        var layoutName = 'components/single-message-viewer';
        var messageComponent = that.sharedUIService.getService('single-message-viewer');
        var titleBar = that.sharedUIService.getTitleBar();

        if (messageComponent && titleBar && titleBar.renderNotificationTemplate) {
            if (!that.get('priceService').connectionStatus) {
                messageComponent.set('message', that.get('app') ? that.get('app').lang.messages.priceDisconnected : '');
                messageComponent.set('showMessage', true);
                messageComponent.set('messageCss', '');
                messageComponent.set('type', utils.Constants.MessageTypes.Error);
                titleBar.renderNotificationTemplate(layoutName, messageComponent);
            } else {
                messageComponent.set('showMessage', false);
                titleBar.hideNotificationTemplate(layoutName, messageComponent);
            }
        }
    }
});

import Ember from 'ember';
import BaseController from '../../../base-controller';
import ThemeDataStore from '../../../../models/shared/data-stores/theme-data-store';
import LanguageDataStore from '../../../../models/shared/language/language-data-store';
import sharedService from '../../../../models/shared/shared-service';

export default BaseController.extend({
    title: 'Settings',
    supportedThemes: Ember.A([]),
    supportedLanguages: Ember.A([]),

    onLoadWidget: function () {
        this.set('title', this.get('app').lang.labels.settings);
    },

    onPrepareData: function () {
        this.set('supportedThemes', this._getSupportedSettings(ThemeDataStore.getUserThemes()));
        this.set('supportedLanguages', this._getSupportedSettings(LanguageDataStore.getUserLanguages()));
    },

    _getSupportedSettings: function (supportedThemes) {
        var that = this;
        var settingsArray = Ember.A([]);

        Ember.$.each(supportedThemes, function (index, item) {
            item.langDesc = item.langKey ? that.get('app').lang.labels[item.langKey] : item.desc;
            settingsArray.pushObject(Ember.Object.create(item));
        });

        return settingsArray;
    },

    onAfterRender: function () {
        this.initializeThemeSettings();
        this.initializeLanguageSettings();
    },

    initializeThemeSettings: function () {
        this._initializeSettings(this.get('supportedThemes'), sharedService.userSettings.currentTheme);
    },

    initializeLanguageSettings: function () {
        this._initializeSettings(this.get('supportedLanguages'), sharedService.userSettings.currentLanguage);
    },

    _initializeSettings: function (settingArray, currentValue) {
        Ember.$.each(settingArray, function (index, item) {
            Ember.set(item, 'active', currentValue && item && currentValue.indexOf(item.code) !== -1 ? 'glyphicon glyphicon-ok' : 'theme-settings');
        });
    },

    onLanguageChanged: function () {
        this.onLoadWidget();
        this.onPrepareData();
    },

    actions: {
        changeTheme: function (code) {
            ThemeDataStore.changeTheme(code);
            this.initializeThemeSettings();
        },

        changeLanguage: function (code) {
            LanguageDataStore.changeLanguage(code);
            this.initializeLanguageSettings();
            this.initializeThemeSettings();
        }
    }
});